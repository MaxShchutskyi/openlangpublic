$(function() {

function isRightCheck() {
  $(".content-footer").css("background-color","#f0f7e8");
  $(".check-sign").css("display","inline-block");
  $(".check-sign").css("color","#87d500");
  $(".content-footer__next").css("display","inline-block");
  
  $(".click-check").css("color","#87d500").html("Correct");
  

}
function isWrongCheck(rightAnswer) {
  $(".content-footer").css("background-color","#fdeeef");
  $(".click-check").css("color","#ff4b5b").html(rightAnswer);
  $(".content-footer__next").css("display","inline-block");
  // $(".content-footer__next button").css("background-color","#ee5a60");

  $(".check-sign").css("display","none");
}

function secondEnter(){
	$( "body" ).keypress(function(e) {
	if (e.keyCode == 13) {
		event.preventDefault();
    console.log('enter');
		$('button').trigger('click');
	}
});
};

////////////////   Remove text from placeholder when focus it   ////////////////////////

$(document).ready(function () {
 $('textarea').focus(function(){
   $(this).data('placeholder',$(this).attr('placeholder'))
   $(this).attr('placeholder','');
 });
 $('input,textarea').blur(function(){
   $(this).attr('placeholder',$(this).data('placeholder'));
 });
 });


/////////////////   Right answer checker  ////////////////////////////////

////////////////   Index   //////////////////////////////////////////////
	$( document ).ready(function() {
    $('.radio-nav').one("click", function(){


    $(".words-check").one( "click", function() {
  var inp = document.getElementsByName('paragrath');
    for (var i = 0; i < inp.length; i++) {
        if (inp[i].type == "radio" && inp[i].checked) {
            
            if (inp[i].value === "a") {

            	isRightCheck();
              $('#correct').css('background-color', '#87d500');
              $('#correct').css('border-color', '#87d500');
            	secondEnter();
            }
            else{

              var rightAnswer = "Wrong";
            	isWrongCheck(rightAnswer);
              $('#correct').css('background-color', '#87d500');
              $('#correct').css('border-color', '#87d500');
              $('#correct').css('color', 'white');
              $('input.checked').css('color', '#87d500');

            }
        }
    }

});
    });
$( "#choice" ).keypress(function(e) {
	if (e.keyCode == 13) {
		event.preventDefault();
		var inp = document.getElementsByName('paragrath');
		for (var i = 0; i < inp.length; i++) {
        if (inp[i].type == "radio" && inp[i].checked) {
            console.log(inp[i].value);
            if (inp[i].value === "a") {
            	isRightCheck();
            	secondEnter();
            }
            else{
            	var rightAnswer = "Wrong";
              isWrongCheck(rightAnswer);
              $('#correct').css('background-color', '#87d500');
              $('#correct').css('border-color', '#87d500');
              $('#correct').css('color', 'white');
              $('input.checked').css('color', '#87d500');
              secondEnter();
            }
        }
    }
   } 
  
});

});


////////////////////    TextAREA (Video)   /////////////////////////

$( document ).ready(function() {

    $("#car-check").one( "click", function() {
      event.preventDefault();
  var car = $("#carquestion").val();
    if (car === "max") {
    	isRightCheck();
    	secondEnter();
    }
    else {
      var rightAnswer = "Max";
      isWrongCheck(rightAnswer);
      $("#carquestion").val('');
      secondEnter();
            }
     
 });

    $( "#video" ).keypress(function(e) {
	if (e.keyCode == 13) {
		event.preventDefault();
		var car = $("#carquestion").val();
    if (car === "max") {
    	isRightCheck();
    	secondEnter();
    }
    else {
      var rightAnswer = "Max";
      isWrongCheck(rightAnswer);
      $("#carquestion").val('');
      secondEnter();
            }

	}
});
});

////////////////////////   PICTURES   ///////////////////////////

	$( document ).ready(function() {
    $('.pictures-nav').one("click", function(){

    $("#pictures-check").one( "click", function() {
  var pic = document.getElementsByName('pictures');
    for (var i = 0; i < pic.length; i++) {
        if (pic[i].type == "radio" && pic[i].checked) {
            
            if (pic[i].value === "2") {
            	isRightCheck();
              $('#correct').css('background-color', '#87d500');
              $('#correct').css('border-color', '#87d500');
              $('#correct').css('color', 'white');
              $('#correct').parent().css('background-color', '#f0f7e8');
            	secondEnter();
            }
            else{
              $('#correct').css('background-color', '#87d500');
              $('#correct').css('border-color', '#87d500');
              $('#correct').css('color', 'white');
              $('#correct').parent().css('background-color', '#f0f7e8');
            	var rightAnswer = "Wrong";
              isWrongCheck(rightAnswer);
              secondEnter();
            }
        }
    }

});
  });

$( "#pictures" ).keypress(function(e) {
	if (e.keyCode == 13) {
		var pic = document.getElementsByName('pictures');
    for (var i = 0; i < pic.length; i++) {
        if (pic[i].type == "radio" && pic[i].checked) {
            
            if (pic[i].value === "2") {
            	isRightCheck();
              $('#correct').css('background-color', '#87d500');
              $('#correct').css('border-color', '#87d500');
              $('#correct').css('color', 'white');
              $('#correct').parent().css('background-color', '#f0f7e8');
            	secondEnter();
            }
            else{
            	var rightAnswer = "Wrong";
              isWrongCheck(rightAnswer);
              $('#correct').css('background-color', '#87d500');
              $('#correct').css('border-color', '#87d500');
              $('#correct').css('color', 'white');
              $('#correct').parent().css('background-color', '#f0f7e8');
              secondEnter();
            }
        }
    }
	}
});
});


//////////          WORDS        //////////////////	

$( document ).ready(function() {
	
$('.answer-nav li input').click(function(){
  $('.answer-nav li').css('background', 'white');
  $('.answer-nav li').css('color', '#009de4');
  $('.answer-nav li').css('border-color', '#009de4');
  $(this).parent().css('background', '#eef2f5');
  $(this).parent().css('color', '#eef2f5');
  $(this).parent().css('border-color', '#ddd');
  $('#task-word').css('background-color', '#009de4');
  $('#task-word').css('color', 'white');
  // $('.answer-nav li').css('cursor', 'default');

		var wrd = $(this).val();
		$('#task-word').html(wrd);
}); 	
	
$("#words-check").one( "click", function() {

  var wor = document.getElementsByName('words');
  
    for (var i = 0; i < wor.length; i++) {
        if (wor[i].type == "radio" && wor[i].checked) {
            
            if (wor[i].value === "Deutsch") {
            	isRightCheck();
              $('#task-word').css('background-color', '#99d332');
            	secondEnter();
            }
            else{
            	var rightAnswer = "Ich spreche kein Deutsch.";
              isWrongCheck(rightAnswer);
              $('#task-word').css('background-color', '#ee5a60');
              secondEnter();
            }
        }
    }

});

$( "#words" ).keypress(function(e) {
	if (e.keyCode == 13) {
		var wor = document.getElementsByName('words');
  
    for (var i = 0; i < wor.length; i++) {
        if (wor[i].type == "radio" && wor[i].checked) {
            
            if (wor[i].value === "Deutsch") {
            	isRightCheck();
              $('#task-word').css('background-color', '#99d332');
            	secondEnter();
            }
            else{
            	var rightAnswer = "Ich spreche kein Deutsch.";
              isWrongCheck(rightAnswer);
              $('#task-word').css('background-color', '#ee5a60');
              secondEnter();
            }
        }
    }
	}
});
});

//////////////////      SENTENCE      ////////////////////////////////


$( document ).ready(function() {


	var sentence = [];
  var answer = '';
	$('.sentence-nav li').click(function(){
	var lett = $(this).html();
	sentence.push(lett);
	answer = sentence.join(' ');
  
	$('#sentence-word1').html(sentence[0]);
	$('#sentence-word2').html(sentence[1]);
	$('#sentence-word3').html(sentence[2]);
	$('#sentence-word4').html(sentence[3]);

  
	});

	
$("#sentence-check").one( "click", function() {
  
  sentence = [];
  

 
    if (answer === "Lass uns Schach spielen?") {
    	isRightCheck();
      $('#sentence-word1').css('background', '#87d500');
      $('#sentence-word2').css('background', '#87d500');
      $('#sentence-word3').css('background', '#87d500');
      $('#sentence-word4').css('background', '#87d500');
    	secondEnter();
    }
    else {
      isWrongCheck();
      $('#sentence-word1').css('background', '#ee5a60');
      $('#sentence-word2').css('background', '#ee5a60');
      $('#sentence-word3').css('background', '#ee5a60');
      $('#sentence-word4').css('background', '#ee5a60');
      answer = '';
      var rightAnswer = "Lass uns Schach spielen?";
              isWrongCheck(rightAnswer);
      secondEnter();
            }
      });
$( "#sentence" ).keypress(function(e) {
	if (e.keyCode == 13) {

 
	sentence = [];
  console.log(answer);
    if (answer === "Lass uns Schach spielen?") {
    	isRightCheck();
      $('#sentence-word1').css('background', '#87d500');
      $('#sentence-word2').css('background', '#87d500');
      $('#sentence-word3').css('background', '#87d500');
      $('#sentence-word4').css('background', '#87d500');
    	secondEnter();
    }
    else {
      isWrongCheck();
      $('#sentence-word1').css('background', '#ee5a60');
      $('#sentence-word2').css('background', '#ee5a60');
      $('#sentence-word3').css('background', '#ee5a60');
      $('#sentence-word4').css('background', '#ee5a60');
      answer = '';
      var rightAnswer = "Lass uns Schach spielen?";
              isWrongCheck(rightAnswer);
      secondEnter();
            }
	}


});

});

///////////////////////        VOICE       ////////////////////////////////////////

		

			function startRecognizer(){
        
    if ('webkitSpeechRecognition' in window) {
      var recognition = new webkitSpeechRecognition();
      recognition.lang = 'en';

      recognition.onresult = function (event) {
        console.log("hello");
        var result = event.results[event.resultIndex];
          console.clear();
          console.log(result[0].transcript);
          var rs = result[0].transcript;
			        $('#voice-word').html(rs.split(' ')[0]);
			        $('.circle').hide();
							$('.circle2').hide();
			          if (rs.split(' ')[0] == 'Spanish') {
			          		isRightCheck();
                    $("#voice-word").css("background-color","#99d332");
			    					secondEnter();
			    					}
			    			else {
                  var rightAnswer = "I don't speak Spanish";
			      			isWrongCheck(rightAnswer);
                    $("#voice-word").css("background-color","#ee5a60");
                    secondEnter();
					          }
          
      };
      recognition.start();
    } else alert('webkitSpeechRecognition не поддерживается :(');
  }
  $('#voice-record').click(function(){
      $('.circle').show();
      $('.circle2').show();
      startRecognizer();
    });
  

/////////////////////////////////////////////////////////////////////////////////

});
