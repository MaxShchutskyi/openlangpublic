﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Context;
using Domain.Entities.IdentityEntities;
using Domain.Helpers;
using Domain.Helpers.Abstract;
using Domain.Repositories;
using Domain.Repositories.Interfaces;
using Ninject;
using Ninject.Web.Common;
using Repositories.Abstract;
using Repositories.Realization.Dashboard;
using Repositories.Realization.Shared;
using Xencore.Managers;
using Xencore.Managers.Abstract;
using IoC;
using Repositories.Realization.BrainCards;
using Repositories.Realization.Dashboard.Company;
using Repositories.Realization.Refferal;
using BrainCardManager;
using BrainCardManager = BrainCardManager.BrainCardManager;

namespace Xencore.Util
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private readonly IKernel _kernel;
        public NinjectDependencyResolver(IKernel kernelParam)
        {
            //_kernel = IoC.IoC.Instance;
            _kernel = kernelParam;
            IoC.IoC.Instance = _kernel;
            AddBindings();
        }
        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }
        private void AddBindings()
        {
            _kernel.Bind<ICustomUserRepository>().To<AttendeeRepository>();
            _kernel.Bind<IAccountManager>().To<AccountManager>();
            _kernel.Bind<ISchedulerRepository>().To<SchedulerRepository>();
            _kernel.Bind<IPaymentTransactions>().To<PaymentRepository>();
            _kernel.Bind<ITeacherManager>().To<TeacherManager>();
            _kernel.Bind<ICompanyRepository>().To<CompanyRepository>();
            _kernel.Bind<IActionReoisitory>().To<ActionRepository>();
            _kernel.Bind<ICorrespondRepository>().To<CorrespondRepository>();

            _kernel.Bind<DbContext>()
                .To<MyDbContext>();
            _kernel.Bind<ITeacherDashboardRepository>()
                .To<TeacherDashboardRepository>();
            _kernel.Bind<IMyAssingmentsOfStudentRepository>()
                .To<MyAssingmentsOfStudentRepository>();
            _kernel.Bind<IMyAssingmentsOfTeachersRepository>()
                .To<MyAssingmentsOfTeachersRepository>();
            _kernel.Bind<IAbstractSchedulerRepository>().To<ScheduleRepository>();
            _kernel.Bind<ITeacherRatesRepository>().To<TeacherRatesRepository>();
            _kernel.Bind<IReffreralProgrammRepository>().To<RefferalProgrammRepository>();
            _kernel.Bind<IStudentsOfCompanyRepository>().To<StudentsOfCompanyRepository>();
            _kernel.Bind<IPackagesCompany>().To<PackagesCompany>();
            _kernel.Bind<IBrainCardRepo>().To<BrailCardRepo>();
            //_kernel.Bind<IGroupOfCard>().To<GroupOfCardsRepo>();
            //_kernel.Bind<IHomeworkRepo>().To<HomeworkRepo>();
            _kernel.Bind<IPaymentRepository>().To<global::Repositories.Realization.Payments.PaymentRepository>();
            _kernel.Bind<IDiscountRepository>().To<global::Repositories.Realization.Discounts.DiscountRepository>();
            _kernel.Bind<IBrainCardManager>().To<global::BrainCardManager.BrainCardManager>();
            _kernel.Bind<IChapterRepo>().To<ChaptersRepo>();
            _kernel.Bind<ILessonsRepo>().To<LessonsRepo>();
        }
    }
}