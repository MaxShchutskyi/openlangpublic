﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Optimization;
using BundleTransformer.Core.Orderers;
using BundleTransformer.Core.Transformers;
using BundleTransformer.Core.Translators;

namespace Xencore
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Clear();
            BundleTable.EnableOptimizations = true;
            string[] pathstaticCss =
            {
                "~/bootstrap/css/bootstrap.min.css", "~/Css/bootstrap-datetimepicker.min.css",
                "~/Css/intlTelInput.css",
                "~/Css/owl.carousel.css",
                "~/Css/jquery.mCustomScrollbar.min.css",
                "~/Css/select2.min.css",
                "~/Css/menu.css",
                "~/Css/bootstrap-datetimepicker.min.css"
            };
            string[] less =
            {
                "~/less/style.less"
            };
            var cssTransformer = new StyleTransformer();
            var nullOrderer = new NullOrderer();
            var css = new Bundle("~/Content/Css/printPage").Include("~/Content/Css/less/printToPdf.less");
            css.Transforms.Add(cssTransformer);
            css.Orderer = nullOrderer;
            var ls = new StyleBundle("~/less/style").Include(less);
            ls.Transforms.Add(cssTransformer);
            //ls.Transforms.Add(new CssMinify());
            ls.Orderer = nullOrderer;
            bundles.Add(ls);
            bundles.Add(new StyleBundle("~/Content/Css/less").Include("~/Content/Css/less/openlangLess.less"));
            bundles.Add(new StyleBundle("~/less/newless").Include("~/less/newStyle.less"));
            bundles.Add(css);
            bundles.Add(new Bundle("~/js/Select2Angular").Include(//"~/Scripts/jquery-1.9.1.min.js",
                "~/bootstrap/js/bootstrap.min.js",
                //"~/Scripts/circle-progress.min.js",
                "~/js/moment.min.js",
                "~/js/intlTelInput.js",
            "~/Scripts/jquery.validate.min.js",
            "~/Scripts/jquery.validate.unobtrusive.min.js",
            "~/Areas/Dashboard/js/AngularInfrastructure/Validations/SharedTemplateValidation.js",
            "~/js/select2.full.min.js",
            "~/Scripts/angular.min.js",
            "~/Scripts/angular-route.js",
            "~/Scripts/angular-animate.min.js",
            "~/js/Select2Angular.js"));
            bundles.Add(new Bundle("~/Scripts/openalng/Prices").IncludeDirectory("~/Scripts/openlang/Prices", "*js", false));
            bundles.Add(new Bundle("~/Scripts/openalng/SocialAuthorize").IncludeDirectory("~/Scripts/SocialAuthorize", "*js", false));
            bundles.Add(
                new Bundle("~/Scripts/openalng/Geolocation").Include("~/Scripts/openlang/Geolocation/aes.js",
                    "~/Scripts/openlang/Geolocation/Coords.js",
                    "~/Scripts/openlang/Geolocation/AbstractGeolocationManager.js",
                    "~/Scripts/openlang/Geolocation/IGeolocationManager.js",
                    "~/Scripts/openlang/Geolocation/geoipnekudo.js",
                    "~/Scripts/openlang/Geolocation/freeGeoIp.js",
                    "~/Scripts/openlang/Geolocation/IPGeolocationManager.js",
                    "~/Scripts/openlang/Geolocation/HTML5GeolocationManager.js",
                    "~/Scripts/openlang/Geolocation/NullbleGeolocationManager.js",
                    "~/Scripts/openlang/Geolocation/GoogleAPIGeplocationManager.js"));
            bundles.Add(
                new StyleBundle("~/Css/css").Include(pathstaticCss));
            var js = new Bundle("~/js/jss")
                .Include(
                    "~/js/jquery.cookie.js",
                    "~/js/patterns.js",
                    "~/js/intlTelInput.js",
                    "~/js/owl.carousel.min.js",
                    "~/js/jquery.mCustomScrollbar.concat.min.js",
                    "~/bootstrap/js/bootstrap.min.js",
                    "~/js/select2.full.min.js",
                    "~/js/bootstrap-timepicker.js",
                    "~/js/moment.min.js",
                    "~/js/bootstrap-datetimepicker.min.js",
                    "~/js/jquery.maskedinput.min.js",
                    "~/js/validator.min.js",
                    "~/js/modernizr.js",
                    "~/js/jquery.validate.min.js",
                    "~/js/jquery.background-video.js");
            bundles.Add(js);
            bundles.Add(
                new Bundle("~/js/jss2").Include("~/js/main.js",
                    "~/js/scriptsForMainPage.js"));
            //bundles.Add(new ScriptBundle("~/js/angularinfrastructure").Include("~/js/AngularInfrastructure/Shared/Shared.js",
            //    "~/js/AngularInfrastructure/Shared/CalendarHelper.js",
            //    "~/js/AngularInfrastructure/Shared/CalendarType.js",
            //    "~/js/AngularInfrastructure/Shared/Timer.js",
            //    "~/js/AngularInfrastructure/mp3recorder/recorder.js",
            //    "~/js/AngularInfrastructure/Validations/SharedTemplateValidation.js",
            //    "~/js/AngularInfrastructure/Services/APIService",
            //    "~/js/AngularInfrastructure/Services/NotificationService",
            //    "~/js/AngularInfrastructure/Services/RecordHelperService",
            //    "~/js/AngularInfrastructure/Filters/balanceIsPositiveFilter.js",
            //    "~/js/AngularInfrastructure/Filters/filterArrayInArray.js"));
            bundles.Add(new Bundle("~/Areas/Dashboard/js/AngularInfrastructure/angularteacher")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Shared", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/mp3recorder", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Validations", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Filters", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Services", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Controllers/SharedControllers", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Controllers/TeachersControllers", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Directives", "*.js"));
            bundles.Add(new Bundle("~/Areas/Dashboard/js/AngularInfrastructure/angularstudent")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Shared", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/mp3recorder", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Validations", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Filters", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Services", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Controllers/SharedControllers", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Controllers/StudentsControllers", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Directives", "*.js", true));
            bundles.Add(new Bundle("~/Areas/Dashboard/js/AngularInfrastructure/angularcompany")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Shared", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Validations", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Filters", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Services", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Controllers/CompanyControllers", "*.js")
                .Include("~/Areas/Dashboard/js/AngularInfrastructure/Controllers/SharedControllers/myaccountpage.js")
                .Include("~/Areas/Dashboard/js/AngularInfrastructure/Controllers/SharedControllers/myclassescontroller.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Directives", "*.js", true));

            bundles.Add(new Bundle("~/Areas/Admin/js/AngularInfrastructure/admin")
                .IncludeDirectory("~/Areas/Admin/js/AngularInfrastructure/Shared", "*.js")
                .IncludeDirectory("~/Areas/Admin/js/AngularInfrastructure/Validations", "*.js")
                .IncludeDirectory("~/Areas/Admin/js/AngularInfrastructure/Filters", "*.js")
                .IncludeDirectory("~/Areas/Dashboard/js/AngularInfrastructure/Services", "*.js")
                .Include("~/Areas/Admin/js/AngularInfrastructure/controllers/Admin/*.js")
                .Include("~/Areas/Admin/js/AngularInfrastructure/controllers/SharedControllers/*.js")
                .IncludeDirectory("~/Areas/Admin/js/AngularInfrastructure/Directives", "*.js"));

            bundles.Add(new Bundle("~/js/forRegistration").Include("~/js/InfrastructureForNewDesign/ForRegistration.js"));
            bundles.Add(new Bundle("~/js/Shared").Include("~/js/InfrastructureForNewDesign/Shared.js"));
            bundles.Add(new Bundle("~/js/SelectBootstrap").Include("~/js/bootstrap-select.js"));
            bundles.Add(new StyleBundle("~/Css/style12").Include("~/Css/style12.css"));
        }
    }
}
