﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using Microsoft.Owin.Security.OAuth;
using Xencore.Entities;
using Xencore.Entities.IdentityEntities;

namespace Xencore
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            //builder.EntitySet<MyAssingnments>("MyAssingnments");
            //config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
            name: "DefaultApiWithAction",
            routeTemplate: "api/{controller}/{action}"
);
        }
    }
}
