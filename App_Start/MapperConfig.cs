﻿using AutoMapper;
using AutoMapper.Configuration;
using Domain.Entities;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;
using Xencore.Entities;
using Xencore.Entities.IdentityEntities;

namespace Xencore
{
    public static class MapperConfig
    {
        public static void Create(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<CustomUser, UserHelper>();
            cfg.CreateMap<UserHelper, CustomUser>()
                .ForMember(x => x.Ispeak, opt => opt.Ignore())
                .ForMember(x => x.Ilearn, opt => opt.Ignore())
                .ForMember(x=>x.TimeZone,x=>x.MapFrom(f=>f.Timezone))
                .ForMember(x=>x.Email, x=>x.Ignore())
                .ForMember(x => x.Active, x => x.Ignore())
                .ForMember(x => x.FaceTimeAccount, x => x.MapFrom(f => f.Facetime))
                .ForMember(x => x.QQAccount, x => x.MapFrom(f => f.Qq))
                .ForMember(x => x.GoogleAccount, x => x.MapFrom(f => f.Google))
                .ForMember(x => x.Skype, x => x.MapFrom(f => f.Skype));


            cfg.CreateMap<TeacherApplicationHepler,TeacherApplication>();
            cfg.CreateMap<PaymentDetailsHelper, OrderDetails>();
            cfg.CreateMap<PreSignInUser, CustomUser>().ForMember(x=>x.UserName, x=>x.MapFrom(z=>z.Email));
            cfg.CreateMap<TeacherApplicationHepler, CustomUser>()
                .ForMember(x => x.UserName, x => x.MapFrom(z => z.Email))
                .ForMember(x => x.PhoneNumber, x => x.MapFrom(z => z.Telephone))
                .ForMember(x => x.TeacherInfo, x => x.MapFrom(z => new TeacherInfo()
                {
                    Link = z.Link,
                }))
                .ForMember(x => x.TeacherRate,
                    x => x.MapFrom(z => new TeacherRate() {Individual60Rate = 10, Individual45Rate = 8, GroupRate = 5}));
            cfg.CreateMap<TeacherApplicationHepler, TeacherInfo>();
            cfg.CreateMap<AboutMeHelper, TeacherInfo>().ForMember(x=>x.Link, x=>x.Ignore());
        }
    }
}
