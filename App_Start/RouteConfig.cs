﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.OData.Builder;
using System.Web.Mvc;
using System.Web.Mvc.Routing.Constraints;
using System.Web.Routing;
using Xencore.Entities;

namespace Xencore
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var constrains = new
            {
                //culture = new NullableConstraint()
                culture = "[a-z]{2}"
            };
            routes.MapMvcAttributeRoutes();
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "myaccount",
            //    url: "{culture}/my-account",
            //    defaults: new { controller = "NewMyAccount", action = "LoadMain" },
            //    constraints: constrains,
            //    namespaces: new[] { "Xencore.Controllers" }
            //);


            //routes.MapRoute(
            //    name: "myaccount0",
            //    url: "my-account",
            //    defaults: new {culture = "ey",controller = "NewMyAccount", action = "LoadMain"},
            //    namespaces: new[] {"Xencore.Controllers"}
            //);

            routes.MapRoute(
                name: "loginas",
                url: "loginas/{email}/{key}",
                defaults: new { controller = "LoginAs", action = "Index" },
                constraints: new { key = new GuidRouteConstraint() },
                namespaces: new[] { "Xencore.Controllers" }
            );

            routes.MapRoute(
                name: "business",
                url: "{culture}/business",
                defaults: new { controller = "openlang", action = "bussiness" },
                constraints: constrains,
                namespaces: new[] { "Xencore.Controllers" }
            );

            routes.MapRoute(
                name: "business0",
                url: "business",
                defaults: new {culture = "ey",controller = "openlang", action = "bussiness"},
                namespaces: new[] {"Xencore.Controllers"}
            );


            routes.MapRoute(
                name: "reports",
                url: "{culture}/reports",
                defaults: new { controller = "Reports", action = "Index" },
                constraints: constrains,
                namespaces: new[] { "Xencore.Controllers" }
            );

            routes.MapRoute(
                name: "reports0",
                url: "reports",
                defaults: new {culture = "ey",controller = "Reports", action = "Index"},
                namespaces: new[] {"Xencore.Controllers"}
            );

            routes.MapRoute(
                name: "terms",
                url: "{culture}/terms",
                defaults: new { controller = "PrivacyAndTerms", action = "Terms" },
                constraints: constrains,
                namespaces: new[] { "Xencore.Controllers" }
            );

            routes.MapRoute(
                name: "terms0",
                url: "terms",
                defaults: new {culture = "ey", controller = "PrivacyAndTerms", action = "Terms"},
                namespaces: new[] {"Xencore.Controllers"}
            );

            routes.MapRoute(
                name: "privacy",
                url: "{culture}/privacy",
                defaults: new { controller = "PrivacyAndTerms", action = "Privacy" },
                constraints: constrains,
                namespaces: new[] { "Xencore.Controllers" }
            );


            routes.MapRoute(
                name: "privacy0",
                url: "privacy",
                defaults: new {culture = "ey",controller = "PrivacyAndTerms", action = "Privacy"},
                namespaces: new[] {"Xencore.Controllers"}
            );

            routes.MapRoute(
                name: "offices",
                url: "{culture}/offices",
                defaults: new { controller = "Offices", action = "Offices" },
                constraints: constrains,
                namespaces: new[] { "Xencore.Controllers" }
            );

            routes.MapRoute(
                name: "offices0",
                url: "offices",
                defaults: new {culture = "ey", controller = "Offices", action = "Offices"},
                namespaces: new[] {"Xencore.Controllers"}
            );

            routes.MapRoute(
                name: "howitworks",
                url: "{culture}/howitworks",
                defaults: new { controller = "HowItWorks", action = "Index" },
                constraints: constrains,
                namespaces: new[] { "Xencore.Controllers" }
            );

            routes.MapRoute(
                name: "howitworks0",
                url: "howitworks",
                defaults: new {culture = "ey",controller = "HowItWorks", action = "Index"},
                namespaces: new[] {"Xencore.Controllers"}
            );

            routes.MapRoute(
                name: "for-auth-teachers",
                url: "{culture}/for-auth-teachers",
                defaults: new { controller = "Forteachers", action = "PostTeacherAuthorizeAuthor" },
                constraints: constrains,
                namespaces: new[] { "Xencore.Controllers" }
            );

            routes.MapRoute(
                name: "for-teachers",
                url: "{culture}/for-teachers",
                defaults: new { controller = "Forteachers", action = "Teachers" },
                constraints: constrains,
                namespaces: new[] { "Xencore.Controllers" }
            );

            routes.MapRoute(
                name: "for-teachers0",
                url: "for-teachers",
                defaults: new {culture = "ey", controller = "Forteachers", action = "Teachers"},
                namespaces: new[] {"Xencore.Controllers"}
            );

            routes.MapRoute(
                name: "faq",
                url: "{culture}/faq",
                defaults: new { controller = "Faq", action = "FAQ" },
                constraints: constrains,
                namespaces: new[] { "Xencore.Controllers" }
            );

            routes.MapRoute(
                name: "faq0",
                url: "faq",
                defaults: new {culture = "ey",controller = "Faq", action = "FAQ"},
                namespaces: new[] {"Xencore.Controllers"}
            );


            routes.MapRoute(
                name: "courses",
                url: "{culture}/courses",
                defaults: new { controller = "Courses", action = "Courses" },
                constraints: constrains,
                namespaces: new[] { "Xencore.Controllers" }
            );


            routes.MapRoute(
                name: "courses0",
                url: "courses",
                defaults: new {culture = "ey",controller = "Courses", action = "Courses"},
                namespaces: new[] {"Xencore.Controllers"}
            );

            routes.MapRoute(
                name: "contact-us",
                url: "{culture}/contact-us",
                defaults: new { controller = "ContactUs", action = "ContactUs" },
                constraints: constrains,
                namespaces: new[] { "Xencore.Controllers" }
            );

            routes.MapRoute(
                name: "contact-us0",
                url: "contact-us",
                defaults: new {culture="ey", controller = "ContactUs", action = "ContactUs"},
                namespaces: new[] {"Xencore.Controllers"}
            );


            routes.MapRoute(
                name: "details",
                url: "{culture}/teacher-details/{id}",
                defaults: new { controller = "ListOfTeachers", action = "GetTeacherDetails" },
                constraints: constrains,
                namespaces: new[] { "Xencore.Controllers" }
            );


            routes.MapRoute(
                name: "details0",
                url: "teacher-details/{id}",
                defaults: new { culture = "ey",controller = "ListOfTeachers", action = "GetTeacherDetails"},
                namespaces: new[] {"Xencore.Controllers"}
            );

            routes.MapRoute(
                name: "find-teachers",
                url: "{culture}/find-teachers",
                defaults: new { controller = "ListOfTeachers", action = "ListOfTeachers" },
                constraints: constrains,
                namespaces: new[] { "Xencore.Controllers" }
            );


            routes.MapRoute(
                name: "find-teachers0",
                url: "find-teachers",
                defaults: new { culture = "ey", controller = "ListOfTeachers", action = "ListOfTeachers" },
                namespaces: new[] { "Xencore.Controllers" }
            );

            routes.MapRoute(
                name: "chat",
                url: "{culture}/chat",
                defaults: new { controller = "Chat", action = "Chat" },
                constraints: constrains,
                namespaces: new[] { "Xencore.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{culture}/{controller}/{action}/{user}",
                defaults: new { controller = "openlang", action = "newlandingpage", user = UrlParameter.Optional },
                constraints: constrains,
                namespaces: new[] { "Xencore.Controllers" }
            );
            routes.MapRoute(
                name: "Default1",
                url: "{controller}/{action}/{user}",
                defaults: new { controller = "openlang", action = "newlandingpage", user = UrlParameter.Optional },
                namespaces: new[] { "Xencore.Controllers" }
            );
        }
    }
}
