﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;
using Domain.Context;
using Domain.Entities.IdentityEntities;
using Domain.Providers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;

using Xencore.Entities.IdentityEntities.Entities;
using Xencore.HubsOfSignalR;
using Xencore.Initializer;

[assembly: OwinStartup(typeof(Xencore.Entities.IdentityEntities.Startup))]

namespace Xencore.Entities.IdentityEntities
{
    public class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext(MyDbContext.Create);
            app.CreatePerOwinContext<CustomUserManager>(CustomUserManager.Create);
            app.CreatePerOwinContext(() => DependencyResolver.Current.GetService<CustomUserManager>());
            app.CreatePerOwinContext<CustomSignIn>(CustomSignIn.Create);
            app.CreatePerOwinContext<CustomRoleManager>(CustomRoleManager.Create);
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/openlang/NewLandingPage"),
                Provider = new CookieAuthenticationProvider
                {
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<CustomUserManager, CustomUser, Guid>(
              TimeSpan.FromDays(14),
             (manager, user) => user.GenerateUserIdentityAsync(manager)
             , claim => new Guid())
                }
            });
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);
            //var googleOAuth2AuthenticationOptions = new GoogleOAuth2AuthenticationOptions
            //{
            //    ClientId = "401093889198-5u5vh75501jdtqcduv54ui48vjbu2vme.apps.googleusercontent.com",
            //    ClientSecret = "EAizVJUi-2NBk-dkN_pZ3v-3",
            //    Provider = new GoogleOAuth2AuthenticationProvider()
            //    {
            //        OnAuthenticated = context =>
            //        {
            //            context.Identity.AddClaim(new Claim("urn:google:accesstoken", context.AccessToken, "XmlSchemaString", "Google"));
            //            return Task.FromResult(0);
            //        }
            //    }
            //};
            //googleOAuth2AuthenticationOptions.Scope.Add("profile");
            //googleOAuth2AuthenticationOptions.Scope.Add("email");

            //app.UseGoogleAuthentication(googleOAuth2AuthenticationOptions);
            //var options = new FacebookAuthenticationOptions
            //{
            //    AppId = "1538355979822340",
            //    AppSecret = "d6968dce62668a2bf125aab9142490af",
            //    BackchannelHttpHandler = new FacebookBackChannelHandler(),
            //    UserInformationEndpoint = "https://graph.facebook.com/v2.4/me?fields=id,picture,name,email,first_name,last_name,location",
            //    Provider = new FacebookAuthenticationProvider()
            //    {
            //        OnAuthenticated = (context) =>
            //        {
            //            context.Identity.AddClaim(new Claim("picture", context.User.GetValue("picture").ToString()));
            //            context.Identity.AddClaim(new Claim("urn:facebook:access_token", context.AccessToken, "XmlSchemaString", "Facebook"));
            //            context.Identity.AddClaim(new Claim("urn:facebook:email", context.Email, "XmlSchemaString", "Facebook"));
            //            return Task.FromResult(0);
            //        }
            //    }
            //};
            //options.Scope.Add("email");
            //app.UseFacebookAuthentication(options);

            PublicClientId = "OpenLanguages";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString("/Openlang/Index"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                AllowInsecureHttp = true
            };
            app.UseOAuthBearerTokens(OAuthOptions);
            var idProvider = new CustomUserIdProvider();
            GlobalHost.DependencyResolver.Register(typeof(IUserIdProvider), () => idProvider);
            app.MapSignalR();
        }
    }
}
