﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entities;
using Xencore.Entities;
using Xencore.Entities.IdentityEntities;

namespace Xencore.Models
{
    public class TeacherClassesViewModel
    {
        public int Id { get; set; }
        public string Language { get; set; }
        public string Level { get; set; }
        public DateTime StartDate { get; set; }
        public string Time { get; set; }
        public byte Duration { get; set; }
        public bool IsPrivate { get; set; }
        public string SubjectOfTheLesson { get; set; }
        public decimal MoneyEarned { get; set; }
        public string Comments { get; set; }
        public string MaterialUsed { get; set; }
        public string TeacherTeach { get; set; }
        public TeacherRate Rate { get; set; }
        public IEnumerable<string> StudentsNames { get; set; }
    }
}