﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Xencore.Models
{
    public class DiscountViewModel
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(32)]
        [MinLength(5)]
        public string Number { get; set; }
        [Required]
        public bool IsPercent { get; set; }
        [Required]
        [Range(0, float.MaxValue, ErrorMessage = "Please enter valid float Number")]
        public string Value { get; set; }

        public string Type { get; set; }

        public bool IsActive { get; set; } = true;
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateStart { get; set; } = DateTime.Now.AddDays(-1);
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateEnd { get; set; } = DateTime.Now.AddDays(5);

    }
}