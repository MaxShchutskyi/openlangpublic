﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Domain.Context;
namespace Xencore.Models
{
    public class TeacherClassesUi
    {
        public int Id { get; set; }
        [Required]
        public string Language { get; set; }
        [Required]
        public Guid TeacherId { get; set; }
        [Required]
        public string Level { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public string Time { get; set; }
        [Required]
        public byte Duration { get; set; }
        [Required]
        public string MaterialsUsed { get; set; }
        public string Comments { get; set; }
        [Required]
        public string SubjectOfTheLesson { get; set; }
        public decimal MoneyEarned { get; set; }
        public virtual IEnumerable<Guid> Students { get; set; }
        public bool IsPrivate => Students?.Count() <= 1;

        public void CalculateEarnings()
        {
            using (var context = new MyDbContext())
            {
                decimal rate = 0;
                decimal gr = 0;
                var res = context.TeacherRates.FirstOrDefault(x => x.UserId == TeacherId);
                rate = Duration == 45 ? res.Individual45Rate : res.Individual60Rate;
                if(!IsPrivate)
                    gr = res.GroupRate * Students.Count();
                MoneyEarned = gr + rate;
            }
        }
    }
}