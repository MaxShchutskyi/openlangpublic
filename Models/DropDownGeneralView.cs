﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyAccount.Models
{
    public class DropDownGeneralView
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}