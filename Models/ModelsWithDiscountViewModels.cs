﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entities;
using Xencore.Entities;

namespace Xencore.Models
{
    public class ModelsWithDiscountViewModels
    {
        public DiscountViewModel Model { get; set; } = new DiscountViewModel();
        public IEnumerable<Discount> Discounts { get; set; }
    }
}