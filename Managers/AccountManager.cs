﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Domain.Context;
using Domain.Entities.IdentityEntities;
using Domain.Repositories.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using Ninject.Activation;
using NLog;
using WebGrease.Css.Extensions;

using Xencore.Entities.IdentityEntities;
using Xencore.Managers.Abstract;

namespace Xencore.Managers
{
    public class AccountManager:IAccountManager
    {
        private CustomUserManager Manager { get; set; }
        private CustomSignIn SignIn { get; set; }

        public AccountManager(CustomUserManager manager)
        {
            Manager = manager;
        }

        public async Task<bool> CheckExistUserAsync(string email)
        {
            var user = await Manager.FindByNameAsync(email);
            return user != null;
        }

        public async Task<bool> CheckExistUserAsync(string email, string password)
        {
                if (email == null || password == null)
                    return false;
                var user = await Manager.FindAsync(email, password);
                return user != null;
        }

        public async Task<bool> CreateNewAccountAsync(CustomUser user, string password)
        {
            var s = await Manager.CreateAsync(user, password);
            if (!s.Succeeded) return false;
            //var claims = await user.GenerateUserIdentityAsync(Manager);
            //HttpContext.Current.GetOwinContext().Authentication.SignIn(claims);
            return true;
        }

        public async Task<string> ResetPasswordBySmsAsync(CustomUser user, string telephone)
        {
            if (user == null || telephone == null)
            {
                LogManager.GetCurrentClassLogger().Error("Invalid Paramater in ResetPasswordBySmsAsync");
                return "Invalid Paramater";
            }
            var newPass = new Random().Next(0, 100000).ToString("000000");
            try
            {
                var token = await Manager.GeneratePasswordResetTokenAsync(user.Id);
                var rst = await Manager.ResetPasswordAsync(user.Id, token, newPass);
                var rst2 = await Manager.UpdateAsync(user);
                if(!rst.Succeeded || !rst2.Succeeded)
                    throw new Exception("Invalid Generated Token");
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error(e, "Error in password update");
                return e.Message;
            }

            await Manager.SendSmsAsync(user.Id, $"Your new password is - {newPass}");
            return "Success";
        }

        public async Task<string> ResetPasswordByEmailAsync(string email)
        {
            try
            {
                var user = await Manager.FindByNameAsync(email);
                var token = await Manager.GeneratePasswordResetTokenAsync(user.Id);
                var newPass = new Random().Next(0, 100000).ToString("000000");
                var update = await Manager.ResetPasswordAsync(user.Id, token, newPass);
                await Manager.UpdateAsync(user);
                if (update.Succeeded)
                {
                    await Manager.SendEmailAsync(user.Id, "Forgot Password", $"Your new password is - {newPass}");
                    HttpContext.Current.Response.StatusCode = 200;
                    return "Success";
                }
                HttpContext.Current.Response.StatusCode = 200;
                return update.Errors.First();
            }
            catch (Exception e)
            {
                HttpContext.Current.Response.StatusCode = 200;
                return e.Message;
            }

        }
    }
}
