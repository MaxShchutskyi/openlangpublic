﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Domain.Entities;
using Xencore.Entities;
using Xencore.Managers.Converter;
using Xencore.Managers.Discounts;

namespace Xencore.Managers
{
    public class DiscountManager
    {
        private Discount Discount { get; set; }
        private ICalculateDiscount CalcDiscount { get; set; }
        private string Amount { get; }

        public DiscountManager(Discount discount, string amount, string currency = "EUR")
        {
            Discount = discount;
            Amount = amount;
            switch (Discount.IsPercent.ToString())
            {
                case "True" : CalcDiscount = new PercentDiscount(Byte.Parse(discount.Value)); break;
                case "False": CalcDiscount = new SumDiscount(discount.Value) {Currency = currency };break;
            }
        }

        public Task<decimal> GetDiscount()
        {
            return CalcDiscount.Calculate(Amount);
        }
    }
}