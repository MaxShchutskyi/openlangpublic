﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xencore.Managers.Converter
{
    public interface IConverter
    {
        string From { get; set; }
        string To { get; set; }
        Task<decimal> Convert(string amount);
    }
}
