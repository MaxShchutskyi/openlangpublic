﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Xencore.Managers.Converter
{
    public class FixerIOConverter:IConverter
    {
        public string From { get; set; } = "EUR";
        public string To { get; set; } = "USD";
        public async Task<decimal> Convert(string amount)
        {
            var am = decimal.Parse(amount, CultureInfo.InvariantCulture);
            var result = "";
            using (var client = new HttpClient())
            {
                var url = $"http://data.fixer.io/api/latest?access_key=891f5425ed43e6f9423ac5821612ad54&base={From}&symbols={To}";
                result = await client.GetStringAsync(url);
            }
            var obj = JObject.Parse(result);
            var value = obj.Last.First.First.First.Value<decimal>();
            return value * am;
        }
    }
}