﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Domain.Entities.IdentityEntities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using Ninject.Activation;
using NLog;
using Xencore.Entities.IdentityEntities;
using Xencore.Entities.IdentityEntities.Entities;
using Xencore.Helpers;

namespace Xencore.Managers
{
    public class ExternalManagerAccount
    {
        private CustomSignIn SignIn { get; set; }
        private ExternalLoginInfo LoginInfo { get; set; }

        public ExternalManagerAccount(CustomSignIn signin, ExternalLoginInfo loginInfo)
        {
            LoginInfo = loginInfo;
            SignIn = signin;
        }

        private string GetJsonResult()
        {
            using (var webClient = new System.Net.WebClient())
            {
                try
                {
                    var accessToken =
                        LoginInfo.ExternalIdentity.Claims.Where(c => c.Type.Equals("urn:google:accesstoken"))
                            .Select(c => c.Value)
                            .FirstOrDefault();
                    var apiRequestUri = new Uri("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + accessToken);
                    return webClient.DownloadString(apiRequestUri);
                }
                catch (Exception e)
                {
                    LogManager.GetCurrentClassLogger().Error(e, "Error get response from GoogleAPI");
                    return "";
                }
            }
        }

        public async Task<CustomUser> SignInGoogleAsync(HttpContextBase context)
        {
            return await
                SignIn.UserManager.FindAsync(LoginInfo.Login);
        }

        public async Task<CustomUser> CreateUserGoogle(string selectedLang)
        {
            var tri = await ChackExistsEmail(LoginInfo.Email);
            if (tri != null)
                return await CreateLogins(tri);
            var jsonRes = GetJsonResult();
            dynamic results = JsonConvert.DeserializeObject(jsonRes);
            var userPicture = results.picture;
            string selLangg = null;
            if (selectedLang != null)
            {
                var sp = selectedLang.Split('|');
                selLangg = sp[1] + "_" + sp[0] + "|";
            }
            var u = new CustomUser()
            {
                Name = LoginInfo.DefaultUserName,
                UserName = LoginInfo.Email,
                Email = LoginInfo.Email,
                ProfilePicture = userPicture,
                SelectedLearnLang = selectedLang,
                Ilearn = selLangg
            };
            return await CreateUser(u);
        }

        private async Task<CustomUser> CreateLogins(CustomUser user)
        {
            try
            {
                await SignIn.UserManager.AddLoginAsync(user.Id, LoginInfo.Login);
                await SignIn.ExternalSignInAsync(LoginInfo, false);
                return user;
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error(e,"Error write logins to database" + user.Name +" - "+ user.Email);
                return user;
            }
        }

        private async Task<CustomUser> ChackExistsEmail(string email)
        {
            return await SignIn.UserManager.FindByEmailAsync(email);
        }

        public async Task<CustomUser> SignInFacebookAsync(HttpContextBase context)
        {
            return await
                SignIn.UserManager.FindAsync(LoginInfo.Login);
        }

        public async Task<CustomUser> CreateUserFacebook(HttpContextBase context)
        {
            var externalIdentity =
                await
                    context.GetOwinContext()
                        .Authentication.GetExternalIdentityAsync(DefaultAuthenticationTypes.ExternalCookie);
            var firstOrDefault = externalIdentity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email);
            try
            {
                if (firstOrDefault == null)
                    throw new Exception();

                    var email = firstOrDefault.Value;
                    var tri = await ChackExistsEmail(email);
                    if (tri != null)
                        return await CreateLogins(tri);
                    var pictureClaim = externalIdentity.Claims.FirstOrDefault(c => c.Type.Equals("picture"));
                    var picture = pictureClaim.Value;
                    var obj = JsonConvert.DeserializeObject<dynamic>(picture);
                    var pictureUrl = obj.data.url;
                    var user2 = new CustomUser()
                    {
                        UserName = email,
                        ProfilePicture = pictureUrl,
                        Name = externalIdentity.Name,
                        Email = email
                    };
                    return await CreateUser(user2);
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error(e, "Error get Email");
                return null;
            }
        }

        private async Task<CustomUser> CreateUser(CustomUser user)
        {
            try
            {
                var res2 = await SignIn.UserManager.CreateAsync(user);
                if (!res2.Succeeded) throw new Exception();
                await SignIn.SignInAsync(user, false, false);
                await SignIn.UserManager.AddLoginAsync(
                    user.Id, LoginInfo.Login);
                //await new MailSenderForNewRegistration("xencoredevelop.azurewebsites.net", user, "admin@openlanguages.com").Send();
                return user;
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error(e, "Error write a user to database " + user.UserName + " - " + user.Email);
                return null;
            }

        }

    }
}
