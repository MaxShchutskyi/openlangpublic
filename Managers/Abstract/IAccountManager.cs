﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities.IdentityEntities;

namespace Xencore.Managers.Abstract
{
    public interface IAccountManager
    {
        Task<bool> CheckExistUserAsync(string email);
        Task<bool> CreateNewAccountAsync(CustomUser user, string password);
        Task<string> ResetPasswordBySmsAsync(CustomUser user, string telephone);
        Task<string> ResetPasswordByEmailAsync(string email);
    }
}
