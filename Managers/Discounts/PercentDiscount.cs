﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Xencore.Managers.Discounts
{
    public class PercentDiscount:ICalculateDiscount
    {
        public byte Percent { get; set; }

        public PercentDiscount(byte percent)
        {
            this.Percent = percent;
        }
        public async Task<decimal> Calculate(string amount)
        {
            //amount = amount.Replace('.', ',');
            var am = decimal.Parse(amount, CultureInfo.InvariantCulture);
            return am + (am/100*Percent);
        }
    }
}