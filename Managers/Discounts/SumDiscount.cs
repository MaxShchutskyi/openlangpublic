﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Xencore.Managers.Converter;

namespace Xencore.Managers.Discounts
{
    public class SumDiscount:ICalculateDiscount
    {
        private IConverter Converter { get; set; } = new FixerIOConverter();
        public string Currency { get; set; } = "EUR";
        public string Value { get; set; }

        public SumDiscount(string value)
        {
            this.Value = value;
        }
        public async Task<decimal> Calculate(string amount)
        {
            //amount = amount.Replace('.', ',');
            if (Currency != "EUR")
            {
                Converter.To = Currency;
                var res = await Converter.Convert(Value);
                return decimal.Parse(amount, CultureInfo.InvariantCulture) + res;
            }
            return decimal.Parse(amount, CultureInfo.InvariantCulture) + decimal.Parse(Value, CultureInfo.InvariantCulture);
        }
    }
}