﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xencore.Managers.Discounts
{
    public interface ICalculateDiscount
    {
        Task<decimal> Calculate(string amount);
    }
}
