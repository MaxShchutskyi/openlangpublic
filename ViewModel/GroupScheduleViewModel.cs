﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xencore.ViewModel
{
    public class GroupScheduleViewModel
    {
        public Guid TeacherId { get; set; }
        public int ScheduleId { get; set; }
        public string Subject { get; set; }
        public string Level { get; set; }
        public string Language { get; set; }
        public string Name { get; set; }
        public short Count { get; set; }
        public string ProfilePicture { get; set; }
        public DateTime StartDateTime { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public bool IsConfirmed { get; set; }
        public string Img { get; set; }
        public string Sbj { get; set; }
        public string GetImage => Img ?? "/Images/lesson-plan.jpg";
    }
}