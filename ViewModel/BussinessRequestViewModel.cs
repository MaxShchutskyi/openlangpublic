﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Xencore.ViewModel
{
    public class BussinessRequestViewModel
    {
        public int Id { get; set; }
        [Required, MaxLength(20)]
        public string Name { get; set; }
        [Required, MaxLength(20)]
        public string Company { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }
        [Required, DataType(DataType.PhoneNumber)]
        
        public string Phone { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string TimeZone { get; set; }
        [Required]
        public string TargetLanguage { get; set; }
        [Required]
        public string Level { get; set; }

        [Required]
        public string EmploeeWithTranungs { get; set; } = "Only Me";

        public IEnumerable<string> Languages { get; set; }
        [Required]
        public string Text { get; set; }


    }
}