﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CurrencyConverter;
using Domain.Entities.HelpersEntity;
using Newtonsoft.Json;
using Xencore.Managers.TablesOfPrices;
using Xencore.Managers.TablesOfPrices.Abstract;
using Xencore.Managers.TablesOfPrices.Entities;

namespace Xencore.ViewModel
{
    public class CurrentUser
    {
        public Guid Id { get; set; }
        public string Currency { get; set; }
        public string Location { get; set; }
        public string Timezone { get; set; }
        public string ProfilePicture { get; set; }
        public string Email { get; set; }
        public string CountryCode { get; set; }
        public string Name { get; set; }

        public PricesPerOneLesson Prices { get; set; }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public void InitializePrices(string country)
        {
            var proceperlesson = PricesPerOneLesson.SetPricesPerOneLesson(CountryAnalizer.Analize(country), 5);
            Prices = proceperlesson.ConvertToPrice(Currency.Split(':')[0]);
            //Prices = TableMaanger.GetFullPriceList(CurrencyConverterFactory.Create<string>(), "EUR", Currency, CountryAnalizer.Analize(Location.Split(':')[0])).ToList();
        }
    }
}