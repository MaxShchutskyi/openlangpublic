﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplicationPostFinanceLatest.ViewModel
{
    public class AttendeeDDModel
    {
        [Display(Name = "Class Id")]
        public Guid AttendeeId { get; set; }
        public IEnumerable<SelectListItem> Classes { get; set; } 
    }
}