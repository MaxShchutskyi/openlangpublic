﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplicationPostFinanceLatest.ViewModel
{
    public class ClassViewModel
    {
        [Display(Name = "Class Id")]
        public int ClassId { get; set; }
        public IEnumerable<SelectListItem> Classes { get; set; } 
    }
}