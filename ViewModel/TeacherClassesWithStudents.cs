﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entities.IdentityEntities;
using Xencore.Entities;
using Xencore.Entities.IdentityEntities;
using Xencore.Models;

namespace Xencore.ViewModel
{
    public class TeacherClassesWithStudents
    {
        public IEnumerable<TeacherClassesViewModel> TeacherClassesViewModels { get; set; }
        public IEnumerable<CustomUser> StudentsOfTheTeacher { get; set; }
    }
}