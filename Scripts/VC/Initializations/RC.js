﻿function LoadRC() {
    $('.pad_confirm').hide();
    $('.great').hide();

    if (!$('#periodRC').is(":visible")) {
        SearchByMonthRC($("#monthRC").slider("value") + 1, years[$("#yearRC").slider("value")]);
        $('.right_content').removeClass("blue_style");
        $('table th:nth-last-child(2) .infos img').attr('src', '/Images/info_green.png');
    } else {
        SearchByDateTimeRC();
        $('.right_content').addClass("blue_style");
        $('table th:nth-last-child(2) .infos img').attr('src', '/Images/info_blue.png');
    }
    SearchByDateTimeRC();
    $.ajax({
        url: 'Home/GetReservedNumber',
        dataType: 'json',
        type: 'POST',
        success: function (data) {
            $('#reservationNumber').text(data);
            $('.pad_load').fadeOut("medium");
        },
        error: function () {
            //alert('error');
            $('.pad_load').fadeOut("medium");
        }
    });
    $('#nextReservation').text("");
    $.ajax({
        url: 'Home/GetNextReservedClass',
        dataType: 'json',
        type: 'POST',
        success: function (data) {
            var fullDate = new Date(parseInt(data.replace(/[^0-9]/g, '')));
            var twoDigitMonth = fullDate.getMonth() + 1 + "";
            if (twoDigitMonth.length == 1) twoDigitMonth = "0" + twoDigitMonth;
            var twoDigitDate = fullDate.getDate() + "";
            if (twoDigitDate.length == 1) twoDigitDate = "0" + twoDigitDate;
            var twoDigitHour = fullDate.getHours() + "";
            if (twoDigitHour.length == 1) twoDigitHour = "0" + twoDigitHour;
            var twoDigitMin = fullDate.getMinutes() + "";
            if (twoDigitMin.length == 1) twoDigitMin = "0" + twoDigitMin;
            var currentDate = twoDigitDate + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
            var currentTime = twoDigitHour + ":" + twoDigitMin;

            $('#nextReservation').text(currentDate + " " + currentTime);
            $('.pad_load').fadeOut("medium");
        },
        error: function () {
            //alert('error');
            $('.pad_load').fadeOut("medium");
        }
    });
}