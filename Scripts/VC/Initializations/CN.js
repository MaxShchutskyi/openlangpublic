﻿function LoadCN() {
    SearchByDateTimeCN();

    $('#generalDdCN option').remove();
    $('#specificDdCN option').remove();

    //CN
    $.ajax({
        url: 'Home/GetGeneralTopicsCN',
        dataType: 'json',
        type: 'POST',
        success: function (data) {
            var generalDdCN = $("#generalDdCN");
            generalDdCN.append($("<option />").val(0).text("General topics"));
            $.each(data, function (i) {
                generalDdCN.append($("<option />").val(data[i].ClassID).text(data[i].Title));
            });
            $('#generalDdCN').selectmenu();
        },
        error: function () {
            //alert('error');
            $('.pad_load').fadeOut("medium");
        }
    });
    $.ajax({
        url: 'Home/GetSpecificTopicsCN',
        dataType: 'json',
        type: 'POST',
        success: function (data) {
            var specificDdCN = $("#specificDdCN");
            specificDdCN.append($("<option />").val(0).text("Specific topics"));
            $.each(data, function (i) {
                specificDdCN.append($("<option />").val(data[i].ClassID).text(data[i].SpecificTopic));
            });
            $('#specificDdCN').selectmenu();
        },
        error: function () {
            //alert('error');
            $('.pad_load').fadeOut("medium");
        }
    });
}