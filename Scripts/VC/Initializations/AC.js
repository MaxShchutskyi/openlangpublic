﻿function LoadAC() {
    //if (!$('#periodAC').is(":visible")) {
    //    SearchByMonth($("#month").slider("value") + 1, years[$("#year").slider("value")]);
    //    $('.right_content').removeClass("blue_style");
    //    $('table th:nth-last-child(2) .infos img').attr('src', '/Images/info_green.png');
    //} else {
    //    SearchByDateTime();
    //    $('.right_content').addClass("blue_style");
    //    $('table th:nth-last-child(2) .infos img').attr('src', '/Images/info_blue.png');
    //}
    ShowAllClasses($("#month").slider("value") + 1, years[$("#year").slider("value")]);
    

    $('#generalDd option').remove();
    $('#specificDd option').remove();
    
    //AC
    $.ajax({
        url: 'Home/GetGeneralTopics',
        dataType: 'json',
        type: 'POST',
        success: function (data) {
            var generalDd = $("#generalDd");
            generalDd.append($("<option />").val(0).text("General topics"));
            $.each(data, function (i) {
                generalDd.append($("<option />").val(data[i].ClassID).text(data[i].Title));
            });
            $('#generalDd').selectmenu();
        },
        error: function () {
            $('.pad_load').fadeOut("medium");
        }
    });
    $.ajax({
        url: 'Home/GetSpecificTopics',
        dataType: 'json',
        type: 'POST',
        success: function (data) {
            var specificDd = $("#specificDd");
            specificDd.append($("<option />").val(0).text("Specific topics"));
            $.each(data, function (i) {
                specificDd.append($("<option />").val(data[i].ClassID).text(data[i].SpecificTopic));
            });
            $('#specificDd').selectmenu();
        },
        error: function () {
            $('.pad_load').fadeOut("medium");
        }
    });
}