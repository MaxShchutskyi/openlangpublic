﻿var AjaxManager = function(){

}
AjaxManager.prototype.GetPage = function (page) {
    $.ajax({
        method: 'get',
        async: true,
        url: location.origin + '/api/getdata/' + page,
        success: function(result) {
            $("#content").text(result.Content);
            $(".main_for_more_same_img").attr('src', result.Img);
            $("title").text(result.PageTitle);
            $("#h1Header").text(result.PageH1Head);
        },
        error: function(err) {
            console.log(err.responseText);
        }
    });
}