﻿function LoadMC() {
     $('.pad_load').fadeIn("medium");
    $.ajax({
        url: '/Home/GetMyCourse',
        dataType: 'json',
        type: 'POST',
        data: { title: $("#search").val() },
        success: function (data) {
            FillHeader(data);
            FillTableMC(data);
            FillTableNext(data);
            $('.pad_load').fadeOut("medium");
        },
        error: function () {
            alert('error');
            $('.pad_load').fadeOut("medium");
        }
    });
}

function FillHeader(data) {
    //fill title table
    $('#helloFirstName').text("Hello " + data.FirstName);
    $('#attendee').text(data.FirstName + " " + data.LastName);
    $('#lessonsTaken').text(data.LessonsTaken + " | " + ConvertHours(data.LessonsTakenHours));
    $('#outstandingLesson').text(data.OutstandingLesson + " | " + ConvertHours(data.OutstandingLessonHours));
    $('#subscriptionStarts').text(ConvertDate(data.SubscriprionStarts));
    $('#subscriptionExp').text(ConvertDate(data.SubscriprionExpires));
}

function FillTableMC(data) {
    //fill passes class table
    if (data.PassedClasses == null) {
        $('#dataTableMCPassed tr:gt(0)').after().remove();
        return;
    }
    if (data.PassedClasses.length == 0) {
        $('#dataTableMCPassed tr:gt(0)').after().remove();
        return;
    }

    $('#dataTableMCPassed tr:gt(0)').after().remove();
    var r = new Array(), j = -1;
    var i = 1;
    $.each(data.PassedClasses, function (k, v) {
        r[++j] = '<tr><td>';
        r[++j] = i++ + ".";
        r[++j] = '</td><td><span class="link_text">';
        r[++j] = ConvertDateMainTable(data.PassedClasses[k].StartTime);
        r[++j] = '</span><span class="title_word">';
        r[++j] = GetDay(data.PassedClasses[k].StartTime);
        r[++j] = '</span></td><td>';
        r[++j] = ConvertTime(data.PassedClasses[k].StartTime);
        r[++j] = '</td><td>';
        r[++j] = data.PassedClasses[k].Title;
        r[++j] = '</td><td style="width: 13%">';
        r[++j] = GetHours(data.PassedClasses[k].Duration);
        r[++j] = '</td><td>';
        r[++j] = GetTime(data.PassedClasses[k].Duration);
        r[++j] = '</td></tr>';
    });
    $('#dataTableMCPassed tr:last').after(r.join(''));
}

function FillTableNext(data) {
    //fill next class table
    $('#dataTableMCNext tr:gt(0)').after().remove();
    var array = new Array(), n = -1;
    if (data.NextClasses == null) {
        $('#dataTableMCNext tr:gt(0)').after().remove();
        array[++n] = '<tr><td><br /></td><td></td><td></td><td></td><td style="width: 13%"></td><td></td></tr>';
        $('#dataTableMCNext tr:last').after(array.join(''));
        return;
    }
    if (data.NextClasses.length == 0) {
        $('#dataTableMCNext tr:gt(0)').after().remove();
        array[++n] = '<tr><td><br /></td><td></td><td></td><td></td><td style="width: 13%"></td><td></td></tr>';
        $('#dataTableMCNext tr:last').after(array.join(''));
        return;
    }

    var q = 1;
    $.each(data.NextClasses, function (k, v) {
        array[++n] = '<tr><td>';
        array[++n] = q++ + ".";
        array[++n] = '</td><td><span class="link_text">';
        array[++n] = ConvertDateMainTable(data.NextClasses[k].StartTime);
        array[++n] = '</span><span class="title_word">';
        array[++n] = GetDay(data.NextClasses[k].StartTime);
        array[++n] = '</span></td><td>';
        array[++n] = ConvertTime(data.NextClasses[k].StartTime);
        array[++n] = '</td><td>';
        array[++n] = data.NextClasses[k].Title;
        array[++n] = '</td><td style="width: 13%">';
        array[++n] = GetHours(data.NextClasses[k].Duration);
        array[++n] = '</td><td>';
        array[++n] = GetTime(data.NextClasses[k].Duration);
        array[++n] = '</td></tr>';
    });
    array[++n] = '<tr><td><br /></td><td></td><td></td><td></td><td style="width: 13%"></td><td></td></tr>';
    $('#dataTableMCNext tr:last').after(array.join(''));
}

function ConvertHours(data) {
    if (data == null)
        return "-H:-M";
    var hours = Math.floor(data / 60) + "";
    if (hours.length == 1) hours = "0" + hours;
    var minutes = data % 60 + "";
    if (minutes.length == 1) minutes = "0" + minutes;
    return  hours + "H:" + minutes + "M";
}

function ConvertDate(data) {
    var fullDate = new Date(parseInt(data.replace(/[^0-9]/g, '')));
    return fullMonths[fullDate.getMonth()] + " " + fullDate.getDate() + ", " + fullDate.getFullYear();
}

function ConvertDateMainTable(data) {
    var fullDate = new Date(parseInt(data.replace(/[^0-9]/g, '')));
    return fullDate.getDate() + " " + shortMonths[fullDate.getMonth()] + " " + fullDate.getFullYear();
}

function GetDay(data) {
    var fullDate = new Date(parseInt(data.replace(/[^0-9]/g, '')));
    return dayWeek[fullDate.getDay()];
}

function ConvertTime(data) {
    if (data == null)
        return "-";
    var fullDate = new Date(parseInt(data.replace(/[^0-9]/g, '')));

    var twoDigitHour = fullDate.getHours() + "";
    if (twoDigitHour.length == 1) twoDigitHour = "0" + twoDigitHour;
    var twoDigitMin = fullDate.getMinutes() + "";
    if (twoDigitMin.length == 1) twoDigitMin = "0" + twoDigitMin;
    return  twoDigitHour + ":" + twoDigitMin;
}

function GetHours(data) {
    var hours = Math.floor(data / 60) + "";
    return hours;
}

function GetTime(data) {
    var minutes = data % 60 + "";
    if (minutes.length == 1) minutes = "0" + minutes;
    return  minutes;
}

var fullMonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var shortMonths = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
var dayWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];