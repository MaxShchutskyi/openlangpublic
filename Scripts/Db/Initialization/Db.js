﻿function LoadDb() {
     $('.pad_load').fadeIn("medium");
    $.ajax({
        url: '/Home/GetDashboard',
        dataType: 'json',
        type: 'POST',
        data: { title: $("#search").val() },
        success: function (data) {
            FillDashboard(data);
            $('.pad_load').fadeOut("medium");
        },
        error: function () {
            alert('error');
            $('.pad_load').fadeOut("medium");
        }
    });
}

function FillDashboard(data) {
    var currentDate = new Date();
    $('.month_year').text(fullMonths[currentDate.getMonth()] + " " + currentDate.getFullYear());
    $('#expDate').text("EXPIRY DATE: " + ConvertDateSlash(data.SubscriprionExpires));
    $('#nextLessonDate').text(ConvertDateSlash(data.NextClassDateTime));
    $('#nextLessonTime').text(ConvertTime(data.NextClassDateTime));
    if (data.Attendee.Languages != null) {
        $('#lang').text(" " + data.Attendee.Languages.Name.split('|')[0] + "  ");
        $('#langFlag').attr('src', data.Attendee.Languages.ImageSrc);
    }
    if (data.Attendee.Levels != null) {
        $('#lvl').text(" " + data.Attendee.Levels.Name.split('|')[0]);
    }
    $('#totalTimeSpent').text(ConvertTimeDash(data.LessonsTakenHours));
    $('#totalClassesCount').text(data.LessonsTaken);

    $('#viewMoreHidden').val(data.NextClassId);
}

function ConvertDateSlash(data) {
    if (data == null)
        return "-";
    var fullDate = new Date(parseInt(data.replace(/[^0-9]/g, '')));
    var twoDigitMonth = fullDate.getMonth() + 1 + "";
    if (twoDigitMonth.length == 1) twoDigitMonth = "0" + twoDigitMonth;
    var twoDigitDate = fullDate.getDate() + "";
    if (twoDigitDate.length == 1) twoDigitDate = "0" + twoDigitDate;
   
    return twoDigitDate + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
}

function ConvertTimeDash(data) {
    var hours = Math.floor(data / 60) + "";
    var minutes = data % 60 + "";
    if (minutes.length == 1) minutes = "0" + minutes;
    return hours + ":" + minutes;
}

function ViewMore() {
    GetDetails($('#viewMoreHidden').val());
}