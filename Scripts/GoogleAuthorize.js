﻿
var clientId = '401093889198-5u5vh75501jdtqcduv54ui48vjbu2vme.apps.googleusercontent.com';
var apiKey = 'AIzaSyDYWctJmgBYQHfDRSRsUUIimrnns0nVt5k';
var scopes = 'https://www.googleapis.com/auth/userinfo.email';

function handleClientLoad() {
    gapi.client.setApiKey(apiKey); 
}


function handleAuthClick(event) {
    gapi.auth.authorize({ client_id: clientId, scope: scopes, immediate: false }, handleAuthResult);
    return false;
}

function handleAuthResult(authResult) {
    console.log(authResult);
    if (authResult && !authResult.error) {
        makeApiCall();
    }
    else
        alert("Error on API");
}

function makeApiCall() {
    gapi.client.load('plus', 'v1', function () {
        var request = gapi.client.plus.people.get({
            'userId': 'me'
        });
        request.execute(function (resp) {
            console.log(resp);
            var img = resp.image.url;
            var displayName = resp.displayName;
            var emails = resp.emails[0].value;
            $.post("/openlang/CheckExistUserByEmail",{'email':emails}, function (result) {
                if (!result.result)
                    window.location.href = "/openlang/LogInViaSocial?email=" + emails;
                else {
                    var pop = $("#formLogin");
                    if (!displayName)
                        displayName = emails;
                    pop.find("#signup-name").val(displayName);
                pop.find("#signup-email").val(emails);
                pop.find("#signup-password").val("00000000")
                pop.find("#signup-passwordConfirm").val("00000000");
                    var frm = document.forms.formToSend;
                    frm.elements.ProfilePicture.value = img;
                    changeModel('.login-modal', '#tell-us-more');
                }
            },'json');
        });
    });
}

function changeModel(current, next) {
    $(current).modal('hide');
    setTimeout(function () {
        $(next).modal('show');
    }, 700)
}