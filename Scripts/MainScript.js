﻿$(document).ready(function () {
    
    $("#sphere").selectmenu();
    $("#city").selectmenu();
    setEqualHeight($(".more_info .col-md-3 div:nth-child(3)"));
    var h = document.getElementsByClassName("main")[0].offsetHeight;
    var h2 = document.getElementsByTagName("body")[0].offsetHeight;
    if (h == h2) {
        $(".prt").height(function () {
            return document.getElementsByTagName("body")[0].offsetHeight - document.getElementsByTagName("header")[0].offsetHeight - document.getElementsByTagName("footer")[0].offsetHeight;
        });
    }
    if (navigator.userAgent.indexOf("Safari") > -1 && $(".prt").length > 0 && $(window).height()>1000) {
        $("html,body,.main").height($(window).height());
        $(".prt").height(function () {
            return document.getElementsByTagName("body")[0].offsetHeight - document.getElementsByTagName("header")[0].offsetHeight - document.getElementsByTagName("footer")[0].offsetHeight;
        });
    }
});
function setEqualHeight(columns) {
    var tallestcolumn = 0;
    columns.each(
    function () {
        currentHeight = $(this).height();
        if (currentHeight > tallestcolumn) {
            tallestcolumn = currentHeight;
        }
    }
    );
    columns.height(tallestcolumn);
}