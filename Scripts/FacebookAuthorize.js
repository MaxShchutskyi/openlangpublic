﻿function facebookLogin()
{
    FB.login(function (response) {
        console.log(response);
        if (response.status === 'connected') {
            testAPI();
        } else if (response.status === 'not_authorized') {

        } else {

        }
    }, { scope: 'email,public_profile,user_about_me' });
}

window.fbAsyncInit = function () {
    FB.init({
        appId: '1538355979822340',
        cookie: true,  
        xfbml: true,
        version: 'v2.2',
        oauth: true,
        status: true
    });
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
} (document, 'script', 'facebook-jssdk'));

function testAPI() {
    FB.api('/me?fields=first_name,last_name,id,email,picture', function (resp) {
        var img = resp.picture.data.url;
        var displayName = resp.first_name + " " + resp.last_name;
        var emails = resp.email;
        $.post("/openlang/CheckExistUserByEmail", { 'email': emails }, function (result) {
            if (!result.result)
                window.location.href = "/openlang/LogInViaSocial?email=" + emails;
            else {
                var pop = $("#formLogin");
                if (!displayName)
                    displayName = emails;
                pop.find("#signup-name").val(displayName);
                pop.find("#signup-email").val(emails);
                pop.find("#signup-password").val("00000000")
                pop.find("#signup-passwordConfirm").val("00000000");
                var frm = document.forms.formToSend;
                frm.elements.ProfilePicture.value = img;
                changeModel('.login-modal', '#tell-us-more');
            }
        }, 'json');
    });
}