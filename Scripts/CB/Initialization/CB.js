﻿function LoadCB() {
     $('.pad_load').fadeIn("medium");
    $.ajax({
        url: '/Home/GetSubscriptions',
        dataType: 'json',
        type: 'POST',
        data: { title: $("#search").val() },
        success: function (data) {
            FillTableCB(data);
            $('.pad_load').fadeOut("medium");
        },
        error: function () {
            alert('error');
            $('.pad_load').fadeOut("medium");
        }
    });
}

function FillTableCB(data) {
    if (data.length == 0) {
        $('#dataTableCB tr:gt(0)').after().remove();
        return;
    }

    $('#dataTableCB tr:gt(0)').after().remove();
    var r = new Array(), j = -1;
    $.each(data, function (k, v) {

        var startDate = new Date(parseInt(data[k].StartDate.replace(/[^0-9]/g, '')));
        var twoDigitMonth = startDate.getMonth() + 1 + "";
        if (twoDigitMonth.length == 1) twoDigitMonth = "0" + twoDigitMonth;
        var twoDigitDate = startDate.getDate() + "";
        if (twoDigitDate.length == 1) twoDigitDate = "0" + twoDigitDate;
        var startDateStr = twoDigitDate + "/" + twoDigitMonth + "/" + startDate.getFullYear();
      
        var expDate = new Date(parseInt(data[k].ExperationDate.replace(/[^0-9]/g, '')));
        twoDigitMonth = expDate.getMonth() + 1 + "";
        if (twoDigitMonth.length == 1) twoDigitMonth = "0" + twoDigitMonth;
         twoDigitDate = expDate.getDate() + "";
        if (twoDigitDate.length == 1) twoDigitDate = "0" + twoDigitDate;
        var expDateStr = twoDigitDate + "/" + twoDigitMonth + "/" + expDate.getFullYear();

        r[++j] = '<tr><td>';
        r[++j] = startDateStr;
        r[++j] = '</td><td><span class="title_word">';
        r[++j] = data[k].Name;
        r[++j] = '</span></td><td><img src="';
        r[++j] = data[k].Languages.ImageSrc;
        r[++j] = '"> ';
        r[++j] = data[k].Languages.Name.split('|')[0];
        r[++j] = '</td><td>';
        r[++j] = data[k].Name;
        r[++j] = '</td><td>';
        //r[++j] = expDateStr;
        r[++j] = '</td></tr>';
    });
    r[++j] = '<tr><td><br /></td><td></td><td></td><td></td><td></td></tr>';
    $('#dataTableCB tr:last').after(r.join(''));
}