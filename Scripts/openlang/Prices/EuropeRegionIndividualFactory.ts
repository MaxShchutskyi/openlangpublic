﻿class EuropeRegionIndividualFactory extends AbstractRegionFactory {
    initialize(): void {
        var price = new EuropeIndividualBasicPrices();
        this.items.push(price);
        this.items.push(new EuropeIndividualPremiumPrices());
        this.items.push(new IndividualGroupPrices(price));
    }
}