﻿class CustomCurrencyConterter implements ICurrencyConverter {
    convertFrom: string = "EUR";
    convertTo: string;
    convert(price: number, callback: (newPrice) => void) {
        var isCoef = this.checkExistsСoefficient(this.convertTo);
        if (!isNaN(isCoef)) {
            callback(isCoef * price);
            return;
        }
        this.getСoefficient(this.convertTo, (rate) => {
            this.setСoefficientToStorage(this.convertTo, rate);
            callback(rate * price);
        }, () => {
            callback(null);
        });
    }
    convertAll(prices: Array<any>, callback: (newPrices) => void) {
        var isCoef = this.checkExistsСoefficient(this.convertTo);
        if (!isNaN(isCoef))
            callback(this.getAllPricesByCoef(prices, isCoef));
        else 
            this.getСoefficient(this.convertTo, rate => {
                this.setСoefficientToStorage(this.convertTo, rate);
                callback(this.getAllPricesByCoef(prices, rate));
            }, () => callback(null));
    }
    private getAllPricesByCoef(prices: Array<any>, coef: number): Array<any> {
        var result = [];
        prices.forEach(price => {
            result.push({ 'price': price.price * coef, 'count': price.count });
        });
        return result;
    }

    private getСoefficient(to: string, callback: (rate: number) => void, error: () => void): void {
        var url = "https://query.yahooapis.com/v1/public/yql?q=select%20%2a%20from%20yahoo.finance.xchange%20where%20pair%20in%20%28%22" + this.convertFrom + "+" + to + "%22)&format=json&env=store://datatables.org/alltableswithkeys";
        $.ajax({
            type: "get",
            dataType: "json",
            url: url,
            success: (result) => {
                var rate = result.query.results.rate.Rate;
                var rateFloat = parseFloat(rate);
                callback(rateFloat);
            },
            error: () => {
                error();
            }
        });
    }

    private checkExistsСoefficient(currency: string): number {
        var checkInStorage = localStorage.getItem(currency);
        if (!checkInStorage)
            return NaN;
        var decript = CryptoJS.AES.decrypt(checkInStorage, "5899404");
        var jsonStore = JSON.parse(decript.toString(CryptoJS.enc.Utf8));
        if (this.compareDates(jsonStore.date))
            return jsonStore.coefficient;
        return NaN;
    }

    private compareDates(date: string): boolean {
        var curdate = new Date(new Date().toLocaleString("en"));
        var dd = curdate.getDate();
        var mm = curdate.getMonth() + 1;
        var yyyy = curdate.getFullYear();
        var dateFromStore = new Date(date);
        if ((dateFromStore.getDate() !== dd) || ((dateFromStore.getMonth() + 1) !== mm) || (dateFromStore.getFullYear() !== yyyy))
            return false;
        return true;

    }

    private setСoefficientToStorage(currency: string, rate: number) {
        var obj = { date: new Date().toLocaleString("en"), coefficient: rate };
        var enctypted = CryptoJS.AES.encrypt(JSON.stringify(obj), "5899404");
        localStorage.setItem(currency, enctypted);

    }
}