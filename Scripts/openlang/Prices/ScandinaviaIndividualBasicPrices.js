var ScandinaviaIndividualBasicPrices = /** @class */ (function () {
    function ScandinaviaIndividualBasicPrices() {
        this.type = TypePrices.InividualBasic;
    }
    ScandinaviaIndividualBasicPrices.prototype.getPrice = function (count) {
        switch (count) {
            case 5:
                {
                    return new ProprtiesPrices(175, (175 / count).toFixed(2).toString());
                }
            case 10:
                {
                    return new ProprtiesPrices(334, (334 / count).toFixed(2).toString());
                }
            case 20:
                {
                    return new ProprtiesPrices(604, (604 / count).toFixed(2).toString());
                }
            case 15:
                {
                    return new ProprtiesPrices(477, (477 / count).toFixed(2).toString());
                }
            case 25:
                {
                    return new ProprtiesPrices(715, (715 / count).toFixed(2).toString());
                }
            case 50:
                {
                    return new ProprtiesPrices(1350, (1350 / count).toFixed(2).toString());
                }
            case 100:
                {
                    return new ProprtiesPrices(2540, (2540 / count).toFixed(2).toString());
                }
            default:
                return null;
        }
    };
    return ScandinaviaIndividualBasicPrices;
}());
//# sourceMappingURL=ScandinaviaIndividualBasicPrices.js.map