var CurrenciesCountry = /** @class */ (function () {
    function CurrenciesCountry(country, currency) {
        this.countryCode = country;
        this.currency = currency;
    }
    CurrenciesCountry.initCurrencies = function () {
        this.listCurrencyCodes = [];
        this.listCurrencyCodes.push(new CurrenciesCountry("US", "USD"));
        this.listCurrencyCodes.push(new CurrenciesCountry("Other", "EUR"));
        this.listCurrencyCodes.push(new CurrenciesCountry("CH", "CHF"));
        this.listCurrencyCodes.push(new CurrenciesCountry("BR", "BRL"));
        this.listCurrencyCodes.push(new CurrenciesCountry("SE", "SEK"));
        this.listCurrencyCodes.push(new CurrenciesCountry("NO", "NOK"));
        this.listCurrencyCodes.push(new CurrenciesCountry("DK", "DKK"));
    };
    CurrenciesCountry.convertAllTypes = function (typeLessons, callback) {
        var _this = this;
        CurrenciesCountry.convertAllTypesResult = [];
        var currencyTable = this.CountryPriceTable.getTypePrice(typeLessons);
        var prices = [];
        var countLessons = [5, 10, 15, 20, 25, 50, 100];
        if (this.selectedCurrency.countryCode == "Other") {
            countLessons.forEach(function (elem) {
                var res = _this.CountryPriceTable.getTypePrice(typeLessons).getPrice(elem);
                res.count = elem;
                CurrenciesCountry.convertAllTypesResult.push(res);
            });
            callback(CurrenciesCountry.convertAllTypesResult, CurrenciesCountry.convertAllTypesResult);
        }
        else {
            countLessons.forEach(function (elem) {
                prices.push({ 'count': elem, 'price': currencyTable.getPrice(elem).price });
            });
            this.CurrentCurrencyConverter.convertAll(prices, function (newPrices) {
                newPrices.forEach(function (elem) {
                    var pr = parseInt(elem.price.toFixed(0));
                    var priceForOneLesson = (pr / elem.count).toFixed(2);
                    var prpr = new ProprtiesPrices(pr, priceForOneLesson);
                    prpr.count = elem.count;
                    CurrenciesCountry.convertAllTypesResult.push(prpr);
                });
                callback(CurrenciesCountry.convertAllTypesResult, prices);
            });
        }
        //countLessons.forEach((count,index) => {
        //    this.convert(count, typeLessons, res => {
        //        res.count = count;
        //        CurrenciesCountry.convertAllTypesResult.push(res);
        //        if (index == countLessons.length - 1)
        //            callback(CurrenciesCountry.convertAllTypesResult);
        //    });
        //});
    };
    CurrenciesCountry.getCurrentEurPrice = function (countLessons, typeLessons) {
        return this.CountryPriceTable.getTypePrice(typeLessons).getPrice(countLessons).price;
    };
    CurrenciesCountry.convert = function (countLessons, typeLessons, callback) {
        if (this.selectedCurrency.countryCode == "Other") {
            callback(this.CountryPriceTable.getTypePrice(typeLessons).getPrice(countLessons));
            return;
        }
        var price = this.CountryPriceTable.getTypePrice(typeLessons).getPrice(countLessons);
        this.CurrentCurrencyConverter.convert(price.price, function (res) {
            var pr = parseInt(res.toFixed(0));
            var priceForOneLesson = (pr / countLessons).toFixed(2);
            return callback(new ProprtiesPrices(pr, priceForOneLesson));
        });
    };
    CurrenciesCountry.initCurrencyCountry = function (code) {
        if (!this.listCurrencyCodes)
            this.initCurrencies();
        for (var i = 0; i < this.listCurrencyCodes.length; i++) {
            if (this.listCurrencyCodes[i].countryCode === code || this.listCurrencyCodes[i].currency === code) {
                this.selectedCurrency = this.listCurrencyCodes[i];
                this.CurrentCurrencyConverter.convertTo = this.selectedCurrency.currency;
                return;
            }
        }
        this.selectedCurrency = new CurrenciesCountry("Other", "EUR");
        this.CurrentCurrencyConverter.convertTo = this.selectedCurrency.currency;
    };
    return CurrenciesCountry;
}());
//# sourceMappingURL=CountryCurrency.js.map