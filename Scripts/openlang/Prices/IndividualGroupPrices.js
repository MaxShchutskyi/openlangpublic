var IndividualGroupPrices = /** @class */ (function () {
    function IndividualGroupPrices(basicTypePrices) {
        this.type = TypePrices.Group;
        this.basicTypePrices = basicTypePrices;
    }
    IndividualGroupPrices.prototype.getPrice = function (count) {
        var properties = this.basicTypePrices.getPrice(count);
        var mainPrice = Math.round(properties.price - ((properties.price / 100) * 13));
        var priveForoneLesson = (mainPrice / count).toFixed(2).toString();
        return new ProprtiesPrices(mainPrice, priveForoneLesson);
    };
    return IndividualGroupPrices;
}());
//# sourceMappingURL=IndividualGroupPrices.js.map