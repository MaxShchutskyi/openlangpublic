var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var SwitzerlandRegionIndividualFactory = /** @class */ (function (_super) {
    __extends(SwitzerlandRegionIndividualFactory, _super);
    function SwitzerlandRegionIndividualFactory() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SwitzerlandRegionIndividualFactory.prototype.initialize = function () {
        var price = new SwitzerlandIndividualBasicPrices();
        this.items.push(price);
        this.items.push(new SwitzerlandIndividualPremiumPrices());
        this.items.push(new IndividualGroupPrices(price));
    };
    return SwitzerlandRegionIndividualFactory;
}(AbstractRegionFactory));
//# sourceMappingURL=SwitzerlandRegionIndividualFactory.js.map