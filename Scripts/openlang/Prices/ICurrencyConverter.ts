﻿interface ICurrencyConverter {
    convertFrom: string;
    convertTo: string;
    convert(price: number, callback: (newPrice) => void);
    convertAll(prices: Array<any>, callback: (newPrices) => void)
}