var SwitzerlandIndividualPremiumPrices = /** @class */ (function () {
    function SwitzerlandIndividualPremiumPrices() {
        this.type = TypePrices.InividualPremium;
    }
    SwitzerlandIndividualPremiumPrices.prototype.getPrice = function (count) {
        switch (count) {
            case 5:
                {
                    return new ProprtiesPrices(185, (185 / count).toFixed(2).toString());
                }
            case 10:
                {
                    return new ProprtiesPrices(351.50, (351.50 / count).toFixed(2).toString());
                }
            case 20:
                {
                    return new ProprtiesPrices(629, (629 / count).toFixed(2).toString());
                }
            case 15:
                {
                    return new ProprtiesPrices(499.50, (499.50 / count).toFixed(2).toString());
                }
            case 25:
                {
                    return new ProprtiesPrices(740, (740 / count).toFixed(2).toString());
                }
            case 50:
                {
                    return new ProprtiesPrices(1387.50, (1387.50 / count).toFixed(2).toString());
                }
            case 100:
                {
                    return new ProprtiesPrices(2701, (2701 / count).toFixed(2).toString());
                }
            default:
                return null;
        }
    };
    return SwitzerlandIndividualPremiumPrices;
}());
//# sourceMappingURL=SwitzerlandIndividualPremiumPrices.js.map