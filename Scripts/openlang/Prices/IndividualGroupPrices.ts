﻿class IndividualGroupPrices implements ITypePrice {
    basicTypePrices:ITypePrice;
    constructor(basicTypePrices:ITypePrice) {
        this.basicTypePrices = basicTypePrices;
    }
    getPrice(count: number): ProprtiesPrices {
        var properties = this.basicTypePrices.getPrice(count);
        var mainPrice = Math.round(properties.price - ((properties.price / 100) * 13));
        var priveForoneLesson = (mainPrice / count).toFixed(2).toString();
        return new ProprtiesPrices(mainPrice, priveForoneLesson);
    }
    type: TypePrices = TypePrices.Group;
}