var EuropeIndividualBasicPrices = /** @class */ (function () {
    function EuropeIndividualBasicPrices() {
        this.type = TypePrices.InividualBasic;
    }
    EuropeIndividualBasicPrices.prototype.getPrice = function (count) {
        switch (count) {
            case 5:
                {
                    return new ProprtiesPrices(105, (105 / count).toFixed(2).toString());
                }
            case 10:
                {
                    return new ProprtiesPrices(205, (205 / count).toFixed(2).toString());
                }
            case 20:
                {
                    return new ProprtiesPrices(390, (390 / count).toFixed(2).toString());
                }
            case 15:
                {
                    return new ProprtiesPrices(300, (300 / count).toFixed(2).toString());
                }
            case 25:
                {
                    return new ProprtiesPrices(475, (475 / count).toFixed(2).toString());
                }
            case 50:
                {
                    return new ProprtiesPrices(925, (925 / count).toFixed(2).toString());
                }
            case 100:
                {
                    return new ProprtiesPrices(1800, (1800 / count).toFixed(2).toString());
                }
            default:
                return null;
        }
    };
    return EuropeIndividualBasicPrices;
}());
//# sourceMappingURL=EuropeIndividualBasicPrices.js.map