var SwitzerlandIndividualBasicPrices = /** @class */ (function () {
    function SwitzerlandIndividualBasicPrices() {
        this.type = TypePrices.InividualBasic;
    }
    SwitzerlandIndividualBasicPrices.prototype.getPrice = function (count) {
        switch (count) {
            case 5:
                {
                    return new ProprtiesPrices(160, (160 / count).toFixed(2).toString());
                }
            case 10:
                {
                    return new ProprtiesPrices(304, (304 / count).toFixed(2).toString());
                }
            case 20:
                {
                    return new ProprtiesPrices(544, (544 / count).toFixed(2).toString());
                }
            case 15:
                {
                    return new ProprtiesPrices(432, (432 / count).toFixed(2).toString());
                }
            case 25:
                {
                    return new ProprtiesPrices(640, (640 / count).toFixed(2).toString());
                }
            case 50:
                {
                    return new ProprtiesPrices(1200, (1200 / count).toFixed(2).toString());
                }
            case 100:
                {
                    return new ProprtiesPrices(2240, (2240 / count).toFixed(2).toString());
                }
            default:
                return null;
        }
    };
    return SwitzerlandIndividualBasicPrices;
}());
//# sourceMappingURL=SwitzerlandIndividualBasicPrices.js.map