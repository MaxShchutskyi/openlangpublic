var CurrencyConverter = /** @class */ (function () {
    function CurrencyConverter() {
    }
    CurrencyConverter.convert = function (to, price, callback) {
        var _this = this;
        var isCoef = this.checkExistsСoefficient(to);
        if (!isNaN(isCoef)) {
            callback(isCoef * price);
            return;
        }
        this.getСoefficient(to, function (rate) {
            _this.setСoefficientToStorage(to, rate);
            callback(rate * price);
        }, function () {
            callback(null);
        });
    };
    CurrencyConverter.getСoefficient = function (to, callback, error) {
        var url = "https://query.yahooapis.com/v1/public/yql?q=select%20%2a%20from%20yahoo.finance.xchange%20where%20pair%20in%20%28%22EUR" + to + "%22)&format=json&env=store://datatables.org/alltableswithkeys";
        $.ajax({
            type: "get",
            dataType: "json",
            url: url,
            success: function (result) {
                var rate = result.query.results.rate.Rate;
                var rateFloat = parseFloat(rate);
                callback(rateFloat);
            },
            error: function () {
                error();
            }
        });
    };
    CurrencyConverter.checkExistsСoefficient = function (currency) {
        var checkInStorage = localStorage.getItem(currency);
        if (!checkInStorage)
            return NaN;
        var decript = CryptoJS.AES.decrypt(checkInStorage, "5899404");
        var jsonStore = JSON.parse(decript.toString(CryptoJS.enc.Utf8));
        if (this.compareDates(jsonStore.date))
            return jsonStore.coefficient;
        return NaN;
    };
    CurrencyConverter.compareDates = function (date) {
        var curdate = new Date(new Date().toLocaleString("en"));
        var dd = curdate.getDate();
        var mm = curdate.getMonth() + 1;
        var yyyy = curdate.getFullYear();
        var dateFromStore = new Date(date);
        if ((dateFromStore.getDate() !== dd) || ((dateFromStore.getMonth() + 1) !== mm) || (dateFromStore.getFullYear() !== yyyy))
            return false;
        return true;
    };
    CurrencyConverter.setСoefficientToStorage = function (currency, rate) {
        var obj = { date: new Date().toLocaleString("en"), coefficient: rate };
        var enctypted = CryptoJS.AES.encrypt(JSON.stringify(obj), "5899404");
        localStorage.setItem(currency, enctypted);
    };
    return CurrencyConverter;
}());
//# sourceMappingURL=CurrencyConverter.js.map