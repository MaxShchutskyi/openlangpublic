var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var ScandinaviaRegionIndividualFactory = /** @class */ (function (_super) {
    __extends(ScandinaviaRegionIndividualFactory, _super);
    function ScandinaviaRegionIndividualFactory() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ScandinaviaRegionIndividualFactory.prototype.initialize = function () {
        var price = new ScandinaviaIndividualBasicPrices();
        this.items.push(price);
        this.items.push(new ScandinaviaIndividualPremium());
        this.items.push(new IndividualGroupPrices(price));
    };
    return ScandinaviaRegionIndividualFactory;
}(AbstractRegionFactory));
//# sourceMappingURL=ScandinaviaRegionIndividualFactory.js.map