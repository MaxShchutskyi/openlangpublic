var ScandinaviaIndividualPremium = /** @class */ (function () {
    function ScandinaviaIndividualPremium() {
        this.type = TypePrices.InividualPremium;
    }
    ScandinaviaIndividualPremium.prototype.getPrice = function (count) {
        switch (count) {
            case 5:
                {
                    return new ProprtiesPrices(195, (195 / count).toFixed(2).toString());
                }
            case 10:
                {
                    return new ProprtiesPrices(370.50, (370.50 / count).toFixed(2).toString());
                }
            case 20:
                {
                    return new ProprtiesPrices(663, (663 / count).toFixed(2).toString());
                }
            case 15:
                {
                    return new ProprtiesPrices(526.50, (526.50 / count).toFixed(2).toString());
                }
            case 25:
                {
                    return new ProprtiesPrices(780, (780 / count).toFixed(2).toString());
                }
            case 50:
                {
                    return new ProprtiesPrices(1462.50, (1462.50 / count).toFixed(2).toString());
                }
            case 100:
                {
                    return new ProprtiesPrices(2847, (2847 / count).toFixed(2).toString());
                }
            default:
                return null;
        }
    };
    return ScandinaviaIndividualPremium;
}());
//# sourceMappingURL=ScandinaviaIndividualPremium.js.map