﻿class EuropeIndividualPremiumPrices implements ITypePrice {
    type: TypePrices = TypePrices.InividualPremium;
    public getPrice(count: number): ProprtiesPrices {
        switch (count) {
            case 5:
                {
                    return new ProprtiesPrices(125, (125 / count).toFixed(2).toString());
                }
            case 10:
                {
                    return new ProprtiesPrices(243, (243 / count).toFixed(2).toString());
                }
            case 20:
                {
                    return new ProprtiesPrices(458, (458 / count).toFixed(2).toString());
                }
            case 15:
                {
                    return new ProprtiesPrices(354, (354 / count).toFixed(2).toString());
                }
            case 25:
                {
                    return new ProprtiesPrices(555, (555 / count).toFixed(2).toString());
                }
            case 50:
                {
                    return new ProprtiesPrices(1075, (1075 / count).toFixed(2).toString());
                }
            case 100:
                {
                    return new ProprtiesPrices(2080, (2080 / count).toFixed(2).toString());
                }
            default:
                return null;
        }
    }
}