﻿class CurrenciesCountry {
    public countryCode: string;
    public currency: string;
    public static listCurrencyCodes: Array<CurrenciesCountry>;
    public static selectedCurrency: CurrenciesCountry;
    public static CountryPriceTable: AbstractRegionFactory;
    public static CurrentCurrencyConverter: ICurrencyConverter;
    private static convertAllTypesResult: Array<ProprtiesPrices>;

    constructor(country: string, currency: string) {
        this.countryCode = country;
        this.currency = currency;
    }

    private static initCurrencies() {
        this.listCurrencyCodes = [];
        this.listCurrencyCodes.push(new CurrenciesCountry("US", "USD"));
        this.listCurrencyCodes.push(new CurrenciesCountry("Other", "EUR"));
        this.listCurrencyCodes.push(new CurrenciesCountry("CH", "CHF"));
        this.listCurrencyCodes.push(new CurrenciesCountry("BR", "BRL"));
        this.listCurrencyCodes.push(new CurrenciesCountry("SE", "SEK"));
        this.listCurrencyCodes.push(new CurrenciesCountry("NO", "NOK"));
        this.listCurrencyCodes.push(new CurrenciesCountry("DK", "DKK"));
    }
    public static convertAllTypes(typeLessons: TypePrices, callback: (result: Array<ProprtiesPrices>, array: any) => void): void {
        CurrenciesCountry.convertAllTypesResult = [];
        var currencyTable = this.CountryPriceTable.getTypePrice(typeLessons);
        var prices = [];
        var countLessons = [5, 10, 15, 20, 25, 50, 100];
        if (this.selectedCurrency.countryCode == "Other") {
            countLessons.forEach(elem => {
                var res = this.CountryPriceTable.getTypePrice(typeLessons).getPrice(elem);
                res.count = elem;
                CurrenciesCountry.convertAllTypesResult.push(res);
            });
            callback(CurrenciesCountry.convertAllTypesResult, CurrenciesCountry.convertAllTypesResult);
        }
        else {
            countLessons.forEach(elem => {
                prices.push({ 'count': elem, 'price': currencyTable.getPrice(elem).price });
            });
            this.CurrentCurrencyConverter.convertAll(prices, newPrices => {
                newPrices.forEach(elem => {
                    var pr = parseInt(elem.price.toFixed(0));
                    var priceForOneLesson = (pr / elem.count).toFixed(2);
                    var prpr = new ProprtiesPrices(pr, priceForOneLesson);
                    prpr.count = elem.count;
                    CurrenciesCountry.convertAllTypesResult.push(prpr);
                });
                callback(CurrenciesCountry.convertAllTypesResult, prices);
            });
        }

        //countLessons.forEach((count,index) => {
        //    this.convert(count, typeLessons, res => {
        //        res.count = count;
        //        CurrenciesCountry.convertAllTypesResult.push(res);
        //        if (index == countLessons.length - 1)
        //            callback(CurrenciesCountry.convertAllTypesResult);
        //    });
        //});
    }
    public static getCurrentEurPrice(countLessons: number, typeLessons: TypePrices) {
        return this.CountryPriceTable.getTypePrice(typeLessons).getPrice(countLessons).price;
    }

    public static convert(countLessons: number, typeLessons: TypePrices, callback: (result: ProprtiesPrices) => void): void {
        if (this.selectedCurrency.countryCode == "Other") {
            callback(this.CountryPriceTable.getTypePrice(typeLessons).getPrice(countLessons));
            return;
        }
        var price = this.CountryPriceTable.getTypePrice(typeLessons).getPrice(countLessons);
        this.CurrentCurrencyConverter.convert(price.price, (res) => {
            var pr = parseInt(res.toFixed(0));
            var priceForOneLesson = (pr / countLessons).toFixed(2);
            return callback(new ProprtiesPrices(pr, priceForOneLesson));
        });
    }

    public static initCurrencyCountry(code: string):void {
        if (!this.listCurrencyCodes)
            this.initCurrencies();
        for (var i = 0; i < this.listCurrencyCodes.length; i++) {
            if (this.listCurrencyCodes[i].countryCode === code || this.listCurrencyCodes[i].currency === code) {
                this.selectedCurrency = this.listCurrencyCodes[i];
                this.CurrentCurrencyConverter.convertTo = this.selectedCurrency.currency;
                return;
            }
        }
        this.selectedCurrency = new CurrenciesCountry("Other", "EUR");
        this.CurrentCurrencyConverter.convertTo = this.selectedCurrency.currency;
    }
}