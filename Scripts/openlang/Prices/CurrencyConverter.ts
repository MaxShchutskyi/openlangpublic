﻿declare var CryptoJS: any;
class CurrencyConverter {
    static convert(to: string, price: number, callback: (result: number) => void):void {
        var isCoef = this.checkExistsСoefficient(to);
        if (!isNaN(isCoef)) {
            callback(isCoef * price);
            return;
        }
        this.getСoefficient(to, (rate) => {
            this.setСoefficientToStorage(to, rate);
            callback(rate * price);
        }, () => {
            callback(null);
        });

    }

    private static getСoefficient(to: string, callback:(rate:number)=>void, error:()=>void):void {
        var url = "https://query.yahooapis.com/v1/public/yql?q=select%20%2a%20from%20yahoo.finance.xchange%20where%20pair%20in%20%28%22EUR" + to + "%22)&format=json&env=store://datatables.org/alltableswithkeys";
        $.ajax({
            type: "get",
            dataType: "json",
            url: url,
            success: (result) => {
                var rate = result.query.results.rate.Rate;
                var rateFloat = parseFloat(rate);
                callback(rateFloat);
            },
            error:() => {
                error();
            }
        });
    }

    private static checkExistsСoefficient(currency: string): number {
        var checkInStorage = localStorage.getItem(currency);
        if (!checkInStorage)
            return NaN;
        var decript = CryptoJS.AES.decrypt(checkInStorage, "5899404");
        var jsonStore = JSON.parse(decript.toString(CryptoJS.enc.Utf8));
        if (this.compareDates(jsonStore.date))
            return jsonStore.coefficient;
        return NaN;
    }

    private static compareDates(date: string):boolean {
        var curdate = new Date(new Date().toLocaleString("en"));
        var dd = curdate.getDate();
        var mm = curdate.getMonth() + 1;
        var yyyy = curdate.getFullYear();
        var dateFromStore = new Date(date);
        if ((dateFromStore.getDate()!== dd) || ((dateFromStore.getMonth()+1)!== mm) || (dateFromStore.getFullYear()!==yyyy))
             return false;
        return true;

    }

    private static setСoefficientToStorage(currency: string, rate: number) {
        var obj = { date: new Date().toLocaleString("en"), coefficient: rate };
        var enctypted = CryptoJS.AES.encrypt(JSON.stringify(obj), "5899404");
        localStorage.setItem(currency, enctypted);

    }
}