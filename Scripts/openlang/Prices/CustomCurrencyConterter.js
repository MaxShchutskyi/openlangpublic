var CustomCurrencyConterter = /** @class */ (function () {
    function CustomCurrencyConterter() {
        this.convertFrom = "EUR";
    }
    CustomCurrencyConterter.prototype.convert = function (price, callback) {
        var _this = this;
        var isCoef = this.checkExistsСoefficient(this.convertTo);
        if (!isNaN(isCoef)) {
            callback(isCoef * price);
            return;
        }
        this.getСoefficient(this.convertTo, function (rate) {
            _this.setСoefficientToStorage(_this.convertTo, rate);
            callback(rate * price);
        }, function () {
            callback(null);
        });
    };
    CustomCurrencyConterter.prototype.convertAll = function (prices, callback) {
        var _this = this;
        var isCoef = this.checkExistsСoefficient(this.convertTo);
        if (!isNaN(isCoef))
            callback(this.getAllPricesByCoef(prices, isCoef));
        else
            this.getСoefficient(this.convertTo, function (rate) {
                _this.setСoefficientToStorage(_this.convertTo, rate);
                callback(_this.getAllPricesByCoef(prices, rate));
            }, function () { return callback(null); });
    };
    CustomCurrencyConterter.prototype.getAllPricesByCoef = function (prices, coef) {
        var result = [];
        prices.forEach(function (price) {
            result.push({ 'price': price.price * coef, 'count': price.count });
        });
        return result;
    };
    CustomCurrencyConterter.prototype.getСoefficient = function (to, callback, error) {
        var url = "https://query.yahooapis.com/v1/public/yql?q=select%20%2a%20from%20yahoo.finance.xchange%20where%20pair%20in%20%28%22" + this.convertFrom + "+" + to + "%22)&format=json&env=store://datatables.org/alltableswithkeys";
        $.ajax({
            type: "get",
            dataType: "json",
            url: url,
            success: function (result) {
                var rate = result.query.results.rate.Rate;
                var rateFloat = parseFloat(rate);
                callback(rateFloat);
            },
            error: function () {
                error();
            }
        });
    };
    CustomCurrencyConterter.prototype.checkExistsСoefficient = function (currency) {
        var checkInStorage = localStorage.getItem(currency);
        if (!checkInStorage)
            return NaN;
        var decript = CryptoJS.AES.decrypt(checkInStorage, "5899404");
        var jsonStore = JSON.parse(decript.toString(CryptoJS.enc.Utf8));
        if (this.compareDates(jsonStore.date))
            return jsonStore.coefficient;
        return NaN;
    };
    CustomCurrencyConterter.prototype.compareDates = function (date) {
        var curdate = new Date(new Date().toLocaleString("en"));
        var dd = curdate.getDate();
        var mm = curdate.getMonth() + 1;
        var yyyy = curdate.getFullYear();
        var dateFromStore = new Date(date);
        if ((dateFromStore.getDate() !== dd) || ((dateFromStore.getMonth() + 1) !== mm) || (dateFromStore.getFullYear() !== yyyy))
            return false;
        return true;
    };
    CustomCurrencyConterter.prototype.setСoefficientToStorage = function (currency, rate) {
        var obj = { date: new Date().toLocaleString("en"), coefficient: rate };
        var enctypted = CryptoJS.AES.encrypt(JSON.stringify(obj), "5899404");
        localStorage.setItem(currency, enctypted);
    };
    return CustomCurrencyConterter;
}());
//# sourceMappingURL=CustomCurrencyConterter.js.map