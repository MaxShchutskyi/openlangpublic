var AbstractRegionFactory = /** @class */ (function () {
    function AbstractRegionFactory() {
        this.items = [];
    }
    AbstractRegionFactory.prototype.getTypePrice = function (type) {
        var selElem;
        this.items.forEach(function (elem) {
            if (elem.type == type)
                selElem = elem;
        });
        return selElem;
    };
    return AbstractRegionFactory;
}());
//# sourceMappingURL=AbstractRegionFactory.js.map