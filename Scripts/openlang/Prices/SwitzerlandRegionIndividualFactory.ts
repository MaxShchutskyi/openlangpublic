﻿class SwitzerlandRegionIndividualFactory extends AbstractRegionFactory{
    initialize(): void {
        var price = new SwitzerlandIndividualBasicPrices();
        this.items.push(price);
        this.items.push(new SwitzerlandIndividualPremiumPrices());
        this.items.push(new IndividualGroupPrices(price));
    }
}