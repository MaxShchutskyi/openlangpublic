﻿class ScandinaviaRegionIndividualFactory extends AbstractRegionFactory {
    initialize(): void {
        var price = new ScandinaviaIndividualBasicPrices();
        this.items.push(price);
        this.items.push(new ScandinaviaIndividualPremium());
        this.items.push(new IndividualGroupPrices(price));
    }
}