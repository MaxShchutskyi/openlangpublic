﻿abstract class AbstractRegionFactory {
    constructor() {
        this.items = [];
    }
    items: Array<ITypePrice>;
    abstract initialize(): void;
    getTypePrice(type: TypePrices): ITypePrice{
        var selElem;
        this.items.forEach(elem => {
            if (elem.type == type)
                selElem = elem;
        });
        return selElem;
    }
}