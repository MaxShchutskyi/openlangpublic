var MyAccountPageLoader = /** @class */ (function () {
    function MyAccountPageLoader() {
    }
    MyAccountPageLoader.preLoad = function () {
        this.timezone.dropdown();
        this.setTelephone();
        this.initLocation();
    };
    MyAccountPageLoader.loadPage = function () {
        this.setLocation(this.userHelper.location);
        this.setCurrency(this.userHelper.currency);
        if (!this.isSelectedTimezone)
            this.setTimezone(this.userHelper.timezone);
    };
    MyAccountPageLoader.initLocation = function () {
        var _this = this;
        this.location.dropdown({
            onChange: function (p, text) {
                _this.userHelper.location = text;
                _this.telephone.intlTelInput("selectCountry", p);
            }
        });
    };
    MyAccountPageLoader.setTelephone = function () {
        this.telephone.intlTelInput({
            utilsScript: '/Scripts/openlang/utils.js'
        });
    };
    MyAccountPageLoader.setTimezone = function (timezoneSel) {
        var selloc = this.timezone.siblings(".menu").children().filter(":contains('" + timezoneSel + "')");
        if (selloc.length > 0)
            this.timezone.dropdown('set selected', timezoneSel);
        else {
            var str = "<div class='item' data-value='" + timezoneSel + "'>" + timezoneSel + "</div>";
            this.timezone.siblings(".menu").append(str);
            var selloc2 = this.timezone.siblings(".menu").children().filter(":contains('" + timezoneSel + "')");
            selloc2.trigger("click");
        }
    };
    MyAccountPageLoader.setCurrency = function (currency) {
        var _this = this;
        if (this.location) {
            if (currency)
                this.currency.children().filter(":contains('" + currency + "')").attr('selected', 'selected');
            else if (localStorage.getItem("region")) {
                var code = JSON.parse(localStorage.getItem("region")).code;
                //var cur = CurrenciesCountry.getCurrencyCountry(code).currency;
                var cur;
                this.userHelper.currency = this.currency.children().filter(":contains('" + cur + "')").attr('selected', 'selected').text();
            }
            else {
                this.userHelper.currency = this.currency.children().first().text();
            }
        }
        this.currency.dropdown({
            onChange: function (p, text) {
                _this.userHelper.currency = text;
            }
        });
    };
    MyAccountPageLoader.setCurrencyByCountryCode = function (code) {
        var selcur = this.currency.siblings(".menu").children("[data-value=" + code + "]");
        selcur.trigger("click");
    };
    MyAccountPageLoader.setLocation = function (location) {
        if (this.userHelper.location) {
            var selloc = this.location.siblings(".menu").children().filter(":contains('" + location + "')");
            selloc.trigger("click");
            this.initLocation();
            this.telephone.intlTelInput("selectCountry", $(selloc).data("value"));
        }
        else {
            //GeolocationManager.init(result => {
            //    this.location.siblings(".menu").children().filter("[data-value='" + result.code + "']").trigger("click");
            //    this.telephone.intlTelInput("selectCountry", result.code);
            //    this.setCurrencyByCountryCode(result.code);
            //    this.getTimeZone(result.timezone);
            //});
            //navigator.geolocation.getCurrentPosition(pos=> {
            //    var coords = pos.coords;
            //    var url = "http://ws.geonames.org/countryCode?lat=" + coords.latitude + "&lng=" + coords.longitude + "&username=eugeny";
            //    $.get(url, result=> {
            //        var countryCode = result.replace(/(\r\n|\n|\r)/gm, "");
            //        this.location.siblings(".menu").children().filter("[data-value='" + countryCode + "']").trigger("click");
            //        this.telephone.intlTelInput("selectCountry", countryCode);
            //        this.setCurrencyByCountryCode(countryCode);
            //        this.getTimeZone(coords);
            //    });
            //});
        }
    };
    MyAccountPageLoader.getTimeZone = function (timezone) {
        var str = "<div class='item' data-value='" + timezone + "'>" + timezone + "</div>";
        this.timezone.siblings(".menu").append(str);
        var selloc = this.timezone.siblings(".menu").children().filter(":contains('" + timezone + "')");
        selloc.trigger("click");
        this.isSelectedTimezone = true;
        //var url = "http://ws.geonames.org/timezoneJSON?lat=" + coords.latitude + "&lng=" + coords.longitude + "&username=eugeny";
        //$.getJSON(url, result=> {
        //    var str = "<div class='item' data-value='" + result.timezoneId + "'>" + result.timezoneId + "</div>";
        //    this.timezone.siblings(".menu").append(str);
        //    var selloc = this.timezone.siblings(".menu").children().filter(":contains('" + result.timezoneId + "')");
        //    selloc.trigger("click");
        //    this.isSelectedTimezone = true;
        //});
    };
    return MyAccountPageLoader;
}());
//# sourceMappingURL=MyAccountPageLoader.js.map