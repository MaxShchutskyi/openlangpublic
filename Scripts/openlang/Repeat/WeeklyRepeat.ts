﻿class WeeklyRepeats extends AbstractRepeat {

    public Repeat(): Array<CalendatItem> {
        var weekss = new Array<number>();
        var momentEndDate = FormRepeatManager.getEndDateToMoment();
        var repeat = FormRepeatManager.getRepeatEvery();
        var daysoftheweek: Array<number> = FormRepeatManager.getCheckedDates();
        var endWeekNumber = momentEndDate.week() - 1;
        var startWeekNumer = moment(FormRepeatManager.getStartDateToMoment()).week() - 1;
        for (var i = startWeekNumer; i <= endWeekNumber; i += repeat)
            weekss.push(i);
        var res = this.getResult(filter => {
            return $.inArray(filter.day(), daysoftheweek) > -1 && $.inArray(filter.week() - 1, weekss) > -1;
        });
        return res;
    }
}