var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var DailyRepeats = /** @class */ (function (_super) {
    __extends(DailyRepeats, _super);
    function DailyRepeats(iterator) {
        if (iterator === void 0) { iterator = null; }
        return _super.call(this, iterator) || this;
    }
    DailyRepeats.prototype.Repeat = function () {
        return this.getResult(function (filter) { return true; });
    };
    return DailyRepeats;
}(AbstractRepeat));
//# sourceMappingURL=DailyRepeats.js.map