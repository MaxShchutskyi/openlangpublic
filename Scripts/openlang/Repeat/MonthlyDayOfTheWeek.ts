﻿class MonthlyDayOfTheWeek extends AbstractRepeat {

    public Repeat(): Array<CalendatItem> {
        return null;
        //for (var i = mnt.month() + 1, t = 1; i <= end_date.getMonth() + 1; i++ , t++){
        //    var nn = mnt.add('month', t);
        //    var date = new Date(nn.year(), nn.month() - 1, 0).getDate();
        //    mnt = mnt.add('day', date);
        //    var ntcurCalItem = new CalendatItem(null, mnt.toDate(), mnt.add('hour', 1).toDate());
        //    ntcurCalItem.week = mnt.week() - 1;
        //    ntcurCalItem.dayOfTheWeek = mnt.day() + 1;
        //    this.resultList.push(ntcurCalItem);
        //}
        //return this.resultList;
    }
    monthDiff(d1: Date, d2: Date): Number {
        var months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getMonth() + 1;
        months += d2.getMonth();
        return months <= 0 ? 0 : months;
    }
}