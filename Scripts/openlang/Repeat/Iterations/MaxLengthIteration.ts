﻿class MaxLengthIteration implements IItereation {
    groupCount: number = null;
    count: number;
    constructor(count: number, countGroup?: number) {
        this.count = count;
        this.groupCount = countGroup;
    }
    iterate(filter: () => moment.Moment): void {
        var gr = this.groupCount;
        while (this.count != 0) {
            if (gr) {
                while (gr > 0) {
                    filter();
                    gr--;
                }
                gr = this.count;
            }
            filter()
            this.count--;
        }
    }
}