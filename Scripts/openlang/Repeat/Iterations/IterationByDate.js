var ITerationByDate = /** @class */ (function () {
    function ITerationByDate(start, end) {
        this.groupCount = null;
        this.start = start;
        this.end = end;
    }
    ITerationByDate.prototype.iterate = function (filter) {
        var mnt = this.start;
        while (true) {
            mnt = filter();
            if (mnt.toDate() > this.end.toDate())
                break;
        }
    };
    return ITerationByDate;
}());
//# sourceMappingURL=IterationByDate.js.map