var MaxLengthIteration = /** @class */ (function () {
    function MaxLengthIteration(count, countGroup) {
        this.groupCount = null;
        this.count = count;
        this.groupCount = countGroup;
    }
    MaxLengthIteration.prototype.iterate = function (filter) {
        var gr = this.groupCount;
        while (this.count != 0) {
            if (gr) {
                while (gr > 0) {
                    filter();
                    gr--;
                }
                gr = this.count;
            }
            filter();
            this.count--;
        }
    };
    return MaxLengthIteration;
}());
//# sourceMappingURL=MaxLengthIteration.js.map