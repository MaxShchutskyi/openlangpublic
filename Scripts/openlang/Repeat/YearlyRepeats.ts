﻿class YearlyRepeats extends AbstractRepeat {

    public Repeat(): Array<CalendatItem> {
        this.repeatEvery = 'year';
        return this.getResult(filter => true);
    }
}