var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var EveryMWFRepeat = /** @class */ (function (_super) {
    __extends(EveryMWFRepeat, _super);
    function EveryMWFRepeat() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    EveryMWFRepeat.prototype.Repeat = function () {
        var _this = this;
        return this.getResult(function (filter) {
            _this.every = 1;
            var numday = filter.day();
            return numday == 1 || numday == 3 || numday == 5;
        });
    };
    return EveryMWFRepeat;
}(AbstractRepeat));
//# sourceMappingURL=EveryMWF.js.map