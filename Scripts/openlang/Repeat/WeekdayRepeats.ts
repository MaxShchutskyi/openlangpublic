﻿class WeekdayRepeats extends AbstractRepeat {

    public Repeat(): Array<CalendatItem> {
        this.every = 1;
        return this.getResult(filter => {
            var day = filter.day();
            return day != 0 && day != 6
        });
    }
}