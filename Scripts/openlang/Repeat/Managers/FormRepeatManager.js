var FormRepeatManager = /** @class */ (function () {
    function FormRepeatManager() {
    }
    FormRepeatManager.getSelectedTypeMethod = function () {
        return FormRepeatManager.object.find("#type-day").val();
    };
    ;
    FormRepeatManager.getEndDate = function () {
        return FormRepeatManager.getEndDateToMoment().toDate();
    };
    FormRepeatManager.getRepeatEvery = function () {
        return parseInt(FormRepeatManager.object.find("#rep-every").val());
    };
    FormRepeatManager.getStartDate = function () {
        return FormRepeatManager.getStartDateToMoment().toDate();
    };
    FormRepeatManager.getEndDateToMoment = function () {
        return moment(FormRepeatManager.object.find("#end_datetime").val(), "DD/MM/YYYY");
    };
    FormRepeatManager.getStartDateToMoment = function () {
        return moment(FormRepeatManager.object.find(".form-control.callendar-date").val(), "DD/MM/YYYY HH:mm");
    };
    FormRepeatManager.clicks = function () {
        if (FormRepeatManager.click != null)
            FormRepeatManager.click();
    };
    FormRepeatManager.getType = function () {
        return FormRepeatManager.object.find("[name='type']:checked").val();
    };
    FormRepeatManager.getMonyhlyType = function () {
        return FormRepeatManager.object.find("[name='WeekMonth']:checked").val();
    };
    FormRepeatManager.getCheckedDates = function () {
        var arr = new Array();
        FormRepeatManager.object.find("#daysoftheweek input:checked").each(function (index, elem) {
            arr.push(parseInt($(elem).data('numweek')));
        });
        return arr;
    };
    FormRepeatManager.getCountMaxRepeats = function () {
        return parseInt(FormRepeatManager.object.find("#maxCountRepeat").val());
    };
    return FormRepeatManager;
}());
//# sourceMappingURL=FormRepeatManager.js.map