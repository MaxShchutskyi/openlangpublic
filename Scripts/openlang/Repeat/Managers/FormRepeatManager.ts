﻿class FormRepeatManager {
    public static object: JQuery;
    public static click:()=>void;
    public static getSelectedTypeMethod(): string {
        return FormRepeatManager.object.find("#type-day").val();
    };
    public static getEndDate(): Date {
        return FormRepeatManager.getEndDateToMoment().toDate();
    } 
    public static getRepeatEvery(): number {
        return parseInt(FormRepeatManager.object.find("#rep-every").val());
    }
    public static getStartDate(): Date {
        return FormRepeatManager.getStartDateToMoment().toDate();
    }
    public static getEndDateToMoment(): moment.Moment {
        return moment(FormRepeatManager.object.find("#end_datetime").val(), "DD/MM/YYYY")
    }
    public static getStartDateToMoment(): moment.Moment {
        return moment(FormRepeatManager.object.find(".form-control.callendar-date").val(), "DD/MM/YYYY HH:mm")
    }
    public static clicks():void {
        if (FormRepeatManager.click != null)
            FormRepeatManager.click();
    }
    public static getType(): string {
        return FormRepeatManager.object.find("[name='type']:checked").val()
    }
    public static getMonyhlyType(): string {
        return FormRepeatManager.object.find("[name='WeekMonth']:checked").val()
    }
    public static getCheckedDates(): Array<number> {
        var arr = new Array<number>();
        FormRepeatManager.object.find("#daysoftheweek input:checked").each((index, elem) => {
            arr.push(parseInt($(elem).data('numweek')));
        });
        return arr;
    }
    public static getCountMaxRepeats(): number {
        return parseInt(FormRepeatManager.object.find("#maxCountRepeat").val());
    }
}