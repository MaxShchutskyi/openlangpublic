﻿class ClickHandler {
    repeater: AbstractRepeat = null;
    public clickHandlerResult: (repeat: AbstractRepeat) => void;
    iterator: IItereation = null;
    constructor() {
        FormRepeatManager.click = () => this.clickHandler();
    }
    clickHandler() {
        var method = FormRepeatManager.getSelectedTypeMethod();
        var type = FormRepeatManager.getType()
        switch (type) {
            case "after": this.iterator = new ITerationByDate(FormRepeatManager.getStartDateToMoment(), FormRepeatManager.getEndDateToMoment()); break;
            case "max": this.iterator = new MaxLengthIteration(FormRepeatManager.getCountMaxRepeats()); break;
        }
        switch (method) {
            case "daily": this.repeater = new DailyRepeats(this.iterator); break;
            case "every-weekday": {
                this.iterator.groupCount = 5;
                this.repeater = new WeekdayRepeats(this.iterator)
            }; break;
            case "every-mwf": {
                this.iterator.groupCount = 3;
                this.repeater = new EveryMWFRepeat(this.iterator)
            }; break;
            case "every-tt": {
                this.iterator.groupCount = 2;
                this.repeater = new EveryTT(this.iterator)
            }; break;
            case "weekly": {
                this.iterator.groupCount = FormRepeatManager.getCheckedDates().length;
                this.repeater = new WeeklyRepeats(this.iterator);
            }; break;
            case "monthly": {
                if (FormRepeatManager.getMonyhlyType() == "monthly")
                    this.repeater = new MonthlyDayOfTheMonth(this.iterator);
                //else
                //repeat = new MonthlyDayOfTheWeek();
            }; break;
            case "yearly": this.repeater = new YearlyRepeats(this.iterator); break;
        }
        if (this.clickHandlerResult != null)
            this.clickHandlerResult(this.repeater);
    }
}