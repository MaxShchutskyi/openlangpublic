﻿abstract class AbstractRepeat implements IRepeat {
    constructor(iter: IItereation) {
        this.iterator = iter;
    }
    protected iterator: IItereation = null;
    public resultList: Array<CalendatItem> = new Array<CalendatItem>();
    abstract Repeat(): Array<CalendatItem>;
    protected repeatEvery: string = 'day';
    protected every = FormRepeatManager.getRepeatEvery();
    protected getResult(filter: (date: moment.Moment) => boolean): Array<CalendatItem> {
        var curCalItem = FormRepeatManager.getStartDateToMoment();
        this.iterator.iterate(() => {
            curCalItem = moment(curCalItem).add(this.every, this.repeatEvery);
            if (!filter(curCalItem)) return curCalItem;
            var ntcurCalItem = new CalendatItem(null, moment(curCalItem).toDate(), moment(curCalItem).add(1,'hour').toDate());
            ntcurCalItem.week = curCalItem.week() - 1;
            ntcurCalItem.dayOfTheWeek = curCalItem.day() + 1;
            this.resultList.push(ntcurCalItem);
            return curCalItem;
        });
        return this.resultList;
    }
}

//abstract class AbstractRepeat implements IRepeat{
//    public static lastSelectedDate: SelectedDate;
//    public static repeatForm: any;
//    public resultList: Array<CalendatItem> = new Array<CalendatItem>();
//    abstract Repeat(): Array<CalendatItem>;
//    protected repeatEvery: string = 'day';
//    protected getRepeatsEvery() {
//        return parseInt($("#rep-every").val())
//    }
//    protected getStartOn(utc: boolean = false): Date {
//        return utc ? moment.utc().toDate() : new Date();
//    }
//    protected getResult(end: Date, filter: (date: moment.Moment) => boolean, every?: number): Array<CalendatItem> {
//        var curCalItem = AbstractRepeat.lastSelectedDate;
//        every = every == null ? 1 : every;
//        while (true) {
//            var mtStart = moment(curCalItem.start).add(every, this.repeatEvery);
//            curCalItem.start = mtStart.toDate();
//            curCalItem.end = moment(curCalItem.end).add(every, this.repeatEvery).toDate();
//            if (!filter(mtStart)) continue;
//            var ntcurCalItem = new CalendatItem(null, curCalItem.start, curCalItem.end);
//            ntcurCalItem.week = mtStart.week() - 1;
//            ntcurCalItem.dayOfTheWeek = mtStart.day() + 1;
//            this.resultList.push(ntcurCalItem);
//            if (curCalItem.start > end)
//                break;
//        }
//        return this.resultList;
//    }
//}