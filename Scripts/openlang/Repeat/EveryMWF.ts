﻿class EveryMWFRepeat extends AbstractRepeat {
    public Repeat(): Array<CalendatItem> {
        return this.getResult(filter => {
            this.every = 1;
            var numday = filter.day();
            return numday == 1 || numday == 3 || numday == 5;
        });
    }
}