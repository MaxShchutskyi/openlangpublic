﻿class DailyRepeats extends AbstractRepeat {
    constructor(iterator: IItereation = null) {
        super(iterator);
    }
    public Repeat(): Array<CalendatItem> {
        return this.getResult((filter) => { return true });
    }
}