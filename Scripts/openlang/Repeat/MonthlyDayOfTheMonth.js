var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var MonthlyDayOfTheMonth = /** @class */ (function (_super) {
    __extends(MonthlyDayOfTheMonth, _super);
    function MonthlyDayOfTheMonth() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MonthlyDayOfTheMonth.prototype.Repeat = function () {
        this.repeatEvery = 'month';
        return this.getResult(function (filter) { return true; });
    };
    return MonthlyDayOfTheMonth;
}(AbstractRepeat));
//# sourceMappingURL=MonthlyDayOfTheMonth.js.map