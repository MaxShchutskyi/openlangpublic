﻿class MonthlyDayOfTheMonth extends AbstractRepeat {

    public Repeat(): Array<CalendatItem> {
        this.repeatEvery = 'month';
        return this.getResult(filter => true);
    }
}