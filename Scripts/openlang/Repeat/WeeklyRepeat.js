var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var WeeklyRepeats = /** @class */ (function (_super) {
    __extends(WeeklyRepeats, _super);
    function WeeklyRepeats() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    WeeklyRepeats.prototype.Repeat = function () {
        var weekss = new Array();
        var momentEndDate = FormRepeatManager.getEndDateToMoment();
        var repeat = FormRepeatManager.getRepeatEvery();
        var daysoftheweek = FormRepeatManager.getCheckedDates();
        var endWeekNumber = momentEndDate.week() - 1;
        var startWeekNumer = moment(FormRepeatManager.getStartDateToMoment()).week() - 1;
        for (var i = startWeekNumer; i <= endWeekNumber; i += repeat)
            weekss.push(i);
        var res = this.getResult(function (filter) {
            return $.inArray(filter.day(), daysoftheweek) > -1 && $.inArray(filter.week() - 1, weekss) > -1;
        });
        return res;
    };
    return WeeklyRepeats;
}(AbstractRepeat));
//# sourceMappingURL=WeeklyRepeat.js.map