var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var MonthlyDayOfTheWeek = /** @class */ (function (_super) {
    __extends(MonthlyDayOfTheWeek, _super);
    function MonthlyDayOfTheWeek() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MonthlyDayOfTheWeek.prototype.Repeat = function () {
        return null;
        //for (var i = mnt.month() + 1, t = 1; i <= end_date.getMonth() + 1; i++ , t++){
        //    var nn = mnt.add('month', t);
        //    var date = new Date(nn.year(), nn.month() - 1, 0).getDate();
        //    mnt = mnt.add('day', date);
        //    var ntcurCalItem = new CalendatItem(null, mnt.toDate(), mnt.add('hour', 1).toDate());
        //    ntcurCalItem.week = mnt.week() - 1;
        //    ntcurCalItem.dayOfTheWeek = mnt.day() + 1;
        //    this.resultList.push(ntcurCalItem);
        //}
        //return this.resultList;
    };
    MonthlyDayOfTheWeek.prototype.monthDiff = function (d1, d2) {
        var months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getMonth() + 1;
        months += d2.getMonth();
        return months <= 0 ? 0 : months;
    };
    return MonthlyDayOfTheWeek;
}(AbstractRepeat));
//# sourceMappingURL=MonthlyDayOfTheWeek.js.map