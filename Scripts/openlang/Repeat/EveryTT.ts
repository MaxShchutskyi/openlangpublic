﻿class EveryTT extends AbstractRepeat {

    public Repeat(): Array<CalendatItem> {
        return this.getResult(filter => {
            this.every = 1;
            var numday = filter.day();
            return numday == 2 || numday == 4;
        });
    }
}