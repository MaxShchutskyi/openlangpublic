﻿class CalendatItem {
    id: number = null;
    start: Date;
    end: Date;
    public week: Number;
    public dayOfTheWeek: number;
    className: string = "view1";
    constructor(id?: number, start?: Date, end?: Date) {
        this.id = id; this.start = start; this.end = end;
    }
}