var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var WeekdayRepeats = /** @class */ (function (_super) {
    __extends(WeekdayRepeats, _super);
    function WeekdayRepeats() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    WeekdayRepeats.prototype.Repeat = function () {
        this.every = 1;
        return this.getResult(function (filter) {
            var day = filter.day();
            return day != 0 && day != 6;
        });
    };
    return WeekdayRepeats;
}(AbstractRepeat));
//# sourceMappingURL=WeekdayRepeats.js.map