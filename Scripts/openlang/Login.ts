﻿class Login {
    private static loginClass:JQuery;
    static initialize():void {
        if (this.loginClass) return;
        this.loginClass = $(".log_in");
        $("#login").click(this.showLogin);
        this.loginClass.find("#facebook_login").click(this.goToGoogle);
        this.loginClass.find("#google_login").click(this.goToGoogle);
        this.loginClass.find("#LOGIN").click(this.checkExistAccount);
        $("#Logout").click(this.logout);
    }

    private static showLogin() {
        Login.loginClass.show().children().first().animate({ "top": "50%" }, 500);
    }
    private static goToGoogle() {
        location.href = '/Account/GoogleLogin';
        //location.href = '/Account/GoogleLogin' + "?selLang=" + (selLangg ? selLang : "");
    }
    private static goToFacebook() {
        location.href = '/Account/FacebookLogin';
        //location.href = '/Account/GoogleLogin' + "?selLang=" + (selLangg ? selLang : "");
    }

    private static checkExistAccount() {
        var name = $("#name").val();
        var pass = $("#pass").val();
        $.ajax({
            url: "/Account/CheckExistUser",
            type: "post",
            data: { 'name': name, 'password': pass },
            async: true,
            success:(result)=> {
                if (result === "True")
                    document.forms["formLogin"].submit();
                else
                    $("#name,#pass").css("border-color", "red");

            },
            error:()=> {
                alert('Error occured');
            }
        });
    }

    private static  logout() {
        RegistratedUser.dispose();
        sessionStorage.removeItem('token');
        location.href = 'Openlang/Logout';
    }
}