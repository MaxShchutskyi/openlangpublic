﻿/// <reference path="../typings/jquery/jquery.d.ts" />
class LogIn {
    public LogBox: JQuery;
    public PopUp: JQuery;
    public IsVisible:boolean;
    public Show(): void {
        this.ShowBackground();
        this.ClickOutBox();
        this.IsVisible = true;
    }
    constructor(cover:string, box:string) {
        if (this.LogBox == null || this.PopUp == null) {
            this.LogBox = $(cover);
            this.PopUp = this.LogBox.find(box);
        }
    }
    public Close() {
        this.LogBox.unbind("mouseup");
        this.AnimatePupUp(false);
        this.IsVisible = false;
    }

    private ShowBackground(): void {
        this.LogBox.css("display", "block");
        this.AnimatePupUp(true);
    }

    private AnimatePupUp(show: boolean = true): void {
        if (show) {
            this.PopUp.animate({ "top": "50%" }, 500);
            return;
        }
        this.PopUp.animate({ "top": "-100%" }, 500, () => {
            this.LogBox.css("display", "none");
        });
    }

    public  ClickOutBox(): void {
        //this.LogBox.unbind("mouseup").bind("mouseup", e => {
        //    if ($(this.PopUp).has(e.target).length === 0) {
        //        this.Close();
        //    }
        //});
    }
} 