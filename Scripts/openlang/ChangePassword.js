var ChangePasswordProvider = /** @class */ (function () {
    function ChangePasswordProvider() {
        this.checkState = "<span class=\"check_state\"><i class=\"fa fa-times error\"></i><i class=\"fa fa-check correct\"></i></span>";
    }
    ChangePasswordProvider.prototype.initialize = function (oldpass, newpass, confirmpass) {
        this.newPass = newpass;
        this.oldPass = oldpass;
        this.confirnPass = confirmpass;
        this.newPass.after(this.checkState);
        this.confirnPass.after(this.checkState);
    };
    ChangePasswordProvider.prototype.initializeSates = function (regExpValid) {
        if (regExpValid === void 0) { regExpValid = null; }
        this.bindChanges();
        if (regExpValid != null) {
            this.regExpressionValidate = new RegExp(regExpValid);
            this.regExpValidation();
        }
    };
    ChangePasswordProvider.prototype.bindChanges = function () {
        var _this = this;
        this.confirnPass.keyup(function () {
            if (!_this.confirnPass.val()) {
                _this.confirnPass.next().children().hide();
                return;
            }
            if (_this.confirnPass.val() === _this.newPass.val()) {
                _this.confirnPass.next().find("i.correct").show().parent().find("i.error").hide();
            }
            else {
                _this.confirnPass.next().find("i.error").show().parent().find("i.correct").hide();
            }
        });
    };
    ChangePasswordProvider.prototype.regExpValidation = function () {
        var _this = this;
        this.newPass.keyup(function () {
            if (!_this.newPass.val()) {
                _this.newPass.next().children().hide();
                _this.confirnPass.trigger("keyup");
                return;
            }
            if (_this.regExpressionValidate.test(_this.newPass.val())) {
                _this.newPass.next(".check_state").find("i.correct").show().parent().find("i.error").hide();
                _this.confirnPass.trigger("keyup");
            }
            else {
                _this.newPass.next(".check_state").find("i.error").show().parent().find("i.correct").hide();
                _this.confirnPass.trigger("keyup");
            }
        });
    };
    ChangePasswordProvider.prototype.changePassword = function () {
        var _this = this;
        $.ajax({
            url: "/api/Password/ChangePassword",
            type: "post",
            beforeSend: function (xhr) { return BeforeSendAuthorizeApi.beforeSend(xhr); },
            data: {
                OldPass: this.oldPass.val(),
                NewPass: this.newPass.val(),
                ConfirmPass: this.confirnPass.val()
            },
            success: function (f) {
                $("#updatedPassword").fadeIn("slow", function () {
                    setTimeout(function () {
                        $("#updatedPassword").fadeOut("slow", function () {
                            AlignHeight();
                        });
                    }, 5000);
                }).css('display', 'flex');
                AlignHeight();
                //Messager.show("PasswordUpdate");
                _this.oldPass.val("");
                _this.newPass.val("");
                _this.confirnPass.val("");
                _this.newPass.trigger("keyup");
            },
            error: function (f) {
                $("#uncorrectPassword").fadeIn("slow", function () {
                    setTimeout(function () {
                        $("#uncorrectPassword").fadeOut("slow");
                    }, 5000);
                }).css("display", "flex");
                //Messager.show("UncorrectPassword");
                //alert(JSON.parse(f.responseText).Message);
            }
        });
    };
    return ChangePasswordProvider;
}());
//# sourceMappingURL=ChangePassword.js.map