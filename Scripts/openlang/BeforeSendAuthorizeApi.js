var BeforeSendAuthorizeApi = /** @class */ (function () {
    function BeforeSendAuthorizeApi() {
    }
    BeforeSendAuthorizeApi.beforeSend = function (xhr) {
        return xhr.setRequestHeader("Authorization", "Bearer " + sessionStorage.getItem('token'));
    };
    return BeforeSendAuthorizeApi;
}());
//# sourceMappingURL=BeforeSendAuthorizeApi.js.map