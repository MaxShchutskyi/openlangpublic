﻿var offerOptions = {
    offerToReceiveAudio: 1,
    offerToReceiveVideo: 1
};
var sdpConstraints = {
    'mandatory': {
        'OfferToReceiveAudio': true,
        'OfferToReceiveVideo': true
    }
};
var stuns;
var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
var gainNode = audioCtx.createGain();
var peerConnection = window.RTCPeerConnection || window.webkitRTCPeerConnection ||
    window.mozRTCPeerConnection ||
    window.msRTCPeerConnection;
var IceCandidate = RTCIceCandidate || window.mozRTCIceCandidate || window.RTCIceCandidate;
var videoChat = $.connection.videoChatHub;
var paramName = getParameterByName("offer");
var clientId = getParameterByName("clientid");
var froms;
var localVideo = document.getElementById('local');
var remoteVideo = document.getElementById('remote');
var answerRecived = false;
var notSendingCandidates = [];
var pc;
videoChat.client.messages = function (from, message) {
    var mess = JSON.parse(message);
    console.log("CONSOLE MESSAGE:");
    console.dir(message);
    if (mess.type == 'offer') {
        froms = from;
        $(".center-video-img").css("display", "none");
        pc.setRemoteDescription(new RTCSessionDescription(JSON.parse(mess.message)), function () { answerRecived = true }, function (err) { console.log("Error RemoteDescription") });

        if (!paramName) {
            console.log("Sending Answer");
            answerPeer();
        }


    } else if (mess.type == 'answer') {
        pc.setRemoteDescription(new RTCSessionDescription(JSON.parse(mess.message)), function () { answerRecived = true }, function (err) { console.log("Error RemoteDescription") });
    } else if (mess.type == 'videoEnabled') {
        remote.style.display = "block";
        $(".center-video-img").css("display", "none");
    }
    else if (mess.type == 'videoDisabled') {
        remote.style.display = "none";
        $(".center-video-img").css("display", "block");
    } else if (mess.type == 'iceCandidate') {
        if (!answerRecived) {
            notSendingCandidates.push(JSON.parse(JSON.parse(message).message));
            return;
        }
        if (notSendingCandidates.length)
            notSendingCandidates.forEach(function (el) {
                pc.addIceCandidate(new IceCandidate(el));
            });
        notSendingCandidates = [];
        console.log("Get Ice Candidate");
        var t = JSON.parse(JSON.parse(message).message);
        //var cand = {      type: 'candidate',
        //    label: t.candidate.sdpMLineIndex,
        //    id: t.candidate.sdpMid,
        //    candidate: t.candidate.candidate
        //}
        pc.addIceCandidate(new IceCandidate(t));
    }

};

function answerPeer() {
    pc.createAnswer(function (answer) {
        pc.setLocalDescription(answer);
        sendMessage("answer", JSON.stringify(answer));
    },
        function (err) { console.log("Sending Answer Error: " + err) }, sdpConstraints
    );
}

function sendMessage(type, message) {
    console.log("Sending Message");
    videoChat.server.sendMessage(JSON.stringify({
        "type": type,
        "message": message
    }),
        id,
        clientId ? clientId : froms);
}

function createPeerConnection(stream) {
    pc = new peerConnection({ 'iceServers': STUN });
    pc.id = id;
    pc.addStream(stream);
    console.dir(pc);

    pc.onicecandidate = function (evt) {
        if (evt.candidate == null) return;
        //pc.onicecandidate = null;

        console.log("Send Ice Candidate");
        sendMessage("iceCandidate", JSON.stringify(evt.candidate));
    };

    pc.onaddstream = function (evt) {
        console.log("An add stream");
        remoteVideo.src = window.URL.createObjectURL(evt.stream);
    };
    pc.oniceconnectionstatechange = function (e) {
        if (pc.iceConnectionState === 'disconnected') {
            $("#end-call").click();
        }
    };
}

function callPeer() {

    pc.createOffer(function (offer) {
        pc.setLocalDescription(offer,
            function () {
                sendMessage("offer", JSON.stringify(offer));
            });
        console.log("Send Offer");
    },
        function (err) { console.log("Offer Error: " + err) }, offerOptions
    ).then(function () {

    });

}
$.connection.hub.start()
.done(done);
function done() {
    $("#videocall").click(function () {
        var mess = $(".mess.active");
        clientId = mess.attr("data-partnerid");
        var name = mess.find(".patnerName text").text();
        var img = mess.find(".media-left img").attr("src");
        $(".center-video-img img").attr("src", img).next().text(name).next().text("You are calling...").parent().css("display", "block");
        if (!clientId) {
            alert("You need select a dialog");
            return;
        }
        if (!mobDev) {
            $('.inner-video-chat').animate({
                right: '0'
            }, 700);
            $('.chat-main, .chat-header')
                .animate({
                    width: '66%'
                },
                    700);
        } else {
            $('.video-chat').show();
            $('.inner-video-chat').animate({
                right: '0',
                width: '100%'
            }, 700);
        }

        navigator.getMedia = (
            navigator.getUserMedia ||
                navigator.webkitGetUserMedia ||
                navigator.mozGetUserMedia ||
                navigator.msGetUserMedia
        );
        chat.server.inviteToVideoChat(clientId);
        call();
    });
}

function call() {
    if (localstream != null) {
        if (paramName) {
            callPeer();
        }
        return;
    }
    navigator.getMedia(
        { video: true, audio: true },
        function (stream) {
            localVideo.src = window.URL.createObjectURL(stream);
            console.log("Add Stream");
            localstream = stream;
            localVideo.volume = 0;
            createPeerConnection(stream);
            if (paramName) {
                callPeer();
            }

        },
        function (err) {
            console.log("The following error occured: ");
            console.dir(err);
        }
    );
}

var localstream;
//$("#disableVideo").click(function () {
//    localstream.getVideoTracks()[0].enabled = false;
//});
$("#disableVideo").click(function () {
    var ss = localstream.getVideoTracks()[0];
    ss.enabled = !ss.enabled;
    if (ss.enabled)
        sendMessage("videoEnabled", null);
    else
        sendMessage("videoDisabled", null);
});

$("#muteAudio").click(function () {
    var rt = localstream.getAudioTracks()[0];
    rt.enabled = !rt.enabled;
});
//$("#unmuteAudio").click(function () {
//    localstream.getAudioTracks()[0].enabled = true;
//});
$("#end-call").click(function () {
    if (!localstream) {
        $(".close-video").click();
        return;
    }
    if (clientId) {
        videoChat.server.endVideoChat(clientId);
        clientId = null;
    }
    localstream.getAudioTracks()[0].enabled = false;
    localstream.getVideoTracks()[0].enabled = false;
    localstream.getAudioTracks()[0].stop();
    localstream.getVideoTracks()[0].stop();

    //синулокс инъекционный
    //499-44-44
    //пулковская 11-1
    remote.src = "";
    pc = null;
    localstream = null;
    clientId = null;
    paramName = null;
    waitingFor = false;
    $(".close-video").click();
    document.getElementById("call").pause();
});
videoChat.client.endCall = function () {
    $("#end-call").click();
    document.getElementById("call").pause();
}
$("#reject-call").click(function () {
    paramName = null;
    $("#end-call").click();
    videoChat.server.endVideoChat(clientId);
    clientId = null;
    waitingFor = false;
    document.getElementById("call").pause();
});
$("#confirm-call").click(function () {
    $("#reject-call,#confirm-call ").css("display", "none");
    $(".center-video-img").css("display", "none");
    $(".bottom-video").css("display", "block");
    document.getElementById("call").pause();
    call();
});
//$("#remote").dblclick(function() {
//    this.webkitEnterFullScreen();
//});
