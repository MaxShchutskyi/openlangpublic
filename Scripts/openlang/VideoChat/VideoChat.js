﻿navigator.getUserMedia = navigator.getUserMedia ||
                         navigator.webkitGetUserMedia ||
                         navigator.mozGetUserMedia;
var offerOptions = {
    offerToReceiveAudio: 1,
    offerToReceiveVideo: 1
};
var sdpConstraints = {
    'mandatory': {
        'OfferToReceiveAudio': true,
        'OfferToReceiveVideo': true
    }
};
var stuns;

//var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
//var audioCtx = new window.AudioContext();

var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
//var audioCtx = new window.AudioContext();

var gainNode = audioCtx.createGain();
var peerConnection = RTCPeerConnection;
var IceCandidate = RTCIceCandidate;
var videoChat = $.connection.videoChatHub;
//var paramName = getParameterByName("offer");
var paramName;
var clientId = getParameterByName("clientid");
var froms;
var localVideo;
var remoteVideo;
var answerRecived = false;
var notSendingCandidates = [];
var pc;
videoChat.client.messages = function (from, message) {
    var mess = JSON.parse(message);
    console.log("CONSOLE MESSAGE:");
    console.dir(message);
    if (mess.type == 'offer') {
        froms = from;
            $(".center-video-img").css("opacity", "1");
        pc.setRemoteDescription(new RTCSessionDescription(JSON.parse(mess.message)), function () { answerRecived = true }, function (err) { console.log("Error RemoteDescription") });

        if (!paramName) {
            console.log("Sending Answer");
            answerPeer();
        }


    } else if (mess.type == 'answer') {
        pc.setRemoteDescription(new RTCSessionDescription(JSON.parse(mess.message)), function () { answerRecived = true }, function (err) { console.log("Error RemoteDescription") });
    } else if (mess.type == 'videoEnabled') {
        remote.style.opacity = "1";
        $(".center-video-img").css("display", "none");
        //$(".cur-user video").css("opacity", "1");
        //$(".center-video-img").css("opacity", "0");
    }
    else if (mess.type == 'videoDisabled') {
        remote.style.opacity = "0";
        $(".center-video-img").css("display", "block");
        //$(".cur-user video").css("opacity", "0");
        //$(".center-video-img").css("opacity", "1");
    } else if (mess.type == 'iceCandidate') {
        if (!answerRecived) {
            notSendingCandidates.push(JSON.parse(JSON.parse(message).message));
            return;
        }
        if (notSendingCandidates.length)
            notSendingCandidates.forEach(function (el) {
                pc.addIceCandidate(new IceCandidate(el));
            });
        notSendingCandidates = [];
        console.log("Get Ice Candidate");
        var t = JSON.parse(JSON.parse(message).message);
        //var cand = {      type: 'candidate',
        //    label: t.candidate.sdpMLineIndex,
        //    id: t.candidate.sdpMid,
        //    candidate: t.candidate.candidate
        //}
        pc.addIceCandidate(new IceCandidate(t));
    }

};

function answerPeer() {
    pc.createAnswer(function (answer) {
        pc.setLocalDescription(answer, function () {
            sendMessage("answer", JSON.stringify(answer));
        }, function (err) {
            console.log(err);
        });
    },
        function (err) { console.log("Sending Answer Error: " + err) }, sdpConstraints
    );
}

function sendMessage(type, message) {
    console.log("Sending Message");
    videoChat.server.sendMessage(JSON.stringify({
        "type": type,
        "message": message
    }),
        id,
        clientId ? clientId : froms);
}

function createPeerConnection(stream) {
    //var servers = createIceServers('stun:stun01.sipphone.com', 'stun:stun.ekiga.net', 'stun:stun.fwdnet.net', 'stun:stun.ideasip.com', 'stun:stun.iptel.org', 'stun:stun.rixtelecom.se', 'stun:stun.l.google.com:19302', 'stun:stun1.l.google.com:19302', 'stun:stun2.l.google.com:19302', 'stun:stun3.l.google.com:19302', 'stun:stun4.l.google.com:19302', 'stun:stunserver.org');
    //console.log(servers);
    pc = new window.RTCPeerConnection({ 'iceServers': STUN });
    pc.id = id;
    pc.addStream(stream);
    console.dir(pc);

    pc.onicecandidate = function (evt) {
        if (evt.candidate == null) return;
        //pc.onicecandidate = null;

        console.log("Send Ice Candidate");
        sendMessage("iceCandidate", JSON.stringify(evt.candidate));
    };

    pc.onaddstream = function (evt) {
        console.log("An add stream");
        if (remoteVideo)
            remoteVideo = document.getElementById('remote');
        remoteVideo.src = window.URL.createObjectURL(evt.stream);
        $(".center-video-img").css("display", "none");
        document.getElementById("call").pause();

    };
    pc.oniceconnectionstatechange = function(e) {
        if (pc.iceConnectionState === 'disconnected') {
            console.log("END CALL!!!");
            $("#end-call,#end-call2").click();
        }
    };
}
function createDownloadLink(anchorSelector, str, fileName) {
    if (window.navigator.msSaveOrOpenBlob) {
        var fileData = [str];
        blobObject = new Blob(fileData);
        $(anchorSelector).click(function () {
            window.navigator.msSaveOrOpenBlob(blobObject, fileName);
        });
    } else {
        var url = "data:text/plain;charset=utf-8," + encodeURIComponent(str);
        $(anchorSelector).attr("download", fileName);
        $(anchorSelector).attr("href", url);
    }
}

function callPeer() {

    pc.createOffer(function (offer) {
        pc.setLocalDescription(offer,
            function () {
                sendMessage("offer", JSON.stringify(offer));
            }, function (err) {
                console.log(err);
            });
        console.log("Send Offer");
    },
        function (err) { console.log("Offer Error: " + err) }, offerOptions
    ).then(function () {

    });

}
$.connection.hub.start()
.done(function() { });
//function done() {
//    $("#videocall").click();
//}
var chat;
function videoCall(clientIdd, hub, currentId) {
        console.log(currentId);
        id = currentId;
        clientId = clientIdd;
        localVideo = document.getElementById('local');
        remoteVideo = document.getElementById('remote');
        chat = hub;
        console.log("videCALLL!!!!")
        //var mess = $(".mess.active");
        //clientId = $(this).attr("data-client");
        //var name = mess.find(".patnerName text").text();
        //var img = mess.find(".media-left img").attr("src");
        //$(".center-video-img img").attr("src", img).next().text(name).next().text("You are calling...").parent().css("display","block");
        //if (!clientId) {
        //    alert("You need select a dialog");
        //    return;
    //}
        document.getElementById("call").play();
            chat.server.inviteToVideoChat(clientId);
            call(false);
}

function call(withVideo) {
    console.log("call");
    if (localstream != null) {
        if (paramName) {
            callPeer();
        }
        return;
    }
    AdapterJS.webRTCReady(function (isUsingPlugin) {
        navigator.getUserMedia({ video: true, audio: true },
        function (stream) {
            localVideo.src = window.URL.createObjectURL(stream);
            console.log("Add Stream");
            localstream = stream;
            localVideo.volume = 0;
            if (!withVideo)
                disableVideo();
            createPeerConnection(stream);
            if (paramName) {
                callPeer();
            }

        },
        function (err) {
            console.log(err);
            alert("You have no video camera or microhone. Or your browser has't access to devices");
        })
    });
    //navigator.getMedia(

    //);
}

var localstream;
//$("#disableVideo").click(function () {
//    localstream.getVideoTracks()[0].enabled = false;
//});
function disableVideo() {
    var ss = localstream.getVideoTracks()[0];
    ss.enabled = !ss.enabled;
    if (ss.enabled)
        sendMessage("videoEnabled", null);
    else
        sendMessage("videoDisabled", null);
}
//$("#disableVideo").click(function () {
//    var ss = localstream.getVideoTracks()[0];
//    ss.enabled = !ss.enabled;
//    if(ss.enabled)
//        sendMessage("videoEnabled", null);
//    else
//        sendMessage("videoDisabled", null);
//});
function mute() {
    var rt = localstream.getAudioTracks()[0];
    rt.enabled = !rt.enabled;
}
$("#muteAudio").click(function () {

});
//$("#unmuteAudio").click(function () {
//    localstream.getAudioTracks()[0].enabled = true;
//});
function endCall() {
    console.log(clientId + "ENDCALL");
    if (clientId) {
        videoChat.server.endVideoChat(clientId);
        clientId = null;
    }
    if (!localstream) {
        $(".close-video").click();
        return;
    }
    localstream.getAudioTracks()[0].enabled = false;
    localstream.getVideoTracks()[0].enabled = false;
    localstream.getAudioTracks()[0].stop();
    localstream.getVideoTracks()[0].stop();

    //синулокс инъекционный
    //499-44-44
    //пулковская 11-1
    remote.src = "";
    pc = null;
    localstream = null;
    clientId = null;
    paramName = null;
    waitingFor = false;
    $(".close-video").click();
    document.getElementById("call").pause();
}
//$("#end-call").click(function () {
//    if (!localstream) {
//        $(".close-video").click();
//        return;
//    }
//    if (clientId) {
//        videoChat.server.endVideoChat(clientId);
//        clientId = null;
//    }
//    localstream.getAudioTracks()[0].enabled = false;
//    localstream.getVideoTracks()[0].enabled = false;
//    localstream.getAudioTracks()[0].stop();
//    localstream.getVideoTracks()[0].stop();

//    //синулокс инъекционный
//    //499-44-44
//    //пулковская 11-1
//    remote.src = "";
//    pc = null;
//    localstream = null;
//    clientId = null;
//    paramName = null;
//    waitingFor = false;
//    $(".close-video").click();
//    document.getElementById("call").pause();
//});
videoChat.client.endCall = function () {
    console.log("END CALL!!!");
    $("#end-call,#end-call2").click();
    document.getElementById("call").pause();
}
$("#reject-call").click(function () {
    console.log("END CALL!!!");
    paramName = null;
    $("#end-call,#end-call2").click();
    videoChat.server.endVideoChat(clientId);
    clientId = null;
    waitingFor = false;
    //document.getElementById("call").pause();
});
function confirmAnswer(ids, withVideo) {
    if (!localVideo)
        localVideo = document.getElementById('local');
    remoteVideo = document.getElementById('remote');
    id = ids;
    call(withVideo);
    console.log("confirm-call");
    document.getElementById("call").pause();
}
$("#confirm-call").click(function () {
    //$("#reject-call,#confirm-call ").css("display", "none");
    //$(".center-video-img").css("display","none");
    //$(".bottom-video").css("display", "block");
    //document.getElementById("call").pause();

});
//$("#remote").dblclick(function() {
//    this.webkitEnterFullScreen();
//});
