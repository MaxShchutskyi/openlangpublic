var Validator = /** @class */ (function () {
    function Validator() {
    }
    Validator.validate = function (items, success, invalid) {
        var _this = this;
        var inputs = items.find("input");
        var required = inputs.filter("[required]");
        var pattern = inputs.filter("[pattern]");
        required.each(function (index, elem) {
            _this.required(elem) ? success(elem) : invalid(elem, "Not required");
        });
        pattern.each(function (index, element) {
            _this.pattern(element) ? success(element) : invalid(element, "Pattern not match");
        });
    };
    Validator.required = function (elem) {
        return $(elem).val();
    };
    Validator.pattern = function (elem) {
        var pat = $(elem).attr("pattern");
        return $(elem).val().match(pat);
    };
    return Validator;
}());
//# sourceMappingURL=Validator.js.map