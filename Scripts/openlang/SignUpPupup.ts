﻿/// <reference path="../typings/jquery.validation/jquery.validation.d.ts" />
/// <reference path="loguppopup.ts" />
declare function startLoader(): void;
declare function stopLoader(): void;
class NewUserUser {
    public Name: string;
    public Email: string;
    public Password: string;
    public ConfirmPassword: string;
}

class SignUpPupup extends LogIn {
    private PopUpStep1SingUp: JQuery;
    public SuccessAllParams: (User: NewUserUser) => void;
    constructor() {
        super(".sing_in", ".pop_up_sing_in");
        this.PopUp.find("#SingUpNow").unbind("click").bind("click", () => this.ClickBtSignIn(this.PopUp));
        this.PopUpStep1SingUp = this.PopUp.siblings(".pop_up_trial.step_1");
        this.PopUpStep1SingUp.find("#go_to_step_22").click(() => this.ClickBtSignInStep(this.PopUpStep1SingUp));
        this.PopUpStep1SingUp.find("#close_trial_step5").click(() => this.ClickBtCloseStep1(this.PopUpStep1SingUp));
    }
    public clickToIhaveAccount(logup: LogUpPopup): void {
        $("#iHaveAccount").unbind("click").bind("click", (e) => {
            this.PopUp.animate({ 'top': '-50%' }, 350, () => {
                localStorage.removeItem("prePopup");
                this.PopUp.parent().hide();
                logup.Show();
            });
            //this.Close();
        });
    }

    private ClickBtCloseStep1(step: JQuery): void {
        step.animate({ top: "-100%" }, 500, () => {
            this.LogBox.unbind("mouseup");
            this.LogBox.css("display", "none");
        });
        localStorage.removeItem("prePopup");
        this.SuccessAllParams = null;
    }

    private ClickBtSignInStep(pop: JQuery): void {
        var User = new NewUserUser();
        User.Name = pop.find("#name").val();
        User.Email = pop.find("#email").val();
        User.Password = pop.find("#pass").val();
        User.ConfirmPassword = pop.find("#conf_pass").val();
        pop.animate({ top: "-100%" }, 500, () => {
            this.LogBox.unbind("mouseup");
            this.LogBox.css("display", "none");
        });
        if (this.SuccessAllParams != null)
            this.SuccessAllParams(User);
        this.SuccessAllParams = null;
    }

    private ClickBtSignIn(pop: JQuery): void {
        var flag = true;
        var name = pop.find("#name");
        var email = pop.find("#email");
        var pass = pop.find("#pass");
        var confPass = pop.find("#conf_pass");
        if (!name.val()) {
            name.css("border-color", "red");
            $("#notEnterName").fadeIn("slow");
            flag = false;
        } else {
            name.css("border-color", "#ccc");   
            $("#notEnterName").fadeOut("slow");
        }
        if (!email.val() || !email.val().match(Patterns.email)) {
            email.css("border-color", "red");
            $("#notEnterEmail").fadeIn("slow");
            flag = false;
        } else {
            $("#notEnterEmail").fadeOut("slow");
            email.css("border-color", "#ccc");   
        }
        if (!pass.val()) {
            $("#notEnterPass").fadeIn("slow");
            pass.css("border-color", "red");
            flag = false;
        } else {
            $("#notEnterPass").fadeOut("slow");
            if (!pass.val().match(Patterns.password)) {
                $("#shortPass").fadeIn("slow");
                flag = false;
            } else {
                $("#shortPass").fadeOut("slow");
                pass.css("border-color", "#ccc");
                if (!confPass.val() || !(confPass.val() === pass.val())) {
                    confPass.css("border-color", "red");
                    $("#notMatchPass").fadeIn("slow");
                    flag = false;
                } else {
                    confPass.css("border-color", "#ccc");
                    $("#notMatchPass").fadeOut("slow");
                }
            }
        }
        if (!flag)
            return;
        $.ajax({
            url: "/Openlang/ChackExistUser",
            type: "post",
            data: { email: email.val() },
            success: (result) => {
                if (result.result) {
                    pop.animate({ top: "-100%" }, 500);
                    $("#emailExists").fadeOut("slow");
                    $("input#email").css("border", "1px solid red");
                    this.LogBox.unbind("mouseup");
                    pop.siblings(".pop_up_trial.step_1").animate({ top: "50%" }, 500);
                } else {
                    $("#emailExists").fadeIn("slow");
                    $("input#email").css("border", "1px solid red");
                    stopLoader();
                }
            },
            error: () => {
        }
        });
    }

}