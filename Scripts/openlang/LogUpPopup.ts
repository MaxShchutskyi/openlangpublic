﻿/// <reference path="signuppupup.ts" />

class LogUpPopup extends LogIn {
    FogotPassPopup: JQuery;
    CompletePass:JQuery;
    constructor() {
        super(".log_in", ".pop_up_log_in");
        var self = this;
        this.PopUp.find("#fogot_password").unbind("click").bind("click", this.FogotPawwsordClick.bind(self));
        this.FogotPassPopup = this.LogBox.find(".resetPassword");
        this.FogotPassPopup.find(".go_to_login").unbind("click").bind("click", this.CobeBackToLigin.bind(self));
        this.FogotPassPopup.find("#reset_complete").unbind("click").bind("click", this.ConfirmReset.bind(self));
        this.CompletePass = this.LogBox.find(".thanks.reset_psw");
        this.CompletePass.find("#close_trial_step_thanks_reset").unbind("click").bind("click", this.CLoseLastWindow.bind(self));
        this.PopUp.find("input").keypress((e)=> {
            if (e.which === 13) {
                $('#LOGIN').click();
                return false;    //<---- Add this line
            }
        });
    }

    private CobeBackToLigin() {
        this.FogotPassPopup.animate({ "top": "-100%" }, 500);
        this.PopUp.animate({ "top": "50%" }, 500);
    }

    private CLoseLastWindow() {
        this.CompletePass.animate({ "top": "-100%" }, 500, ()=>this.LogBox.hide());
    }

    private FogotPawwsordClick():void {
        this.LogBox.unbind("mouseup");
        this.PopUp.animate({"top":"-100%"}, 500);
        this.FogotPassPopup.animate({ "top": "50%" }, 500);
    }

    private ConfirmReset(): void {
        var email = $("#email_fogot").val();
        var telephone: any = $("#tel_number2_fogot");
        var tel = telephone.intlTelInput("getNumber");
        if ((!email && !tel) || (email && tel)) {
            //alert("You must select only one way");
            $("#youMustSelectOneWay").fadeIn("slow");
            return;
        }
        $("#youMustSelectOneWay").fadeOut("slow");
        if (!tel && !email.match(Patterns.email)) {
            $("#incorrectResetPassword").fadeIn("slow");
            return;
        }
        $("#incorrectResetPassword").fadeOut("slow");
        $.ajax({
                context:this,
                url: "/Account/FogotPassword",
                type: "post",
                data: { 'email': email, 'telephone': tel },
                async: true,
                success(result) {
                    if (result === "True") {
                        alert(result);
                    } else {
                        $("#email_fogot").css("border-color", "red");
                        console.log(result);
                    }
                },
                error(e) {
                    //alert('Error occured');
                }
            });
            this.FogotPassPopup.animate({ "top": "-100%" }, 500);
            this.CompletePass.animate({ "top": "50%" }, 500).find("#resetMathod").text(email?"Email":"Telephone");

    }

    public CreateNewAccountEvent(singIn: SignUpPupup) {
        this.PopUp.find("#CreateNewAccount").click(() => {
            this.PopUp.animate({'top':"-50%"},350, () => {
                this.PopUp.parent().hide();
                singIn.Show();
            });
            //this.Close();
            //singIn.Show();
        });
    }
}