var ImageForMessage = /** @class */ (function () {
    function ImageForMessage(path, index, message) {
        this.path = path;
        this.index = index;
        this.message = message;
    }
    ImageForMessage.initImages = function () {
        this.imageCollection = [];
        this.imageCollection.push(new ImageForMessage("/Images/openlang/forMessage/sendEmail.jpg", "SendEmail", "Message was sent successful"));
        this.imageCollection.push(new ImageForMessage("/Images/openlang/forMessage/forApplication.jpg", "Application", "Your application was sent successful"));
        this.imageCollection.push(new ImageForMessage("/Images/openlang/forMessage/alreadyVouted.jpg", "AlreadyVouted", "You have already voted on this issue"));
        this.imageCollection.push(new ImageForMessage("/Images/openlang/forMessage/forRequest.jpg", "Request", "Your request was sent successful"));
        this.imageCollection.push(new ImageForMessage("/Images/openlang/forMessage/LanguagesUpdated.jpg", "LanguageUpdate", "Languages updated"));
        this.imageCollection.push(new ImageForMessage("/Images/openlang/forMessage/PasswordUpdated.jpg", "PasswordUpdate", "Password updated"));
        this.imageCollection.push(new ImageForMessage("/Images/openlang/forMessage/success.jpg", "Success", "Success"));
        this.imageCollection.push(new ImageForMessage("/Images/openlang/forMessage/uncorrectPassword.jpg", "UncorrectPassword", "Incorrect password"));
        this.imageCollection.push(new ImageForMessage("/Images/openlang/forMessage/profileUpdate.jpg", "ProfileUpdate", "Profie updated"));
    };
    ImageForMessage.getImage = function (index) {
        if (!this.imageCollection)
            this.initImages();
        for (var i = 0; i < this.imageCollection.length; i++) {
            if (this.imageCollection[i].index === index)
                return this.imageCollection[i];
        }
        return null;
    };
    return ImageForMessage;
}());
var Messager = /** @class */ (function () {
    function Messager() {
    }
    Messager.show = function (index) {
        if (!this.messeger)
            this.messeger = $(".messeger");
        var imgForMess = ImageForMessage.getImage(index);
        this.messeger.find(".icon img").attr("src", imgForMess.path);
        this.messeger.find(".textMessage").text(imgForMess.message);
        this.messeger.css("display", "block").children(".message").animate({ 'top': '50%' }, 500);
    };
    Messager.close = function () {
        var _this = this;
        this.messeger.children(".message").animate({ 'top': '-100%' }, 500, function () {
            _this.messeger.css("display", "none");
        });
    };
    return Messager;
}());
//# sourceMappingURL=Messager.js.map