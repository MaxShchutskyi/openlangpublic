﻿class BeforeSendAuthorizeApi {
    public static beforeSend(xhr: JQueryXHR): any {
        return xhr.setRequestHeader("Authorization", "Bearer " + sessionStorage.getItem('token'));
    }
}