var Login = /** @class */ (function () {
    function Login() {
    }
    Login.initialize = function () {
        if (this.loginClass)
            return;
        this.loginClass = $(".log_in");
        $("#login").click(this.showLogin);
        this.loginClass.find("#facebook_login").click(this.goToGoogle);
        this.loginClass.find("#google_login").click(this.goToGoogle);
        this.loginClass.find("#LOGIN").click(this.checkExistAccount);
        $("#Logout").click(this.logout);
    };
    Login.showLogin = function () {
        Login.loginClass.show().children().first().animate({ "top": "50%" }, 500);
    };
    Login.goToGoogle = function () {
        location.href = '/Account/GoogleLogin';
        //location.href = '/Account/GoogleLogin' + "?selLang=" + (selLangg ? selLang : "");
    };
    Login.goToFacebook = function () {
        location.href = '/Account/FacebookLogin';
        //location.href = '/Account/GoogleLogin' + "?selLang=" + (selLangg ? selLang : "");
    };
    Login.checkExistAccount = function () {
        var name = $("#name").val();
        var pass = $("#pass").val();
        $.ajax({
            url: "/Account/CheckExistUser",
            type: "post",
            data: { 'name': name, 'password': pass },
            async: true,
            success: function (result) {
                if (result === "True")
                    document.forms["formLogin"].submit();
                else
                    $("#name,#pass").css("border-color", "red");
            },
            error: function () {
                alert('Error occured');
            }
        });
    };
    Login.logout = function () {
        RegistratedUser.dispose();
        sessionStorage.removeItem('token');
        location.href = 'Openlang/Logout';
    };
    return Login;
}());
//# sourceMappingURL=Login.js.map