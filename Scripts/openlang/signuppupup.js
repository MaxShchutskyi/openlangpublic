var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <reference path="../typings/jquery.validation/jquery.validation.d.ts" />
/// <reference path="loguppopup.ts" />
var NewUserUser = /** @class */ (function () {
    function NewUserUser() {
    }
    return NewUserUser;
}());
var SignUpPupup = /** @class */ (function (_super) {
    __extends(SignUpPupup, _super);
    function SignUpPupup() {
        var _this = _super.call(this, ".sing_in", ".pop_up_sing_in") || this;
        _this.PopUp.find("#SingUpNow").unbind("click").bind("click", function () { return _this.ClickBtSignIn(_this.PopUp); });
        _this.PopUpStep1SingUp = _this.PopUp.siblings(".pop_up_trial.step_1");
        _this.PopUpStep1SingUp.find("#go_to_step_22").click(function () { return _this.ClickBtSignInStep(_this.PopUpStep1SingUp); });
        _this.PopUpStep1SingUp.find("#close_trial_step5").click(function () { return _this.ClickBtCloseStep1(_this.PopUpStep1SingUp); });
        return _this;
    }
    SignUpPupup.prototype.clickToIhaveAccount = function (logup) {
        var _this = this;
        $("#iHaveAccount").unbind("click").bind("click", function (e) {
            _this.PopUp.animate({ 'top': '-50%' }, 350, function () {
                localStorage.removeItem("prePopup");
                _this.PopUp.parent().hide();
                logup.Show();
            });
            //this.Close();
        });
    };
    SignUpPupup.prototype.ClickBtCloseStep1 = function (step) {
        var _this = this;
        step.animate({ top: "-100%" }, 500, function () {
            _this.LogBox.unbind("mouseup");
            _this.LogBox.css("display", "none");
        });
        localStorage.removeItem("prePopup");
        this.SuccessAllParams = null;
    };
    SignUpPupup.prototype.ClickBtSignInStep = function (pop) {
        var _this = this;
        var User = new NewUserUser();
        User.Name = pop.find("#name").val();
        User.Email = pop.find("#email").val();
        User.Password = pop.find("#pass").val();
        User.ConfirmPassword = pop.find("#conf_pass").val();
        pop.animate({ top: "-100%" }, 500, function () {
            _this.LogBox.unbind("mouseup");
            _this.LogBox.css("display", "none");
        });
        if (this.SuccessAllParams != null)
            this.SuccessAllParams(User);
        this.SuccessAllParams = null;
    };
    SignUpPupup.prototype.ClickBtSignIn = function (pop) {
        var _this = this;
        var flag = true;
        var name = pop.find("#name");
        var email = pop.find("#email");
        var pass = pop.find("#pass");
        var confPass = pop.find("#conf_pass");
        if (!name.val()) {
            name.css("border-color", "red");
            $("#notEnterName").fadeIn("slow");
            flag = false;
        }
        else {
            name.css("border-color", "#ccc");
            $("#notEnterName").fadeOut("slow");
        }
        if (!email.val() || !email.val().match(Patterns.email)) {
            email.css("border-color", "red");
            $("#notEnterEmail").fadeIn("slow");
            flag = false;
        }
        else {
            $("#notEnterEmail").fadeOut("slow");
            email.css("border-color", "#ccc");
        }
        if (!pass.val()) {
            $("#notEnterPass").fadeIn("slow");
            pass.css("border-color", "red");
            flag = false;
        }
        else {
            $("#notEnterPass").fadeOut("slow");
            if (!pass.val().match(Patterns.password)) {
                $("#shortPass").fadeIn("slow");
                flag = false;
            }
            else {
                $("#shortPass").fadeOut("slow");
                pass.css("border-color", "#ccc");
                if (!confPass.val() || !(confPass.val() === pass.val())) {
                    confPass.css("border-color", "red");
                    $("#notMatchPass").fadeIn("slow");
                    flag = false;
                }
                else {
                    confPass.css("border-color", "#ccc");
                    $("#notMatchPass").fadeOut("slow");
                }
            }
        }
        if (!flag)
            return;
        $.ajax({
            url: "/Openlang/ChackExistUser",
            type: "post",
            data: { email: email.val() },
            success: function (result) {
                if (result.result) {
                    pop.animate({ top: "-100%" }, 500);
                    $("#emailExists").fadeOut("slow");
                    $("input#email").css("border", "1px solid red");
                    _this.LogBox.unbind("mouseup");
                    pop.siblings(".pop_up_trial.step_1").animate({ top: "50%" }, 500);
                }
                else {
                    $("#emailExists").fadeIn("slow");
                    $("input#email").css("border", "1px solid red");
                    stopLoader();
                }
            },
            error: function () {
            }
        });
    };
    return SignUpPupup;
}(LogIn));
//# sourceMappingURL=signuppupup.js.map