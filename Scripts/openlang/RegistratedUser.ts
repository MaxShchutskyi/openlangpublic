﻿class RegistratedUser {
    private static callbackUpdateMethods: Array<() => void>;
    static curName: string;
    static profilePicture: string;
    static userName: string;
    private static isExist:boolean = false;
    static get isInit(): boolean {
        if (this.isExist)
            return true;
        var res = localStorage.getItem("user");
        if (res) {
            this.isExist = true;
            this.initialize();
            return true;
        }
        return false;
    }
    static currency: string;
    static selectedLearnLang: string;
    static initialize(): void {
        var user = localStorage.getItem("user").replace(/&quot;/g, '\"');
        var currentUser = JSON.parse(user);
        this.init(currentUser);
    }

    private static init(obj: any) {
        this.curName = obj.Name;
        this.profilePicture = obj.ProfilePicture;
        if (!obj.Currency) {
            var code = JSON.parse(localStorage.getItem("region")).code;
            try {
                //var cur = CurrenciesCountry.getCurrencyCountry(code).currency + ":";  
                var cur;
                this.currency = cur;
            } catch (e) {
                this.currency = obj.Currency;
            } 
        }
        else
            this.currency = obj.Currency;
        this.selectedLearnLang = obj.SelectedLearnLang;
        this.callbackUpdateMethods = [];
        this.userName = obj.UserName;
        this.isExist = true;
    }

    static setUser(responseUser: string) {
        responseUser = responseUser.replace(/&quot;/g, '\"');
        localStorage.setItem("user", responseUser);
        var currentUser = JSON.parse(responseUser);
        this.init(currentUser);
    }

    static setOrUpdate():void {
        var newUser = { "Name": this.curName, "ProfilePicture": this.profilePicture, "UserName": this.userName, "Currency": this.currency, "selectedLearnLang": this.selectedLearnLang };
        localStorage.setItem("user", JSON.stringify(newUser));
        this.callCallbacksMethods();
    }

    private static callCallbacksMethods():void {
        for (var i = 0; i < this.callbackUpdateMethods.length; i++) {
            this.callbackUpdateMethods[i]();
        }
    }

    static addCallBack(callback: () => void):void {
        this.callbackUpdateMethods.push(callback);
    }

    static dispose():void {
        localStorage.removeItem("user");
        this.isExist = true;
    }

    static getImage():string {
        if (this.profilePicture)
            return "<img style='width:50px' class='img-responsive' src ='" + RegistratedUser.profilePicture + "'/>";
        return "<img style='width:50px' class='img-responsive' src=\"/Images/openlang/user.png\" />";
    }
}