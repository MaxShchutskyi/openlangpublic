﻿class MyAccountPageLoader {
    private static isSelectedTimezone: boolean;
    public static telephone: any;
    public static timezone: any;
    public static location: any;
    public static currency: any;
    public static userHelper: User;

    public static preLoad() {
        this.timezone.dropdown();
        this.setTelephone();
        this.initLocation();
    }

    public static loadPage(): void {
        this.setLocation(this.userHelper.location);
        this.setCurrency(this.userHelper.currency);
        if (!this.isSelectedTimezone)
            this.setTimezone(this.userHelper.timezone);
    }

    private static initLocation(): void {
        this.location.dropdown({
            onChange: (p, text) => {
                this.userHelper.location = text;
                this.telephone.intlTelInput("selectCountry", p);
            }
        });
    }
    private static setTelephone() {
        this.telephone.intlTelInput({
            utilsScript: '/Scripts/openlang/utils.js'
        });
    }
    private static setTimezone(timezoneSel: string): void {
        var selloc = this.timezone.siblings(".menu").children().filter(":contains('" + timezoneSel + "')");
        if (selloc.length > 0)
            this.timezone.dropdown('set selected', timezoneSel);
        else {
            var str = "<div class='item' data-value='" + timezoneSel + "'>" + timezoneSel + "</div>";
            this.timezone.siblings(".menu").append(str);
            var selloc2 = this.timezone.siblings(".menu").children().filter(":contains('" + timezoneSel + "')");
            selloc2.trigger("click");
        }
    }
    private static setCurrency(currency: string): void {
            if (this.location) {
                if (currency)
                    this.currency.children().filter(":contains('" + currency + "')").attr('selected', 'selected');
                else if (localStorage.getItem("region")) {
                    var code = JSON.parse(localStorage.getItem("region")).code;
                    //var cur = CurrenciesCountry.getCurrencyCountry(code).currency;
                    var cur;
                    this.userHelper.currency = this.currency.children().filter(":contains('" + cur + "')").attr('selected', 'selected').text();
                } else {
                    this.userHelper.currency = this.currency.children().first().text();
                }
            }
        this.currency.dropdown({
            onChange: (p, text) => {
                this.userHelper.currency = text;
            }
        });
    }

    private static setCurrencyByCountryCode(code: string) {
        var selcur = this.currency.siblings(".menu").children("[data-value=" + code + "]");
        selcur.trigger("click");
    }

    private static setLocation(location: string): void {
        if (this.userHelper.location) {
            var selloc = this.location.siblings(".menu").children().filter(":contains('" + location + "')");
            selloc.trigger("click");
            this.initLocation();
            this.telephone.intlTelInput("selectCountry", $(selloc).data("value"));
        }
        else {
            //GeolocationManager.init(result => {
            //    this.location.siblings(".menu").children().filter("[data-value='" + result.code + "']").trigger("click");
            //    this.telephone.intlTelInput("selectCountry", result.code);
            //    this.setCurrencyByCountryCode(result.code);
            //    this.getTimeZone(result.timezone);
            //});
            //navigator.geolocation.getCurrentPosition(pos=> {
            //    var coords = pos.coords;
            //    var url = "http://ws.geonames.org/countryCode?lat=" + coords.latitude + "&lng=" + coords.longitude + "&username=eugeny";
            //    $.get(url, result=> {
            //        var countryCode = result.replace(/(\r\n|\n|\r)/gm, "");
            //        this.location.siblings(".menu").children().filter("[data-value='" + countryCode + "']").trigger("click");
            //        this.telephone.intlTelInput("selectCountry", countryCode);
            //        this.setCurrencyByCountryCode(countryCode);
            //        this.getTimeZone(coords);
            //    });
            //});
        }
    }
    private static getTimeZone(timezone) {
        var str = "<div class='item' data-value='" + timezone + "'>" + timezone + "</div>";
            this.timezone.siblings(".menu").append(str);
            var selloc = this.timezone.siblings(".menu").children().filter(":contains('" + timezone + "')");
            selloc.trigger("click");
            this.isSelectedTimezone = true;
        //var url = "http://ws.geonames.org/timezoneJSON?lat=" + coords.latitude + "&lng=" + coords.longitude + "&username=eugeny";
        //$.getJSON(url, result=> {
        //    var str = "<div class='item' data-value='" + result.timezoneId + "'>" + result.timezoneId + "</div>";
        //    this.timezone.siblings(".menu").append(str);
        //    var selloc = this.timezone.siblings(".menu").children().filter(":contains('" + result.timezoneId + "')");
        //    selloc.trigger("click");
        //    this.isSelectedTimezone = true;
        //});
    }
}