﻿class ImageForMessage {
    public path: string;
    public index: string;
    public message:string;
    private static imageCollection: Array<ImageForMessage>;
    private static initImages(): void {
        this.imageCollection = [];
        this.imageCollection.push(new ImageForMessage("/Images/openlang/forMessage/sendEmail.jpg", "SendEmail","Message was sent successful"));
        this.imageCollection.push(new ImageForMessage("/Images/openlang/forMessage/forApplication.jpg", "Application","Your application was sent successful"));
        this.imageCollection.push(new ImageForMessage("/Images/openlang/forMessage/alreadyVouted.jpg", "AlreadyVouted","You have already voted on this issue"));
        this.imageCollection.push(new ImageForMessage("/Images/openlang/forMessage/forRequest.jpg", "Request","Your request was sent successful"));
        this.imageCollection.push(new ImageForMessage("/Images/openlang/forMessage/LanguagesUpdated.jpg", "LanguageUpdate","Languages updated"));
        this.imageCollection.push(new ImageForMessage("/Images/openlang/forMessage/PasswordUpdated.jpg", "PasswordUpdate","Password updated"));
        this.imageCollection.push(new ImageForMessage("/Images/openlang/forMessage/success.jpg", "Success","Success"));
        this.imageCollection.push(new ImageForMessage("/Images/openlang/forMessage/uncorrectPassword.jpg", "UncorrectPassword", "Incorrect password"));
        this.imageCollection.push(new ImageForMessage("/Images/openlang/forMessage/profileUpdate.jpg", "ProfileUpdate", "Profie updated"));
    }

    public static getImage(index: string): ImageForMessage {
        if (!this.imageCollection)
            this.initImages();
        for (var i = 0; i < this.imageCollection.length; i++) {
            if (this.imageCollection[i].index === index)
                return this.imageCollection[i];
        }
        return null;
    }

    constructor(path: string, index: string, message:string) {
        this.path = path;
        this.index = index;
        this.message = message;
    }
}
class Messager {
    private static messeger:JQuery;
    public static show(index: string) {
        if (!this.messeger)
            this.messeger = $(".messeger");
        var imgForMess = ImageForMessage.getImage(index);
        this.messeger.find(".icon img").attr("src", imgForMess.path);
        this.messeger.find(".textMessage").text(imgForMess.message);
        this.messeger.css("display", "block").children(".message").animate({ 'top': '50%' },500);
    }

    public static close(): void {
        this.messeger.children(".message").animate({ 'top': '-100%' }, 500, ()=> {
            this.messeger.css("display", "none");
        });
    }
}