﻿class Languages {
    public iLearn: Array<string>;
    public iSpeak: Array<string>;
    private ispeak: any;
    private ilearn: any;
    public initialize(ilearn: string, ispeak: string) {
        this.iLearn = new Array<string>();
        this.iSpeak = new Array<string>();
        if (ilearn)
            this.iLearn = this.splitStr(ilearn);
        if (ispeak)
            this.iSpeak = this.splitStr(ispeak);
    }
    private splitStr(str: string): Array<string> {
        var sp = str.split('|');
        sp.pop();
        return sp;
    }

    public selectedValuesISpeak(ispeak: any) {
        this.ispeak = ispeak;
        this.selectedValues($(ispeak).children("option"), this.iSpeak);
    }
    public selectedValuesILearn(ilearn: any) {
        this.ilearn = ilearn;
        this.selectedValues($(ilearn).children(""), this.iLearn);
    }
    private selectedValues(arr: JQuery, selected: Array<string>): void {
        if (!selected) return;
        $.each(arr, (index, obj) => {
            $.each(selected, (index2, obj2) => {
                if ($(obj).val() === obj2.split('_')[1]) {
                    $(obj).attr("selected", "selected");
                }
            });
        });
    }

    public bindChanges() {
        this.ispeak.dropdown({
            onAdd:(val, text)=> {
                this.iSpeak.push(text + "_" + val);
            },
            onRemove:(val)=> {
                this.iSpeak = $.grep(this.iSpeak, val1 => (val1.split("_")[1] !== val));
            }
        });
        this.ilearn
            .dropdown({
                onAdd:(val, text)=> {
                    this.iLearn.push(text + "_" + val);
                },
                onRemove:(val)=> {
                    this.iLearn = $.grep(this.iLearn, val1 => (val1.split("_")[1] !== val));
                }
            });
    }

    public saveChanges() {
        $.ajax({
            url: "/api/Languages",
            type: 'put',
            beforeSend:(xhr)=> BeforeSendAuthorizeApi.beforeSend(xhr),
            data: { Ilearn: this.iLearn, Ispeak: this.iSpeak },
            success: () => {
                $("#updatedLanguages").fadeIn("slow",  ()=> {
                    setTimeout( ()=> {
                        $("#updatedLanguages").fadeOut("slow");
                    }, 5000);
                }).css('display', 'flex');;
                //Messager.show("LanguageUpdate");
            },
            error:()=> {
                alert("error");
            }
        });
    }
}