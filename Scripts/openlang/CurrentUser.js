var User = /** @class */ (function () {
    function User() {
        this.maxFileSize = 7e+6;
        this.languages = new Languages();
        this.changePassword = new ChangePasswordProvider();
    }
    User.prototype.initialize = function (comeUser) {
        this.email = comeUser.Email;
        this.name = comeUser.Name;
        this.firstName = comeUser.FirstName;
        this.lastName = comeUser.LastName;
        this.location = comeUser.Location;
        this.currency = comeUser.Currency;
        this.timezone = comeUser.Timezone;
        this.ilearn = comeUser.Ilearn;
        this.ispeak = comeUser.Ispeak;
        this.phonenumber = comeUser.PhoneNumber;
        this.active = comeUser.Active;
        this.languages.initialize(this.ilearn, this.ispeak);
        this.initializeComplete();
        this.activeStatusReady(this.active);
    };
    User.prototype.changeAvatar = function (file, success) {
        var fileData = $(file).prop("files")[0];
        var result = this.validateFile(fileData);
        if (result === "OK") {
        }
        else {
            $(file).val("");
            $("#uncorrectParameters").fadeIn("slow", function () {
                setTimeout(function () {
                    $("#uncorrectParameters").fadeOut("slow");
                }, 5000);
            }).css("display", "flex");
            return;
        }
        $("#loadingImage").fadeIn("slow");
        var formData = new FormData();
        formData.append("file", fileData);
        $.ajax({
            url: "/api/Account/UploadFile",
            dataType: 'script',
            beforeSend: function (xhr) { return BeforeSendAuthorizeApi.beforeSend(xhr); },
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            type: 'post',
            success: function (data) { return success(data); },
            error: function (e) {
            }
        });
    };
    User.prototype.validateFile = function (file) {
        if (!file)
            return "null";
        if (file.size > this.maxFileSize) {
            return resource.match('file_format');
        }
        var math = Patterns.loadImage;
        if (!math.exec(file.type)) {
            return resource.match('file_size');
        }
        return "OK";
    };
    User.prototype.getPropertyValues = function () {
        return { email: this.email, name: this.name, firstName: this.firstName, lastName: this.lastName, location: this.location, currency: this.currency, timezone: this.timezone, phonenumber: this.phonenumber, active: this.active };
    };
    User.prototype.saveChanges = function (name, phonenumber, email, timezone, success) {
        var _this = this;
        this.name = name;
        this.phonenumber = phonenumber;
        this.email = email;
        this.timezone = timezone;
        $.ajax({
            type: "put",
            beforeSend: function (xhr) { return BeforeSendAuthorizeApi.beforeSend(xhr); },
            url: "/api/Account",
            data: this.getPropertyValues(),
            success: function (e) {
                RegistratedUser.currency = _this.currency;
                RegistratedUser.curName = _this.name;
                RegistratedUser.setOrUpdate();
                success(e);
            },
            error: function (e) {
                //alert(e.responseText);
                //console.log(e);
            }
        });
    };
    User.prototype.deactivateAccount = function (callback) {
        var _this = this;
        $.ajax({
            method: "post",
            url: "/api/Deactivate/DeactivateAccount",
            beforeSend: function (xhr) { return BeforeSendAuthorizeApi.beforeSend(xhr); },
            success: function () { return callback(_this.active = !_this.active, null); },
            error: function (e) { return callback(false, e.responseText); }
        });
    };
    return User;
}());
//# sourceMappingURL=CurrentUser.js.map