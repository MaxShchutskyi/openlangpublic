﻿class Validator {
    static validate(items: JQuery, success: (item) => void, invalid: (item, key) => void): void {
        var inputs = items.find("input");
        var required = inputs.filter("[required]");
        var pattern = inputs.filter("[pattern]");
        required.each((index, elem) => {
            this.required(elem) ? success(elem) : invalid(elem, "Not required");
        });
        pattern.each((index, element) => {
            this.pattern(element)?success(element):invalid(element,"Pattern not match");
        });
    }

    private static required(elem):boolean {
        return $(elem).val();
    }

    private static pattern(elem):boolean {
        var pat = $(elem).attr("pattern");
        return $(elem).val().match(pat);
    }
}