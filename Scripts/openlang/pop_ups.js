/// <reference path="../typings/jquery/jquery.d.ts" />
var LogIn = /** @class */ (function () {
    function LogIn(cover, box) {
        if (this.LogBox == null || this.PopUp == null) {
            this.LogBox = $(cover);
            this.PopUp = this.LogBox.find(box);
        }
    }
    LogIn.prototype.Show = function () {
        this.ShowBackground();
        this.ClickOutBox();
        this.IsVisible = true;
    };
    LogIn.prototype.Close = function () {
        this.LogBox.unbind("mouseup");
        this.AnimatePupUp(false);
        this.IsVisible = false;
    };
    LogIn.prototype.ShowBackground = function () {
        this.LogBox.css("display", "block");
        this.AnimatePupUp(true);
    };
    LogIn.prototype.AnimatePupUp = function (show) {
        var _this = this;
        if (show === void 0) { show = true; }
        if (show) {
            this.PopUp.animate({ "top": "50%" }, 500);
            return;
        }
        this.PopUp.animate({ "top": "-100%" }, 500, function () {
            _this.LogBox.css("display", "none");
        });
    };
    LogIn.prototype.ClickOutBox = function () {
        //this.LogBox.unbind("mouseup").bind("mouseup", e => {
        //    if ($(this.PopUp).has(e.target).length === 0) {
        //        this.Close();
        //    }
        //});
    };
    return LogIn;
}());
//# sourceMappingURL=pop_ups.js.map