var UserManager = /** @class */ (function () {
    function UserManager() {
    }
    UserManager.initalizeRegionsInformation = function (code, timezone, usercountrycode) {
        var _this = this;
        var ipGeolocationManager = new IPGeolocationManager();
        var freeGeoIp = new FreeGeoIp();
        var necudo = new geoipnekudo();
        var htmlGeolocationManager = new HTML5GeolocationManager();
        var nullGeplocationManager = new NullbleGeolocationManager();
        var googleAPIGeolocationManager = new GoogleAPIGeplocationManager();
        var currentCurrencyTable;
        necudo.Loosefull = freeGeoIp;
        freeGeoIp.Loosefull = googleAPIGeolocationManager;
        googleAPIGeolocationManager.Loosefull = ipGeolocationManager;
        ipGeolocationManager.Loosefull = htmlGeolocationManager;
        htmlGeolocationManager.Loosefull = nullGeplocationManager;
        necudo.init(function (location) {
            if (location.country === "Sweden" || location.country === "Norway" || location.country === "Finland" || location.country === "Denmark" || location.country === "Iceland") {
                currentCurrencyTable = new ScandinaviaRegionIndividualFactory();
            }
            else if (location.country === "Switzerland") {
                currentCurrencyTable = new SwitzerlandRegionIndividualFactory();
            }
            else {
                currentCurrencyTable = new EuropeRegionIndividualFactory();
            }
            UserManager.location = location;
            currentCurrencyTable.initialize();
            console.log(currentCurrencyTable);
            _this.initializeCurrency(code, currentCurrencyTable);
            _this.initializeTimezonesAndTelFlags(location, timezone, usercountrycode);
            if (UserManager.allInitialized)
                UserManager.allInitialized();
        });
    };
    UserManager.initializeCurrency = function (code, currentCurrencyTable) {
        if (!code)
            code = AbstractGeolocationManager.location.code;
        CurrenciesCountry.CountryPriceTable = currentCurrencyTable;
        CurrenciesCountry.CurrentCurrencyConverter = new FixerIOCurrency();
        CurrenciesCountry.initCurrencyCountry(code);
        $(".currency-for-price").text(CurrenciesCountry.selectedCurrency.currency);
        $(".lesson-item").first().click();
    };
    UserManager.initializeTimezonesAndTelFlags = function (location, timezone, usercountrycode) {
        $(document).ready(function () {
            if (!timezone) {
                var sel = $(".js-select2-timezone");
                sel.select2({
                    'data': [{ id: location.timezone, text: location.timezone, selected: true }],
                    minimumResultsForSearch: -1
                });
            }
            var tel = $(".phone");
            if (!usercountrycode)
                tel.intlTelInput("setCountry", location.code);
            else
                tel.intlTelInput("setCountry", usercountrycode);
        });
    };
    return UserManager;
}());
//# sourceMappingURL=UserManager.js.map