﻿declare var resource: any;
class User {
    initializeComplete: () => void;
    activeStatusReady: (status: boolean) => void;
    email: string;
    name: string;
    firstName: string;
    lastName: string;
    location: string;
    currency: string;
    timezone: string;
    ilearn: string;
    ispeak: string;
    private maxFileSize: number = 7e+6;
    phonenumber: string;
    active: boolean;
    languages: Languages;
    changePassword: ChangePasswordProvider;

    constructor() {
        this.languages = new Languages();
        this.changePassword = new ChangePasswordProvider();
    }
    public initialize(comeUser: any): void {
        this.email = comeUser.Email;
        this.name = comeUser.Name;
        this.firstName = comeUser.FirstName;
        this.lastName = comeUser.LastName;
        this.location = comeUser.Location;
        this.currency = comeUser.Currency;
        this.timezone = comeUser.Timezone;
        this.ilearn = comeUser.Ilearn;
        this.ispeak = comeUser.Ispeak;
        this.phonenumber = comeUser.PhoneNumber;
        this.active = comeUser.Active;
        this.languages.initialize(this.ilearn, this.ispeak);
        this.initializeComplete();
        this.activeStatusReady(this.active);
    }

    public changeAvatar(file: JQuery, success: (data:string) => void) {
        var fileData = $(file).prop("files")[0];
        var result = this.validateFile(fileData);
        if (result === "OK") {
        } else {
            $(file).val("");
            $("#uncorrectParameters").fadeIn("slow", () => {
                setTimeout(() => {
                    $("#uncorrectParameters").fadeOut("slow");
                }, 5000);
            }).css("display","flex");
            return;
        }
        $("#loadingImage").fadeIn("slow");
        var formData = new FormData();
        formData.append("file", fileData);
        $.ajax({
            url: "/api/Account/UploadFile",
            dataType: 'script',
            beforeSend: (xhr) => BeforeSendAuthorizeApi.beforeSend(xhr),
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            type: 'post',
            success: data=> success(data),
            error: e => {

            }
        });
    }

    private validateFile(file):string {
        if (!file) return "null";
        if (file.size > this.maxFileSize) {
            return resource.match('file_format');
        }
        var math = Patterns.loadImage;
        if (!math.exec(file.type)) {
            return resource.match('file_size');
        }
        return "OK";
    }

    public getPropertyValues(): any {
        return { email: this.email, name: this.name, firstName: this.firstName, lastName: this.lastName, location: this.location, currency: this.currency, timezone: this.timezone, phonenumber: this.phonenumber, active: this.active };
    }

    public saveChanges(name: string, phonenumber: string, email: string, timezone:string, success:(data:any)=>void): void {
        this.name = name;
        this.phonenumber = phonenumber;
        this.email = email;
        this.timezone = timezone;
        $.ajax({
            type: "put",
            beforeSend: (xhr) => BeforeSendAuthorizeApi.beforeSend(xhr),
            url: "/api/Account",
            data: this.getPropertyValues(),
            success: e => {
                RegistratedUser.currency = this.currency;
                RegistratedUser.curName = this.name;
                RegistratedUser.setOrUpdate();
                success(e);
            },
            error: e => {
                //alert(e.responseText);
                //console.log(e);
            }
        });
    }

    public deactivateAccount(callback: (status: boolean, errorMessage: string) => void): void {
        $.ajax({
            method: "post",
            url: "/api/Deactivate/DeactivateAccount",
            beforeSend: (xhr) => BeforeSendAuthorizeApi.beforeSend(xhr),
            success: () => callback(this.active = !this.active, null),
            error: (e) => callback(false, e.responseText)
        });
    }
}