﻿class Patterns {
    static time = /^\s*([01]?\d|2[0-3]):?([0-5]\d)\s*$/;
    static replaceSpaces = / /g;
    static replaceTabsAndNewLines = /[_\s]/g;
    static replaceQuot = /&quot;/g;
    static email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    static numbers = /^[0-9]$/;
    static date = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    static password = /^[a-zA-Z0-9!#$%^&*]{6,16}$/;
    static loadFile = /(\jpg|\jpeg|\png|\pdf|\xls)$/i;
    static replaceSpecSymbols = /[&\\#,=_+()$~%'"*?<>{}]/g;
    static loadImage = /(\jpg|\jpeg|\BMP|\png|\image\/x-icon)$/i;
}