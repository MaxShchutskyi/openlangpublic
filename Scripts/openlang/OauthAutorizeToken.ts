﻿declare var resource: any;
class OAuthToken {
    public static successResponse: (data: any) => void;
    public static successResponses: Array<(data: any) => void> = [];
    private static loginData: any = {
        grant_type: 'password',
        username: '',
        password: ''
    };
    public static setTokenOnly(userName: string, redirect:string): void {
        if (sessionStorage.getItem('token')){
            OAuthToken.successResponses.forEach(function (elem) {
                elem(true);
            });
            return;
        }
        this.loginData.username = userName,
            $.ajax({
                type: 'POST',
                url: '/Token',
                data: this.loginData,
                success: data => {
                    sessionStorage.setItem('token', data.access_token);
                    OAuthToken.successResponses.forEach(function (elem) {
                        elem(true);
                    });
                    if (redirect)
                        location.href = redirect;
                    //OAuthToken.successResponse(true);
                },
                error: err => alert(resource.match('login_error') + err)
            });
    }
    //public static getToken(): void {
    //    this.loginData.username = RegistratedUser.userName,
    //    $.ajax({
    //        type: 'POST',
    //        url: '/Token',
    //        data: OAuthToken.loginData,
    //        success: data => {
    //            sessionStorage.setItem('token', data.access_token);
    //            OAuthToken.getPage();
    //        },
    //        error: err => alert('При логине возникла ошибка' + err)
    //    });
    //}

    //public static getPage() {
    //    $.ajax({
    //        type: 'GET',
    //        url: '/api/Account',
    //        beforeSend: xhr=> xhr.setRequestHeader("Authorization", "Bearer " + sessionStorage.getItem('token')),
    //        success: data => OAuthToken.successResponse(data),
    //        error: data=> alert(data)
    //    });
    //}
}