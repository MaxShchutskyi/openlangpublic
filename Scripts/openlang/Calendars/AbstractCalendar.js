var AbstractCalendar = /** @class */ (function () {
    function AbstractCalendar(calendar) {
        this.calendar = calendar;
    }
    AbstractCalendar.prototype.initializeControl = function (url) {
        $.getJSON(url, { 'teacherId': this.selectedUserId, 'week': null }).always(function (result) {
            setTimeout(function () {
                var _this = this;
                this.calendar.fullCalendar({
                    dayClick: function (date, events, view) { return _this.dayClick(); },
                    header: {
                        left: false,
                        center: 'title',
                        right: 'prev,next',
                    },
                    selectable: true,
                    select: function (start, end) { return _this.select(start, end); },
                    selectHelper: false,
                    eventLimit: true,
                    events: result,
                    eventClick: function (calEvent, jsEvent, view) { return _this.eventClick(calEvent, jsEvent, view); },
                    editable: false,
                    defaultView: 'agendaWeek',
                    allDaySlot: false,
                    slotDuration: '01:00:00',
                    slotLabelFormat: "HH:mm",
                    weekNumbers: true,
                    maxTime: '24:00:00',
                    timezone: 'local',
                    ignoreTimezone: false,
                });
                this.listOfShowedWeeks = new Array();
                //$('.fc-axis.fc-widget-header').append('<span class="local-time">UTC' + localTime + '</span>')
                $('.fc-toolbar').append('<span class="previous-week"><i class="fa fa-angle-left" aria-hidden="true"></i>Previous week</span><span class="next-week">Next week<i class="fa fa-angle-right" aria-hidden="true"></i></span>');
            }, 500);
        });
    };
    return AbstractCalendar;
}());
//# sourceMappingURL=AbstractCalendar.js.map