﻿abstract class AbstractCalendar {
    public lastSelectedDate: any;
    public calendar: any;
    public listOfShowedWeeks: Array<number>;
    public selectedUserId: string;
    constructor(calendar: any) {
        this.calendar = calendar;
    }
    abstract dayClick(date: any, events: any, view: any): void;
    abstract select(start: any, end: any): void;
    abstract eventClick(calEvent: any, jsEvent: any, view: any): void;
    public initializeControl(url: string): void {
        $.getJSON(url, { 'teacherId': this.selectedUserId, 'week': null }).always(result=> {
            setTimeout(function () {
                this.calendar.fullCalendar({
                    dayClick: (date, events, view) => this.dayClick(),
                    header: {
                        left: false,
                        center: 'title',
                        right: 'prev,next',
                    },
                    selectable: true,
                    select: (start, end) => this.select(start, end),
                    selectHelper: false,
                    eventLimit: true, 
                    events: result,
                    eventClick: (calEvent, jsEvent, view) => this.eventClick(calEvent, jsEvent, view),
                    editable: false,
                    defaultView: 'agendaWeek',
                    allDaySlot: false,
                    slotDuration: '01:00:00',
                    slotLabelFormat: "HH:mm",
                    weekNumbers: true,
                    maxTime: '24:00:00',
                    timezone: 'local',
                    ignoreTimezone: false,
                });
                this.listOfShowedWeeks = new Array<number>();
                //$('.fc-axis.fc-widget-header').append('<span class="local-time">UTC' + localTime + '</span>')
                $('.fc-toolbar').append('<span class="previous-week"><i class="fa fa-angle-left" aria-hidden="true"></i>Previous week</span><span class="next-week">Next week<i class="fa fa-angle-right" aria-hidden="true"></i></span>')
            }, 500)
        });
    }
}