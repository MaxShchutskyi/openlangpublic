var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var CalendarManager = /** @class */ (function (_super) {
    __extends(CalendarManager, _super);
    function CalendarManager(calendar, object) {
        var _this = _super.call(this, calendar) || this;
        _this.clickHandler = new ClickHandler();
        _this.clickHandler.clickHandlerResult = function (repeater) { return _this.clickHandlerResult(repeater); };
        FormRepeatManager.object = object;
        return _this;
    }
    CalendarManager.prototype.select = function (start, end) {
        this.curentDateTime = start._d;
        var title = ' ';
        var eventData;
        var numb = $(".fc-week-number span:first-child").text();
        numb = parseInt(numb.substr(1)) - 1;
        if (title) {
            eventData = {
                title: title,
                start: start,
                end: end,
                className: 'view1',
                week: numb,
                dayOfTheWeek: start._d.getDay() + 1
            };
            this.calendar.fullCalendar('renderEvent', eventData, true); // stick? = true
            this.calendar.fullCalendar('unselect');
        }
    };
    CalendarManager.prototype.eventClick = function (calEvent, jsEvent, view) {
        //console.log(calEvent)
        //console.log(jsEvent)
        //console.log(view)
        if (calEvent.className[0] == 'view1') {
            this.calendar.fullCalendar('removeEvents', calEvent._id);
            if (calEvent.id)
                this.deletedEvents.push(calEvent.id);
        }
    };
    ;
    CalendarManager.prototype.dayClick = function (date, events, view) {
        this.lastSelectedDate = date;
        this.calendar.find(".form-control.callendar-date").val(moment(date._d).format("DD/ MM / YYYY HH: 00"));
    };
    CalendarManager.prototype.clickHandlerResult = function (result) {
        var _this = this;
        var rslt = result.Repeat();
        var finishFilter = [];
        var events = this.calendar.fullCalendar('clientEvents');
        rslt.forEach(function (elem) {
            var isExist = false;
            events.forEach(function (event) {
                var st = event.start._d;
                var st2 = elem.start;
                if (st.getFullYear() == st2.getFullYear() && st.getMonth() == st2.getMonth() && st.getDate() == st2.getDate() && st.getHours() == st2.getHours())
                    isExist = true;
            });
            if (!isExist)
                finishFilter.push(elem);
        });
        finishFilter.forEach(function (elem) {
            _this.calendar.fullCalendar('renderEvent', elem, true); // stick? = true
        });
    };
    return CalendarManager;
}(AbstractCalendar));
//# sourceMappingURL=MyAccountTeacherCalendar.js.map