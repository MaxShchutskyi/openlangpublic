﻿class CalendarManager extends AbstractCalendar {
    curentDateTime: any;
    deletedEvents: Array<number>;
    clickHandler: ClickHandler;
    constructor(calendar: any, object: JQuery) {
        super(calendar);
        this.clickHandler = new ClickHandler();
        this.clickHandler.clickHandlerResult = (repeater) => this.clickHandlerResult(repeater);
        FormRepeatManager.object = object;
    }
    select(start: any, end: any): void {
        this.curentDateTime = start._d;
        var title = ' ';
        var eventData;
        var numb: any = $(".fc-week-number span:first-child").text();
        numb = parseInt(numb.substr(1)) - 1;
        if (title) {
            eventData = {
                title: title,
                start: start,
                end: end,
                className: 'view1',
                week: numb,
                dayOfTheWeek: start._d.getDay() + 1
            };
            this.calendar.fullCalendar('renderEvent', eventData, true); // stick? = true
            this.calendar.fullCalendar('unselect');
        }
    }
    eventClick(calEvent: any, jsEvent: any, view: any) {
        //console.log(calEvent)
        //console.log(jsEvent)
        //console.log(view)
        if (calEvent.className[0] == 'view1') {
            this.calendar.fullCalendar('removeEvents', calEvent._id)
            if (calEvent.id)
                this.deletedEvents.push(calEvent.id)
        }
    };
    dayClick(date: any, events: any, view: any): void {
        this.lastSelectedDate = date;
        this.calendar.find(".form-control.callendar-date").val(moment(date._d).format("DD/ MM / YYYY HH: 00"));
    }
    clickHandlerResult(result: AbstractRepeat) {
        var rslt = result.Repeat();
        var finishFilter = [];
        var events = this.calendar.fullCalendar('clientEvents');
        rslt.forEach(elem => {
            var isExist = false;
            events.forEach(event => {
                var st = event.start._d;
                var st2 = elem.start;
                if (st.getFullYear() == st2.getFullYear() && st.getMonth() == st2.getMonth() && st.getDate() == st2.getDate() && st.getHours() == st2.getHours())
                    isExist = true;
            });
            if (!isExist)
                finishFilter.push(elem);
        });
        finishFilter.forEach(elem => {
            this.calendar.fullCalendar('renderEvent', elem, true); // stick? = true
        });
    }
}