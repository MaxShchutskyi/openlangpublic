var Languages = /** @class */ (function () {
    function Languages() {
    }
    Languages.prototype.initialize = function (ilearn, ispeak) {
        this.iLearn = new Array();
        this.iSpeak = new Array();
        if (ilearn)
            this.iLearn = this.splitStr(ilearn);
        if (ispeak)
            this.iSpeak = this.splitStr(ispeak);
    };
    Languages.prototype.splitStr = function (str) {
        var sp = str.split('|');
        sp.pop();
        return sp;
    };
    Languages.prototype.selectedValuesISpeak = function (ispeak) {
        this.ispeak = ispeak;
        this.selectedValues($(ispeak).children("option"), this.iSpeak);
    };
    Languages.prototype.selectedValuesILearn = function (ilearn) {
        this.ilearn = ilearn;
        this.selectedValues($(ilearn).children(""), this.iLearn);
    };
    Languages.prototype.selectedValues = function (arr, selected) {
        if (!selected)
            return;
        $.each(arr, function (index, obj) {
            $.each(selected, function (index2, obj2) {
                if ($(obj).val() === obj2.split('_')[1]) {
                    $(obj).attr("selected", "selected");
                }
            });
        });
    };
    Languages.prototype.bindChanges = function () {
        var _this = this;
        this.ispeak.dropdown({
            onAdd: function (val, text) {
                _this.iSpeak.push(text + "_" + val);
            },
            onRemove: function (val) {
                _this.iSpeak = $.grep(_this.iSpeak, function (val1) { return (val1.split("_")[1] !== val); });
            }
        });
        this.ilearn
            .dropdown({
            onAdd: function (val, text) {
                _this.iLearn.push(text + "_" + val);
            },
            onRemove: function (val) {
                _this.iLearn = $.grep(_this.iLearn, function (val1) { return (val1.split("_")[1] !== val); });
            }
        });
    };
    Languages.prototype.saveChanges = function () {
        $.ajax({
            url: "/api/Languages",
            type: 'put',
            beforeSend: function (xhr) { return BeforeSendAuthorizeApi.beforeSend(xhr); },
            data: { Ilearn: this.iLearn, Ispeak: this.iSpeak },
            success: function () {
                $("#updatedLanguages").fadeIn("slow", function () {
                    setTimeout(function () {
                        $("#updatedLanguages").fadeOut("slow");
                    }, 5000);
                }).css('display', 'flex');
                ;
                //Messager.show("LanguageUpdate");
            },
            error: function () {
                alert("error");
            }
        });
    };
    return Languages;
}());
//# sourceMappingURL=Languages.js.map