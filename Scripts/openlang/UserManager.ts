﻿class UserManager {
    public static allInitialized: () => void;
    public static location:any;
    static initalizeRegionsInformation(code: string, timezone: string, usercountrycode: string): void {
        var ipGeolocationManager = new IPGeolocationManager();
        var freeGeoIp = new FreeGeoIp();
        var necudo = new geoipnekudo();
        var htmlGeolocationManager = new HTML5GeolocationManager();
        var nullGeplocationManager = new NullbleGeolocationManager();
        var googleAPIGeolocationManager = new GoogleAPIGeplocationManager();

        var currentCurrencyTable: AbstractRegionFactory;

        necudo.Loosefull = freeGeoIp;
        freeGeoIp.Loosefull = googleAPIGeolocationManager;
        googleAPIGeolocationManager.Loosefull = ipGeolocationManager;
        ipGeolocationManager.Loosefull = htmlGeolocationManager;
        htmlGeolocationManager.Loosefull = nullGeplocationManager;
        necudo.init(location => {
            if (location.country === "Sweden" || location.country === "Norway" || location.country === "Finland" || location.country === "Denmark" || location.country === "Iceland") {
                currentCurrencyTable = new ScandinaviaRegionIndividualFactory();
            } else if (location.country === "Switzerland") {
                currentCurrencyTable = new SwitzerlandRegionIndividualFactory();
            } else {
                currentCurrencyTable = new EuropeRegionIndividualFactory();
            }
            UserManager.location = location;
            currentCurrencyTable.initialize();
            console.log(currentCurrencyTable);
            this.initializeCurrency(code, currentCurrencyTable);
            this.initializeTimezonesAndTelFlags(location, timezone, usercountrycode);
            if (UserManager.allInitialized)
                UserManager.allInitialized();
        });
    }
    static initializeCurrency(code, currentCurrencyTable: AbstractRegionFactory) {
        if (!code)
            code = AbstractGeolocationManager.location.code;
        CurrenciesCountry.CountryPriceTable = currentCurrencyTable;
        CurrenciesCountry.CurrentCurrencyConverter = new FixerIOCurrency();
        CurrenciesCountry.initCurrencyCountry(code);
        $(".currency-for-price").text(CurrenciesCountry.selectedCurrency.currency);
        $(".lesson-item").first().click();
    }
    static initializeTimezonesAndTelFlags(location:any,timezone: string, usercountrycode: string)
    {
        $(document).ready(() => {
            if (!timezone) {
                var sel: any = $(".js-select2-timezone");
                sel.select2({
                    'data': [{ id: location.timezone, text: location.timezone, selected: true }],
                    minimumResultsForSearch: -1
                });
            }
            var tel: any = $(".phone");
            if (!usercountrycode) 
                tel.intlTelInput("setCountry", location.code)
            else
                tel.intlTelInput("setCountry", usercountrycode)
        });
    }
}