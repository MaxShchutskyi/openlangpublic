/// <reference path="signuppupup.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var LogUpPopup = /** @class */ (function (_super) {
    __extends(LogUpPopup, _super);
    function LogUpPopup() {
        var _this = _super.call(this, ".log_in", ".pop_up_log_in") || this;
        var self = _this;
        _this.PopUp.find("#fogot_password").unbind("click").bind("click", _this.FogotPawwsordClick.bind(self));
        _this.FogotPassPopup = _this.LogBox.find(".resetPassword");
        _this.FogotPassPopup.find(".go_to_login").unbind("click").bind("click", _this.CobeBackToLigin.bind(self));
        _this.FogotPassPopup.find("#reset_complete").unbind("click").bind("click", _this.ConfirmReset.bind(self));
        _this.CompletePass = _this.LogBox.find(".thanks.reset_psw");
        _this.CompletePass.find("#close_trial_step_thanks_reset").unbind("click").bind("click", _this.CLoseLastWindow.bind(self));
        _this.PopUp.find("input").keypress(function (e) {
            if (e.which === 13) {
                $('#LOGIN').click();
                return false; //<---- Add this line
            }
        });
        return _this;
    }
    LogUpPopup.prototype.CobeBackToLigin = function () {
        this.FogotPassPopup.animate({ "top": "-100%" }, 500);
        this.PopUp.animate({ "top": "50%" }, 500);
    };
    LogUpPopup.prototype.CLoseLastWindow = function () {
        var _this = this;
        this.CompletePass.animate({ "top": "-100%" }, 500, function () { return _this.LogBox.hide(); });
    };
    LogUpPopup.prototype.FogotPawwsordClick = function () {
        this.LogBox.unbind("mouseup");
        this.PopUp.animate({ "top": "-100%" }, 500);
        this.FogotPassPopup.animate({ "top": "50%" }, 500);
    };
    LogUpPopup.prototype.ConfirmReset = function () {
        var email = $("#email_fogot").val();
        var telephone = $("#tel_number2_fogot");
        var tel = telephone.intlTelInput("getNumber");
        if ((!email && !tel) || (email && tel)) {
            //alert("You must select only one way");
            $("#youMustSelectOneWay").fadeIn("slow");
            return;
        }
        $("#youMustSelectOneWay").fadeOut("slow");
        if (!tel && !email.match(Patterns.email)) {
            $("#incorrectResetPassword").fadeIn("slow");
            return;
        }
        $("#incorrectResetPassword").fadeOut("slow");
        $.ajax({
            context: this,
            url: "/Account/FogotPassword",
            type: "post",
            data: { 'email': email, 'telephone': tel },
            async: true,
            success: function (result) {
                if (result === "True") {
                    alert(result);
                }
                else {
                    $("#email_fogot").css("border-color", "red");
                    console.log(result);
                }
            },
            error: function (e) {
                //alert('Error occured');
            }
        });
        this.FogotPassPopup.animate({ "top": "-100%" }, 500);
        this.CompletePass.animate({ "top": "50%" }, 500).find("#resetMathod").text(email ? "Email" : "Telephone");
    };
    LogUpPopup.prototype.CreateNewAccountEvent = function (singIn) {
        var _this = this;
        this.PopUp.find("#CreateNewAccount").click(function () {
            _this.PopUp.animate({ 'top': "-50%" }, 350, function () {
                _this.PopUp.parent().hide();
                singIn.Show();
            });
            //this.Close();
            //singIn.Show();
        });
    };
    return LogUpPopup;
}(LogIn));
//# sourceMappingURL=LogUpPopup.js.map