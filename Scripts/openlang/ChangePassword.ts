﻿declare function AlignHeight();
class ChangePasswordProvider {
    private oldPass: JQuery;
    private newPass: JQuery;
    private confirnPass: JQuery;
    private regExpressionValidate: RegExp;
    private checkState = "<span class=\"check_state\"><i class=\"fa fa-times error\"></i><i class=\"fa fa-check correct\"></i></span>";

    initialize(oldpass: JQuery, newpass: JQuery, confirmpass: JQuery):void {
        this.newPass = newpass;
        this.oldPass = oldpass;
        this.confirnPass = confirmpass;
        this.newPass.after(this.checkState);
        this.confirnPass.after(this.checkState);
    }

    initializeSates(regExpValid:string = null):void {
        this.bindChanges();
        if (regExpValid != null) {
            this.regExpressionValidate = new RegExp(regExpValid);
            this.regExpValidation();
        }
    }

    bindChanges():void {
        this.confirnPass.keyup(() => {
            if (!this.confirnPass.val()) {
                this.confirnPass.next().children().hide();
                return;
            }
            if (this.confirnPass.val() === this.newPass.val()) {
                this.confirnPass.next().find("i.correct").show().parent().find("i.error").hide();
            } else {
                this.confirnPass.next().find("i.error").show().parent().find("i.correct").hide();
            }
        });
    }

    regExpValidation(): void {
        this.newPass.keyup(() => {
            if (!this.newPass.val()) {
                this.newPass.next().children().hide();
                this.confirnPass.trigger("keyup");
                return;
            }
            if (this.regExpressionValidate.test(this.newPass.val())) {
                this.newPass.next(".check_state").find("i.correct").show().parent().find("i.error").hide();
                this.confirnPass.trigger("keyup");
            } else {
                this.newPass.next(".check_state").find("i.error").show().parent().find("i.correct").hide();
                this.confirnPass.trigger("keyup");
            }
        });

    }


    changePassword() {
        $.ajax({
            url: "/api/Password/ChangePassword",
            type: "post",
            beforeSend: (xhr) => BeforeSendAuthorizeApi.beforeSend(xhr),
            data: {
                OldPass: this.oldPass.val(),
                NewPass: this.newPass.val(),
                ConfirmPass: this.confirnPass.val()
            },
            success: (f) => {
                $("#updatedPassword").fadeIn("slow", () => {
                    setTimeout(() => {
                        $("#updatedPassword").fadeOut("slow", () => {
                            AlignHeight();
                        });
                    }, 5000);
                }).css('display', 'flex');
                AlignHeight();
                //Messager.show("PasswordUpdate");
                this.oldPass.val("");
                this.newPass.val("");
                this.confirnPass.val("");
                this.newPass.trigger("keyup");
            },
            error: (f) => {
                $("#uncorrectPassword").fadeIn("slow", () => {
                    setTimeout(() => {
                        $("#uncorrectPassword").fadeOut("slow");
                    }, 5000);
                }).css("display","flex");
                //Messager.show("UncorrectPassword");
                //alert(JSON.parse(f.responseText).Message);
            }
        });
    }
}