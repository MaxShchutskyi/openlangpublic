﻿class HTML5GeolocationManager extends AbstractGeolocationManager {
    public initLocation(callback: (locationInfo: any) => void) {
        navigator.geolocation.getCurrentPosition((pos) => {
            var coords = new Coords();
            coords.lat = pos.coords.latitude;
            coords.lng = pos.coords.longitude;
            this.chainFullStack(coords, callback);
        }, () => {
            if (this.Loosefull != null)
                this.Loosefull.init(callback);
        });
    }
    protected chainFullStack(coords: Coords, callback: (locationInfo: any) => void): void
    {
        var url = "//ws.geonames.org/countryCode?lat=" + coords.lat + "&lng=" + coords.lng + "&username=eugeny";
        $.get(url, result => {
            var res = result.replace(/(\r\n|\n|\r)/gm, "");
            var url2 = "//restcountries.eu/rest/v1/alpha/" + res;
            $.get(url2, result2 => {
                AbstractGeolocationManager.location = { version: AbstractGeolocationManager.version, country: result2.name, region: result2.region, code: result2.alpha2Code, currencies: result2.currencies };
                var url = "//ws.geonames.org/timezoneJSON?lat=" + coords.lat + "&lng=" + coords.lng + "&username=eugeny";
                $.getJSON(url, result => {
                    AbstractGeolocationManager.location.timezone = result.timezoneId;
                    localStorage.setItem("region", JSON.stringify(AbstractGeolocationManager.location));
                    callback(AbstractGeolocationManager.location);
                });
            });
        });          
    }
}