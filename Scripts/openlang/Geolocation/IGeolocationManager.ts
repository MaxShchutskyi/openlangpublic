﻿interface IGeolocationManager {
    initLocation(callback: (locationInfo: any) => void): void;
    init(callback: (locationInfo: any) => void): void;
}