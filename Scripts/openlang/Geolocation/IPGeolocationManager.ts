﻿class IPGeolocationManager extends AbstractGeolocationManager {
    public initLocation(callback: (locationInfo: any) => void) {
        $.getJSON("//ip-api.com/json/", result => {
            AbstractGeolocationManager.location = { version: AbstractGeolocationManager.version, country: result.country, region: result.regionName, code: result.countryCode, timezone: result.timezone };
            var url2 = "//restcountries.eu/rest/v1/alpha/" + result.countryCode;
            $.getJSON(url2, result2 => {
                AbstractGeolocationManager.location.currencies = result2.currencies;
                localStorage.setItem("region", JSON.stringify(AbstractGeolocationManager.location));
                callback(AbstractGeolocationManager.location);
            });
        }).fail(error => {
            console.log("her");
            if (this.Loosefull != null)
                this.Loosefull.init(callback);
        });
    }
}