var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var NullbleGeolocationManager = /** @class */ (function (_super) {
    __extends(NullbleGeolocationManager, _super);
    function NullbleGeolocationManager() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NullbleGeolocationManager.prototype.initLocation = function (callback) {
        AbstractGeolocationManager.location = { version: "-1", "country": "England", "region": "Europe", "code": "US", "currencies": ["EUR"], "timezone": "Greenwich Mean Time" };
        localStorage.setItem("region", JSON.stringify(AbstractGeolocationManager.location));
        callback(AbstractGeolocationManager.location);
    };
    return NullbleGeolocationManager;
}(AbstractGeolocationManager));
//# sourceMappingURL=NullbleGeolocationManager.js.map