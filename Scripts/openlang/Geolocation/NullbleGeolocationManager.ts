﻿class NullbleGeolocationManager extends AbstractGeolocationManager {
    public initLocation(callback: (locationInfo: any) => void) {
        AbstractGeolocationManager.location = { version: "-1", "country": "England", "region": "Europe", "code": "US", "currencies": ["EUR"], "timezone": "Greenwich Mean Time" };
        localStorage.setItem("region", JSON.stringify(AbstractGeolocationManager.location));
        callback(AbstractGeolocationManager.location);
    }
}