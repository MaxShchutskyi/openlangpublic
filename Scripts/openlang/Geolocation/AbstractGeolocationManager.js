var AbstractGeolocationManager = /** @class */ (function () {
    function AbstractGeolocationManager() {
    }
    AbstractGeolocationManager.prototype.init = function (callback) {
        if (AbstractGeolocationManager.location) {
            callback(AbstractGeolocationManager.location);
            return;
        }
        var region = localStorage.getItem("region");
        if (region) {
            AbstractGeolocationManager.location = JSON.parse(region);
            if (AbstractGeolocationManager.location.version === AbstractGeolocationManager.version)
                callback(AbstractGeolocationManager.location);
            else
                this.initLocation(callback);
        }
        else {
            this.initLocation(callback);
        }
    };
    AbstractGeolocationManager.version = "2.0";
    return AbstractGeolocationManager;
}());
//# sourceMappingURL=AbstractGeolocationManager.js.map