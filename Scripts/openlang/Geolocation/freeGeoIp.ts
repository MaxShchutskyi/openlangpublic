﻿class FreeGeoIp extends AbstractGeolocationManager
{
    public initLocation(callback: (locationInfo: any) => void) {
        $.getJSON("https://freegeoip.net/json/", result => {
            AbstractGeolocationManager.location = { version: AbstractGeolocationManager.version, country: result.country_name, region: result.region_name, code: result.country_code, timezone: result.time_zone };
            var url2 = "https://restcountries.eu/rest/v1/alpha/" + result.country_code;
            $.getJSON(url2, result2 => {
                AbstractGeolocationManager.location.currencies = result2.currencies;
                localStorage.setItem("region", JSON.stringify(AbstractGeolocationManager.location));
                callback(AbstractGeolocationManager.location);
            });
        }).fail(error => {
            if (this.Loosefull != null)
                this.Loosefull.init(callback);
        });
    }
}