var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var IPGeolocationManager = /** @class */ (function (_super) {
    __extends(IPGeolocationManager, _super);
    function IPGeolocationManager() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    IPGeolocationManager.prototype.initLocation = function (callback) {
        var _this = this;
        $.getJSON("//ip-api.com/json/", function (result) {
            AbstractGeolocationManager.location = { version: AbstractGeolocationManager.version, country: result.country, region: result.regionName, code: result.countryCode, timezone: result.timezone };
            var url2 = "//restcountries.eu/rest/v1/alpha/" + result.countryCode;
            $.getJSON(url2, function (result2) {
                AbstractGeolocationManager.location.currencies = result2.currencies;
                localStorage.setItem("region", JSON.stringify(AbstractGeolocationManager.location));
                callback(AbstractGeolocationManager.location);
            });
        }).fail(function (error) {
            console.log("her");
            if (_this.Loosefull != null)
                _this.Loosefull.init(callback);
        });
    };
    return IPGeolocationManager;
}(AbstractGeolocationManager));
//# sourceMappingURL=IPGeolocationManager.js.map