var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var HTML5GeolocationManager = /** @class */ (function (_super) {
    __extends(HTML5GeolocationManager, _super);
    function HTML5GeolocationManager() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    HTML5GeolocationManager.prototype.initLocation = function (callback) {
        var _this = this;
        navigator.geolocation.getCurrentPosition(function (pos) {
            var coords = new Coords();
            coords.lat = pos.coords.latitude;
            coords.lng = pos.coords.longitude;
            _this.chainFullStack(coords, callback);
        }, function () {
            if (_this.Loosefull != null)
                _this.Loosefull.init(callback);
        });
    };
    HTML5GeolocationManager.prototype.chainFullStack = function (coords, callback) {
        var url = "//ws.geonames.org/countryCode?lat=" + coords.lat + "&lng=" + coords.lng + "&username=eugeny";
        $.get(url, function (result) {
            var res = result.replace(/(\r\n|\n|\r)/gm, "");
            var url2 = "//restcountries.eu/rest/v1/alpha/" + res;
            $.get(url2, function (result2) {
                AbstractGeolocationManager.location = { version: AbstractGeolocationManager.version, country: result2.name, region: result2.region, code: result2.alpha2Code, currencies: result2.currencies };
                var url = "//ws.geonames.org/timezoneJSON?lat=" + coords.lat + "&lng=" + coords.lng + "&username=eugeny";
                $.getJSON(url, function (result) {
                    AbstractGeolocationManager.location.timezone = result.timezoneId;
                    localStorage.setItem("region", JSON.stringify(AbstractGeolocationManager.location));
                    callback(AbstractGeolocationManager.location);
                });
            });
        });
    };
    return HTML5GeolocationManager;
}(AbstractGeolocationManager));
//# sourceMappingURL=HTML5GeolocationManager.js.map