﻿abstract class AbstractGeolocationManager implements IGeolocationManager {
    public Loosefull: IGeolocationManager;
    public static version = "2.0";
    public static location: any;
    public init(callback: (locationInfo: any) => void): void {
        if (AbstractGeolocationManager.location) {
            callback(AbstractGeolocationManager.location);
            return;
        }
        var region = localStorage.getItem("region");
        if (region) {
            AbstractGeolocationManager.location = JSON.parse(region);
            if (AbstractGeolocationManager.location.version === AbstractGeolocationManager.version)
                callback(AbstractGeolocationManager.location);
            else
                this.initLocation(callback);
        }
        else {
            this.initLocation(callback);
        }
    }
    abstract initLocation(callback: (locationInfo: any) => void):void;
}