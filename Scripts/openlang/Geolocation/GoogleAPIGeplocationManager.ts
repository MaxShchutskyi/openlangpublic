﻿class GoogleAPIGeplocationManager extends HTML5GeolocationManager {
    initLocation(callback: (locationInfo: any) => void) {
        $.post("https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDYWctJmgBYQHfDRSRsUUIimrnns0nVt5k",(pos) => {
            var coords = new Coords();
            coords.lat = pos.location.lat;
            coords.lng = pos.location.lng;
            coords.accuracy = pos.accuracy;
            this.chainFullStack(coords, callback);

        }, "json").fail(() => {
            if (this.Loosefull != null)
                this.Loosefull.init(callback);
        });
    }
}