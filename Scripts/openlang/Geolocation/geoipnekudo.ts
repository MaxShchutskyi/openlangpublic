﻿class geoipnekudo extends AbstractGeolocationManager
{
    public initLocation(callback: (locationInfo: any) => void) {
        $.getJSON("https://geoip.nekudo.com/api/", result => {
            AbstractGeolocationManager.location =
                {
                    version: AbstractGeolocationManager.version,
                    country: result.country.name,
                    region: result.city,
                    code: result.country.code,
                    timezone: (!result.location.time_zone) ? "Greenwich Mean Time" : result.location.time_zone
                };
            var url2 = "https://restcountries.eu/rest/v1/alpha/" + result.country.code;
            $.getJSON(url2, result2 => {
                AbstractGeolocationManager.location.currencies = result2.currencies;
                localStorage.setItem("region", JSON.stringify(AbstractGeolocationManager.location));
                callback(AbstractGeolocationManager.location);
            });
        }).fail(error => {
            if (this.Loosefull != null)
                this.Loosefull.init(callback);
        });
    }
}