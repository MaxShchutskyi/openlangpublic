var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var geoipnekudo = /** @class */ (function (_super) {
    __extends(geoipnekudo, _super);
    function geoipnekudo() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    geoipnekudo.prototype.initLocation = function (callback) {
        var _this = this;
        $.getJSON("https://geoip.nekudo.com/api/", function (result) {
            AbstractGeolocationManager.location =
                {
                    version: AbstractGeolocationManager.version,
                    country: result.country.name,
                    region: result.city,
                    code: result.country.code,
                    timezone: (!result.location.time_zone) ? "Greenwich Mean Time" : result.location.time_zone
                };
            var url2 = "https://restcountries.eu/rest/v1/alpha/" + result.country.code;
            $.getJSON(url2, function (result2) {
                AbstractGeolocationManager.location.currencies = result2.currencies;
                localStorage.setItem("region", JSON.stringify(AbstractGeolocationManager.location));
                callback(AbstractGeolocationManager.location);
            });
        }).fail(function (error) {
            if (_this.Loosefull != null)
                _this.Loosefull.init(callback);
        });
    };
    return geoipnekudo;
}(AbstractGeolocationManager));
//# sourceMappingURL=geoipnekudo.js.map