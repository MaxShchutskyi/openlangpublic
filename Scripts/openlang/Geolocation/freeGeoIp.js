var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var FreeGeoIp = /** @class */ (function (_super) {
    __extends(FreeGeoIp, _super);
    function FreeGeoIp() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FreeGeoIp.prototype.initLocation = function (callback) {
        var _this = this;
        $.getJSON("https://freegeoip.net/json/", function (result) {
            AbstractGeolocationManager.location = { version: AbstractGeolocationManager.version, country: result.country_name, region: result.region_name, code: result.country_code, timezone: result.time_zone };
            var url2 = "https://restcountries.eu/rest/v1/alpha/" + result.country_code;
            $.getJSON(url2, function (result2) {
                AbstractGeolocationManager.location.currencies = result2.currencies;
                localStorage.setItem("region", JSON.stringify(AbstractGeolocationManager.location));
                callback(AbstractGeolocationManager.location);
            });
        }).fail(function (error) {
            if (_this.Loosefull != null)
                _this.Loosefull.init(callback);
        });
    };
    return FreeGeoIp;
}(AbstractGeolocationManager));
//# sourceMappingURL=freeGeoIp.js.map