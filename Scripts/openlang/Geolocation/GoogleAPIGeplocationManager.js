var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var GoogleAPIGeplocationManager = /** @class */ (function (_super) {
    __extends(GoogleAPIGeplocationManager, _super);
    function GoogleAPIGeplocationManager() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    GoogleAPIGeplocationManager.prototype.initLocation = function (callback) {
        var _this = this;
        $.post("https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDYWctJmgBYQHfDRSRsUUIimrnns0nVt5k", function (pos) {
            var coords = new Coords();
            coords.lat = pos.location.lat;
            coords.lng = pos.location.lng;
            coords.accuracy = pos.accuracy;
            _this.chainFullStack(coords, callback);
        }, "json").fail(function () {
            if (_this.Loosefull != null)
                _this.Loosefull.init(callback);
        });
    };
    return GoogleAPIGeplocationManager;
}(HTML5GeolocationManager));
//# sourceMappingURL=GoogleAPIGeplocationManager.js.map