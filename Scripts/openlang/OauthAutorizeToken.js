var OAuthToken = /** @class */ (function () {
    function OAuthToken() {
    }
    OAuthToken.setTokenOnly = function (userName, redirect) {
        if (sessionStorage.getItem('token')) {
            OAuthToken.successResponses.forEach(function (elem) {
                elem(true);
            });
            return;
        }
        this.loginData.username = userName,
            $.ajax({
                type: 'POST',
                url: '/Token',
                data: this.loginData,
                success: function (data) {
                    sessionStorage.setItem('token', data.access_token);
                    OAuthToken.successResponses.forEach(function (elem) {
                        elem(true);
                    });
                    if (redirect)
                        location.href = redirect;
                    //OAuthToken.successResponse(true);
                },
                error: function (err) { return alert(resource.match('login_error') + err); }
            });
    };
    OAuthToken.successResponses = [];
    OAuthToken.loginData = {
        grant_type: 'password',
        username: '',
        password: ''
    };
    return OAuthToken;
}());
//# sourceMappingURL=OauthAutorizeToken.js.map