﻿
function AlignForHightElement(element) {
    var child = $(element).children();
    var f = 0;
    for (var i = 0; i < child.length; i++) {
        if (f < $(child[i]).height())
            f = $(child[i]).height();
    }
    child.height(f);
}


function AlignForTopCenter(element) {
    var parent = $(element).parent();
    var elem = $(element);
    var mrg = (parent.outerHeight(true) - elem.height()) / 2;
    elem.css({
        "margin-top": mrg
    });
}


function SearchByLanguageIDAC(id) {
     $('.pad_load').fadeIn("medium");
    $.ajax({
        url: 'Home/SearchByLanguageID',
        dataType: 'json',
        type: 'POST',
        data: { languageId: id },
        success: function (data) {
            dataAc = data;
            FillTable(data);
            $('.pad_load').fadeOut("medium");
        },
        error: function () {
            //alert('error');
            $('.pad_load').fadeOut("medium");
        }
    });
};
function SearchByLanguageIDCN(id) {
    $('.pad_load').fadeIn("medium");
    var timeFrom = $("#timeFromForm").val().split(':');
    var timeTo = $("#timeToForm").val().split(':');

    var from = new Date();
    from.setHours(timeFrom[0], timeFrom[1], 0);
    var to = new Date();
    to.setHours(timeTo[0], timeTo[1], 0);

    $.ajax({
        url: 'Home/SearchByLanguageIDCN',
        dataType: 'json',
        type: 'POST',
        data: { languageId: id, timeTo: to.toString().split(' (')[0], timeFrom: from.toString().split(' (')[0] },
        success: function (data) {
            dataCn = data;
            FillTableCN(data);
            $('.pad_load').fadeOut("medium");
        },
        error: function () {
            //alert('error');
            $('.pad_load').fadeOut("medium");
        }
    });
};


$(document).ready(function () {
    var dataAc;
    var dataCn;
    var dataRc;

    $('.donut').peity('donut');
    var signAC = 0;
    var signCN = 0;

    $('.ownwidth li').click(function(e) {
        $('.smm').attr('src', $(this).find('img').attr('src'));
    });
    //$('#countryBirth').selectmenu();
    //$('#nationality').selectmenu();
    $('.infos').tooltip();
    //AJAX
     $('.pad_load').fadeIn("medium");
    
    $('#search').keypress(function (e) {
        if (e.which == 13) {
            Search();
        }
    });
    $('#searchCN').keypress(function (e) {
        if (e.which == 13) {
            SearchCN();
        }
    });
    $('#searchRC').keypress(function (e) {
        if (e.which == 13) {
            SearchRC();
        }
    });
     $('.pad_load').fadeIn("medium");
    //account
    GetUser();
    
    //languages
    $.ajax({
        url: 'Home/GetLanguages',
        dataType: 'json',
        type: 'POST',
        success: function (data) {
            var object = {
                text: data[0].Abbreviation,
                value: data[0].Id,
                description: data[0].Name.split('|')[0] + " Classes",
                imageSrc: data[0].ImageSrc
            };
            $.each(data, function(k) {
                object = {
                    text: data[k].Name.split('|')[0] + " Classes",
                    value: data[k].Id,
                    imageSrc: data[k].ImageSrc
                };
                ddata.push(object);
            });
            $('#MyDropDownAC').ddslick({
                data: ddata,
                width: 300,
                defaultSelectedIndex:2,
                imagePosition: "left",
                onSelected: function (value) {
                    if (signAC > 0) {
                        SearchByLanguageIDAC(value.selectedData.value);
                        $('.form-control').css("margin-top", "3px");
                        $('.input-group-btn').css("top", "2px");
                    } else {
                        signAC++;
                    }
                }
            });
            $('#MyDropDownCN').ddslick({
                data: ddata,
                width: 300,
                defaultSelectedIndex: 2,
                imagePosition: "left",
                onSelected: function (value) {
                    if (signCN > 0) {
                        SearchByLanguageIDCN(value.selectedData.value);
                        $('.form-control').css("margin-top", "3px");
                        $('.input-group-btn').css("top", "2px");
                    } else {
                        signCN++;
                    }
                }
            });
        },
        error: function () {
            //alert('error');
            $('.pad_load').fadeOut("medium");
        }
    });
    //$('#MyDropDown').on('change', function () {
    //    SearchByLanguageID(0);
    //    $('.form-control').css("margin-top", "3px");
    //    $('.input-group-btn').css("top", "2px");
    //});
    //Controls
    
    AlignForHightElement('.align_height');
    AlignForTopCenter('.align_for_hight_center');
    $("#month").slider({
            min: 0,
            max: months.length - 1,
            value: activeMonth,
            animate: true
        })

        .slider("pips", {
            rest: "label",
            labels: months,
        }).on("slidechange", function (e, ui) {
            sliderChange(ui.value + 1);
        });

    $("#monthRC").slider({
        rest:"label",
        min: 0,
        max: months.length - 1,
        value: activeMonth,
        animate: true
    })

        .slider("pips", {
            rest: "label",
            labels: months
        }).on("slidechange", function (e, ui) {
            sliderChangeRC(ui.value + 1);
        });

    $("#yearRC").slider({
        min: 0,
        max: years.length - 1,
        value: activeYear,
        animate: true
    })

    .slider("pips", {
        rest: "label",
        labels: years,
    }).on("slidechange", function (e, ui) {
        sliderChange2RC(years[ui.value]);
    });

    $("#year").slider({
        min: 0,
        max: years.length - 1,
        value: activeYear,
        animate: true
    })

    .slider("pips", {
        rest: "label",
        labels: years,
    }).on("slidechange", function (e, ui) {
        sliderChange2(years[ui.value]);
    });

    $("#generalDd").on("selectmenuchange", function (event, ui) {
        SearchByTopic(ui.item.value, $("#specificDd option:selected").text());
    });
    $("#specificDd").on("selectmenuchange", function (event, ui) {
        SearchByTopic($("#generalDd option:selected").val(), ui.item.label);
    });
    $("#generalDdCN").on("selectmenuchange", function (event, ui) {
        SearchByTopicCN(ui.item.value, $("#specificDdCN option:selected").text());
    });
    $("#specificDdCN").on("selectmenuchange", function (event, ui) {
        SearchByTopicCN($("#generalDdCN option:selected").val(), ui.item.label);
    });

    $('.pad_load').fadeOut("medium");
    $('#datetimepicker1').datetimepicker({
        format: 'DD.MM.YYYY'
    });
    $('#datetimepicker2').datetimepicker({
        format: 'DD.MM.YYYY'
    });
    $('#timefromformID').datetimepicker({
        format: 'HH:mm',
        defaultDate: new Date()
    });
    $('#timetoformID').datetimepicker({
        format: 'HH:mm',
        defaultDate: new Date(2010, 10, 10, 23, 59, 59)
    });
    $('#datetimepickerRC').datetimepicker({
        format: 'DD.MM.YYYY'
    });
    $('#datetimepickerRC2').datetimepicker({
        format: 'DD.MM.YYYY'
    });

    //Account
    $('#datetimepickerBirth').datetimepicker({
        format: 'DD.MM.YYYY'
    });


    $("#dateFromForm").val($.datepicker.formatDate('dd.mm.yy', new Date()));
    $("#dateToForm").val($.datepicker.formatDate('dd.mm.yy', new Date(2020, 0, 01, 0, 0, 0)));

    $("#dateFromFormRC").val($.datepicker.formatDate('dd.mm.yy', new Date()));
    $("#dateToFormRC").val($.datepicker.formatDate('dd.mm.yy', new Date(2020, 0, 01, 0, 0, 0)));
    var date = new Date();
    var weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var monthss = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    
    $('#currentDate').text(weekdays[date.getDay()] + ", " + monthss[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear());

});

//function HeightOfParent(element) {
//    $(element).height($('div.col-md-9.right_content').height());
//}
var months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEPT", "OCT", "NOV", "DEC"];
var years = [2015, 2016];
var activeMonth = new Date().getMonth();
var activeYear = 0; //new Date().getYear();

var ddata = [];