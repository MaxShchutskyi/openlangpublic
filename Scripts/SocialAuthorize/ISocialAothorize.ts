﻿interface ISocialAuthorize{
    apiClass: any;
    clientId: string;
    scopes: string;
    callRequest(): Boolean;
    responseCallback: (userResplonse: any, type:string) => void;
}