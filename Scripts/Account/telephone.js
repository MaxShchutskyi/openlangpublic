var code = "";
(function (e) {
	
    if (typeof define === "function" && define.amd) {
        define(["jquery"], function (t) {
            e(t, window, document)
        })
    } else {
        e(jQuery, window, document)
    }
})(function (e, t, n, r) {
    "use strict";

    function f(t, n) {
        this.element = t;
        this.options = e.extend({}, o, n);
        this._defaults = o;
        this.ns = "." + i + s++;
        this.isGoodBrowser = Boolean(t.setSelectionRange);
        this.hadInitialPlaceholder = Boolean(e(t).attr("placeholder"));
        this._name = i;
        this.init()
    }
    var i = "intlTelInput",
        s = 1,
        o = {
            autoFormat: true,
            autoHideDialCode: true,
            defaultCountry: "",
            ipinfoToken: "",
            nationalMode: "",
            numberType: "MOBILE",
            onlyCountries: [],
            preferredCountries: ["us", "gb"],
            responsiveDropdown: false,
            utilsScript: ""
        },
        u = {
            UP: 38,
            DOWN: 40,
            ENTER: 13,
            ESC: 27,
            PLUS: 43,
            A: 65,
            Z: 90,
            ZERO: 48,
            NINE: 57,
            SPACE: 32,
            BSPACE: 8,
            DEL: 46,
            CTRL: 17,
            CMD1: 91,
            CMD2: 224
        },
        a = false;
    e(t).load(function () {
        a = true
    });
    f.prototype = {
        init: function () {
            var t = this;
            if (this.options.defaultCountry == "auto") {
                this.options.defaultCountry = "";
                var n = "//ipinfo.io";
                if (this.options.ipinfoToken) {
                    n += "?token=" + this.options.ipinfoToken
                }
                e.get(n, function (e) {
                    if (e && e.country) {
                        t.options.defaultCountry = e.country.toLowerCase()
                    }
                }, "jsonp").always(function () {
                    t._ready()
                })
            } else {
                this._ready()
            }
        },
        _ready: function () {
            if (this.options.nationalMode) {
                this.options.autoHideDialCode = false
            }
            if (navigator.userAgent.match(/IEMobile/i)) {
                this.options.autoFormat = false
            }
            if (t.innerWidth < 500) {
                this.options.responsiveDropdown = true
            }
            this._processCountryData();
            this._generateMarkup();
            this._setInitialState();
            this._initListeners()
        },
        _processCountryData: function () {
            this._setInstanceCountryData();
            this._setPreferredCountries()
        },
        _addCountryCode: function (e, t, n) {
            if (!(t in this.countryCodes)) {
                this.countryCodes[t] = []
            }
            var r = n || 0;
            this.countryCodes[t][r] = e
        },
        _setInstanceCountryData: function () {
            var t;
            if (this.options.onlyCountries.length) {
                this.countries = [];
                for (t = 0; t < l.length; t++) {
                    if (e.inArray(l[t].iso2, this.options.onlyCountries) != -1) {
                        this.countries.push(l[t])
                    }
                }
            } else {
                this.countries = l
            }
            this.countryCodes = {};
            for (t = 0; t < this.countries.length; t++) {
                var n = this.countries[t];
                this._addCountryCode(n.iso2, n.dialCode, n.priority);
                if (n.areaCodes) {
                    for (var r = 0; r < n.areaCodes.length; r++) {
                        this._addCountryCode(n.iso2, n.dialCode + n.areaCodes[r])
                    }
                }
            }
        },
        _setPreferredCountries: function () {
            this.preferredCountries = [];
            for (var e = 0; e < this.options.preferredCountries.length; e++) {
                var t = this.options.preferredCountries[e],
                    n = this._getCountryData(t, false, true);
                if (n) {
                    this.preferredCountries.push(n)
                }
            }
        },
        _generateMarkup: function () {
            
            this.telInput = e(this.element);
            this.telInput.wrap(e("<div>", {
                "class": "intl-tel-input"
            }));
            var t = e("<div>", {
                "class": "flag-dropdown"
            }).insertAfter(this.telInput);
            var n = e("<div>", {
                "class": "selected-flag"
            }).appendTo(t);
            this.selectedFlagInner = e("<div>", {
                "class": "flag"
            }).appendTo(n);
            e("<div>", {
                "class": "arrow"
            }).appendTo(this.selectedFlagInner);
            this.countryList = e("<ul>", {
                "class": "country-list v-hide"
            }).appendTo(t);
            if (this.preferredCountries.length) {
                this._appendListItems(this.preferredCountries, "preferred");
                e("<li>", {
                    "class": "divider"
                }).appendTo(this.countryList)
            }
            this._appendListItems(this.countries, "");
            this.dropdownHeight = this.countryList.outerHeight();
            this.countryList.removeClass("v-hide").addClass("hide");
            if (this.options.responsiveDropdown) {
                this.countryList.outerWidth(this.telInput.outerWidth())
            }
            this.countryListItems = this.countryList.children(".country")
        },
        _appendListItems: function (e, t) {
            var n = "";
            for (var r = 0; r < e.length; r++) {
                var i = e[r];
                n += "<li class='country " + t + "' data-dial-code='" + i.dialCode + "' data-country-code='" + i.iso2 + "'>";
                n += "<div class='flag " + i.iso2 + "'></div>";
                n += "<span class='country-name'>" + i.name + "</span>";
                n += "<span class='dial-code'>+" + i.dialCode + "</span>";
                n += "</li>"
            }
            this.countryList.append(n)
        },
        _setInitialState: function () {
			
            var t = this.telInput.val();
            if (this._getDialCode(t)) {
                this._updateFlagFromNumber(t)
            } else {
                var n;
                if (this.options.defaultCountry) {
                    n = this._getCountryData(this.options.defaultCountry, false, false)
                } else {
                    n = this.preferredCountries.length ? this.preferredCountries[0] : this.countries[0]
                }
                this._selectFlag(n.iso2);
                if (!t) {
					
                    code = n.dialCode;
					
                    e("span.country-tel-code1").text(n.dialCode);
                    this._updateDialCode(n.dialCode, false)
                }
            }
            if (t) {
                this._updateVal(t, false)
            }
        },
        _initListeners: function () {
            var n = this;
            this._initKeyListeners();
            if (this.options.autoHideDialCode || this.options.autoFormat) {
                this._initFocusListeners()
            }
            var r = this.telInput.closest("label");
            if (r.length) {
                r.on("click" + this.ns, function (e) {
                    if (n.countryList.hasClass("hide")) {
                        n.telInput.focus()
                    } else {
                        e.preventDefault()
                    }
                })
            }
            var i = this.selectedFlagInner.parent();
            i.on("click" + this.ns, function (e) {
                if (n.countryList.hasClass("hide") && !n.telInput.prop("disabled")) {
                    n._showDropdown();
                }
            });
			i.on("keypress" + this.ns, function(e){
                if (e.keyCode == 13) {
                    if (n.countryList.hasClass("hide") && !n.telInput.prop("disabled")) {
                    n._showDropdown();
                    }
                }
            });
            if (this.options.utilsScript) {
                if (a) {
                    this.loadUtils()
                } else {
                    e(t).load(function () {
                        n.loadUtils()
                    })
                }
            }
        },
        _initKeyListeners: function () {
            var e = this;
            if (this.options.autoFormat) {
                this.telInput.on("keypress" + this.ns, function (n) {
                    if (n.which >= u.SPACE && !n.metaKey && t.intlTelInputUtils) {
                        n.preventDefault();
                        var r = n.which >= u.ZERO && n.which <= u.NINE || n.which == u.PLUS,
                            i = e.telInput[0],
                            s = e.isGoodBrowser && i.selectionStart == i.selectionEnd,
                            o = e.telInput.attr("maxlength"),
                            a = o ? e.telInput.val().length < o : true;
                        if (a && (r || s)) {
                            var f = r ? String.fromCharCode(n.which) : null;
                            e._handleInputKey(f, true)
                        }
                        if (!r) {
                            e.telInput.trigger("invalidkey")
                        }
                    }
                })
            }
            this.telInput.on("keyup" + this.ns, function (n) {
                if (n.which == u.ENTER) { } else if (e.options.autoFormat && t.intlTelInputUtils) {
                    var r = n.which == u.CTRL || n.which == u.CMD1 || n.which == u.CMD2,
                        i = e.telInput[0],
                        s = e.isGoodBrowser && i.selectionStart == i.selectionEnd,
                        o = e.isGoodBrowser && i.selectionStart == e.telInput.val().length;
                    if (n.which == u.DEL && !o || n.which == u.BSPACE || r && s) {
                        e._handleInputKey(null, r && o)
                    }
                    if (!e.options.nationalMode) {
                        var a = e.telInput.val();
                        if (a.substr(0, 1) != "+") {
                            var f = e.isGoodBrowser ? i.selectionStart + 1 : 0;
                            e.telInput.val("+" + a);
                            if (e.isGoodBrowser) {
                                i.setSelectionRange(f, f)
                            }
                        }
                    }
                } else {
                    e._updateFlagFromNumber(e.telInput.val())
                }
            })
        },
        _handleInputKey: function (e, t) {
            var n = this.telInput.val(),
                r = null,
                i = false,
                s = this.telInput[0];
            if (this.isGoodBrowser) {
                var o = s.selectionEnd,
                    u = n.length;
                i = o == u;
                if (e) {
                    n = n.substr(0, s.selectionStart) + e + n.substring(o, u);
                    if (!i) {
                        r = o + (n.length - u)
                    }
                } else {
                    r = s.selectionStart
                }
            } else if (e) {
                n += e
            }
            this.setNumber(n, t);
            if (this.isGoodBrowser) {
                if (i) {
                    r = this.telInput.val().length
                }
                s.setSelectionRange(r, r)
            }
        },
        _initFocusListeners: function () {
            var e = this;
            if (this.options.autoHideDialCode) {
                this.telInput.on("mousedown" + this.ns, function (t) {
                    if (!e.telInput.is(":focus") && !e.telInput.val()) {
                        t.preventDefault();
                        e.telInput.focus()
                    }
                })
            }
            this.telInput.on("focus" + this.ns, function () {
                var n = e.telInput.val();
                e.telInput.data("focusVal", n);
                if (e.options.autoHideDialCode) {
                    if (!n) {
                        e._updateVal("+" + e.selectedCountryData.dialCode, true);
                        e.telInput.one("keypress.plus" + e.ns, function (n) {
                            if (n.which == u.PLUS) {
                                var r = e.options.autoFormat && t.intlTelInputUtils ? "+" : "";
                                e.telInput.val(r)
                            }
                        });
                        setTimeout(function () {
                            var t = e.telInput[0];
                            if (e.isGoodBrowser) {
                                var n = e.telInput.val().length;
                                t.setSelectionRange(n, n)
                            }
                        })
                    }
                }
            });
            this.telInput.on("blur" + this.ns, function () {
                if (e.options.autoHideDialCode) {
                    var n = e.telInput.val(),
                        r = n.substr(0, 1) == "+";
                    if (r) {
                        var i = e._getNumeric(n);
                        if (!i || e.selectedCountryData.dialCode == i) {
                            e.telInput.val("")
                        }
                    }
                    e.telInput.off("keypress.plus" + e.ns)
                }
                if (e.options.autoFormat && t.intlTelInputUtils && e.telInput.val() != e.telInput.data("focusVal")) {
                    e.telInput.trigger("change")
                }
            })
        },
        _getNumeric: function (e) {
            return e.replace(/\D/g, "")
        },
        _showDropdown: function () {
            this._setDropdownPosition();
            var e = this.countryList.children(".active");
            this._highlightListItem(e);
            this.countryList.removeClass("hide");
            this._scrollTo(e);
            this._bindDropdownListeners();
            this.selectedFlagInner.children(".arrow").addClass("up")
        },
        _setDropdownPosition: function () {
            var n = this.telInput.offset().top,
                r = e(t).scrollTop(),
                i = n + this.telInput.outerHeight() + this.dropdownHeight < r + e(t).height(),
                s = n - this.dropdownHeight > r;
            var o = !i && s ? "-" + (this.dropdownHeight - 1) + "px" : "";
            this.countryList.css("top", o)
        },
        _bindDropdownListeners: function () {
            var t = this;
            this.countryList.on("mouseover" + this.ns, ".country", function (n) {
                t._highlightListItem(e(this))
            });
            this.countryList.on("click" + this.ns, ".country", function (n) {
                t._selectListItem(e(this))
            });
            var r = true;
            e("html").on("click" + this.ns, function (e) {
                if (!r) {
                    t._closeDropdown()
                }
                r = false
            });
            var i = "",
                s = null;
            e(n).on("keydown" + this.ns, function (e) {
                e.preventDefault();
                if (e.which == u.UP || e.which == u.DOWN) {
                    t._handleUpDownKey(e.which)
                } else if (e.which == u.ENTER) {
                    t._handleEnterKey()
                } else if (e.which == u.ESC) {
                    t._closeDropdown()
                } else if (e.which >= u.A && e.which <= u.Z || e.which == u.SPACE) {
                    if (s) {
                        clearTimeout(s)
                    }
                    i += String.fromCharCode(e.which);
                    t._searchForCountry(i);
                    s = setTimeout(function () {
                        i = ""
                    }, 1e3)
                }
            })
        },
        _handleUpDownKey: function (e) {
            var t = this.countryList.children(".highlight").first();
            var n = e == u.UP ? t.prev() : t.next();
            if (n.length) {
                if (n.hasClass("divider")) {
                    n = e == u.UP ? n.prev() : n.next()
                }
                this._highlightListItem(n);
                this._scrollTo(n)
            }
        },
        _handleEnterKey: function () {
            var e = this.countryList.children(".highlight").first();
            if (e.length) {
                this._selectListItem(e)
            }
        },
        _searchForCountry: function (e) {
            for (var t = 0; t < this.countries.length; t++) {
                if (this._startsWith(this.countries[t].name, e)) {
                    var n = this.countryList.children("[data-country-code=" + this.countries[t].iso2 + "]").not(".preferred");
                    this._highlightListItem(n);
                    this._scrollTo(n, true);
                    break
                }
            }
        },
        _startsWith: function (e, t) {
            return e.substr(0, t.length).toUpperCase() == t
        },
        _updateVal: function (e, n) {
			
            var r;
            if (this.options.autoFormat && t.intlTelInputUtils) {
                r = intlTelInputUtils.formatNumber(e, this.selectedCountryData.iso2, n);
                var i = this.telInput.attr("maxlength");
                if (i && r.length > i) {
                    r = r.substr(0, i)
                }
            } else {
                r = e
            }
            this.telInput.val(r)
        },
        _updateFlagFromNumber: function (e) {
            if (this.options.nationalMode && this.selectedCountryData && this.selectedCountryData.dialCode == "1" && e.substr(0, 1) != "+") {
                e = "+1" + e
            }
            var t = this._getDialCode(e);
            if (t) {
                var n = this.countryCodes[this._getNumeric(t)],
                    r = false;
                if (this.selectedCountryData) {
                    for (var i = 0; i < n.length; i++) {
                        if (n[i] == this.selectedCountryData.iso2) {
                            r = true
                        }
                    }
                }
                if (!r || this._isUnknownNanp(e, t)) {
                    for (var s = 0; s < n.length; s++) {
                        if (n[s]) {
                            this._selectFlag(n[s]);
                            break
                        }
                    }
                }
            }
        },
        _isUnknownNanp: function (e, t) {
            return t == "+1" && this._getNumeric(e).length >= 4
        },
        _highlightListItem: function (e) {
            this.countryListItems.removeClass("highlight");
            e.addClass("highlight")
        },
        _getCountryData: function (e, t, n) {
			
            var r = t ? l : this.countries;
            for (var i = 0; i < r.length; i++) {
                if (r[i].iso2 == e) {
                    return r[i]
                }
            }
            if (n) {
                return null
            } else {
                throw new Error("No country data for '" + e + "'")
            }
        },
        _selectFlag: function (e) {
			
            this.selectedCountryData = this._getCountryData(e, false, false);
            this.selectedFlagInner.attr("class", "flag " + e);
            var t = this.selectedCountryData.name + ": +" + this.selectedCountryData.dialCode;
            this.selectedFlagInner.parent().attr("title", t);
            this._updatePlaceholder();
            var n = this.countryListItems.children(".flag." + e).first().parent();
            this.countryListItems.removeClass("active");
            n.addClass("active")
        },
        _updatePlaceholder: function () {
			
            if (t.intlTelInputUtils && !this.hadInitialPlaceholder) {
                
                var n = this.selectedCountryData.iso2,
                    r = intlTelInputUtils.numberType[this.options.numberType || "FIXED_LINE"],
                    i = intlTelInputUtils.getExampleNumber(n, this.options.nationalMode, r);
                var s = e("#myTabContent1 li.highlight span.country-name").text();
                var o = e("#myTabContent1 li.highlight span.dial-code").text();
                if (o == "") {
					if(code.indexOf("+")>-1)
					{
					 e("#myTabContent1 .country-tel-code1").text(code)
					}
					else	
					{
						code="+"+code;
					 e("#myTabContent1 .country-tel-code1").text(code)	
					}
                   
                } else {
                    e("#myTabContent1 .country-tel-code1").text(o)
                }
            }
        },
        _selectListItem: function (e) {
            var t = e.attr("data-country-code");
            this._selectFlag(t);
            this._closeDropdown();
            this._updateDialCode(e.attr("data-dial-code"), true);
            this.telInput.trigger("change");
            this.telInput.focus()
        },
        _closeDropdown: function () {
            this.countryList.addClass("hide");
            this.selectedFlagInner.children(".arrow").removeClass("up");
            e(n).off(this.ns);
            e("html").off(this.ns);
            this.countryList.off(this.ns)
        },
        _scrollTo: function (e, t) {
            var n = this.countryList,
                r = n.height(),
                i = n.offset().top,
                s = i + r,
                o = e.outerHeight(),
                u = e.offset().top,
                a = u + o,
                f = u - i + n.scrollTop(),
                l = r / 2 - o / 2;
            if (u < i) {
                if (t) {
                    f -= l
                }
                n.scrollTop(f)
            } else if (a > s) {
                if (t) {
                    f += l
                }
                var c = r - o;
                n.scrollTop(f - c)
            }
        },
        _updateDialCode: function (t, n) {
			//debugger;
            var r = this.telInput.val(),
                i;
            t = "+" + t;
            if (this.options.nationalMode && r.substr(0, 1) != "+") {
                i = r
            } else if (r) {
                var s = this._getDialCode(r);
                if (s.length > 1) {
                    i = r.replace(s, t)
                } else {
                    var o = r.substr(0, 1) != "+" ? e.trim(r) : "";
                    i = t + o
                }
            } else {
                i = !this.options.autoHideDialCode || n ? t : ""
            }
            this._updateVal(i, n)
        },
        _getDialCode: function (t) {
            var n = "";
            if (t.charAt(0) == "+") {
                var r = "";
                for (var i = 0; i < t.length; i++) {
                    var s = t.charAt(i);
                    if (e.isNumeric(s)) {
                        r += s;
                        if (this.countryCodes[r]) {
                            n = t.substr(0, i + 1)
                        }
                        if (r.length == 4) {
                            break
                        }
                    }
                }
            }
            return n
        },
        destroy: function () {
            this._closeDropdown();
            this.telInput.off(this.ns);
            this.selectedFlagInner.parent().off(this.ns);
            this.telInput.closest("label").off(this.ns);
            var e = this.telInput.parent();
            e.before(this.telInput).remove()
        },
        getCleanNumber: function () {
            if (t.intlTelInputUtils) {
                return intlTelInputUtils.formatNumberE164(this.telInput.val(), this.selectedCountryData.iso2)
            }
            return ""
        },
        getNumberType: function () {
            if (t.intlTelInputUtils) {
                return intlTelInputUtils.getNumberType(this.telInput.val(), this.selectedCountryData.iso2)
            }
            return -99
        },
        getSelectedCountryData: function () {
            return this.selectedCountryData || {}
        },
        getValidationError: function () {
            if (t.intlTelInputUtils) {
                return intlTelInputUtils.getValidationError(this.telInput.val(), this.selectedCountryData.iso2)
            }
            return -99
        },
        isValidNumber: function () {
            var n = e.trim(this.telInput.val()),
                r = this.options.nationalMode ? this.selectedCountryData.iso2 : "",
                i = /[a-zA-Z]/.test(n);
            if (!i && t.intlTelInputUtils) {
                return intlTelInputUtils.isValidNumber(n, r)
            }
            return false
        },
        loadUtils: function (t) {
			
            var n = t || this.options.utilsScript;
            if (!e.fn[i].loadedUtilsScript && n) {
                e.fn[i].loadedUtilsScript = true;
                e.ajax({
                    url: n,
                    success: function () {
                        e(".intl-tel-input input#mobile-number1").intlTelInput("utilsLoaded")
                    },
                    dataType: "script",
                    cache: true
                })
            }
        },
        selectCountry: function (e) {
            if (!this.selectedFlagInner.hasClass(e)) {
                this._selectFlag(e);
                this._updateDialCode(this.selectedCountryData.dialCode, false)
            }
        },
        setNumber: function (e, t) {
            if (!this.options.nationalMode && e.substr(0, 1) != "+") {
                e = "+" + e
            }
            this._updateFlagFromNumber(e);
            this._updateVal(e, t)
        },
        utilsLoaded: function () {
			//debugger;
            if (this.options.autoFormat && this.telInput.val()) {
                this._updateVal(this.telInput.val())
            }
            this._updatePlaceholder()
        }
    };
    e.fn[i] = function (t) {
        var n = arguments;
        if (t === r || typeof t === "object") {
            return this.each(function () {
                if (!e.data(this, "plugin_" + i)) {
                    e.data(this, "plugin_" + i, new f(this, t))
                }
            })
        } else if (typeof t === "string" && t[0] !== "_" && t !== "init") {
            var s;
            this.each(function () {
                var r = e.data(this, "plugin_" + i);
                if (r instanceof f && typeof r[t] === "function") {
                    s = r[t].apply(r, Array.prototype.slice.call(n, 1))
                }
                if (t === "destroy") {
                    e.data(this, "plugin_" + i, null)
                }
            });
            return s !== r ? s : this
        }
    };
    e.fn[i].getCountryData = function () {
        return l
    };
    e.fn[i].setCountryData = function (e) {
        l = e
    };
    var l = [
        ["Afghanistan (‫افغانستان‬‎)", "af", "93"],
        ["Albania (Shqipëri)", "al", "355"],
        ["Algeria (‫الجزائر‬‎)", "dz", "213"],
        ["American Samoa", "as", "1684"],
        ["Andorra", "ad", "376"],
        ["Angola", "ao", "244"],
        ["Anguilla", "ai", "1264"],
        ["Antigua and Barbuda", "ag", "1268"],
        ["Argentina", "ar", "54"],
        ["Armenia (Հայաստան)", "am", "374"],
        ["Aruba", "aw", "297"],
        ["Australia", "au", "61"],
        ["Austria (Österreich)", "at", "43"],
        ["Azerbaijan (Azərbaycan)", "az", "994"],
        ["Bahamas", "bs", "1242"],
        ["Bahrain (‫البحرين‬‎)", "bh", "973"],
        ["Bangladesh (বাংলাদেশ)", "bd", "880"],
        ["Barbados", "bb", "1246"],
        ["Belarus (Беларусь)", "by", "375"],
        ["Belgium (België)", "be", "32"],
        ["Belize", "bz", "501"],
        ["Benin (Bénin)", "bj", "229"],
        ["Bermuda", "bm", "1441"],
        ["Bhutan (འབྲུག)", "bt", "975"],
        ["Bolivia", "bo", "591"],
        ["Bosnia and Herzegovina (Босна и Херцеговина)", "ba", "387"],
        ["Botswana", "bw", "267"],
        ["Brazil (Brasil)", "br", "55"],
        ["British Indian Ocean Territory", "io", "246"],
        ["British Virgin Islands", "vg", "1284"],
        ["Brunei", "bn", "673"],
        ["Bulgaria (България)", "bg", "359"],
        ["Burkina Faso", "bf", "226"],
        ["Burundi (Uburundi)", "bi", "257"],
        ["Cambodia (កម្ពុជា)", "kh", "855"],
        ["Cameroon (Cameroun)", "cm", "237"],
        ["Canada", "ca", "1", 1, ["204", "236", "249", "250", "289", "306", "343", "365", "387", "403", "416", "418", "431", "437", "438", "450", "506", "514", "519", "548", "579", "581", "587", "604", "613", "639", "647", "672", "705", "709", "742", "778", "780", "782", "807", "819", "825", "867", "873", "902", "905"]],
        ["Cape Verde (Kabu Verdi)", "cv", "238"],
        ["Caribbean Netherlands", "bq", "599", 1],
        ["Cayman Islands", "ky", "1345"],
        ["Central African Republic (République centrafricaine)", "cf", "236"],
        ["Chad (Tchad)", "td", "235"],
        ["Chile", "cl", "56"],
        ["China (中国)", "cn", "86"],
        ["Colombia", "co", "57"],
        ["Comoros (‫جزر القمر‬‎)", "km", "269"],
        ["Congo (DRC) (Jamhuri ya Kidemokrasia ya Kongo)", "cd", "243"],
        ["Congo (Republic) (Congo-Brazzaville)", "cg", "242"],
        ["Cook Islands", "ck", "682"],
        ["Costa Rica", "cr", "506"],
        ["Côte d’Ivoire", "ci", "225"],
        ["Croatia (Hrvatska)", "hr", "385"],
        ["Cuba", "cu", "53"],
        ["Curaçao", "cw", "599", 0],
        ["Cyprus (Κύπρος)", "cy", "357"],
        ["Czech Republic (Česká republika)", "cz", "420"],
        ["Denmark (Danmark)", "dk", "45"],
        ["Djibouti", "dj", "253"],
        ["Dominica", "dm", "1767"],
        ["Dominican Republic (República Dominicana)", "do", "1", 2, ["809", "829", "849"]],
        ["Ecuador", "ec", "593"],
        ["Egypt (‫مصر‬‎)", "eg", "20"],
        ["El Salvador", "sv", "503"],
        ["Equatorial Guinea (Guinea Ecuatorial)", "gq", "240"],
        ["Eritrea", "er", "291"],
        ["Estonia (Eesti)", "ee", "372"],
        ["Ethiopia", "et", "251"],
        ["Falkland Islands (Islas Malvinas)", "fk", "500"],
        ["Faroe Islands (Føroyar)", "fo", "298"],
        ["Fiji", "fj", "679"],
        ["Finland (Suomi)", "fi", "358"],
        ["France", "fr", "33"],
        ["French Guiana (Guyane française)", "gf", "594"],
        ["French Polynesia (Polynésie française)", "pf", "689"],
        ["Gabon", "ga", "241"],
        ["Gambia", "gm", "220"],
        ["Georgia (საქართველო)", "ge", "995"],
        ["Germany (Deutschland)", "de", "49"],
        ["Ghana (Gaana)", "gh", "233"],
        ["Gibraltar", "gi", "350"],
        ["Greece (Ελλάδα)", "gr", "30"],
        ["Greenland (Kalaallit Nunaat)", "gl", "299"],
        ["Grenada", "gd", "1473"],
        ["Guadeloupe", "gp", "590", 0],
        ["Guam", "gu", "1671"],
        ["Guatemala", "gt", "502"],
        ["Guinea (Guinée)", "gn", "224"],
        ["Guinea-Bissau (Guiné Bissau)", "gw", "245"],
        ["Guyana", "gy", "592"],
        ["Haiti", "ht", "509"],
        ["Honduras", "hn", "504"],
        ["Hong Kong (香港)", "hk", "852"],
        ["Hungary (Magyarország)", "hu", "36"],
        ["Iceland (Ísland)", "is", "354"],
        ["India (भारत)", "in", "91"],
        ["Indonesia", "id", "62"],
        ["Iran (‫ایران‬‎)", "ir", "98"],
        ["Iraq (‫العراق‬‎)", "iq", "964"],
        ["Ireland", "ie", "353"],
        ["Israel (‫ישראל‬‎)", "il", "972"],
        ["Italy (Italia)", "it", "39", 0],
        ["Jamaica", "jm", "1876"],
        ["Japan (日本)", "jp", "81"],
        ["Jordan (‫الأردن‬‎)", "jo", "962"],
        ["Kazakhstan (Казахстан)", "kz", "7", 1],
        ["Kenya", "ke", "254"],
        ["Kiribati", "ki", "686"],
        ["Kuwait (‫الكويت‬‎)", "kw", "965"],
        ["Kyrgyzstan (Кыргызстан)", "kg", "996"],
        ["Laos (ລາວ)", "la", "856"],
        ["Latvia (Latvija)", "lv", "371"],
        ["Lebanon (‫لبنان‬‎)", "lb", "961"],
        ["Lesotho", "ls", "266"],
        ["Liberia", "lr", "231"],
        ["Libya (‫ليبيا‬‎)", "ly", "218"],
        ["Liechtenstein", "li", "423"],
        ["Lithuania (Lietuva)", "lt", "370"],
        ["Luxembourg", "lu", "352"],
        ["Macau (澳門)", "mo", "853"],
        ["Macedonia (FYROM) (Македонија)", "mk", "389"],
        ["Madagascar (Madagasikara)", "mg", "261"],
        ["Malawi", "mw", "265"],
        ["Malaysia", "my", "60"],
        ["Maldives", "mv", "960"],
        ["Mali", "ml", "223"],
        ["Malta", "mt", "356"],
        ["Marshall Islands", "mh", "692"],
        ["Martinique", "mq", "596"],
        ["Mauritania (‫موريتانيا‬‎)", "mr", "222"],
        ["Mauritius (Moris)", "mu", "230"],
        ["Mexico (México)", "mx", "52"],
        ["Micronesia", "fm", "691"],
        ["Moldova (Republica Moldova)", "md", "373"],
        ["Monaco", "mc", "377"],
        ["Mongolia (Монгол)", "mn", "976"],
        ["Montenegro (Crna Gora)", "me", "382"],
        ["Montserrat", "ms", "1664"],
        ["Morocco (‫المغرب‬‎)", "ma", "212"],
        ["Mozambique (Moçambique)", "mz", "258"],
        ["Myanmar (Burma) (မြန်မာ)", "mm", "95"],
        ["Namibia (Namibië)", "na", "264"],
        ["Nauru", "nr", "674"],
        ["Nepal (नेपाल)", "np", "977"],
        ["Netherlands (Nederland)", "nl", "31"],
        ["New Caledonia (Nouvelle-Calédonie)", "nc", "687"],
        ["New Zealand", "nz", "64"],
        ["Nicaragua", "ni", "505"],
        ["Niger (Nijar)", "ne", "227"],
        ["Nigeria", "ng", "234"],
        ["Niue", "nu", "683"],
        ["Norfolk Island", "nf", "672"],
        ["North Korea (조선 민주주의 인민 공화국)", "kp", "850"],
        ["Northern Mariana Islands", "mp", "1670"],
        ["Norway (Norge)", "no", "47"],
        ["Oman (‫عُمان‬‎)", "om", "968"],
        ["Pakistan (‫پاکستان‬‎)", "pk", "92"],
        ["Palau", "pw", "680"],
        ["Palestine (‫فلسطين‬‎)", "ps", "970"],
        ["Panama (Panamá)", "pa", "507"],
        ["Papua New Guinea", "pg", "675"],
        ["Paraguay", "py", "595"],
        ["Peru (Perú)", "pe", "51"],
        ["Philippines", "ph", "63"],
        ["Poland (Polska)", "pl", "48"],
        ["Portugal", "pt", "351"],
        ["Puerto Rico", "pr", "1", 3, ["787", "939"]],
        ["Qatar (‫قطر‬‎)", "qa", "974"],
        ["Réunion (La Réunion)", "re", "262"],
        ["Romania (România)", "ro", "40"],
        ["Russia (Россия)", "ru", "7", 0],
        ["Rwanda", "rw", "250"],
        ["Saint Barthélemy (Saint-Barthélemy)", "bl", "590", 1],
        ["Saint Helena", "sh", "290"],
        ["Saint Kitts and Nevis", "kn", "1869"],
        ["Saint Lucia", "lc", "1758"],
        ["Saint Martin (Saint-Martin (partie française))", "mf", "590", 2],
        ["Saint Pierre and Miquelon (Saint-Pierre-et-Miquelon)", "pm", "508"],
        ["Saint Vincent and the Grenadines", "vc", "1784"],
        ["Samoa", "ws", "685"],
        ["San Marino", "sm", "378"],
        ["São Tomé and Príncipe (São Tomé e Príncipe)", "st", "239"],
        ["Saudi Arabia (‫المملكة العربية السعودية‬‎)", "sa", "966"],
        ["Senegal (Sénégal)", "sn", "221"],
        ["Serbia (Србија)", "rs", "381"],
        ["Seychelles", "sc", "248"],
        ["Sierra Leone", "sl", "232"],
        ["Singapore", "sg", "65"],
        ["Sint Maarten", "sx", "1721"],
        ["Slovakia (Slovensko)", "sk", "421"],
        ["Slovenia (Slovenija)", "si", "386"],
        ["Solomon Islands", "sb", "677"],
        ["Somalia (Soomaaliya)", "so", "252"],
        ["South Africa", "za", "27"],
        ["South Korea (대한민국)", "kr", "82"],
        ["South Sudan (‫جنوب السودان‬‎)", "ss", "211"],
        ["Spain (España)", "es", "34"],
        ["Sri Lanka (ශ්‍රී ලංකාව)", "lk", "94"],
        ["Sudan (‫السودان‬‎)", "sd", "249"],
        ["Suriname", "sr", "597"],
        ["Swaziland", "sz", "268"],
        ["Sweden (Sverige)", "se", "46"],
        ["Switzerland (Schweiz)", "ch", "41"],
        ["Syria (‫سوريا‬‎)", "sy", "963"],
        ["Taiwan (台灣)", "tw", "886"],
        ["Tajikistan", "tj", "992"],
        ["Tanzania", "tz", "255"],
        ["Thailand (ไทย)", "th", "66"],
        ["Timor-Leste", "tl", "670"],
        ["Togo", "tg", "228"],
        ["Tokelau", "tk", "690"],
        ["Tonga", "to", "676"],
        ["Trinidad and Tobago", "tt", "1868"],
        ["Tunisia (‫تونس‬‎)", "tn", "216"],
        ["Turkey (Türkiye)", "tr", "90"],
        ["Turkmenistan", "tm", "993"],
        ["Turks and Caicos Islands", "tc", "1649"],
        ["Tuvalu", "tv", "688"],
        ["U.S. Virgin Islands", "vi", "1340"],
        ["Uganda", "ug", "256"],
        ["Ukraine (Україна)", "ua", "380"],
        ["United Arab Emirates (‫الإمارات العربية المتحدة‬‎)", "ae", "971"],
        ["United Kingdom", "gb", "44"],
        ["United States", "us", "1", 0],
        ["Uruguay", "uy", "598"],
        ["Uzbekistan (Oʻzbekiston)", "uz", "998"],
        ["Vanuatu", "vu", "678"],
        ["Vatican City (Città del Vaticano)", "va", "39", 1],
        ["Venezuela", "ve", "58"],
        ["Vietnam (Việt Nam)", "vn", "84"],
        ["Wallis and Futuna", "wf", "681"],
        ["Yemen (‫اليمن‬‎)", "ye", "967"],
        ["Zambia", "zm", "260"],
        ["Zimbabwe", "zw", "263"]
    ];
    for (var c = 0; c < l.length; c++) {
        var h = l[c];
        l[c] = {
            name: h[0],
            iso2: h[1],
            dialCode: h[2],
            priority: h[3] || 0,
            areaCodes: h[4] || null
        }
    }
})