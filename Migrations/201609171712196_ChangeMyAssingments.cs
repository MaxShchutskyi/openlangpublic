namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeMyAssingments : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MyAssingnments", "Topic", c => c.String(maxLength: 150));
            AlterColumn("dbo.MyAssingnments", "Description", c => c.String(maxLength: 1500));
            AlterColumn("dbo.MyAssingnments", "Image", c => c.String(maxLength: 150));
            AlterColumn("dbo.MyAssingnments", "Answer", c => c.String(maxLength: 1500));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MyAssingnments", "Answer", c => c.String(maxLength: 500));
            AlterColumn("dbo.MyAssingnments", "Image", c => c.String(maxLength: 100));
            AlterColumn("dbo.MyAssingnments", "Description", c => c.String(maxLength: 300));
            AlterColumn("dbo.MyAssingnments", "Topic", c => c.String(maxLength: 50));
        }
    }
}
