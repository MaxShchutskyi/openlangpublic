namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLevelId : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Levels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Chapters", "LevelId", c => c.Int());
            CreateIndex("dbo.Chapters", "LevelId");
            AddForeignKey("dbo.Chapters", "LevelId", "dbo.Levels", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Chapters", "LevelId", "dbo.Levels");
            DropIndex("dbo.Chapters", new[] { "LevelId" });
            DropColumn("dbo.Chapters", "LevelId");
            DropTable("dbo.Levels");
        }
    }
}
