namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStartDateTimeDateTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Schedules", "StartDateTimeDateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Schedules", "StartDateTimeDateTime");
        }
    }
}
