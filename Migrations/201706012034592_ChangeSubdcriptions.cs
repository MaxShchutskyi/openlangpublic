namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeSubdcriptions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StripeSubscriptions", "CountLessons", c => c.Short(nullable: false));
            AddColumn("dbo.StripeSubscriptions", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.StripeSubscriptions", "EurAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.StripeSubscriptions", "Currency", c => c.String(nullable: false));
            AddColumn("dbo.StripeSubscriptions", "TypeProvider", c => c.String(maxLength: 20));
            AlterColumn("dbo.StripeSubscriptions", "CustomerId", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.StripeSubscriptions", "CustomerId", c => c.String(nullable: false, maxLength: 100));
            DropColumn("dbo.StripeSubscriptions", "TypeProvider");
            DropColumn("dbo.StripeSubscriptions", "Currency");
            DropColumn("dbo.StripeSubscriptions", "EurAmount");
            DropColumn("dbo.StripeSubscriptions", "Amount");
            DropColumn("dbo.StripeSubscriptions", "CountLessons");
        }
    }
}
