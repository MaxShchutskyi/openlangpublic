namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClearGabage : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.GroupBrainCards", "Group_Id", "dbo.Groups");
            DropForeignKey("dbo.GroupBrainCards", "BrainCard_Id", "dbo.BrainCards");
            DropForeignKey("dbo.Groups", "TeacherId", "dbo.CustomUsers");
            DropForeignKey("dbo.Homework", "GroupId", "dbo.Groups");
            DropForeignKey("dbo.LevelsHomeworks", "Levels_Id", "dbo.Levels");
            DropForeignKey("dbo.LevelsHomeworks", "Homework_Id", "dbo.Homework");
            DropForeignKey("dbo.Homework", "StudentId", "dbo.CustomUsers");
            DropForeignKey("dbo.Homework", "TeacherId", "dbo.CustomUsers");
            DropForeignKey("dbo.AnswersOfStudents", "HomeworkId", "dbo.Homework");
            DropForeignKey("dbo.SubAnswers", "CardId", "dbo.SubBrainCards");
            DropForeignKey("dbo.SubBrainCards", "CardId", "dbo.BrainCards");
            DropIndex("dbo.AnswersOfStudents", new[] { "HomeworkId" });
            DropIndex("dbo.Homework", new[] { "GroupId" });
            DropIndex("dbo.Homework", new[] { "TeacherId" });
            DropIndex("dbo.Homework", new[] { "StudentId" });
            DropIndex("dbo.Groups", new[] { "TeacherId" });
            DropIndex("dbo.SubBrainCards", new[] { "CardId" });
            DropIndex("dbo.SubAnswers", new[] { "CardId" });
            DropIndex("dbo.GroupBrainCards", new[] { "Group_Id" });
            DropIndex("dbo.GroupBrainCards", new[] { "BrainCard_Id" });
            DropIndex("dbo.LevelsHomeworks", new[] { "Levels_Id" });
            DropIndex("dbo.LevelsHomeworks", new[] { "Homework_Id" });
            DropColumn("dbo.AnswersOfStudents", "HomeworkId");
            DropTable("dbo.Homework");
            DropTable("dbo.Groups");
            DropTable("dbo.Levels");
            DropTable("dbo.SubBrainCards");
            DropTable("dbo.SubAnswers");
            DropTable("dbo.GroupBrainCards");
            DropTable("dbo.LevelsHomeworks");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.LevelsHomeworks",
                c => new
                    {
                        Levels_Id = c.Int(nullable: false),
                        Homework_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Levels_Id, t.Homework_Id });
            
            CreateTable(
                "dbo.GroupBrainCards",
                c => new
                    {
                        Group_Id = c.Int(nullable: false),
                        BrainCard_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Group_Id, t.BrainCard_Id });
            
            CreateTable(
                "dbo.SubAnswers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Variant = c.String(),
                        IsRight = c.Boolean(nullable: false),
                        ImageSrc = c.String(),
                        CardId = c.Int(nullable: false),
                        Index = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SubBrainCards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Question = c.String(),
                        CardId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Levels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        TeacherId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Homework",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreationDate = c.DateTime(nullable: false),
                        PlanForComplete = c.DateTime(nullable: false),
                        GroupId = c.Int(nullable: false),
                        Statistic = c.String(),
                        TeacherId = c.Guid(nullable: false),
                        StudentId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AnswersOfStudents", "HomeworkId", c => c.Int(nullable: false));
            CreateIndex("dbo.LevelsHomeworks", "Homework_Id");
            CreateIndex("dbo.LevelsHomeworks", "Levels_Id");
            CreateIndex("dbo.GroupBrainCards", "BrainCard_Id");
            CreateIndex("dbo.GroupBrainCards", "Group_Id");
            CreateIndex("dbo.SubAnswers", "CardId");
            CreateIndex("dbo.SubBrainCards", "CardId");
            CreateIndex("dbo.Groups", "TeacherId");
            CreateIndex("dbo.Homework", "StudentId");
            CreateIndex("dbo.Homework", "TeacherId");
            CreateIndex("dbo.Homework", "GroupId");
            CreateIndex("dbo.AnswersOfStudents", "HomeworkId");
            AddForeignKey("dbo.SubBrainCards", "CardId", "dbo.BrainCards", "Id", cascadeDelete: true);
            AddForeignKey("dbo.SubAnswers", "CardId", "dbo.SubBrainCards", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AnswersOfStudents", "HomeworkId", "dbo.Homework", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Homework", "TeacherId", "dbo.CustomUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Homework", "StudentId", "dbo.CustomUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.LevelsHomeworks", "Homework_Id", "dbo.Homework", "Id", cascadeDelete: true);
            AddForeignKey("dbo.LevelsHomeworks", "Levels_Id", "dbo.Levels", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Homework", "GroupId", "dbo.Groups", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Groups", "TeacherId", "dbo.CustomUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.GroupBrainCards", "BrainCard_Id", "dbo.BrainCards", "Id", cascadeDelete: true);
            AddForeignKey("dbo.GroupBrainCards", "Group_Id", "dbo.Groups", "Id", cascadeDelete: true);
        }
    }
}
