namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddScheduleTransactions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ScheduleTransactions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ScheduleId = c.Int(nullable: false),
                        PaymentTransactionId = c.String(nullable: false, maxLength: 100),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PaymentTransactions", t => t.PaymentTransactionId, cascadeDelete: false)
                .ForeignKey("dbo.Schedules", t => t.ScheduleId, cascadeDelete: false)
                .Index(t => t.ScheduleId)
                .Index(t => t.PaymentTransactionId);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScheduleTransactions", "ScheduleId", "dbo.Schedules");
            DropForeignKey("dbo.ScheduleTransactions", "PaymentTransactionId", "dbo.PaymentTransactions");
            DropIndex("dbo.ScheduleTransactions", new[] { "PaymentTransactionId" });
            DropIndex("dbo.ScheduleTransactions", new[] { "ScheduleId" });
            DropTable("dbo.ScheduleTransactions");
        }
    }
}
