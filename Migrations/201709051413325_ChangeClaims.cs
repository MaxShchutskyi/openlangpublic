namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeClaims : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUserClaims", "CustomUser_Id", "dbo.CustomUsers");
            DropIndex("dbo.AspNetUserClaims", new[] { "CustomUser_Id" });
            DropColumn("dbo.AspNetUserClaims", "UserId");
            RenameColumn(table: "dbo.AspNetUserClaims", name: "CustomUser_Id", newName: "UserId");
            AlterColumn("dbo.AspNetUserClaims", "UserId", c => c.Guid(nullable: false));
            AlterColumn("dbo.TeacherClasses", "MoneyEarned", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            CreateIndex("dbo.AspNetUserClaims", "UserId");
            AddForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.CustomUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.CustomUsers");
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            AlterColumn("dbo.TeacherClasses", "MoneyEarned", c => c.Short(nullable: false));
            AlterColumn("dbo.AspNetUserClaims", "UserId", c => c.Guid());
            RenameColumn(table: "dbo.AspNetUserClaims", name: "UserId", newName: "CustomUser_Id");
            AddColumn("dbo.AspNetUserClaims", "UserId", c => c.Guid(nullable: false));
            CreateIndex("dbo.AspNetUserClaims", "CustomUser_Id");
            AddForeignKey("dbo.AspNetUserClaims", "CustomUser_Id", "dbo.CustomUsers", "Id");
        }
    }
}
