namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleongIntro : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AdditionalUserInformations", "LongIntrodution");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AdditionalUserInformations", "LongIntrodution", c => c.String(maxLength: 4000));
        }
    }
}
