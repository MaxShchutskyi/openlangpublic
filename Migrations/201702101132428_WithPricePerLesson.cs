namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WithPricePerLesson : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomUsers", "PricesForAllTypes", c => c.String(nullable: true, maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CustomUsers", "PricesForAllTypes");
        }
    }
}
