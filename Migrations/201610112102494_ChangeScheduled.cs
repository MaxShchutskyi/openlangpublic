namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeScheduled : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Schedules", "StartDateTimeDateTime");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Schedules", new[] { "StartDateTimeDateTime" });
        }
    }
}
