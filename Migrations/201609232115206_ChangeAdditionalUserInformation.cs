namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeAdditionalUserInformation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AdditionalUserInformations", "WhatLookingFor", c => c.String(maxLength: 4000));
            AlterColumn("dbo.AdditionalUserInformations", "ShotIntrodution", c => c.String(maxLength: 4000));
            AlterColumn("dbo.AdditionalUserInformations", "LongIntrodution", c => c.String(maxLength: 4000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AdditionalUserInformations", "LongIntrodution", c => c.String(maxLength: 1200));
            AlterColumn("dbo.AdditionalUserInformations", "ShotIntrodution", c => c.String(maxLength: 1200));
            AlterColumn("dbo.AdditionalUserInformations", "WhatLookingFor", c => c.String(maxLength: 500));
        }
    }
}
