namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TeachersEquipment : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Certifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 200),
                        PeriodFrom = c.Short(nullable: false),
                        PeriodTo = c.Short(nullable: false),
                        Description = c.String(nullable: false, maxLength: 4000),
                        CustomUser_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attendees", t => t.CustomUser_Id)
                .Index(t => t.CustomUser_Id);
            
            CreateTable(
                "dbo.Educations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 200),
                        PeriodFrom = c.Short(nullable: false),
                        PeriodTo = c.Short(nullable: false),
                        Description = c.String(nullable: false, maxLength: 4000),
                        CustomUser_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attendees", t => t.CustomUser_Id)
                .Index(t => t.CustomUser_Id);
            
            CreateTable(
                "dbo.PhotoGalleries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LinkBigImage = c.String(nullable: false, maxLength: 150),
                        LinkSmallImage = c.String(maxLength: 150),
                        FileName = c.String(nullable: false, maxLength: 50),
                        FileType = c.String(nullable: false, maxLength: 10),
                        FileSize = c.Int(),
                        Dimensision = c.String(maxLength: 15),
                        UploadDate = c.DateTime(nullable: false),
                        Title = c.String(maxLength: 150),
                        Description = c.String(maxLength: 4000),
                        CustomUser_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attendees", t => t.CustomUser_Id)
                .Index(t => t.CustomUser_Id);
            
            CreateTable(
                "dbo.Schedules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDateTime = c.String(),
                        EndDateTime = c.String(),
                        CustomUser_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attendees", t => t.CustomUser_Id)
                .Index(t => t.CustomUser_Id);
            
            CreateTable(
                "dbo.WorkExperiences",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 200),
                        PeriodFrom = c.Short(nullable: false),
                        PeriodTo = c.Short(nullable: false),
                        Description = c.String(nullable: false, maxLength: 4000),
                        CustomUser_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attendees", t => t.CustomUser_Id)
                .Index(t => t.CustomUser_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkExperiences", "CustomUser_Id", "dbo.Attendees");
            DropForeignKey("dbo.Schedules", "CustomUser_Id", "dbo.Attendees");
            DropForeignKey("dbo.PhotoGalleries", "CustomUser_Id", "dbo.Attendees");
            DropForeignKey("dbo.Educations", "CustomUser_Id", "dbo.Attendees");
            DropForeignKey("dbo.Certifications", "CustomUser_Id", "dbo.Attendees");
            DropIndex("dbo.WorkExperiences", new[] { "CustomUser_Id" });
            DropIndex("dbo.Schedules", new[] { "CustomUser_Id" });
            DropIndex("dbo.PhotoGalleries", new[] { "CustomUser_Id" });
            DropIndex("dbo.Educations", new[] { "CustomUser_Id" });
            DropIndex("dbo.Certifications", new[] { "CustomUser_Id" });
            DropTable("dbo.WorkExperiences");
            DropTable("dbo.Schedules");
            DropTable("dbo.PhotoGalleries");
            DropTable("dbo.Educations");
            DropTable("dbo.Certifications");
        }
    }
}
