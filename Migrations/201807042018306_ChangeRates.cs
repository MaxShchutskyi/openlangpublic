namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeRates : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.IndividualTeacherRates", "TrialPercent", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.IndividualTeacherRates", "OneLessonRatePercent", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.IndividualTeacherRates", "FiveLessonRatePercent", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.IndividualTeacherRates", "TenLessonRatePercent", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.IndividualTeacherRates", "FifteenLessonRatePercent", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.IndividualTeacherRates", "FifteenLessonRatePercent");
            DropColumn("dbo.IndividualTeacherRates", "TenLessonRatePercent");
            DropColumn("dbo.IndividualTeacherRates", "FiveLessonRatePercent");
            DropColumn("dbo.IndividualTeacherRates", "OneLessonRatePercent");
            DropColumn("dbo.IndividualTeacherRates", "TrialPercent");
        }
    }
}
