namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeOrderDetailsAndOther : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderDetails", "CoursesSpent", c => c.Byte(nullable: true));
            AddColumn("dbo.OrderDetails", "CourseBoughtFromId", c => c.Guid(nullable: true));
            CreateIndex("dbo.OrderDetails", "CourseBoughtFromId");
            AddForeignKey("dbo.OrderDetails", "CourseBoughtFromId", "dbo.CustomUsers", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderDetails", "CourseBoughtFromId", "dbo.CustomUsers");
            DropIndex("dbo.OrderDetails", new[] { "CourseBoughtFromId" });
            DropColumn("dbo.OrderDetails", "CourseBoughtFromId");
            DropColumn("dbo.OrderDetails", "CoursesSpent");
        }
    }
}
