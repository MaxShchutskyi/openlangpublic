namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUnread : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DialogReplies", "Unread", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DialogReplies", "Unread");
        }
    }
}
