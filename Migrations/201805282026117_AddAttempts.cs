namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAttempts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserLessonAttempts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StudentId = c.Guid(nullable: false),
                        AttemptDate = c.DateTime(nullable: false),
                        LessonId = c.Int(nullable: false),
                        IsSuccess = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Lessons", t => t.LessonId, cascadeDelete: true)
                .ForeignKey("dbo.CustomUsers", t => t.StudentId, cascadeDelete: true)
                .Index(t => t.StudentId)
                .Index(t => t.LessonId);
            
            AddColumn("dbo.AnswersOfStudents", "Answer", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserLessonAttempts", "StudentId", "dbo.CustomUsers");
            DropForeignKey("dbo.UserLessonAttempts", "LessonId", "dbo.Lessons");
            DropIndex("dbo.UserLessonAttempts", new[] { "LessonId" });
            DropIndex("dbo.UserLessonAttempts", new[] { "StudentId" });
            DropColumn("dbo.AnswersOfStudents", "Answer");
            DropTable("dbo.UserLessonAttempts");
        }
    }
}
