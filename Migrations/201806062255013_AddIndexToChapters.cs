namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIndexToChapters : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Chapters", "Index", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Chapters", "Index");
        }
    }
}
