namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDiscount : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Discounts",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Number = c.String(maxLength: 32),
                        IsPercent = c.Boolean(nullable: false),
                        PeriodAccessStart = c.DateTime(nullable: false),
                        PeriodAccessEnd = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DiscountTransactions",
                c => new
                    {
                        Discount_Id = c.Short(nullable: false),
                        PaymentTransactions_PaymentId = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => new { t.Discount_Id, t.PaymentTransactions_PaymentId })
                .ForeignKey("dbo.Discounts", t => t.Discount_Id, cascadeDelete: true)
                .ForeignKey("dbo.PaymentTransactions", t => t.PaymentTransactions_PaymentId, cascadeDelete: true)
                .Index(t => t.Discount_Id)
                .Index(t => t.PaymentTransactions_PaymentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DiscountTransactions", "PaymentTransactions_PaymentId", "dbo.PaymentTransactions");
            DropForeignKey("dbo.DiscountTransactions", "Discount_Id", "dbo.Discounts");
            DropIndex("dbo.DiscountTransactions", new[] { "PaymentTransactions_PaymentId" });
            DropIndex("dbo.DiscountTransactions", new[] { "Discount_Id" });
            DropTable("dbo.DiscountTransactions");
            DropTable("dbo.Discounts");
        }
    }
}
