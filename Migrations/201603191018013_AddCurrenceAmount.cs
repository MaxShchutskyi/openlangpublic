namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCurrenceAmount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentTransactions", "CurrenceAmount", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaymentTransactions", "CurrenceAmount");
        }
    }
}
