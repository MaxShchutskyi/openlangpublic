namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddForeignKeyForScheduleStudentses : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ScheduleStudents", "CustomUser_Id", "dbo.CustomUsers");
            DropIndex("dbo.ScheduleStudents", new[] { "CustomUser_Id" });
            RenameColumn(table: "dbo.ScheduleStudents", name: "CustomUser_Id", newName: "CustomUserId");
            AlterColumn("dbo.ScheduleStudents", "CustomUserId", c => c.Guid(nullable: false));
            CreateIndex("dbo.ScheduleStudents", "CustomUserId");
            AddForeignKey("dbo.ScheduleStudents", "CustomUserId", "dbo.CustomUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScheduleStudents", "CustomUserId", "dbo.CustomUsers");
            DropIndex("dbo.ScheduleStudents", new[] { "CustomUserId" });
            AlterColumn("dbo.ScheduleStudents", "CustomUserId", c => c.Guid());
            RenameColumn(table: "dbo.ScheduleStudents", name: "CustomUserId", newName: "CustomUser_Id");
            CreateIndex("dbo.ScheduleStudents", "CustomUser_Id");
            AddForeignKey("dbo.ScheduleStudents", "CustomUser_Id", "dbo.CustomUsers", "Id");
        }
    }
}
