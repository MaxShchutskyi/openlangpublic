namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserPoints : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomUsers", "Points", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CustomUsers", "Points");
        }
    }
}
