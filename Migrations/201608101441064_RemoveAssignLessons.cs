namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveAssignLessons : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AssignLessonsOfStudents", "PaymentId", "dbo.PaymentTransactions");
            DropForeignKey("dbo.AssignLessonsOfStudents", "ScheduleId", "dbo.Schedules");
            DropIndex("dbo.AssignLessonsOfStudents", new[] { "ScheduleId" });
            DropIndex("dbo.AssignLessonsOfStudents", new[] { "PaymentId" });
            AddColumn("dbo.Schedules", "PaymentTransactionsId", c => c.String(maxLength: 100));
            CreateIndex("dbo.Schedules", "PaymentTransactionsId");
            AddForeignKey("dbo.Schedules", "PaymentTransactionsId", "dbo.PaymentTransactions", "PaymentId");
            DropColumn("dbo.Schedules", "ScheduleStudentsId");
            DropTable("dbo.AssignLessonsOfStudents");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.AssignLessonsOfStudents",
                c => new
                    {
                        ScheduleId = c.Int(nullable: false),
                        PaymentId = c.String(maxLength: 100),
                        Description = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.ScheduleId);
            
            AddColumn("dbo.Schedules", "ScheduleStudentsId", c => c.String());
            DropForeignKey("dbo.Schedules", "PaymentTransactionsId", "dbo.PaymentTransactions");
            DropIndex("dbo.Schedules", new[] { "PaymentTransactionsId" });
            DropColumn("dbo.Schedules", "PaymentTransactionsId");
            CreateIndex("dbo.AssignLessonsOfStudents", "PaymentId");
            CreateIndex("dbo.AssignLessonsOfStudents", "ScheduleId");
            AddForeignKey("dbo.AssignLessonsOfStudents", "ScheduleId", "dbo.Schedules", "Id");
            AddForeignKey("dbo.AssignLessonsOfStudents", "PaymentId", "dbo.PaymentTransactions", "PaymentId");
        }
    }
}
