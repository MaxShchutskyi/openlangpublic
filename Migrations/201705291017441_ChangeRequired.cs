namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeRequired : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Credits", new[] { "Payment_PaymentId" });
            AlterColumn("dbo.Credits", "TypeOfPrice", c => c.String(maxLength: 50));
            AlterColumn("dbo.Credits", "Payment_PaymentId", c => c.String(maxLength: 100));
            CreateIndex("dbo.Credits", "Payment_PaymentId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Credits", new[] { "Payment_PaymentId" });
            AlterColumn("dbo.Credits", "Payment_PaymentId", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Credits", "TypeOfPrice", c => c.String());
            CreateIndex("dbo.Credits", "Payment_PaymentId");
        }
    }
}
