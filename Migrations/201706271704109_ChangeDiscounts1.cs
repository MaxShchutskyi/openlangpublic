namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDiscounts1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Discounts", "Type", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Discounts", "Type");
        }
    }
}
