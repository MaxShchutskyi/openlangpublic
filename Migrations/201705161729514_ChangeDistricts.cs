namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDistricts : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Corresponds", "Name", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Corresponds", "Subject", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Corresponds", "Text", c => c.String(nullable: false, maxLength: 4000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Corresponds", "Text", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.Corresponds", "Subject", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.Corresponds", "Name", c => c.String(nullable: false, maxLength: 50));
        }
    }
}
