namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NeZnayu : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderDetails", "CourseBoughtFromId", "dbo.CustomUsers");
            DropIndex("dbo.OrderDetails", new[] { "CourseBoughtFromId" });
            AlterColumn("dbo.OrderDetails", "CourseBoughtFromId", c => c.Guid());
            CreateIndex("dbo.OrderDetails", "CourseBoughtFromId");
            AddForeignKey("dbo.OrderDetails", "CourseBoughtFromId", "dbo.CustomUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderDetails", "CourseBoughtFromId", "dbo.CustomUsers");
            DropIndex("dbo.OrderDetails", new[] { "CourseBoughtFromId" });
            AlterColumn("dbo.OrderDetails", "CourseBoughtFromId", c => c.Guid(nullable: false));
            CreateIndex("dbo.OrderDetails", "CourseBoughtFromId");
            AddForeignKey("dbo.OrderDetails", "CourseBoughtFromId", "dbo.CustomUsers", "Id", cascadeDelete: true);
        }
    }
}
