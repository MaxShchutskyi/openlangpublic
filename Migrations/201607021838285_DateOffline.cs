namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DateOffline : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomUsers", "DateOffline", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CustomUsers", "DateOffline");
        }
    }
}
