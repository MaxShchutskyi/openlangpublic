namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TryToChangeIt : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Role", newName: "AspNetRoles");
            RenameTable(name: "dbo.UserRole", newName: "AspNetUserRoles");
            DropForeignKey("dbo.UserRole", "CustomRole_Id", "dbo.Role");
            DropIndex("dbo.AspNetUserRoles", new[] { "CustomRole_Id" });
            DropColumn("dbo.AspNetUserRoles", "RoleID");
            RenameColumn(table: "dbo.AspNetUserRoles", name: "CustomRole_Id", newName: "RoleId");
            //DropPrimaryKey("dbo.AspNetUserRoles");
            AlterColumn("dbo.AspNetRoles", "Name", c => c.String(nullable: false, maxLength: 256));
            AlterColumn("dbo.AspNetUserRoles", "RoleId", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.AspNetUserRoles", new[] { "UserId", "RoleId" });
            CreateIndex("dbo.AspNetRoles", "Name", unique: true, name: "RoleNameIndex");
            CreateIndex("dbo.AspNetUserRoles", "RoleId");
            AddForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            //DropPrimaryKey("dbo.AspNetUserRoles");
            AlterColumn("dbo.AspNetUserRoles", "RoleId", c => c.Guid());
            AlterColumn("dbo.AspNetRoles", "Name", c => c.String());
            AddPrimaryKey("dbo.AspNetUserRoles", new[] { "RoleID", "UserID" });
            RenameColumn(table: "dbo.AspNetUserRoles", name: "RoleId", newName: "CustomRole_Id");
            AddColumn("dbo.AspNetUserRoles", "RoleID", c => c.Guid(nullable: false));
            CreateIndex("dbo.AspNetUserRoles", "CustomRole_Id");
            AddForeignKey("dbo.UserRole", "CustomRole_Id", "dbo.Role", "Id");
            RenameTable(name: "dbo.AspNetUserRoles", newName: "UserRole");
            RenameTable(name: "dbo.AspNetRoles", newName: "Role");
        }
    }
}
