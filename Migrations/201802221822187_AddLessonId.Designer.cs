// <auto-generated />
namespace Xencore.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddLessonId : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddLessonId));
        
        string IMigrationMetadata.Id
        {
            get { return "201802221822187_AddLessonId"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
