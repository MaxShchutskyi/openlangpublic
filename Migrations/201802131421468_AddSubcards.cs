namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSubcards : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SubBrainCards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Question = c.String(),
                        CardId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BrainCards", t => t.CardId, cascadeDelete: false)
                .Index(t => t.CardId);
            
            CreateTable(
                "dbo.SubAnswers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Variant = c.String(),
                        IsRight = c.Boolean(nullable: false),
                        ImageSrc = c.String(),
                        CardId = c.Int(nullable: false),
                        Index = c.Int(),
                        AdditionalJSON = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SubBrainCards", t => t.CardId, cascadeDelete: true)
                .Index(t => t.CardId);
            
            AddColumn("dbo.Answers", "AdditionalJSON", c => c.String(storeType: "ntext"));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SubBrainCards", "CardId", "dbo.BrainCards");
            DropForeignKey("dbo.SubAnswers", "CardId", "dbo.SubBrainCards");
            DropIndex("dbo.SubAnswers", new[] { "CardId" });
            DropIndex("dbo.SubBrainCards", new[] { "CardId" });
            DropColumn("dbo.Answers", "AdditionalJSON");
            DropTable("dbo.SubAnswers");
            DropTable("dbo.SubBrainCards");
        }
    }
}
