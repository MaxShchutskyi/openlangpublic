namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeCustomUser : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Attendees", newName: "CustomUsers");
            DropForeignKey("dbo.AspNetUserClaims", "CustomUser_Id", "dbo.Attendees");
            DropForeignKey("dbo.AspNetUserLogins", "CustomUser_Id", "dbo.Attendees");
            DropForeignKey("dbo.PaymentTransactions", "AttendeeId", "dbo.Attendees");
            DropForeignKey("dbo.ReservedClasses", "Attendees_Id", "dbo.Attendees");
            DropForeignKey("dbo.AspNetUserRoles", "CustomUser_Id", "dbo.Attendees");
            DropForeignKey("dbo.Subscriptions", "Attendees_Id", "dbo.Attendees");
            DropForeignKey("dbo.TeacherInfoes", "Id", "dbo.Attendees");
            DropForeignKey("dbo.Comments", "UserId", "dbo.Attendees");
            DropForeignKey("dbo.AnyQuestions", "UserId", "dbo.Attendees");
            DropForeignKey("dbo.Certifications", "CustomUser_Id", "dbo.Attendees");
            DropForeignKey("dbo.Educations", "CustomUser_Id", "dbo.Attendees");
            DropForeignKey("dbo.PhotoGalleries", "CustomUser_Id", "dbo.Attendees");
            DropForeignKey("dbo.Schedules", "CustomUser_Id", "dbo.Attendees");
            DropForeignKey("dbo.WorkExperiences", "CustomUser_Id", "dbo.Attendees");
            RenameColumn(table: "dbo.CustomUsers", name: "AttendeeID", newName: "Id");
            DropPrimaryKey("dbo.CustomUsers");
            AlterColumn("dbo.CustomUsers", "Id", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.CustomUsers", "Id");
            AddForeignKey("dbo.AspNetUserClaims", "CustomUser_Id", "dbo.CustomUsers", "Id");
            AddForeignKey("dbo.AspNetUserLogins", "CustomUser_Id", "dbo.CustomUsers", "Id");
            AddForeignKey("dbo.PaymentTransactions", "AttendeeId", "dbo.CustomUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ReservedClasses", "Attendees_Id", "dbo.CustomUsers", "Id");
            AddForeignKey("dbo.AspNetUserRoles", "CustomUser_Id", "dbo.CustomUsers", "Id");
            AddForeignKey("dbo.Subscriptions", "Attendees_Id", "dbo.CustomUsers", "Id");
            AddForeignKey("dbo.TeacherInfoes", "Id", "dbo.CustomUsers", "Id");
            AddForeignKey("dbo.Comments", "UserId", "dbo.CustomUsers", "Id");
            AddForeignKey("dbo.AnyQuestions", "UserId", "dbo.CustomUsers", "Id");
            AddForeignKey("dbo.Certifications", "CustomUser_Id", "dbo.CustomUsers", "Id");
            AddForeignKey("dbo.Educations", "CustomUser_Id", "dbo.CustomUsers", "Id");
            AddForeignKey("dbo.PhotoGalleries", "CustomUser_Id", "dbo.CustomUsers", "Id");
            AddForeignKey("dbo.Schedules", "CustomUser_Id", "dbo.CustomUsers", "Id");
            AddForeignKey("dbo.WorkExperiences", "CustomUser_Id", "dbo.CustomUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkExperiences", "CustomUser_Id", "dbo.CustomUsers");
            DropForeignKey("dbo.Schedules", "CustomUser_Id", "dbo.CustomUsers");
            DropForeignKey("dbo.PhotoGalleries", "CustomUser_Id", "dbo.CustomUsers");
            DropForeignKey("dbo.Educations", "CustomUser_Id", "dbo.CustomUsers");
            DropForeignKey("dbo.Certifications", "CustomUser_Id", "dbo.CustomUsers");
            DropForeignKey("dbo.AnyQuestions", "UserId", "dbo.CustomUsers");
            DropForeignKey("dbo.Comments", "UserId", "dbo.CustomUsers");
            DropForeignKey("dbo.TeacherInfoes", "Id", "dbo.CustomUsers");
            DropForeignKey("dbo.Subscriptions", "Attendees_Id", "dbo.CustomUsers");
            DropForeignKey("dbo.AspNetUserRoles", "CustomUser_Id", "dbo.CustomUsers");
            DropForeignKey("dbo.ReservedClasses", "Attendees_Id", "dbo.CustomUsers");
            DropForeignKey("dbo.PaymentTransactions", "AttendeeId", "dbo.CustomUsers");
            DropForeignKey("dbo.AspNetUserLogins", "CustomUser_Id", "dbo.CustomUsers");
            DropForeignKey("dbo.AspNetUserClaims", "CustomUser_Id", "dbo.CustomUsers");
            DropPrimaryKey("dbo.CustomUsers");
            AlterColumn("dbo.CustomUsers", "Id", c => c.Guid(nullable: false, identity: true));
            AddPrimaryKey("dbo.CustomUsers", "AttendeeID");
            RenameColumn(table: "dbo.CustomUsers", name: "Id", newName: "AttendeeID");
            AddForeignKey("dbo.WorkExperiences", "CustomUser_Id", "dbo.Attendees", "AttendeeID");
            AddForeignKey("dbo.Schedules", "CustomUser_Id", "dbo.Attendees", "AttendeeID");
            AddForeignKey("dbo.PhotoGalleries", "CustomUser_Id", "dbo.Attendees", "AttendeeID");
            AddForeignKey("dbo.Educations", "CustomUser_Id", "dbo.Attendees", "AttendeeID");
            AddForeignKey("dbo.Certifications", "CustomUser_Id", "dbo.Attendees", "AttendeeID");
            AddForeignKey("dbo.AnyQuestions", "UserId", "dbo.Attendees", "AttendeeID");
            AddForeignKey("dbo.Comments", "UserId", "dbo.Attendees", "AttendeeID");
            AddForeignKey("dbo.TeacherInfoes", "Id", "dbo.Attendees", "AttendeeID");
            AddForeignKey("dbo.Subscriptions", "Attendees_Id", "dbo.Attendees", "AttendeeID");
            AddForeignKey("dbo.AspNetUserRoles", "CustomUser_Id", "dbo.Attendees", "AttendeeID");
            AddForeignKey("dbo.ReservedClasses", "Attendees_Id", "dbo.Attendees", "AttendeeID");
            AddForeignKey("dbo.PaymentTransactions", "AttendeeId", "dbo.Attendees", "AttendeeID", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUserLogins", "CustomUser_Id", "dbo.Attendees", "AttendeeID");
            AddForeignKey("dbo.AspNetUserClaims", "CustomUser_Id", "dbo.Attendees", "AttendeeID");
            RenameTable(name: "dbo.CustomUsers", newName: "Attendees");
        }
    }
}
