namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLangToMyWords : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MyWords", "Language", c => c.String(maxLength: 30));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MyWords", "Language");
        }
    }
}
