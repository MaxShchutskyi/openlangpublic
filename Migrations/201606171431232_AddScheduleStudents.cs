namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddScheduleStudents : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ScheduleStudents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 200),
                        CustomUser_Id = c.Guid(),
                        Schedule_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomUsers", t => t.CustomUser_Id)
                .ForeignKey("dbo.Schedules", t => t.Schedule_Id)
                .Index(t => t.CustomUser_Id)
                .Index(t => t.Schedule_Id);
            
            AddColumn("dbo.Schedules", "WeekNumber", c => c.Short(nullable: false));
            AddColumn("dbo.Schedules", "Status", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Schedules", "StartDateTime", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Schedules", "EndDateTime", c => c.String(nullable: false, maxLength: 20));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScheduleStudents", "Schedule_Id", "dbo.Schedules");
            DropForeignKey("dbo.ScheduleStudents", "CustomUser_Id", "dbo.CustomUsers");
            DropIndex("dbo.ScheduleStudents", new[] { "Schedule_Id" });
            DropIndex("dbo.ScheduleStudents", new[] { "CustomUser_Id" });
            AlterColumn("dbo.Schedules", "EndDateTime", c => c.String());
            AlterColumn("dbo.Schedules", "StartDateTime", c => c.String());
            DropColumn("dbo.Schedules", "Status");
            DropColumn("dbo.Schedules", "WeekNumber");
            DropTable("dbo.ScheduleStudents");
        }
    }
}
