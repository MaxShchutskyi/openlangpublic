namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDiscounts : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DiscountTransactions", "Discount_Id", "dbo.Discounts");
            DropForeignKey("dbo.DiscountTransactions", "PaymentTransactions_PaymentId", "dbo.PaymentTransactions");
            DropIndex("dbo.DiscountTransactions", new[] { "Discount_Id" });
            DropIndex("dbo.DiscountTransactions", new[] { "PaymentTransactions_PaymentId" });
            AddColumn("dbo.PaymentTransactions", "DiscountId", c => c.Short());
            AddColumn("dbo.Discounts", "PaymentTransactions_PaymentId", c => c.String(maxLength: 100));
            CreateIndex("dbo.PaymentTransactions", "DiscountId");
            CreateIndex("dbo.Discounts", "PaymentTransactions_PaymentId");
            AddForeignKey("dbo.PaymentTransactions", "DiscountId", "dbo.Discounts", "Id");
            AddForeignKey("dbo.Discounts", "PaymentTransactions_PaymentId", "dbo.PaymentTransactions", "PaymentId");
            DropTable("dbo.DiscountTransactions");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.DiscountTransactions",
                c => new
                    {
                        Discount_Id = c.Short(nullable: false),
                        PaymentTransactions_PaymentId = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => new { t.Discount_Id, t.PaymentTransactions_PaymentId });
            
            DropForeignKey("dbo.Discounts", "PaymentTransactions_PaymentId", "dbo.PaymentTransactions");
            DropForeignKey("dbo.PaymentTransactions", "DiscountId", "dbo.Discounts");
            DropIndex("dbo.Discounts", new[] { "PaymentTransactions_PaymentId" });
            DropIndex("dbo.PaymentTransactions", new[] { "DiscountId" });
            DropColumn("dbo.Discounts", "PaymentTransactions_PaymentId");
            DropColumn("dbo.PaymentTransactions", "DiscountId");
            CreateIndex("dbo.DiscountTransactions", "PaymentTransactions_PaymentId");
            CreateIndex("dbo.DiscountTransactions", "Discount_Id");
            AddForeignKey("dbo.DiscountTransactions", "PaymentTransactions_PaymentId", "dbo.PaymentTransactions", "PaymentId", cascadeDelete: true);
            AddForeignKey("dbo.DiscountTransactions", "Discount_Id", "dbo.Discounts", "Id", cascadeDelete: true);
        }
    }
}
