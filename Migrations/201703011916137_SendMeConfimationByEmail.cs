namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SendMeConfimationByEmail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomUsers", "IsSendMeConfirmationByEmail", c => c.Boolean(nullable: false, defaultValue:true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CustomUsers", "IsSendMeConfirmationByEmail");
        }
    }
}
