namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.NotificationSubscribes");
            AlterColumn("dbo.NotificationSubscribes", "Endpoint", c => c.String(nullable: false, maxLength: 500));
            AddPrimaryKey("dbo.NotificationSubscribes", "Endpoint");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.NotificationSubscribes");
            AlterColumn("dbo.NotificationSubscribes", "Endpoint", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.NotificationSubscribes", "Endpoint");
        }
    }
}
