namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCompany : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Transfers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyId = c.Guid(nullable: false),
                        StudentId = c.Guid(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomUsers", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.CustomUsers", t => t.StudentId, cascadeDelete: false)
                .Index(t => t.CompanyId)
                .Index(t => t.StudentId);
            
            AddColumn("dbo.CustomUsers", "IpClient", c => c.String(maxLength: 16));
            AddColumn("dbo.CustomUsers", "CompanyId", c => c.Guid());
            CreateIndex("dbo.CustomUsers", "CompanyId");
            AddForeignKey("dbo.CustomUsers", "CompanyId", "dbo.CustomUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transfers", "StudentId", "dbo.CustomUsers");
            DropForeignKey("dbo.Transfers", "CompanyId", "dbo.CustomUsers");
            DropForeignKey("dbo.CustomUsers", "CompanyId", "dbo.CustomUsers");
            DropIndex("dbo.Transfers", new[] { "StudentId" });
            DropIndex("dbo.Transfers", new[] { "CompanyId" });
            DropIndex("dbo.CustomUsers", new[] { "CompanyId" });
            DropColumn("dbo.CustomUsers", "CompanyId");
            DropColumn("dbo.CustomUsers", "IpClient");
            DropTable("dbo.Transfers");
        }
    }
}
