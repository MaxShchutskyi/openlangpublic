namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeMyWords : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.MyWords", "Word");
            CreateIndex("dbo.MyWords", "Description");
        }
        
        public override void Down()
        {
            DropIndex("dbo.MyWords", new[] { "Description" });
            DropIndex("dbo.MyWords", new[] { "Word" });
        }
    }
}
