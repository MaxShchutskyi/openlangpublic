namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Alpha2_code = c.String(),
                        Alpha3_code = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PaymentTransactions",
                c => new
                    {
                        PaymentId = c.String(nullable: false, maxLength: 100),
                        InnerTransactionId = c.Guid(nullable: false),
                        AttendeeId = c.Guid(nullable: false),
                        Approved = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PayerId = c.String(),
                    })
                .PrimaryKey(t => t.PaymentId)
                .ForeignKey("dbo.Attendees", t => t.AttendeeId, cascadeDelete: true)
                .Index(t => t.AttendeeId);
            
            CreateTable(
                "dbo.Attendees",
                c => new
                    {
                        AttendeeID = c.Guid(nullable: false, identity: true),
                        ScreenName = c.String(),
                        LanguageCultureName = c.String(),
                        AttendeeUrl = c.String(),
                        Active = c.Boolean(nullable: false),
                        LanguageId = c.Int(),
                        LevelId = c.Int(),
                        Currency = c.String(),
                        Password = c.String(),
                        CreationDateTime = c.DateTime(nullable: false),
                        Ilearn = c.String(),
                        Ispeak = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Skype = c.String(maxLength: 100),
                        QqId = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        PlaceOfBirth = c.String(),
                        CountryOfBirth = c.String(),
                        Nationality = c.String(),
                        Address = c.String(),
                        Location = c.String(),
                        ProfilePicture = c.String(maxLength: 200),
                        ZoneId = c.Int(),
                        Name = c.String(),
                        TimeZone = c.String(maxLength: 50),
                        ChosenLang = c.String(maxLength: 20),
                        ChosenLevel = c.String(),
                        HowDidFind = c.String(maxLength: 20),
                        Role_Id = c.Guid(),
                        Email = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(),
                        CustomRole_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.AttendeeID)
                .ForeignKey("dbo.Languages", t => t.LanguageId)
                .ForeignKey("dbo.Levels", t => t.LevelId)
                .ForeignKey("dbo.Role", t => t.CustomRole_Id)
                .ForeignKey("dbo.Timezones", t => t.ZoneId)
                .Index(t => t.LanguageId)
                .Index(t => t.LevelId)
                .Index(t => t.ZoneId)
                .Index(t => t.CustomRole_Id);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        CustomUser_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attendees", t => t.CustomUser_Id)
                .Index(t => t.CustomUser_Id);
            
            CreateTable(
                "dbo.Languages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Abbreviation = c.String(),
                        Name = c.String(),
                        Culture = c.String(),
                        ImageSrc = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Levels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Guid(nullable: false),
                        CustomUser_Id = c.Guid(),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.Attendees", t => t.CustomUser_Id)
                .Index(t => t.CustomUser_Id);
            
            CreateTable(
                "dbo.ReservedClasses",
                c => new
                    {
                        ReservedClassID = c.Long(nullable: false, identity: true),
                        AttendeeUrl = c.String(),
                        ClassId = c.Int(),
                        AttendeeId = c.Guid(),
                        Attendees_Id = c.Guid(),
                        Classes_ClassID = c.Long(),
                    })
                .PrimaryKey(t => t.ReservedClassID)
                .ForeignKey("dbo.Attendees", t => t.Attendees_Id)
                .ForeignKey("dbo.Classes", t => t.Classes_ClassID)
                .Index(t => t.Attendees_Id)
                .Index(t => t.Classes_ClassID);
            
            CreateTable(
                "dbo.Classes",
                c => new
                    {
                        ClassID = c.Long(nullable: false, identity: true),
                        ClassApiId = c.Int(nullable: false),
                        ClassMasterID = c.Int(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        Title = c.String(nullable: false),
                        TimeZone = c.String(),
                        AttendeeLimit = c.Int(nullable: false),
                        Duration = c.Int(nullable: false),
                        PresenterDefaultControls = c.String(),
                        AttendeeDefaultControls = c.String(),
                        CreateRecording = c.Boolean(nullable: false),
                        ReturnUrl = c.String(),
                        StatusPingUrl = c.String(),
                        PresenterModelId = c.Int(),
                        RecordingUrl = c.String(),
                        PresenterUrl = c.String(),
                        CoPresenterUrl = c.String(),
                        IsPermanent = c.Boolean(nullable: false),
                        IsRecurring = c.Boolean(nullable: false),
                        Type = c.String(),
                        LevelId = c.Int(),
                        SpecificTopic = c.String(),
                        LanguageCultureName = c.String(),
                        LanguageId = c.Int(),
                        HasSeats = c.Boolean(nullable: false),
                        CanJoin = c.Boolean(nullable: false),
                        Description = c.String(),
                        Image = c.String(),
                        Document = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        Presenters_Id = c.Int(),
                    })
                .PrimaryKey(t => t.ClassID)
                .ForeignKey("dbo.Languages", t => t.LanguageId)
                .ForeignKey("dbo.Levels", t => t.LevelId)
                .ForeignKey("dbo.Presenters", t => t.Presenters_Id)
                .Index(t => t.LevelId)
                .Index(t => t.LanguageId)
                .Index(t => t.Presenters_Id);
            
            CreateTable(
                "dbo.Presenters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PresenterEmail = c.String(),
                        PresenterId = c.Int(nullable: false),
                        Name = c.String(),
                        Password = c.String(),
                        PhoneNumber = c.String(),
                        MobileNumber = c.String(),
                        TimeZone = c.String(),
                        AboutTheTeacher = c.String(),
                        CanScheduleClass = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        PostFilePath = c.String(),
                        CreationDateTime = c.DateTime(nullable: false),
                        RoleId = c.Int(),
                        Role_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Role", t => t.Role_Id)
                .Index(t => t.Role_Id);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                        CustomRole_Id = c.Guid(),
                        CustomUser_Id = c.Guid(),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Role", t => t.CustomRole_Id)
                .ForeignKey("dbo.Attendees", t => t.CustomUser_Id)
                .Index(t => t.CustomRole_Id)
                .Index(t => t.CustomUser_Id);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ClassModels_ClassID = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Classes", t => t.ClassModels_ClassID)
                .Index(t => t.ClassModels_ClassID);
            
            CreateTable(
                "dbo.Subscriptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        ExperationDate = c.DateTime(nullable: false),
                        AttendeeId = c.Guid(nullable: false),
                        LevelId = c.Int(nullable: false),
                        LanguageId = c.Int(nullable: false),
                        Attendees_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attendees", t => t.Attendees_Id)
                .ForeignKey("dbo.Languages", t => t.LanguageId, cascadeDelete: true)
                .ForeignKey("dbo.Levels", t => t.LevelId, cascadeDelete: true)
                .Index(t => t.LevelId)
                .Index(t => t.LanguageId)
                .Index(t => t.Attendees_Id);
            
            CreateTable(
                "dbo.Timezones",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Zone = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        TransactionId = c.String(nullable: false, maxLength: 100),
                        CountLessons = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        SelectedLevel = c.String(),
                        SelectedLanguage = c.String(),
                    })
                .PrimaryKey(t => t.TransactionId)
                .ForeignKey("dbo.PaymentTransactions", t => t.TransactionId)
                .Index(t => t.TransactionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderDetails", "TransactionId", "dbo.PaymentTransactions");
            DropForeignKey("dbo.PaymentTransactions", "AttendeeId", "dbo.Attendees");
            DropForeignKey("dbo.Attendees", "ZoneId", "dbo.Timezones");
            DropForeignKey("dbo.Subscriptions", "LevelId", "dbo.Levels");
            DropForeignKey("dbo.Subscriptions", "LanguageId", "dbo.Languages");
            DropForeignKey("dbo.Subscriptions", "Attendees_Id", "dbo.Attendees");
            DropForeignKey("dbo.AspNetUserRoles", "CustomUser_Id", "dbo.Attendees");
            DropForeignKey("dbo.Subjects", "ClassModels_ClassID", "dbo.Classes");
            DropForeignKey("dbo.ReservedClasses", "Classes_ClassID", "dbo.Classes");
            DropForeignKey("dbo.Presenters", "Role_Id", "dbo.Role");
            DropForeignKey("dbo.AspNetUserRoles", "CustomRole_Id", "dbo.Role");
            DropForeignKey("dbo.Attendees", "CustomRole_Id", "dbo.Role");
            DropForeignKey("dbo.Classes", "Presenters_Id", "dbo.Presenters");
            DropForeignKey("dbo.Classes", "LevelId", "dbo.Levels");
            DropForeignKey("dbo.Classes", "LanguageId", "dbo.Languages");
            DropForeignKey("dbo.ReservedClasses", "Attendees_Id", "dbo.Attendees");
            DropForeignKey("dbo.AspNetUserLogins", "CustomUser_Id", "dbo.Attendees");
            DropForeignKey("dbo.Attendees", "LevelId", "dbo.Levels");
            DropForeignKey("dbo.Attendees", "LanguageId", "dbo.Languages");
            DropForeignKey("dbo.AspNetUserClaims", "CustomUser_Id", "dbo.Attendees");
            DropIndex("dbo.OrderDetails", new[] { "TransactionId" });
            DropIndex("dbo.Subscriptions", new[] { "Attendees_Id" });
            DropIndex("dbo.Subscriptions", new[] { "LanguageId" });
            DropIndex("dbo.Subscriptions", new[] { "LevelId" });
            DropIndex("dbo.Subjects", new[] { "ClassModels_ClassID" });
            DropIndex("dbo.AspNetUserRoles", new[] { "CustomUser_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "CustomRole_Id" });
            DropIndex("dbo.Presenters", new[] { "Role_Id" });
            DropIndex("dbo.Classes", new[] { "Presenters_Id" });
            DropIndex("dbo.Classes", new[] { "LanguageId" });
            DropIndex("dbo.Classes", new[] { "LevelId" });
            DropIndex("dbo.ReservedClasses", new[] { "Classes_ClassID" });
            DropIndex("dbo.ReservedClasses", new[] { "Attendees_Id" });
            DropIndex("dbo.AspNetUserLogins", new[] { "CustomUser_Id" });
            DropIndex("dbo.AspNetUserClaims", new[] { "CustomUser_Id" });
            DropIndex("dbo.Attendees", new[] { "CustomRole_Id" });
            DropIndex("dbo.Attendees", new[] { "ZoneId" });
            DropIndex("dbo.Attendees", new[] { "LevelId" });
            DropIndex("dbo.Attendees", new[] { "LanguageId" });
            DropIndex("dbo.PaymentTransactions", new[] { "AttendeeId" });
            DropTable("dbo.OrderDetails");
            DropTable("dbo.Timezones");
            DropTable("dbo.Subscriptions");
            DropTable("dbo.Subjects");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.Role");
            DropTable("dbo.Presenters");
            DropTable("dbo.Classes");
            DropTable("dbo.ReservedClasses");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.Levels");
            DropTable("dbo.Languages");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.Attendees");
            DropTable("dbo.PaymentTransactions");
            DropTable("dbo.Countries");
        }
    }
}
