namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCredits : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Credits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StudentId = c.Guid(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Duration = c.Short(nullable: false),
                        TimeOfLessons = c.Short(nullable: false),
                        Type = c.Int(nullable: false),
                        TypeOfPrice = c.String(),
                        PricePerLesson = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CountLessons = c.Short(nullable: false),
                        LeftLessons = c.Short(nullable: false),
                        Payment_PaymentId = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomUsers", t => t.StudentId, cascadeDelete: true)
                .ForeignKey("dbo.PaymentTransactions", t => t.Payment_PaymentId)
                .Index(t => t.StudentId)
                .Index(t => t.Payment_PaymentId);
            
            AddColumn("dbo.ScheduleTransactions", "CreditId", c => c.Int());
            CreateIndex("dbo.ScheduleTransactions", "CreditId");
            AddForeignKey("dbo.ScheduleTransactions", "CreditId", "dbo.Credits", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Credits", "Payment_PaymentId", "dbo.PaymentTransactions");
            DropForeignKey("dbo.Credits", "StudentId", "dbo.CustomUsers");
            DropForeignKey("dbo.ScheduleTransactions", "CreditId", "dbo.Credits");
            DropIndex("dbo.ScheduleTransactions", new[] { "CreditId" });
            DropIndex("dbo.Credits", new[] { "Payment_PaymentId" });
            DropIndex("dbo.Credits", new[] { "StudentId" });
            DropColumn("dbo.ScheduleTransactions", "CreditId");
            DropTable("dbo.Credits");
        }
    }
}
