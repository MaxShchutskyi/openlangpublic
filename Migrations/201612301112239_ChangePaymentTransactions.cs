namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePaymentTransactions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ScheduleTransactions", "Level", c => c.String(nullable: false, maxLength: 20));
            AddColumn("dbo.ScheduleTransactions", "Language", c => c.String(nullable: false, maxLength: 20));
            AddColumn("dbo.ScheduleTransactions", "Subject", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ScheduleTransactions", "Subject");
            DropColumn("dbo.ScheduleTransactions", "Language");
            DropColumn("dbo.ScheduleTransactions", "Level");
        }
    }
}
