namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddActions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Actions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        ActionDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        ShortMessage = c.String(maxLength: 4000),
                        Message = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            DropColumn("dbo.CustomUsers", "DateLoggedIn");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CustomUsers", "DateLoggedIn", c => c.DateTime());
            DropForeignKey("dbo.Actions", "UserId", "dbo.CustomUsers");
            DropIndex("dbo.Actions", new[] { "UserId" });
            DropTable("dbo.Actions");
        }
    }
}
