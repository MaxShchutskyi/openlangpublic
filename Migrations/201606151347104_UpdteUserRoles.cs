namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdteUserRoles : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.AspNetUserRoles", newName: "UserRole");
            DropPrimaryKey("dbo.UserRole");
            AddPrimaryKey("dbo.UserRole", new[] { "RoleID", "UserID" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.UserRole");
            AddPrimaryKey("dbo.UserRole", new[] { "UserId", "RoleId" });
            RenameTable(name: "dbo.UserRole", newName: "AspNetUserRoles");
        }
    }
}
