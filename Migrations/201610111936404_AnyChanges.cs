namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AnyChanges : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CustomUsers", "Timezones_Id", "dbo.Timezones");
            DropIndex("dbo.CustomUsers", new[] { "Timezones_Id" });
            DropColumn("dbo.CustomUsers", "Timezones_Id");
            DropTable("dbo.Timezones");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Timezones",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Zone = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.CustomUsers", "Timezones_Id", c => c.Int());
            CreateIndex("dbo.CustomUsers", "Timezones_Id");
            AddForeignKey("dbo.CustomUsers", "Timezones_Id", "dbo.Timezones", "Id");
        }
    }
}
