namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTimeStart : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Schedules", "TimeStart", c => c.Byte(nullable: false));
            AlterColumn("dbo.Schedules", "WeekNumber", c => c.Byte(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Schedules", "WeekNumber", c => c.Short(nullable: false));
            DropColumn("dbo.Schedules", "TimeStart");
        }
    }
}
