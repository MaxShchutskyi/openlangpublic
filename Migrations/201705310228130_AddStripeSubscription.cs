namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStripeSubscription : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Subscriptions", "Attendees_Id", "dbo.CustomUsers");
            DropIndex("dbo.Subscriptions", new[] { "Attendees_Id" });
            CreateTable(
                "dbo.StripeSubscriptions",
                c => new
                    {
                        SubscribeId = c.String(nullable: false, maxLength: 100),
                        CustomerId = c.String(nullable: false, maxLength: 100),
                        PlanId = c.String(maxLength: 100),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SubscribeId);
            
            AddColumn("dbo.PaymentTransactions", "StripeSubscriptionId", c => c.String(maxLength: 100));
            CreateIndex("dbo.PaymentTransactions", "StripeSubscriptionId");
            AddForeignKey("dbo.PaymentTransactions", "StripeSubscriptionId", "dbo.StripeSubscriptions", "SubscribeId");
            DropTable("dbo.Subscriptions");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Subscriptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        ExperationDate = c.DateTime(nullable: false),
                        AttendeeId = c.Guid(nullable: false),
                        Attendees_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.PaymentTransactions", "StripeSubscriptionId", "dbo.StripeSubscriptions");
            DropIndex("dbo.PaymentTransactions", new[] { "StripeSubscriptionId" });
            DropColumn("dbo.PaymentTransactions", "StripeSubscriptionId");
            DropTable("dbo.StripeSubscriptions");
            CreateIndex("dbo.Subscriptions", "Attendees_Id");
            AddForeignKey("dbo.Subscriptions", "Attendees_Id", "dbo.CustomUsers", "Id");
        }
    }
}
