namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Rollback : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CustomUsers", "Schedule_Id", "dbo.Schedules");
            DropIndex("dbo.CustomUsers", new[] { "Schedule_Id" });
            DropIndex("dbo.Schedules", new[] { "CustomUser_Id1" });
            DropColumn("dbo.Schedules", "CustomUser_Id");
            RenameColumn(table: "dbo.Schedules", name: "CustomUser_Id1", newName: "CustomUser_Id");
            CreateTable(
                "dbo.ScheduleStudents",
                c => new
                    {
                        ScheduleId = c.Int(nullable: false),
                        Description = c.String(maxLength: 200),
                        CustomUser_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.ScheduleId)
                .ForeignKey("dbo.CustomUsers", t => t.CustomUser_Id)
                .ForeignKey("dbo.Schedules", t => t.ScheduleId)
                .Index(t => t.ScheduleId)
                .Index(t => t.CustomUser_Id);
            
            DropColumn("dbo.CustomUsers", "Schedule_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CustomUsers", "Schedule_Id", c => c.Int());
            DropForeignKey("dbo.ScheduleStudents", "ScheduleId", "dbo.Schedules");
            DropForeignKey("dbo.ScheduleStudents", "CustomUser_Id", "dbo.CustomUsers");
            DropIndex("dbo.ScheduleStudents", new[] { "CustomUser_Id" });
            DropIndex("dbo.ScheduleStudents", new[] { "ScheduleId" });
            DropTable("dbo.ScheduleStudents");
            RenameColumn(table: "dbo.Schedules", name: "CustomUser_Id", newName: "CustomUser_Id1");
            AddColumn("dbo.Schedules", "CustomUser_Id", c => c.Guid());
            CreateIndex("dbo.Schedules", "CustomUser_Id1");
            CreateIndex("dbo.CustomUsers", "Schedule_Id");
            AddForeignKey("dbo.CustomUsers", "Schedule_Id", "dbo.Schedules", "Id");
        }
    }
}
