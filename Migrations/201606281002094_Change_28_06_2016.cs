namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change_28_06_2016 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DialogReplies", "DateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DialogReplies", "DateTime");
        }
    }
}
