namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRefferalProgtramm : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ParticipantsOfRefferalProgramms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RefferalCode = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        RegisterDateTime = c.DateTime(nullable: false),
                        ActiveDateTime = c.DateTime(),
                        Tupe = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomUsers", t => t.RefferalCode, cascadeDelete: false)
                .ForeignKey("dbo.CustomUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.RefferalCode)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ParticipantsOfRefferalProgramms", "UserId", "dbo.CustomUsers");
            DropForeignKey("dbo.ParticipantsOfRefferalProgramms", "RefferalCode", "dbo.CustomUsers");
            DropIndex("dbo.ParticipantsOfRefferalProgramms", new[] { "UserId" });
            DropIndex("dbo.ParticipantsOfRefferalProgramms", new[] { "RefferalCode" });
            DropTable("dbo.ParticipantsOfRefferalProgramms");
        }
    }
}
