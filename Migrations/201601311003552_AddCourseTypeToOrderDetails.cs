namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCourseTypeToOrderDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderDetails", "CourseType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderDetails", "CourseType");
        }
    }
}
