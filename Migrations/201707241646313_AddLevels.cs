namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLevels : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TeacherInfoes", "TeachLevels", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TeacherInfoes", "TeachLevels");
        }
    }
}
