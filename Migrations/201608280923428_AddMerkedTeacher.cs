namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMerkedTeacher : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BookMarkedTeachers",
                c => new
                    {
                        TeacherId = c.Guid(nullable: false),
                        StudentId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.TeacherId, t.StudentId })
                .ForeignKey("dbo.CustomUsers", t => t.StudentId, cascadeDelete: false)
                .ForeignKey("dbo.CustomUsers", t => t.TeacherId, cascadeDelete: false)
                .Index(t => t.TeacherId)
                .Index(t => t.StudentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BookMarkedTeachers", "TeacherId", "dbo.CustomUsers");
            DropForeignKey("dbo.BookMarkedTeachers", "StudentId", "dbo.CustomUsers");
            DropIndex("dbo.BookMarkedTeachers", new[] { "StudentId" });
            DropIndex("dbo.BookMarkedTeachers", new[] { "TeacherId" });
            DropTable("dbo.BookMarkedTeachers");
        }
    }
}
