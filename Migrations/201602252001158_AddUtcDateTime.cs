namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUtcDateTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderDetails", "UtcDateTime", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderDetails", "UtcDateTime");
        }
    }
}
