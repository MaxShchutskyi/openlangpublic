namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_23_06_2016 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PhotoGalleries", "CustomUser_Id", "dbo.CustomUsers");
            DropIndex("dbo.PhotoGalleries", new[] { "CustomUser_Id" });
            RenameColumn(table: "dbo.PhotoGalleries", name: "CustomUser_Id", newName: "CustomUserId");
            AlterColumn("dbo.PhotoGalleries", "CustomUserId", c => c.Guid(nullable: false));
            CreateIndex("dbo.PhotoGalleries", "CustomUserId");
            AddForeignKey("dbo.PhotoGalleries", "CustomUserId", "dbo.CustomUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PhotoGalleries", "CustomUserId", "dbo.CustomUsers");
            DropIndex("dbo.PhotoGalleries", new[] { "CustomUserId" });
            AlterColumn("dbo.PhotoGalleries", "CustomUserId", c => c.Guid());
            RenameColumn(table: "dbo.PhotoGalleries", name: "CustomUserId", newName: "CustomUser_Id");
            CreateIndex("dbo.PhotoGalleries", "CustomUser_Id");
            AddForeignKey("dbo.PhotoGalleries", "CustomUser_Id", "dbo.CustomUsers", "Id");
        }
    }
}
