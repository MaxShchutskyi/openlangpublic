namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MyAssingments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MyAssingnments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreatedId = c.Guid(nullable: false),
                        StudentId = c.Guid(nullable: false),
                        Topic = c.String(maxLength: 50),
                        Description = c.String(maxLength: 300),
                        Image = c.String(maxLength: 100),
                        IsCheched = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomUsers", t => t.CreatedId, cascadeDelete: false)
                .ForeignKey("dbo.CustomUsers", t => t.StudentId, cascadeDelete: false)
                .Index(t => t.CreatedId)
                .Index(t => t.StudentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MyAssingnments", "StudentId", "dbo.CustomUsers");
            DropForeignKey("dbo.MyAssingnments", "CreatedId", "dbo.CustomUsers");
            DropIndex("dbo.MyAssingnments", new[] { "StudentId" });
            DropIndex("dbo.MyAssingnments", new[] { "CreatedId" });
            DropTable("dbo.MyAssingnments");
        }
    }
}
