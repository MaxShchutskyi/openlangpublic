namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddChLang : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderDetails", "ChLang", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderDetails", "ChLang");
        }
    }
}
