namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HrenZnaetChto : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Attendees", "CustomRole_Id", "dbo.Role");
            DropIndex("dbo.Attendees", new[] { "CustomRole_Id" });
            DropColumn("dbo.Attendees", "CustomRole_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Attendees", "CustomRole_Id", c => c.Guid());
            CreateIndex("dbo.Attendees", "CustomRole_Id");
            AddForeignKey("dbo.Attendees", "CustomRole_Id", "dbo.Role", "Id");
        }
    }
}
