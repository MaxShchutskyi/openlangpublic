namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddValueToDiscount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Discounts", "Value", c => c.String(maxLength: 10));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Discounts", "Value");
        }
    }
}
