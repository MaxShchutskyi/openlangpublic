namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIndividualRates : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IndividualTeacherRates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Trial = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OneLessonRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FiveLessonRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenLessonRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FifteenLessonRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.IndividualTeacherRates", "UserId", "dbo.CustomUsers");
            DropIndex("dbo.IndividualTeacherRates", new[] { "UserId" });
            DropTable("dbo.IndividualTeacherRates");
        }
    }
}
