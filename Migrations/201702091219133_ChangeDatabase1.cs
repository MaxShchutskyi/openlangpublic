namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDatabase1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ScheduleTransactions", "PaymentTransactionId", "dbo.PaymentTransactions");
            DropIndex("dbo.ScheduleTransactions", new[] { "PaymentTransactionId" });
            AddColumn("dbo.Schedules", "Type", c => c.Int());
            AddColumn("dbo.Schedules", "Level", c => c.String(maxLength: 30));
            AddColumn("dbo.Schedules", "Language", c => c.String(maxLength: 10));
            AddColumn("dbo.ScheduleTransactions", "CustomUserId", c => c.Guid(nullable: false));
            CreateIndex("dbo.ScheduleTransactions", "CustomUserId");
            AddForeignKey("dbo.ScheduleTransactions", "CustomUserId", "dbo.CustomUsers", "Id", cascadeDelete: true);
            DropColumn("dbo.ScheduleTransactions", "PaymentTransactionId");
            DropColumn("dbo.ScheduleTransactions", "Type");
            DropColumn("dbo.ScheduleTransactions", "Level");
            DropColumn("dbo.ScheduleTransactions", "Subject");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ScheduleTransactions", "Subject", c => c.String(maxLength: 50));
            AddColumn("dbo.ScheduleTransactions", "Level", c => c.String(nullable: false, maxLength: 20));
            AddColumn("dbo.ScheduleTransactions", "Type", c => c.String(nullable: false, maxLength: 10));
            AddColumn("dbo.ScheduleTransactions", "PaymentTransactionId", c => c.String(nullable: false, maxLength: 100));
            DropForeignKey("dbo.ScheduleTransactions", "CustomUserId", "dbo.CustomUsers");
            DropIndex("dbo.ScheduleTransactions", new[] { "CustomUserId" });
            DropColumn("dbo.ScheduleTransactions", "CustomUserId");
            DropColumn("dbo.Schedules", "Language");
            DropColumn("dbo.Schedules", "Level");
            DropColumn("dbo.Schedules", "Type");
            CreateIndex("dbo.ScheduleTransactions", "PaymentTransactionId");
            AddForeignKey("dbo.ScheduleTransactions", "PaymentTransactionId", "dbo.PaymentTransactions", "PaymentId", cascadeDelete: true);
        }
    }
}
