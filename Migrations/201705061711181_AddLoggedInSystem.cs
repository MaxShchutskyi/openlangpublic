namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLoggedInSystem : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomUsers", "DateLoggedIn", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CustomUsers", "DateLoggedIn");
        }
    }
}
