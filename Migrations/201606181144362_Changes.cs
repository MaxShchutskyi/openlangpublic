namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Changes : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Schedules", "CustomUser_Id", "dbo.CustomUsers");
            DropIndex("dbo.Schedules", new[] { "CustomUser_Id" });
            RenameColumn(table: "dbo.Schedules", name: "CustomUser_Id", newName: "CustomUserId");
            AlterColumn("dbo.Schedules", "CustomUserId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Schedules", "CustomUserId");
            AddForeignKey("dbo.Schedules", "CustomUserId", "dbo.CustomUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Schedules", "CustomUserId", "dbo.CustomUsers");
            DropIndex("dbo.Schedules", new[] { "CustomUserId" });
            AlterColumn("dbo.Schedules", "CustomUserId", c => c.Guid());
            RenameColumn(table: "dbo.Schedules", name: "CustomUserId", newName: "CustomUser_Id");
            CreateIndex("dbo.Schedules", "CustomUser_Id");
            AddForeignKey("dbo.Schedules", "CustomUser_Id", "dbo.CustomUsers", "Id");
        }
    }
}
