namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeToMoneyAccount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomUsers", "Money", c => c.Decimal(nullable: false, precision: 6, scale: 2));
            AddColumn("dbo.PaymentTransactions", "EurPrice", c => c.Decimal(nullable: false, precision: 6, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaymentTransactions", "EurPrice");
            DropColumn("dbo.CustomUsers", "Money");
        }
    }
}
