namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTeacherInfo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TeacherInfoes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Link = c.String(maxLength: 200),
                        Relevant = c.String(maxLength: 4000),
                        Additional = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attendees", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TeacherInfoes", "Id", "dbo.Attendees");
            DropIndex("dbo.TeacherInfoes", new[] { "Id" });
            DropTable("dbo.TeacherInfoes");
        }
    }
}
