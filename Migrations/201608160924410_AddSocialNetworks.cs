namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSocialNetworks : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomUsers", "GoogleAccount", c => c.String(maxLength: 100));
            AddColumn("dbo.CustomUsers", "FaceTimeAccount", c => c.String(maxLength: 100));
            AddColumn("dbo.CustomUsers", "QQAccount", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CustomUsers", "QQAccount");
            DropColumn("dbo.CustomUsers", "FaceTimeAccount");
            DropColumn("dbo.CustomUsers", "GoogleAccount");
        }
    }
}
