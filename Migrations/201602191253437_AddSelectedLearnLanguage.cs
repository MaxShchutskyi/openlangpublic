namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSelectedLearnLanguage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Attendees", "SelectedLearnLang", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Attendees", "SelectedLearnLang");
        }
    }
}
