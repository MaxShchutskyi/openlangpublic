namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeBrainCard2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BrainCards", "OrderId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BrainCards", "OrderId");
        }
    }
}
