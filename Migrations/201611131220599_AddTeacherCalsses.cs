namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTeacherCalsses : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TeacherClasses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Language = c.String(maxLength: 30),
                        Level = c.String(maxLength: 4),
                        StartDate = c.DateTime(nullable: false),
                        Time = c.String(maxLength: 6),
                        Duration = c.Byte(nullable: false),
                        IsPrivate = c.Boolean(nullable: false),
                        MoneyEarned = c.Short(nullable: false),
                        TeacherCreatedId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomUsers", t => t.TeacherCreatedId, cascadeDelete: true)
                .Index(t => t.TeacherCreatedId);
            
            CreateTable(
                "dbo.StudentsClasses",
                c => new
                    {
                        TeacherClasses_Id = c.Int(nullable: false),
                        CustomUser_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.TeacherClasses_Id, t.CustomUser_Id })
                .ForeignKey("dbo.TeacherClasses", t => t.TeacherClasses_Id, cascadeDelete: false)
                .ForeignKey("dbo.CustomUsers", t => t.CustomUser_Id, cascadeDelete: false)
                .Index(t => t.TeacherClasses_Id)
                .Index(t => t.CustomUser_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TeacherClasses", "TeacherCreatedId", "dbo.CustomUsers");
            DropForeignKey("dbo.StudentsClasses", "CustomUser_Id", "dbo.CustomUsers");
            DropForeignKey("dbo.StudentsClasses", "TeacherClasses_Id", "dbo.TeacherClasses");
            DropIndex("dbo.StudentsClasses", new[] { "CustomUser_Id" });
            DropIndex("dbo.StudentsClasses", new[] { "TeacherClasses_Id" });
            DropIndex("dbo.TeacherClasses", new[] { "TeacherCreatedId" });
            DropTable("dbo.StudentsClasses");
            DropTable("dbo.TeacherClasses");
        }
    }
}
