namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeAnyQuestion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AnyQuestions", "FaqQuestionId", c => c.Int(nullable: false));
            AddColumn("dbo.AnyQuestions", "UserId", c => c.Guid());
            CreateIndex("dbo.AnyQuestions", "FaqQuestionId");
            CreateIndex("dbo.AnyQuestions", "UserId");
            AddForeignKey("dbo.AnyQuestions", "FaqQuestionId", "dbo.FAQQuestions", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AnyQuestions", "UserId", "dbo.Attendees", "AttendeeID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnyQuestions", "UserId", "dbo.Attendees");
            DropForeignKey("dbo.AnyQuestions", "FaqQuestionId", "dbo.FAQQuestions");
            DropIndex("dbo.AnyQuestions", new[] { "UserId" });
            DropIndex("dbo.AnyQuestions", new[] { "FaqQuestionId" });
            DropColumn("dbo.AnyQuestions", "UserId");
            DropColumn("dbo.AnyQuestions", "FaqQuestionId");
        }
    }
}
