namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNotifictos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NotificationSubscribes",
                c => new
                    {
                        Endpoint = c.String(nullable: false, maxLength: 128),
                        UserId = c.Guid(nullable: false),
                        Auth = c.String(maxLength: 500),
                        Pg = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.Endpoint)
                .ForeignKey("dbo.CustomUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NotificationSubscribes", "UserId", "dbo.CustomUsers");
            DropIndex("dbo.NotificationSubscribes", new[] { "UserId" });
            DropTable("dbo.NotificationSubscribes");
        }
    }
}
