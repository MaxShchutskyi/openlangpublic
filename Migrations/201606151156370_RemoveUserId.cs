namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveUserId : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Attendees", "Role_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Attendees", "Role_Id", c => c.Guid());
        }
    }
}
