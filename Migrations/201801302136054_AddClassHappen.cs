namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddClassHappen : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Schedules", "ClassIsNotHappen", c => c.Boolean(nullable: false, defaultValue:false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Schedules", "ClassIsNotHappen");
        }
    }
}
