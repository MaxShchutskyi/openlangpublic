namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAdditional : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdditionalUserInformations",
                c => new
                    {
                        CustomUserId = c.Guid(nullable: false),
                        WhatLookingFor = c.String(maxLength: 500),
                        ShotIntrodution = c.String(maxLength: 1200),
                        LongIntrodution = c.String(maxLength: 1200),
                    })
                .PrimaryKey(t => t.CustomUserId)
                .ForeignKey("dbo.CustomUsers", t => t.CustomUserId)
                .Index(t => t.CustomUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AdditionalUserInformations", "CustomUserId", "dbo.CustomUsers");
            DropIndex("dbo.AdditionalUserInformations", new[] { "CustomUserId" });
            DropTable("dbo.AdditionalUserInformations");
        }
    }
}
