namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BrainCards", "VideoSrc", c => c.String(storeType: "ntext"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BrainCards", "VideoSrc");
        }
    }
}
