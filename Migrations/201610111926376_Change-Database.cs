namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDatabase : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CustomUsers", "LanguageId", "dbo.Languages");
            DropForeignKey("dbo.CustomUsers", "LevelId", "dbo.Levels");
            DropForeignKey("dbo.ReservedClasses", "Attendees_Id", "dbo.CustomUsers");
            DropForeignKey("dbo.Classes", "LanguageId", "dbo.Languages");
            DropForeignKey("dbo.Classes", "LevelId", "dbo.Levels");
            DropForeignKey("dbo.Classes", "Presenters_Id", "dbo.Presenters");
            DropForeignKey("dbo.Presenters", "Role_Id", "dbo.AspNetRoles");
            DropForeignKey("dbo.ReservedClasses", "Classes_ClassID", "dbo.Classes");
            DropForeignKey("dbo.Subjects", "ClassModels_ClassID", "dbo.Classes");
            DropForeignKey("dbo.Subscriptions", "LanguageId", "dbo.Languages");
            DropForeignKey("dbo.Subscriptions", "LevelId", "dbo.Levels");
            DropIndex("dbo.CustomUsers", new[] { "LanguageId" });
            DropIndex("dbo.CustomUsers", new[] { "LevelId" });
            DropIndex("dbo.ReservedClasses", new[] { "Attendees_Id" });
            DropIndex("dbo.ReservedClasses", new[] { "Classes_ClassID" });
            DropIndex("dbo.Classes", new[] { "LevelId" });
            DropIndex("dbo.Classes", new[] { "LanguageId" });
            DropIndex("dbo.Classes", new[] { "Presenters_Id" });
            DropIndex("dbo.Presenters", new[] { "Role_Id" });
            DropIndex("dbo.Subjects", new[] { "ClassModels_ClassID" });
            DropIndex("dbo.Subscriptions", new[] { "LevelId" });
            DropIndex("dbo.Subscriptions", new[] { "LanguageId" });
            RenameColumn(table: "dbo.CustomUsers", name: "ZoneId", newName: "Timezones_Id");
            RenameIndex(table: "dbo.CustomUsers", name: "IX_ZoneId", newName: "IX_Timezones_Id");
            DropColumn("dbo.CustomUsers", "ScreenName");
            DropColumn("dbo.CustomUsers", "LanguageCultureName");
            DropColumn("dbo.CustomUsers", "AttendeeUrl");
            DropColumn("dbo.CustomUsers", "LanguageId");
            DropColumn("dbo.CustomUsers", "LevelId");
            DropColumn("dbo.CustomUsers", "FirstName");
            DropColumn("dbo.CustomUsers", "LastName");
            DropColumn("dbo.CustomUsers", "QqId");
            DropColumn("dbo.CustomUsers", "PlaceOfBirth");
            DropColumn("dbo.CustomUsers", "CountryOfBirth");
            DropColumn("dbo.CustomUsers", "Nationality");
            DropColumn("dbo.CustomUsers", "Address");
            DropColumn("dbo.Subscriptions", "LevelId");
            DropColumn("dbo.Subscriptions", "LanguageId");
            DropTable("dbo.Languages");
            DropTable("dbo.Levels");
            DropTable("dbo.ReservedClasses");
            DropTable("dbo.Classes");
            DropTable("dbo.Presenters");
            DropTable("dbo.Subjects");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ClassModels_ClassID = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Presenters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PresenterEmail = c.String(),
                        PresenterId = c.Int(nullable: false),
                        Name = c.String(),
                        Password = c.String(),
                        PhoneNumber = c.String(),
                        MobileNumber = c.String(),
                        TimeZone = c.String(),
                        AboutTheTeacher = c.String(),
                        CanScheduleClass = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        PostFilePath = c.String(),
                        CreationDateTime = c.DateTime(nullable: false),
                        RoleId = c.Int(),
                        Role_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Classes",
                c => new
                    {
                        ClassID = c.Long(nullable: false, identity: true),
                        ClassApiId = c.Int(nullable: false),
                        ClassMasterID = c.Int(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        Title = c.String(nullable: false),
                        TimeZone = c.String(),
                        AttendeeLimit = c.Int(nullable: false),
                        Duration = c.Int(nullable: false),
                        PresenterDefaultControls = c.String(),
                        AttendeeDefaultControls = c.String(),
                        CreateRecording = c.Boolean(nullable: false),
                        ReturnUrl = c.String(),
                        StatusPingUrl = c.String(),
                        PresenterModelId = c.Int(),
                        RecordingUrl = c.String(),
                        PresenterUrl = c.String(),
                        CoPresenterUrl = c.String(),
                        IsPermanent = c.Boolean(nullable: false),
                        IsRecurring = c.Boolean(nullable: false),
                        Type = c.String(),
                        LevelId = c.Int(),
                        SpecificTopic = c.String(),
                        LanguageCultureName = c.String(),
                        LanguageId = c.Int(),
                        HasSeats = c.Boolean(nullable: false),
                        CanJoin = c.Boolean(nullable: false),
                        Description = c.String(),
                        Image = c.String(),
                        Document = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        Presenters_Id = c.Int(),
                    })
                .PrimaryKey(t => t.ClassID);
            
            CreateTable(
                "dbo.ReservedClasses",
                c => new
                    {
                        ReservedClassID = c.Long(nullable: false, identity: true),
                        AttendeeUrl = c.String(),
                        ClassId = c.Int(),
                        AttendeeId = c.Guid(),
                        Attendees_Id = c.Guid(),
                        Classes_ClassID = c.Long(),
                    })
                .PrimaryKey(t => t.ReservedClassID);
            
            CreateTable(
                "dbo.Levels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Languages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Abbreviation = c.String(),
                        Name = c.String(),
                        Culture = c.String(),
                        ImageSrc = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Subscriptions", "LanguageId", c => c.Int(nullable: false));
            AddColumn("dbo.Subscriptions", "LevelId", c => c.Int(nullable: false));
            AddColumn("dbo.CustomUsers", "Address", c => c.String());
            AddColumn("dbo.CustomUsers", "Nationality", c => c.String());
            AddColumn("dbo.CustomUsers", "CountryOfBirth", c => c.String());
            AddColumn("dbo.CustomUsers", "PlaceOfBirth", c => c.String());
            AddColumn("dbo.CustomUsers", "QqId", c => c.String());
            AddColumn("dbo.CustomUsers", "LastName", c => c.String());
            AddColumn("dbo.CustomUsers", "FirstName", c => c.String());
            AddColumn("dbo.CustomUsers", "LevelId", c => c.Int());
            AddColumn("dbo.CustomUsers", "LanguageId", c => c.Int());
            AddColumn("dbo.CustomUsers", "AttendeeUrl", c => c.String());
            AddColumn("dbo.CustomUsers", "LanguageCultureName", c => c.String());
            AddColumn("dbo.CustomUsers", "ScreenName", c => c.String());
            RenameIndex(table: "dbo.CustomUsers", name: "IX_Timezones_Id", newName: "IX_ZoneId");
            RenameColumn(table: "dbo.CustomUsers", name: "Timezones_Id", newName: "ZoneId");
            CreateIndex("dbo.Subscriptions", "LanguageId");
            CreateIndex("dbo.Subscriptions", "LevelId");
            CreateIndex("dbo.Subjects", "ClassModels_ClassID");
            CreateIndex("dbo.Presenters", "Role_Id");
            CreateIndex("dbo.Classes", "Presenters_Id");
            CreateIndex("dbo.Classes", "LanguageId");
            CreateIndex("dbo.Classes", "LevelId");
            CreateIndex("dbo.ReservedClasses", "Classes_ClassID");
            CreateIndex("dbo.ReservedClasses", "Attendees_Id");
            CreateIndex("dbo.CustomUsers", "LevelId");
            CreateIndex("dbo.CustomUsers", "LanguageId");
            AddForeignKey("dbo.Subscriptions", "LevelId", "dbo.Levels", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Subscriptions", "LanguageId", "dbo.Languages", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Subjects", "ClassModels_ClassID", "dbo.Classes", "ClassID");
            AddForeignKey("dbo.ReservedClasses", "Classes_ClassID", "dbo.Classes", "ClassID");
            AddForeignKey("dbo.Presenters", "Role_Id", "dbo.AspNetRoles", "Id");
            AddForeignKey("dbo.Classes", "Presenters_Id", "dbo.Presenters", "Id");
            AddForeignKey("dbo.Classes", "LevelId", "dbo.Levels", "Id");
            AddForeignKey("dbo.Classes", "LanguageId", "dbo.Languages", "Id");
            AddForeignKey("dbo.ReservedClasses", "Attendees_Id", "dbo.CustomUsers", "Id");
            AddForeignKey("dbo.CustomUsers", "LevelId", "dbo.Levels", "Id");
            AddForeignKey("dbo.CustomUsers", "LanguageId", "dbo.Languages", "Id");
        }
    }
}
