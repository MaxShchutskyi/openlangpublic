namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBonusToPayment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentTransactions", "Bonus", c => c.Decimal(nullable: false, precision: 18, scale: 2, defaultValue:0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaymentTransactions", "Bonus");
        }
    }
}
