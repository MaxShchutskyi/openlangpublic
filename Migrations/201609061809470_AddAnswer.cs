namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAnswer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MyAssingnments", "Answer", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MyAssingnments", "Answer");
        }
    }
}
