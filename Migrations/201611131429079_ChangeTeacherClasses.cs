namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeTeacherClasses : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TeacherClasses", "MaterialsUsed", c => c.String(maxLength: 200));
            AddColumn("dbo.TeacherClasses", "Comments", c => c.String(maxLength: 4000));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TeacherClasses", "Comments");
            DropColumn("dbo.TeacherClasses", "MaterialsUsed");
        }
    }
}
