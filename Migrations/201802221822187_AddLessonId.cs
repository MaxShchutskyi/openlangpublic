namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLessonId : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.BrainCards", name: "Lesson_Id", newName: "LessonId");
            RenameIndex(table: "dbo.BrainCards", name: "IX_Lesson_Id", newName: "IX_LessonId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.BrainCards", name: "IX_LessonId", newName: "IX_Lesson_Id");
            RenameColumn(table: "dbo.BrainCards", name: "LessonId", newName: "Lesson_Id");
        }
    }
}
