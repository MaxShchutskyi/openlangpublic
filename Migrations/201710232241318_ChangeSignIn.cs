namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeSignIn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomUsers", "IsSentApplication", c => c.Boolean(nullable: false));
            AddColumn("dbo.CustomUsers", "IsApplicationChanckedByAdmin", c => c.Boolean(nullable: false));
            AddColumn("dbo.CustomUsers", "IsSignedContract", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CustomUsers", "IsSignedContract");
            DropColumn("dbo.CustomUsers", "IsApplicationChanckedByAdmin");
            DropColumn("dbo.CustomUsers", "IsSentApplication");
        }
    }
}
