namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddQuestionsAndComments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(),
                        CreateDate = c.DateTime(nullable: false),
                        Comment = c.String(),
                        FaqQuestion = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FAQQuestions", t => t.FaqQuestion, cascadeDelete: true)
                .ForeignKey("dbo.Attendees", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.FaqQuestion);
            
            CreateTable(
                "dbo.FAQQuestions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Question = c.String(),
                        Answer = c.String(),
                        IsHelpCount = c.Int(nullable: false),
                        IsNotHelpCount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comments", "UserId", "dbo.Attendees");
            DropForeignKey("dbo.Comments", "FaqQuestion", "dbo.FAQQuestions");
            DropIndex("dbo.Comments", new[] { "FaqQuestion" });
            DropIndex("dbo.Comments", new[] { "UserId" });
            DropTable("dbo.FAQQuestions");
            DropTable("dbo.Comments");
        }
    }
}
