namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddChat : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DialogReplies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        DialogId = c.Int(nullable: false),
                        UtcDateTime = c.String(maxLength: 50),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomUsers", t => t.UserId)
                .ForeignKey("dbo.Dialogs", t => t.DialogId)
                .Index(t => t.UserId)
                .Index(t => t.DialogId);
            
            CreateTable(
                "dbo.Dialogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserOneId = c.Guid(nullable: false),
                        UserTwoId = c.Guid(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomUsers", t => t.UserOneId)
                .ForeignKey("dbo.CustomUsers", t => t.UserTwoId)
                .Index(t => t.UserOneId)
                .Index(t => t.UserTwoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DialogReplies", "DialogId", "dbo.Dialogs");
            DropForeignKey("dbo.Dialogs", "UserTwoId", "dbo.CustomUsers");
            DropForeignKey("dbo.Dialogs", "UserOneId", "dbo.CustomUsers");
            DropForeignKey("dbo.DialogReplies", "UserId", "dbo.CustomUsers");
            DropIndex("dbo.Dialogs", new[] { "UserTwoId" });
            DropIndex("dbo.Dialogs", new[] { "UserOneId" });
            DropIndex("dbo.DialogReplies", new[] { "DialogId" });
            DropIndex("dbo.DialogReplies", new[] { "UserId" });
            DropTable("dbo.Dialogs");
            DropTable("dbo.DialogReplies");
        }
    }
}
