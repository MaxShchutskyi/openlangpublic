namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePropertyForAllTypes : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CustomUsers", "PricesForAllTypes", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CustomUsers", "PricesForAllTypes", c => c.String(nullable: false, maxLength: 100));
        }
    }
}
