namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CjangeRates : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TeacherRates", "Individual45Rate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.TeacherRates", "Individual60Rate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.TeacherRates", "GroupRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TeacherRates", "GroupRate", c => c.Short(nullable: false));
            AlterColumn("dbo.TeacherRates", "Individual60Rate", c => c.Short(nullable: false));
            AlterColumn("dbo.TeacherRates", "Individual45Rate", c => c.Short(nullable: false));
        }
    }
}
