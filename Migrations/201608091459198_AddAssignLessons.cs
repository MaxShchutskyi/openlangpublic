namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAssignLessons : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ScheduleStudents", newName: "AssignLessonsOfStudents");
            DropForeignKey("dbo.AssignLessonsOfStudents", "CustomUserId", "dbo.CustomUsers");
            DropIndex("dbo.AssignLessonsOfStudents", new[] { "CustomUserId" });
            AddColumn("dbo.Schedules", "ScheduleStudentsId", c => c.String());
            AddColumn("dbo.AssignLessonsOfStudents", "PaymentId", c => c.String(maxLength: 100));
            CreateIndex("dbo.AssignLessonsOfStudents", "PaymentId");
            AddForeignKey("dbo.AssignLessonsOfStudents", "PaymentId", "dbo.PaymentTransactions", "PaymentId");
            DropColumn("dbo.AssignLessonsOfStudents", "CustomUserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AssignLessonsOfStudents", "CustomUserId", c => c.Guid(nullable: false));
            DropForeignKey("dbo.AssignLessonsOfStudents", "PaymentId", "dbo.PaymentTransactions");
            DropIndex("dbo.AssignLessonsOfStudents", new[] { "PaymentId" });
            DropColumn("dbo.AssignLessonsOfStudents", "PaymentId");
            DropColumn("dbo.Schedules", "ScheduleStudentsId");
            CreateIndex("dbo.AssignLessonsOfStudents", "CustomUserId");
            AddForeignKey("dbo.AssignLessonsOfStudents", "CustomUserId", "dbo.CustomUsers", "Id", cascadeDelete: true);
            RenameTable(name: "dbo.AssignLessonsOfStudents", newName: "ScheduleStudents");
        }
    }
}
