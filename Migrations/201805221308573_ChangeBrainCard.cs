namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeBrainCard : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BrainCards", "IconSrc", c => c.String());
            AddColumn("dbo.BrainCards", "ImageSlideSrc", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BrainCards", "ImageSlideSrc");
            DropColumn("dbo.BrainCards", "IconSrc");
        }
    }
}
