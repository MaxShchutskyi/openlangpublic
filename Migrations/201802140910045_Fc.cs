namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BrainCards", "AdditionalJSON", c => c.String(storeType: "ntext"));
            DropColumn("dbo.Answers", "AdditionalJSON");
            DropColumn("dbo.SubAnswers", "AdditionalJSON");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SubAnswers", "AdditionalJSON", c => c.String());
            AddColumn("dbo.Answers", "AdditionalJSON", c => c.String(storeType: "ntext"));
            DropColumn("dbo.BrainCards", "AdditionalJSON");
        }
    }
}
