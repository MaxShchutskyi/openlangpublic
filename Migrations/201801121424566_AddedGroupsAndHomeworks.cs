namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedGroupsAndHomeworks : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnswersOfStudents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CardId = c.Int(nullable: false),
                        HomeworkId = c.Int(nullable: false),
                        IsRightAnswer = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BrainCards", t => t.CardId, cascadeDelete: false)
                .ForeignKey("dbo.Homework", t => t.HomeworkId, cascadeDelete: false)
                .Index(t => t.CardId)
                .Index(t => t.HomeworkId);
            
            CreateTable(
                "dbo.Homework",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreationDate = c.DateTime(nullable: false),
                        PlanForComplete = c.DateTime(nullable: false),
                        GroupId = c.Int(nullable: false),
                        Statistic = c.String(),
                        TeacherId = c.Guid(nullable: false),
                        StudentId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Groups", t => t.GroupId, cascadeDelete: false)
                .ForeignKey("dbo.CustomUsers", t => t.StudentId, cascadeDelete: false)
                .ForeignKey("dbo.CustomUsers", t => t.TeacherId, cascadeDelete: false)
                .Index(t => t.GroupId)
                .Index(t => t.TeacherId)
                .Index(t => t.StudentId);
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        TeacherId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomUsers", t => t.TeacherId, cascadeDelete: false)
                .Index(t => t.TeacherId);
            
            CreateTable(
                "dbo.GroupBrainCards",
                c => new
                    {
                        Group_Id = c.Int(nullable: false),
                        BrainCard_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Group_Id, t.BrainCard_Id })
                .ForeignKey("dbo.Groups", t => t.Group_Id, cascadeDelete: true)
                .ForeignKey("dbo.BrainCards", t => t.BrainCard_Id, cascadeDelete: true)
                .Index(t => t.Group_Id)
                .Index(t => t.BrainCard_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnswersOfStudents", "HomeworkId", "dbo.Homework");
            DropForeignKey("dbo.Homework", "TeacherId", "dbo.CustomUsers");
            DropForeignKey("dbo.Homework", "StudentId", "dbo.CustomUsers");
            DropForeignKey("dbo.Homework", "GroupId", "dbo.Groups");
            DropForeignKey("dbo.Groups", "TeacherId", "dbo.CustomUsers");
            DropForeignKey("dbo.GroupBrainCards", "BrainCard_Id", "dbo.BrainCards");
            DropForeignKey("dbo.GroupBrainCards", "Group_Id", "dbo.Groups");
            DropForeignKey("dbo.AnswersOfStudents", "CardId", "dbo.BrainCards");
            DropIndex("dbo.GroupBrainCards", new[] { "BrainCard_Id" });
            DropIndex("dbo.GroupBrainCards", new[] { "Group_Id" });
            DropIndex("dbo.Groups", new[] { "TeacherId" });
            DropIndex("dbo.Homework", new[] { "StudentId" });
            DropIndex("dbo.Homework", new[] { "TeacherId" });
            DropIndex("dbo.Homework", new[] { "GroupId" });
            DropIndex("dbo.AnswersOfStudents", new[] { "HomeworkId" });
            DropIndex("dbo.AnswersOfStudents", new[] { "CardId" });
            DropTable("dbo.GroupBrainCards");
            DropTable("dbo.Groups");
            DropTable("dbo.Homework");
            DropTable("dbo.AnswersOfStudents");
        }
    }
}
