namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewLigic : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Chapters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        Language = c.String(maxLength: 50),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Lessons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        Language = c.String(maxLength: 50),
                        Level = c.String(),
                        UserId = c.Guid(nullable: false),
                        ChapterId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Chapters", t => t.ChapterId, cascadeDelete: true)
                .ForeignKey("dbo.CustomUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.ChapterId);
            
            AddColumn("dbo.BrainCards", "Lesson_Id", c => c.Int());
            CreateIndex("dbo.BrainCards", "Lesson_Id");
            AddForeignKey("dbo.BrainCards", "Lesson_Id", "dbo.Lessons", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Lessons", "UserId", "dbo.CustomUsers");
            DropForeignKey("dbo.Lessons", "ChapterId", "dbo.Chapters");
            DropForeignKey("dbo.BrainCards", "Lesson_Id", "dbo.Lessons");
            DropForeignKey("dbo.Chapters", "UserId", "dbo.CustomUsers");
            DropIndex("dbo.Lessons", new[] { "ChapterId" });
            DropIndex("dbo.Lessons", new[] { "UserId" });
            DropIndex("dbo.Chapters", new[] { "UserId" });
            DropIndex("dbo.BrainCards", new[] { "Lesson_Id" });
            DropColumn("dbo.BrainCards", "Lesson_Id");
            DropTable("dbo.Lessons");
            DropTable("dbo.Chapters");
        }
    }
}
