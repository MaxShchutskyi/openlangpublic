namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAudioPath : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MyWords", "AudioPath", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MyWords", "AudioPath");
        }
    }
}
