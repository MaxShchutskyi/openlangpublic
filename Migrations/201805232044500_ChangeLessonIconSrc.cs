namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeLessonIconSrc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Lessons", "IconSrc", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Lessons", "IconSrc");
        }
    }
}
