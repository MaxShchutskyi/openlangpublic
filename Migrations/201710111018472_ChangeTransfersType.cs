namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeTransfersType : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Transfers", "StudentId", "dbo.CustomUsers");
            DropIndex("dbo.Transfers", new[] { "StudentId" });
            AddColumn("dbo.Transfers", "PieceOfCreditId", c => c.Int(nullable: false));
            AddColumn("dbo.Transfers", "CompanyCreditId", c => c.Int(nullable: false));
            AddColumn("dbo.Transfers", "TotatPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            CreateIndex("dbo.Transfers", "PieceOfCreditId");
            CreateIndex("dbo.Transfers", "CompanyCreditId");
            AddForeignKey("dbo.Transfers", "CompanyCreditId", "dbo.Credits", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Transfers", "PieceOfCreditId", "dbo.Credits", "Id", cascadeDelete: false);
            DropColumn("dbo.Transfers", "StudentId");
            DropColumn("dbo.Transfers", "Amount");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Transfers", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Transfers", "StudentId", c => c.Guid(nullable: false));
            DropForeignKey("dbo.Transfers", "PieceOfCreditId", "dbo.Credits");
            DropForeignKey("dbo.Transfers", "CompanyCreditId", "dbo.Credits");
            DropIndex("dbo.Transfers", new[] { "CompanyCreditId" });
            DropIndex("dbo.Transfers", new[] { "PieceOfCreditId" });
            DropColumn("dbo.Transfers", "TotatPrice");
            DropColumn("dbo.Transfers", "CompanyCreditId");
            DropColumn("dbo.Transfers", "PieceOfCreditId");
            CreateIndex("dbo.Transfers", "StudentId");
            AddForeignKey("dbo.Transfers", "StudentId", "dbo.CustomUsers", "Id", cascadeDelete: true);
        }
    }
}
