namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPdfFile : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Lessons", "PathOfSlides", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Lessons", "PathOfSlides");
        }
    }
}
