namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ManyToMany : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ScheduleStudents", "CustomUser_Id", "dbo.CustomUsers");
            DropForeignKey("dbo.ScheduleStudents", "Schedule_Id", "dbo.Schedules");
            DropForeignKey("dbo.Schedules", "CustomUser_Id", "dbo.CustomUsers");
            DropIndex("dbo.ScheduleStudents", new[] { "CustomUser_Id" });
            DropIndex("dbo.ScheduleStudents", new[] { "Schedule_Id" });
            AddColumn("dbo.CustomUsers", "Schedule_Id", c => c.Int());
            AddColumn("dbo.Schedules", "CustomUser_Id1", c => c.Guid());
            CreateIndex("dbo.CustomUsers", "Schedule_Id");
            CreateIndex("dbo.Schedules", "CustomUser_Id1");
            AddForeignKey("dbo.CustomUsers", "Schedule_Id", "dbo.Schedules", "Id");
            AddForeignKey("dbo.Schedules", "CustomUser_Id1", "dbo.CustomUsers", "Id");
            DropTable("dbo.ScheduleStudents");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ScheduleStudents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 200),
                        CustomUser_Id = c.Guid(),
                        Schedule_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.Schedules", "CustomUser_Id1", "dbo.CustomUsers");
            DropForeignKey("dbo.CustomUsers", "Schedule_Id", "dbo.Schedules");
            DropIndex("dbo.Schedules", new[] { "CustomUser_Id1" });
            DropIndex("dbo.CustomUsers", new[] { "Schedule_Id" });
            DropColumn("dbo.Schedules", "CustomUser_Id1");
            DropColumn("dbo.CustomUsers", "Schedule_Id");
            CreateIndex("dbo.ScheduleStudents", "Schedule_Id");
            CreateIndex("dbo.ScheduleStudents", "CustomUser_Id");
            AddForeignKey("dbo.Schedules", "CustomUser_Id", "dbo.CustomUsers", "Id");
            AddForeignKey("dbo.ScheduleStudents", "Schedule_Id", "dbo.Schedules", "Id");
            AddForeignKey("dbo.ScheduleStudents", "CustomUser_Id", "dbo.CustomUsers", "Id");
        }
    }
}
