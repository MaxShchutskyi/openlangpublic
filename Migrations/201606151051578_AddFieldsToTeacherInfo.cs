namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldsToTeacherInfo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TeacherInfoes", "From", c => c.String(maxLength: 100));
            AddColumn("dbo.TeacherInfoes", "Teachers", c => c.String(maxLength: 150));
            AddColumn("dbo.TeacherInfoes", "SpecialSkills", c => c.String(maxLength: 500));
            AddColumn("dbo.TeacherInfoes", "AboutMe", c => c.String(maxLength: 4000));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TeacherInfoes", "AboutMe");
            DropColumn("dbo.TeacherInfoes", "SpecialSkills");
            DropColumn("dbo.TeacherInfoes", "Teachers");
            DropColumn("dbo.TeacherInfoes", "From");
        }
    }
}
