namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsPendingToScheduler : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Schedules", "IsPending", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Schedules", "IsPending");
        }
    }
}
