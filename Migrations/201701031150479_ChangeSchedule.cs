namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeSchedule : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Schedules", "IsConfirmed", c => c.Boolean(nullable: false));
            AddColumn("dbo.Schedules", "Title", c => c.String(maxLength: 100));
            AddColumn("dbo.Schedules", "Subject", c => c.String(maxLength: 150));
            AddColumn("dbo.Schedules", "Description", c => c.String(maxLength: 500));
            AddColumn("dbo.Schedules", "Img", c => c.String(maxLength: 150));
            DropColumn("dbo.Schedules", "IsPending");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Schedules", "IsPending", c => c.Boolean(nullable: false));
            DropColumn("dbo.Schedules", "Img");
            DropColumn("dbo.Schedules", "Description");
            DropColumn("dbo.Schedules", "Subject");
            DropColumn("dbo.Schedules", "Title");
            DropColumn("dbo.Schedules", "IsConfirmed");
        }
    }
}
