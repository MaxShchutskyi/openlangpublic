namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUploadFilePath : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MyAssingnments", "UploadFilePath", c => c.String(maxLength: 150));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MyAssingnments", "UploadFilePath");
        }
    }
}
