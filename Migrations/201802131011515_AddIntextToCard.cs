namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIntextToCard : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Answers", "Index", c => c.Int(nullable:true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Answers", "Index", c => c.Int(nullable: false));
        }
    }
}
