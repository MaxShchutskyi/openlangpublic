namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIconSrcChapter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Chapters", "IconSrc", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Chapters", "IconSrc");
        }
    }
}
