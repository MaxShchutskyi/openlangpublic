namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeColumnType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Answers", "ImageSrc", c => c.String(storeType: "ntext"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Answers", "ImageSrc", c => c.String(maxLength: 1000));
        }
    }
}
