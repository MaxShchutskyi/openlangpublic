namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeTeacherClassesAgain : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TeacherClasses", "SubjectOfTheLesson", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TeacherClasses", "SubjectOfTheLesson");
        }
    }
}
