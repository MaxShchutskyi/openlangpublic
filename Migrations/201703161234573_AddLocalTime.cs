namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLocalTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomUsers", "LocalTime", c => c.Short());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CustomUsers", "LocalTime");
        }
    }
}
