namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNotifications : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IconName = c.String(),
                        Message = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.NotificationsUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NotificationId = c.Int(nullable: false),
                        CustomUserId = c.Guid(nullable: false),
                        IsRead = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomUsers", t => t.CustomUserId, cascadeDelete: false)
                .ForeignKey("dbo.Notifications", t => t.NotificationId, cascadeDelete: false)
                .Index(t => t.NotificationId)
                .Index(t => t.CustomUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Notifications", "UserId", "dbo.CustomUsers");
            DropForeignKey("dbo.NotificationsUsers", "NotificationId", "dbo.Notifications");
            DropForeignKey("dbo.NotificationsUsers", "CustomUserId", "dbo.CustomUsers");
            DropIndex("dbo.NotificationsUsers", new[] { "CustomUserId" });
            DropIndex("dbo.NotificationsUsers", new[] { "NotificationId" });
            DropIndex("dbo.Notifications", new[] { "UserId" });
            DropTable("dbo.NotificationsUsers");
            DropTable("dbo.Notifications");
        }
    }
}
