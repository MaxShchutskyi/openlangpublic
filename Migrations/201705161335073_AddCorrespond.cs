namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCorrespond : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Corresponds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TeacherId = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        Subject = c.String(nullable: false, maxLength: 200),
                        Text = c.String(nullable: false, maxLength: 200),
                        Email = c.String(nullable: false, maxLength: 50),
                        Input = c.Boolean(nullable: false),
                        MessageCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomUsers", t => t.TeacherId, cascadeDelete: true)
                .Index(t => t.TeacherId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Corresponds", "TeacherId", "dbo.CustomUsers");
            DropIndex("dbo.Corresponds", new[] { "TeacherId" });
            DropTable("dbo.Corresponds");
        }
    }
}
