namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updates_25_06_2016 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TeacherInfoes", "VideoDescription", c => c.String(maxLength: 1000));
            AddColumn("dbo.TeacherInfoes", "HasTrialLessonn", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TeacherInfoes", "HasTrialLessonn");
            DropColumn("dbo.TeacherInfoes", "VideoDescription");
        }
    }
}
