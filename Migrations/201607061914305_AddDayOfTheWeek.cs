namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDayOfTheWeek : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Schedules", "DayOfTheWeek", c => c.Byte(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Schedules", "DayOfTheWeek");
        }
    }
}
