namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdteTimezones : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Schedules", "StartDateTime", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.Schedules", "EndDateTime", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.Schedules", "Status", c => c.String(nullable: false, maxLength: 40));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Schedules", "Status", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Schedules", "EndDateTime", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Schedules", "StartDateTime", c => c.String(nullable: false, maxLength: 20));
        }
    }
}
