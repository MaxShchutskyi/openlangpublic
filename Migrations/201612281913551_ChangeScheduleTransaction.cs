namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeScheduleTransaction : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ScheduleTransactions", "Type", c => c.String(nullable: false, maxLength: 10));
            AddColumn("dbo.ScheduleTransactions", "CreatedDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ScheduleTransactions", "CreatedDate");
            DropColumn("dbo.ScheduleTransactions", "Type");
        }
    }
}
