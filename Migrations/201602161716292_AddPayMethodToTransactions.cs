namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPayMethodToTransactions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentTransactions", "PaymentMethod", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaymentTransactions", "PaymentMethod");
        }
    }
}
