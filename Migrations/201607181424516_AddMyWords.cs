namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMyWords : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MyWords",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Word = c.String(maxLength: 500),
                        Image = c.String(maxLength: 150),
                        Description = c.String(maxLength: 4000),
                        CustomUserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomUsers", t => t.CustomUserId, cascadeDelete: true)
                .Index(t => t.CustomUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MyWords", "CustomUserId", "dbo.CustomUsers");
            DropIndex("dbo.MyWords", new[] { "CustomUserId" });
            DropTable("dbo.MyWords");
        }
    }
}
