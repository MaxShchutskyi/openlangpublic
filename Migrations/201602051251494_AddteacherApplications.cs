namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddteacherApplications : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TeacherApplications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 15),
                        Language = c.String(maxLength: 20),
                        Telephone = c.String(maxLength: 21),
                        Link = c.String(maxLength: 200),
                        Relevant = c.String(),
                        Additional = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TeacherApplications");
        }
    }
}
