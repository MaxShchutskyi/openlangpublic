namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeOrderDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderDetails", "Timezone", c => c.String());
            AddColumn("dbo.OrderDetails", "Telephone", c => c.String());
            AddColumn("dbo.OrderDetails", "HowDidFind", c => c.String());
            AddColumn("dbo.OrderDetails", "Skype", c => c.String());
            AddColumn("dbo.OrderDetails", "StartTime", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderDetails", "StartTime");
            DropColumn("dbo.OrderDetails", "Skype");
            DropColumn("dbo.OrderDetails", "HowDidFind");
            DropColumn("dbo.OrderDetails", "Telephone");
            DropColumn("dbo.OrderDetails", "Timezone");
        }
    }
}
