namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBrainCards : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BrainCards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 300),
                        Subject = c.String(maxLength: 300),
                        UploadDate = c.DateTime(nullable: false),
                        Language = c.String(maxLength: 20),
                        UploadedUserId = c.Guid(nullable: false),
                        Question = c.String(maxLength: 1000),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CustomUsers", t => t.UploadedUserId, cascadeDelete: true)
                .Index(t => t.UploadedUserId);
            
            CreateTable(
                "dbo.Answers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Variant = c.String(maxLength: 500),
                        IsRight = c.Boolean(nullable: false),
                        ImageSrc = c.String(maxLength: 1000),
                        CardId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BrainCards", t => t.CardId, cascadeDelete: true)
                .Index(t => t.CardId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BrainCards", "UploadedUserId", "dbo.CustomUsers");
            DropForeignKey("dbo.Answers", "CardId", "dbo.BrainCards");
            DropIndex("dbo.Answers", new[] { "CardId" });
            DropIndex("dbo.BrainCards", new[] { "UploadedUserId" });
            DropTable("dbo.Answers");
            DropTable("dbo.BrainCards");
        }
    }
}
