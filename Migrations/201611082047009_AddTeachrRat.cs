namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTeachrRat : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TeacherRates",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        Individual45Rate = c.Short(nullable: false),
                        Individual60Rate = c.Short(nullable: false),
                        GroupRate = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.CustomUsers", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TeacherRates", "UserId", "dbo.CustomUsers");
            DropIndex("dbo.TeacherRates", new[] { "UserId" });
            DropTable("dbo.TeacherRates");
        }
    }
}
