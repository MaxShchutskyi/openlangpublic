namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUpladMssagePath : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MyAssingnments", "UploadMessagePath", c => c.String(maxLength: 150));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MyAssingnments", "UploadMessagePath");
        }
    }
}
