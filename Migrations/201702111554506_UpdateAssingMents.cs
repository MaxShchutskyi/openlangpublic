using DTO.Enums;

namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAssingMents : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MyAssingnments", "DateOfChange", c => c.DateTime(nullable: false, defaultValue:DateTime.UtcNow));
            AddColumn("dbo.MyAssingnments", "State", c => c.Int(nullable: false, defaultValue: (int?) StateAssingmets.Unchecked));
            DropColumn("dbo.MyAssingnments", "IsCheched");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MyAssingnments", "IsCheched", c => c.Boolean(nullable: false));
            DropColumn("dbo.MyAssingnments", "State");
            DropColumn("dbo.MyAssingnments", "DateOfChange");
        }
    }
}
