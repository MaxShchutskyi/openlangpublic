namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLevels1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Levels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LevelsHomeworks",
                c => new
                    {
                        Levels_Id = c.Int(nullable: false),
                        Homework_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Levels_Id, t.Homework_Id })
                .ForeignKey("dbo.Levels", t => t.Levels_Id, cascadeDelete: true)
                .ForeignKey("dbo.Homework", t => t.Homework_Id, cascadeDelete: true)
                .Index(t => t.Levels_Id)
                .Index(t => t.Homework_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LevelsHomeworks", "Homework_Id", "dbo.Homework");
            DropForeignKey("dbo.LevelsHomeworks", "Levels_Id", "dbo.Levels");
            DropIndex("dbo.LevelsHomeworks", new[] { "Homework_Id" });
            DropIndex("dbo.LevelsHomeworks", new[] { "Levels_Id" });
            DropTable("dbo.LevelsHomeworks");
            DropTable("dbo.Levels");
        }
    }
}
