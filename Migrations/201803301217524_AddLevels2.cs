namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLevels2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Chapters", "Level", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Chapters", "Level");
        }
    }
}
