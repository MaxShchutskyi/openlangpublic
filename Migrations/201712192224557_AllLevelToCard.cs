namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AllLevelToCard : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BrainCards", "Level", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BrainCards", "Level");
        }
    }
}
