// <auto-generated />
namespace Xencore.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Updates_25_06_2016 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Updates_25_06_2016));
        
        string IMigrationMetadata.Id
        {
            get { return "201606251343560_Updates_25_06_2016"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
