namespace Xencore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddForeignKey : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Certifications", "CustomUser_Id", "dbo.CustomUsers");
            DropForeignKey("dbo.Educations", "CustomUser_Id", "dbo.CustomUsers");
            DropForeignKey("dbo.WorkExperiences", "CustomUser_Id", "dbo.CustomUsers");
            DropIndex("dbo.Certifications", new[] { "CustomUser_Id" });
            DropIndex("dbo.Educations", new[] { "CustomUser_Id" });
            DropIndex("dbo.WorkExperiences", new[] { "CustomUser_Id" });
            RenameColumn(table: "dbo.Certifications", name: "CustomUser_Id", newName: "CustomUserId");
            RenameColumn(table: "dbo.Educations", name: "CustomUser_Id", newName: "CustomUserId");
            RenameColumn(table: "dbo.WorkExperiences", name: "CustomUser_Id", newName: "CustomUserId");
            AlterColumn("dbo.Certifications", "CustomUserId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Educations", "CustomUserId", c => c.Guid(nullable: false));
            AlterColumn("dbo.WorkExperiences", "CustomUserId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Certifications", "CustomUserId");
            CreateIndex("dbo.Educations", "CustomUserId");
            CreateIndex("dbo.WorkExperiences", "CustomUserId");
            AddForeignKey("dbo.Certifications", "CustomUserId", "dbo.CustomUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Educations", "CustomUserId", "dbo.CustomUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.WorkExperiences", "CustomUserId", "dbo.CustomUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkExperiences", "CustomUserId", "dbo.CustomUsers");
            DropForeignKey("dbo.Educations", "CustomUserId", "dbo.CustomUsers");
            DropForeignKey("dbo.Certifications", "CustomUserId", "dbo.CustomUsers");
            DropIndex("dbo.WorkExperiences", new[] { "CustomUserId" });
            DropIndex("dbo.Educations", new[] { "CustomUserId" });
            DropIndex("dbo.Certifications", new[] { "CustomUserId" });
            AlterColumn("dbo.WorkExperiences", "CustomUserId", c => c.Guid());
            AlterColumn("dbo.Educations", "CustomUserId", c => c.Guid());
            AlterColumn("dbo.Certifications", "CustomUserId", c => c.Guid());
            RenameColumn(table: "dbo.WorkExperiences", name: "CustomUserId", newName: "CustomUser_Id");
            RenameColumn(table: "dbo.Educations", name: "CustomUserId", newName: "CustomUser_Id");
            RenameColumn(table: "dbo.Certifications", name: "CustomUserId", newName: "CustomUser_Id");
            CreateIndex("dbo.WorkExperiences", "CustomUser_Id");
            CreateIndex("dbo.Educations", "CustomUser_Id");
            CreateIndex("dbo.Certifications", "CustomUser_Id");
            AddForeignKey("dbo.WorkExperiences", "CustomUser_Id", "dbo.CustomUsers", "Id");
            AddForeignKey("dbo.Educations", "CustomUser_Id", "dbo.CustomUsers", "Id");
            AddForeignKey("dbo.Certifications", "CustomUser_Id", "dbo.CustomUsers", "Id");
        }
    }
}
