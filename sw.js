﻿'use strict';

self.addEventListener('install', function (event) {
    event.waitUntil(self.skipWaiting());
});

self.addEventListener('push', function (event) {
    var data = event.data.json();
    console.log(data);
    self.registration.showNotification(data.Title, {
        body: data.Message,
        icon: data.Icon,
        url: data.Url
    }).then(function () { });
});

self.addEventListener('notificationclick', function (event) {
    console.log(event)
    //event.notification.close();
    //var url = event.notification.data.url;
    //event.waitUntil(clients.openWindow(url));
});