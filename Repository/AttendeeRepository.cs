﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Domain.Context;
using Domain.Entities.IdentityEntities;

namespace MvcApplicationPostFinanceLatest.Repository
{
    public class AttendeeRepository
    {  
        public List<CustomUser> GetAttendees()
        {
            using (var ctx = new MyDbContext())
            {
                return
                     ctx.Users.Where(x=>x.Active).Include(x => x.PayPalPaymentTransactions.Select(f => f.OrderDetailsId)).OrderByDescending(x=>x.CreationDateTime).ToList();

            }
            //var attendees = context.AttendeesSet.Include(x=>x.Levels).Include(x=>x.Languages).AsEnumerable().OrderByDescending(x => x.CreationDateTime).ToList();
            //return attendees;
        }

        public List<CustomUser> GetAttendees(string param)
        {
            using (var ctx = new MyDbContext())
            {
                var attendees =
                    ctx.Users.Where(
                        x =>
                            (x.Email.Contains(param) || x.Name.Contains(param) ||
                            x.Skype.Contains(param) || x.Ilearn.Contains(param) || x.Ispeak.Contains(param))&& x.Active)
                            .Include(x => x.PayPalPaymentTransactions.Select(f => f.OrderDetailsId))
                        .AsEnumerable()
                        .OrderByDescending(x => x.CreationDateTime).ToList();
                return attendees;
            }
        }
        public async Task<CustomUser> GetAttendeeAllInfo(Guid attendeeId)
        {
            using (var ctx = new MyDbContext())
            {
                return
                await ctx.Users.Where(x => x.Id == attendeeId)
                    .Include(x=>x.Credits.Select(f=>f.Payment))
                    .FirstAsync();
            }
        }
        public CustomUser GetAttendee(Guid attendeeId)
        {
            using (var ctx = new MyDbContext())
            {
                var result = ctx.Users.SingleOrDefault(x => x.Id == attendeeId);
                return result;
            }
        }
        public async Task EditPassword(CustomUser attendee, CustomUserManager manager)
        {
            var user = await manager.FindByIdAsync(attendee.Id);
            if (user != null)
            {
                if (user.PasswordHash == null)
                {
                    var res = await manager.AddPasswordAsync(user.Id, attendee.PasswordHash);
                    return;
                }
                var token = await manager.GeneratePasswordResetTokenAsync(user.Id);
                var update = await manager.ResetPasswordAsync(user.Id, token, attendee.PasswordHash);
            }
        }

        public bool DeleteMultiple(Guid[] ids)
        {
            using (var ctx = new MyDbContext())
            {
                try
                {
                    var users = ctx.Users.Where(x => ids.Contains(x.Id)).ToList();
                    foreach (var id in users)
                    {
                        id.Active = false;
                    }
                    ctx.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}