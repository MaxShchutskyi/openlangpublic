﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Context;
using Domain.Entities;
using NLog;

using Xencore.Entities;

namespace Xencore.Repository
{
    public class FAQRepository
    {
        public IEnumerable<FAQQuestions> GetQuestions()
        {
            using (var contetx = new MyDbContext())
            {
                return contetx.FaqQuestionses.Include(x => x.Commentses).ToList();
            }
        }

        public FAQQuestions GetQuestionById(int id)
        {
            using (var contetx = new MyDbContext())
            {
                return contetx.FaqQuestionses.First(x=>x.Id == id);
            }
        }

        public bool Thumb(int id,bool isHelp)
        {
            using (var contetx = new MyDbContext())
            {
                try
                {
                    var quest = contetx.FaqQuestionses.First(x => x.Id == id);
                    if (isHelp)
                        quest.IsHelpCount += 1;
                    else
                        quest.IsNotHelpCount += 1;
                    contetx.FaqQuestionses.AddOrUpdate(quest);
                    contetx.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    LogManager.GetCurrentClassLogger().Error(e,"Error in Thumb method");
                    return false;
                }

            }
        }
    }
}
