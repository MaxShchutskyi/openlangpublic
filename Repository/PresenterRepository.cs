﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.IdentityEntities;
using Microsoft.AspNet.Identity;
using MvcApplicationPostFinanceLatest.Models;
using NLog;
using Xencore.Entities.IdentityEntities;

namespace MvcApplicationPostFinanceLatest.Repository
{
    public class PresenterRepository
    {
        public IQueryable<CustomUser> GetTeachers
        {
            get
            {
                return Context.Roles.Where(x => x.Name == "Presenter")
                    .SelectMany(x => x.Users)
                    .Join(Context.Users, x => x.UserId, y => y.Id, (x, y) => y);
            }
        }

        //private ClassRepository classRepository = new ClassRepository();
        private readonly MyDbContext Context;

        public PresenterRepository()
        {
            Context = new MyDbContext();
        }

        public async Task<int> GetCountUnscheduledLessons()
        {
            var utcTime = DateTime.UtcNow.AddDays(-7);
            return
                await
                    Context.Schedules.Where(x => x.StartDateTimeDateTime < utcTime && x.PaymentTransactionsId == null)
                        .CountAsync();
        }

        public async Task<bool> ClearOldUnscheduledLessons()
        {
            try
            {
                var utcTime = DateTime.UtcNow.AddDays(-7);
                Context.Schedules.RemoveRange(Context.Schedules.Where(x => x.StartDateTimeDateTime < utcTime && x.PaymentTransactionsId == null).ToList());
                await Context.SaveChangesAsync();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public IEnumerable<SelectListItem> GetPresenters()
        {
            //var presenters = context.PresentersSet.AsEnumerable().Select(x =>
            //    new SelectListItem
            //    {
            //        Value = x.Id.ToString(),
            //        Text = x.PresenterEmail.ToString()
            //    }).ToList();
            //presenters.Remove(presenters.FirstOrDefault(x => x.Value == "0"));
            //return new SelectList(presenters, "Value", "Text");
            return null;
        }
        public async Task<CustomUser> GetPresentersDetails(Guid id)
        {
            return await Context.Users.Where(x => x.Id == id)
                .Include(x => x.Educations).Include(x => x.Certifications)
                .Include(x => x.WorkExperiences).Include(x => x.TeacherInfo).Include(x => x.AdditionalUserInformation).Include(x => x.TeacherRate).FirstOrDefaultAsync();
        }
        public Presenter GetPresenter(int presenterModelId)
        {
            //return context.PresentersSet.SingleOrDefault(x => x.Id == presenterModelId);
            return null;
        }

        public Presenter GetPresenternew(int presenterId) //todo ATTENTION
        {
            //var presenter = context.PresentersSet.AsEnumerable().LastOrDefault(x => x.Id == presenterId);
            //var classes = context.ClassModelsSet.Where(z => z.PresenterModelId == presenter.Id).ToList();
            //if (presenter != null)
            //{
            //    presenter.ClassModels = classes;
            //    return presenter;
            //}
            return null;
        }

        public Presenter GetPresenters(long classId)
        {
            //var classModel = context.ClassModelsSet.SingleOrDefault(x => x.ClassID == classId);
            //var presenter = context.PresentersSet.SingleOrDefault(x => x.Id == classModel.PresenterModelId);
            return null;
        }

        public int GetPresenterId(int classId)
        {
            return 1;
            //return
            //    context.PresentersSet.SingleOrDefault(
            //        x => x.ClassModels.FirstOrDefault(z => z.ClassID == classId).ClassID == classId).Id;
        }

        public List<CustomUser> GetAllPresenters() //todo ATTENTION
        {
            return Context.Users.Include(x => x.TeacherInfo).Where(x => x.TeacherInfo != null).OrderByDescending(x => x.CreationDateTime).ToList();
            //var allPresenters = context.PresentersSet.ToList();
            //allPresenters.Remove(allPresenters.FirstOrDefault(x => x.PresenterId == 0));
            //foreach (var presenter in allPresenters)
            //{
            //    presenter.ClassModels = context.ClassModelsSet.Where(z => z.PresenterModelId == presenter.Id).ToList();
            //}
            //return allPresenters;
        }

        public async Task<bool> ChangeActivete(Guid id, CustomUserManager manag)
        {
            try
            {
                using (var ct = new MyDbContext())
                {
                    var user = await ct.Users.FirstOrDefaultAsync(x => x.Id == id);
                    //if (await manag.IsInRoleAsync(id, "Presenter"))
                    //   await manag.RemoveFromRoleAsync(id, "Presenter");
                    //else
                    //    await manag.AddToRoleAsync(id, "Presenter");
                    user.Active = !user.Active;
                    ct.Configuration.ValidateOnSaveEnabled = false;
                    await ct.SaveChangesAsync();
                    return true;
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                var str = dbEx.EntityValidationErrors.Aggregate("", (current, error) => current + error.ValidationErrors.First().ErrorMessage);
                LogManager.GetCurrentClassLogger().Error(dbEx, str);
                return false;
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error(e, "Change Activity Error");
                return false;
            }
        }

        public void SavePresenter(Presenter presenter)
        {
            //context.PresentersSet.Add(presenter);
            //context.SaveChanges();
        }

        public void EditPresenter(EditTeacherRequestModel presenter)
        {
            //Presenter dbEntry =
            //    context.PresentersSet.SingleOrDefault(x => x.Id == presenter.PresenterId);
            //if (dbEntry != null)
            //{
            //    dbEntry.Name = presenter.Name;
            //    dbEntry.PresenterEmail = presenter.Email;
            //    dbEntry.Password = presenter.Password;
            //    dbEntry.PhoneNumber = presenter.PhoneNumber;
            //    dbEntry.MobileNumber = presenter.MobileNumber;
            //    dbEntry.TimeZone = presenter.TimeZone;
            //    dbEntry.AboutTheTeacher = presenter.AboutTheTeacher;
            //    dbEntry.CanScheduleClass = presenter.CanScheduleClass;
            //    dbEntry.IsActive = presenter.IsActive;
            //    dbEntry.PostFilePath = presenter.PostFilePath;

            //    context.SaveChanges();
            //}
        }

        public Presenter GetPresenterByEmail(string email)
        {
            //return context.PresentersSet.SingleOrDefault(x => x.PresenterEmail == email);
            return null;
        }

        public void Remove(int id)
        {
            //if (id == 1)
            //{
            //    return;
            //}
            //var dbentry = context.PresentersSet.SingleOrDefault(x => x.Id == id);
            //if (dbentry == null) return;
            //context.PresentersSet.Remove(dbentry);
            //context.SaveChanges();
        }

        public List<Presenter> GetAllPresentersAll()
        {
            return null;
            //return context.PresentersSet.ToList();
        }
    }
}