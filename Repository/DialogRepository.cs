﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Domain.Context;
using Domain.Entities;

namespace Xencore.Repository
{
    public class DialogRepository
    {

        private MyDbContext Context { get; set; }

        public DialogRepository(MyDbContext context)
        {
            Context = context;
        }
        public async Task<int> CreateDialogIfNotExistAsync(Guid userOne, Guid userTwo)
        {
            var dialog = Context.Dialogs.FirstOrDefault(
                x =>
                    (x.UserOneId == userOne && x.UserTwoId == userTwo) ||
                    (x.UserOneId == userTwo && x.UserTwoId == userOne));
            if (dialog != null) return dialog.Id;
            dialog = new Dialog() { UserOneId = userOne, UserTwoId = userTwo };
            Context.Dialogs.Add(dialog);
            await Context.SaveChangesAsync();
            return dialog.Id;
        }
    }
}