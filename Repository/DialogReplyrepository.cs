﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Domain.Context;
using Domain.Entities;

namespace Xencore.Repository
{
    public class DialogReplyrepository
    {
        public MyDbContext Context { get;set; }

        public DialogReplyrepository(MyDbContext context)
        {
            Context = context;
        }

        public async Task<int> AddMessageAsync(int dialogId, Guid from, string message)
        {
            var dialogreply = new DialogReply() { UserId = from, DialogId = dialogId, Message = message };
            Context.DialogReplies.Add(dialogreply);
            await Context.SaveChangesAsync();
            return dialogreply.Id;
        }
    }
}