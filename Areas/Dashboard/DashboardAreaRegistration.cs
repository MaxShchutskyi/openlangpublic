﻿using System.Web.Mvc;

namespace Xencore.Areas.Dashboard
{
    public class DashboardAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Dashboard";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            var constrains = new
            {
                //culture = new NullableConstraint()
                culture = "[a-z]{2}"
            };
            context.MapRoute(
                name: "myaccount",
                url: "{culture}/my-account",
                defaults: new { controller = "Main", action = "LoadMain" },
                constraints: constrains,
                namespaces: new[] { "Xencore.Areas.Dashboard.Controllers" }
            );


            context.MapRoute(
                name: "myaccount0",
                url: "my-account",
                defaults: new { culture = "ey", controller = "Main", action = "LoadMain" },
                namespaces: new[] { "Xencore.Areas.Dashboard.Controllers" }
            );
            context.MapRoute(
                name: "beforeActivation2",
                url: "{culture}/before-activation",
                defaults: new { controller = "BeforeActivationTeacherAccount", action = "Index" },
                namespaces: new[] { "Xencore.Areas.Dashboard.Controllers" }
            );
            context.MapRoute(
                name: "beforeActivation",
                url: "before-activation",
                defaults: new { culture = "ey", controller = "BeforeActivationTeacherAccount", action = "Index" },
                namespaces: new[] { "Xencore.Areas.Dashboard.Controllers" }
            );
            context.MapRoute(
                "Dashboard_default2",
                "{culture}/Dashboard/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "Dashboard_default",
                "Dashboard/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional, culture = "ey" }
            );
            context.MapRoute(
                "Dashboard_default3",
                "{culture}/Dashboard/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}