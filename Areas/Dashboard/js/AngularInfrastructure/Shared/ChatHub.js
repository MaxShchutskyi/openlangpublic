﻿navigator.getMedia = (
navigator.getUserMedia ||
navigator.webkitGetUserMedia ||
navigator.mozGetUserMedia ||
navigator.msGetUserMedia
);
var initHub = function (params) {
    $.connection.hub.qs = "partners=" + params.partners;
    var chat = $.connection.chatHub;
    chat.client.getAllUsersInChat = function (users) {
        var usrs = JSON.parse(users);
        console.log(usrs);
        params.getMyActiveUsers(usrs);
    };
    chat.client.sombodyConnected = function (partnerId) {
        params.someClietnChangedState(partnerId, true);
    };
    chat.client.sombodyDisconnected = function (partnerId) {
        params.someClietnChangedState(partnerId, false);
    };
    function sendTo(dialogid, curUser, message, toId) {
        chat.server.sendTo(dialogid, curUser, message, toId);
    }
    function messagesWasRead(ids) {
        chat.server.messageRead(ids);
    }
    function getHub() {
        return chat;
    }
    chat.client.replyMoreMessages = function (result) {
        console.log("replyMoreMessages");
        params.replyMoreMessages(result);
        //var active = $(".border-bot.mess.active").data("partnerid");
        //if (result.length < 10)
        //    finishedMeessages.push({ 'id': active });
        //result.forEach(function(mess) {
        //    var mst = $(messTemplate).clone();
        //    if (mess.UserId.toString() !== active)
        //        mst.removeClass("pull-right second").addClass("first");
        //    smiles.forEach(function(elem) {
        //        mess.Message = mess.Message.replace(elem.sh, "<img src = '" + elem.src + "'/>");
        //    });
        //    if (mess.Message.indexOf("<a href") > -1)
        //        mess.Message = "<img src = '/Images/file.png'/>" + mess.Message;
        //    $(mst)
        //        .attr("data-messageid", mess.Id)
        //        .find(".message-text")
        //        .append(mess.Message)
        //        .end()
        //        .find("img.media-object")
        //        .attr('src', mess.ProfilePicture.replace(/&amp;/g, '&'))
        //        .end()
        //        .find(".chat-date")
        //        .text(moment(mess.UtcDateTime).format("dddd, MMMM, D, YYYY, HH:mm:ss"));
        //    $(".dlg[data-clientid='" + active + "']").prepend(mst);
        //});
        //flagMoreMessages = true;
    };
    chat.client.receiveMessage = function (message, from, dialogReplyId, dialogId) {
        params.receiveMessage(message, from, dialogReplyId, dialogId);
    }
    chat.client.inviteToVideoChatToVideo = function (from) {
        console.log("inviteToVideoChatToVideo");
        clientId = from;
        paramName = 1;
        params.inviteToVideoChatToVideo(from);
        document.getElementById("call").play();
    }
    function getMoreMessages(dialogId, page) {
        //console.log(dialogId + "!!!");
        //console.log(page + "QQQQQ");
        chat.server.getMoreMessages(dialogId, page);
    }
    $.connection.hub.start()
    .done(function () {

    });
    return {
        messagesWasRead: messagesWasRead,
        sendTo: sendTo,
        getHub: getHub,
        getMoreMessages: getMoreMessages
    }
};