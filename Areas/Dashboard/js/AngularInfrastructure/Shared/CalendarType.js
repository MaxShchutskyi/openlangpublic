var Calendar = /** @class */ (function () {
    function Calendar(elementId, url, options) {
        this.listOfShowedWeeks = [];
        this.localTime = new Date().getTimezoneOffset() / 60;
        this.week = null;
        this.curday = moment();
        this.selectCell = null;
        this.previousClick = $(window).width() < 768 ? this.prevClickDat : this.previousClickClick;
        this.nextClick = $(window).width() < 768 ? this.nextDayClickClick : this.nextClickClick;
        this.eventDrop = null;
        this.renderEvent = this.renderEvnt;
        this.deletedEvents = [];
        this.eventClick = this.defaultEventClick;
        this.urlLink = url;
        this.jQueryElem = $(elementId);
        this.elemId = elementId;
        if (options) {
            if (options.hasOwnProperty('eventClick'))
                this.eventClick = options.eventClick;
            if (options.hasOwnProperty('selectCell'))
                this.selectCell = options.selectCell;
            if (options.hasOwnProperty('nextClick'))
                this.nextClick = options.nextClick;
            if (options.hasOwnProperty('previousClick'))
                this.previousClick = options.previousClick;
            if (options.hasOwnProperty('renderEvents'))
                this.renderEvent = options.renderEvents;
            if (options.hasOwnProperty('eventDrop'))
                this.eventDrop = options.eventDrop;
        }
        else { }
        this.initCalendar();
    }
    Calendar.prototype.query = function (result) {
        $.ajax({
            url: this.urlLink, method: 'get', data: { 'teacherId': this.teacherId, 'week': this.week ? this.week : 'null', localtime: this.localTime },
            beforeSend: function (xhr) {
                var token = sessionStorage.getItem('token');
                xhr.setRequestHeader("Authorization", "Bearer " + token);
            }
        })
            .done(function (results) { return result(results); })
            .fail(function (results) { return result(null); });
    };
    Calendar.prototype.renderEvnt = function (evn, elem) {
        //console.log("EVNT!!!!");
        $(elem).attr("role", "tab").attr("data-toggle", "tab").attr("data-target", "#vidgets");
    };
    Calendar.prototype.nextClickClick = function (e) {
        var _this = this;
        $('button.fc-next-button.fc-button.fc-state-default.fc-corner-right').click();
        var prevweek = $(e.currentTarget).siblings('.previous-week');
        prevweek.css('display', 'block');
        var page = parseInt(prevweek.data('curpage'));
        prevweek.data('curpage', ++page);
        var numb = this.week + 1;
        this.week = numb;
        if ($.inArray(numb, this.listOfShowedWeeks) != -1)
            return;
        console.log(this.listOfShowedWeeks);
        this.listOfShowedWeeks.push(numb);
        this.query(function (result) {
            _this.jQueryElem.fullCalendar("addEventSource", result);
        });
    };
    ;
    Calendar.prototype.prevClickDat = function (e) {
        this.curday.add('day', -1);
        $('button.fc-prev-button.fc-button.fc-state-default.fc-corner-left').click();
        //jQuery(".fc-scroller").mCustomScrollbar();
        if (this.curday.format("DDMMYYYY") == moment().format("DDMMYYYY"))
            $(e.currentTarget).css('display', 'none');
    };
    Calendar.prototype.nextDayClickClick = function (e) {
        console.log(this.curday.day());
        if (this.curday.day() != 6) {
            $('button.fc-next-button.fc-button.fc-state-default.fc-corner-right').click();
            this.curday.add('day', 1);
            //jQuery(".fc-scroller").mCustomScrollbar();
            $(e.currentTarget).css('display', 'flex');
            return;
        }
        this.curday.add('day', 1);
        this.nextClickClick(e);
    };
    ;
    Calendar.prototype.previousClickClick = function (e) {
        var _this = this;
        var data = parseInt($(e.currentTarget).data('curpage'));
        if (--data <= 0) {
            $(e.currentTarget).data('curpage', data);
            $(e.currentTarget).css('display', 'none');
        }
        else
            $(e.currentTarget).data('curpage', data);
        $('button.fc-prev-button.fc-button.fc-state-default.fc-corner-left').click();
        var numb = this.week - 1;
        this.week = numb;
        if ($.inArray(numb, this.listOfShowedWeeks) != -1)
            return;
        this.listOfShowedWeeks.push(numb);
        this.query(function (result) {
            _this.jQueryElem.fullCalendar("addEventSource", result);
        });
    };
    ;
    Calendar.prototype.getLegend = function () {
        //var group = this.jQueryElem.attr("data-group");
        var premium = this.jQueryElem.attr("data-premium");
        var basic = this.jQueryElem.attr("data-basic");
        var trial = this.jQueryElem.attr("data-trial");
        var unconfirmed = this.jQueryElem.attr("data-unconfirmed");
        var confirmed = this.jQueryElem.attr("data-confirmed");
        var html = "<div class='legend row'><div class='col-md-2 some-height col-xs-12'>" + confirmed + "<div class='confirmed'></div></div><div class='col-md-2 some-height col-xs-12'>" + unconfirmed + "<div class='unconfirmed'></div></div><div class='col-md-2 some-height col-xs-12'>" + trial + "<div class='trial'></div></div><div class='col-md-2 some-height col-xs-12'>" + basic + "<div class='basic'></div></div><div class='col-md-2 some-height col-xs-12'>" + premium + "<div class='premium'></div></div></div>";
        var elem = $.parseHTML(html);
        return elem;
    };
    Calendar.prototype.update = function (url, id) {
        var _this = this;
        this.urlLink = url;
        this.teacherId = id;
        this.jQueryElem.fullCalendar('removeEvents');
        this.jQueryElem.fullCalendar('today');
        var weeek = $(".fc-week-number span:first-child").text();
        this.week = parseInt(weeek.substr(1));
        this.listOfShowedWeeks = [];
        this.listOfShowedWeeks.push(this.week);
        this.query(function (result) {
            console.log(result);
            _this.jQueryElem.fullCalendar("addEventSource", result);
        });
    };
    Calendar.prototype.render = function (teacherId) {
        var _this = this;
        this.jQueryElem.fullCalendar('removeEvents');
        this.jQueryElem.fullCalendar('gotoDate', moment.now());
        this.teacherId = teacherId;
        setTimeout(function () {
            _this.jQueryElem.fullCalendar('render');
            var weeek = $(".fc-week-number span:first-child").text();
            _this.week = parseInt(weeek.substr(1));
            _this.listOfShowedWeeks = [];
            _this.listOfShowedWeeks.push(_this.week);
            _this.query(function (res) {
                _this.jQueryElem.fullCalendar("addEventSource", res);
            });
        }, 200);
    };
    Calendar.prototype.checkMobileShowingWidget = function () {
        //jQuery(document).ready(function () {
        //    var windowHeight = jQuery(window).height();
        //    var windowWidth = jQuery(window).width();
        //    if (jQuery(window).width() < 992) {
        //        jQuery(".fc-v-event").click(function () {
        //            jQuery(".left-left-content").animate({
        //                right: "-100%", 
        //                height: "700px"
        //            }, 500);
        //            jQuery(".mylessons.selected-lessons").animate({
        //                right: "0"
        //            }, 500);
        //        })
        //        jQuery("body .remove-widget").click(function () {
        //            jQuery(".left-left-content").animate({
        //                right: "0%", 
        //                height: "auto"
        //            }, 500);
        //            jQuery(".mylessons.selected-lessons").animate({
        //                right: "-100%"
        //            }, 500);
        //        })
        //    }
        //})
    };
    Calendar.prototype.defaultEventClick = function (calEvent, jsEvent, view) {
        this.checkMobileShowingWidget();
        if (calEvent.className[0] === 'view1') {
            $('.view1').removeClass('view2');
            this.dateId = calEvent.id;
            $(jsEvent.currentTarget).addClass('view2');
            var dt = moment(calEvent.start._d).format("DD/MM/YYYY HH:mm");
            var time = moment(calEvent.start._d).format("HH:mm");
            $("#start-hours").val(time);
            $('.datetimepicker,.hasMinDate').data("DateTimePicker").date(dt);
        }
        else {
        }
    };
    Calendar.prototype.updateEvents = function (arr) {
        var vk = this.week + 1;
        if ($.grep(this.listOfShowedWeeks, (function (el) {
            console.log(el);
            return el === vk;
        })).length > 0)
            this.jQueryElem.fullCalendar("addEventSource", arr);
    };
    Calendar.prototype.initCalendar = function () {
        var _this = this;
        console.log(this.jQueryElem);
        var locale = this.jQueryElem.attr("data-locale");
        this.jQueryElem.fullCalendar({
            header: {
                left: false,
                center: 'title',
                right: 'prev,next',
            },
            lang: locale,
            eventClick: this.eventClick,
            select: this.selectCell,
            eventRender: this.renderEvent,
            defaultView: $(window).width() < 768 ? 'agendaDay' : 'agendaWeek',
            allDaySlot: false,
            slotDuration: '01:00:00',
            slotLabelFormat: "HH:mm",
            weekNumbers: true,
            firstDay: 0,
            maxTime: '24:00:00',
            timezone: 'local',
            ignoreTimezone: false,
            editable: this.eventDrop,
            eventDurationEditable: false,
            selectable: true,
            eventDrop: this.eventDrop,
        });
        var prevWeek = this.jQueryElem.attr("data-prev-week");
        var nextWeek = this.jQueryElem.attr("data-next-week");
        console.log("INITIALIZED");
        this.jQueryElem.find('.fc-axis.fc-widget-header')
            .append('<span class="local-time">UTC' + this.localTime + '</span>');
        this.jQueryElem.find('.fc-toolbar')
            .append('<span data-curpage="0" class="previous-week"><i class="fa fa-angle-left" aria-hidden="true"></i>' + '<span>' + prevWeek + '</span>' + '</span><span class="next-week">' + '<span>' + nextWeek + '</span>' + '<i class="fa fa-angle-right" aria-hidden="true"></i></span>');
        this.jQueryElem.find('.previous-week')
            .on('click', function (e) { return _this.previousClick(e); });
        this.jQueryElem.find('.next-week')
            .on('click', function (e) { return _this.nextClick(e); });
        //$(document).keydown((e)=> {
        //    switch (e.which) {
        //        case 37: this.previousClick(e);
        //            break;
        //        case 39: this.nextClick(e);
        //            break;
        //        default: return; // exit this handler for other keys
        //    }
        //    e.preventDefault(); // prevent the default action (scroll / move caret)
        //});
        this.jQueryElem.find('.fc-toolbar').after(this.getLegend());
    };
    return Calendar;
}());
//# sourceMappingURL=CalendarType.js.map