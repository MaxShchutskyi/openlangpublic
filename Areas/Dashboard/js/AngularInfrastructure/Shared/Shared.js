﻿var shared = function () {

    //jQuery(window).load(function () {

        //jQuery("#navWrap").css('height', jQuery("#contentWrap").height());

    //})
    //var windowHeight = jQuery(window).height();
    //var windowWidth = jQuery(window).width();
    //if (windowWidth < 768) {
    //    $(window).load(function () {
    //        jQuery(".dataTables_wrapper").width($(window).width());
    //        jQuery(".dataTable").width(jQuery(".dataTables_wrapper").width() * 2);

    //    });
    //    $(window).resize(function () {
    //        jQuery(".dataTables_wrapper").width($(window).width());
    //        jQuery(".dataTable").width(jQuery(".dataTables_wrapper").width() * 2);
    //    });
    //}



        //jQuery(document).ready(function () {
        //    var windowHeight = jQuery(window).height();
        //    var windowWidth = jQuery(window).width();
        //    if (windowWidth < 1200) {
        //        jQuery("body .nav-sidebar > li").click(function () {
        //            if (jQuery(this).children("#collapseExample").length !== 1 && jQuery(this).children(".dropdown").length !== 1) {
        //                if (jQuery(".left-side-bar .sidebar-wrapper").is(":visible")) {
        //                    jQuery(".left-side-bar .sidebar-wrapper").slideToggle()
        //                }
        //            }
        //        })
        //        jQuery("body .page-name-wrap").click(function () {
        //            jQuery(".left-side-bar .sidebar-wrapper").slideToggle()
        //        })
        //        jQuery("body .mobile-btn-nav span").click(function () {
        //            if (jQuery("body .mobile-btn-nav span").hasClass("active")) {
        //                jQuery("body .mobile-btn-nav span").removeClass("active")
        //                jQuery(".left-side-bar .sidebar-wrapper").slideToggle()
        //            } else {
        //                jQuery("body .mobile-btn-nav span").addClass("active")
        //                jQuery(".left-side-bar .sidebar-wrapper").slideToggle()
        //            }
        //        })

        //    }
        //})
    var maxFileSize = 7340032;
    var loadImage = /(jpg|jpeg|BMP|png|image\/x-icon)$/i;
    function validateFileGallery(file) {
        if (!file) return "null";
        if (file.size > maxFileSize) {
            return resource.match('file_size');
        }
        if (!loadImage.exec(file.type)) {
            return resource.match('file_format');
        }
        if (file.name.length>50) {
            return resource.match('one_or_more_img');
        }
        return "OK";
    }
    Array.prototype.firstOrNull = function (condition) {
        if (!condition)
            return null;
        for (var i = 0; i < this.length; i++) {
            if (condition(this[i]))
                return this[i];
        }
    }
    Array.prototype.select = function (condition) {
        if (!condition)
            return null;
        var res = [];
        for (var i = 0; i < this.length; i++) {
            res.push(condition(this[i]));
        }
        return res;
    }
    Array.prototype.removeBy = function (prop, val) {
        var _this = this;
        this.forEach(function (el, index) {
            if (el[prop] === val)
                _this.splice(index, 1);
        });
    }
    Array.prototype.move = function (old_index, new_index) {
        if (new_index >= this.length) {
            var k = new_index - this.length;
            while ((k--) + 1) {
                this.push(undefined);
            }
        }
        this.splice(new_index, 0, this.splice(old_index, 1)[0]);
        return this;
    };
    Array.prototype.where = function (condition) {
        if (!condition)
            return null;
        var res = [];
        for (var i = 0; i < this.length; i++) {
            if (condition(this[i]))
                res.push(this[i]);
        }
        return res;
    }
    function gup(name, url) {
        if (!url) url = location.href;
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(url);
        return results == null ? null : results[1];
    }
    function MobileClick() {
        jQuery("body .dropdown-lessons .tabs-head  a.accordion-toggle").click(function () {
            if (jQuery(this).hasClass("active")) {
                jQuery(this).removeClass("active")
                jQuery(this).children("span").children("a").children("i").removeClass("fa-check-circle-o").addClass("fa-circle-o");
            } else {
                jQuery(this).addClass("active")
                jQuery(this).children("span").children("a").children("i").removeClass("fa-circle-o").addClass("fa-check-circle-o");
            }
        })
    }
    function loadVideoFile(data, url, progress, callback) {
        $.ajax({
            type: "POST",
            url: url,
            contentType: false,
            dataType: 'json',
            processData: false,
            data: data,
            xhr: function () {
                var xhr = $.ajaxSettings.xhr();
                xhr.upload.addEventListener('progress', function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = Math.ceil(evt.loaded / evt.total * 100);
                        progress.val(percentComplete).text('Loaded ' + percentComplete + '%');
                    }
                }, false);
                return xhr;
            },
            success: callback,
            error: callback,
        });
    }
    function validateFiles(files) {
        for (var i = 0; i < files.length; i++) {
            var res = validateFileChat(files[i]);
            if (res !== "OK") {
                return res;
            }
        }
        return "OK";
    }
    function validateFileChat(file) {
        var maxFileSize = 7340032;
        var loadImage = /(jpg|jpeg|BMP|png|xls|doc|gif|xlsx|html|pdf|mp4|image\/x-icon)$/i;
            if (!file) return "null";
            if (file.size > maxFileSize) {
                return resource.match('file_size');
            }
            if (!loadImage.exec(file.type)) {
                return resource.match('file_format');
            }
            if (file.name.length > 50) {
                return resource.match('one_or_more_img');
            }
            return "OK";
    }

    function validateImages(images) {
        for (var i = 0; i < images.length; i++) {
            var res = validateFileGallery(images[i]);
            if (res !== "OK") {
                return res;
            }
        }
        return "OK";
    }
    function getTableProperties(elem) {
        var nextpage = elem.attr("data-next-page");
        var prevpage = elem.attr("data-prev-page");
        var showing = elem.attr("data-info");
        var noData = elem.attr("data-nodata");
        var empty = elem.attr("data-empty")
        return {
            "language": {
                "info": showing,
                "emptyTable": noData,
                "infoEmpty": empty,
                "paginate": {
                    "previous": prevpage,
                    "next": nextpage,
                }
            },
            "bLengthChange": false,
            "bFilter": false,
            "initComplete": function (e, t) {
                if (jQuery(window).width() < 768) {
                    jQuery(".dataTables_wrapper").width($(window).width() - 40);
                    jQuery(".dataTable").width(jQuery(".dataTables_wrapper").width() * 2);
                    jQuery(".students-enquiries .dataTables_wrapper .panel-default>.panel-heading+.panel-collapse>.panel-body").width(jQuery(".dataTables_wrapper").width() * 2);
                    $(window).resize(function () {
                        jQuery(".dataTables_wrapper").width($(window).width() - 40);
                        jQuery(".dataTable").width(jQuery(".dataTables_wrapper").width() * 2);
                        jQuery(".students-enquiries .dataTables_wrapper .panel-default>.panel-heading+.panel-collapse>.panel-body").width(jQuery(".dataTables_wrapper").width() * 2);
                    });
                }
            }
        }
    }
    function clearCookie(cookie) {
        $.cookie(cookie, null, { path: '/' });
        $.removeCookie(cookie, { path: '/' });
        document.cookie = cookie + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }

   
 
    if(!$.cookie("timezone")){
        $.cookie("timezone", new Date().getTimezoneOffset() * -1,{expires : 10,path:'/'} );
    }
    navigator.browser = (function () {
        var ua = navigator.userAgent, tem,
        M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE ' + (tem[1] || '');
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
            if (tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
        return M.join(' ');
    })();
    return {
        gup: gup,
        loadVideoFile: loadVideoFile,
        validateImages: validateImages,
        validateFiles: validateFiles,
        getTableProperties: getTableProperties,
        clearCookie: clearCookie
    }
}();