﻿levelcontroller.$inject = ["$scope", "$timeout", "$location", "api", "$anchorScroll"];
function levelcontroller($scope, $timeout, $location, api, $anchorScroll) {
    console.log("levelcontroller")
    $scope.shared.currentPath = $location.path();
    $anchorScroll();
    $scope.goTo = function (path) {
        $location.path(path);
    }
    $(".self-sudy-directive").animate({
        marginTop: "0",
    }, 300);
    api.ajax("/api/level/Get", "get", null, function (res) {
        $scope.levels = res;
        $scope.$digest();
    })
    jQuery('.page-preloader-wrap-dashboard').hide();
};

