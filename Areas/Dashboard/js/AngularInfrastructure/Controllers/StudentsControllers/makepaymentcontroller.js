﻿makepaymentcontroller.$inject = ["$scope", "api", "$http", "notifications", "$timeout", '$filter'];
function makepaymentcontroller($scope, api, $http, notifications, $timeout, $filter) {
    $scope.paymentConfig = {};
    $scope.succeeded = true;
    $scope.paymentConfig.validMonth = 1;
    checkPaymentParams($scope);
    jQuery("#creditform").validate({
        errorPlacement: function errorPlacement() { },
        highlight: function highlight(element) {
            $(element).addClass("error").next().find(".select2-selection").addClass("error");
        },
        unhighlight: function unhighlight(element) {
            $(element).removeClass("error").next().find(".select2-selection").removeClass("error");
        }
    });
    function checkPaymentParams(scope) {
        var paymentparams = $.cookie('paymentparams');
        if (paymentparams) {
            var params = JSON.parse(paymentparams);
            scope.paymentConfig.type = params.type;
            scope.paymentConfig.member = params.member;
            scope.paymentConfig.countLessons = params.type == 'basic' ? 45 : 60;
            scope.paymentConfig.key = params.key;
            setTimeout(function () {
                //console.log("a[data-type='" + $scope.paymentConfig.type + "'][data-member='" + $scope.paymentConfig.member + "'][data-key='" + $scope.paymentConfig.key + "']");
                $("#payments_lessons a[data-key='" + $scope.paymentConfig.key + "']").click();
                shared.clearCookie("paymentparams");
            }, 200);
            //shared.clearCookie("paymentparams");
        }
        else if (!scope.paymentConfig.type || !scope.paymentConfig.member) {
            scope.paymentConfig.type = 'basic';
            scope.paymentConfig.member = 'membership';
            scope.paymentConfig.countLessons = 45;
        }
    }
    $scope.selectedPackage = function (key, value, index, event) {
        $scope.paymentConfig.state = index;
        $scope.paymentConfig.countLess = key;
        $scope.paymentConfig.price = value;
        $(event.currentTarget).parents("tr").addClass('active').siblings().removeClass('active');
        //$(event.currentTarget).children("i").removeClass('fa-circle-o').addClass('fa-check-circle-o').parents(".tabs-head").siblings().find(".choice i").removeClass('fa-check-circle-o').addClass('fa-circle-o');
        //$scope.nextStep("#inputBalance", "#replenish-balance-step-2");
        //$("#inputBalance").modal("hide");
        //$("#replenish-balance-step-2").modal("show");
    };
    $scope.selectPackage = function (pk) {
        //if (pk == 'membership' && $scope.shared.credits.firstOrNull(x=>x.typeOfPrice == "Membership" && x.subscription.Active)) {
        //    notifications.show('warning', "You can not have more than one subscription");
        //    return;
        //}
        $location.path('/makepayment');
        //$scope.paymentConfig.member = pk;
        //if (!$scope.paymentConfig.type) $scope.paymentConfig.type = 'basic';
    };
    $scope.getNextDate = function () {
        return moment().add($scope.paymentConfig.validMonth, 'month').format("DD.MM.YYYY");
    }
    $scope.changeType = function (newVal) {
        $scope.paymentConfig.type = newVal;
        console.log($scope.paymentConfig.type);
        if (newVal == 'basic') $scope.paymentConfig.countLessons = 45; else $scope.paymentConfig.countLessons = 60;
        clearSettings();
    }
    $scope.changeMember = function (newVal) {
        $scope.paymentConfig.member = newVal;
        if (newVal == 'membership') $scope.paymentConfig.validMonth = 1; else $scope.paymentConfig.validMonth = 6;
        clearSettings();
    }
    $scope.continue = function (finish) {
        if (finish) {
            jQuery(".payment-window").hide(300);
            var check = "false";
            if (finish === "tr") {
                jQuery(".succsess-window").show(300);
            } else {
                jQuery(".unsuccsess-window").show(300);
            }
            return;
        }
        if (jQuery(".tabs-wrapper ul li.first-step-tab").hasClass("active") && $scope.paymentConfig.price) {
            jQuery(".tabs-wrapper ul li.first-step-tab").removeClass("active")
            jQuery(".tabs-wrapper ul li.second-step-tab").addClass("active");
            jQuery(".first-step-wrap").hide(300);
            jQuery(".second-step-wrap").show(300);
            //jQuery(".navigation-wrap .continue").html('Confirm My Order <i class="fa fa-long-arrow-right" aria-hidden="true"></i>');
            //jQuery(".navigation-wrap .cancel").removeAttr("href").addClass("backTo").html('<i class="fa fa-long-arrow-left" aria-hidden="true"></i> Return')
        } else if (jQuery(".tabs-wrapper ul li.second-step-tab").hasClass("active") && (($scope.discount) || !$scope.replenish ||(!$scope.replenish.additionalBonus && !$scope.discount))) {
            jQuery(".tabs-wrapper ul li.second-step-tab").removeClass("active")
            jQuery(".tabs-wrapper ul li.three-step-tab").addClass("active");
            jQuery(".second-step-wrap").hide(300);
            jQuery(".three-step-wrap").show(300);
            //jQuery(".navigation-wrap .continue").html('Pay' + '$269' + 'For a New Package' + '<i class="fa fa-long-arrow-right" aria-hidden="true"></i>');
            jQuery(".navigation-wrap .cancel").removeAttr("href").addClass("backTo").html('<i class="fa fa-long-arrow-left" aria-hidden="true"></i> Return')
        } else if (jQuery(".tabs-wrapper ul li.three-step-tab").hasClass("active")) {
            if (($scope.selectedMethod.setectedType == 'credit' && !$("#creditform").valid()))
                return;
            $scope.confirmPayment();
            //jQuery(".payment-window").hide(300);
            //var check = "false";
            //if (check = "true") {
            //    jQuery(".unsuccsess-window").show(300);
            //} else {
            //    jQuery(".unsuccsess-window").show(300);
            //}
        }
    }
    function clearSettings() {
        $("#payments_lessons tr").removeClass("active");

        $scope.paymentConfig.state = null;
        $scope.paymentConfig.price = null;
    }
    //$(document).ready(function () {

    //});

    //$scope.$watch('paymentConfig.type', function (newVal, oldVal) {
    //    if (!newVal || newVal == oldVal) return;
    //    if (newVal == 'basic') $scope.paymentConfig.countLessons = 45; else $scope.paymentConfig.countLessons = 60;
    //});
    //$scope.$watch('paymentConfig.member', function (newVal, oldVal) {
    //    if (!newVal || newVal == oldVal) return;
    //    if (newVal == 'membership') $scope.paymentConfig.validMonth = 1; else $scope.paymentConfig.validMonth = 6;
    //});
    $scope.selectedMethod = { setectedType: "credit" };
    $scope.membership = true;
    $scope.checkLessons = function (countLessons, duration, money, eurManey, typeOfLessons) {
        $scope.selectedMethod.countLessons = countLessons;
        $scope.selectedMethod.duration = duration;
        $scope.selectedMethod.money = money.toFixed(2);
        $scope.selectedMethod.eurMoney = eurManey;
        $scope.selectedMethod.typeOfLessons = typeOfLessons;
        $scope.selectedMethod.timeOfLesson = typeOfLessons == "Premium" ? "60" : "45";
        $scope.selectedMethod.typeOfPrice = duration == 1 ? "Membership" : "Package";
    };
    $scope.nextStep = function (prev, next) {
        $(prev).modal('hide');
        $(next).modal('show');
    };
    $scope.activeOrNot = function () {
        if (!$scope.shared.credits) return false;
        return $scope.shared.credits.firstOrNull(function (x) {
            return x.typeOfPrice == 'Membership' && x.subscription.Active;
        });
    };
    $scope.unsubscribe = function () {
        $scope.selectedUnsubscribe = $scope.shared.credits.firstOrNull(function (x) {
            return x.typeOfPrice == "Membership" && x.subscription.Active;
        });
        $("#replenish-balance-step-4").modal('show');
    };
    $scope.unsubscribeConfirm = function () {
        var credit = $scope.shared.credits.firstOrNull(function (x) {
            return x.typeOfPrice == "Membership" && x.subscription.Active;
        });
        if (!credit || $scope.shared.isButtonLoading) return;
        //notifications.show('warning', "Please wait a result");
        $scope.shared.isButtonLoading = true;
        api.ajax('/api/Payment/' + credit.subscription.TypeProvider + "_Unsubscribe", 'post', JSON.stringify(credit.subscription), function (res) {
            if (res) credit.subscription.Active = false;
            //notifications.show('success', "Saved");
            $("#replenish-balance-step-4").modal('hide');
            $scope.shared.isButtonLoading = false;
            $scope.shared.updateCredits();
        });
    };
    $scope.confirmPayment = function () {
        if ($scope.shared.isButtonLoading) return;
        $scope.unsubscribeConfirm();
        $scope.shared.isButtonLoading = true;
        $scope.selectedMethod.countLessons = $scope.paymentConfig.countLess;
        $scope.selectedMethod.currency = $scope.shared.globalCurrency;
        $scope.selectedMethod.duration = $scope.paymentConfig.validMonth;
        $scope.selectedMethod.money = ($scope.paymentConfig.price * $scope.shared.globalDelta).toFixed(2);
        $scope.selectedMethod.eurMoney = $scope.paymentConfig.price;
        $scope.selectedMethod.typeOfLessons = $scope.paymentConfig.type.toUpperCase();
        $scope.selectedMethod.timeOfLesson = $scope.paymentConfig.type.toUpperCase() == "PREMIUM" ? "60" : "45";
        //$scope.selectedMethod.typeOfPrice = $scope.paymentConfig.validMonth == 1 ? "Membership" : "Package";
        $scope.selectedMethod.typeOfPrice = "Package";
        if ($scope.discount) {
            $scope.selectedMethod.discount = $scope.discount.Id;
            $scope.selectedMethod.bonus = $scope.replenish.bonus;
            $scope.selectedMethod.eurBonus = $scope.replenish.eurBonus;
        }
        //$("#replenish-balance-step-3").modal('hide');
        if ($scope.selectedMethod.setectedType == 'credit') {
            var date = $('.form-wrap .input-wrap .data').data('date');
            $scope.selectedMethod.month = date.split('/')[0];
            $scope.selectedMethod.year = date.split('/')[1];
            api.ajax('/api/Payment/CreditCardPayment', 'post', JSON.stringify($scope.selectedMethod), function (res) {
                console.log(res);
                $scope.shared.isButtonLoading = false;
                if (res.hasOwnProperty('error')) {
                    $scope.succeeded = false;
                    //$("#replenish-balance-step-7").modal('show');
                    console.log(res);
                    $scope.$digest();
                    $scope.continue('err');
                    return;
                }
                $scope.shared.updateCredits();
                $scope.selectedMethod = { setectedType: "credit" };
                $scope.paymentConfig = { validMonth: 1 };
                console.log(res);
                $scope.continue('tr');
                //$("#replenish-balance-step-7").modal('show');
                $scope.$digest();
            });
        }
        if ($scope.selectedMethod.setectedType == 'paypal') {
            api.ajax('/api/Payment/PayPalPayment', 'post', JSON.stringify($scope.selectedMethod), function (res) {
                location.href = res.path;
            });
        }
    };
    $scope.createReplenish = function () {
        if ($scope.selectedMethod.setectedType == 'credit' && !$("#creditform").valid()) return;
        if ($scope.shared.isButtonLoading) return;
        //$("#replenish-balance-step-2").modal('hide');
        $scope.selectedMethod.currency = $scope.shared.globalCurrency;
        $scope.nextStep("#replenish-balance-step-2", "#replenish-balance-step-3");
    };
    $(".info-tooltip").tooltip();
    $scope.getDate = function (date) {
        var dt = moment.utc(date, "DD-MM-YYYY").toDate();
        return moment(dt).local().format('DD.MM.YYYY');
    };
    $scope.getDate2 = function (date) {
        var dt = moment.utc(date, "DD-MM-YYYY HH:mm").toDate();
        return moment(dt).local().format('DD.MM.YYYY HH:mm');
    };
    if ($.cookie("showWelcome")) {
        var bal = JSON.parse($.cookie("showWelcome"));
        $scope.dtt = $scope.getDate2(bal.date);
        $("#replenish-balance-step-5").modal('show');
        $.cookie("showWelcome", null, { path: '/' });
        $.removeCookie('showWelcome', { path: '/' });
        document.cookie = "showWelcome" + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        $scope.$digest();
    }
    if ($.cookie("showstep3")) {
        //console.log($.cookie("replenish-balance-step-7"));
        //var bal = JSON.parse($.cookie("replenish-balance-step-7"));
        //$scope.dtt = $scope.getDate2(bal.date);
        $("#replenish-balance-step-7").modal('show');
        shared.clearCookie("showstep3");
        $scope.$digest();
    }
    $scope.$watch('replenish.additionalBonus', function (newVal, oldVal) {
        if (newVal == oldVal) return;
        api.ajax("/api/Discounts/CheckExistsDiscount", "get", { discount: newVal, type: $scope.paymentConfig.member }, function (res) {
            $scope.discount = res;
            console.log(res);
            $scope.replenish.discount = $scope.discount ? $scope.discount.Id : null;
            $scope.getDiscountIfExist(parseFloat($scope.paymentConfig.price));
            $scope.$digest();
        });
        //$scope.replenish.eurbalance = ($scope.replenish.replenishacount / $scope.shared.globalDelta).toFixed(2);
    });
    $scope.getDiscountIfExist = function (floatVal) {
        if (!$scope.discount) return;
        if ($scope.discount.IsPercent) $scope.replenish.eurBonus = floatVal * parseFloat($scope.discount.Value) / 100; else $scope.replenish.eurBonus = parseFloat($scope.discount.Value);
        $scope.replenish.bonus = ($scope.replenish.eurBonus * $scope.shared.globalDelta).toFixed(2);
    };
    jQuery('.page-preloader-wrap-dashboard').hide();
}