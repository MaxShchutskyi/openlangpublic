﻿mylessonsstudentcontroller.$inject = ["$scope", "api", "notifications", "$timeout"];
function mylessonsstudentcontroller($scope, api, notifications, $timeout) {


    setInterval(function () {
        $(".inLocalTimezones").text(moment().format("HH:mm:ss"));
    }, 1000);
    var event;
    var cal = $("#mylessonscalendar");
    if (!$scope.shared.ilearn) {
        api.ajax('/api/Languages/GetILearn', 'get', null, function (res) {
            $scope.shared.ilearn = res;
            $scope.shared.teachersTeach = $scope.shared.ilearn;
            $scope.$apply();
        });
    }
    if (!$scope.shared.shortTeachersList) {
        api.ajax("/api/Schedule/GetShortListTeachers", 'get', null, function (res) {
            $scope.shared.shortTeachersList = res;
            $scope.shared.own = true;
            $scope.$apply();
        });
    }
    $scope.$watch('shared.selectedLesson.type', function (newVal, oldVal) {
        if (newVal === oldVal || !newVal) return;
        //console.log(newVal + " " + oldVal);
        var credid = $scope.shared.credits.firstOrNull(function (x) { return x.type == newVal });
        if (!credid) {
            notifications.show('error', resource.match('you_cannot_book_this_type'));
            $timeout(function () {
                $scope.shared.selectedLesson.type = oldVal;
            });
        }
        else
            $scope.selectedCredit = credid;
    });
    $scope.shared.selectedTeacherFromList = 'own';
    $scope.$watch("shared.selectedTeacherFromList", function (newVal, oldVal) {
        if (newVal === oldVal || oldVal === undefined || newVal === undefined)
            return;
        if (newVal === 'own') {
            $scope.shared.own = true;
            var path = '/api/Schedule/GetStudentScheduler';
            cal.find('.previous-week').css('display', 'inline');
            calendar.update(path, $scope.shared.userId);
            
        }
        else {
            var path = "/api/Schedule/GetTeacherCalendar";
            cal.find('.previous-week').css('display', 'none');
            $scope.shared.own = false;
            $scope.shared.teachersTeach = $scope.shared.shortTeachersList.firstOrNull(function (elem) { return elem.Id === newVal }).teach;
            calendar.update(path, newVal);
        }
        $scope.shared.selectedLesson = {};
    });
    var newLang = $("#lessonlang");
    var newLevel = $("#levellesson");
    newLevel.select2().prop("disabled", true);
    newLang.select2().prop("disabled", true);
    var calendar = new Calendar("#mylessonscalendar", "/api/Schedule/GetStudentScheduler", { previousClick: calendarHelper.prevClick, eventClick: clickToTvent });
    calendarHelper.setCalendar(calendar);
    function clickToTvent(calEvent, jsEvent, view) {
        calendar.checkMobileShowingWidget();
        if (event && event.className[event.className.length - 1] === 'selected') {
            event.className.splice(-1, 1);
            cal.fullCalendar('updateEvent', event)
            if (event.id === calEvent.id) {
                $scope.shared.selectedLesson.isTrue = false;
                $scope.$apply();
                return;
            }
        }
        calEvent.className.push('selected');
        event = calEvent;
        //console.log($scope.shared.money - $scope.shared.currentPrices[$scope.shared.selectedLesson.type]);
        cal.fullCalendar('updateEvent', event)
        $scope.shared.selectedLesson.id = calEvent.id;
        $scope.shared.selectedLesson.date = calEvent._start.format("ll")
        $scope.shared.selectedLesson.time = calEvent._start.format("HH:mm");
        $scope.shared.selectedLesson.datetime = calEvent._start.toISOString();
        $scope.shared.selectedLesson.isConfirmed = calEvent.IsConfirmed;
        $scope.shared.selectedLesson.type = calEvent.type;
        $scope.shared.selectedLesson.language = calEvent.language ? calEvent.language : $scope.shared.ilearn[0].key;
        $scope.shared.selectedLesson.level = calEvent.level ? calEvent.level : "Beginner";
        $scope.shared.selectedLesson.topic = calEvent.topic;
        $scope.shared.selectedLesson.teacher = calEvent.teacher;
        $scope.shared.selectedLesson.students = calEvent.students;
        $scope.shared.selectedLesson.findStudent = false;
        $scope.shared.selectedLesson.isTrue = true;
        if ($scope.shared.selectedLesson.students && $scope.shared.selectedLesson.students.firstOrNull(function (elem) { return elem.Id === $scope.shared.userId }))
            $scope.shared.disableSaving = true;
        else
            $scope.shared.disableSaving = false;
        if ($scope.shared.selectedLesson.type) {
            newLevel.select2().prop("disabled", true);
            newLang.select2().prop("disabled", true);
        }
        else {
            newLevel.select2().prop("disabled", false);
            newLang.select2().prop("disabled", false);
        }
        console.log($scope.shared.selectedLesson.teacher);
        $scope.$apply();
    }
    $scope.shared.bindToLesson = function (evt) {
        //if (evt.currentTarget.attributes.disabled) {
        //    notifications.show('error', "You can't complete this operation. Please check your cash balance");
        //    return;
        //}
        //if (moment.utc(new Date()).add(1, 'hours') > moment($scope.shared.selectedLesson.date)) {
        //    notifications.show('error', "You can't to bind to lessons in the past time");
        //    return;
        //}
        //if ($scope.shared.isButtonLoading)
        //    return;
        //$scope.shared.isButtonLoading = true;
        //api.ajax('/api/Schedule/BindToGroupLesson', 'post', JSON.stringify({ id: $scope.shared.selectedLesson.id, type: $scope.shared.selectedLesson.type, teacherInfo: { id: $scope.shared.selectedLesson.teacher.Id, name: $scope.shared.selectedLesson.teacher.Name } }), function (res) {
        //    //event.status += " " + $scope.shared.selectedLesson.type;
        //    event.students.push({ Id: $scope.shared.userId, ProfilePicture: $scope.shared.user.profilePicture, Name: $scope.shared.user.name });
        //    cal.fullCalendar('updateEvent', event);
        //    $scope.shared.selectedLesson = {};
        //    $scope.shared.isButtonLoading = false;
        //    $scope.$apply();
        //    notifications.show('success', 'Saved');
        //});
    }
    //console.log(new Date(moment.utc().add(1,'hours').format()));
    $scope.shared.bookingLesson = function (evt) {
        if (!$scope.shared.selectedLesson.type) {
            notifications.show('error', resource.match('select_lesson'));
            return;
        }
        //if (evt.currentTarget.attributes.disabled) {
        //    notifications.show('error', "You can't complete this operation. Please check your cash balance");
        //    return;
        //}
        if ($scope.shared.selectedLesson.type === "Group" && moment.utc(new Date()).add(7, 'days') > moment($scope.shared.selectedLesson.date)) {
            notifications.show('warning', resource.match('group_lessons_can'));
            return;
        }
        //console.log(moment.utc($scope.shared.selectedLesson.datetime).format());
        if (moment.utc(new Date()).add(8, 'hours') > moment.utc($scope.shared.selectedLesson.datetime)) {
            notifications.show('error', resource.match('you_can_not_book'));
            return;
        }
        if ($scope.shared.selectedLesson.type === "Group" && !$("#topics").valid()) {
            notifications.show('error', resource.match('you_should_to_fill'));
            return;
        }
        if ($scope.shared.isButtonLoading)
            return;
        $scope.shared.isButtonLoading = true;
        api.ajax('/api/Schedule/BindToLesson', 'post', JSON.stringify({ id: $scope.shared.selectedLesson.id, creditId: $scope.selectedCredit.id, type: $scope.shared.selectedLesson.type, language: $scope.shared.selectedLesson.language, level: $scope.shared.selectedLesson.level, teacherInfo: { id: $scope.shared.selectedLesson.teacher.Id, name: $scope.shared.selectedLesson.teacher.Name }, topic: $scope.shared.selectedLesson.topic }), function (res) {
            event.className.pop();
            event.className.push('unconfirmed');
            event.students.push({ Id: $scope.shared.userId, ProfilePicture: $scope.shared.user.profilePicture, Name: $scope.shared.user.name });
            event.type = $scope.shared.selectedLesson.type;
            event.language = $scope.shared.selectedLesson.language;
            event.level = $scope.shared.selectedLesson.level;
            event.topic = $scope.shared.selectedLesson.topic;
            cal.fullCalendar('updateEvent', event);
            $scope.shared.selectedLesson = {};
            $scope.shared.isButtonLoading = false
            notifications.show('success', 'Saved');
            updateCredits();
        });
    }
    function updateCredits() {
        api.ajax('/api/Account/GetCurrentGetAllCreditsPrices', 'get', null, function (res) {
            $scope.shared.credits = res;
            $scope.$apply();
        })
    }
    $scope.shared.classNotHappen = function() {
      console.log("classNotHappen");
      if (!$scope.shared.selectedLesson.problem) {
        alert("Please type a problem");
        return;
      }
      $scope.shared.isButtonLoading = true;
      $("#problem_modal").modal('hide');
      api.ajax('/api/SchedulerOfStudents/ClassNotHappen',
        'post',
        JSON.stringify($scope.shared.selectedLesson),
        function(res) {
          console.log(res);
          if (res.Error)
            notifications.show('error', res.Error);
          else
            notifications.show('success',"Success");
          $scope.shared.selectedLesson.problem = "";
        });
    }
    $scope.$watch('shared.selectedLesson.language', function (newVal, oldVal) {
        if (newVal === oldVal || newVal === null) return;
        newLang.select2().val(newVal);
    });
    $scope.$watch('shared.selectedLesson.level', function (newVal, oldVal) {
        if (newVal === oldVal || newVal === null) return;
        newLevel.select2().val(newVal);
    });
    calendar.render($scope.shared.userId);
    jQuery('.page-preloader-wrap-dashboard').hide();
}