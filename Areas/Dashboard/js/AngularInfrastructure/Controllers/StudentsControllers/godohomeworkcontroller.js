﻿godohomeworkcontroller.$inject = ["$scope", "api", "$http", "notifications", "$timeout", '$filter', '$routeParams','$location'];
function godohomeworkcontroller($scope, api, $http, notifications, $timeout, $filter, $routeParams, $location) {
  var id = $routeParams.id;
  var recognizer;
  if (!id)
    return;
  $scope.currentCardsNumber = 1;
  $scope.data = { answer: null };
  api.ajax('/api/HomeworkStudent/GetDoHomework?id=' + id, 'get', null, function (res) {
    res.forEach(function(x) {
      x.Type = x.Type.toString();
    });
    $scope.cards = res;
    $scope.card = res[0];
    checkType();
    $scope.$digest();
  });
  function checkType() {
    if ($scope.card.Type === '0') {
      var lng;
      switch ($scope.card.Language) {
      case 'us':
        lng = "en-US";
        break;
      case 'ae':
        lng = "ar-EG";
        break;
      case 'cn':
        lng = "zh-CN";
        break;
      case 'fr':
        lng = "fr-FR";
        break;
      case 'de':
        lng = "de-DE";
        break;
      case 'it':
        lng = "it-IT";
        break;
      case 'br':
        lng = "pt-BR";
        break;
      case 'ru':
        lng = "ru-RU";
        break;
      case 'es':
        lng = "es-ES";
        break;
      default:
        lng = "en-US";
      }
      recognizer = recognizerSetup(window.SDK, "Interactive", lng, window.SDK.SpeechResultFormat["Simple"], "7d42ba63a3614726aae549a4f5b1232a");
    }
  }
    
  $scope.reply = function () {
    console.log($scope.data.answer);
    if (!$scope.data.answer) {
      $scope.data.answer = "Test";
    }
    switch ($scope.card.Type) {//
    case "1":
    case "3":
      {
          var crd = $scope.card.Answers.find(function (x) { return x.Id === $scope.data.answer });
        $scope.card.isStudentRight = crd.IsRight;
      }
      break;
    case "0":
    case "2":
    case "4":
        {
          $scope.data.answer = "Test";
        if ($scope.data.answer.trim() === $scope.card.Answers[0].Variant.trim())
          $scope.card.isStudentRight = true;
        else
          $scope.card.isStudentRight = false;
      }
      break;
    }
    //console.log($scope.cards.length - $scope.currentCardsNumbe);
    if ($scope.cards.length - $scope.currentCardsNumber === 0) {
      var model = { answers: [], homeworkId: id };
      $scope.cards.forEach(function(card) {
        model.answers.push({ cardId: card.Id, isRightAnswer: card.isStudentRight});
      });
      console.log(model);
      api.ajax('/api/homeworkstudent/FinishHomework',
        'post',
        JSON.stringify(model),
        function(res) {
          $location.path('/studenthomework');
          $scope.$digest();
        });
    } else {
      ++$scope.currentCardsNumber;
      $scope.card = $scope.cards[$scope.currentCardsNumber - 1];
      $scope.data.answer = null;
      checkType(); 
    }

  }
  $scope.RecognizerStart = function () {
    recognizer.Recognize((event) => {
        /*
         Alternative syntax for typescript devs.
         if (event instanceof SDK.RecognitionTriggeredEvent)
        */
        switch (event.Name) {
        case "SpeechSimplePhraseEvent":
          updateRecognizedPhrase(event.Result);
          break;
        case "SpeechDetailedPhraseEvent":
          updateRecognizedPhrase(JSON.stringify(event.Result, null, 3));
          break;
        default:
          console.log(JSON.stringify(event)); // Debug information
        }
      })
      .On(() => {
          // The request succeeded. Nothing to do here.
        },
        (error) => {
          console.error(error);
        });
  }
  function updateRecognizedPhrase(json) {
    console.log(json);
    $scope.data.answer = json.DisplayText;
    $scope.$digest();
  }
  function Initialize(onComplete) {
    if (!!window.SDK) {
      onComplete(window.SDK);
    } else
      alert("Speach SDK is not completed");
  }
  Initialize(function () {
    recognizer = recognizerSetup(window.SDK, "Interactive", "en-US", window.SDK.SpeechResultFormat["Simple"], "7d42ba63a3614726aae549a4f5b1232a");
  });
  function recognizerSetup(SDK, recognitionMode, language, format, subscriptionKey) {

    switch (recognitionMode) {
      case "Interactive":
        recognitionMode = SDK.RecognitionMode.Interactive;
        break;
      case "Conversation":
        recognitionMode = SDK.RecognitionMode.Conversation;
        break;
      case "Dictation":
        recognitionMode = SDK.RecognitionMode.Dictation;
        break;
      default:
        recognitionMode = SDK.RecognitionMode.Interactive;
    }

    var recognizerConfig = new SDK.RecognizerConfig(
      new SDK.SpeechConfig(
        new SDK.Context(
          new SDK.OS(navigator.userAgent, "Browser", null),
          new SDK.Device("SpeechSample", "SpeechSample", "1.0.00000"))),
      recognitionMode,
      language, // Supported languages are specific to each recognition mode. Refer to docs.
      format); // SDK.SpeechResultFormat.Simple (Options - Simple/Detailed)


    var useTokenAuth = false;

    var authentication = function () {
      if (!useTokenAuth)
        return new SDK.CognitiveSubscriptionKeyAuthentication(subscriptionKey);

      var callback = function () {
        var tokenDeferral = new SDK.Deferred();
        try {
          var xhr = new (XMLHttpRequest || ActiveXObject)('MSXML2.XMLHTTP.3.0');
          xhr.open('GET', '/token', 1);
          xhr.onload = function () {
            if (xhr.status === 200) {
              tokenDeferral.Resolve(xhr.responseText);
            } else {
              tokenDeferral.Reject('Issue token request failed.');
            }
          };
          xhr.send();
        } catch (e) {
          window.console && console.log(e);
          tokenDeferral.Reject(e.message);
        }
        return tokenDeferral.Promise();
      }

      return new SDK.CognitiveTokenAuthentication(callback, callback);
    }();
    return SDK.CreateRecognizer(recognizerConfig, authentication);
  }
  jQuery('.page-preloader-wrap-dashboard').hide();
}
