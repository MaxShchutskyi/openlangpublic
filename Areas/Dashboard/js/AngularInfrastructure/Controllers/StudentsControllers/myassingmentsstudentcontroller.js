﻿myassingmentsstudentcontroller.$inject = ["$scope", "api", "$http", "notifications", "$timeout", '$filter'];
function myassingmentsstudentcontroller($scope, api, $http, notifications, $timeout, $filter) {
    if (!$scope.shared.currentPageOfAssingments)
        $scope.shared.currentPageOfAssingments = 0;
    getData();
    function getData() {
        api.ajax("/api/MyAssignmentsStudent/GetMyAssingment?page=" + $scope.shared.currentPageOfAssingments, 'get', null, function (res) {
            $scope.myAssingments = res;
            $scope.$digest();
        });
    }
    var selectedAssign = {};
    var originalAssign = {};
    $scope.clickToAssingment = function (assing, event) {
        if (assing.state === 'Checked') {
            notifications.show("error", resource.match('you_cant_chang_assignments'));
            return;
        }
        $("#hd").tab("show");
        $scope.shared.sceditorcap = true;
        selectedAssign = $.extend({}, assing);
        originalAssign = assing;
        //console.log($scope.shared.selectedAssingmen)
        console.log(selectedAssign);
        $scope.shared.selectedAssingmen = selectedAssign;
        var mt = moment(selectedAssign.dateOfChange);
        $scope.shared.selectedAssingmen.date = mt.format("ll");
        $scope.shared.selectedAssingmen.time = mt.format("HH:mm");
        $(event.target).parents('.assig-media').addClass("active").siblings().removeClass("active");
    };
    $scope.shared.saveAssingmentChanges = function () {
        api.ajax("/api/MyAssignmentsStudent/SaveAnswerAssingment", 'post', JSON.stringify(selectedAssign), function (res) {
            originalAssign.answer = res.answer;
            originalAssign.state = res.state;
            originalAssign.dateOfChage = res.dateOfChage;
            $scope.shared.selectedAssingmen = null;
            $scope.shared.sceditorcap = false;
            notifications.show("success", "Saved");
            $scope.$apply();
            $scope.shared.intervalAss();
        });
    }
    $scope.getNextPrevPage = function (state) {
        $scope.shared.currentPageOfAssingments = (state) ? ++$scope.shared.currentPageOfAssingments : --$scope.shared.currentPageOfAssingments;
        getData();
        $scope.shared.sceditorcap = false;
        $scope.shared.selectedAssingmen = null;
    }
    jQuery('.page-preloader-wrap-dashboard').hide();
}