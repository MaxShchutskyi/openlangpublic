﻿homeworktudentcontroller.$inject = ["$scope", "api", "$http", "notifications", "$timeout", '$filter'];
function homeworktudentcontroller($scope, api, $http, notifications, $timeout, $filter) {
  api.ajax('/api/HomeworkStudent/GetTablesHomework', 'get', null, function (res) {
    console.log(res.length + "   -  Homeworks");
    $scope.homeworks = res;
    $scope.$digest();
  });
  jQuery('.page-preloader-wrap-dashboard').hide();
}