﻿choisecontroller.$inject = ["$scope", "$timeout", "$location", "$routeParams", "api"];
function choisecontroller($scope, $timeout, $location, $routeParams, api) {
    $scope.curSlide = 0;
    $scope.curType = -1;
    $scope.curCheck = 0;
    console.log("choisecontroller");

    if ($routeParams.lessonId) {
        $scope.levelId = $routeParams.levelId;
        $scope.chapterId = $routeParams.chapterId;
        $scope.index = $routeParams.index;
        $scope.lessonName = $routeParams.lessonName
        api.ajax("/api/ChapterStudent/GetCards?lessonId=" + $routeParams.lessonId, "get", null, function (res) {
            $scope.cards = res;
            $scope.card = $scope.cards[$scope.curSlide];
            $scope.curType = $scope.card.Type;
            console.log("Type", $scope.curType);
            console.log($scope.card);
            $scope.$digest();
        });
    }
    $scope.shared.currentPath = $location.path();

    $scope.setTab = function () {
         $scope.curCheck += 1;
        if ($scope.cards.length - 1 === $scope.curSlide) {
            lessonComplete();
            return;
        }
        $scope.curType = -1;
        setTimeout(function () {
            $scope.card = $scope.cards[++$scope.curSlide];
            $scope.curType = $scope.card.Type;
        }, 0);
        console.log($scope.card);
    };

    $scope.tryAgain = function () {
        $.magnificPopup.close();
        document.location.reload();
    };

    $scope.nextTry = function () {
        $.magnificPopup.close();
        $scope.goto('/self-study/level/' + $scope.levelId + '/chapter/' + $scope.chapterId);
    };
    $scope.isFinishedCards = function () {
        if (!$scope.cards) return;
        if ($scope.cards.length === $scope.curCheck)
            return { 'backgroundColor': '#87d500', 'color': '#fff'};
    }
    function lessonComplete() {
       
        var totalCountCards = $scope.cards.filter(function (x) { return !x.ImageSlideSrc }).length;
        var successCountAnswers = $scope.cards.filter(function (x) { return !x.ImageSlideSrc && x.IsRightAnswer }).length;
        var percentCompleted = successCountAnswers / totalCountCards * 100;
        console.log("Percent completed - " + percentCompleted);
        var isSuccess = percentCompleted >= 75;
        $scope.attempt = getPopupData(isSuccess);
        $scope.attempt.isSuccess = isSuccess;
        $scope.attempt.percentSuccuss = percentCompleted.toFixed(0);
        setTimeout(function () {
            showPopup();
            drawCircle(percentCompleted, isSuccess);
        }, 1500);
        
        saveAttempt();
    }
    function saveAttempt() {
        api.ajax('/api/ChapterStudent/SaveAttempt', 'post', JSON.stringify($scope.attempt), function (res) {

        })
    }
    function drawCircle(percentCompleted, isSuccess) {
        var color = isSuccess ? { color: "rgba(154, 211, 65, 0.85)" } : { color: "#FF5649" };
        $('.circle-succes').circleProgress({
            fill: color,
            value: percentCompleted / 100,
            lineCap: "round",
            startAngle: Math.PI + 1.5708,
        });
    }
    function showPopup() {
        $.magnificPopup.open({
            removalDelay: 500,
            callbacks: {
                close: function () {
                    $scope.goto('/self-study');
                }
            },
            mainClass: 'mfp-zoom-in',
            items: {
                src: '.white-popup'
            },
            type: 'inline',
            closeOnBgClick: false,
            closeBtnInside: true,
            showCloseBtn: true
        }, 0);
    }
    function getPopupData(isSuccess) {
        if (isSuccess)
            return {
                title: "Congratulation!",
                description: "You have successfully completed this lesson and can proceed to the next",
                lessonId : $routeParams.lessonId
            }
        else
            return {
                title: "Lesson failed, please try again",
                description: "To propceed to the next lesson your success should be at least 75%",
                lessonId : $routeParams.lessonId
            }
    }
    
    $scope.testCheck = function () {
        
        var answer = $scope.card.checkAnswer();
        if (!answer)
            $scope.rightAnswerIs = $scope.card.getCorrectAnswer?$scope.card.getCorrectAnswer():"";
        console.log(answer);
    };
    $scope.goto = function (path) {
        console.log(path);
        $location.path(path);

    }
    $scope.closeLessons = function () {
        var isCloseProg = confirm("Are you sure you want to quit? The entire progress of this session will be lost.");
        if (isCloseProg) {
            $scope.goto('/self-study');
        };
    }
    jQuery('.page-preloader-wrap-dashboard').hide();
};
