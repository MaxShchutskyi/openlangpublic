﻿selfstudycontroller.$inject = ["$scope", "$timeout", "$location", "api", "$anchorScroll", "$routeParams"];
function selfstudycontroller($scope, $timeout, $location, api, $anchorScroll, $routeParams) {
    console.log("selfstudycontroller")
    if ($routeParams.levelId) {
        $scope.levelId = $routeParams.levelId;
        api.ajax("/api/ChapterStudent/GetAllChapters?levelId=" + $routeParams.levelId, "get", null, function (res) {
            $scope.chapters = res;
            $scope.$digest();
        })
        jQuery('.page-preloader-wrap-dashboard').hide();
    }
    $scope.shared.currentPath = $location.path();
    $anchorScroll();
    $scope.goTo = function (path) {
        $location.path(path);
    }
    $(".self-sudy-directive").animate({
        marginTop: "0",
    }, 300);

};

