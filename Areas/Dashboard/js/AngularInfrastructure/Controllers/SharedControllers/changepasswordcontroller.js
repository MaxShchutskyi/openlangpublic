﻿changepasswordcontroller.$inject = ["$scope", "notifications", "api"];
function changepasswordcontroller($scope, notifications, api) {
    var template = new TemplateValidation()
    template.setSettings({ errorPlacement: function () { } });
    template.init("#changepassword", "#changepasserrorplace", function (elem) {

    });
    $scope.changePasswordClick = function () {
        if (!$("#changepassword").valid() || !$("#confpasswords").valid())
            notifications.show('error', "Form has errors");
        else {
            api.ajax("/api/Password/ChangePassword", 'post',
                JSON.stringify({ OldPass: $scope.oldPass, NewPass: $scope.newPass, ConfirmPass: $scope.confPass }),
                function (res) {
                if(res.hasOwnProperty('error'))
                    notifications.show('error', res.error);
                if (res.hasOwnProperty('success')) {
                    notifications.show('success', res.success);
                    $scope.oldPass = $scope.newPass = $scope.confPass = "";
                    $scope.$digest();
                }
            });
        }
    }
    jQuery('.page-preloader-wrap-dashboard').hide();
}