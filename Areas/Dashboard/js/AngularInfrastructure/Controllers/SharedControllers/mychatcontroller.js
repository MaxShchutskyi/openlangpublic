﻿mychatcontroller.$inject = ["$scope", "api", "notifications","$routeParams"];

function mychatcontroller($scope, api, notifications, $routeParams) {
    $scope.shared.currentPath = "/chat"
    $scope.$on('$viewContentLoaded', function () {
        jQuery(".video-chat .full-screen").click(function () {
            alert("Works");
            if (jQuery(".video-chat .inner-video-chat").hasClass("toLowerScreen")) {
                jQuery(".video-chat .inner-video-chat").removeClass("toLowerScreen");
                jQuery(".video-chat .full-screen i").addClass("fa-compress").removeClass("fa-expand");
                jQuery("body").removeClass("toLowerScreen-wrap")
                $(".custom-scroll1.sc").mCustomScrollbar("scrollTo", "bottom", { scrollInertia: 0 });
            } else {
                jQuery(".video-chat .inner-video-chat").addClass("toLowerScreen");
                jQuery("body").addClass("toLowerScreen-wrap")
                jQuery(".video-chat .full-screen i").addClass("fa-expand").removeClass("fa-compress");
            }
        })
    });
    jQuery('.page-preloader-wrap-dashboard').hide();
    //jQuery(".contact-side .mess ").removeClass("active");
    //$("#chatPartner a").click(function () {
    //    if ($(".left-left-content").is(":visible")) {
    //        $(".left-left-content").hide();
    //        $(".sidebar-chat").removeClass("hidden-xs").removeClass("hidden-sm").addClass("visible-xs").addClass("visible-sm");
    //    }
    //})

   
    //$(".scroll").mCustomScrollbar();
    $scope.shared.flagMoreMessages = true;
    $scope.$evalAsync(function () {
        $("#showChatVidget").tab('show');
    });
    $scope.getReplies = function () {
        if (!$scope.shared.selectedDialog) return [];
        return $scope.shared.selectedDialog.Replies;
    }
    setTimeout(function () {
        //var y = 7200;
        //var duration = "top";
        //$(".chat-main .custom-scroll1").getNiceScroll(0).doScrollTop(y, duration);
        console.log("initScrollBars");
        $(".custom-scroll1").mCustomScrollbar({
            autoDraggerLength: false,
            advanced: {
                updateOnContentResize: true,
                autoUpdateTimeout: 300
            },
            callbacks: {
                //onBeforeUpdate: function () {
                //    $('.chat-main .chat-side .custom-scroll1 .mCustomScrollBox .mCSB_container').css('top', '-100px')

                //},
                whileScrolling: function () {
                    console.log(this.mcs.draggerTop)
                    if ($scope.shared.selectedDialog.finishPages)
                        return;
                    if (this.mcs.draggerTop == 0 && $scope.shared.flagMoreMessages) {
                        $(".custom-scroll1").mCustomScrollbar("stop");
                        console.log("sendMoreMessages");
                        $scope.shared.flagMoreMessages = false;
                        $scope.shared.selectedDialog.PageCounter += 1;
                        $scope.shared.hub.getMoreMessages(parseInt($scope.shared.selectedDialog.Id), $scope.shared.selectedDialog.PageCounter)
                    }
                }
            }
        });
        $(".custom-scroll").mCustomScrollbar({
            advanced: {
                updateOnContentResize: false,
                autoUpdateTimeout: 300
            },
        });
        console.log($scope.shared.globalDialogForConnect);
        if ($.cookie("selectDialog")) {
            setTimeout(function () {
                var res = $.cookie("selectDialog");
                angular.element(".mess .chat-item[data-clientid='" + res + "']").parent().trigger("click");
                $.cookie("selectDialog", null, { path: '/' });
                $.removeCookie('selectDialog', { path: '/' });
                document.cookie = "selectDialog" + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            }, 200);
        }
        if ($scope.shared.globalDialogForConnect) {
            jQuery(".mess .chat-item[data-clientid='" + $scope.shared.globalDialogForConnect + "']").parent().click();
            $scope.shared.globalDialogForConnect = null;
        }
        if ($routeParams.selectedDialogId) {
            console.log($routeParams.selectedDialogId);
            jQuery(".chat-item[data-clientid='" + $routeParams.selectedDialogId + "']").parent().click();
        }
        //$(".custom-scroll").mCustomScrollbar();
    }, 500);
    initSizes();
    $scope.getImage = function (reply, dialog) {
        if (dialog.Id == reply.UserId)
            return dialog.ProfilePicture;
        return $scope.shared.user.profilePicture;
        //if ($scope.shared.selectedDialog.Dialog.Id == reply.UserId)
        //    return $scope.shared.selectedDialog.Dialog.ProfilePicture;
        //return $scope.shared.user.profilePicture;
    }
    $scope.shared.clickToDialog = function (dialog, event) {
        console.log("clikked!!!");
        if (jQuery(window).width() < 768) {
            $(".left-left-content").hide(500);
            $(".sidebar-chat").show(500);
        }
        if (jQuery(".dlg").children().length !== 0) {
            jQuery("#videocall").removeClass("disable");
        }
        jQuery("#chat-preloader").fadeIn(0);
        $("#show-content").tab('show');
        $(event.currentTarget).addClass("active").siblings().removeClass("active");

        //$scope.selectedDialog = dialog;
        $scope.shared.selectedDialog = dialog;
        if ($scope.shared.selectedDialog.Replies) {
            console.log($scope.shared.selectedDialog.Replies);
            var sds = $scope.shared.selectedDialog.Replies.where(function (x) { return x.Unread });
            sds.forEach(function (x) { x.Unread = false });
            try{
                $scope.shared.hub.messagesWasRead(sds.select(function (x){return x.Id}).join(","));
            }
            catch(err){}
        } 
        setTimeout(function () {
            var y = 7200;
            var duration = "top";
            $(".custom-scroll1").mCustomScrollbar("scrollTo", "bottom", { scrollInertia: 0 });
            $("#chat-preloader").fadeOut(500);
        }, 200);

    }
    $scope.keyDown = function (event,dialog) {
        if (event.which === 13) {
            var html = $(event.currentTarget).html().trim().replace(/&lt;br&gt;/g, " ");
            console.log(html);
            if (!html) {
                event.stopPropagation();
                event.preventDefault();
                return;
            }
            event.stopPropagation();
            event.preventDefault();
            $(event.currentTarget).html('');
            sendMs(html, dialog);
        }
        //console.log(key);
    }
    $scope.videoScroll = function () {
        jQuery("#chat-preloader").fadeIn(0);
        setTimeout(function () {
            $(".custom-scroll1.sc").mCustomScrollbar("scrollTo", "bottom", { scrollInertia: 0 })
        }, 200);
        jQuery("#chat-preloader").fadeOut(500);
    }


    $scope.addFileChat = function (param) {
        if (param)
            $("#" + param).click();
        else
            $("#chatloadfiles").click();
    }
    $scope.uploadFiles = function (isVideoChat) {
        debugger;
        var files = document.getElementById('chatloadfiles').files;
        console.log(files);
        var res = shared.validateFiles(files);
        if (res !== "OK") {
            //console.log(res);
            notifications.show('error', res);
            return;
        }
        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            data.append(files[i].name, files[i]);
        }
        $.ajax({
            type: "POST",
            url: '/en/Chat/UploadImages',
            contentType: false,
            dataType: 'json',
            processData: false,
            //xhr: function() {
            //    var myXhr = $.ajaxSettings.xhr();
            //    if(myXhr.upload){
            //        myXhr.upload.addEventListener('progress',progress, false);
            //    }
            //    return myXhr;
            //},
            data: data,
            success: function(result) {
                console.log(result);
                var textv = "<a href = '" + result.path + "' target='_blank'>" + result.name + "</a>";
                var dialog = $scope.shared.selectedDialog;
                if (isVideoChat)
                    dialog = $scope.shared.selectedVideoDialog;
                dialog.Replies.unshift({
                    DialogId: $scope.shared.selectedDialog.Id,
                    Message: textv,
                    Unread: false,
                    UtcDateTime: new Date().toISOString(),
                    UserId: $scope.shared.userId
                });
                $scope.shared.hub.sendTo(dialog.Id, $scope.shared.userId, textv, dialog.Dialog.Id);
                //chat.server.sendTo(dialogid, JSON.stringify(curUser), message.text(), toId);
                setTimeout(function () {
                    $(".custom-scroll1").mCustomScrollbar("scrollTo", "bottom", { scrollInertia: 0 })
                }, 200);
                $scope.$apply();
            },
            error: function (xhr, status, p3) {
                alert(xhr.responseText);
            }
        });
    }

    //mobile




    //$(".chat-widget #mCSB_2").attr("id", "#mCSB_3");
    //$(".chat-widget #mCSB_2_container").attr("id", "#mCSB_2_container");



    $scope.shared.getDate = function (date) {
        var dt = moment.utc(date).toDate();
        return moment(dt).local().format('DD MM')
    }
    $scope.shared.getDate2 = function (date) {
        var dt = moment.utc(date).toDate();
        return moment(dt).local().format('DD/MM/YYYY HH:mm')
    }
    $scope.getStatus = function () {
        if (!$scope.shared.selectedDialog.Active) {
            jQuery("#onOrOff").removeClass("point-active");
            return $scope.shared.getDate2($scope.shared.selectedDialog.Dialog.DateOffline);
        } else {
            jQuery("#onOrOff").addClass("point-active");
            return "Online"
        }

    }
    $scope.checkVideoSupport = function () {
        var browser = navigator.browser.toLowerCase();
        if (!browser || browser.indexOf("ie") !== -1 || browser.indexOf("safari") !== -1 || browser.indexOf("edge") !== -1)
            return false;
        return true;
    }
    $scope.confirmAnswer = function (withVideo) {
        $.magnificPopup.open({
            items: {
                src: '#video-chat'
            },
            enableEscapeKey: false,
            closeOnBgClick: false,
            callbacks: {
                beforeOpen: function () { this.wrap.removeAttr('tabindex') }
            }
        });

        $("#video-chat .controls div").each(function (index, elem) { $(elem).removeClass('turn') });
        if (!withVideo)
            $("#video-chat .controls div.hide-video").addClass('turn');
        setTimeout(function () {
            $(".custom-scroll1.sc").mCustomScrollbar().mCustomScrollbar("scrollTo", "bottom", { scrollInertia: 0 })
        }, 200);
        confirmAnswer($scope.shared.userId, withVideo);
    }
    $scope.hideVideo = function () {
        disableVideo();
    }
    $scope.endCall = function () {
        endCall();
    }
    $scope.mute = function () {
        mute();
    }


    $scope.call = function () {
        if (!$scope.shared.selectedDialog || !$scope.shared.selectedDialog.Active)
            return;
        //if (!$scope.shared.selectedDialog)
        //    return;
        $.magnificPopup.open({
            items: {
                src: '#video-chat'
            },
            enableEscapeKey: false,
            closeOnBgClick: false,
            callbacks: {
                beforeOpen: function () { this.wrap.removeAttr('tabindex') }
            }
        });
        $scope.shared.selectedVideoDialog = $scope.shared.dialogs.firstOrNull(function (el) { return el.Id == $scope.shared.selectedDialog.Id });
        //console.log($scope.shared.selectedDialog);
        $(".center-video-img").css("display", "block");
        $("#video-chat .controls div").each(function (index, elem) { $(elem).removeClass('turn') });
        $("#video-chat .controls div.hide-video").addClass('turn');
        setTimeout(function () {
            $(".custom-scroll1.sc").mCustomScrollbar("scrollTo", "bottom", { scrollInertia: 0 });
        }, 200);
        jQuery("#chat-preloader").fadeOut(1000);
        var hub = $scope.shared.hub.getHub();
        videoCall($scope.shared.selectedDialog.Dialog.Id, hub, $scope.shared.userId);
    }
    $scope.getClass = function (reply, dialog) {
        return reply.UserId == dialog.Id ? "pull-right second" : "pull-left first";
    }
    function sendMs(text, dialog) {
        //var text = $(".chat-message .textarea").html();
        if (!text) return;
        dialog.Replies.unshift({
            DialogId: dialog.Id,
            Message: text,
            Unread: false,
            UtcDateTime: new Date().toISOString(),
            UserId: $scope.shared.userId
        });
        console.log(new Date().toISOString());
        $scope.shared.hub.sendTo(dialog.Id, $scope.shared.userId, text, dialog.Dialog.Id);
        var selIndex = $scope.shared.dialogs.findIndex(function (el) {
            return el.Id == dialog.Id;
        });
        $scope.shared.dialogs.move(selIndex, 0);
        $(".chat-message .textarea").html("");
        setTimeout(function () {
            var y = 7200;
            var duration = "top";
            $(".custom-scroll1").mCustomScrollbar("scrollTo", "bottom", { scrollInertia: 0 });
            
        }, 200);
        //var el = document.getElementById("sendMessage");
        //var range = document.createRange();
        //range.setStart(el.childNodes[1], 1);
    }
    $scope.sendMessage = function (cls, dialog) {
        //console.log("SNDMSG");
        //sendMs();
        var html = $(".chat-message .textarea" + cls).html().trim().replace(/&lt;br&gt;/g, " ");
        //console.log(html);
        if (!html) {
            event.stopPropagation();
            event.preventDefault();
            return;
        }
        jQuery('.chat-message .textarea' + cls).html('');
        sendMs(html, dialog);
        //$scope.$apply();
    }

    //Press enter to send

    $(".send-message").click(function () {
        //$scope.sendMessage();
    })
    $(".chat-message .textarea").bind("keydown keypress", function (event) {


    });
}


function getUtcDate() {
    var now = new Date();
    return new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
}
function initSizes() {
    var allDialogs = $(".media.chat-item .patnerName text");
    $("#searchByName")
     .keyup(function () {
         allDialogs.each(function (index, elem) {
             if (elem.innerText.toLowerCase().indexOf($("#searchByName").val().toLowerCase()) == -1)
                 $(elem).parents(".mess").css("display", "none");
             else
                 $(elem).parents(".mess").css("display", "block");
         });
     });

    jQuery(".smile").click(function () {
        if (jQuery(".smile-block").is(":visible")) {
            jQuery(".smile-block").hide();
        } else {
            jQuery(".smile-block").show();
        };
    });
    jQuery(".smile-img").click(function () {
        jQuery(".smile-block").hide(300);
    })
    jQuery("#video-chat .controls div").click(function () {
        if (jQuery(this).hasClass("turn")) {
            jQuery(this).removeClass("turn");
        } else {
            jQuery(this).addClass("turn");
        }
    })



    //$(".video-answer").magnificPopup();
    //$("#videocall").magnificPopup();


    $(".hide-chat ").click(function () {
        if ($(".chat-side").is(":visible")) {
            $("#video-chat .chat-side").slideToggle("slow");
            $("#video-chat .video-messages").slideToggle("slow", function () {
                $(".bottom-video").toggleClass("botPosition");
            });
            var videoHeight = jQuery(".video-wrapper").height();
            //jQuery("video#remote , .video-wrapper").height(videoHeight + $(".chat-side").height() + $(".video-messages").height())
        } else {
            $("#video-chat .chat-side").slideToggle("slow");
            $("#video-chat .video-messages").slideToggle("slow", function () {
                $(".bottom-video").toggleClass("botPosition");
            });
            var videoHeight = jQuery(".video-wrapper").height();
            //jQuery("video#remote , .video-wrapper").height(videoHeight - $(".chat-side").height() - $(".video-messages").height())
        }
    })

    $(".end-call").click(function () {
        $(".mfp-close").click();
        $(".controls div").removeClass("turn")
    });
    $(".smile-block .smile-img").click( function call(x) {
        var x = $(this).attr('src');
        var currentHtml = $(".chat-message .textarea").html();
        $(".chat-message .textarea").html(currentHtml + "<img src=\" " + x + " \">");
    });

    $(".mess").removeClass("active")
    $(window).load(function () {
        $(".mess").removeClass("active");
        //$(".mess").click(function () {
        //    if ($(".no-choice-chat").is(":visible")) {
        //        $(".no-choice-chat").hide(300);
        //        $(".chat-header , .chat-main").show(300);
        //    }
        //})
        $("#ascrail2001").appendTo(".chat-side");
        $("#ascrail2001-hr").appendTo(".chat-side");
        //If no teache , find a teacher



        //top buttons click , add class active choice to angular 

        //if (jQuery(".messages").attr("onclick").slice(-5, -1) == window.location.href.slice(-4)) {
        //    jQuery(".top-header .messages").addClass("active");
        //}
        //if online add green point
        if ($(".teach-title #onOrOff").html() == 'Online') {
            $(this).addClass('point-active');
        }

    });


    jQuery(document).ready(function () {
    
         //To lower screen throw video calling



        //Chat and right sidebar height
       
        var windowHeight = jQuery(window).height();
        var windowWidth = jQuery(window).width();
        jQuery('.chat-window  .custom-scroll1').height(windowHeight - jQuery('.chat-header').height() - jQuery('.chat-message').height() - jQuery('.myAccount-footer').height());

        setInterval(function () {
            var windowWidth = jQuery(window).width();
            if (windowWidth > 768) {
                jQuery('.chat-window  .no-choice-chat .flex-wrap').height(windowHeight - jQuery('.myAccount-footer').height());
                jQuery('.chat-widget').height(jQuery('.chat-window').height() - 15);


                //video chat height
                jQuery(".video-wrapper , video#remote").height(windowHeight - jQuery("#video-chat .chat-side").height() - jQuery("#video-chat .video-messages .chat-message").height());
                //jQuery("video#remote , .video-wrapper").height((windowHeight - jQuery("#video-chat .chat-side").height() - jQuery("#video-chat .video-messages").height()) / 1.4);
                var videoMessageH = $("#video-chat .video-wrapper").height();
                $("#video-chat .custom-scroll1").height(videoMessageH / 4);
                jQuery('.chat-widget .mCustomScrollbar').height(jQuery(".chat-widget").height() - jQuery(".top-panel").height() + 10);
                var videoMessageHs = jQuery(".chat-side .custom-scroll1").height() + 100;

                if (windowWidth > 992) {
                    $(".video-chat .inner-video-chat .bottom-video").css("bottom", videoMessageHs);
                }
            }

            
        },200);

        //jQuery(window).load(function () {
        //    var windowWidth = jQuery(window).width();
        //    if (windowWidth < 992) {
        //        $("#chatPartner a").click(function () {
        //            $(".left-left-content").animate({
        //                right: "-100%"
        //            }, 500);
        //            $(".sidebar-chat").animate({
        //                right: "0%"
        //            }, 500);
        //        })
        //        $(".chat-item").click(function () {
        //            $(".left-left-content").animate({
        //                right: "0%"
        //            }, 500);
        //            $(".sidebar-chat").animate({
        //                right: "-100%"
        //            }, 500);
        //        });
        //    }

        //});


        //jQuery('.chat-message .textarea').keyup(function (eventObject) { //отлавливаем нажатие клавиш

        //    if (event.keyCode == 13) { //если нажали Enter, то true
        //        var html = $(this).html().trim().replace(/&lt;br&gt;/g, " ");
        //        console.log(html);
        //        if (!html) {
        //            event.stopPropagation();
        //            event.preventDefault();
        //            return;
        //        }
        //        //var range = document.createRange();
        //        //var el = document.getElementById("sendMessage");
        //        //range.setStart(el, 0);
        //        //range.setEnd(el, 0)
        //        //var sel = window.getSelection();
        //        //sel.removeAllRanges();
        //        //sel.addRange(range);

        //        jQuery('.chat-message .textarea').html('');

        //    }
        //});
        //jQuery(".contact-side .mess").click(function () {
        //    jQuery(".sidebar-chat").hide();
        //    jQuery("#bodys").show().css("opacity", "1");
        //})
        //jQuery("#chatPartner").click(function () {
        //    jQuery(".sidebar-chat").show();
        //    jQuery("#bodys").hide().css("opacity", "0");
        //})
    });
}
