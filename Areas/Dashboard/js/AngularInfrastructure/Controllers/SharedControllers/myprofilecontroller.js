﻿myprofilecontroller.$inject = ["$route", "$scope", "$templateCache", "$http", "notifications", "api"];
function myprofilecontroller($route, $scope, $templateCache, $http, notifications, api) {
    var currentPageTemplate = $route.current.templateUrl;
    $templateCache.remove(currentPageTemplate);
    $scope.currencies = currencies;
    $scope.currentUser = usr;
    $scope.myPhoto = [];
    $scope.currentUser.isSendMeConfirmationByWeb = Boolean($.cookie("subscribed"));
    $scope.$watch('currentUser.name', function (newval) {
        $scope.shared.user.name = newval;
    });
    $scope.$watch('currentUser.isSendMeConfirmationByWeb', function (newVal, oldVal) {
        if (newVal === oldVal)
            return;
        newVal ? subscribe() : unsubscribe();
        newVal ? $.cookie("subscribed", true) : shared.clearCookie("subscribed");
    })
    'use strict';
    $scope.onPaginate = function () {
        $scope.selected = [];
    }
    $scope.limitOptions = [5, 10, 15];
    $scope.query = {
        order: 'name',
        limit: 5,
        page: 1
    };

    $scope.options = {

        limitSelect: true,
        pageSelect: true
    };
    $scope.logPagination = function (page, limit) {
        console.log('page: ', page);
        console.log('limit: ', limit);
    }
    $scope.myFrame = {
        "color": "red",
        "background-color": "coral",
        "font-size": "60px",
        "padding": "50px"
    }
    console.log($scope.myFrame);
    
  

    $scope.$watch('currentUser.currency', function (newval, oldval) {
        if (newval === oldval)
            return;
        var nv = newval.split(':')[0];
        var money2 = $(".moneyNative").text().split(' ')[0];
        $http({ url: "/en/NewMyAccount/GetNativeMoney", params: { money: money2, currency: nv }, method: 'get' }).success(function (res) {
            $scope.shared.globalDelta = res.delta;
            $scope.shared.globalCurrency = nv;
            $scope.shared.NativeMoney = res.nativeMoney;
            //$(".money").text("~ " + res.nativeMoney); 
        });
    });
    $scope.uoPhto = function () {
        $("#upload-photo").click();
    }
    $scope.save = function () {
        var localNumb = $scope.currentUser.phone;
        //$scope.currentUser.phone = $scope.customUser.fullPhone.intlTelInput("getNumber");
        api.ajax('/api/Account', 'put', JSON.stringify($scope.currentUser), function (res) {
            console.log(res);
            notifications.show('success', "Saved");
        });
    };
    api.ajax("/api/DashboardLanguages/GetMyLanguagesApi", 'get', null, function (langData) {
        //console.log(langData);
        $scope.ilearn = langData.ilearn ? langData.ilearn : [];
        $scope.ispeak = langData.ispeak ? langData.ispeak : [];
    });
    $scope.saveLanguages = function () {
        //debugger;
        var il = $scope.ilearn.map(function (elem) {
            return "cap_" + elem;
        }).join('|');
        if (il)
            il += "|";
        var is = $scope.ispeak.map(function (elem) {
            return "cap_" + elem;
        }).join('|');
        if (is)
            is += "|";
        $http({ method: 'post', url: '/en/NewMyAccount/SaveLanguages', params: { 'il': il, 'is': is } }).success(function () {
            $scope.shared.changeMyILearnLanguages = !$scope.shared.changeMyILearnLanguages;
            notifications.show('success', "Saved");
        })
    }
    $scope.changePasswordClick = function () {
        if (!$("#changepassword").valid() || !$("#password-check").valid())
            notifications.show('error', "Form has errors");
        else {
            api.ajax("/api/Password/ChangePassword", 'post',
                JSON.stringify({ OldPass: $scope.oldPass, NewPass: $scope.newPass, ConfirmPass: $scope.confPass }),
                function (res) {
                    if (res.hasOwnProperty('error'))
                        notifications.show('error', res.error);
                    if (res.hasOwnProperty('success')) {
                        notifications.show('success', res.success);
                        $scope.oldPass = $scope.newPass = $scope.confPass = "";
                        $scope.$digest();
                    }
                });
        }
    }
    $scope.getNormalDate = function (date) {
        if (!date) return "";
        return moment(date).format("DD-MM-YYYY HH:mm");
    }
    api.ajax("/api/PaymentReceipts/GetPaymentReceipts", 'get', null, function (res) {
        //debugger;
        $scope.paymentReceipts = res;
        $scope.$apply();
    })
    setTimeout(function () {
        $('select').formSelect();
    }, 500)
    if (isTeacher) {
        api.ajax('/api/IndividualRates/GetAllRates', 'get', null, function (res) {
            $scope.rate = res;
            $scope.$digest();
        })
        $scope.saveRates = function () {
            api.ajax('/api/IndividualRates/UpdateDay', 'post', JSON.stringify($scope.rate), function () {
                notifications.show('success', "Saved");
            })
        }
        $scope.commonRate = {};
        api.ajax('/api/AdminTeacherRates/GetCommonRate', 'get', null, function (res) {
            $scope.commonRate = res;
            $scope.$digest();
        });
        $scope.getClearRate = function (rate, percent) {
            percent = percent !== -1 ? percent : $scope.commonRate.Rate;
            //var percent = "30"; // Необходимый процент
            var number_percent = rate / 100 * percent;
            var res = Number(rate) - Number(number_percent);
            if (res == NaN)
                return 0;
            return res.toFixed(2);
        }
        $scope.uploadImage = function () {
            var data = new FormData();
            var file = document.getElementById("upload-photo").files[0];
            data.append(file.name, file);
            $http.post('/UploadImages/UploadAvatar', data, {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            }).success(function (res) {
                $scope.currentUser.profilePicture = res.path;
                $scope.shared.user.profilePicture = res.path;
                usr.profilePicture = res.path;
            });
        };
        jQuery('.page-preloader-wrap-dashboard').hide();
      

        //////////////////     ABOUT Controller  ////////////////////
        $scope.item = { videoSrc: '' };

        $scope.uploadVideo = function () {
            var file = document.getElementById('loadMyVideo').files[0];
            $scope.uploadedVideo = file;
            console.log($scope.uploadedVideo);
            if (file.type != "video/mp4") {
                notifications.show('error', "You should load only in MP4 format");
                return;
            }
            if (file.size > 41943040) {
                notifications.show('error', "You should load file not more 40MB");
                return;
            }
            var data = new FormData();
            data.append(file.name, file);
            var progressBar = $('#progressbar');
            progressBar.css("display", "block")
            shared.loadVideoFile(data, '/en/LoadFiles/LoadVideoForTheTeacher', progressBar, function (res) {
                $scope.item.videoSrc = res.link;
                $scope.$digest();
            })
        }
        //$scope.deleteVideo = function () {
        //    $scope.uploadedVideo = [];
        //    console.log($scope.uploadedVideo.length);
        //}
        $scope.$watch('item.videoSrc', function (newVal) {
            if (newVal.indexOf("http") === -1)
                document.getElementById('vdvideo').load();
            else if (newVal.indexOf("youtube") !== -1 && newVal.indexOf("embed") === -1) {
                var v = shared.gup('v', newVal);
                var someval = "https://www.youtube.com/embed/" + v;
                $scope.item.videoSrc = someval;
            }
            else if (newVal.indexOf("vimeo") !== -1 && newVal.indexOf("player") === -1) {
                var vl = newVal.split('/');
                var le = vl[vl.length - 1];
                var someval = "https://player.vimeo.com/video/" + le;
                $scope.item.videoSrc = someval;
            }
        })
        $scope.saveChanges = function () {
            console.log($scope.item.teacherLevels = JSON.stringify($scope.item.levels));
            api.ajax("/api/AboutMe/UpdateAboutMeInfo", 'post', JSON.stringify($scope.item), function (res) {
                if (res.hasOwnProperty('error'))
                    notifications.show('error', res.error);
                else
                    notifications.show('success', "Saved");
            })
        };
        api.ajax("/api/AboutMe/GetAboutMeInfo", 'get', null, function (res) {
            console.log(res);
            $scope.item = res;
            $scope.item.levels = {};
            $scope.item.levels.Beginner = false;
            $scope.item.levels.UpperBeginner = false;
            $scope.item.levels.Intermediate = false;
            $scope.item.levels.UpperIntermediate = false;
            $scope.item.levels.Advanced = false;
            $scope.item.levels.UpperAdvanced = false;
            var lt = JSON.parse(res.TeacherLevels);
            for (key in lt) {
                $scope.item.levels[key] = lt[key];
            }
            console.log($scope.levels);
            $scope.$digest();
        })
        jQuery('.page-preloader-wrap-dashboard').hide();

        //////////////////////    EXPERIENCE  ///////////////////////////
        $scope.options = {
            language: 'en',
            allowedContent: true,
            entities: false
        };
        $scope.educations = [];
        $scope.expiriences = [];
        $scope.certifications = [];
        $scope.addItem = function (array, elem) {
            var item = { Description: "", Title: "", PeriodFrom: "2016", PeriodTo: "2016", Id: -1, Deleted: false };
            array.push(item);
        }
        $scope.getYears = function () {
            var curYear = new Date().getFullYear()
            var years = [];
            for (var i = 1945; i <= curYear; i++) {
                years.push(i);
            }
            return years;
        }
       
        $scope.deleteItem = function (array, elem, index) {
            if (elem.Id == -1)
                array.splice(index, 1);
            else
                elem.Deleted = true;
        }
        $scope.removeEdu = function (edu) {
            edu.Deleted = true;
        }
        $scope.removeWork = function (work) {
            work.Deleted = true;
        }
        $scope.removeCert = function (cert) {
            cert.Deleted = true;
        }
        $scope.saveChangesExp = function () {

            api.ajax("/api/MyExperience/SaveEducation", 'post', JSON.stringify($scope.educations), function (res) {
                if (res.hasOwnProperty('error')) {
                    notifications.show('error', res.error);
                    return;
                }
            });
            api.ajax("/api/MyExperience/SaveExpiriences", 'post', JSON.stringify($scope.expiriences), function (res) {
                if (res.hasOwnProperty('error')) {
                    notifications.show('error', res.error);
                    return;
                }
            })
            api.ajax("/api/MyExperience/SaveCertifications", 'post', JSON.stringify($scope.certifications), function (res) {
                if (res.hasOwnProperty('error')) {
                    notifications.show('error', res.error);
                    return;
                }
                else
                    notifications.show('success', "Saved");
            })

        }
        api.ajax("/api/MyExperience/GetMyExperiance", 'get', null, function (res) {
            //debugger;
            $scope.educations = res.educations;
            $scope.expiriences = res.expiriences;
            $scope.certifications = res.certifications;
            $scope.$digest();
        })
        console.log("myexperiencecontroler");
        function onlyDigits() {
            if (this.value.length == 1) {
                console.log("adadad");
                this.value = this.value.replace(/\./g, "0.");
            }
            this.value = this.value.replace(/,/, '.');
            this.value = this.value.replace(/[^\d\.]/g, "");
            console.log(this.value.length);

            if (this.value.match(/\./g).length > 1) {
                this.value = this.value.substr(0, this.value.lastIndexOf("."));
            }
        }
        document.querySelector(".onlyDigits").onkeyup = onlyDigits
        document.querySelector(".onlyDigits2").onkeyup = onlyDigits
        document.querySelector(".onlyDigits3").onkeyup = onlyDigits
        document.querySelector(".onlyDigits4").onkeyup = onlyDigits
        document.querySelector(".onlyDigits5").onkeyup = onlyDigits
    }
    jQuery('.page-preloader-wrap-dashboard').hide();


    ////////////////////////   PAYMENT   ////////////////////////
    'use strict';
    $scope.onPaginate = function () {
        $scope.selected = [];
    }
    $scope.limitOptions = [5, 10, 15];
    $scope.query = {
        order: 'name',
        limit: 5,
        page: 1
    };

    $scope.options = {

        limitSelect: true,
        pageSelect: true
    };
    $scope.logPagination = function (page, limit) {
        console.log('page: ', page);
        console.log('limit: ', limit);
    }

    ///////////////////////////   PHOTOGALLERY    ///////////////////////
    $scope.$watch('files.length', function (newVal, oldVal) {
        setTimeout(function () {
            var elems = document.querySelectorAll('.materialboxed');
            var instances = M.Materialbox.init(elems);
            if (newVal > oldVal) {
                var formData = new FormData();
                angular.forEach($scope.files, function (obj) {
                    if (!obj.isRemote) {
                        formData.append('files[]', obj.lfFile);
                    }
                });
                $http.post('/en/MyAccountTeacher/UploadImages', formData, {
                    withCredentials: true,
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                }).then(function (result) {
                    $scope.files.forEach(function (x) { x.isRemote = true });                 
                });
            }
        }, 200);
    });
    api.ajax('/api/Photogallery/GetGallery', 'get', null, function (res) {
        res.forEach(function (res,i) {
            $scope.api.addRemoteFile(res.linkBigImage, res.fileName, 'image')
        })
        //console.log("Photogallery");
        console.log(res);
        //$scope.photos = res;
        $scope.$digest();
    });
    $scope.onFileClick = function (obj, idx, $event) {
        console.log(obj);
        var myPhoto = $event.currentTarget;
        if (!obj.selected) {
            myPhoto.style.borderColor = "#2196f3";
            myPhoto.style.backgroundColor = "#e8f0fe";
            
        } else {
            myPhoto.style.borderColor = "#eee";
            myPhoto.style.backgroundColor = "transparent";
        }
        obj.selected = !obj.selected;
    };
    $scope.getChosenFiles = function () {
        if (!$scope.files) return 0;
        return $scope.files.filter(function (x) { return x.selected }).length;
    }
    $scope.removeFiles = function () {
        var selected = $scope.files.filter(x => x.selected);
        var names = selected.map(function (x) { return x.lfFileName });
        debugger;
        var data = { imgs: names };
        api.ajax('/api/Photogallery/RemoveActiveImages', 'post', JSON.stringify(names), function (res) {
        });
        $scope.files = $scope.files.filter(x => !x.selected);
    };
    
}