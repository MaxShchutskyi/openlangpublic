﻿myaccountpage.$inject = ["$scope", "$location", "$interval", "api", "$animate", "$http","notifications"];
function myaccountpage($scope, $location, $interval, api, $animate, $http, notifications ) {
    //$.fn.select2.defaults.set('language', $.cookie('lang'));
    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false
    };
    $scope.shared = {};
    $scope.shared.selectedLesson = {};
    $scope.shared.changeMyILearnLanguages = false;
    $scope.shared.selectedWord = {};
    $scope.shared.user = user;
    $scope.shared.notifications = [];
    console.log("currentUser is  -")
    console.log($scope.shared.user);
    $scope.shared.classActive = true;
    console.log($scope.shared.user);
    $scope.shared.globalTitle = '';
    $scope.shared.getNormalDate = function (date) {
        if (!date) return;
        return moment(date).format("DD.MM.YYYY");
    }

    $scope.shared.removeNotification = function (note) {


        var index = $scope.shared.notifications.indexOf(note);
        $scope.shared.notifications.splice(index, 1);

        api.ajax("/api/Notification/WasRead?id=" + note.Id, 'get', null, function () {

        });
    }


    $scope.shared.currentPath = $location.path();
    if (!$scope.shared.currentPath)
        $scope.shared.currentPath = "/mylessons";
    $scope.shared.userId = userId;
    $scope.isActive = function (path, state) {
        return path === $location.path();
    }
    $scope.isActiveProfile = function (paths) {
        var state = false;
        for (var i = 0; i < paths.length; i++) {
            if ($location.path() === paths[i]) {
                state = true;
                break;
            }
        }
        return state;
    }
    $scope.changePage = function (event, path) {

        $(this).addClass("active");
        if (path.indexOf('chat') === -1)
            $("#show-content2").tab('show');
        $location.path(path);
        $scope.shared.currentPath = path;
        $scope.shared.selectedAssingmen = null;
        $scope.shared.sceditorcap = false;
        $scope.shared.selectedWord = {};
        $scope.shared.selectedDialog = null;
        changeHeader(path);
        $("a[data-target='#main_content']").click();

    }
    $scope.changeClassPath = function (path, state) {
        $scope.shared.classActive = state;
        $location.path(path);
    };
    function changeHeader(path) {
        if (!path)
            path = $location.path();
        var elem = routes.firstOrNull(function (elem) { return elem.route === path });
        if (elem)
            $scope.shared.globalTitle = elem.header;
    }
    var lng = $.cookie('lang');
    if (lng == 'en') lng = 'gb';
    if (lng == 'zh') lng = 'cn';
    if (lng == 'pt') lng = 'br';

    $scope.selectedLang = "https://media.info/i/fl/" + lng.toUpperCase() + ".png";
    $scope.checkSelectedLang = function (lang) {
        var lnguage = $.cookie('lang');
        //console.log("Current lang is -" + lnguage)
        return lang === lnguage;
    }
    $scope.changeLang = function (lang) {
        // $.cookie('lang', lang, { path: '/', expires: 30 });
        var query = location.pathname.split('/');
        query[1] = lang;
        location.href = query.join("/");
        $scope.selectedLang = "https://media.info/i/fl/" + lang.toUpperCase() + ".png";
        //location.reload();
    }
    $scope.shared.intervalAss = function () {
        var controller = isTeacher ? "MyAssignments" : "MyAssignmentsStudent";
        api.ajax('/api/' + controller + '/GetActiveAssingments', 'post', null, function (res) {
            $scope.countAss = res;
            //$scope.$digest();
        });
    }
    $scope.countMessages = null;
    $scope.getCountMessages = function () {
        if (!$scope.shared.dialogs) return null;
        var reureads = $scope.shared.dialogs.select(function (dialog) {
            var doal = dialog.Replies.where(function (reply) {
                return reply.Unread == true
            });
            if (doal)
                return doal;
            return null;
        }).where(function (el) {
            return el.length
        });
        var merged = [].concat.apply([], reureads);
        return merged.length;
    }
    $interval($scope.shared.intervalAss, 60000);
    $scope.shared.intervalAss();

    $scope.$watch('shared.changeMyILearnLanguages', function (newVal, oldVal) {
        if (newVal === oldVal)
            return;
        api.ajax('/api/Languages/GetILearn', 'get', null, function (res) {
            $scope.shared.ilearn = res;
            $scope.$digest();
        });
    });
    api.ajax('/api/Account/GetCurrentInfo', 'get', null, function (res) {
        $scope.shared.currentPrices = res;
        console.log(res);
    })
    api.ajax('/api/Account/GetCurrentPrices?country=' + currentUserHelper.getCountry() + '&timezone=' + currentUserHelper.getTimezone() + '', 'get', null, function (res) {
        $scope.shared.prices = res;
    })
    function getNotifications() {
        api.ajax('/api/Notification/GetNotifications', "get", null, function (res) {
            console.log("Notifications are - ");
            console.log(res);
            $scope.shared.notifications = res;
            $scope.$apply();
        });
    }
    setInterval(function () {
        getNotifications();
    }, 15000);
    getNotifications();
    $scope.shared.updateCredits = function () {
        api.ajax('/api/Account/GetCurrentGetAllCreditsPrices', 'get', null, function (res) {
            $scope.shared.credits = res;
            console.log(res);
            //$scope.$digest();
        })
    }
    $scope.shared.updateCredits();
    api.ajax("/api/Account/GetBalance", 'get', null, function (res) {
        //$scope.shared.globalDelta = res.delta?res.delta:1;
        //$scope.shared.globalCurrency = res.Currency;
        //$scope.shared.NativeMoney = res.NativeMoney;
        //$scope.shared.money = res.Money;
        //$scope.$digest();
        //scope.primaryPrice = (scope.shared.currentPrices.Premium * res.delta).toFixed(2) + " " + res.Currency;
        //scope.basicPrice = (scope.shared.currentPrices.Basic * res.delta).toFixed(2) + " " + res.Currency;
        //scope.groupPrice = (scope.shared.currentPrices.Group * res.delta).toFixed(2) + " " + res.Currency;
    });
    changeHeader();
    function getMyActiveUsers(ids) {
        var dd = $scope.shared.dialogs.where(function (x) { return ids.firstOrNull(function (f) { return f == x.Dialog.Id }) });
        dd.forEach(function (x) { x.Active = true });
        $scope.$apply()
    }
    function getUtcDate() {
        var now = new Date();
        return new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
    }
    function someClietnChangedState(id, state) {
        var d = $scope.shared.dialogs.firstOrNull(function (x) { return x.Dialog.Id == id });
        d.Active = state;
        if (!state)
            d.Dialog.DateOffline = getUtcDate();
        $scope.$apply()
    }
    function receiveMessage(message, from, dialogReplyId, dialogId) {
        var dialog = $scope.shared.dialogs.firstOrNull(function (x) { return x.Id == dialogId });
        var selIndex = $scope.shared.dialogs.findIndex(function (el) {
            return el.Id == dialogId;
        });
        $scope.shared.dialogs.move(selIndex, 0);
        var reply = {
            Id: dialogReplyId,
            DialogId: dialogId,
            Message: message,
            Unread: $scope.shared.selectedDialog == null ? true : $scope.shared.selectedDialog.Id != dialogId,
            UtcDateTime: new Date().toISOString(),
            UserId: from
        };
        dialog.Replies.unshift(reply);
        if (!reply.Unread) {
            $scope.shared.hub.messagesWasRead(dialogReplyId.toString());
            setTimeout(function () {
                $(".custom-scroll1").mCustomScrollbar("scrollTo", "bottom");
            }, 200);
        }
        else {
            document.getElementById("recMess").play()
        }
        $scope.$digest();

    }

    function inviteToVideoChatToVideo(from) {
        $scope.shared.selectedVideoDialog = $scope.shared.dialogs.firstOrNull(function (el) { return el.Dialog.Id == from });
        if (!$scope.shared.selectedVideoDialog) return;
        $.magnificPopup.open({
            items: {
                src: '#video-chat-answer'
            },
            enableEscapeKey: false,
            closeOnBgClick: false,
            callbacks: {
                beforeOpen: function () { this.wrap.removeAttr('tabindex') }
            }
        });
        $scope.$apply();
    }
    function replyMoreMessages(result) {
        if (!$scope.shared.selectedDialog)
            return;
        //var active = $(".border-bot.mess.active").data("partnerid");
        if (result.length < 10)
            $scope.shared.selectedDialog.finishPages = true;
        result.forEach(function (mess) {
            $scope.shared.selectedDialog.Replies.unshift({
                DialogId: $scope.shared.selectedDialog.Id,
                Message: mess.Message,
                Unread: false,
                UtcDateTime: mess.UtcDateTime,
                UserId: $scope.shared.userId
            });
            //var mst = $(messTemplate).clone();
            //if (mess.UserId.toString() !== active)
            //    mst.removeClass("pull-right second").addClass("first");
            //smiles.forEach(function(elem) {
            //    mess.Message = mess.Message.replace(elem.sh, "<img src = '" + elem.src + "'/>");
        });
        //setTimeout(function () {
        //    $(".custom-scroll1").mCustomScrollbar({
        //        enable: true,
        //    });
        //});
        $scope.shared.flagMoreMessages = true;
        $scope.$apply();
        //if (mess.Message.indexOf("<a href") > -1)
        //    mess.Message = "<img src = '/Images/file.png'/>" + mess.Message;
        //    $(mst)
        //        .attr("data-messageid", mess.Id)
        //        .find(".message-text")
        //        .append(mess.Message)
        //        .end()
        //        .find("img.media-object")
        //        .attr('src', mess.ProfilePicture.replace(/&amp;/g, '&'))
        //        .end()
        //        .find(".chat-date")
        //        .text(moment(mess.UtcDateTime).format("dddd, MMMM, D, YYYY, HH:mm:ss"));
        //    $(".dlg[data-clientid='" + active + "']").prepend(mst);
        //});
        //flagMoreMessages = true;
    }
    function initChatUsersList(scope, api) {
        console.log("DASHBOARD PANEL");
        if (scope.shared.dialogs) return;
        api.ajax("/api/ChatApi/GetAllDialogs", 'get', null, function (res) {
            console.log(res);
            scope.shared.dialogs = res;
            var partners = res.select(function (x) { return x.Dialog.Id }).join(",");
            $scope.shared.hub = initHub({
                partners: partners,
                getMyActiveUsers: getMyActiveUsers,
                someClietnChangedState: someClietnChangedState,
                receiveMessage: receiveMessage,
                inviteToVideoChatToVideo: inviteToVideoChatToVideo,
                replyMoreMessages: replyMoreMessages
            });
        });
    }
    $scope.shared.getLastMessage = function (dialog) {
        if (!dialog.Replies || dialog.Replies.length == 0)
            return;
        var mess = dialog.Replies[0].Message;
        if (mess.indexOf("href") !== -1)
            return resource.match('received_a_file');
        var sub = mess.substring(0, 15);
        if (sub.indexOf("<img") !== -1)
            return resource.match('smile');
        return sub;
    }
    $scope.shared.clickToDialog2 = function (dialog) {
        console.log("clicked");
        $location.path("/chat/" + dialog.Dialog.Id);
    }
    $scope.shared.checkIfExistUnred = function (dialog) {

        return dialog.Replies.firstOrNull(function (x) { return x.Unread })
    }
    $scope.shared.getUnreadMessagesDialogs = function () {
        if (!$scope.shared.dialogs) return [];
        return $scope.shared.dialogs.filter(function (x) {
            return x.Replies.find(function (f) { return f.Unread })
        });
    }
    $scope.shared.checkIfExistUnredAll = function () {
        if (!$scope.shared.dialogs) return 0;
        var ss = $scope.shared.dialogs.filter(function (x) {
            return x.Replies.find(function (f) { return f.Unread })
        }).length;
        return ss;
        //return dialog.Replies.firstOrNull(function (x) { return x.Unread })
    }
    $scope.shared.getUnreadMessages = function (dialog) {
        return dialog.Replies.where(function (x) { return x.Unread }).length;
    }
    initChatUsersList($scope, api);
}