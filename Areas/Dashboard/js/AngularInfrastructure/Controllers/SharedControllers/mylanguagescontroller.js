﻿mylanguagescontroller.$inject = ["$scope", "$route", "$templateCache", "$http", "notifications"];
function mylanguagescontroller($scope, $route, $templateCache, $http, notifications) {
    var currentPageTemplate = $route.current.templateUrl;
    $templateCache.remove(currentPageTemplate);
    $scope.ilearn = langData.ilearn ? langData.ilearn : [];
    $scope.ispeak = langData.ispeak ? langData.ispeak : [];
    $scope.saveLanguages = function () {
        var il = $scope.ilearn.map(function (elem) {
            return "cap_" + elem;
        }).join('|');
        if (il)
            il += "|";
        var is = $scope.ispeak.map(function (elem) {
            return "cap_" + elem;
        }).join('|');
        if (is)
            is += "|";
        $http({ method: 'post', url: '/en/NewMyAccount/SaveLanguages', params: { 'il': il, 'is': is } }).success(function () {
            $scope.shared.changeMyILearnLanguages = !$scope.shared.changeMyILearnLanguages;
            notifications.show('success', "Saved");
        })
    }
    console.log("mylanguagescontroller");
    jQuery('.page-preloader-wrap-dashboard').hide();
}