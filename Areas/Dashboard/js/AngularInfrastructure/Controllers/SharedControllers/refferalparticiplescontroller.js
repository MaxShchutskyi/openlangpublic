﻿refferalparticiplescontroller.$inject = ["$scope", "notifications", "api",'$compile'];
function refferalparticiplescontroller($scope, notifications, api,$compile) {
    var mystds = $('#mystudents');
    $scope.dtOptions = shared.getTableProperties(mystds);
    $scope.isLoading = false;
    $scope.sendEmail = function () {
        debugger;
        var text = document.getElementById('friend_text').value;
        var model = { Email: $scope.friendEmail, Message: text };
        $scope.isLoading = true;
        api.ajax('/api/ReferralProgramApi/SendEmail',
            'post',
            JSON.stringify(model),
            function(res) {
                res ? notifications.show('success', "Your message was sent") : notifications.show('error', "Your message was not sent");
                $scope.isLoading = false;
                $('#share_modal').modal('hide');
                $scope.$digest();
            });
    }
    $scope.openWindow = function () {
        setTimeout(function () {
            var id = document.getElementById("modal-refferal");
            $compile(id)($scope);
            $scope.$apply();
        }, 2000)
        //$("#modal-refferal").modal();
    }
    $scope.sendSms = function () {
      var text = document.getElementById('friend_sms_text').value;
      console.log($scope.friendPhone.indexOf("+"));
      if ($scope.friendPhone.indexOf("+") === -1) {
        console.log("Error format");
          alert("Please enter phone number by forat:+79818789343");
          return;
        }
        var model = { Phone: $scope.friendPhone, Message: text };
        $scope.isLoading = true;
        api.ajax('/api/ReferralProgramApi/SendSms',
            'post',
            JSON.stringify(model),
            function (res) {
                res ? notifications.show('success', "Your message was sent") : notifications.show('error', "Your message was not sent");
                $scope.isLoading = false;
                $('#share_modal').modal('hide');
                $scope.$digest();
            });
        console.log(text);
    }
    jQuery('.page-preloader-wrap-dashboard').hide();
}