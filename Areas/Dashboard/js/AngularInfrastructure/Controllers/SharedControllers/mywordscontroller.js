﻿mywordscontroller.$inject = ["$scope", "api", "$timeout", "$http", "notifications", "$filter", "$animate", "recorderService"];
function mywordscontroller($scope, api, $timeout, $http, notifications, $filter, $animate, recorderService) {
    var wordlang = $("#wordlang");
    if (!$scope.shared.selectedWordsPage)
        $scope.shared.selectedWordsPage = 0;
    if (!$scope.shared.ilearn) {
        api.ajax('/api/Languages/GetILearn', 'get', null, function (res) {
            $scope.shared.ilearn = res;
            $scope.$apply();
        });
    }
    $animate.enabled(false, $("#no-animate-words"));
    var local;
    $scope.shared.selectWord = function (word, event) {
        local = $.extend({}, word); 
        $scope.shared.selectedWord = local;
        //$timeout(function () {
        //    wordlang.select2().val(local.language);
        //})
        $(event.target).parents(".my-words-media").addClass('active').siblings().removeClass('active');
    }
    $scope.changePage = function (state) {
        $scope.shared.selectedWordsPage = state ? $scope.shared.selectedWordsPage + 1 : $scope.shared.selectedWordsPage -1;
        getWords();
        $scope.shared.selectedWords = local =  null;
    }
    $scope.shared.removeWordMessage = function () {
        api.ajax('/api/MyWords/RemoveMessage', 'post', JSON.stringify(local), function (res) {
            var el = getElem($scope.mywords, res)
            el.audioPath = local.audioPath = null;
            $scope.$apply();
        })
    }
    $scope.$watch('shared.selectedWord.language', function (newVal, oldVal) {
        if (newVal === oldVal || newVal == null) return;
        $timeout(function () {
            wordlang.select2().val(newVal);
        })
        //console.log(newVal);
        //$timeout(function () {
        //    wordlang.select2('val', newVal);
        //})
    });
    $scope.shared.addNewWord = function () {
        if (!$scope.shared.ilearn.length) {
            notifications.show('error', resource.match('you_have_not_lang'));
            return;
        }
        local = { language: $scope.shared.ilearn[0].key, image: '/Images/assig-img2.png', id:-1 };
        $scope.shared.selectedWord = local;
    };
    saveMessagePath = "/en/UploadImages/UploadWordAudioMessageFile";
    var callback = function (res) {
        $scope.shared.selectedWord.audioPath = res.path;
        var el = getElem($scope.mywords, local)
        el.audioPath = res.path;
        notifications.show('success', "Loaded");
        $timeout(function () {
            document.getElementById('word' + $scope.shared.selectedWord.id).load();
        });
    };
    $scope.shared.recordWordAudio = function (res) {
        recorderService.start(saveMessagePath, callback);
        $scope.shared.isRecording = true;
        $("#timer1").timer({ format: '%m ' + resource.match('minutes') + ' %S' + resource.match('minutes') + '' });
    }
    $scope.shared.stopRecordAudio = function () {
        recorderService.stop(function (data) { data.append('id', $scope.shared.selectedWord.id); return data });
        $scope.shared.isRecording = false;
        $("#timer1").timer('remove');
    }
    getWords();
    $scope.playAudio = function (word) {
        document.getElementById('word' + word.id).play();
    }
    $scope.shared.saveWordsChanges = function () {
        api.ajax('/api/MyWords/SaveChanges', 'post', JSON.stringify(local), function (res) {
            if (local.id == -1){
                $scope.mywords.unshift(res);
                $scope.shared.selectedWord = local = res;
                $scope.$apply();
                return;
            }
            var el = getElem($scope.mywords, res)
            el.language = res.language; el.word = res.word; el.description = res.description;
            $scope.shared.selectedWord = local = null;
            notifications.show('success', resource.match('saved'));
            $scope.$apply();
        })
    }
    $scope.shared.changeWordPhoto = function () {
        var files = document.getElementById('loadWordImage').files;
        var res = shared.validateImages(files);
        if (res !== "OK") {
            notifications.show('error', res);
            return;
        }
        var data = new FormData();
        data.append(files[0].name, files[0]);
        data.append('id', local.id);
        $http.post('/en/UploadImages/UploadWordImage', data, {
            withCredentials: true,
            headers: { 'Content-Type': undefined },
            transformRequest: angular.identity
        }).success(function (res) {
            if (!res.path)
                return;
            var el = getElem($scope.mywords, local)
            el.image = res.path;
            $scope.shared.selectedWord.image = res.path;
            local.image = res.path;
            notifications.show('success', resource.match('image_uploaded'));
        });
    }
    $scope.$watch('filter', function (newVal, oldVal) {
        if (newVal == oldVal) return;
        $scope.shared.selectedWordsPage = 0;
        getWords();
    })
    function getWords() {
        api.ajax('/api/MyWords/GetMyWords?page=' + $scope.shared.selectedWordsPage + "&filter=" + $scope.filter, 'get', null, function (res) {
            $scope.mywords = res;
            $scope.$apply();
        });
    }
    function getElem(from,res) {
        var angelem = $filter('filter')(from, { id: res.id }, true)[0];
        var index = from.indexOf(angelem);
        var elem = from[index];
        return elem;
    }
    jQuery('.page-preloader-wrap-dashboard').hide();
}