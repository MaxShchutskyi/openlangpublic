﻿mystudentscompanycontroller.$inject = ["$scope","api",'$timeout'];
function mystudentscompanycontroller($scope, api, $timeout) {
    var mystds = $('#mystudents1');
    $scope.dtOptions = shared.getTableProperties(mystds);
  $scope.getStudents = function() {
    api.ajax("/api/StudentsOfCompany/TableStudents",
      'get',
      null,
      function(res) {
        $scope.tableOfStudents = res;
        console.log(res);
        $timeout(function() {
            if ($scope.dtInstance.rerender)
              $scope.dtInstance.rerender();
          },
          2000);
      });
  }
  $scope.getDate = function (date) {
        var dt = moment.utc(date).toDate();
      return moment(dt).local().format('DD/MM/YYYY HH:mm');
    }
    $scope.findUser = function () {
      api.ajax("/api/StudentsOfCompany/FindByEmail", 'get', { email: $scope.findingEmail }, function (res) {
        if (!res) {
          $scope.students = [];
          $scope.$digest();
          return;
        };
        $scope.students = res;
        $scope.$digest();
        console.log(res);
      });
      console.log($scope.findingEmail);
    }
  $scope.bind = function(student) {
    var model = { "studentId": student.Id }
    console.log(model);
    api.ajax("/api/StudentsOfCompany/BindTo",
      'post',
      JSON.stringify(student.Id),
      function(res) {
        var st = $scope.students.find(function(x) { return x.Id === student.Id });
        st.wasbindled = true;
        $scope.getStudents();
        $scope.$digest();
      });
    };
  $scope.inviteByEmail = function () {
    $scope.invited = true;
    api.ajax("/api/StudentsOfCompany/InviteByEmail", 'post', JSON.stringify($scope.findingEmail), function (res) {
      $timeout(function() {
        $scope.invited = false;
      }, 3000);
    });
  }
  $scope.getStudents();
    //mystds.DataTable(shared.getTableProperties(mystds));
    //console.log("mystudentscompanycontroller");
    jQuery('.page-preloader-wrap-dashboard').hide();
}