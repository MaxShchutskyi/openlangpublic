﻿mytransferscompanycontroller.$inject = ["$scope", "api", '$timeout'];
function mytransferscompanycontroller($scope, api, $timeout) {
    var mystds = $('#mystudents1');
    $scope.dtOptions = shared.getTableProperties(mystds);
    api.ajax("/api/CompanyDashboard/GetTransfers", 'get', null, function (res) {
        $scope.tableOfTransfers = res;
        console.log(res);
      $scope.$digest();
        $timeout(function () {
          updateTable();
        }, 0);
    });
    $scope.getDate = function (date) {
        var dt = moment.utc(date).toDate();
        return moment(dt).local().format('DD/MM/YYYY HH:mm')
    }
    jQuery('.page-preloader-wrap-dashboard').hide();
    //$('.table-item .count').each(function () {
    //    if ($(this).text() == '0') {
    //        $(this).addClass('LeftEmpty')
    //    }
    //})
  function updateTable() {
    $(document).ready(function () {
      $("#mystudents1 tr.table-upper-row").show();
      $("#mystudents1 tr.table-lower-row").hide();
      $("#mystudents1 tr.table-upper-row").click(function () {
        $("#mystudents1 tr.table-lower-row").hide();
        if ($(this).hasClass('active')) {
          $(this).removeClass('active');
        } else {
            $("#mystudents1 tr.table-upper-row").removeClass("active")
            $(this).addClass('active');
            $(this).parent().children('.table-lower-row').show();
        }
      });
    });
  }
}