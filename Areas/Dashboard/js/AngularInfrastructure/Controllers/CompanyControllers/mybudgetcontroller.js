﻿mybudgetcontroller.$inject = ["$scope", "api", '$compile', 'DTOptionsBuilder', '$timeout', 'notifications'];
function mybudgetcontroller($scope, api, $compile, DTOptionsBuilder, $timeout, notifications) {
    //$(".js-select2-lessons").select2({
    //    minimumResultsForSearch: Infinity
    //})
  var mystds = $('#mystudents');
  $scope.dtOptions = shared.getTableProperties(mystds);
  if (!$scope.selectedChart)
    $scope.selectedChart = 'monthly';
  var reg = /^\d+$/;
  $scope.students = [];
  Chart.defaults.global.legend.display = false;
  var data = {
    //labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
      {
        backgroundColor: 'rgba(153, 211, 50, 0.2)',
        pointBackgroundColor: 'rgb(153, 211, 50)',
        pointBorderColor: 'rgb(153, 211, 50)',
        pointHoverBackgroundColor: 'rgb(153, 211, 50)',
        pointHoverBorderColor: 'rgb(153, 211, 50)',
        pointHighlightStroke: "rgba(153, 211, 50,1)",
        //data: [65, 59, 80, 81, 56, 55, 40]
      },
    ]
  };

  function getAvailablePackages() {
    api.ajax("/api/PackagesOfLessons/GetAvailablePackages", 'get', null, function (res) {
      $scope.availablePackages = res;
      //if ($scope.availablePackages.length > 0)
      //  $scope.selectedPackage = $scope.availablePackages[0];
      console.log(res);
      //$scope.$apply();
    });
  }

  function updateTable() {
    api.ajax("/api/StudentsOfCompany/TableStudents", 'get', null, function (res) {
      $scope.tableOfStudents = res;
      $timeout(function () {
        if ($scope.dtInstance.rerender)
          $scope.dtInstance.rerender();
      }, 2000);
    });
  }
  $scope.getDate = function (date) {
    var dt = moment.utc(date).toDate();
    return moment(dt).local().format('DD/MM/YYYY HH:mm');
  }
  function updateChart() {
    api.ajax("/api/CompanyDashboard/GetStats?chat=" + $scope.selectedChart, 'get', null, function (res) {
      data.labels = res.labels;
      data.datasets[0].data = res.data;
      drawChart();
    });
  };
  $scope.topUpBalance = function (id) {
    if ($scope.students.firstOrNull(function (elem) { return (elem.Id === id) })) {
      $("#inputBalanceOfStudent").modal("show");
      return;
    }
    var std = $scope.allStudents.firstOrNull(function (elem) { return (elem.Id === id) });
    $scope.students.push(std);
    $("#inputBalanceOfStudent").modal("show");
  }
  $scope.checkChart = function (chart) {
    if (chart)
      $scope.selectedChart = chart;
    updateChart();
  }
  $scope.data = data;
  function drawChart() {
    if ($scope.chart)
      $scope.chart.destroy();
    var ctx = document.getElementById("myChart").getContext("2d");
    ctx.clearRect(0, 0, ctx.width, ctx.height);
    $scope.chart = new Chart(ctx, {
      type: "line",
      data: data,
    });
  };
  $scope.clickOnCancel = function () {
  }
  updateChart();

  //var props = shared.getTableProperties(mystds);
  //props.createdRow = function (rw) {
  //    $compile(angular.element(rw).contents())($scope);
  //},
  //mystds.dataTable(props);
  api.ajax("/api/CompanyDashboard/GetShortListOfStudents", 'get', null, function (res) {
    console.log(res);
    $scope.allStudents = res;
    $scope.$apply();
  });
  $scope.checkLeftLessons = function () {
    if (!$scope.availablePackages) return 0;
    if (!reg.test($scope.amount)) return $scope.selectedPackage.LeftLessons;
    return $scope.selectedPackage.LeftLessons - $scope.students.length * (isNaN(parseInt($scope.amount)) ? 0 : $scope.amount);
  };
  $scope.addNewStudentToSelectedlesson = function (student) {
    $scope.students.push(student);
  }
  $scope.deleteStudentFromList = function (student, index) {
    $scope.students.splice(index, 1);
  }
  $scope.getMinus = function () {
    var val = $scope.students.length;
    var amnt = parseInt($scope.amount ? $scope.amount : "0");
    return -val * amnt;
  };
  $scope.checkCorrectAmount = function () {
    if (!$scope.amount || !reg.test($scope.amount)) return true;
    if ($scope.selectedPackage.LeftLessons -
      $scope.students.length * (isNaN(parseInt($scope.amount)) ? 0 : $scope.amount) <
      0) return true;
    return false;
  }
  $scope.getStringNamePackage = function (package) {
    var str = "";
    switch (package.Type) {
    case 0:
        str += "Basic";break;
      case 2:
        str += "Premium";
        break;
    }
    str += " " + package.CountLessons + " (" + package.LeftLessons + ")";
    return str;
  }
  $scope.$watch('selectedPackageId',
    function(newValue, oldValue) {
      if (!newValue || newValue === oldValue) return;
      $scope.selectedPackage = $scope.availablePackages.find(function (package) { return package.Id.toString() === newValue });
    });
  $scope.topUp = function () {
    var packageId = $scope.selectedPackage.Id;
    var studentsIds = $scope.students.select(function (x) { return x.Id });
    console.log(studentsIds);
    $("#inputBalanceOfStudent").modal("hide");
    api.ajax('/api/PackagesOfLessons/SplitPackage',
      'post',
      JSON.stringify({ 'packageId': packageId, 'StudentIds': studentsIds, 'Count': $scope.amount }),
      function (res) {
        notifications.show("success", "Done!");
        $scope.checkChart();
        updateTable();
        $scope.$apply();
      });
    //var getMinor = $scope.getMinus();
    //if (!getMinor) {
    //    alert(resource.match('incorrect_data'));
    //    return;
    //}
    //if ((-1 * getMinor) > $scope.shared.money * $scope.shared.globalDelta) {
    //    alert(resource.match('insufficient_funds'));
    //    return;
    //}
    //var amount = ($scope.amount / $scope.shared.globalDelta).toFixed(2);
    //var ids = $scope.students.map(function (elem) { return elem.Id});
    //api.ajax('/api/CompanyDashboard/TopUpBalanceOfStudents', 'post', JSON.stringify({ 'amount': amount, 'students': ids }), function (res) {
    //    if (res == -1) {
    //        $scope.succeeded = false;
    //        $scope.$digest();
    //        $("#inputBalanceOfStudent").modal("hide");
    //        $("#replenish-balance-student-3").modal("show");
    //        return;
    //    }
    //    $scope.succeeded = true;
    //    $scope.shared.money = res;
    //    $scope.checkChart();
    //    updateTable();
    //    $scope.$apply();
    //    $("#inputBalanceOfStudent").modal("hide");
    //    $("#replenish-balance-student-3").modal("show");
    //});
  }
  updateTable();
  getAvailablePackages();
  jQuery('.page-preloader-wrap-dashboard').hide();
}