﻿mylessonscompanycontroller.$inject = ["$scope","api"];
function mylessonscompanycontroller($scope,api) {
    var cal = $("#mylessonscalendar");
    var event = null;
    var newLang = $("#lessonlang");
    var newLevel = $("#levellesson");
    setInterval(function () {
        $(".inLocalTimezones").text(moment().format("HH:mm:ss"));
    }, 1000);
    if (!$scope.shared.shortCompanyStudentsList) {
        api.ajax("/api/CompanyDashboard/GetShortListOfStudents", "get", null, function (res) {
            console.log(res);
            $scope.shared.shortCompanyStudentsList = res;
            $scope.$apply();
        })
    }
    if (!$scope.shared.selectedStudentFromList)
        $scope.shared.selectedStudentFromList = "own";
    $scope.$watch("shared.selectedStudentFromList", function (newVal, oldVal) {
        if (newVal === oldVal || oldVal === undefined || newVal === undefined)
            return;
        if (newVal === 'own') {
            var path = '/api/CompanyDashboard/GetCalendarForAllStudents';
            //cal.find('.previous-week').css('display', 'inline');
            calendar.update(path, $scope.shared.userId);
        }
        else {
            var path = "/api/CompanyDashboard/GetLessonsOfStudent?studentId=" + newVal;
            //cal.find('.previous-week').css('display', 'none');
            //console.log($scope.shared.shortTeachersList.firstOrNull(function (elem) { return elem.Id == newVal }));
            //$scope.shared.student = $scope.shared.shortCompanyStudentsList.firstOrNull(function (elem) { return elem.Id === newVal }).teach;
            calendar.update(path, newVal);
        }
        $scope.shared.selectedLesson = {};
    });
    var calendar = new Calendar("#mylessonscalendar", "/api/CompanyDashboard/GetCalendarForAllStudents", { previousClick: calendarHelper.prevClick, eventClick: clickToTvent });
    calendarHelper.setCalendar(calendar);
    function clickToTvent(calEvent, jsEvent, view) {
        if (event && event.className[event.className.length - 1] === 'selected') {
            event.className.splice(-1, 1);
            cal.fullCalendar('updateEvent', event)
            if (event.id === calEvent.id) {
                $scope.shared.selectedLesson.isTrue = false;
                $scope.$apply();
                return;
            }
        }
        calEvent.className.push('selected');
        event = calEvent;
        //console.log($scope.shared.money - $scope.shared.currentPrices[$scope.shared.selectedLesson.type]);
        cal.fullCalendar('updateEvent', event)
        $scope.shared.selectedLesson.id = calEvent.id;
        $scope.shared.selectedLesson.date = calEvent._start.format("ll")
        $scope.shared.selectedLesson.time = calEvent._start.format("HH:mm")
        $scope.shared.selectedLesson.isConfirmed = calEvent.IsConfirmed;
        $scope.shared.selectedLesson.type = calEvent.type;
        $scope.shared.selectedLesson.language = calEvent.language;
        $scope.shared.selectedLesson.level = calEvent.level ? calEvent.level : "Beginner";
        $scope.shared.selectedLesson.topic = calEvent.topic;
        $scope.shared.selectedLesson.teacher = calEvent.teacher;
        $scope.shared.selectedLesson.students = calEvent.students;
        $scope.shared.selectedLesson.findStudent = false;
        $scope.shared.selectedLesson.isTrue = true;
        //if ($scope.shared.selectedLesson.students && $scope.shared.selectedLesson.students.firstOrNull(function (elem) { return elem.Id === $scope.shared.userId }))
        //    $scope.shared.disableSaving = true;
        //else
        //    $scope.shared.disableSaving = false;
        //if ($scope.shared.selectedLesson.type) {
        //    newLevel.select2().prop("disabled", true);
        //    newLang.select2().prop("disabled", true);
        //}
        //else {
        //    newLevel.select2().prop("disabled", false);
        //    newLang.select2().prop("disabled", false);
        //}

        console.log($scope.shared.selectedLesson);
        $scope.$apply();
    }
    calendar.render($scope.shared.userId);
    $scope.$watch('shared.selectedLesson.language', function (newVal, oldVal) {
        if (newVal === oldVal || newVal === null) return;
        newLang.select2().val(newVal);
    });
    $scope.$watch('shared.selectedLesson.level', function (newVal, oldVal) {
        if (newVal === oldVal || newVal === null) return;
        newLevel.select2().val(newVal);
    });
    jQuery('.page-preloader-wrap-dashboard').hide();
}