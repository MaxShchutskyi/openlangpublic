﻿groupscontroller.$inject = ["$scope", "api", "$http", "notifications", "$timeout", '$filter', 'recorderService'];
function groupscontroller($scope, api, $http, notifications, $timeout, $filter, recorderService) {

    //call popup and check current border color
    $(document).ready(function () {
    $('.group-item .changeColor').click(function () {
        var currentBorder = $(this).closest('.group-item').css('border-left-color');
        $('.changeColor-form ul li').each(function () {
            if ($(this).children('a').css('background-color') == currentBorder) {
                $('.changeColor-form ul li').removeClass('current');
                $(this).addClass('current');
            }
        })

        if ($(this).next('.changeColor-form').is(":visible")) {
            $(this).next('.changeColor-form').hide();
        } else {
            $('.changeColor-form').show();
            $(this).parent().append($('.changeColor-form'));
        }

    })
    })
  $scope.models = {};
  api.ajax('/api/Cards/GetAllCards', 'get', null, function (res) {
    $scope.cards = res;
    //console.log(res);
    $scope.$digest();
  });
  api.ajax('/api/Group/GetGroups', 'get', null, function (res) {
    $scope.groups = res;
    console.log(res);
    $scope.$digest();
  });
  $scope.getAllLevels = function (cards) {
    var levels = cards.map(function (r) { return r.Level });
    //console.log(levels);
    return levels;
  }
  $scope.addNewGroup = function() {
    if (!$scope.models.groupname) {
      alert("Incorrect group name");
      return;
    }
    var group = { name: $scope.models.groupname };
    console.log(group);
    api.ajax('/api/Group/AddNewGroup',
      'post',
      JSON.stringify( group ),
      function (res) {
        //console.log(res);
        $scope.groups.push(res);
        $scope.models.groupname = "";
        $scope.$digest();
      });
  }
  $scope.clickOnGroup = function(event) {
    $(event.currentTarget).siblings('.combo').css('display', 'block').end().parent().siblings().find('.combo')
      .css('display', 'none');
  }
  $scope.deleteCard = function(group, card) {
    group.Cards = group.Cards.filter(function (x) { return x.Id !== card.Id });
    var model = { groupId: group.Id, cardId: card.Id };
    api.ajax('/api/group/DeleteCardFromGroup', 'post', JSON.stringify(model), function(res) {});
  }
  $(document).mousemove(function (e) {
      var X = e.pageX; // положения по оси X
      var Y = e.pageY; // положения по оси Y
      $('.GiveMeCourd').val($('.drag-container-active').offset());
  });
  $scope.dragStart = function(el, vs) {
  		
  }
  $scope.dragEnd = function (el, vs) {
    
  }
  $scope.dropAccept = function ($dragData) {

  }
  $scope.dropStartEnter = function ($event,$dragData) {

  }
  $scope.dropDragOver = function ($event, $dragData) {

  }
  $scope.onDragLeave = function ($event, $dragData) {

  }
  $scope.onDrop = function ($event, $dragData, group) {
    console.log($dragData);
    if (group.Cards.find(function (r) { return r.Id === $dragData.Id })) return;
    group.Cards.push($dragData);
    var model = { groupId: group.Id, cardId: $dragData.Id };
    api.ajax('/api/group/AddCardToroup', 'post', JSON.stringify(model), function(res){});
  //console.log($dragData);

}
  jQuery('.page-preloader-wrap-dashboard').hide();
}