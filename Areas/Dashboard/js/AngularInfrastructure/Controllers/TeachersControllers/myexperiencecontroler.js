﻿myexperiencecontroler.$inject = ["$scope", "notifications", "api"];
function myexperiencecontroler($scope, notifications, api) {
    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false
    };
    $scope.educations = [];
    $scope.addItem = function (array, elem) {
        var item = { Description: "", Title: "", PeriodFrom: "2016", PeriodTo: "2016", Id: -1, Deleted: false };
        array.push(item);
    }
    $scope.deleteItem = function (array,elem, index) {
        if (elem.Id == -1)
            array.splice(index, 1);
        else
            elem.Deleted = true;
    }
    $scope.saveChanges = function (array, method) {
        api.ajax("/api/MyExperience/" + method, 'post', JSON.stringify(array), function (res) {
            if (res.hasOwnProperty('error')){
                notifications.show('error', res.error);
                return;
            }
            array = res;
            $scope.$digest();
            notifications.show('success', "Saved");
        })
    }
    api.ajax("/api/MyExperience/GetMyExperiance", 'get', null, function (res) {
        console.log(res);
        $scope.educations = res.educations;
        $scope.expiriences = res.expiriences;
        $scope.certifications = res.certifications;
        $scope.$digest();
    })
    console.log("myexperiencecontroler");
    jQuery('.page-preloader-wrap-dashboard').hide();
}