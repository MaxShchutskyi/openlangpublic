﻿aboutmecontroller.$inject = ["$scope", "notifications","api", "$http"];
function aboutmecontroller($scope, notifications, api, $http) {
    $scope.item = { videoSrc: '' };
    $scope.uploadVideo = function () {
        var file = document.getElementById('loadMyVideo').files[0];
        if (file.type != "video/mp4") {
            notifications.show('error', "You should load only in MP4 format");
            return;
        }
        if (file.size > 41943040) {
            notifications.show('error', "You should load file not more 40MB");
            return;
        }
        var data = new FormData();
        data.append(file.name, file);
        var progressBar = $('#progressbar');
        progressBar.css("display", "block")
        shared.loadVideoFile(data, '/en/LoadFiles/LoadVideoForTheTeacher', progressBar, function (res) {
            $scope.item.videoSrc = res.link;
            $scope.$digest();
        })
    }
    $scope.$watch('item.videoSrc', function (newVal) {
        if (newVal.indexOf("http") === -1)
            document.getElementById('vdvideo').load();
        else if (newVal.indexOf("youtube") !== -1 && newVal.indexOf("embed") === -1) {
            var v = shared.gup('v', newVal);
            var someval = "https://www.youtube.com/embed/" + v;
            $scope.item.videoSrc = someval;
        }
        else if (newVal.indexOf("vimeo") !== -1 && newVal.indexOf("player") === -1) {
            var vl = newVal.split('/');
            var le = vl[vl.length - 1];
            var someval = "https://player.vimeo.com/video/" + le;
            $scope.item.videoSrc = someval;
        }
    })
    $scope.saveChanges = function () {
        console.log($scope.item.teacherLevels = JSON.stringify($scope.item.levels));
        api.ajax("/api/AboutMe/UpdateAboutMeInfo", 'post', JSON.stringify($scope.item), function (res) {
            if (res.hasOwnProperty('error'))
                notifications.show('error', res.error);
            else
                notifications.show('success', "Saved");
        })
    };
    api.ajax("/api/AboutMe/GetAboutMeInfo", 'get', null, function (res) {
        console.log(res);
        $scope.item = res;
        $scope.item.levels = {};
        $scope.item.levels.Beginner = false;
        $scope.item.levels.UpperBeginner = false;
        $scope.item.levels.Intermediate = false;
        $scope.item.levels.UpperIntermediate = false;
        $scope.item.levels.Advanced = false;
        $scope.item.levels.UpperAdvanced = false;
        var lt = JSON.parse(res.TeacherLevels);
        for (key in lt) {
            $scope.item.levels[key] = lt[key];
        }
        console.log($scope.levels);
        $scope.$digest();
    })
    jQuery('.page-preloader-wrap-dashboard').hide();
}