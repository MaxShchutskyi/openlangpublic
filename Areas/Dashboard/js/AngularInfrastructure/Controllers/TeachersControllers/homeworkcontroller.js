﻿homeworkcontroller.$inject = ["$scope", "api", "$http", "notifications", "$timeout", '$filter', 'recorderService'];
function homeworkcontroller($scope, api, $http, notifications, $timeout, $filter, recorderService) {
  api.ajax('/api/Homework/GetTablesHomework', 'get', null, function (res) {
    console.log(res.length + "   -  Homeworks");
    $scope.homeworks = res;
    $scope.$digest();
  });
  jQuery('.page-preloader-wrap-dashboard').hide();
}