﻿mylessonscontroller.$inject = ["$scope", "api", "notifications", "$timeout", "$http"];
function mylessonscontroller($scope, api, notifications, $timeout, $http) {
    var event;

    setInterval(function () {
        $(".inLocalTimezones").text(moment().format("HH:mm:ss"));
    }, 1000);
    //jQuery(window).load(function () {
    //    var localTime = new Date().getTimezoneOffset() / 60;

    //    jQuery("#mylessonscalendar .fc-center").html(jQuery("#mylessonscalendar .fc-left").html());
    //    if ($(window).width() < 768) {
    //        $('#mylessonscalendar .fc-axis.fc-widget-header')
    //        .append('<span class="local-time">' + localTime + '</span>')
    //    } else {
    //        $('#mylessonscalendar .fc-axis.fc-widget-header')
    //    .append('<span class="local-time">' + localTime + '</span>');
    //    }
    //});
    var cal = $("#mylessonscalendar");
    //jQuery("#mylessonscalendar").fullCalendar({
    //    defaultView: $(window).width() < 768 ? 'agendaDay' : 'agendaWeek',
    //    selectHelper: false,
    //    firstDay: 0,
    //    eventLimit: true, // allow "more" link when too many events
    //    allDaySlot: false,
    //    selectable: true,
    //    slotDuration: '01:00:00',
    //    slotLabelFormat: "HH:mm",
    //    weekNumbers: true,
    //    maxTime: '24:00:00',
    //    timezone: 'local',
    //    ignoreTimezone: false,
    //    editable: false,
    //})
    if (!$scope.shared.ilearn) {
        api.ajax('/api/Languages/GetILearn', 'get', null, function (res) {
            $scope.shared.ilearn = res;
            $scope.$apply();
        });
    }
    function clickToTvent(calEvent, jsEvent, view) {
        calendar.checkMobileShowingWidget();
        if (event && event.className[event.className.length - 1] == 'selected') {
            event.className.splice(-1, 1);
            cal.fullCalendar('updateEvent', event)
            if (event.id === calEvent.id) {
                $scope.shared.selectedLesson.isTrue = false;
                $scope.$apply();
                return;
            }
        }
        calEvent.className.push('selected');
        event = calEvent;
        cal.fullCalendar('updateEvent', event)

        $scope.shared.selectedLesson.id = calEvent.id;
        $scope.shared.selectedLesson.date = calEvent._start.format("ll")
        $scope.shared.selectedLesson.time = calEvent._start.format("HH:mm")
        $scope.shared.selectedLesson.isConfirmed = calEvent.IsConfirmed;
        $scope.shared.selectedLesson.type = calEvent.type;
        $scope.shared.selectedLesson.topic = calEvent.topic;
        $scope.shared.selectedLesson.language = calEvent.language ? calEvent.language : $scope.shared.ilearn[0].key;
        $scope.shared.selectedLesson.level = calEvent.level ? calEvent.level : "Beginner";
        $scope.shared.selectedLesson.students = calEvent.students.slice();
        $scope.shared.selectedLesson.findStudent = false;
        $scope.shared.selectedLesson.isTrue = true;
        $scope.$apply();
    }
    var calendar = new Calendar("#mylessonscalendar", "/api/TeacherSchedule/GetTeacherScheduleFullDetails", { eventClick: clickToTvent, previousClick: calendarHelper.prevClick, selectCell: selectCell, renderEvents: renderEvents, eventDrop: eventDrop })
    calendarHelper.setCalendar(calendar);

    if (!$scope.shared.myCurrentStudents) {
        updateListOfStudents();
    }
    function updateListOfStudents() {
        api.ajax("/api/MyStudents/GetMyStudents", 'get', null, function (res) {
            //console.log("!!!!!!!!!!!!!!!!");
            console.log(res);
            $scope.shared.myCurrentStudents = res;
        })
    }
    var newLang = $("#lessonlang");
    var newLevel = $("#levellesson");
    $scope.$watch('shared.selectedLesson.language', function (newVal, oldVal) {
        if (newVal === oldVal || newVal == null) return;
        $timeout(function () {
            newLang.select2().val(newVal);
        })
    });
    $scope.$watch('shared.selectedLesson.type', function (newVal, oldVal) {
        if (newVal === oldVal || newVal == null) return;
        var ids = $scope.shared.myCurrentStudents.where(function (x) { return x.credits.firstOrNull(function (t) { return t.Type == newVal }) }).select(function (x) { return x.Id });
        var stds = $scope.shared.selectedLesson.students.where(function (x) { return !ids.firstOrNull(function (f) { return f == x.Id }) });
        stds.forEach(function (el) {
            console.log(el);
            if (el.active && el.Added) {
                $scope.shared.selectedLesson.students.removeBy("Id", el.Id);
                return;
            }
            if (el.active && !el.Added) {
                el.active = false;
                return;
            }
        });
    });

    $scope.$watch('shared.selectedLesson.level', function (newVal, oldVal) {
        if (newVal === oldVal || newVal == null) return;
        $timeout(function () {
            newLevel.select2().val(newVal);
        })
    });
    $scope.copyCurrentWeek = function () {
        if ($scope.shared.isButtonLoading)
            return;
        var curWeek = calendar.week;
        $scope.shared.isButtonLoading = true;
        //$scope.$digest();
        api.ajax("/api/SchedulerOfStudents/CopyCurrentWeek", 'post', JSON.stringify(curWeek), function (res) {
            calendar.updateEvents(res.schedules)
            $scope.shared.isButtonLoading = false;
            notifications.show('success', resource.match('saved'));
            $scope.$digest();
        });
    }
    $scope.shared.deleteStudentFromList = function (student, index) {
        var flt = $scope.shared.selectedLesson.students.filter(function (elem) {
            if (elem.active)
                return elem;
            if (elem.Added)
                return elem;
        });
        if (flt.length == 1 && !flt[0].Added && student.active) {
            notifications.show('error', resource.match('class_should'));
            return;
        }
        if (student.hasOwnProperty('Added'))
            $scope.shared.selectedLesson.students.splice(index, 1);
        else
            student.active = !student.active
    }
    $scope.shared.addNewStudentToSelectedlesson = function (newStudent) {
        $scope.shared.selectedLesson.students.push({
            Id: newStudent.Id, ProfilePicture: newStudent.ProfilePicture, Name: newStudent.Name, Location: newStudent.Location, active: true, Added: true, Credit: newStudent.credits.firstOrNull(function (el) {
                return el.Type == $scope.shared.selectedLesson.type
            })
        });
        $scope.shared.selectedLesson.findStudent = false;
    }
    $scope.shared.updateLesson = function () {
        if (!$scope.shared.selectedLesson.level || !$scope.shared.selectedLesson.language || !$scope.shared.selectedLesson.students.length || !$scope.shared.selectedLesson.type) {
            notifications.show('error', resource.match('you_did_not_select'));
            return;
        }
        if ($scope.shared.selectedLesson.type == "Group" && !$("#topics").valid()) {
            notifications.show('error', resource.match('you_should_to_fill'));
            return;
        }
        $scope.shared.isButtonLoading = true;
        api.ajax('/api/SchedulerOfStudents/UpdateLesson', 'post', JSON.stringify($scope.shared.selectedLesson), function (res) {
            var stds = $scope.shared.selectedLesson.students.filter(function (elem) {
                if (elem.Added)
                    delete elem.Added;
                if (elem.active)
                    return elem;
            });
            $scope.shared.isButtonLoading = false;
            event.students = stds;
            event.type = $scope.shared.selectedLesson.type;
            event.title = "";
            event.topic = $scope.shared.selectedLesson.topic;
            event.language = $scope.shared.selectedLesson.language;
            event.level = $scope.shared.selectedLesson.level;
            event.className = res.status.split(' ');
            if (res.status.indexOf("unconfirmed") === -1)
                event.IsConfirmed = true;
            stds.forEach(function (elem) {
                event.title += elem.Name + " ,";
            })
            notifications.show('success', resource.match('saved'));
            $scope.shared.selectedLesson.isTrue = false;
            updateListOfStudents();
            cal.fullCalendar('rerenderEvents');
            $scope.$apply();
        })
    }
    $scope.shared.selectedLesson.cancelLesson = function () {
        console.log("cancelleson");
        if (!$scope.shared.selectedLesson.isCancelActive)
            return;
        $scope.shared.isButtonLoading = true;
        api.ajax('/api/SchedulerOfStudents/CancelLesson', 'post', JSON.stringify($scope.shared.selectedLesson), function (res) {
            console.log(res);
            updateListOfStudents();
            $scope.shared.isButtonLoading = false;
            cal.fullCalendar('removeEvents', [event._id]);
            $scope.shared.selectedLesson.isTrue = false;
            $scope.shared.selectedLesson.isCancelActive = false;
            $scope.$apply();
            $timeout(function () {
                $scope.updateStatistic();
            });
        })
    }
  $scope.shared.classNotHappen = function () {
    console.log("classNotHappen");
    if (!$scope.shared.selectedLesson.problem) {
      alert("Please type a problem");
      return;
    }
    $scope.shared.isButtonLoading = true;
    $("#problem_modal").modal('hide');
    api.ajax('/api/SchedulerOfStudents/ClassNotHappenForTeacher',
      'post',
      JSON.stringify($scope.shared.selectedLesson),
      function (res) {
        $scope.shared.isButtonLoading = false;
        console.log(res);
        if (res.Error)
          notifications.show('error', res.Error);
        else
          notifications.show('success', "Success");
        $scope.shared.selectedLesson.problem = "";
      });
  }
    $scope.shared.selectedLesson.confirm = function (lesson) {
        if (event.className.length == 2) {
            notifications.show("error", resource.match('you_should_save'));
            return;
        }
        $scope.shared.isButtonLoading = true;
        api.ajax('/api/TeacherSchedule/ConfirmLesson', 'post', JSON.stringify(lesson.id), function (res) {
            event.className = res.split(' ');
            $scope.shared.isButtonLoading = false;
            //$scope.shared.selectedLesson.isTrue = false;
            $scope.shared.selectedLesson.isConfirmed = true;
            cal.fullCalendar('rerenderEvents');
            $scope.$apply();
        })
    }

    function selectCell(start, end) {
        var numb = $(".fc-week-number span:first-child").text();
        if (end - start !== 3600000) {
            var selEvents = [];
            cal
                .fullCalendar('clientEvents',
                    function (event) {
                        selEvents.push(event.start._d);
                    });
            var st = start.clone();
            var nd = start.clone().add(1, "hours");
            var dates = [];
            while (st < end) {
                if (selEvents.map(Number).indexOf(+st._d) >= 0) {
                    st = st.add(1, "hours");
                    nd = nd.add(1, "hours");
                    continue;
                }
                var clone = st.clone();
                var eventDatas = {
                    title: ' ', start: clone, end: nd.clone(), className: 'view1', week: parseInt(numb.substr(1)), dayOfTheWeek: start._d.getDay() + 1
                };
                st = st.add(1, "hours");
                nd = nd.add(1, "hours");
                dates.push(eventDatas);
            }
            addEvents(dates);
            return;
        }
        curentDateTime = start._d;
        var eventData;
        numb = parseInt(numb.substr(1));
        eventData = {
            title: ' ', start: start, end: end, className: 'view1', week: numb, dayOfTheWeek: start._d.getDay() + 1
        };
        addEvents([eventData]);

    }
    function addEvents(events) {
        var newEvts = [];
        events
            .forEach(function (elem) {
                var el = {
                    'startDateTime': elem.start.toISOString() + "+00:00", 'endDateTime': elem.end.toISOString() + "+00:00", 'weekNumber': elem.week, 'timeStart': new Date(elem.start.toISOString()).getUTCHours(), 'customUserId': userId, 'status': elem.className, 'dayOfTheWeek': elem.dayOfTheWeek
                };
                newEvts.push(el);
            });
        $http.post("/en/TeacherSchedule/UpdateSchedule", { 'schedule': newEvts, 'deleted': [] }).success(function (result) {
            calendar.deletedEvents = [];
            if (result && result.length > 0) {
                cal.fullCalendar('addEventSource', result);
                $scope.updateStatistic();
            }
            // notifications.show('success', "Saved");
        });
    }
    function renderEvents(event, element, calEvent) {
        //console.log(event.className[0]);
        if (event.className.length !== 1) return;
        element.find(".fc-bg").css("text-align", "right").html("<span class='checker'><i class='fa fa-times' aria-hidden='true'></i></span>").children(".checker").click(function (evt) {
            evt.stopPropagation();
            cal.fullCalendar('removeEvents', event._id);
            revemoFreeEvent(event.id);
        });
    }
    function revemoFreeEvent(id) {
        api.ajax('/api/SchedulerOfStudents/RemoveFreeLesson', 'post', JSON.stringify(id), function (res) {
            $scope.updateStatistic();
        })
    }
    function eventDrop(event, delta, revertFunc, jsEvent, ui, view) {
        var el = {
            'id': event.id, 'startDateTime': event.start.toISOString() + "+00:00", 'endDateTime': event.end.toISOString() + "+00:00", 'timeStart': new Date(event.start.toISOString()).getUTCHours(), 'dayOfTheWeek': event.dayOfTheWeek
        }
        api.ajax('/api/SchedulerOfStudents/ChangeDate', 'post', JSON.stringify(el), function (res) {
            $scope.updateStatistic();
        })
    }
    calendar.render($scope.shared.userId);
    jQuery('.page-preloader-wrap-dashboard').hide();
}