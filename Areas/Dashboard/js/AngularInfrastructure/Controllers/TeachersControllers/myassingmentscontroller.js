﻿myassingmentscontroller.$inject = ["$scope", "api", "$http", "notifications", "$timeout", '$filter', 'recorderService'];
function myassingmentscontroller($scope, api, $http, notifications, $timeout, $filter, recorderService) {
    if (!$scope.shared.currentPageOfAssingments)
        $scope.shared.currentPageOfAssingments = 0;
    getData();
    var saveMessagePath = '/en/UploadImages/UploadAudioMessageFile';
    function getData() {
        api.ajax("/api/MyAssignments/GetMyAssingment?page=" + $scope.shared.currentPageOfAssingments, 'get', null, function (res) {
            $scope.myAssingments = res;
            $scope.$digest();
        });
    }
    var callback = function (res) {
        $scope.shared.selectedAssingmen.uploadMessagePath = res.path;
        originalAssign.uploadMessagePath = res.path;
        notifications.show('success', resource.match('loaded'));
        $timeout(function () {
            document.getElementById('audio' + originalAssign.id).load();
        });
    };
    $scope.getNextPrevPage = function(state){
        $scope.shared.currentPageOfAssingments = (state) ? ++$scope.shared.currentPageOfAssingments : --$scope.shared.currentPageOfAssingments;
        getData();
        $scope.shared.sceditorcap = false;
        $scope.shared.selectedAssingmen = null;
    }
    $scope.shared.changeStatus = function (status) {
        selectedAssign.state = status;
        api.ajax('/api/MyAssignments/ChangeStatus', 'post', JSON.stringify(selectedAssign), function (res) {
            var angelem = $filter('filter')($scope.myAssingments, { id: selectedAssign.id }, true)[0];
            var index = $scope.myAssingments.indexOf(angelem);
            var elem = $scope.myAssingments[index];
            elem.state = selectedAssign.state;
            notifications.show("success", resource.match('saved'));
            $scope.$apply();
            $scope.shared.intervalAss();
        });
    }
    var selectedAssign = {};
    var originalAssign;
    $scope.clickToAssingment = function (assing, event) {
        $scope.shared.sceditorcap = true;
        selectedAssign = $.extend({},assing);
        originalAssign = assing;
        //console.log($scope.shared.selectedAssingmen)
        console.log(selectedAssign);
        $scope.shared.selectedAssingmen = selectedAssign;
        var mt = moment(selectedAssign.dateOfChange);
        $scope.shared.selectedAssingmen.date = mt.format("ll");
        $scope.shared.selectedAssingmen.time = mt.format("HH:mm");
        $(event.target).parents('.assig-media').addClass("active").siblings().removeClass("active");
    };
    $scope.shared.addNewStudentToSelectedlesson = function (student) {
        //console.log($scope.shared.selectedAssingmen);
        $scope.shared.selectedAssingmen.student = { profilePicture: student.ProfilePicture, name:student.Name, active: true, added: true, Id: student.Id };
        $scope.shared.selectedAssingmen.findStudent = false;
    }
    $scope.shared.addNewAssingment = function () {
        selectedAssign = {
            id: -1, dateOfChange: new Date(), student: null, description: null, topic: null, image: "/Images/assig-img2.png", uploadFilePath: null, uploadMessagePath:null
        };
        $scope.shared.selectedAssingmen = selectedAssign;
        var mt = moment(selectedAssign.dateOfChange);
        $scope.shared.selectedAssingmen.date = mt.format("ll");
        $scope.shared.selectedAssingmen.time = mt.format("HH:mm");
        $scope.shared.sceditorcap = true;
    }
    $scope.shared.changePhoto = function () {
        var files = document.getElementById('loadImage').files;
        var res = shared.validateImages(files);
        if (res !== "OK") {
            notifications.show('error', res);
            return;
        }
        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            data.append(files[i].name, files[i]);
        }
        data.append('id', selectedAssign.id);
        $http.post('/en/UploadImages/UploadAssingment', data, {
            withCredentials: true,
            headers: { 'Content-Type': undefined },
            transformRequest: angular.identity
        }).success(function (res) {
            if (!res.path)
                return;
            $scope.shared.selectedAssingmen.image = res.path;
            originalAssign.image = res.path;
        });
    }
    $scope.shared.attachedFile = function () {
        var files = document.getElementById('attachedFiles').files;
        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            data.append(files[i].name, files[i]);
        }
        data.append('id', selectedAssign.id);
        $http.post('/en/UploadImages/UploadAssingmentFile', data, {
            withCredentials: true,
            headers: { 'Content-Type': undefined },
            transformRequest: angular.identity
        }).success(function (res) {
            console.log(res)
            $scope.shared.selectedAssingmen.uploadFilePath = res.path;
            originalAssign.uploadFilePath = res.path;
        });
    }
    $scope.shared.removeAttachedFile = function () {
        api.ajax('/api/MyAssignments/RemoveUploadedFile', 'post', JSON.stringify(selectedAssign), function (res) {
            var angelem = $filter('filter')($scope.myAssingments, { id: selectedAssign.id }, true)[0];
            var index = $scope.myAssingments.indexOf(angelem);
            var elem = $scope.myAssingments[index];
            elem.uploadFilePath = selectedAssign.uploadFilePath = null;
            $scope.$apply();
        });
    }
    $scope.shared.removeAssMessage = function () {
        api.ajax('/api/MyAssignments/RemoveMessage', 'post', JSON.stringify(selectedAssign), function (res) {
            var angelem = $filter('filter')($scope.myAssingments, { id: selectedAssign.id }, true)[0];
            var index = $scope.myAssingments.indexOf(angelem);
            var elem = $scope.myAssingments[index];
            elem.uploadMessagePath = selectedAssign.uploadMessagePath = null;
            $scope.$apply();
        });
    }
    $scope.shared.recordAudio = function (res) {
        recorderService.start(saveMessagePath, callback);
        $scope.shared.isRecording = true;
        $("#timer").timer({ format: '%m ' + resource.match('minutes') + ' %S '  + resource.match('seconds') + ''});
    }
    $scope.shared.stopRecordAudio = function () {
        recorderService.stop(function (data) { data.append('id', selectedAssign.id); return data});
        $scope.shared.isRecording = false;
        $("#timer").timer('remove');
    }
    $scope.shared.saveAssingmentChanges = function () {
        if (!selectedAssign.student) {
            notifications.show('error', resource.match('you_should_select'));
            return;
        }
        api.ajax('/api/MyAssignments/SaveChanges', 'post', JSON.stringify(selectedAssign), function (res) {
            if (selectedAssign.id === -1)
                addNewAssingment(res);
            else
                changeDataOfAssingment(res);
            notifications.show("success", resource.match('saved'));
        })
    }
    function addNewAssingment(res) {
        $scope.myAssingments.unshift(res);
        selectedAssign = res;
        $scope.shared.selectedAssingmen = res;
        $scope.$apply();
    }
    function changeDataOfAssingment(res) {
        var angelem = $filter('filter')($scope.myAssingments, { id: res.id }, true)[0];
        var index = $scope.myAssingments.indexOf(angelem);
        var elem = $scope.myAssingments[index];
        elem.topic = res.topic;
        elem.description = res.description;
        elem.student = res.student;
        $scope.shared.selectedAssingmen = null;
        notifications.show('success', resource.match('saved'));
        $scope.shared.sceditorcap = false;
        $scope.$apply();
    }
    if (!$scope.shared.myCurrentStudents) {
        api.ajax("/api/MyStudents/GetMyStudents", 'get', null, function (res) {
            $scope.shared.myCurrentStudents = res;
            //console.log(res);
        })
    }
    jQuery('.page-preloader-wrap-dashboard').hide();
}