﻿beforeactivationcontroller.$inject = ["$scope", "notifications", "api", "$http"];
function beforeactivationcontroller($scope, notifications, api, $http) {
  $scope.options = {
    language: 'en',
    allowedContent: true,
    entities: false
  };
  $scope.user = user.user;
  $scope.ilearn = langData.ilearn ? langData.ilearn : [];
  $scope.ispeak = langData.ispeak ? langData.ispeak : [];
  console.log(user);
  $scope.uploadImage = function () {
    var data = new FormData();
    var file = document.getElementById("upload-photo").files[0];
    data.append(file.name, file);
    $http.post('/UploadImages/UploadAvatar', data, {
      withCredentials: true,
      headers: { 'Content-Type': undefined },
      transformRequest: angular.identity
    }).success(function (res) {
      $scope.user.profilePicture = res.path;
    });
  };
  api.ajax("/api/MyExperience/GetMyExperiance", 'get', null, function (res) {
    console.log(res);
    $scope.educations = res.educations;
    $scope.expiriences = res.expiriences;
    $scope.certifications = res.certifications;
    $scope.$digest();
  });
  $scope.addItem = function (array, elem) {
    var item = { Description: "", Title: "", PeriodFrom: "2016", PeriodTo: "2016", Id: -1, Deleted: false };
    array.push(item);
  }
  $scope.deleteItem = function (array, elem, index) {
    if (elem.Id == -1)
      array.splice(index, 1);
    else
      elem.Deleted = true;
  }
  $scope.submitForm = function () {
    saveAll();
  }
  api.ajax("/api/AboutMe/GetAboutMeInfo", 'get', null, function (res) {
    console.log(res);
    $scope.item = res;
    $scope.$digest();
  })
  function saveAll() {
    if (!$scope.ilearn.length || !$scope.ispeak.length || !$scope.educations.length || !$scope.expiriences.length || !$scope.certifications.length || !$scope.item.videoSrc) {
      alert("You did not fill required fields!");
      return;
    }
    var conf = confirm("Are you sure you want to submit your application?");
    if (!conf) return;
    $(".page-preloader-wrap").css("display", "block");
    //saveSkype().then(function(res) {
    //  console.log(res);
    //}).then(function() {
    //  return $http.post('/en/NewMyAccount/Send');
    //}).then(function() {
    //  console.log("DONE");
    //});
    api.ajaxPromise("/api/AboutMe/UpdateAboutMeInfo", 'post', JSON.stringify($scope.item))
      .then(function(res) {
        return getSavingMyLanguages()
      }).then(function() { return saveSkype() })
      .then(function(res) {
        return $.when(saveMyExperiance($scope.educations, 'SaveEducation'),
          saveMyExperiance($scope.expiriences, 'SaveExpiriences'),
          saveMyExperiance($scope.certifications, 'SaveCertifications'));
      }).then(function() {
        return $http.post('/en/NewMyAccount/Send');
      }).then(function(res) {
          console.log(redirect);
          alert('success');
          location.href = redirect;
          //notifications.show('success', 'Saved');
          return null;
        },
        function(err) {
          $(".page-preloader-wrap").css("display", "none")
          alert('error');
          //notifications.show('error', 'Error');
        });
  };
  function saveMyExperiance(array, method) {
    return api.ajaxPromise("/api/MyExperience/" + method, 'post', JSON.stringify(array));
  }
  function getSavingMyLanguages() {
    var il = $scope.ilearn.map(function (elem) {
      return "cap_" + elem;
    }).join('|');
    if (il)
      il += "|";
    var is = $scope.ispeak.map(function (elem) {
      return "cap_" + elem;
    }).join('|');
    if (is)
      is += "|";
    return $http({ method: 'post', url: '/en/NewMyAccount/SaveLanguages', params: { 'il': il, 'is': is } });
  }
  function saveSkype() {
    return $http({ method: 'post', url: '/en/NewMyAccount/SaveSkype', params: { 'skype': $scope.skype} });
  }
  $scope.$watch('item.videoSrc', function (newVal) {
    if (newVal.indexOf("http") === -1)
      document.getElementById('vdvideo').load();
    else if (newVal.indexOf("youtube") !== -1 && newVal.indexOf("embed") === -1) {
      var v = shared.gup('v', newVal);
      var someval = "https://www.youtube.com/embed/" + v;
      $scope.item.videoSrc = someval;
    }
    else if (newVal.indexOf("vimeo") !== -1 && newVal.indexOf("player") === -1) {
      var vl = newVal.split('/');
      var le = vl[vl.length - 1];
      var someval = "https://player.vimeo.com/video/" + le;
      $scope.item.videoSrc = someval;
    }
  })
}
