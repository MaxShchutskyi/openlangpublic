﻿mystudentscontroller.$inject = ["$scope", "$location"];
function mystudentscontroller($scope, $location) {

    var mystds = $('#mystudents');
    $scope.dtOptions = shared.getTableProperties(mystds);
    $scope.goTo = function (path, event) {
        $scope.shared.globalDialogForConnect = $(event.currentTarget).attr("data-clientid");
        $location.path(path);
        $scope.shared.currentPath = path;
        $scope.shared.selectedAssingmen = null;
        $scope.shared.sceditorcap = false;
        $scope.shared.selectedWord = {};
    }
    jQuery('.page-preloader-wrap-dashboard').hide();
}