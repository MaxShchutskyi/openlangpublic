﻿photogallerycontroller.$inject = ["$scope", "notifications", "api", "$http"];
function photogallerycontroller($scope, notifications, api, $http) {
    $scope.activeAll = function () {
        angular.forEach($scope.photos, function (elem) {
            elem.active = !elem.active;
        });
    }
    $scope.uploadImages = function () {
        var files = document.getElementById('uploadFiles').files;
        var res = shared.validateImages(files);
        if (res !== "OK"){
            notifications.show('error', res);
            return;
        }
        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
            data.append(files[i].name, files[i]);
        }
        $http.post('/en/MyAccountTeacher/UploadImages', data, {
            withCredentials: true,
            headers: { 'Content-Type': undefined },
            transformRequest: angular.identity
        }).success(function (res) {
            angular.forEach(res.items, function (elem) {
                $scope.photos.push(elem);
            });
        });
    }
    $scope.removeItems = function (elem, index) {
        api.ajax('/api/Photogallery/RemoveImages', 'post', JSON.stringify(elem), function (res) {
            if (res.hasOwnProperty('success')) {
                $scope.photos.splice(index, 1);
                $scope.$digest();
            }
        });
    }
    $scope.removeAllActive = function () {
        var actives = $scope.photos.filter(function (elem) {
            if (elem.active)
                return elem;
        });
        api.ajax('/api/Photogallery/RemoveActiveImages', 'post', JSON.stringify(actives), function (res) {
            if (res.hasOwnProperty('success')) {
                for (var i = 0; i < actives.length; i++) {
                    var index = $scope.photos.indexOf(actives[i])
                    $scope.photos.splice(index, 1);
                }
                $scope.$digest();
            }
        });
    }
    api.ajax('/api/Photogallery/GetGallery', 'get', null, function (res) {
        $scope.photos = res;
        $scope.$digest();
    });
    jQuery('.page-preloader-wrap-dashboard').hide();
}