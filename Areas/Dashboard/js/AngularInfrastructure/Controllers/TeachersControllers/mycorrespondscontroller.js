﻿mycorrespondscontroller.$inject = ["$scope", 'api', "notifications"];
function mycorrespondscontroller($scope, api, notifications) {
    console.log("mycorrespondscontroller");
    api.ajax("/api/Coresponds/GetAllCorrespons", 'get', null, function (res) {
        $scope.groups = res;
        $scope.$digest();
    })
    $scope.selectAnswer = function (event, group) {
        event.preventDefault()
        event.stopPropagation();
        $scope.selectedUser = group;
        $("#answerMessage").modal('show');
    }
    $scope.status = {
        isFirstOpen: true,
        isFirstDisabled: false
    };
    $scope.sendMessage = function () {
        if (!$("#sndAnswer").valid()) return;
        var message = { Text: $scope.answerTextMessage, Subject: $scope.answerSubject, Input: false, MessageCreated: moment().utc(), Email: $scope.selectedUser.Email, Name: $scope.selectedUser.Name };
        api.ajax('/api/Coresponds/SendMessage', 'post', JSON.stringify(message), function (res) {
            notifications.show('success', resource.match('your_mess_was_sent'));
        })
        $scope.selectedUser.Messages.unshift(message);
        $("#answerMessage").modal('hide');
    }
    $scope.getDate = function (date) {
        var dt = moment.utc(date).toDate();
        return moment(dt).local().format('DD/MM/YYYY HH:mm')
    }
    $("#sndAnswer").validate({
        errorPlacement: function () { }
    });
    jQuery('.page-preloader-wrap-dashboard').hide();
}