﻿homeworkdetailscontroller.$inject = ["$scope", "api", "$http", "notifications", "$timeout", '$filter', 'recorderService','$routeParams'];
function homeworkdetailscontroller($scope, api, $http, notifications, $timeout, $filter, recorderService, $routeParams) {
  $('#datetimepicker4').datetimepicker({
    format: 'DD.MM.YYYY',
    minDate: moment().add(1, 'day')
  });
  if ($routeParams.id) {
    api.ajax('/api/Homework/GetHomework?id=' + $routeParams.id, 'get', null, function (res) {
      $scope.model = { groupId: res.GroupId, studentId: res.StudentId, id: $routeParams.id };
      updateGroups();
      $('#datetimepicker4').data("DateTimePicker").date(moment(res.DueDate));
      $scope.$digest();
    });
  }
  else
    $scope.model = {};
  api.ajax('/api/Group/GetGroups', 'get', null, function (res) {
    $scope.groups = res;
    console.log(res);
    updateGroups();
    $scope.$digest();
  });
  function updateGroups() {
    if (!$scope.groups || !$scope.model)
      return;
    $scope.groups.forEach(function(elem) {
      if ($scope.model.groupId === elem.Id)
        elem.checked = true;
    });
  }
  $scope.addNewCard = function () {
    var ts = $scope.groups.filter(function (x) { return x.checked });
    if (!ts.length) {
      alert("You did not check any group");
      return;
    }
    $scope.model.groupId = ts[0].Id;
    $scope.model.duedate = $('#datetimepicker4').data('DateTimePicker').date().format("YYYY-MM-DD[T]HH:mm:ss.SSSZZ");
    var item = $scope.model;
    console.log($scope.model);
    api.ajax('/api/homework/AddNewHomework',
      'post',
      JSON.stringify(item),
      function(res) {
        $scope.model.id = res.Id;
        alert("Saved");
      });
  }
  $scope.getAllLevels = function(cards) {
    var levels = cards.map(function (r) { return r.Level });
    console.log(levels);
    return levels;
  }
  api.ajax("/api/MyStudents/GetMyStudents", 'get', null, function (res) {
    console.log(res);
    $scope.students = res;
  })
  jQuery('.page-preloader-wrap-dashboard').hide();
}