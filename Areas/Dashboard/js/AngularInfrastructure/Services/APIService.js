﻿angular.module("api", [])
    .factory("api",
    function () {
        var key = 'token';
        var isRunning = false;
        var d1 = $.Deferred();
        function getToken() {
            if (isRunning)
                return $.when(d1);
            var email = $("div[data-user-email]").attr("data-user-email");
            var k = sessionStorage.getItem(key);
            if (k)
                return $.when({ key: k });
            else {
                if (email) {
                    isRunning = true;
                    var loginData = {
                        grant_type: 'password',
                        username: email,
                    };
                    return $.ajax({
                        type: 'POST',
                        url: '/Token',
                        data: loginData
                    }).then(function (res) {
                        sessionStorage.setItem(key, res.access_token);
                        isRunning = false;
                        d1.resolve({ key: res.access_token });
                        return { key: res.access_token };
                    }).fail(function (err) {
                        console.log(err);
                    });
              };
              return $.when({ key: null });
            }
        }

        function ajax(url, type, data, callback) {
          getToken().then(function (res) {
            if (!res.key)
              return null;
                return $.ajax({
                    url: url,
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader("Authorization", "Bearer " + res.key);
                    },
                    async: true,
                    dataType: "json",
                    contentType: "application/json",
                    method: type,
                    data: data
                });
            }).done(function(res) {
                callback(res);
            }).fail(function(res) {
              if (res.status === 201)
                callback(res);
          });
        }
        function ajaxPromise(url, type, data) {
            return $.ajax({
                url: url,
                beforeSend: function (xhr) {
                    var token = sessionStorage.getItem(key);
                    xhr.setRequestHeader("Authorization", "Bearer " + token);
                },
                async: true,
                dataType: "json",
                contentType: "application/json",
                method: type,
                data: data,
            });
        }
        return {
            stateOfToken: null,
            ajax: ajax,
            ajaxPromise: ajaxPromise
        }
    });