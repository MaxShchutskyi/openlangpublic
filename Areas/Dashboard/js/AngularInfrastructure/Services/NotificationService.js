﻿angular.module("notifications", [])
    .factory("notifications",
        function () {
            var obj = $("#errorSharedPlace");
            var notifications = [{ name: 'error', color: '#eec8c5', title: 'Oops!', textcolor: '#c25e5f' }, { name: 'success', color: '#cee789', title: 'CHANGES!', textcolor: '#7e9258' }, { name: 'warning', color: '#f8f4c4 ', title: 'Oops!', textcolor: '#c0a56f' }];
            function show(type, message) {
                var selectedType;
                for (var i = 0; i < notifications.length; i++) {
                    if(notifications[i].name === type){
                        selectedType = notifications[i];
                        break;
                    }
                }
                obj.css({ "background": selectedType.color, "color": selectedType .textcolor}).find("#title").text(selectedType.title);
                obj.find("#message").text(message);
                obj.fadeIn(1000);
                setTimeout(function () {
                    obj.fadeOut(1000);
                }, 3000)
            }
            return {
                show:show
            }
        });