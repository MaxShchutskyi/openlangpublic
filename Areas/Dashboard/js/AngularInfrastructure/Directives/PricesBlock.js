﻿pricesblock.$inject = ["$http", "$timeout", "api", "notifications","$location"];
function pricesblock($http, $timeout, api, notifications, $location) {
    return {
        restrict: 'A',
        scope: false,
        templateUrl: "/Dashboard/NewMyAccount/PricesBlock",
        link: function link(scope, elem, attr) {
            scope.paymentConfig = {};
            scope.succeeded = true;
            //scope.paymentConfig.type = 'basic';
            scope.paymentConfig.validMonth = 1;
            jQuery("#creditform").validate({
                errorPlacement: function errorPlacement() { },
                highlight: function highlight(element) {
                    $(element).addClass("error").next().find(".select2-selection").addClass("error");
                },
                unhighlight: function unhighlight(element) {
                    $(element).removeClass("error").next().find(".select2-selection").removeClass("error");
                }
            });
            scope.selectedPackage = function (key, value, index) {
                scope.paymentConfig.state = index;
                scope.paymentConfig.countLess = key;
                scope.paymentConfig.price = value;
                scope.nextStep("#inputBalance", "#replenish-balance-step-2");
                //$("#inputBalance").modal("hide");
                //$("#replenish-balance-step-2").modal("show");
            };
            scope.selectPackage = function (pk) {
                //if (pk == 'membership' && scope.shared.credits.firstOrNull(x=>x.typeOfPrice == "Membership" && x.subscription.Active)) {
                //    notifications.show('warning', "You can not have more than one subscription");
                //    return;
                //}
                $location.path('/makepayment');
                //scope.paymentConfig.member = pk;
                //if (!scope.paymentConfig.type) scope.paymentConfig.type = 'basic';
                //$("#inputBalance").modal('show');
            };
            scope.$watch('paymentConfig.type', function (newVal, oldVal) {
                if (!newVal || newVal == oldVal) return;
                if (newVal == 'basic') scope.paymentConfig.countLessons = 45; else scope.paymentConfig.countLessons = 60;
            });
            scope.$watch('paymentConfig.member', function (newVal, oldVal) {
                if (!newVal || newVal == oldVal) return;
                if (newVal == 'membership') scope.paymentConfig.validMonth = 1; else scope.paymentConfig.validMonth = 6;
            });
            scope.selectedMethod = { setectedType: "credit" };
            scope.membership = true;
            scope.checkLessons = function (countLessons, duration, money, eurManey, typeOfLessons) {
                scope.selectedMethod.countLessons = countLessons;
                scope.selectedMethod.duration = duration;
                scope.selectedMethod.money = money.toFixed(2);
                scope.selectedMethod.eurMoney = eurManey;
                scope.selectedMethod.typeOfLessons = typeOfLessons;
                scope.selectedMethod.timeOfLesson = typeOfLessons == "Premium" ? "60" : "45";
                scope.selectedMethod.typeOfPrice = duration == 1 ? "Membership" : "Package";
            };
            scope.nextStep = function (prev, next) {
                $(prev).modal('hide');
                $(next).modal('show');
            };
            scope.activeOrNot = function () {
                if (!scope.shared.credits) return false;
                return scope.shared.credits.firstOrNull(function (x) {
                    return x.typeOfPrice == 'Membership' && x.subscription.Active;
                });
            };
            scope.selectedUnsubscribe;
            scope.unsubscribe = function () {
                scope.selectedUnsubscribe = scope.shared.credits.firstOrNull(function (x) {
                    return x.typeOfPrice == "Membership" && x.subscription.Active;
                });
                $("#replenish-balance-step-4").modal('show');
            };
            scope.unsubscribeConfirm = function () {
                var credit = scope.shared.credits.firstOrNull(function (x) {
                    return x.typeOfPrice == "Membership" && x.subscription.Active;
                });
                if (!credit || scope.shared.isButtonLoading) return;
                notifications.show('warning', "Please wait a result");
                scope.shared.isButtonLoading = true;
                api.ajax('/api/Payment/' + credit.subscription.TypeProvider + "_Unsubscribe", 'post', JSON.stringify(credit.subscription), function (res) {
                    if (res) credit.subscription.Active = false;
                    notifications.show('success', "Saved");
                    $("#replenish-balance-step-4").modal('hide');
                    scope.shared.isButtonLoading = false;
                    scope.shared.updateCredits();
                });
            };
            scope.confirmPayment = function () {
                if (scope.shared.isButtonLoading) return;
                scope.unsubscribeConfirm();
                scope.shared.isButtonLoading = true;
                scope.selectedMethod.countLessons = scope.paymentConfig.countLess;
                scope.selectedMethod.duration = scope.paymentConfig.validMonth;
                scope.selectedMethod.money = (scope.paymentConfig.price * scope.shared.globalDelta).toFixed(2);
                scope.selectedMethod.eurMoney = scope.paymentConfig.price;
                scope.selectedMethod.typeOfLessons = scope.paymentConfig.type.toUpperCase();
                scope.selectedMethod.timeOfLesson = scope.paymentConfig.type.toUpperCase() == "PREMIUM" ? "60" : "45";
                scope.selectedMethod.typeOfPrice = scope.paymentConfig.validMonth == 1 ? "Membership" : "Package";
                if (scope.discount) {
                    scope.selectedMethod.discount = scope.discount.Id;
                    scope.selectedMethod.bonus = scope.replenish.bonus;
                    scope.selectedMethod.eurBonus = scope.replenish.eurBonus;
                }
                $("#replenish-balance-step-3").modal('hide');
                if (scope.selectedMethod.setectedType == 'credit') {
                    api.ajax('/api/Payment/CreditCardPayment', 'post', JSON.stringify(scope.selectedMethod), function (res) {
                        console.log(res);
                        scope.shared.isButtonLoading = false;
                        if (res.hasOwnProperty('error')) {
                            scope.succeeded = false;
                            $("#replenish-balance-step-7").modal('show');
                            scope.$digest();
                            return;
                        }
                        scope.shared.updateCredits();
                        scope.selectedMethod = { setectedType: "credit" };
                        scope.paymentConfig = { validMonth: 1 };
                        $("#replenish-balance-step-7").modal('show');
                        scope.$digest();
                    });
                }
                if (scope.selectedMethod.setectedType == 'paypal') {
                    api.ajax('/api/Payment/PayPalPayment', 'post', JSON.stringify(scope.selectedMethod), function (res) {
                        location.href = res.path;
                    });
                }
            };
            scope.createReplenish = function () {
                if (scope.selectedMethod.setectedType == 'credit' && !$("#creditform").valid()) return;
                if (scope.shared.isButtonLoading) return;
                //$("#replenish-balance-step-2").modal('hide');
                scope.selectedMethod.currency = scope.shared.globalCurrency;
                scope.nextStep("#replenish-balance-step-2", "#replenish-balance-step-3");
            };
            $(".info-tooltip").tooltip();
            scope.getDate = function (date) {
                var dt = moment.utc(date, "DD-MM-YYYY").toDate();
                return moment(dt).local().format('DD.MM.YYYY');
            };
            scope.getDate2 = function (date) {
                var dt = moment.utc(date, "DD-MM-YYYY HH:mm").toDate();
                return moment(dt).local().format('DD.MM.YYYY HH:mm');
            };
            if ($.cookie("showWelcome")) {
                var bal = JSON.parse($.cookie("showWelcome"));
                scope.dtt = scope.getDate2(bal.date);
                $("#replenish-balance-step-5").modal('show');
                $.cookie("showWelcome", null, { path: '/' });
                $.removeCookie('showWelcome', { path: '/' });
                document.cookie = "showWelcome" + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                scope.$digest();
            }
            if ($.cookie("showstep3")) {
                //console.log($.cookie("replenish-balance-step-7"));
                //var bal = JSON.parse($.cookie("replenish-balance-step-7"));
                //scope.dtt = scope.getDate2(bal.date);
                $("#replenish-balance-step-7").modal('show');
                $.cookie("showstep3", null, { path: '/' });
                $.removeCookie('showstep3', { path: '/' });
                document.cookie = "showstep3" + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                scope.$digest();
            }
            scope.$watch('replenish.additionalBonus', function (newVal, oldVal) {
                if (newVal == oldVal) return;
                api.ajax("/api/Discounts/CheckExistsDiscount", "get", { discount: newVal, type: scope.paymentConfig.member }, function (res) {
                    scope.discount = res;
                    console.log(res);
                    scope.replenish.discount = scope.discount ? scope.discount.Id : null;
                    scope.getDiscountIfExist(parseFloat(scope.paymentConfig.price));
                    scope.$digest();
                });
                //scope.replenish.eurbalance = (scope.replenish.replenishacount / scope.shared.globalDelta).toFixed(2);
            });
            scope.getDiscountIfExist = function (floatVal) {
                if (!scope.discount) return;
                if (scope.discount.IsPercent) scope.replenish.eurBonus = floatVal * parseFloat(scope.discount.Value) / 100; else scope.replenish.eurBonus = parseFloat(scope.discount.Value);
                scope.replenish.bonus = (scope.replenish.eurBonus * scope.shared.globalDelta).toFixed(2);
            };
            jQuery(".pricingBlock .inf-tool").each(function (elem, en) {
                $(en).tooltip().click(function () {
                    if (jQuery(this).siblings(".tooltip.fade").hasClass("in") && $(this).hasClass("opened")) {
                        $(this).removeClass('opened')
                        $(this).tooltip('hide');
                    }
                    else {
                        $(this).addClass('opened');
                        $(this).tooltip('show');
                    }
                });
            });
            $(document).mouseup(function (e) {
                var container = $(".inf-tool");
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    jQuery(".inf-tool").removeClass("opened");
                }
            });
        }

    };
}