﻿function intlTelInputAngular() {
    return {
        restrict: 'A',
        scope: {fullnumber:'=fullNumber', location:'=location'},
        link: function (scope,elem, attrs) {
            console.log(elem);
            elem.intlTelInput({
                autoFormat: true,
                defaultCountry: "gb",
                nationalMode: true,
                utilsScript: "/js/utils.js"
            });
            scope.$watch('location', function () {
                scope.fullnumber = elem.intlTelInput('setCountry', scope.location.split(':')[1].toLowerCase());
            }, true);
        }
    }
}