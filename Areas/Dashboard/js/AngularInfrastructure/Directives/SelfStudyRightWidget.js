﻿selfStudyRightWidget.$inject = ["api"];
function selfStudyRightWidget(api) {
    return {
        restrict: 'A',
        templateUrl: function (elem, attrs) {
            return "/Dashboard/SelfStudy/RightWidget?date=" +new Date()
        },
        //scope: { top: '=top', template: '=template' },
        //require: ['?top', '?template'],
        //scope: { top: '=top', template: '=template' },
        link: function ($scope, elem, attr, $location) {
            $scope.statistic = {};
            $scope.getTotalPoints = function(){
                if ($scope.statistic.TotalPoint === undefined) return "";
                return $scope.statistic.TotalPoint.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
            }
            api.ajax("/api/ChapterStudent/GetRating", 'get', null, function (res) {
                console.log(res);
                $scope.statistic.rating = res;
            });

            api.ajax("/api/ChapterStudent/GetRightStatistic", 'get', null, function (res) {
                //{ TotalPoint = totalPoints, TotalLessons = totalLessons, CompletedLessons = completedLessons, Percent = percent };
                $scope.statistic.TotalPoint = res.TotalPoint;
                $scope.statistic.TotalLessons = res.TotalLessons;
                $scope.statistic.CompletedLessons = res.CompletedLessons;
                $scope.statistic.Percent = res.Percent.toFixed(0);
                console.log(res);
                $('.circle-progress').circleProgress({
                    value: res.Percent / 100,
                    size: 120,
                    lineCap: "round",
                    startAngle: Math.PI + 1.5708,
                    emptyFill: "#f6f5f5",
                    fill: { color: "rgba(154, 211, 65, 0.85)" }
                });
            })
        },
    }
}
