﻿levelProgress.$inject = ["api"];
function levelProgress(api) {
    return {
        restrict: "E",
        templateUrl: function () {
            return '/Areas/Dashboard/js/AngularInfrastructure/Directives/Chapter/templates/levelPropgress.html?date' + new Date();
        },
        scope: {
            levelobj: "="
        },
        link: function (scope, element, attrs) {
            console.log("linklinklinklinklinklinklink")
            api.ajax("/api/ChapterStudent/GetLevelProgress?levelId=" + scope.levelobj.Id, "get", null, function (res) {
                console.log(res);
                if (!res)
                    scope.levelobj.percent = 0;
                else
                    scope.levelobj.percent = res.toFixed(1);
                $("#level" + scope.levelobj.Id).circleProgress({
                    value: scope.levelobj.percent / 100,
                    size: 88,
                    lineCap: "round",
                    startAngle: Math.PI + 1.5708,
                    emptyFill: "#f6f5f5",
                    fill: { color: "rgba(154, 211, 65, 0.85)" }
                });
            });
        }
    }
};