﻿lessonProgress.$inject = ["api"];
function lessonProgress(api) {
    return {
        restrict: "E",
        templateUrl: function () {
            return '/Areas/Dashboard/js/AngularInfrastructure/Directives/Chapter/templates/lessonProgress.html?date' + new Date();
        },
        scope: {
            lessonobj: "="
        },
        link: function (scope, element, attrs) {
            var prevId = attrs["previd"];
            api.ajax("/api/ChapterStudent/GetPercentSuccess?currentId=" + scope.lessonobj.Id + "&prevId=" + prevId, "get", null, function (res) {
                scope.lessonobj.PercentSuccuss = res.PercentSuccuss;
                scope.lessonobj.IsAvailable = res.IsAvailable;
                scope.$digest();
            });
        }
    }
};