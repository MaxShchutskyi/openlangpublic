﻿chapterProgress.$inject = ["api"];
function chapterProgress(api) {
    return {
        restrict: "E",
       // require: ['chapterobj'],
        templateUrl: function () {
            return '/Areas/Dashboard/js/AngularInfrastructure/Directives/Chapter/templates/chapterProgress.html?date' + new Date();
        },
        scope: {
            chapterobj:"="
        },
        link: function (scope, element, attrs) {
            api.ajax("/api/ChapterStudent/GetChapterStatic?chapterId=" + scope.chapterobj.Id, "get", null, function (res) {
                console.log(res);
                if (!res)
                    scope.chapterobj.percent = 0;
                else
                    scope.chapterobj.percent = res;
                $("#chapter" + scope.chapterobj.Id).circleProgress({
                    value: scope.chapterobj.percent/100,
                    size: 88,
                    lineCap: "round",
                    startAngle: Math.PI + 1.5708,
                    emptyFill: "#f6f5f5",
                    fill: { color: "rgba(154, 211, 65, 0.85)" }
                });
            });
        }
    }
};