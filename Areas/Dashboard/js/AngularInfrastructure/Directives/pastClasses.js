﻿function pastClasses() {
    return {
        restrict: 'A',
        templateUrl: function (elem, attrs) {
            return "/Dashboard/PastClasses/" + attrs.method + "?take=" + attrs.top + "&template=" + attrs.template;
        },
        scope: { top: '=top', template: '=template' },
        require: ['?top','?template'],
        scope: { top: '=top', template: '=template' },
        link: function (scope, elem, attr, $location) {
            $.fn.dataTable.ext.errMode = 'none';
            var item = $(".customcalendar");
            if (item) {
                item.DataTable(shared.getTableProperties(item));
            }
            scope.changeClassPath = scope.$parent.changeClassPath;
        }
    }
}