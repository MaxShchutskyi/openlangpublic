﻿statistic.$inject = ["api"];
function statistic(api) {
    return {
        restrict: 'A',
        scope: false,
        templateUrl: "/Dashboard/NewMyAccount/GetStatistic",
        link: function (scope, elem, attrs) {
            scope.updateStatistic = function () {
                api.ajax(attrs.url, 'get', null, function (res) {
                    scope.statistic = res;
                    scope.$digest();
                });
            }
            scope.updateStatistic();
        }
    }
}