﻿voiceDir.$inject = ["$compile"];
function voiceDir($compile) {
    return {
        restrict: "E",
        scope: {
            card: '=card'
        },
        templateUrl: function () {
            return '/Areas/Dashboard/js/AngularInfrastructure/Directives/Cards/templates/voice-model.html?date' + new Date();
        },
        controller: function ($scope) {
            var answerText = $scope.card.Answers[0].Variant;
            var html = "<div class='task-ans'>" + answerText + "</div>";
            html = html.replace(/\((.+?)\)/g, "<input type='text' class='task-word' placeholder={{voiceAnswer}} ng-model='wordAnswer'>");
            var compiledElements = $compile(html)($scope);
            var pageElement = angular.element(document.getElementById("wrr"));
            pageElement.empty()
            pageElement.append(compiledElements);
            var rightAnswer = answerText.match(/\(([^)]+)\)/)[1]
            var recognizer = recognizerSetup(window.SDK, "Interactive", "en-US", window.SDK.SpeechResultFormat["Simple"], "7d42ba63a3614726aae549a4f5b1232a");
            $scope.card.checkAnswer = function () {
                
                $scope.card.IsAnswered = true;
                if ($scope.wordAnswer) {
                    return $scope.card.IsRightAnswer = rightAnswer.toUpperCase() === $scope.wordAnswer.toUpperCase();
                };
                return $scope.card.IsRightAnswer = rightAnswer.toUpperCase() === $scope.voiceAnswer.toUpperCase();
            }
            $scope.card.getCorrectAnswer = function () {
                return rightAnswer;
            }
            $scope.startVoice = function () {
                if ($scope.isRecording) return;
                $scope.isRecording = true;
                recognizer.Recognize((event) => {
                    switch (event.Name) {
                        case "SpeechSimplePhraseEvent":
                            updateRecognizedPhrase(event.Result);
                            break;
                        case "SpeechDetailedPhraseEvent":
                            updateRecognizedPhrase(JSON.stringify(event.Result, null, 3));
                            break;
                        default:
                            console.log(JSON.stringify(event));
                    }
                })
                    .On(() => {
                        // The request succeeded. Nothing to do here.
                    },
                        (error) => {
                            console.log(error);
                        });
                function updateRecognizedPhrase(json) {
                    console.log(json);
                    debugger;
                    $scope.voiceAnswer = json.DisplayText.slice(0, -1);
                    $scope.isRecording = false;
                    $scope.$apply();
                }
            }
            function recognizerSetup(SDK, recognitionMode, language, format, subscriptionKey) {
                switch (recognitionMode) {
                    case "Interactive":
                        recognitionMode = SDK.RecognitionMode.Interactive;
                        break;
                    case "Conversation":
                        recognitionMode = SDK.RecognitionMode.Conversation;
                        break;
                    case "Dictation":
                        recognitionMode = SDK.RecognitionMode.Dictation;
                        break;
                    default:
                        recognitionMode = SDK.RecognitionMode.Interactive;
                }
                var recognizerConfig = new SDK.RecognizerConfig(
                    new SDK.SpeechConfig(
                        new SDK.Context(
                            new SDK.OS(navigator.userAgent, "Browser", null),
                            new SDK.Device("SpeechSample", "SpeechSample", "1.0.00000"))),
                    recognitionMode,
                    language, // Supported languages are specific to each recognition mode. Refer to docs.
                    format); // SDK.SpeechResultFormat.Simple (Options - Simple/Detailed)
                var useTokenAuth = false;
                var authentication = function () {
                    if (!useTokenAuth)
                        return new SDK.CognitiveSubscriptionKeyAuthentication(subscriptionKey);
                    var callback = function () {
                        var tokenDeferral = new SDK.Deferred();
                        try {
                            var xhr = new (XMLHttpRequest || ActiveXObject)('MSXML2.XMLHTTP.3.0');
                            xhr.open('GET', '/token', 1);
                            xhr.onload = function () {
                                if (xhr.status === 200) {
                                    tokenDeferral.Resolve(xhr.responseText);
                                } else {
                                    tokenDeferral.Reject('Issue token request failed.');
                                }
                            };
                            xhr.send();
                        } catch (e) {
                            window.console && console.log(e);
                            tokenDeferral.Reject(e.message);
                        }
                        return tokenDeferral.Promise();
                    }

                    return new SDK.CognitiveTokenAuthentication(callback, callback);
                }();
                return SDK.CreateRecognizer(recognizerConfig, authentication);
            }
            //$('.button_skip').show();
            //$('.words-check').hide();
            //function startRecognizer() {
            //    if ('webkitSpeechRecognition' in window) {
            //        var recognition = new webkitSpeechRecognition();
            //        recognition.lang = 'en';
            //        recognition.onresult = function (event) {
            //            var result = event.results[event.resultIndex];

            //            var rs = result[0].transcript;
            //            console.log(rs);
            //            $('#voice-word').html(rs.split(' ')[0]);
            //            $('.circle').hide();
            //            $('.circle2').hide();
            //            if (rs.split(' ')[0] == 'Spanish') {
            //                $scope.voiceCheck = function () {
            //                    $scope.counter.rightAnswers += 1;
            //                    $scope.counter.pass += 1;

            //                    $scope.setCheck(2);

            //                    $('.words-check').trigger('click');
            //                };
            //                $scope.voiceCheck();
            //                $("#voice-word").css("background-color", "#99d332");

            //            }
            //            else {

            //                $scope.voiceCheck = function () {
            //                    rightAnswer = "I don't speak Spanish";
            //                    $scope.counter.pass += 1;

            //                    $scope.setCheck(3);
            //                    $('.words-check').trigger('click');
            //                };
            //                $scope.voiceCheck();

            //                $("#voice-word").css("background-color", "#ee5a60");

            //            }

            //        };
            //        recognition.start();
            //    } else {
            //        alert('webkitSpeechRecognition is not supported in your browser :(');
            //        $('.nextVideo').show();
            //    };
            //}
            //$('#voice-record').click(function () {
            //    $('.circle').show();
            //    $('.circle2').show();
            //    startRecognizer();
            //});

        },
        controllerAs: "voice"
    };
};