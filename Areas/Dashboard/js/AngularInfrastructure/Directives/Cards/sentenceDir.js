﻿
function sentenceDir() {
    return {
        restrict: "E",
        scope: {
            card: '=card'
        },
        templateUrl: function () {
            return '/Areas/Dashboard/js/AngularInfrastructure/Directives/Cards/templates/sentence-model.html?date' + new Date();
        },
        controller: function ($scope) {
            //$scope.$watch("card", function (newValue, oldValue) {
            //    if (!newValue || !oldValue || newValue.Id === oldValue.Id)
            //        return;
            //    $scope.answerModel = $scope.card.Answers.filter(function (x) { return x.Index!=-1 }).map(function (x) { return {}; });
            //});
            $scope.answerModel = $scope.card.Answers.filter(function (x) { return x.Index != -1 }).map(function (x) { return {}; });
            $scope.card.Answers = shuffle($scope.card.Answers);
            $scope.card.getCorrectAnswer = function () {
                return $scope.card.Answers.filter(function (x) { return x.Index != -1 }).sort(function (a, b) {
                    if (a.Index > b.Index) return 1;
                    else if (a.Index < b.Index) return -1;
                    else return 0;
                }).map(function (x) {return x.Variant}).join(" ");

            }
            $scope.card.checkAnswer = function () {
                var ans = $scope.card.Answers.filter(function (x) { return x.Index != -1 });
                ans.sort(function (a, b) {
                    if (a.Index > b.Index) return 1;
                    else if (a.Index < b.Index) return -1;
                    else return 0;
                })
                var isCorrect = true;
                for (var i = 0; i< $scope.answerModel.length; i++) {
                    if (!$scope.answerModel[i].Variant || $scope.answerModel[i].Index != ans[i].Index) {
                        isCorrect = false;
                        break;
                    }
                }
                $scope.card.IsRightAnswer = isCorrect;
                $scope.card.IsAnswered = true;
                return isCorrect;
            }
            $scope.selectAnswer = function (answer) {
                if (answer.IsSelected) return;
                if ($scope.answerModel.find(function (x) { return !x.Variant}))
                    answer.IsSelected = true;
                for (var i = 0; i < $scope.answerModel.length; i++) {
                    if (!$scope.answerModel[i].Variant) {
                        $scope.answerModel[i] = answer;
                        break;
                    }
                }
                return;
            }
            $scope.removeAnswer = function (answer) {
                answer.IsSelected = false;
                for (var i = 0; i < $scope.answerModel.length; i++) {
                    if ($scope.answerModel[i].Variant === answer.Variant) {
                        $scope.answerModel[i] = {};
                        break;
                    }
                }
            }
            $scope.getStyle = function (answer) {
                if (answer.IsSelected)
                    return { opacity: 0, cursor: 'default' }
                return;
            }
            function shuffle(a) {
                var j, x, i;
                for (i = a.length - 1; i > 0; i--) {
                    j = Math.floor(Math.random() * (i + 1));
                    x = a[i];
                    a[i] = a[j];
                    a[j] = x;
                }
                return a;
            }
        },
    };
};