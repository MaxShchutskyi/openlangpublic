﻿function choiceDir() {
    return {
        restrict: "E",
        scope: {
            card: '=card'
        },
        templateUrl: function () {
            return '/Areas/Dashboard/js/AngularInfrastructure/Directives/Cards/templates/choice-model.html?date' + new Date();
        },
        link: function ($scope) {

            $scope.alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            init();
            //$scope.$watch("card", function (newValue, oldValue) {
            //    if (!newValue || !oldValue || newValue.Id === oldValue.Id)
            //        return;
            //    init();
            //});
            function init() {
                $scope.sentence = JSON.parse($scope.card.AdditionalJSON)[0];
                $scope.answerModel = {};
                $scope.card.getCorrectAnswer = function () {
                    var res = $scope.sentence.Answers.find(function (x) { return x.IsRight });
                    return res ? res.Variant : "";
                }
                $scope.card.checkAnswer = function () {
                    var answer = $scope.sentence.Answers.find(function (x) {
                        return x.Variant === $scope.answerModel.selectedValue
                    });
                    if (!answer)
                        $scope.card.IsRightAnswer = false;
                    else
                        $scope.card.IsRightAnswer = answer.IsRight;
                    $scope.card.IsAnswered = true;

                    return answer ? answer.IsRight : false;
                }
                $scope.getStyle = function (answer) {
                    if ($scope.card.IsAnswered && answer.IsRight)
                        return { backgroundColor: '#87d500', borderColor: '#87d500', color: '#fff' }
                    return;
                }
            }
        },
    };
};