﻿
function picturesDir() {
    return {
        restrict: "E",
        scope: {
            card: '=card'
        },
        templateUrl: function () {
            return '/Areas/Dashboard/js/AngularInfrastructure/Directives/Cards/templates/pictures-model.html?date' + new Date();
        },
        controller: function ($scope) {
            init();
            var numbers = ["First", "Second", "Thisrd", "Fourth"];
            $scope.$watch("card", function (newValue, oldValue) {
                if (!newValue || !oldValue || newValue.Id === oldValue.Id)
                    return;
                init();
            });
            function init() {
                $scope.answerModel = {};
                $scope.card.getCorrectAnswer = function () {
                    var res = $scope.card.Answers.find(function (x) { return x.IsRight });
                    if (!res) return "";
                    var index = $scope.card.Answers.indexOf(res);
                    return numbers[index] + " image";
                }
                $scope.card.checkAnswer = function () { 
                    var answer = $scope.card.Answers.find(function (x) {
                        return x.Variant === $scope.answerModel.selectedValue
                    });

                    if (!answer)
                        $scope.card.IsRightAnswer = false;
                    else
                        $scope.card.IsRightAnswer = answer.IsRight;
                    $scope.card.IsAnswered = true;
                    return answer ? answer.IsRight : false;

                }
                $scope.getStyle = function (answer) {
                    if ($scope.card.IsAnswered && answer.IsRight)
                        return { backgroundColor: '#87d500', borderColor: '#87d500', color: 'white' }
                    return;
                }
                $scope.getStyleLi = function (answer) {
                    if ($scope.card.IsAnswered && answer.IsRight) {
                        return { backgroundColor: '#F0F7E8' };
                    }

                    if (answer.IsSelected) {
                        return { backgroundColor: '#E5F5FC' };
                    } else {
                        return { backgroundColor: '#fff' };
                    }
                    return;
                }
                $scope.changeBack = function (answer) {
                    
                    if ($scope.first) {
                        $scope.first.IsSelected = false;
                    }
                    $scope.first = answer;
                    answer.IsSelected = true;
                    
                }
                
            }
        },
    };
};