﻿function listenDir() {
    return {
        restrict: "E",
        scope: {
            card: '=card'
        },
        templateUrl: function () {
            return '/Areas/Dashboard/js/AngularInfrastructure/Directives/Cards/templates/listen-model.html?date' + new Date();
        },
        link: function ($scope) {
            var answerText = $scope.card.Answers[0].Variant;
            console.log("Translate", answerText);

            $scope.card.getCorrectAnswer = function () {
                return answerText;
            }
            $scope.playAudio = function () {
                document.getElementById("audio1").play();
            }
            $scope.answerModel = {};
            $scope.card.checkAnswer = function () {
                $scope.card.IsAnswered = true;
                if (!$scope.answerModel.Answer)
                    $scope.card.IsRightAnswer = false;
                else if ($scope.answerModel.Answer.toUpperCase() == $scope.card.Answers[0].Variant.toUpperCase())
                    $scope.card.IsRightAnswer = true;
                else
                    $scope.card.IsRightAnswer = false;
                return $scope.card.IsRightAnswer;
            }
        },
    };
};