﻿function nonDir() {
    return {
        restrict: "E",
        scope: {
            card: '=card'
        },
        templateUrl: function () {
            return '/Areas/Dashboard/js/AngularInfrastructure/Directives/Cards/templates/non-action-model.html?date' + new Date();
        },
        link: function ($scope) {
            $scope.$watch("card", function (newValue, oldValue) {
                if (!newValue || !oldValue || newValue.Id === oldValue.Id)
                    return;
                $scope.sentence = { ImageSlideSrc: $scope.card.ImageSlideSrc };
                
            });
            
            $scope.sentence = { ImageSlideSrc: $scope.card.ImageSlideSrc };
           
                
          
           
           
        },
    };
};