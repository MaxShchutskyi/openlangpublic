﻿function memoryDir() {
    return {
        restrict: "E",
        $scope: {
            card: '=card'
        },
        templateUrl: function () {
            return '/Areas/Dashboard/js/AngularInfrastructure/Directives/Cards/templates/memory-games-model.html?date' + new Date();
        },
        controller: function ($scope) {
            var answers = $scope.card.Answers.map(function (x) { return Object.assign({}, x) });
            var answers2 = $scope.card.Answers.map(function (x) { return Object.assign({}, x) });
            var res = shuffle(answers.concat(answers2));
            res.forEach(function (x, i) { x.Index = i });
            $scope.answers = res;
            $scope.selectCard = function (answer) {
                if (answer.IsSelected) return;
                answer.IsSelected = true;
                if (!$scope.first) {
                    $scope.first = answer;
                    return;
                }
                else
                    $scope.second = answer;
                setTimeout(checkMatch, 500);
            }
            function checkAllRight() {
                if ($scope.answers.find(function (x) { return !x.IsSelected })) return;
                $scope.curCheck = $scope.curCheck + 1;
                $scope.$digest();
                $scope.card.IsRightAnswer = true;
                $scope.card.IsAnswered = true;
               
            }
            function checkMatch() {
                if ($scope.second && $scope.first) {
                    if ($scope.second.Id === $scope.first.Id)
                        $scope.second.IsMatched = $scope.first.IsMatched = true;
                    else
                        $scope.second.IsSelected = $scope.first.IsSelected = false;
                    $scope.second = $scope.first = null;
                }
                checkAllRight();
                $scope.$apply();
            }
            $scope.getStyle = function (answer) {
                return answer.IsSelected ? { transform: 'rotateY(180deg)' } : { transform: 'rotateY(360deg)' };
            }
        },

    };
};
function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}