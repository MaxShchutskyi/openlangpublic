﻿wordsDir.$inject = ["$compile"];
function wordsDir($compile) {
    return {
        restrict: "E",
        scope: {
            card: '=card'
        },
        templateUrl: function () {
            return '/Areas/Dashboard/js/AngularInfrastructure/Directives/Cards/templates/words-model.html?date' + new Date();
        },
        controller: function ($scope) {
            var obj = JSON.parse($scope.card.AdditionalJSON);
            var html = '<div class="task-ans">' + obj.text + '</div>'
            html = html.replace('__', '<div class="task-word" id="task-word" ng-style="getTaskWordStyle()">{{model.Variant}}</div>')
            var compiledElements = $compile(html)($scope);
            var pageElement = angular.element(document.getElementById("wr"));
            pageElement.empty()
            pageElement.append(compiledElements);
            $scope.model = {};
            $scope.card.getCorrectAnswer = function () {
                var cotAnswer = $scope.card.Answers.find(function (x) { return x.IsRight });
                return cotAnswer ? cotAnswer.Variant : "";
            }
            $scope.selectAnswer = function (answer) {
                if (answer.Id === $scope.model.Id)
                    return;
                for (var i = 0; i < $scope.card.Answers.length; i++) {
                    if ($scope.card.Answers[i].Id === answer.Id) {
                        $scope.card.Answers[i].IsSelected = true;
                        $scope.model = $scope.card.Answers[i];
                    }
                    else
                        $scope.card.Answers[i].IsSelected = false;
                }
            }
            $scope.card.checkAnswer = function () {
                var selectedAnswer = $scope.card.Answers.find(function (x) { return x.IsSelected });
                debugger;
                if (!selectedAnswer)
                    $scope.card.IsRightAnswer = false;
                else
                    $scope.card.IsRightAnswer = selectedAnswer.IsRight;
                $scope.card.IsAnswered = true;
                return $scope.card.IsRightAnswer;
            }
            $scope.getNavStyle = function (ansr) {
                if (ansr.IsSelected) 
                    return { 'background': '#eef2f5', 'color': '#eef2f5', 'borderColor': '#ddd' };
            }
            $scope.getTaskWordStyle = function (ansr) {
                if ($scope.model.Id)
                    return { 'backgroundColor': '#009de4', 'color': '#fff'};
            }
        }
    };
};