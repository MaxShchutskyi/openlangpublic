﻿
function videoDir() {
    return {
        restrict: "E",
        scope: {
            card: '=card'
        },
        templateUrl: function () {
            return '/Areas/Dashboard/js/AngularInfrastructure/Directives/Cards/templates/video-model.html?date' + new Date();
        },
        controller: function ($scope) {
            //$scope.$watch("card", function (newValue, oldValue) {
            //    if (!newValue || !oldValue || newValue.Id === oldValue.Id)
            //        return;
            //    $scope.answerModel = {};
            //});
            $scope.answerModel = {};
            $("#videoControl source").attr('src', $scope.card.VideoSrc);
            $("#videoControl").load();
            $scope.card.getCorrectAnswer = function () {
                return $scope.card.Answers[0].Variant;
            }
            $scope.card.checkAnswer = function () {
                console.log("rifght", $scope.answerModel.Answer.toUpperCase());
                $scope.card.IsAnswered = true;
                if (!$scope.answerModel.Answer)
                    $scope.card.IsRightAnswer = false;
                else if ($scope.answerModel.Answer.toUpperCase() == $scope.card.Answers[0].Variant.toUpperCase())
                    $scope.card.IsRightAnswer = true;
                else
                    $scope.card.IsRightAnswer = false;
                return $scope.card.IsRightAnswer;
            }
            $scope.getStyle = function (answer) {
                if ($scope.card.IsAnswered && answer.IsRight)
                    return { backgroundColor: '#87d500', borderColor: '#87d500', color: 'white' }
                return;
            }
            $scope.getVideoId = function () {
                if (!$scope.card.VideoSrc) return;
                var url = new URL($scope.card.VideoSrc);
                return url.searchParams.get("v");
            }
        },
    };
};