﻿balanceispositive.$inject = ["$filter"];
function balanceispositive($filter) {
    return function (arrayFilter, type) {
        if (type) {
            return $filter("filter")(arrayFilter, function (elem) {
                return elem.credits.firstOrNull(function (el) {
                    return el.Type == type
                });
            });
        }
    }
};