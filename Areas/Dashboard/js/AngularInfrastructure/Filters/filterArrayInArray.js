﻿filterarrayinarray.$inject = ["$filter"];
function filterarrayinarray($filter) {
    return function (list, arrayFilter, element) {
        if(arrayFilter){
            return $filter("filter")(list, function (listItem) {
                var founded = true;
                for (var i = 0; i < arrayFilter.length; i++) {
                    if (!arrayFilter[i]) continue;
                    if (arrayFilter[i].Id == listItem[element] || listItem[element] == "11111111-1111-1111-1111-111111111111") {
                        founded = false;
                        break;
                    }
                }
                return founded;
               // return arrayFilter.indexOf(listItem[element]) != -1;
            });
        }
    }};