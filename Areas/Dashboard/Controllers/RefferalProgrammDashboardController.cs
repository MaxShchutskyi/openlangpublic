﻿using Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Xencore.Filters;

namespace Xencore.Areas.Dashboard.Controllers
{
    [Culture(IsCookieOnly = true)]
    [Authorize]
    [GlobalUserDetect]
    public class RefferalProgrammDashboardController : ParentController
    {
        IReffreralProgrammRepository Repo { get; set; }
        public RefferalProgrammDashboardController(IReffreralProgrammRepository repo)
        {
            Repo = repo;
        }
        public async Task<ActionResult> GetRefferalPage()
        {
            var res = await Repo.GetParticiples(GetUserId);
            return View(res);
        }
        protected override void Dispose(bool disposing)
        {
            Repo.Dispose();
        }
    }
}