﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Context;
using Domain.Entities.HelpersEntity;
using Xencore.Extentions;
using Xencore.Filters;

namespace Xencore.Areas.Dashboard.Controllers
{
    public class MyStudentsController : ParentController
    {
        [Culture(IsCookieOnly = true)]
        [Authorize]
        [GlobalUserDetect]
        public async Task<ActionResult> GetMyStudents()
        {
            using (var context = new MyDbContext())
            {
                var transactions = await context.Schedules.Where(x => x.CustomUserId == GetUserId)
                    .Where(x => x.ScheduleTransactions.Any())
                    .SelectMany(x => x.ScheduleTransactions.Select(f => f.User))
                    .ToListAsync();
                try
                {
                    var groups = transactions.GroupBy(x => new { x.Id, x.ProfilePicture, x.Name, x.Ilearn }).Select(x => new MyStudentsView()
                    {
                        Id = x.Key.Id,
                        ProfilePicture = x.Key.ProfilePicture,
                        Name = x.Key.Name,
                        Level = x.SelectMany(f => f.PayPalPaymentTransactions).OrderByDescending(f => f.CreateDate).FirstOrDefault()?.OrderDetailsId.SelectedLevel,
                        ILearn = x.Key.Ilearn.ISomethingToShort('|').ToShort(22),
                        LastClass = x.SelectMany(f => f.ScheduleTransactions.Select(t => t.Schedule).OrderByDescending(t => t.StartDateTimeDateTime)).FirstOrDefault()?.StartDateTimeDateTime,
                        Attendance = "100%",
                        Classes = (short)x.SelectMany(f => f.ScheduleTransactions.Select(t => t.Schedule)).Where(f => f.CustomUserId == GetUserId).Distinct().Count()
                    }).ToList();
                    return View(groups);
                }
                catch (Exception errs)
                {

                }
                return null;
            }
        }
    }
}