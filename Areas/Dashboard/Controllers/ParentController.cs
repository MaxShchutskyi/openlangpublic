﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entities.IdentityEntities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Xencore.Areas.Dashboard.Controllers
{
    public class ParentController : Controller
    {
        protected Guid GetUserId => Guid.Parse(HttpContext.User.Identity.GetUserId());
        protected CustomUserManager GetUserManager => HttpContext.GetOwinContext().Get<CustomUserManager>();
    }
}