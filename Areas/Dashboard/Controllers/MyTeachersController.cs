﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Context;
using Domain.Entities.HelpersEntity;
using Repositories.Abstract;
using WebGrease.Css.Extensions;
using Xencore.Extentions;
using Xencore.Filters;

namespace Xencore.Areas.Dashboard.Controllers
{
    [Culture(IsCookieOnly = true)]
    [Authorize]
    public class MyTeachersController : ParentController
    {
        private ITeacherDashboardRepository Repo { get; set; }

        public MyTeachersController(ITeacherDashboardRepository repo)
        {
            Repo = repo;
        }
        [Timezone]
        public async Task<ActionResult> GetMyTeachers(short timezone)
        {
            var res = await Repo.GetTeachersOfStudent(GetUserId);
            res = res.Select(x => x.EqualsTime(timezone));
            res.ForEach(x=> x.Iteach = x.Iteach.ISomethingToShort('|').ToShort(20));
            return View(res);
        }
        [HttpGet]
        public ActionResult GetMyBookedTeachers()
        {
            var res = Repo.GetMyFaviriteTeachers(GetUserId);
            res.ForEach(x => x.Iteach =  x.Iteach.ISomethingToShort('|').ToShort(20));
            return PartialView(res);
        }

        protected override void Dispose(bool disposing)
        {
            Repo?.Dispose();
            base.Dispose(disposing);
        }
    }
}