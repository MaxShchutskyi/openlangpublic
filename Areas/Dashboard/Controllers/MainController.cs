﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Context;
using Newtonsoft.Json;
using Xencore.Filters;
using System.Security.Claims;
using Microsoft.AspNet.Identity;

namespace Xencore.Areas.Dashboard.Controllers
{
    [Culture(IsCookieOnly = true)]
    [Authorize]
    [GlobalUserDetect]
    public class MainController : ParentController
    {
        [Culture(IsCookieOnly = false)]
        public ActionResult LoadMain(DateTime? dt)
        {
            //var usr = User as ClaimsPrincipal;
            //if (usr.HasClaim("BeforeActivate", "first"))
            //    return RedirectToAction("Index", "BeforeActivationTeacherAccount");
            //if (usr.HasClaim("BeforeActivate", "second") && !usr.HasClaim("BeforeActivate", "third"))
            //    return RedirectToAction("Index", "BeforeActivationTeacherAccount");
            if (dt != null && dt != default(DateTime))
            {
                Response.Cookies.Add(new HttpCookie("showWelcome",
                    JsonConvert.SerializeObject(new { date = dt?.ToString("dd.MM.yyyy hh:mm") })));
                return RedirectToAction("LoadMain");
            }
            var user = GetUserManager.FindById(GetUserId);
            if (User.IsInRole("Presenter"))
            {
                if (!user.IsSentApplication || user.IsSentApplication && user.IsApplicationChanckedByAdmin && !user.IsSignedContract)
                    return RedirectToAction("Index", "BeforeActivationTeacherAccount");
            }
            ViewBag.User = JsonConvert.SerializeObject(new { profilePicture = user.ProfilePicture, name = user.Name, email = user.Email });
            return View();
        }
    }
}