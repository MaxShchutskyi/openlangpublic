﻿using Domain.Context;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EmailSenderService;
using EmailSenderService.Models;
using Microsoft.AspNet.Identity;
using Xencore.CompaignMonitor;
using Xencore.Extentions;
using Xencore.Filters;

namespace Xencore.Areas.Dashboard.Controllers
{
    [Authorize(Roles = "Presenter")]
    public class BeforeActivationTeacherAccountController : ParentController
    {
        [Culture]
        public async Task<ActionResult> Index()
        {
            var user = await GetUserManager.FindByIdAsync(GetUserId);
            if (user.IsSentApplication && user.IsApplicationChanckedByAdmin && !user.IsSignedContract)
                return Redirect(Url.RouteUrl(new { controller = "BeforeActivationTeacherAccount", action = "Terms" }) + "#terms");
            if(user.IsSentApplication && !user.IsApplicationChanckedByAdmin || user.IsSentApplication && user.IsApplicationChanckedByAdmin && user.IsSignedContract)
                return RedirectToAction("LoadMain", "Main");
            //var identity = User.Identity as ClaimsIdentity;
            //if (identity.HasClaim("BeforeActivate", "second") && !us.Active)
            //    return Redirect(Url.RouteUrl(new { controller = "BeforeActivationTeacherAccount", action = "Wait" }) + "#waitingconfirmation");
            //if (identity.HasClaim("BeforeActivate", "second") && us.Active)
            //    return Redirect(Url.RouteUrl(new { controller = "BeforeActivationTeacherAccount", action = "Terms" }) + "#terms");
            //if (identity.HasClaim("BeforeActivate", "third") && us.Active)
            //    return Redirect(Url.RouteUrl(new { controller = "Main", action = "LoadMain" }));
            return View();
        }
        [Culture]
        public async Task<ActionResult> Wait()
        {
            var url = Request.Url;
            var us = await GetUserManager.FindByIdAsync(GetUserId);
            var identity = User.Identity as ClaimsIdentity;
            if (identity.HasClaim("BeforeActivate", "second") && us.Active)
                return Redirect(Url.RouteUrl(new { controller = "BeforeActivationTeacherAccount", action = "Terms" }) + "#terms");
            return View("Index");
        }
        [Culture]
        public async Task<ActionResult> Terms()
        {
            var us = await GetUserManager.FindByIdAsync(GetUserId);
            //var identity = User.Identity as ClaimsIdentity;
            //if (identity.HasClaim("BeforeActivate", "third") && us.Active)
            //    return Redirect(Url.RouteUrl(new { controller = "Main", action = "LoadMain" }));
            return View("Index");
        }
        [Culture]
        public ActionResult AcceptTerms()
        {
            return View();
        }
        [Culture]
        public ActionResult PreRegister()
        {
            using (var context = new MyDbContext())
            {
                var user = context.Users.Find(GetUserId);
                var ispeak = user.Ispeak?.Split('|').Reverse().Skip(1).Select(x => x.Split('_')[1]);
                var ilearn = user.Ilearn?.Split('|').Reverse().Skip(1).Select(x => x.Split('_')[1]);
                var json = JsonConvert.SerializeObject(new { ispeak, ilearn });
                ViewBag.Languages = json;
                ViewBag.User = JsonConvert.SerializeObject(new { user = new { profilePicture = user.ProfilePicture } });
            }
            return View();
        }
        [Culture]
        public async Task<ActionResult> AfterRegisterBeforeTerms() {
            //GetUserManager.FindByIdAsync()
            //await GetUserManager.RemoveClaimAsync(GetUserId, new System.Security.Claims.Claim("BeforeActivate", "first"));
            //var cl = new System.Security.Claims.Claim("BeforeActivate", "second");
            //await GetUserManager.AddClaimAsync(GetUserId, cl);
            //User.AddUpdateClaim("BeforeActivate", "second");
            var user = await GetUserManager.FindByIdAsync(GetUserId);
            user.IsSentApplication = true;
            await GetUserManager.UpdateAsync(user);
            return Redirect(Url.RouteUrl(new { controller = "BeforeActivationTeacherAccount", action = "Wait" }) + "#waitingconfirmation");
            //return Redirect(Url.RouteUrl(new { controller = "BeforeActivationTeacherAccount", action = "index", isRedirect = true }) + "#terms");
        }
        [Culture]
        public ActionResult Waitingconfirmation() {
            return View();
        }
        [HttpPost]
        [Culture(IsCookieOnly = true)]
        public async Task<ActionResult> LastConfirm() {
            //await GetUserManager.RemoveClaimAsync(GetUserId, new System.Security.Claims.Claim("BeforeActivate", "second"));
            await GetUserManager.SendEmailAsync(Guid.Parse("11111111-1111-1111-1111-111111111111"), "A teacher signed their contract",
                EmailGeneratorService.GetBodyWithText(
                    $"{User.UserName()}, ({User.Identity.GetUserName()}) has signed their contract"));
            await new NativeEmailService.NativeEmailService().SendWithAttachmentAsync(new IdentityMessage()
            {
                Destination = User.Identity.GetUserName(),
                Subject = "Welcome to team! Here is your teacher guide",
                Body = EmailGeneratorService.GetBody(new AfterSignedUpContract(User.UserName()))
            },Server.MapPath("/img/TEACHER_ONBOARDING_GUIDE.pdf"));
            //User.DeleteClaim("BeforeActivate", "third");
            var cmp = new CompaignMonitorClient();
            cmp.AddNewTeacherAsync(Guid.Parse(User.Identity.GetUserId()));
            var user = await GetUserManager.FindByIdAsync(GetUserId);
            user.IsSignedContract = true;
            user.Active = true;
            await GetUserManager.UpdateAsync(user);
            return RedirectToAction("LoadMain", "Main");
        }
    }
}