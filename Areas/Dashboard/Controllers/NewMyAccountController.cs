﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CurrencyConverter;
using Domain.Context;
using Domain.Entities.HelpersEntity;
using EmailSenderService;
using EmailSenderService.Models;
using GeolocationManager.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using NLog;
using Xencore.Filters;
using Xencore.Extentions;
using Xencore.Managers.TablesOfPrices.Abstract;

namespace Xencore.Areas.Dashboard.Controllers
{
    [Culture(IsCookieOnly = true)]
    [Authorize]
    [GlobalUserDetect]
    public class NewMyAccountController : ParentController
    {
        public async Task<ActionResult> ReplenishBalance()
        {
            return View();
        }
        public async Task<ActionResult> PricesBlock()
        {
            return View();
        }
        public async Task<ActionResult> GetMyLessons()
        {
            return View();
        }
        public async Task<ActionResult> GetMyChat()
        {
            return View();
        }
        public async Task<ActionResult> GetMyWords()
        {
            return View();
        }
        
       public async Task<ActionResult> GetMyTransfers()
        {
            return View();
        }
        public async Task<ActionResult> GetStatistic()
        {
            return View();
        }
        public async Task<ActionResult> GetBudgetStatistic()
        {
            return View();
        }
        public async Task<ActionResult> GetMyAssingments()
        {
            return View();
        }
        public async Task<ActionResult> MakePayment()
        {
            return View();
        }
        public async Task<ActionResult> GetMyCorresponds()
        {
            return View();
        }
        public ActionResult GetTutorials()
        {
            return View();
        }
        public async Task<ActionResult> GetSchedule()
        {
            return View();
        }
        public async Task<ActionResult> GetDoHomework()
        {
            return View();
        }
        
        public async Task<ActionResult> GetCourseBooking()
        {
            using (var context = new MyDbContext())
            {
                var result  = await context.PaymentTransactions.Where(x => x.AttendeeId == GetUserId).Include(x=>x.OrderDetailsId).ToListAsync();
                return View(result);
            }
        }
        public async Task<ActionResult> GetPaymentReceipts()
        {
            using (var context = new MyDbContext())
            {
                var result = await context.PaymentTransactions.Where(x => x.AttendeeId == GetUserId).ToListAsync();
                return View(result);
            }
        }
        public async Task<ActionResult> ChangePassword()
        {
            return View();
        }
        public async Task<ActionResult> AboutMe()
        {
            return View();
        }
        public async Task<ActionResult> GetMyExperience()
        {
            return View();
        }
        public async Task<ActionResult> PhotoGallery()
        {
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> SaveLanguages(string il, string Is)
        {
            using (var context = new MyDbContext())
            {
                var user = context.Users.Find(GetUserId);
                user.Ilearn = il;
                user.Ispeak = Is;
                await context.SaveChangesAsync();
            }
            return Json(new {});
        }
        [HttpPost]
        public async Task<JsonResult> SaveSkype(string skype)
        {
            var user = await GetUserManager.FindByIdAsync(Guid.Parse(User.Identity.GetUserId()));
            if (user == null) return null;
            user.Skype = skype;
            await GetUserManager.UpdateAsync(user);
            return Json(new { });
        }
        public async Task<ActionResult> GetMyLanguages()
        {
            using (var context = new MyDbContext())
            {
                var user = context.Users.Find(GetUserId);
                var ispeak = user.Ispeak?.Split('|').Reverse().Skip(1).Select(x=>x.Split('_')[1]);
                var ilearn =  user.Ilearn?.Split('|').Reverse().Skip(1).Select(x => x.Split('_')[1]);
                var json = JsonConvert.SerializeObject(new {ispeak, ilearn});
                ViewBag.Data = json;
                return View();
            }
        }

        public ActionResult GetGroupsPage()
        {
            return View();
        }
        public ActionResult GetHomeworkPage()
        {
            return View();
        }
        public ActionResult GetHomeworkStudentPage()
        {
            return View();
        }
        public ActionResult GetHomeworkDetailsPage()
        {
            return View();
        }
        public async Task<JsonResult> GetNativeMoney(decimal money, string currency)
        {
            string nativeMoney;
            decimal delta;
            if (!string.IsNullOrEmpty(currency) && !currency.Contains("EUR"))
            {
                var factory = CurrencyConverterFactory.Create<string>();
                nativeMoney =
                Math.Round(factory.Convert(currency, "EUR", money).Result, 2) +
                " " + currency;
                delta = factory.Convert(currency, "EUR", 1).Result;
            }
            else
            {
                nativeMoney = money + " " + currency;
                delta = 1;
            }
            return Json(new { nativeMoney, delta }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<bool> Send()
        {
            var userid = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var teacher = await context.Users.Where(x => x.Id == userid).Include(x => x.WorkExperiences).Include(x => x.Educations)
                    .Include(x => x.Certifications).Include(x => x.TeacherInfo).FirstOrDefaultAsync();
                var submitTeacherApp = new SubmitTeacherApplication();
                var submitTeacherAppAdmin = new SubmitTeacherApplicationForAdmin();
                submitTeacherApp.Name = submitTeacherAppAdmin.Name=  teacher.Name;
                submitTeacherApp.Telephone = submitTeacherAppAdmin.Telephone =  teacher.PhoneNumber;
                submitTeacherApp.Skype = submitTeacherAppAdmin.Skype =  teacher.Skype;
                submitTeacherApp.Link = submitTeacherAppAdmin.Link =  teacher.TeacherInfo.Link;
                submitTeacherApp.Language = submitTeacherAppAdmin.Language = 
                    Resources.Resource.ResourceManager.GetString(teacher.Ilearn.Split('|')[0].Split('_')[1]);
                submitTeacherApp.Certifications = submitTeacherAppAdmin.Certifications =  new List<CommonInfoOfTeacher>();
                submitTeacherApp.WorkExpirience = submitTeacherAppAdmin.WorkExpirience =  new List<CommonInfoOfTeacher>();
                submitTeacherApp.Education = submitTeacherAppAdmin.Education =  new List<CommonInfoOfTeacher>();
                foreach (var item in teacher.WorkExperiences)
                {
                    submitTeacherAppAdmin.WorkExpirience.Add(new CommonInfoOfTeacher() { Description = item.Description, PeriodFrom = item.PeriodFrom, PeriodTo = item.PeriodTo, Title = item.Title });
                    submitTeacherApp.WorkExpirience.Add(new CommonInfoOfTeacher() { Description = item.Description, PeriodFrom = item.PeriodFrom, PeriodTo = item.PeriodTo, Title = item.Title });
                }
                foreach (var item in teacher.Educations)
                {
                    submitTeacherAppAdmin.WorkExpirience.Add(new CommonInfoOfTeacher() { Description = item.Description, PeriodFrom = item.PeriodFrom, PeriodTo = item.PeriodTo, Title = item.Title });
                    submitTeacherApp.Education.Add(new CommonInfoOfTeacher() { Description = item.Description, PeriodFrom = item.PeriodFrom, PeriodTo = item.PeriodTo, Title = item.Title });
                }
                foreach (var item in teacher.Certifications)
                {
                    submitTeacherAppAdmin.WorkExpirience.Add(new CommonInfoOfTeacher() { Description = item.Description, PeriodFrom = item.PeriodFrom, PeriodTo = item.PeriodTo, Title = item.Title });
                    submitTeacherApp.Certifications.Add(new CommonInfoOfTeacher() { Description = item.Description, PeriodFrom = item.PeriodFrom, PeriodTo = item.PeriodTo, Title = item.Title });
                }
                var body = EmailGeneratorService.GetBody(submitTeacherApp);
                var bodyForAdmin = EmailGeneratorService.GetBody(submitTeacherAppAdmin);
                await new NativeEmailService.NativeEmailService().SendAsync(new IdentityMessage()
                {
                    Body = body,
                    Destination = teacher.Email,
                    Subject = "Your teacher Application created successful"
                });
                await GetUserManager.SendEmailAsync(Guid.Parse("11111111-1111-1111-1111-111111111111"),
                    "Teacher submitted application", bodyForAdmin);

            }
            return true;
        }
    }
}