﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Context;
using Domain.Entities.HelpersEntity;
using Newtonsoft.Json;
using Xencore.Filters;

namespace Xencore.Areas.Dashboard.Controllers
{
    [Culture(IsCookieOnly = true)]
    [Authorize]
    [GlobalUserDetect]
    public class ProfileController : ParentController
    {
        public async Task<ActionResult> Index()
        {
            using (var context = new MyDbContext())
            {
                var user =
                    await context.Users.Where(x => x.Id == GetUserId)
                    .Include(x => x.AdditionalUserInformation).FirstAsync();
                var res = new UserForMyProfile()
                {
                    Id = user.Id,
                    Currency = user.Currency,
                    Email = user.Email,
                    Location = user.Location,
                    Name = user.Name,
                    TimeZone = user.TimeZone,
                    Shortintro = user.AdditionalUserInformation?.ShotIntrodution,
                    Lookingfor = user.AdditionalUserInformation?.WhatLookingFor,
                    Google = user.GoogleAccount,
                    Qq = user.QQAccount,
                    Skype = user.Skype,
                    Facetime = user.FaceTimeAccount,
                    ProfilePicture = user.ProfilePicture,
                    Phone = user.PhoneNumber,
                    IsSendMeConfirmationByEmail = user.IsSendMeConfirmationByEmail
                };
                var des = await context.Countries.ToListAsync();
                ViewBag.MyProfile = JsonConvert.SerializeObject(res);
                return View(des);
            }
        }
    }
}