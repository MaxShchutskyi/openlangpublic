﻿using Domain.Context;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Xencore.Areas.Dashboard.Controllers.api
{
    [Authorize]
    public class DashboardLanguagesController : ApiController
    {
        public object GetMyLanguagesApi()
        {
            var userId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var user = context.Users.Find(userId);
                var ispeak = user.Ispeak?.Split('|').Reverse().Skip(1).Select(x => x.Split('_')[1]);
                var ilearn = user.Ilearn?.Split('|').Reverse().Skip(1).Select(x => x.Split('_')[1]);
                return new { ispeak, ilearn };
            }
        }
    }
}
