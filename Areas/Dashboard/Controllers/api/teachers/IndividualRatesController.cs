﻿using Domain.Context;
using Domain.Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Xencore.Areas.Dashboard.Controllers.api.teachers
{
    [Authorize(Roles = "Presenter")]
    public class IndividualRatesController : ApiController
    {
        [HttpGet]
        public async Task<object> GetAllRates()
        {
            var userId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var rate = await context.IndividualTeacherRates.FirstOrDefaultAsync(x => x.UserId == userId);
                if (rate == null)
                {
                    rate = new Domain.Entities.IndividualTeacherRate()
                    {
                        UserId = userId
                    };
                    context.IndividualTeacherRates.Add(rate);
                    await context.SaveChangesAsync();
                }
                return rate.User != null ? new IndividualTeacherRate { Id = rate.Id, UserId = rate.UserId, Trial = rate.Trial, OneLessonRate = rate.OneLessonRate, TenLessonRate = rate.TenLessonRate, FiveLessonRate = rate.FiveLessonRate, FifteenLessonRate = rate.FifteenLessonRate, TrialPercent = rate.TrialPercent, OneLessonRatePercent = rate.OneLessonRatePercent, FiveLessonRatePercent = rate.FiveLessonRatePercent, FifteenLessonRatePercent = rate.FifteenLessonRatePercent, TenLessonRatePercent = rate.TenLessonRatePercent } : rate;
            }
        }
        [HttpPost]
        public async Task<bool> UpdateDay(IndividualTeacherRate rate)
        {
            var userId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var oldRate = await context.IndividualTeacherRates.FirstOrDefaultAsync(x => x.UserId == userId);
                oldRate.Trial = rate.Trial;
                oldRate.OneLessonRate = rate.OneLessonRate;
                oldRate.FiveLessonRate = rate.FiveLessonRate;
                oldRate.TenLessonRate = rate.TenLessonRate;
                oldRate.FifteenLessonRate = rate.FifteenLessonRate;
                await context.SaveChangesAsync();
                return true;
            }
        }
    }
}
