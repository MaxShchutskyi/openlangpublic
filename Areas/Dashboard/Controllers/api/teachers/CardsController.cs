﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Domain.Entities.IdentityEntities;
using DTO;
using DTO.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Repositories.Abstract;
using Xencore.Filters;

namespace Xencore.Areas.Dashboard.Controllers.api.teachers
{
    [Authorize(Roles = "Presenter")]
    [CultureWebApi]
    public class CardsController : ApiController
    {
        private IBrainCardRepo Repo { get; set; }

        public CardsController(IBrainCardRepo repo)
        {
            Repo = repo;
        }
        public async Task<IEnumerable<BrainCardDto>> GetAllCards()
        {
            var manager = HttpContext.Current.GetOwinContext().Get<CustomUserManager>();
            var user = await manager.FindByIdAsync(Guid.Parse(User.Identity.GetUserId()));
            var res =  await Repo.GetAllCardsByTeachLanguage(user.Ilearn);
            var brainCardDtos = res as BrainCardDto[] ?? res.ToArray();
            brainCardDtos.ToList().ForEach(x => {
                x.DateTimeUtcString = x.UploadDate.ToString("dd.MM.yyyy");
                x.Language = Resources.Resource.ResourceManager.GetString(x.Language);
                x.TypeCardName = x.Type.ToString();
                x.Level = Pseudo.GetPseudoLevel(x.Level);
            });
            return brainCardDtos;
        }

        protected override void Dispose(bool disposing)
        {
            Repo?.Dispose();
            base.Dispose(disposing);
        }
    }
}
