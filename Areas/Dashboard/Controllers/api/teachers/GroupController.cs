﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DTO;
using DTO.Helpers;
using Microsoft.AspNet.Identity;
using Repositories.Abstract;
using Xencore.Filters;

namespace Xencore.Areas.Dashboard.Controllers.api.teachers
{
    //[Authorize(Roles = "Presenter")]
    //[CultureWebApi]
    //public class GroupController : ApiController
    //{
    //    private IGroupOfCard Repo { get; }

    //    public GroupController(IGroupOfCard repo)
    //    {
    //        Repo = repo;
    //    }
    //    public async Task<IEnumerable<GroupOfCardDto>> GetGroups()
    //    {
    //        var res =  await Repo.GetAllGroupsByTeacherId(Guid.Parse(User.Identity.GetUserId()));
    //        var groupOfCardDtos = res as GroupOfCardDto[] ?? res.ToArray();
    //        foreach (var groupOfCardDto in groupOfCardDtos)
    //        {
    //            groupOfCardDto.Cards = groupOfCardDto.Cards.ToArray();
    //            foreach (var x in groupOfCardDto.Cards)
    //            {
    //                x.DateTimeUtcString = x.UploadDate.ToString("dd.MM.yyyy");
    //                x.Language = Resources.Resource.ResourceManager.GetString(x.Language);
    //                x.TypeCardName = x.Type.ToString();
    //                x.Level = Pseudo.GetPseudoLevel(x.Level);
    //            }

    //        }
    //        return groupOfCardDtos;
    //    }

    //    [HttpPost]
    //    public async Task<bool> DeleteCardFromGroup(Dt item)
    //    {
    //        return await Repo.RemoveCardFromGroup(item.GroupId, item.CardId);
    //    }
    //    [HttpPost]
    //    public async Task<bool> AddCardToroup(Dt item)
    //    {
    //        return await Repo.AddCardToGroup(item.GroupId, item.CardId);
    //    }
    //    [HttpPost]
    //    public async Task<GroupOfCardDto> AddNewGroup(GroupOfCardDto group)
    //    {
    //        return await Repo.AddNewGroup(Guid.Parse(User.Identity.GetUserId()), group.Name);
    //    }
    //    protected override void Dispose(bool disposing)
    //    {
    //        Repo?.Dispose();
    //        base.Dispose(disposing);
    //    }
    //}

    public class Dt
    {
        public int GroupId { get; set; }
        public int CardId { get; set; }
    }
}
