﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DTO;
using Microsoft.AspNet.Identity;
using Repositories.Abstract;

namespace Xencore.Areas.Dashboard.Controllers.api.teachers
{
    [Authorize(Roles = "Presenter")]
    public class HomeworkController : ApiController
    {
        private IHomeworkRepo Repo { get; }

        public HomeworkController(IHomeworkRepo repo)
        {
            Repo = repo;
        }

        public async Task<HomeworkDetailsDto> GetHomework(int id)
        {
            return await Repo.GetHomeWorkById(id);
        }
        public async Task<IEnumerable<HomeworkTableDto>> GetTablesHomework()
        {
            return await Repo.GetAllHomeworks(Guid.Parse(User.Identity.GetUserId()));
        }

        [HttpPost]
        public async Task<HomeworkDetailsDto> AddNewHomework(HomeworkDetailsDto item)
        {
            item.TeacherId = Guid.Parse(User.Identity.GetUserId());
            return await Repo.AddNewHomeworks(item);
        }
        protected override void Dispose(bool disposing)
        {
            Repo?.Dispose();
            base.Dispose(disposing);
        }
    }
}
