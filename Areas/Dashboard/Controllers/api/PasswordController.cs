﻿using System;
using System.Web;
using System.Web.Http;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Xencore.Areas.Dashboard.Controllers.api
{
    [Authorize]
    public class PasswordController : ApiController
    {
        public CustomSignIn SignIn => HttpContext.Current.GetOwinContext().Get<CustomSignIn>();
        [HttpPost]
        [ActionName("ChangePassword")]
        public object PostChangePassword([FromBody]ChangePassworsHelper obj)
        {
            if (!ModelState.IsValid)
                return new {error = "All fields below are required"};
            var idUser = Guid.Parse(HttpContext.Current.User.Identity.GetUserId());
            if (SignIn.UserManager.FindById(idUser).PasswordHash == null)
            {
                return SignIn.UserManager.AddPassword(idUser, obj.NewPass).Succeeded?
                    (object) new {success = "Success"}:
                    new { error = "Something wrong on the server" };
            }
            if (string.IsNullOrEmpty(obj.OldPass))
                return (object)new { error = "Incorrect old password" };
            var result = SignIn.UserManager.ChangePassword(idUser, obj.OldPass, obj.NewPass);
            return result.Succeeded ? (object)new { success = "Success" } : (object)new { error = "Incorrect old password" };
        }
    }
}
