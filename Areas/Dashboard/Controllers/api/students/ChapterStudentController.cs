﻿using Domain.Context;
using Domain.Entities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Xencore.Areas.Dashboard.Controllers.api.students
{
    [Authorize]
    public class ChapterStudentController : ApiController
    {
        [HttpGet]
        public async Task<IEnumerable<object>> GetAllChapters(int levelId)
        {
            using (var context = new MyDbContext())
            {
                return await context.Chapters.Where(x => x.Language == "us" && x.LevelId == levelId).Select(x => new
                {
                    x.IconSrc,
                    x.Id,
                    x.Level,
                    x.Name
                }).ToArrayAsync();
            }
        }

        [HttpGet]
        public async Task<float> GetChapterStatic(int chapterId)
        {
            var usId = User.Identity.GetUserId();
            if (usId == null)
                return 0;
            var userId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var countLessons = await context.Lessons.CountAsync(x => x.ChapterId == chapterId);
                if (countLessons == 0)
                    return 0;
                var succeeded = await context.UserLessonAttempts.CountAsync(x => x.Lesson.ChapterId == chapterId && x.StudentId == userId && x.IsSuccess);
                var res = ((float)succeeded / countLessons) * 100;
                return res;
            }
        }
        [HttpGet]
        public async Task<object> GetRightStatistic()
        {
            var userId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var user = await context.Users.FirstOrDefaultAsync(x => x.Id == userId);
                var totalPoints = user.Points;
                var totalLessons = await context.Lessons.CountAsync(x => x.Language == "us");
                var completedLessons = await context.UserLessonAttempts.Where(x => x.IsSuccess && x.StudentId == userId).Select(x => x.LessonId).Distinct().CountAsync();
                var percent = ((float)completedLessons / totalLessons) * 100;
                return new { TotalPoint = totalPoints, TotalLessons = totalLessons, CompletedLessons = completedLessons, Percent = percent };
            }
        }
        [HttpGet]
        public async Task<object> GetRating()
        {
            var userId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var rating = await context.Users.Select(x => new {
                    x.Name, x.Id, CompletedLessons = x.UserLessonAttempts.Count(f => f.IsSuccess)
                }).OrderByDescending(x => x.CompletedLessons).ToArrayAsync();
                return rating.Select((x, i) => new
                {
                    x.Name,
                    x.Id,
                    x.CompletedLessons,
                    Index = i,
                    IsYou = x.Id == userId
                })
                .Where(x => (x.Index > 0 && x.Index <= 3) || x.Id == userId).ToArray();
            }
        }
        [HttpGet]
        public async Task<object> GetChapterWithLesson(int chapterId)
        {
            using (var context = new MyDbContext())
            {
                return await context.Chapters.Where(x => x.Id == chapterId).Select(x => new
                {
                    x.Id,
                    x.Name,
                    Lessons = x.Lessons.Select(f => new { f.Id, f.Name, f.IconSrc, CountCards = f.BrainCards.Count() })
                }).FirstOrDefaultAsync();
            }
        }
        [HttpGet]
        public async Task<object> GetPercentSuccess(int currentId, int? prevId)
        {
            var userId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                if (prevId == null)
                {
                    var lastAttempt = await context.UserLessonAttempts.Where(x => x.LessonId == currentId && x.StudentId == userId).OrderByDescending(x => x.AttemptDate).Select(x => new { x.PercentSuccuss, IsAvailable = true }).FirstOrDefaultAsync();
                    if (lastAttempt == null)
                        return new { PercentSuccuss = 0, IsAvailable = true };
                    else
                        return new { PercentSuccuss = lastAttempt.PercentSuccuss, IsAvailable = true };
                }

                var rs = await context.UserLessonAttempts.Where(x => (x.LessonId == currentId || x.LessonId == prevId) && x.StudentId == userId).Select(x => new { x.Id, x.AttemptDate, x.PercentSuccuss, x.LessonId }).GroupBy(x => x.LessonId)
                    .Select(x => new
                    {
                        x.Key,
                        x.OrderByDescending(f => f.AttemptDate).FirstOrDefault().PercentSuccuss
                    }).ToArrayAsync();
                if (rs.Length == 0 || rs.FirstOrDefault(x => x.Key == prevId) == null)
                    return new { PercentSuccuss = 0, IsAvailable = false };
                var rr = rs.FirstOrDefault(x => x.Key == currentId);
                if (rr != null)
                    return new { rr.Key, rr.PercentSuccuss, IsAvailable = rs.First(x => x.Key == prevId).PercentSuccuss > 75 };
                else
                    return new { PercentSuccuss = 0, IsAvailable = rs.First(x => x.Key == prevId).PercentSuccuss > 75 };
            };
        }
        [HttpGet]
        public async Task<IEnumerable<object>> GetCards(int lessonId)
        {
            using (var context = new MyDbContext())
            {
                var res =  await context.BrainCards.Where(x => x.LessonId == lessonId).OrderBy(x=>x.OrderId).Select(x=> new
                {
                    x.Id,
                    x.Question,
                    x.OrderId,
                    x.Subject,
                    x.Type,
                    x.UploadDate,
                    x.AdditionalJSON,
                    Answers = x.Answers.Select(s=> new { s.Id, s.IsRight, s.ImageSrc, s.Variant, s.Index }),
                    x.VideoSrc,
                    x.ImageSlideSrc
                }).ToArrayAsync();
                return res;
            }
        }
        [HttpPost]
        public async Task<object> SaveAttempt(UserLessonAttempt attempt)
        {
            var userId = Guid.Parse(User.Identity.GetUserId());
            attempt.StudentId = userId;
            using (var context = new MyDbContext())
            {
                var user = context.Users.FirstOrDefault(x => x.Id == userId);
                var lastAttempt = await context.UserLessonAttempts.OrderByDescending(x => x.AttemptDate).FirstOrDefaultAsync(x => x.LessonId == attempt.LessonId && x.StudentId == userId);
                if (lastAttempt == null && attempt.IsSuccess)
                {
                    user.Points += 100;
                    context.UserLessonAttempts.Add(attempt);
                }
                else if (lastAttempt == null && !attempt.IsSuccess)
                    context.UserLessonAttempts.Add(attempt);
                else
                {
                    if (attempt.PercentSuccuss > lastAttempt.PercentSuccuss)
                    {
                        context.UserLessonAttempts.Add(attempt);
                        if (!lastAttempt.IsSuccess && attempt.IsSuccess)
                            user.Points += 50;
                    }
                }
                await context.SaveChangesAsync();
            }
            return true;
        }
        [HttpGet]
        public async Task<float> GetLevelProgress(int levelId)
        {
            var usId = User.Identity.GetUserId();
            if (usId == null)
                return 0;
            var userId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var countLessons = await context.Lessons.CountAsync(x => x.Chapter.LevelId == levelId);
                if (countLessons == 0)
                    return 0;
                var succeeded = await context.UserLessonAttempts.CountAsync(x => x.Lesson.Chapter.LevelId == levelId && x.StudentId == userId && x.IsSuccess);
                var res = ((float)succeeded / countLessons) * 100;
                return res;
            }
        }
    }
}
