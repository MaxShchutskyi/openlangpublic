﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DTO;
using Microsoft.AspNet.Identity;
using Repositories.Abstract;

namespace Xencore.Areas.Dashboard.Controllers.api.students
{
    [Authorize]
    public class HomeworkStudentController : ApiController
    {
        private IHomeworkRepo Repo { get; }

        public HomeworkStudentController(IHomeworkRepo repo)
        {
            Repo = repo;
        }
        public async Task<IEnumerable<HomeworkTableDto>> GetTablesHomework()
        {
            return await Repo.GetAllHomeworksByStudentId(Guid.Parse(User.Identity.GetUserId()));
        }
        public async Task<IEnumerable<BrainCardDto>> GetDoHomework(int id)
        {
            return await Repo.GetDoingGroupById(id);
        }
        [HttpPost]
        public async Task<bool> FinishHomework(HomeworkCompletedDto item)
        {
            return await Repo.FinishHomework(item);
        }
        protected override void Dispose(bool disposing)
        {
            Repo?.Dispose();
            base.Dispose(disposing);
        }
    }
}
