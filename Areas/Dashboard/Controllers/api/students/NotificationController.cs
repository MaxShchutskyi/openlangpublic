﻿using Domain.Context;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Xencore.Areas.Dashboard.Controllers.api.students
{
    [Authorize]
    public class NotificationController : ApiController
    {
        [HttpGet]
        public async Task<IEnumerable<object>> GetNotifications()
        {
            var userId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                return await context.NotificationsUsers.Where(x => x.CustomUserId == userId && !x.IsRead).Select(x => 
                new {
                    x.Notification.Id,
                    x.Notification.IconName,
                    x.Notification.Message,
                    x.Notification.CreationDate,
                    x.Notification.UserId,
                    x.Notification.Title
                }).ToArrayAsync();
            }
        }
        [HttpGet]
        public async Task<bool> WasRead(int id)
        {
            var userId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var note =  await context.NotificationsUsers.FirstOrDefaultAsync(x=>x.CustomUserId == userId && x.NotificationId == id);
                if (note == null)
                    return false;
                note.IsRead = true;
                await context.SaveChangesAsync();
                return true;
            }
        }
    }
}
