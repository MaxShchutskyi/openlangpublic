﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using EmailSenderService;
using Microsoft.AspNet.Identity;
using NLog;

namespace Xencore.Areas.Dashboard.Controllers.api
{
    [Authorize]
    public class ReferralProgramApiController : ApiController
    {
        [HttpPost]
        public async Task<bool> SendEmail(EmailModel model)
        {
            try
            {
                var body = EmailGeneratorService.GetBodyWithText(model.Message);
                await new NativeEmailService.NativeEmailService().SendAsync(new IdentityMessage()
                {
                    Body = body,
                    Destination = model.Email,
                    Subject = "Open Languages"
                });
                return true;
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error(e, "Error ReferralProgramApiController SendEmail");
                return false;
            }
        }
        [HttpPost]
        public async Task<bool> SendSms(SmsModel model)
        {
            try
            {
                await new NativeSmsService.NativeSmsService().SendAsync(new IdentityMessage()
                {
                    Body = model.Message,
                    Destination = model.Phone,
                    Subject = "Open Languages"
                });
                return true;
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error(e, "Error ReferralProgramApiController SendEmail");
                return false;
            }
        }
    }

    public class SmsModel
    {
        public string Phone { get; set; }
        public string Message { get; set; }
    }

    public class EmailModel
    {
        public string Email { get; set; }
        public string Message { get; set; }
    }
}
