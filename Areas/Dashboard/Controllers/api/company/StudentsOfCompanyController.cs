﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Domain.Context;
using Domain.Entities.IdentityEntities;
using DTO;
using EmailSenderService;
using Microsoft.AspNet.Identity;
using NLog;
using Repositories.Abstract;
using WebGrease.Css.Extensions;
using Xencore.Extentions;
using Xencore.Filters;
using Microsoft.AspNet.Identity.Owin;

namespace Xencore.Areas.Dashboard.Controllers.api.company
{
    [Authorize(Roles="Company")]
    public class StudentsOfCompanyController : ApiController
    {
        private CustomUserManager Manager { get; } = HttpContext.Current.GetOwinContext().Get<CustomUserManager>();
        private IStudentsOfCompanyRepository Repo { get; }

        public StudentsOfCompanyController(IStudentsOfCompanyRepository repo)
        {
            Repo = repo;
        }
        [HttpGet]
        [CultureWebApi]
        public async Task<IEnumerable<StudentsOfCompanyTableDto>> TableStudents()
        {
            try
            {
                var res =  await Repo.GetStudentsCompany(Guid.Parse(User.Identity.GetUserId()));
                res.ForEach(x=>x.ILearn = x.ILearn.ISomethingToShort('|'));
                return res;
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error(e);
                return null;
            }

        }

        [HttpGet]
        public async Task<IEnumerable<StudentsOfCompanyTableDto>> FindByEmail(string email)
        {
            return await Repo.FindStudentsByEmail(email);
        }
        [HttpPost]
        public async Task<bool> BindTo([FromBody]string studentId)
        {
            return  await Repo.BindTo(Guid.Parse(User.Identity.GetUserId()), Guid.Parse(studentId));
        }
        [HttpPost]
        public async Task<bool> InviteByEmail([FromBody]string email)
        {
            var company = await Manager.FindByIdAsync(Guid.Parse(User.Identity.GetUserId()));
            var body = EmailGeneratorService.GetBodyWithText($"{company.Name} invited you to join them on Open Languages. <br><a href='https://openlanguages.com/en/?compy={company.Id}&showregister=i'>By following this link</a> you can create an account and start learning a language right away invites you to the Learning language" );
            await new NativeEmailService.NativeEmailService().SendAsync(new IdentityMessage()
            {
                Body = body,
                Destination = email,
                Subject = $"{company.Name} is inviting you to Open Languages"
            });
            return true;
            //return await Repo.BindTo(Guid.Parse(User.Identity.GetUserId()), Guid.Parse(studentId));
        }

        protected override void Dispose(bool disposing)
        {
            Repo.Dispose();
            base.Dispose(disposing);
        }
    }
    
}
