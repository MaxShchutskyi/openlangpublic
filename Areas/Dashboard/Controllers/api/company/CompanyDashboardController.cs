﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Domain.Context;
using Domain.Entities.HelpersEntity;
using Domain.Repositories.Interfaces;
using Microsoft.AspNet.Identity;
using MvcApplicationPostFinanceLatest.Helpers;
using NLog;
using WebGrease.Css.Extensions;
using Xencore.Extentions;
using Xencore.Filters;

namespace Xencore.Controllers.api
{
    [Authorize(Roles = "Company")]
    public class CompanyDashboardController : ApiController
    {
        private Guid Id  => Guid.Parse(User.Identity.GetUserId());
        private ICompanyRepository CompanyRepo { get; set; }

        public CompanyDashboardController(ICompanyRepository companyrepo)
        {
            CompanyRepo = companyrepo;
        }
        [HttpGet]
        public async Task<IEnumerable<ShortUser2>> GetShortListOfStudents()
        {
            return await CompanyRepo.GetStudentsOfCompany(Id);
        }
        [CultureWebApi]
        public async Task<object> GetTransfers()
        {
            var res = await CompanyRepo.GetTransfers(Id);
            foreach (var r in res)
            {
                r.Ilearn = r.Ilearn.ISomethingToShort('|').ToShort(22);
            }
            return res;
        }
        //[HttpPost]
        //public async Task<decimal> TopUpBalanceOfStudents(TopUpBalanceOfStudents obj)
        //{
        //    return await CompanyRepo.TopUpBalanceOfStudents(Id, obj);
        //}

        public async Task<IEnumerable<object>> GetLessonsOfStudent(Guid studentId, int? week, string localtime)
        {
            var weeks = GetCurrentWeek(week, localtime);
            return await CompanyRepo.GetCalendarOfStudent(studentId, (short)weeks);
        }

        public async Task<IEnumerable<object>> GetCalendarForAllStudents(int? week, string localtime)
        {
            var weeks = GetCurrentWeek(week, localtime);
            return await CompanyRepo.GetCalendarAllStudents(Id, (short) weeks);
        }

        public async Task<object> GetScheduleStatistic()
        {
            return await CompanyRepo.GetScheduleStatistic(Id);
        }
        [CultureWebApi]
        public async Task<object> GetStats(string chat)
        {
            var res = await CompanyRepo.GetStatisticByPeriod(Id, chat);
            var labels = new List<string>();
            var data = new List<decimal>();
            if (chat == "monthly")
            {
                var datas = res.GroupBy(x => x.CreationDate.Month).Select(x=> new {month=x.Key, amount= x.Sum(f => f.TotatPrice)}).ToArray();
                for (var i = 1; i < DateTime.UtcNow.Month + 1; i++)
                {
                    labels.Add(CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i));
                    data.Add(datas.FirstOrDefault(x=>x.month == i)?.amount??0);
                }
            }
            if (chat == "weekly")
            {
                var date = DateTime.UtcNow;
                var date1 = new DateTime(date.Year,date.Month,1);
                var st = date1;
                var p = 1;
                while ((date1 = date1.AddDays(7)).Month == date.Month)
                {
                    labels.Add(p.ToString());
                    var t = res.Where(x => x.CreationDate > st && x.CreationDate < date1).Sum(x => x.TotatPrice);
                    data.Add(t);
                    st = st.AddDays(7);
                    p++;
                }
            }
            if (chat == "daily")
            {
                var date = DateTime.UtcNow;
                var countDaysInMonth = DateTime.DaysInMonth(date.Year, date.Month);
                for (var i = 1; i <= countDaysInMonth; i++)
                {
                    labels.Add(i.ToString());
                    data.Add(
                        res.Where(x => new DateTime(x.CreationDate.Year, x.CreationDate.Month, x.CreationDate.Day)  == new DateTime(date.Year, date.Month, i)).Sum(x=>x.TotatPrice));
                }
            }
            if (chat == "yearly")
            {
                //var datas = res.GroupBy(x => x.CreationDate.Month).Select(x => new { month = x.Key, amount = x.Sum(f => f.Amount) }).ToArray();
                labels.AddRange(new []{"2016","2017","2018"});
                labels.ForEach(x=>{data.Add(res.Where(f=>f.CreationDate.Year == int.Parse(x)).Sum(f=>f.TotatPrice));});
            }
            return new { labels, data };
        }

        public async Task<object> GetTotalStatistic()
        {
            var res = await CompanyRepo.GetTotalStatistic(Id);
            var date = DateTime.UtcNow;
            var firstDate = new DateTime(date.Year, date.Month, 1);
            var thisMonth = res.Where(x => x.CreationDate > firstDate && x.CreationDate < date).Sum(x => x.TotatPrice);
            var lastMonth = res.Where(x => x.CreationDate.Month == date.Month - 1).Sum(x => x.TotatPrice);
            var total = res.Sum(x => x.TotatPrice);
            return new { thisMonth, lastMonth, total};
        }


        protected override void Dispose(bool disposing)
        {
            CompanyRepo.Dispose();
            base.Dispose(disposing);
        }
        [System.Web.Http.NonAction]
        private int GetCurrentWeek(int? week, string localtime)
        {
            if (week != null) return (int)week;
            var utc = DateTime.UtcNow;
            week = CultureInfo.GetCultureInfo("en-GB").Calendar.GetWeekOfYear(utc,
                CultureInfo.CurrentCulture.DateTimeFormat.CalendarWeekRule,
                DayOfWeek.Sunday);
            if (localtime.Contains("."))
            {
                var hours = localtime.Split('.');
                var local = int.Parse(hours[0]) * -1;
                if (hours[1].Length == 1)
                    hours[1] = hours[1] + "0";
                var min = (int.Parse(hours[1]) * 0.6) * -1;
                if (utc.DayOfWeek == DayOfWeek.Saturday && utc.AddHours(local).DayOfWeek == DayOfWeek.Sunday)
                    week++;
                if (utc.DayOfWeek == DayOfWeek.Saturday && utc.AddHours(local).AddMinutes(min).DayOfWeek == DayOfWeek.Sunday)
                    week++;

            }
            else
            {
                var local = int.Parse(localtime) * -1;
                if (utc.DayOfWeek == DayOfWeek.Saturday && utc.AddHours(local).DayOfWeek == DayOfWeek.Sunday)
                    week++;
            }
            return (int)week;
        }
    }
}
