﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DTO;
using Microsoft.AspNet.Identity;
using Repositories.Abstract;

namespace Xencore.Areas.Dashboard.Controllers.api.company
{
    [Authorize(Roles = "Company")]
    public class PackagesOfLessonsController : ApiController
    {
        private IPackagesCompany Repo { get; }

        public PackagesOfLessonsController(IPackagesCompany repo)
        {
            Repo = repo;
        }
        [HttpGet]
        public async Task<IEnumerable<CompanyAvailablePackagesDto>> GetAvailablePackages()
        {
            return await Repo.GetAvailablePackages(Guid.Parse(User.Identity.GetUserId()));
        }
        [HttpPost]
        public async Task<bool> SplitPackage(SplitPackageContainer data)
        {
            return await Repo.SplitPackageTo(data.PackageId, data.StudentIds, data.Count);
        }

        protected override void Dispose(bool bl)
        {
            Repo?.Dispose();
            base.Dispose(bl);
        }
    }

    public class SplitPackageContainer
    {
        public int PackageId { get; set; }
        public IEnumerable<Guid> StudentIds { get; set; }
        public int Count { get; set; }
    }
}
