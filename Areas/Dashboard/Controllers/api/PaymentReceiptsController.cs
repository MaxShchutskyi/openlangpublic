﻿using Domain.Context;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Xencore.Areas.Dashboard.Controllers.api
{
    [Authorize]
    public class PaymentReceiptsController : ApiController
    {
        public async Task<IEnumerable<object>> GetPaymentReceipts()
        {
            var userId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                return await context.PaymentTransactions.Where(x => x.AttendeeId == userId).Where(x => x.Approved).OrderByDescending(x => x.CreateDate)
                    .Select(x => new
                    {
                        x.PaymentId,
                        x.CreateDate,
                        x.Amount,
                        x.CurrenceAmount,
                        x.Bonus,
                        x.PaymentMethod
                    }).ToArrayAsync();
                //return View(result);
            }
        }
    }
}
