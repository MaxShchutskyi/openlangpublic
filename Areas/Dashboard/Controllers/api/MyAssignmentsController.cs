﻿using System.Threading.Tasks;
using System.Web.Http;
using DTO;
using Repositories.Abstract;

namespace Xencore.Areas.Dashboard.Controllers.api
{
    [System.Web.Http.Authorize(Roles = "Presenter")]
    public class MyAssignmentsController : ParrentAssingmentController
    {
        private IMyAssingmentsOfTeachersRepository Repo { get; }

        public MyAssignmentsController(IMyAssingmentsOfTeachersRepository repo):base(repo)
        {
            Repo = repo;
        }
        [System.Web.Http.HttpPost]
        public async Task<AssingmentDto> SaveChanges([FromBody] AssingmentDto helper)
        {
            return await Repo.SaveChanges(helper, UserId);
        }
        [System.Web.Http.HttpPost]
        public async Task<bool> ChangeStatus([FromBody] AssingmentDto helper)
        {
            return await Repo.ChangeStatus(helper);
        }
        [System.Web.Http.HttpPost]
        public async Task<AssingmentDto> RemoveMessage([FromBody] AssingmentDto helper)
        {
            var path = await Repo.RemoveAudioMessage(helper);
            helper.UploadMessagePath = null;
            RemoveFile(path);
            return helper;
        }
        [System.Web.Http.HttpPost]
        public async Task<AssingmentDto> RemoveUploadedFile([FromBody] AssingmentDto helper)
        {
            var path = await Repo.RemoveAttachedFile(helper);
            helper.UploadFilePath = null;
            RemoveFile(path);
            return helper;
        }
    }
}
