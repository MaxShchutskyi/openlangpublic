﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Domain.Context;
using Domain.Entities;
using Microsoft.AspNet.Identity;
using Repositories.Abstract;
using WebGrease.Css.Extensions;

namespace Xencore.Areas.Dashboard.Controllers.api
{
    public class ParrentAssingmentController : ApiController
    {
        protected Guid UserId => Guid.Parse(User.Identity.GetUserId());
        private IParrentAssingntRepo Repo { get; }

        protected ParrentAssingmentController(IParrentAssingntRepo repo)
        {
            Repo = repo;
        }
        [System.Web.Http.HttpPost]
        public async Task<int> GetActiveAssingments()
        {
            return await Repo.GetCountActiveAssingments(UserId);
        }
        [System.Web.Http.HttpGet]
        public async Task<IEnumerable<object>> GetMyAssingment(int page)
        {
            var timezome = int.Parse(Request.Headers.GetCookies()[0].Cookies.First(x => x.Name == "timezone").Value);
            var res = await Repo.GetStudentAssingments(UserId, page);
            res = res.Select(x => x.SetQuealsDateTime(timezome));
            res.ForEach(x => x.State = x.StateAssingment.ToString());
            return res;
        }
        [NonAction]
        protected void RemoveFile(string path)
        {
            var directory = HttpContext.Current.Server.MapPath("~/");
            if (File.Exists(Path.Combine(directory, path)))
                File.Delete(Path.Combine(directory, path));
        }
    }
}
