﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Domain.Context;
using Domain.Entities;
using DTO;
using DTO.Enums;
using Microsoft.AspNet.Identity;
using Repositories.Abstract;
using WebGrease.Css.Extensions;

namespace Xencore.Areas.Dashboard.Controllers.api
{
    [System.Web.Http.Authorize]
    public class MyAssignmentsStudentController : ParrentAssingmentController
    {
        private IMyAssingmentsOfStudentRepository Repo { get; }


        public MyAssignmentsStudentController(IMyAssingmentsOfStudentRepository repo):base(repo)
        {
            Repo = repo;
        }
        [System.Web.Http.HttpPost]
        public async Task<AssingmentDto> SaveAnswerAssingment(AssingmentDto helper)
        {
            if (helper == null)
                return null;
            return await Repo.SaveChanges(helper);
        }

        protected override void Dispose(bool disposing)
        {
            Repo?.Dispose();
            base.Dispose(disposing);
        }
    }
}