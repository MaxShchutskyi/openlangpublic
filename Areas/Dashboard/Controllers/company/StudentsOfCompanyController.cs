﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CsQuery.ExtensionMethods;
using Microsoft.AspNet.Identity;
using NLog;
using Repositories.Abstract;
using Xencore.Extentions;
using Xencore.Filters;

namespace Xencore.Areas.Dashboard.Controllers.company
{
    [Culture(IsCookieOnly = true)]
    [Authorize(Roles = "Company")]
    [GlobalUserDetect]
    public class StudentsOfCompanyController : Controller
    {
        private IStudentsOfCompanyRepository Repo { get; }

        public StudentsOfCompanyController(IStudentsOfCompanyRepository repo)
        {
            Repo = repo;
        }

        public async Task<ActionResult> Index()
        {
            try
            {
                var res = await Repo.GetStudentsCompany(Guid.Parse(User.Identity.GetUserId()));
                res.ForEach(x => x.ILearn = x.ILearn.ISomethingToShort('|'));
                return View("~/Areas/Dashboard/Views/Company/StudentsOfCompany/GetCompanyStudents.cshtml", res);
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error(e);
                return View("~/Areas/Dashboard/Views/Company/StudentsOfCompany/GetCompanyStudents.cshtml", null);

            }
        }

        protected override void Dispose(bool disposing)
        {
            Repo.Dispose();
            base.Dispose(disposing);
        }
    }
}