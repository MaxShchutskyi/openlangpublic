﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Domain.Context;
using Microsoft.AspNet.Identity;
using Xencore.Filters;

namespace Xencore.Areas.Dashboard.Controllers.company
{
    [Culture(IsCookieOnly = true)]
    [Authorize(Roles = "Company")]
    [GlobalUserDetect]
    public class PackageTrackerController : Controller
    {
        public async Task<ActionResult> Index()
        {
            var companyId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var res = await context.Credits.Where(x => x.StudentId == companyId).Include(x => x.CreditsOfCompany.Select(f=>f.PieceOfCredit.Student))
                    .Include(x => x.ScheduleTransactions.Select(f => f.Schedule.CustomUser)).ToListAsync();
                return View("~/Areas/Dashboard/Views/Company/PackageTracker/Index.cshtml", res);
            }
        }
    }
}