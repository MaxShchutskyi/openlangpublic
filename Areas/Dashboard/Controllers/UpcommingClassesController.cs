﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Context;
using Domain.Entities.HelpersEntity;
using Xencore.Extentions;
using Xencore.Filters;

namespace Xencore.Areas.Dashboard.Controllers
{
    [Culture(IsCookieOnly = true)]
    [Authorize]
    [GlobalUserDetect]
    public class UpcommingClassesController : ParentController
    {
        [Timezone]

        public async Task<ActionResult> GetUpcommingStudentClasses(int take, string template, short timezone)
        {
            using (var context = new MyDbContext())
            {
                var utc = DateTime.UtcNow;
                var user = await
                    context.ScheduleTransactions.Where(
                            x => x.CustomUserId == GetUserId && x.Schedule.StartDateTimeDateTime > utc)
                        .OrderByDescending(x => x.Schedule.StartDateTimeDateTime)
                        .Take(take).Select(x => x.Schedule).Select(x => new
                        {
                            x.Id,
                            x.StartDateTimeDateTime,
                            x.Level,
                            x.Type,
                            x.Language,
                            x.CustomUser.ProfilePicture,
                            x.CustomUser.Name
                        }).ToListAsync();
                var res = user.Select(x => new UpcomingPastClassesTemplate
                {
                    Id = x.Id,
                    SatrtDateTime = x.StartDateTimeDateTime.AddMinutes(timezone),
                    ProfilePicture = x.ProfilePicture,
                    Name = x.Name,
                    Level = x.Level.ToNullIfEmpty(),
                    Language = x.Language.ToNullIfEmpty(),
                    Type = x.Type
                });
                return View(template, res);
            }
        }
        [Timezone]

        public async Task<ActionResult> GetUpcommingClasses(int take, string template, short timezone)
        {
            using (var context = new MyDbContext())
            {
                var utc = DateTime.UtcNow;
                var user = await
                    context.Schedules.Where(x => x.StartDateTimeDateTime > utc && x.CustomUserId == GetUserId)
                        .Include(x => x.ScheduleTransactions.Select(f => f.User)).Where(x => x.ScheduleTransactions.Any())
                        .OrderByDescending(x => x.StartDateTimeDateTime).Take(take)
                        .Select(x => new
                        {
                            x.Id,
                            x.StartDateTimeDateTime,
                            x.Level,
                            x.Language,
                            x.Type,
                            x.ScheduleTransactions.FirstOrDefault().User.ProfilePicture,
                            x.ScheduleTransactions.FirstOrDefault().User.Name,
                        }).ToListAsync();
                var res = user.Select(x => new UpcomingPastClassesTemplate
                {
                    Id = x.Id,
                    SatrtDateTime = x.StartDateTimeDateTime.AddMinutes(timezone),
                    ProfilePicture = x.ProfilePicture,
                    Name = x.Name,
                    Level = x.Level.ToNullIfEmpty(),
                    Language = x.Language.ToNullIfEmpty(),
                    Type = x.Type
                }).ToList();
                return View(template, res);
            }
        }
        [Authorize(Roles = "Company")]
        [Timezone]
        public async Task<ActionResult> GetUpcommingCompanyClasses(int take, string template, short timezone)
        {
            using (var context = new MyDbContext())
            {
                var usersIds = await context.Users.Where(x => x.CompanyId == GetUserId).Select(x => x.Id).ToArrayAsync();
                var utc = DateTime.UtcNow;
                var user = await
                    context.ScheduleTransactions.Where(
                            x => usersIds.Contains(x.CustomUserId) && x.Schedule.StartDateTimeDateTime > utc && x.Schedule.ScheduleTransactions.Any())
                        .OrderByDescending(x => x.Schedule.StartDateTimeDateTime)
                        .Take(take).Select(x => x.Schedule).Select(x => new
                        {
                            x.Id,
                            x.StartDateTimeDateTime,
                            x.Level,
                            x.Type,
                            x.Language,
                            x.ScheduleTransactions.FirstOrDefault(f => usersIds.Contains(f.CustomUserId)).User.ProfilePicture,
                            x.ScheduleTransactions.FirstOrDefault(f => usersIds.Contains(f.CustomUserId)).User.Name,
                        }).ToListAsync();
                var res = user.Select(x => new UpcomingPastClassesTemplate
                {
                    Id = x.Id,
                    SatrtDateTime = x.StartDateTimeDateTime.AddMinutes(timezone),
                    ProfilePicture = x.ProfilePicture,
                    Name = x.Name,
                    Level = x.Level.ToNullIfEmpty(),
                    Language = x.Language.ToNullIfEmpty(),
                    Type = x.Type
                });
                return View(template, res);
            }
        }
    }



}