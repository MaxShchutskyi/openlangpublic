﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CurrencyConverter;
using Domain.Context;
using Domain.Entities.HelpersEntity;
using DTO.Enums;
using GeolocationManager.Models;
using Repositories.Abstract;
using TeacherMonthlyEarnings;
using Xencore.Filters;
using Xencore.Managers.TablesOfPrices.Abstract;

namespace Xencore.Areas.Dashboard.Controllers
{
    [Culture(IsCookieOnly = true)]
    [Authorize]
    [GlobalUserDetect]
    public class MonthlyEarningsController : ParentController
    {
        private IAbstractSchedulerRepository Repo { get; set; }

        public MonthlyEarningsController(IAbstractSchedulerRepository repo)
        {
            Repo = repo;
        }

        public ActionResult GetMonthlyEarnings()
        {
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> GetMyMonthlyEarnings(UserInfo usr)
        {
            var money = new TeacherMonthlyEarningsManager().CalculateEarnings(GetUserId);
            var countlessons = Repo.GetCountActiveLessonsByTeacherId(GetUserId);
            var user = GetUserManager.FindByIdAsync(GetUserId);
            await Task.WhenAll(money, countlessons, user);
            var currency = user.Result.Currency;
            string nativeMoney;
            if (!string.IsNullOrEmpty(currency) && !currency.Contains("EUR"))
                nativeMoney =
                    Math.Round(
                        CurrencyConverterFactory.Create<string>()
                            .Convert(currency.Split(':')[0], "EUR", money.Result)
                            .Result, 2) + " " + currency.Split(':')[0];
            else
                nativeMoney =
                    Math.Round(
                        CurrencyConverterFactory.Create<string>().Convert(usr.CurrentCurency.Split(':')[0], "EUR", money.Result).Result,
                        2) + " " + usr.CurrentCurency.Split(':')[0];
            return Json(new MoneyEarningsView { CountLessons = countlessons.Result, Money = money.Result, NativeMoney = nativeMoney });

        }
    }
}