﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xencore.Filters;

namespace Xencore.Areas.Dashboard.Controllers
{
    [Culture(IsCookieOnly = true)]
    [Authorize]
    [GlobalUserDetect]
    public class MyClassesController : ParentController
    {
        public ActionResult GetMyClasses()
        {
            return View();
        }
    }
}