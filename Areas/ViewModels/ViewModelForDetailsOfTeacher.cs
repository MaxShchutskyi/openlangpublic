﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Domain.Entities.IdentityEntities;
using Xencore.Entities.IdentityEntities;

namespace Xencore.Areas.ViewModels
{
    public class ViewModelForDetailsOfTeacher
    {
        public CustomUser CustomUser { get; set; }
        [Display(Name = "Password")]
        [Required]
        [MaxLength(10)]
        public string Password { get; set; }
        [Required]
        [MaxLength(10)]
        [Compare("Password")]
        [MinLength(6)]
        [Display(Name = "Confirm password")]
        public string ConfirmPassword { get; set; }
        [Required]
        [MaxLength(100)]
        [DataType(DataType.Url)]
        [Display(Name = "Link of video")]
        public string Url { get; set; }
    }
}