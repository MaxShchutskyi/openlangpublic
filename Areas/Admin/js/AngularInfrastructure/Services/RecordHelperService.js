﻿angular.module("recorderService", ['notifications'])
    .factory("recorderService",["$http",'notifications', function ($http, notifications) {
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
        window.URL = window.URL || window.webkitURL;
        var audio_context = new AudioContext;
        var localStream;
        var recorder;
        var path;
        var callback;
        function startUserMedia(stream) {
            localStream = stream;
            var input = audio_context.createMediaStreamSource(stream);
            recorder = new Recorder(input);
            recorder && recorder.record();
        }
        function startRecord(pt, call) {
            path = pt;
            callback = call;
            navigator.getUserMedia({ audio: true }, startUserMedia, function (e) {
                notifications.show('error', resource.match('you_disabled_assess'));
            });
        }
        function stopRecord(call) {
            recorder && recorder.stop();
            createDownloadLink(call);
            recorder.clear();
            localStream.getAudioTracks()[0].stop();
        }
        function createDownloadLink(cal) {
            recorder && recorder.exportWAV(function (blob) {
                var url = URL.createObjectURL(blob);
                var data = new FormData();
                data.append("blob", blob, "download.wav");
                if (cal)
                    data = cal(data)
                $http.post(path, data, {
                    withCredentials: true,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                }).success(callback);
            });
        }
        return {
            start: startRecord,
            stop: stopRecord
        }
    }]);