﻿var calendarHelper = function () {
    var calendar;
    function prevClick(e) {
        $('button.fc-prev-button.fc-button.fc-state-default.fc-corner-left').click();
        console.log(calendar.week);
        var numb = calendar.week - 1;
        calendar.week = numb;
        console.log(calendar.week);
        if ($.inArray(numb, calendar.listOfShowedWeeks) != -1)
            return;
        calendar.listOfShowedWeeks.push(numb);
        calendar.query(function(result) {
            calendar.jQueryElem.fullCalendar("addEventSource", result);
        });
    }
    function prevClickDay(e) {
        //$('button.fc-prev-button.fc-button.fc-state-default.fc-corner-left').click();
        //console.log(calendar.week);
        //var numb = calendar.week - 1;
        //calendar.week = numb;
        //console.log(calendar.week);
        //if ($.inArray(numb, calendar.listOfShowedWeeks) != -1)
        //    return;
        //calendar.listOfShowedWeeks.push(numb);
        //calendar.query(function (result) {
        //    calendar.jQueryElem.fullCalendar("addEventSource", result);
        //});
    }
    function setCalendar(cal) {
        calendar = cal;
    }
    return {
        setCalendar: setCalendar,
        prevClick: prevClick
    }
}()