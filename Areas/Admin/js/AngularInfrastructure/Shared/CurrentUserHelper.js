﻿var currentUserHelper = (function () {
    var user = $.cookie("Usr");
    if (user)
        var userObj = JSON.parse(user);
    function getCountry() {
        return userObj.CountryCode;
    }
    function getTimezone(){
        return userObj.Timezone;
    }
    return {
        getCountry: getCountry,
        getTimezone: getTimezone
    }
})()