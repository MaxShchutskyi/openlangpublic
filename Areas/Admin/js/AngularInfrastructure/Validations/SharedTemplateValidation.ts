﻿class TemplateValidation2 {
    private errPlace: (error, element) => void = this.errorPlacement;
    private signuperrorplacement: JQuery;
    init(formId, errorPalce, callback: (param: HTMLFormElement)=>void):void {
        this.signuperrorplacement = $(errorPalce);
        jQuery(formId).validate({
            errorPlacement: this.errPlace,
            highlight: (element) => {
                $(element).addClass("error").next().find(".select2-selection").addClass("error");
            },
            unhighlight: (element) => {
                $(element).removeClass("error").next().find(".select2-selection").removeClass("error");
            },
            submitHandler: callback
        });
    }
    public setSettings(options:any):void {
        if (options.hasOwnProperty('errorPlacement'))
            this.errPlace = options.errorPlacement;
    }
    private errorPlacement(error, element) {
        this.signuperrorplacement.append(error);
    }
}