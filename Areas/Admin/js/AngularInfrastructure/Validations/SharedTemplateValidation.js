var TemplateValidation2 = /** @class */ (function () {
    function TemplateValidation2() {
        this.errPlace = this.errorPlacement;
    }
    TemplateValidation2.prototype.init = function (formId, errorPalce, callback) {
        this.signuperrorplacement = $(errorPalce);
        jQuery(formId).validate({
            errorPlacement: this.errPlace,
            highlight: function (element) {
                $(element).addClass("error").next().find(".select2-selection").addClass("error");
            },
            unhighlight: function (element) {
                $(element).removeClass("error").next().find(".select2-selection").removeClass("error");
            },
            submitHandler: callback
        });
    };
    TemplateValidation2.prototype.setSettings = function (options) {
        if (options.hasOwnProperty('errorPlacement'))
            this.errPlace = options.errorPlacement;
    };
    TemplateValidation2.prototype.errorPlacement = function (error, element) {
        this.signuperrorplacement.append(error);
    };
    return TemplateValidation2;
}());
//# sourceMappingURL=SharedTemplateValidation.js.map