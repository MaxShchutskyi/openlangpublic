﻿attendeescontroller.$inject = ["$scope", "$location", "$interval", "api", "$animate"];
function attendeescontroller($scope, $location, $interval, api, $animate) {
  setTimeout(function() {
    $(".attende-table")
      .dataTable({
        "autoWidth": true,
        "ordering": false,
        "oLanguage": {
          "sSearch": "",
        },
        language: {
          searchPlaceholder: "Enter information to search..."
        },
        "dom": '<"top" flp>rt<"bottom"flp>'
      });
    $('.dataTables_filter input').attr("placeholder", "Enter information to search...");
    Chart.defaults.global.legend.display = false;
    var mainChart;
    var data = {
      //labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [
        {
          backgroundColor: 'rgba(153, 211, 50, 0.2)',
          pointBackgroundColor: 'rgb(153, 211, 50)',
          pointBorderColor: 'rgb(153, 211, 50)',
          pointHoverBackgroundColor: 'rgb(153, 211, 50)',
          pointHoverBorderColor: 'rgb(153, 211, 50)',
          pointHighlightStroke: "rgba(153, 211, 50,1)",
          //data: [65, 59, 80, 81, 56, 55, 40]
        },
      ]
    };

    function drawChart() {
      if (mainChart)
        mainChart.destroy();
      var ctx = document.getElementById("myChart").getContext("2d");
      ctx.clearRect(0, 0, ctx.width, ctx.height);
      mainChart = new Chart(ctx,
        {
          type: "line",
          data: data,
        });
    };


    $scope.getData = function (type, event) {
      if (event)
        $(event.currentTarget).parent().addClass('active').siblings().removeClass('active');
      $.get("/Admin/Attendee/GetStats?chat=" + type).done(function (res) {
        data.labels = res.labels;
        data.datasets[0].data = res.data;
        drawChart();
      });
    }

    $scope.getData('monthly');
    $('.dataTables_length select').select2();
    if ($(window).width() <= '768') {
        $('.admin #table_wrapper .top').prependTo('.table-mobile-wrap');
    }
    jQuery('.page-preloader-wrap-dashboard').hide();
  });
}