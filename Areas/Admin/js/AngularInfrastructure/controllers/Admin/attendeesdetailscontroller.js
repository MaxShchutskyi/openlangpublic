﻿
attendeesdetailscontroller.$inject = ["$scope", "$location", "$interval", "api", "$animate","$routeParams"];
function attendeesdetailscontroller($scope, $location, $interval, api, $animate, $routeParams) {
    console.log($routeParams.id);
    $(".info-table").dataTable({
        "autoWidth": true,
        "ordering": false,
        "oLanguage": {
            "sSearch": "",
        },
        language: {
            searchPlaceholder: "Enter information to search..."
        },
        "dom": '<"top" flp>rt<"bottom"flp>'
    });
    $('.dataTables_filter input').attr("placeholder", "Enter information to search...");
    $('.dataTables_length select').select2();
    jQuery('.page-preloader-wrap-dashboard').hide();
}