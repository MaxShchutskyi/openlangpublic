﻿teachersdetailscontroller.$inject = ["$scope", "$location", "$interval", "api", "$animate", "$routeParams"];

function teachersdetailscontroller($scope, $location, $interval, api, $animate, $routeParams) {
  console.log($routeParams.id);
  $scope.password = {};
  $scope.rates = {};
  $scope.changePassword = function() {
    var model = { id: $routeParams.id, password: $scope.password.password, confirmPassword: $scope.password.confirmPassword };
    api.ajax('/Admin/Teachers/ChangePassword',
      'post',
      JSON.stringify(model),
      function() {
        alert("Password changed");
      });
  }
  $scope.saveRates = function() {
    var model = {
      userId: $routeParams.id,
      Individual45Rate: $scope.rates.basic,
      Individual60Rate: $scope.rates.premium,
      GroupRate: $scope.rates.group
    };
    api.ajax('/Admin/Teachers/ChangeRate',
      'post',
      JSON.stringify(model),
      function () {
        alert("Rates was changed");
      });
    };
    $scope.sendContract = function() {
      api.ajax('/Admin/Teachers/SendConfirmationAboutRegistration', 'post', JSON.stringify({ id: $routeParams.id}), function(res) {
        alert("Contract was sent");
      });
  }
  //$scope.ispeak = ["us"];
    $scope.changeTeach = function () {
      api.ajax('/Admin/Teachers/ChangeITeachn', 'post', JSON.stringify({ id: $routeParams.id, langs: $scope.iteach }), function(res) {
        alert("saved");
      });
    console.log($scope.iteach);
  }
  $scope.changeSpeak = function () {
    api.ajax('/Admin/Teachers/ChangeISpeak', 'post', JSON.stringify({ id: $routeParams.id, langs: $scope.ispeak }), function (res) {
      alert("saved");
    });
  }
  $scope.getJson = function(data) {
    return data.split(",");
  }
  $scope.saveVideoLink = function() {
    api.ajax("/Admin/Teachers/ChangeVideoLink",
      'post',
      JSON.stringify({ id: $routeParams.id, url: $scope.videoLink }),
      function(res) {
        alert("saved");
      });
  }
  var url = "https://www.youtube.com/embed/";
  var urlforsave = "https://www.youtube.com/watch?v=";
  var vimeo = "https://player.vimeo.com/video/";
  $scope.$watch('videoLink',
    function(news, old) {
      if (news === old || !news || news.indexOf("embed") >= 0 || news.indexOf("player.vimeo.com") > -1) return;
      if (news.indexOf("youtube") > -1) {
        var arr = gup('v', news);
        $scope.videoLink = url + arr;
      }
      else if (text.indexOf("vimeo")) {
        var ar = news.split('/');
        var ids = ar[ar.length - 1];
        $scope.videoLink = vimeo + ids;
        //return;
      }
    });
  function gup(name, url) {
    if (!url) url = location.href;
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    return results == null ? null : results[1];
  }
    //$('.dataTables_length select').select2();
    jQuery('.page-preloader-wrap-dashboard').hide();
}