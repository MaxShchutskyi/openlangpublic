﻿cardsdetailcontroller.$inject = ["$scope", "$location", "$interval", "api", "$animate", "$routeParams","$http"];
function cardsdetailcontroller($scope, $location, $interval, api, $animate, $routeParams, $http) {
    if ($routeParams.id && $routeParams.id.toString() !== "0") {
        api.ajax('/api/BrainCard/GetCardById?id=' + $routeParams.id,
            'get',
            null,
            function (res) {
                console.log(res.Answers);
                res.Type = res.Type.toString();
                //res.Answers.forEach(function(x) {x.IsRight = x.IsRight?"true":"false"});
                $scope.card = res;
                $scope.card.LessonId = $routeParams.lessid;
                $scope.card.currentType = res.Type.toString();
                checkNewData();
            });
    } else {
        $scope.card = { Answers: [], Type: '0', Level: "Beginner" };
        if ($routeParams.lessid)
            $scope.card.LessonId = $routeParams.lessid;
        $scope.tests = [];
        $scope.initLang = function () {
            $scope.card.Language = $("#lng option").first().val();
        }
    }
    $scope.backTo = function () {
        $location.path(getPath());
        return;
    }
    $scope.changeTypeOfCard = function (ind) {
        $scope.card.Type = ind;
        //$scope.card.checkData();
    }
    function getPath() {
        return "lessons/" + $routeParams.chapterId + "/lessondetails/" + $routeParams.lessid
    }
    function checkNewData() {
        switch ($scope.card.Type) {
            case "1":
                {
                    setTimeout(function () {
                        $scope.card.checkData();
                        $scope.$apply();
                    }, 100);
                    $scope.tests = [];
                }
                break;
            case "3":
                {
                    setTimeout(function () {
                        $scope.card.checkData();
                        $scope.$apply();
                    }, 100);
                    $scope.images = [];
                }
                break;
            case "0":
                {
                    setTimeout(function () {
                        $scope.card.checkData();
                        $scope.$apply();
                    }, 100);
                    $scope.images = [];
                };
                break;
            case "2":
            case "4":
                {
                    $scope.textAnswer = $scope.card.Answers[0];
                    $scope.tests = [];
                    $scope.images = [];
                };
                break;
            case "7":
                {
                    setTimeout(function () {
                        $scope.card.checkData();
                        $scope.$apply();
                    }, 100);
                }
                break;
            case "8":
                {
                    setTimeout(function () {
                        $scope.card.checkData();
                        $scope.$apply();
                    }, 100);

                }
                break;
            case "12":
                {
                    setTimeout(function () {
                        $scope.card.checkData();
                        $scope.$apply();
                    }, 100);

                }
                break;
            case "13":
                {
                    setTimeout(function () {
                        $scope.card.checkData();
                        $scope.$apply();
                    }, 100);

                }
                break;
        }
        $scope.$digest();
        setTimeout(function () {
            $(".cards-details .field-wrap select").select2();
        }, 500);
    }
    console.log("cardsdetailcontroller");
    //$(".cards-details .field-wrap select").select2();
    jQuery('.page-preloader-wrap-dashboard').hide();
    $scope.addCard = function () {
        $scope.card.answers = [];
        console.log($scope.card);
        switch ($scope.card.Type) {
            case '1':
                {
                    var res = $scope.card.addCard();
                    if (res.Error) {
                        alert(res.Error);
                        return;
                    }
                    //$scope.card.Answers = $scope.images.filter(function (el) { return !el.isDeleted; });
                }
                break;
            case '3':
                {
                    var resr = $scope.card.addCard();
                    if (resr.Error) {
                        alert(resr.Error);
                        return;
                    }
                }
                break;
            case '0':
                {
                    var resr7 = $scope.card.addCard();
                    if (!resr7)
                        return;
                };
                break;
            case '2':
            case '4':
                $scope.card.Answers.push($scope.textAnswer);
                break;
            case '5':
                {
                    var resr3 = $scope.card.addCard();
                    if (!resr3)
                        return;
                };
                break;
            case '6':
                {
                    var resr2 = $scope.card.addCard();
                    if (!resr2)
                        return;
                };
                break;
            case '7':
                {
                    var resr = $scope.card.addCard();
                    if (resr.Error) {
                        alert(resr.Error);
                        return;
                    }
                };
                break;
            case '8':
                {
                    var resr22 = $scope.card.addCard();
                    if (resr22.Error) {
                        alert(resr22.Error);
                        return;
                    }
                }
            case '12':
                {
                    var resr12 = $scope.card.addCard();
                    if (resr12.Error) {
                        alert(resr12.Error);
                        return;
                    }
                };
                break;
            case '13':
                {
                    var resr13 = $scope.card.addCard();
                    if (resr13.Error) {
                        alert(resr13.Error);
                        return;
                    }
                };
                break;
        }
        if ($scope.card.isNewRecord) {
            var data = new FormData();
            data.append("blob", $scope.card.VideoSrc, "download.wav");
            $http.post("/en/UploadImages/UploadAudioPath", data, {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            }).success(function (res) {
                $scope.card.VideoSrc = res.path;
                saveCard();
            });
        }
        saveCard();
    }
    function saveCard() {
        if ($scope.card.Id) {
            api.ajax('/api/BrainCard/UpdateCard',
                'post',
                JSON.stringify($scope.card),
                function (res) {
                    debugger;
                    res.Type = res.Type.toString();
                    //res.Answers.forEach(function(x) { x.IsRight = x.IsRight ? "true" : "false" });
                    $scope.card.Id = res.Id;
                    console.log(res);
                    checkNewData();
                    alert("Saved");
                    $location.path(getPath());
                    $scope.$apply();
                });
        } else {
            api.ajax('/api/BrainCard/AddNewCard',
                'post',
                JSON.stringify($scope.card),
                function (res) {
                    res.Type = res.Type.toString();
                    //res.Answers.forEach(function(x) { x.IsRight = x.IsRight ? "true" : "false" });
                    $scope.card = res;
                    checkNewData();
                    alert("Saved");
                    $location.path(getPath());
                    $scope.$apply();
                });
        }
    }
}

