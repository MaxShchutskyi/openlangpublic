﻿companiescontroller.$inject = ["$scope", "$location", "$interval", "api", "$animate"];
function companiescontroller($scope, $location, $interval, api, $animate) {
  $scope.dtOptions = {
    "autoWidth": true,
    "ordering": false,
    "oLanguage": {
      "sSearch": "",
    },
    language: {
      searchPlaceholder: "Enter information to search..."
    },
    "dom": '<"top" flp>rt<"bottom"flp>'
  }
  api.ajax('/Admin/Companies/GetAllCompanies',
    'get',null,
    function(res) {
      $scope.companies = res;
      $scope.$digest();
    });
  $scope.onAddNewCompanyClick = function () {
    $scope.newCompany = {
      howDidFind: "company",
      timezone: "0",
      chosenLang: "en|",
      chosenLevel: "Beginner",
      iSpeak: "en|"
    };
    //console.log("newCompany");
  }
  $scope.save = function() {
    api.ajax("/Admin/Companies/AddCompany",
      'post',
      JSON.stringify($scope.newCompany),
      function (res) {
        res.CreationDateTime = "/Date(" + new Date().getTime() + ")/";
        res.Count = 0;
        $scope.companies.unshift(res);
        $scope.$digest();
      });
  }
  $scope.getCorrectDate = function (date) {
    //console.log(date);
    var milli = date.replace(/\/Date\((-?\d+)\)\//, '$1');
    return moment(parseInt(milli)).format("DD.MM.YYYY");
  }
    console.log("companiescontroller");
    //$('.dataTables_length select').select2();
    //jQuery('.page-preloader-wrap-dashboard').hide();
}