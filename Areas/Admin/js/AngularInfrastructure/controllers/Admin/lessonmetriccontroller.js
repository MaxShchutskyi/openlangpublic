﻿lessonmetriccontroller.$inject = ["$scope", "$location", "$interval", "api", "$animate"];
function lessonmetriccontroller($scope, $location, $interval, api, $animate) {
  $scope.dtOptions = {
    "autoWidth": true,
    "ordering": false,
    "oLanguage": {
      "sSearch": "",
    },
    language: {
      searchPlaceholder: "Enter information to search..."
    },
    "dom": '<"top" flp>rt<"bottom"flp>'
  }
  api.ajax('/Admin/LessonsMetric/GetData','get', null, function(res) {
    $scope.model = res;
    $scope.$digest();
  })
  $scope.getNewData = function(type,event) {
    $scope.selectedType = type;
    $(event.currentTarget).addClass('active').siblings().removeClass('active');
    api.ajax('/Admin/LessonsMetric/GetData',
      'get',
      { 'type': type },
      function(res) {
        $scope.model = res;
        $scope.$digest();
      });
  }
    console.log("lessonmetriccontroller");
    $('.dataTables_length select').select2();
    jQuery('.page-preloader-wrap-dashboard').hide();
}

