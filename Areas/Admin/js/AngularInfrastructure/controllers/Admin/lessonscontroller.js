﻿lessonscontroller.$inject = ["$scope", "$location", "$interval", "api", "$animate","$routeParams"];
function lessonscontroller($scope, $location, $interval, api, $animate, $routeParams) {
  console.log("lessonscontroller");
  $scope.lessons = [];
    var fixHelper = function (e, ui) {
        ui.children().each(function () {
            $(this).width($(this).width());
        });
        return ui;
    };
    $scope.sortableOptions = {
        stop: function (e, ui) {
            for (let i = 0; i < $scope.lessons.length; i++) {
                $scope.lessons[i].OrderId = i + 1;
            }
            var md = $scope.chapters.map(function (k) {
                return { Id: k.Id, orderId: k.OrderId };
            });
            api.ajax("/api/braincard/UpdateOrder", "post", JSON.stringify(md), function () {
                console.log("updateorder");
                $scope.$digest();
            });
        },
        helper: fixHelper
    };
  if ($routeParams.chapterId) {
    $scope.chapterId = $routeParams.chapterId;
    api.ajax("/api/lessons/GetLessonsByChapterId?id=" + $routeParams.chapterId,
      'get',
      null,
      function (res) {
        $scope.lessons = res;
        $scope.$digest();
      });
  }
  $scope.model = {};
  $scope.dtOptions = {
    "autoWidth": true,
    "ordering": false,
    "oLanguage": {
      "sSearch": "",
    },
    language: {
      searchPlaceholder: "Enter information to search..."
    },
    "dom": '<"top" flp>rt<"bottom"flp>'
  }
  jQuery('.page-preloader-wrap-dashboard').hide();
}