﻿chapterscontroller.$inject = ["$scope", "$location", "$interval", "api", "$animate","$http"];
function chapterscontroller($scope, $location, $interval, api, $animate, $http) {
    console.log("chapterscontroller");
    
    $scope.chapters = [];
    var fixHelper = function (e, ui) {
        ui.children().each(function () {
            $(this).width($(this).width());
        });
        return ui;
    };
    api.ajax('/api/Level/Get', 'get', null, function (res) {
        console.log(res);
        $scope.levels = res;
    });
    $scope.sortableOptions = {
        stop: function (e, ui) {
            for (let i = 0; i < $scope.chapters.length; i++) {
                $scope.chapters[i].Index = i + 1;
            }
            var md = $scope.chapters.map(function (k) {
                return { Id: k.Id, Index: k.Index };
            });
            api.ajax("/api/chapter/UpdateOrder", "post", JSON.stringify(md), function () {
                console.log("updateorder");
                $scope.$digest();
            });
        },
        helper: fixHelper,

    };
  $scope.initLang = function () {
    $scope.model.Language = $("#lng option").first().val();
  }
  $scope.initLevel = function () {
    $scope.model.Level = $("#lvl option").first().val();
    }
    $scope.initCategory = function () {
        $scope.model.Category = $("#category option").first().val();
    }
  api.ajax("/api/chapter/GetChaptersTable", 'get', null, function(res) {
    $scope.chapters = res;
    $scope.$digest();
  });
  $scope.model = {};
  $scope.dtOptions = {
    "autoWidth": true,
    "ordering": false,
    "oLanguage": {
      "sSearch": "",
    },
    language: {
      searchPlaceholder: "Enter information to search..."
    },
    "dom": '<"top" flp>rt<"bottom"flp>'
    }
    
  $scope.addChapter = function() {
    if (!$scope.chapterName) {
        console.log($scope.chapters);
      alert("You did not enter name of Chapter");
      return;
    }
    $("#myModal").modal('hide');
    api.ajax('/api/chapter/AddOrUpdateChapter',
      'post',
        JSON.stringify({ Name: $scope.chapterName, Language: $scope.model.Language, Level: $scope.model.Level, LevelId: $scope.model.Category}),
        function (res) {
            var chapter = { Name: $scope.chapterName, Language: $scope.model.Language, Level: $scope.model.Level, Id: res.Id, Creator: res.Creator, LevelId: res.LevelId, LevelName : $scope.levels.find(function (x) { return x.Id == $scope.model.Category }).Name };
          $scope.chapters.push(chapter);
          
          if (data) {
              data.append('chapterId', chapter.Id);
              $http.post('/Admin/Chapters/UploadChapterIcon', data, {
                  withCredentials: true,
                  headers: { 'Content-Type': undefined },
                  transformRequest: angular.identity
              }).success(function (res) {
                  chapter.IconSrc = res;
                  $scope.IconSrc = null;
                  data = null;
              });
          }
        $scope.$digest();
      });
    }
    
    $scope.uploadFileLogoTrigger = function () {
        $('.upload-logo-chapter-button').trigger("click");
    };
    var data;
    $scope.myFileLogo = function (input) {
        if (input.files && input.files[0]) {
            data = new FormData();
            data.append(input.files[0].name, input.files[0]);
            var reader = new FileReader();

            reader.onload = function (e) {
                $scope.IconSrc = e.target.result;
                $scope.$digest();
                //$(".upload-logo-btn").html("");
                //$('.upload-logo-btn').css('backgroundImage', 'url(' + e.target.result + ')');
            };

            reader.readAsDataURL(input.files[0]);
        }


    };


   
  jQuery('.page-preloader-wrap-dashboard').hide();
}