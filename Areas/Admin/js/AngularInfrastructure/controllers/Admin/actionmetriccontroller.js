﻿actionmetriccontroller.$inject = ["$scope", "$location", "$interval", "api", "$animate"];
function actionmetriccontroller($scope, $location, $interval, api, $animate) {
  console.log("actionmetriccontroller");
  $scope.dtOptions = {
    "autoWidth": true,
    "ordering": false,
    "oLanguage": {
      "sSearch": "",
    },
    language: {
      searchPlaceholder: "Enter information to search..."
    },
    "dom": '<"top" flp>rt<"bottom"flp>'
  }
  $scope.getData = function (type, event) {
    if (event) {
      $(event.currentTarget).addClass('active').siblings().removeClass('active');
    }
      api.ajax('/Admin/ActionsMetric/GetData' + "?type=" + type,
        'get',
        null,
        function(res) {
          $scope.model = res;
          $scope.$digest();
        });
    }
    //$('.dataTables_length select').select2();
  $scope.getData(null);
    jQuery('.page-preloader-wrap-dashboard').hide();
}
