﻿cardscontroller.$inject = ["$scope", "$location", "$interval", "api", "$animate"];
function cardscontroller($scope, $location, $interval, api, $animate) {
    console.log("cardscontroller");
  $scope.dtOptions = {
    "autoWidth": true,
    "ordering": false,
    "oLanguage": {
      "sSearch": "",
    },
    language: {
      searchPlaceholder: "Enter information to search..."
    },
    "dom": '<"top" flp>rt<"bottom"flp>'
  }
  api.ajax("/api/BrainCard/GetAllCards",
    "get",
    null,
    function (res) {
      console.log(res);
      $scope.cards = res;
      $scope.$digest();
    });
    $('.dataTables_filter input').attr("placeholder", "Enter information to search...");
    $('.dataTables_length select , .select-wrap select').select2();
    jQuery('.page-preloader-wrap-dashboard').hide();
}
