﻿discountscontroller.$inject = ["$scope","$timeout", "$location", "$interval", "api", "$animate"];
function discountscontroller($scope, $timeout, $location, $interval, api, $animate) {
  $scope.dtOptions = {
      "autoWidth": true,
      "ordering": false,
      "oLanguage": {
        "sSearch": "",
      },
      language: {
        searchPlaceholder: "Enter information to search..."
      },
      "dom": '<"top" flp>rt<"bottom"flp>'
    }
  api.ajax('/Admin/Discounts/GetDiscounts',
    'get',
    null,
    function (res) {
      console.log(res);
      $scope.discounts = res;
      $scope.$digest();
    });
  $scope.getCorrectDate = function(date)
  {
    var milli = date.replace(/\/Date\((-?\d+)\)\//, '$1');
    return moment(parseInt(milli)).format("DD.MM.YYYY");
  }
  $scope.onAddNewDiscountClick = function() {
    $scope.newDiscount = { type: "Packages", number:makeid(), isPercent:"true" };

  }
  $timeout(
    function () {
      $(".datepicker").datetimepicker({
        format: 'DD/MM/YYYY'
      });
    }, 500);
  $scope.save = function () {
    $scope.newDiscount.dateStart = $("#startdate").data("DateTimePicker").date().toISOString();
    $scope.newDiscount.dateEnd = $("#enddate").data("DateTimePicker").date().toISOString();
    api.ajax('/Admin/Discounts/AddDiscount',
      'post',
      JSON.stringify($scope.newDiscount),
      function(res) {
        $scope.discounts.unshift(res);
        $scope.$digest();
      });
  }

  //$(".table-discount").dataTable({
  //  "autoWidth": true,
  //  "ordering": false,
  //  "oLanguage": {
  //    "sSearch": "",
  //  },
  //  language: {
  //    searchPlaceholder: "Enter information to search..."
  //  },
  //  "dom": '<"top" flp>rt<"bottom"flp>'
  //});
  //$('.dataTables_filter input').attr("placeholder", "Enter information to search...");
  //jQuery('.page-preloader-wrap-dashboard').hide();
  function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

  //$("#number").val(makeid());
  //jQuery.validator.methods["date"] = function (value, element) { return true; }
  console.log("discountscontroller");
    $('.dataTables_length select').select2();
    if ($(window).width() <= '768') {
        $('.admin #table_wrapper .top').prependTo('.table-mobile-wrap');
    }
   jQuery('.page-preloader-wrap-dashboard').hide();
}
