﻿sandboxcontroller.$inject = ["$scope", "$location", "$interval", "api", "$animate"];
function sandboxcontroller($scope, $location, $interval, api, $animate) {
   
    //cards steps task
    var quastNumber = 1;
    $('.card-quast .next-quast.active , .card-quast form .btn.active').click(function () {    
        $('.card-quast .next-quast.active , .card-quast form .btn.active').removeClass('active');
        quastNumber++;
        $('.cards-steps .middle-panel ul li.current').addClass('prew').removeClass('current').next().addClass('current').removeClass('next'); 
        $('.cards-steps .middle-panel .step-count span').text(quastNumber);
        $('.card-quast .quast-type-wrap:not(.hidden)').addClass('hidden').next().removeClass('hidden');
        return quastNumber;
    });
    $('.answ-item input , .field-answer input').click(function () {
        $(this).closest('.quast-type-wrap').children('.btn').addClass('active');
        $('.card-quast .next-quast').addClass('active')
    });
    $('.cards-group select').select2();


  var recognizer;
  $scope.data = {answer:""}
  $scope.countRight = 0;
  $scope.countWrong = 0;
  $scope.reply = function () {
    console.log($scope.data.answer);
    if (!$scope.data.answer) {
      alert("You did not onput answer");
      return;
    }
    switch ($scope.card.Type) {//
    case "1":
    case "3":
      {
          var crd = $scope.card.Answers.find(function (x) { return x.Id === $scope.data.answer });
        if (crd.IsRight)
          $scope.countRight++;
        else
          $scope.countWrong++;
      }
      break;
    case "0":
    case "2":
    case "4":
      {
          if ($scope.data.answer.trim() === $scope.card.Answers[0].Variant.trim())
            $scope.countRight++;
          else
            $scope.countWrong++;
      }
      break;
    }
    api.ajax('/api/Sandbox/GetRandomCard', 'get', null, function (res) {
      res.Type = res.Type.toString();
      $scope.card = res;
      checkType();
      $scope.data.answer = null;
      $scope.$digest();
    });
  }

  function checkType() {
    if ($scope.card.Type === '0') {
      var lng;
      switch ($scope.card.Language) {
      case 'us':
        lng = "en-US";
        break;
      case 'ae':
        lng = "ar-EG";
        break;
      case 'cn':
        lng = "zh-CN";
        break;
      case 'fr':
        lng = "fr-FR";
        break;
      case 'de':
        lng = "de-DE";
        break;
      case 'it':
        lng = "it-IT";
        break;
      case 'br':
        lng = "pt-BR";
        break;
      case 'ru':
        lng = "ru-RU";
        break;
      case 'es':
        lng = "es-ES";
        break;
      default:
        lng = "en-US";
      }
      recognizer = recognizerSetup(window.SDK, "Interactive", lng, window.SDK.SpeechResultFormat["Simple"], "7d42ba63a3614726aae549a4f5b1232a");
    }
  }

  $scope.changes = function (value) {
    console.log(value);
    $scope.answer = value;
  }
  $scope.RecognizerStart =  function() {
    recognizer.Recognize((event) => {
      switch (event.Name) {
        case "SpeechSimplePhraseEvent":
          updateRecognizedPhrase(event.Result);
          break;
        case "SpeechDetailedPhraseEvent":
          updateRecognizedPhrase(JSON.stringify(event.Result, null, 3));
          break;
        default:
          console.log(JSON.stringify(event)); // Debug information
      }
    })
      .On(() => {
        // The request succeeded. Nothing to do here.
      },
      (error) => {
        console.error(error);
      });
  }
  function updateRecognizedPhrase(json) {
    console.log(json);
    $scope.data.answer = json.DisplayText;
    $scope.$digest();
  }
  jQuery('.page-preloader-wrap-dashboard').hide();
  function Initialize(onComplete) {
    if (!!window.SDK) {
      onComplete(window.SDK);
    } else
      alert("Speach SDK is not completed");
  }

  Initialize(function () {
    recognizer = recognizerSetup(window.SDK, "Interactive", "en-US", window.SDK.SpeechResultFormat["Simple"], "7d42ba63a3614726aae549a4f5b1232a");
  });
  function recognizerSetup(SDK, recognitionMode, language, format, subscriptionKey) {

    switch (recognitionMode) {
      case "Interactive":
        recognitionMode = SDK.RecognitionMode.Interactive;
        break;
      case "Conversation":
        recognitionMode = SDK.RecognitionMode.Conversation;
        break;
      case "Dictation":
        recognitionMode = SDK.RecognitionMode.Dictation;
        break;
      default:
        recognitionMode = SDK.RecognitionMode.Interactive;
    }

    var recognizerConfig = new SDK.RecognizerConfig(
      new SDK.SpeechConfig(
        new SDK.Context(
          new SDK.OS(navigator.userAgent, "Browser", null),
          new SDK.Device("SpeechSample", "SpeechSample", "1.0.00000"))),
      recognitionMode,
      language, // Supported languages are specific to each recognition mode. Refer to docs.
      format); // SDK.SpeechResultFormat.Simple (Options - Simple/Detailed)


    var useTokenAuth = false;

    var authentication = function () {
      if (!useTokenAuth)
        return new SDK.CognitiveSubscriptionKeyAuthentication(subscriptionKey);

      var callback = function () {
        var tokenDeferral = new SDK.Deferred();
        try {
          var xhr = new (XMLHttpRequest || ActiveXObject)('MSXML2.XMLHTTP.3.0');
          xhr.open('GET', '/token', 1);
          xhr.onload = function () {
            if (xhr.status === 200) {
              tokenDeferral.Resolve(xhr.responseText);
            } else {
              tokenDeferral.Reject('Issue token request failed.');
            }
          };
          xhr.send();
        } catch (e) {
          window.console && console.log(e);
          tokenDeferral.Reject(e.message);
        }
        return tokenDeferral.Promise();
      }

      return new SDK.CognitiveTokenAuthentication(callback, callback);
    }();
    return SDK.CreateRecognizer(recognizerConfig, authentication);
  }
    
  api.ajax('/api/Sandbox/GetRandomCard', 'get', null, function (res) {
    res.Type = res.Type.toString();
    console.log(res);
    $scope.card = res;
    checkType();
    $scope.$digest();
  });
}
