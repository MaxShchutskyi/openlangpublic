﻿notificationscontroller.$inject = ["$scope", "$timeout", "$location", "$interval", "api", "$animate", '$mdEditDialog', '$q'];
function notificationscontroller($scope, $timeout, $location, $interval, api, $animate, $mdEditDialog, $q) {
    $scope.model = {};
    api.ajax("/api/ApiAdminNotifications/GetAllNotes", 'get', null, function (res) {
        //console.log();
        $scope.notifications = res;
        $scope.$digest();
    })
    $scope.getNormalDate = function (date) {
        if (!date) return "";
        return moment(date).format("DD.MM.YYYY HH:mm");
    }
    'use strict';
    $scope.onPaginate = function () {
        $scope.selected = [];
    }
    $scope.limitOptions = [5, 10, 15];
    $scope.query = {
        order: 'name',
        limit: 5,
        page: 1
    };

    $scope.options = {
        
        limitSelect: true,
        pageSelect: true
    };
    $scope.logPagination = function (page, limit) {
        console.log('page: ', page);
        console.log('limit: ', limit);
    }
    

    $scope.send = function () {
        if (!$scope.model.title || !$scope.model.message) {
            alert("You should fill all fields");
            return;
        }
        api.ajax("/api/ApiAdminNotifications/CreateNotification", 'post', JSON.stringify($scope.model), function (res) {
            $scope.notifications.push(res);
            $scope.model = {};
            $scope.$digest();
        });
    }
    console.log("notificationscontroller");
    jQuery('.page-preloader-wrap-dashboard').hide();
}
