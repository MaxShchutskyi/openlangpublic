﻿ratescontroller.$inject = ["$scope", "$location", "$interval", "api", "$animate"];
function ratescontroller($scope, $location, $interval, api, $animate) {
   
    console.log("ratescontroller");
    api.ajax('/api/AdminTeacherRates/GetTeachersRates', 'get', null, function (res) {
        console.log(res);
        $scope.rates = res;
        $scope.$digest();
    });
    $scope.save = function (rate) {
        api.ajax('/api/AdminTeacherRates/UpdateDay', 'post', JSON.stringify(rate), function (res) {
            alert('Updated')
            //console.log(res);
            //$scope.rates = res;
            //$scope.$digest();
        });
    }
    api.ajax('/api/AdminTeacherRates/GetCommonRate', 'get',null, function (res) {
        $scope.mainRate = res;
    });
    $scope.saveCommon = function () {
        api.ajax('/api/AdminTeacherRates/UpdateCommonRate', 'post', JSON.stringify($scope.mainRate), function (res) {
            alert('Updated')
        });
    }
}
