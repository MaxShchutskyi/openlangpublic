﻿monthlyearningscontroller.$inject = ["$scope", "$location", "$interval", "api", "$animate"];
function monthlyearningscontroller($scope, $location, $interval, api, $animate) {
  console.log("monthlyearningscontroller");
  $scope.items = [];
  $scope.model = {};
  $scope.dtOptions = {
    "autoWidth": true,
    "ordering": false,
    "oLanguage": {
      "sSearch": "",
    },
    language: {
      searchPlaceholder: "Enter information to search..."
    },
    "dom": '<"top" flp>rt<"bottom"flp>'
  }
  jQuery('.page-preloader-wrap-dashboard').hide();
  setCurrentMonth();
  $scope.$watch("model.month",
    function(newVal, oldVal) {
      if (!newVal || newVal === oldVal) return;
      getNewData();
    });
  $scope.$watch("model.year",
    function (newVal, oldVal) {
      if (!newVal || newVal === oldVal) return;
      getNewData();
    });
  getNewData();
  function setCurrentMonth() {
    var date = moment();
    $scope.model.month = (date.month() + 1).toString();
    $scope.model.year = date.year().toString();
    //console.log($scope.model.month);
  }
  function getNewData() {
    //var date = moment();
    api.ajax(
      '/api/MonthlyEarningsApi/GetMonthlyEarnings?date=' + $scope.model.year + "-" + $scope.model.month + "-25T00:00:00", 'get', null,
      function(res) {
        $scope.items = res;
        console.log("Count of results = " + res.length);
        console.log($scope.items);
        $scope.$digest();
      });
    //console.log($scope.model.month);
  }
}