﻿lessondetailscontroller.$inject = ["$scope", "$location", "$interval", "api", "$animate", "$routeParams", "$http"];
function lessondetailscontroller($scope, $location, $interval, api, $animate, $routeParams, $http) {
    $scope.model = { Id: 0 };
    $scope.chapter = {};
    $scope.curChap = 0;
    $scope.slides = [];
    $scope.sortableOptions = {
        stop: function (e, ui) {
            for (let i = 0; i < $scope.slides.length; i++) {
                $scope.slides[i].OrderId = i + 1;
            }
            var md = $scope.slides.map(function (k) {
                return { Id: k.Id, orderId: k.OrderId };
            });
            api.ajax("/api/braincard/UpdateOrder", "post", JSON.stringify(md), function () {
                console.log("updateorder");
                $scope.$digest();
            });
        }
    };
    if ($routeParams.lessid) {
        api.ajax("/api/lessons/GetLessonsById?id=" + $routeParams.lessid,
            'get',
            null,
            function (res) {
                $scope.model = res;
                $scope.curChap = res.ChapterId.toString();
                console.log($scope.model);
                updateSlides();
            });
        //api.ajax("/api/lessons/GetSlidesFromImages?lessonId=" + $routeParams.lessid,
        //  'get',
        //  null,
        //    function (res) {
        //        console.log(res);
        //    $scope.imageSlides = res;
        //    $scope.$digest();
        //  });
    }
    if ($routeParams.chapterId) {
        api.ajax("/api/Chapter/GetChapterById?chapter=" + $routeParams.chapterId,
            'get',
            null,
            function (res) {
                $scope.chapter = res;
                $scope.$digest();
            });

    }
    console.log("lessondetailscontroller");
    $scope.getNameOfSlide = function (tp) {
        switch (tp.toString()) {
            case '0':
                return "Record Voice";
            case '1':
                return "Image";
            case '7':
                return "Video";
            case '3':
                return "Quiz";
            case '5':
                return "Words";
            case '6':
                return "Sentense";
            case '8':
                return "Memory Games";
            case '12':
                return "Translate";
            case '13':
                return "Listen";
        }
    }
    $scope.getClass = function (tp) {
        switch (tp.toString()) {
            case '0':
                return "fa fa-microphone";
            case '1':
                return "fa fa-image";
            case '7':
                return "fa fa-play-circle";
            case '3':
                return "fa fa-check-square-o";
            case '5':
                return "fa fa-check-square-o";
            case '6':
                return "fa fa-edit";
            case '8':
                return "fa fa-th-large";
            case '12':
                return "fa fa-language";
            case '13':
                return "fa fa-volume-up";
        }
    }
    $scope.initLang = function () {
        $scope.model.Language = $("#lng option").first().val();
    }
    $scope.initLevel = function () {
        $scope.model.Level = $("#lvl option").first().val();
    }
    function updateSlides(withReload) {
        if (!$scope.model.Id) return;
        api.ajax("/api/braincard/GetCardsByLessonId?id=" + $scope.model.Id, 'get', null, function (res) {
            $scope.slides = res;
            console.log($scope.slides);
            $scope.$digest();
            if (withReload)
                $location.path(getPath());
        });
    }
    $scope.deleteSlide = function (id) {
        api.ajax("/api/braincard/DeleteCard?id=" + id, 'delete', null, function (res) {
            if (res)
                updateSlides();
        });
    }
    function getPath() {
        return '/lessons/' + $scope.chapter.Id + "/lessondetails/" + $scope.model.Id;
    }
    $scope.addNewCard = function () {
        $scope.closePopup();
        if (!$scope.model.Id) {
            $scope.model.Language = $scope.chapter.Language;
            $scope.model.Level = $scope.chapter.Level;
            $scope.model.chapterId = $scope.chapter.Id;
            api.ajax('/api/lessons/AddOrUpdateLesson',
                'post',
                JSON.stringify($scope.model),
                function (res) {
                    $scope.model.Id = res.Id;
                    $location.path(getPath() + "/0");

                    $scope.$apply();
                });
        } else {
            $location.path(getPath() + "/0");
        }
    }
    $scope.pathToCard = function (id) {
        $location.path(getPath() + "/" + id);
    }
    $scope.saveChanges = function () {
        if (!$scope.model.Name) {
            alert("You did not enter name of Lesson name");
            return;
        }
        api.ajax('/api/lessons/AddOrUpdateLesson',
            'post',
            JSON.stringify($scope.model),
            function (res) {
                $scope.model.Id = res.Id;
                $scope.$digest();
                alert("Saved");
            });
    }

    $scope.changePDF = function (files) {
        //console.log(files);
        $scope.closePopup();
        if (!$scope.model.Id) {
            $scope.model.Language = $scope.chapter.Language;
            $scope.model.Level = $scope.chapter.Level;
            $scope.model.chapterId = $scope.chapter.Id;
            api.ajax('/api/lessons/AddOrUpdateLesson',
                'post',
                JSON.stringify($scope.model),
                function (res) {
                    $scope.model.Id = res.Id;
                    $scope.$apply();
                    upd(files);
                });
        }
        else
            upd(files);

    }
    function upd(files){
        var data = new FormData();
        data.append(files[0].name, files[0]);
        data.append('lessonId', $scope.model.Id);
        $http.post('/Admin/UploadPDF/UploadPDFLesson', data, {
            withCredentials: true,
            headers: { 'Content-Type': undefined },
            transformRequest: angular.identity
        }).success(function (res) {
            updateSlides(true);
        });
    }
    $scope.chooseTypeOfCard = function () {
        if (!$scope.model.Name) {
            alert("You did not enter name of Lesson name");
            return;
        }
        $.magnificPopup.open({
            items: {
                src: '.chooseTypeCard-popup'
            },
            mainClass: 'mfp-zoom-in',
            enableEscapeKey: false,
            closeOnBgClick: false,
            callbacks: {
                beforeOpen: function () { }
            }
        });


    }
    $scope.closePopup = function () {
        $.magnificPopup.close();
    };
    $scope.uploadFileTrigger = function () {
        if (!$scope.model.Name) {
            alert("You did not enter name of Lesson name");
            return;
        }
        $('.upload-file-button-pdf').trigger("click");
    };
    $scope.uploadFileLogoTrigger = function () {
        if (!$scope.model.Name) {
            alert("You did not enter name of Lesson name");
            return;
        }
        $('.upload-logo-chapter-button').trigger("click");
    };
    $scope.myFileLogo = function (input) {
        if (input.files && input.files[0]) {
            if (!$scope.model.Id) {
                $scope.model.Language = $scope.chapter.Language;
                $scope.model.Level = $scope.chapter.Level;
                $scope.model.chapterId = $scope.chapter.Id;
                api.ajax('/api/lessons/AddOrUpdateLesson',
                    'post',
                    JSON.stringify($scope.model),
                    function (res) {
                        $scope.model.Id = res.Id;
                        $scope.$apply();
                        var data = new FormData();
                        data.append(input.files[0].name, input.files[0]);
                        data.append('lessonId', $scope.model.Id);
                        $http.post('/Admin/UploadPDF/UploadLessonIcon', data, {
                            withCredentials: true,
                            headers: { 'Content-Type': undefined },
                            transformRequest: angular.identity
                        }).success(function (res) {
                            $scope.model.IconSrc = res;
                        });
                    });
            }
            else {
                var data = new FormData();
                data.append(input.files[0].name, input.files[0]);
                data.append('lessonId', $scope.model.Id);
                $http.post('/Admin/UploadPDF/UploadLessonIcon', data, {
                    withCredentials: true,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                }).success(function (res) {
                    $scope.model.IconSrc = res;
                });
            }
        }
      

    };
    //jQuery('.page-preloader-wrap-dashboard').hide();
    //$("#sortable").sortable({
    //    items: ".ui-state-default"б
    //stop:
    //});
    //$("#sortable").disableSelection();
}