﻿function upcommingClasses() {
    return {
        restrict: 'A',
        templateUrl: function(elem, attrs){
            return "/Dashboard/UpcommingClasses/" + attrs.method + "?take=" + attrs.top + "&template=" + attrs.template;
        },
        require: ['?top', '?template','?method'],
        scope: { top: '=top', template: '=template' },
        link: function (scope, elem, attr, $location) {
            $.fn.dataTable.ext.errMode = 'none';
            var item = $("#myfullupcomingclasses");
            if (item) {
                item.DataTable(shared.getTableProperties(item));
            }
            scope.changeClassPath = scope.$parent.changeClassPath;
        }
    }
}