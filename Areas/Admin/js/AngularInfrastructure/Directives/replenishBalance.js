﻿replenishBalance.$inject = ["$http", "$timeout", "api", "notifications"];
function replenishBalance($http, $timeout, api, notifications) {
    return {
        restrict: 'A',
        scope: false,
        templateUrl: "/Dashboard/NewMyAccount/ReplenishBalance",
        link: function (scope, elem, attr) {
            scope.succeeded = true;
            $('.info-tooltip').tooltip();
            scope.replenish = { eurbalance: 0.00, bonus: 0.00 };
            api.ajax("/api/Account/GetBalance", 'get', null, function (res) {
                scope.shared.globalDelta = res.delta;
                scope.shared.globalCurrency = res.Currency;
                scope.balance = res;
                scope.shared.NativeMoney = res.NativeMoney;
                scope.shared.money = res.Money;
                scope.$digest();
                //scope.primaryPrice = (scope.shared.currentPrices.Premium * res.delta).toFixed(2) + " " + res.Currency;
                //scope.basicPrice = (scope.shared.currentPrices.Basic * res.delta).toFixed(2) + " " + res.Currency;
                //scope.groupPrice = (scope.shared.currentPrices.Group * res.delta).toFixed(2) + " " + res.Currency;
            });
            scope.$watch('shared.money', function (newVal, oldVal) {
                console.log(newVal);
                if (newVal == oldVal || !newVal) return;
                scope.balance.Money = newVal;
                scope.shared.NativeMoney = (newVal * scope.shared.globalDelta).toFixed(2) + " " + scope.shared.globalCurrency;
            });
            jQuery("#creditform").validate({
                errorPlacement: function () { },
                highlight: function (element) {
                    $(element).addClass("error").next().find(".select2-selection").addClass("error");
                },
                unhighlight: function (element) {
                    $(element).removeClass("error").next().find(".select2-selection").removeClass("error");
                },
            });
            scope.$watch('replenish.additionalBonus', function (newVal, oldVal) {
                if (newVal == oldVal) return;
                api.ajax("/api/Discounts/CheckExistsDiscount", "get", { discount: newVal }, function (res) {
                    scope.discount = res;
                    scope.replenish.discount = scope.discount ? scope.discount.Id : null;
                    scope.getDiscountIfExist(parseFloat(scope.replenish.replenishacount));
                    scope.$digest();
                });
                //scope.replenish.eurbalance = (scope.replenish.replenishacount / scope.shared.globalDelta).toFixed(2);
            });
            scope.replenish.setectedType = 'credit'
            scope.$watch('replenish.replenishacount', function (newVal, oldVal) {
                if (newVal == oldVal) return;
                scope.replenish.eurbalance = (scope.replenish.replenishacount / scope.shared.globalDelta).toFixed(2);
                scope.getDiscountIfExist(scope.replenish.replenishacount);
            });
            scope.getDiscountIfExist = function (floatVal) {
                var bonus = 0;
                if (floatVal < 50) bonus = 0;
                else if (floatVal >= 50 && floatVal <= 199) bonus = 5;
                else if (floatVal >= 200 && floatVal <= 499) bonus = 7.5;
                else if (floatVal >= 500 && floatVal <= 999) bonus = 10;
                else if (floatVal >= 1000) bonus = 12.5;
                scope.replenish.bonus = (scope.replenish.replenishacount / 100 * bonus).toFixed(2);
                scope.replenish.eurBonus = (scope.replenish.eurbalance / 100 * bonus).toFixed(2);
                if (!scope.discount)
                    return;
                if (scope.discount.IsPercent) {
                    scope.replenish.eurBonus = (parseFloat(scope.replenish.eurBonus) + parseFloat(((scope.replenish.eurbalance / 100) * parseFloat(scope.discount.Value)).toFixed(2))).toFixed(2);
                    scope.replenish.bonus = (parseFloat(scope.replenish.bonus) + parseFloat((scope.replenish.replenishacount / 100 * parseFloat(scope.discount.Value)).toFixed(2))).toFixed(2);
                }
                else {
                    scope.replenish.eurBonus = (parseFloat(scope.replenish.eurBonus) + parseFloat(scope.discount.Value)).toFixed(2);
                    scope.replenish.bonus = (parseFloat(scope.replenish.bonus) + parseFloat(((parseFloat(scope.discount.Value)) * scope.shared.globalDelta).toFixed(2))).toFixed(2);
                }
            }
            scope.nextStep = function (prev, next) {
                if (scope.checkIsNone(scope.replenish.replenishacount))
                    return;
                $(prev).modal('hide');
                $(next).modal('show');
            }
            scope.createReplenish = function () {
                if (scope.replenish.setectedType == 'credit' && !$("#creditform").valid())
                    return;
                if (scope.shared.isButtonLoading)
                    return;
                //$("#replenish-balance-step-2").modal('hide');
                scope.shared.isButtonLoading = true;
                scope.replenish.currency = scope.shared.globalCurrency;
                if (scope.replenish.setectedType == 'credit') {
                    api.ajax('/api/Payment/CreditCardPayment', 'post', JSON.stringify(scope.replenish), function (res) {
                        scope.shared.isButtonLoading = false;
                        if (res.hasOwnProperty('error')) {
                            scope.succeeded = false;
                            scope.$digest();
                            $("#replenish-balance-step-2").modal('hide');
                            $("#replenish-balance-step-3").modal('show');
                            return;
                        }
                        scope.balance = res;
                        scope.shared.NativeMoney = res.NativeMoney;
                        scope.confirmedAmount = scope.replenish.replenishacount;
                        scope.confirmedBonus = scope.replenish.bonus;
                        scope.succeeded = true;
                        scope.replenish = {};
                        scope.$digest();
                        $("#replenish-balance-step-2").modal('hide');
                        $("#replenish-balance-step-3").modal('show');
                    })
                }
                if (scope.replenish.setectedType == 'paypal') {
                    api.ajax('/api/Payment/PayPalPayment', 'post', JSON.stringify(scope.replenish), function (res) {
                        location.href = res.path;
                    })
                }
            }
            if ($.cookie("showstep3")) {
                var bal = JSON.parse($.cookie("showstep3"));
                scope.confirmedAmount = bal.amount;
                scope.confirmedBonus = bal.bonus;
                $("#replenish-balance-step-3").modal('show');
                $.cookie("showstep3", null, { path: '/' });
                $.removeCookie('showstep3', { path: '/' });
                document.cookie = "showstep3" + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                scope.$digest();
            }
            scope.checkIsNone = function (elem) {
                if (!elem) return true;
                if (isNaN(parseFloat(elem))) return true;
                if (parseFloat(elem) === 0) return true;
                return !elem.toString().match(/^\d+$/) && !elem.toString().match(/^\d+\.\d+$/);
            }
        }

    }
}



