﻿monthlyEarnings.$inject = ["$http", "$timeout"];
function monthlyEarnings($http, $timeout) {
    return {
        restrict: "A",
        templateUrl: "/Dashboard/MonthlyEarnings/GetMonthlyEarnings",
        scope:false,
        link: function (scope, elem, attr) {
            $http({ url: '/Dashboard/MonthlyEarnings/GetMyMonthlyEarnings', method: 'post' }).success(function (rest) {
                scope.elem = rest;
                scope.shared.NativeMoney = rest.NativeMoney;
                scope.maxvalue = 50;
                if (scope.elem.CountLessons > 50 && scope.elem.CountLessons <= 100)
                    scope.maxvalue = 100;
                if (scope.elem.CountLessons > 100 && scope.elem.CountLessons <= 500)
                    scope.maxvalue = 500;
                if (scope.elem.CountLessons > 500 && scope.elem.CountLessons <= 1000)
                    scope.maxvalue = 1000;
                if (scope.elem.CountLessons > 1000 && scope.elem.CountLessons <= 2000)
                    scope.maxvalue = 2000;
                var perc = 100 * scope.elem.CountLessons/ scope.maxvalue ;
                console.log(perc);
                $timeout(function () {
                    $('.progress-earnings').progressbar({
                        display_text: 'center',
                        use_percentage: false,
                    });
                    $("#progress").asPieProgress({ 'namespace': 'pie_progress', size: 75, barcolor: '#88cc49', speed: 25, trackcolor: '#4b5363' }).asPieProgress('go', perc + '%');
                });
            });
        }
    }
}