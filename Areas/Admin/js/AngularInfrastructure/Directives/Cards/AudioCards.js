﻿audioCards.$inject = ["api", 'recorderService','$interval'];
function audioCards(api, recorderService, $interval) {
  return {
    restrict: 'E',
    scope: {
      card: '=card'
    },
    templateUrl: '/Areas/Admin/js/AngularInfrastructure/Directives/Cards/Templates/AudioTemplate.html',
    link: function ($scope, elem, attrs) {
      $scope.currentTime = "00:00:00";
      $scope.startingTime = "00:00:00";
      $scope.stateOfPlayer = 0;
      $scope.tracker = 0;
      $scope.card.checkData = function () {
        if ($scope.card.Id && $scope.card.currentType === '0') {
          $scope.Answer = $scope.card.Answers[0].Variant;
          $scope.Question = $scope.card.Question;
          $scope.Source = $scope.card.VideoSrc;
          $("#aud source").attr('src', $scope.Source);
          var dt = document.getElementById("aud");
          dt.load();
          dt.onloadedmetadata = function () {
            $scope.currentTime = moment(toDateTime(dt.duration)).format("HH:mm:ss");
            $scope.stateOfPlayer = 2;
            $scope.$digest();
            //alert(dt.duration);
          };
        }
      }
      $scope.getStateOfPlayer = function() {
        if ($scope.stateOfPlayer === 0)
          return 'unrecord';
        else if ($scope.stateOfPlayer === 1)
          return 'recordering';
        else if ($scope.stateOfPlayer === 2)
          return 'recordered';
        else if ($scope.stateOfPlayer === 3)
          return 'repeatAudio';
      }
      function toDateTime(secs) {
        var t = new Date(1970, 0, 1); // Epoch
        t.setSeconds(secs);
        return t;
      }
      function callbackCinvertedMp3(res) {
        $scope.Source = "data:audio/mpeg;base64," + res;
        $("#aud source").attr('src', "data:audio/mpeg;base64," + res);
        var dt = document.getElementById("aud");
        dt.load();
        dt.onloadedmetadata = function () {
          //alert(dt.duration);
        };
      }

      var promise;
      var percentIncrementation = 0;
      $scope.startAudio = function () {
        if ($scope.stateOfPlayer === 2)
          return;
        if ($scope.stateOfPlayer === 1) {
          recorderService.stop();
          $interval.cancel(promise);
          $scope.stateOfPlayer = 2;
          return;
        }
        $scope.currentTime = "00:00:00";
        $scope.stateOfPlayer = 1;
        recorderService.start("/Admin/Cards/ConvertToMp3", callbackCinvertedMp3);
        promise = $interval(function() {
          var mnt = moment($scope.currentTime, "HH:mm:ss");
          var nv = mnt.add(1, 'second');
          $scope.currentTime = nv.format("HH:mm:ss");
          console.log($scope.currentTime);
        }, 1000);
      }
      $scope.stopAudio = function () {
        $scope.Source = null;
        $("#aud source").attr('src', '');
        $scope.currentTime = "00:00:00";
        $scope.stateOfPlayer = 0;
        $scope.tracker = 0;
      }
      $scope.playAudio = function () {
        if ($scope.stateOfPlayer === 3) {
          $interval.cancel(promise);
          $scope.stateOfPlayer = 2;
          return;
        }
        if ($scope.startingTime !== $scope.currentTime && $scope.startingTime === "00:00:00") {
          
          var allSeconrs = moment($scope.currentTime, "HH:mm:ss").seconds();
          percentIncrementation = 100 / allSeconrs;
          $scope.startingTime = $scope.currentTime;
          $scope.currentTime = "00:00:00";
          $scope.tracker = 0;
        }
        $scope.stateOfPlayer = 3;
        var audio = document.getElementById("aud");
        promise = $interval(function() {
          var mnt = moment($scope.currentTime, "HH:mm:ss");
          var nv = mnt.add(1, 'second');
          $scope.currentTime = nv.format("HH:mm:ss");
          $scope.tracker += percentIncrementation;
          if ($scope.currentTime === $scope.startingTime) {
            $interval.cancel(promise);
            $scope.stateOfPlayer = 2;
            $scope.startingTime = "00:00:00";
          }
        }, 1000);
        audio.play();
      }
        $scope.card.checkData();
        $scope.card.addCard = function () {
          //|| !$scope.Source
        if (!$scope.Answer || !$scope.Question ) {
          alert("You did not enter Answer or Question or Not record audio");
          return null;
        }
        $scope.card.Answers = [{ Variant: $scope.Answer, IsRight: true }];
        $scope.card.Question = $scope.Question;
        $scope.card.VideoSrc = $scope.Source;
        return $scope.card;
      }
    }
  }
}
$(window).load(function () {
    //$('.audio-wrap.unrecord .pauseAudio').click(function () {
    //    $('.audio-wrap').removeClass('unrecord').addClass('recordering');
    //    var maxWidth = $('.track-counter').width()
    //    for (activeWidth = 0; activeWidth < maxWidth; activeWidth++) {
    //        $('.track-countering').width(activeWidth)
    //    }
    //    //timer
    //    var base = 60;
    //    var clocktimer, dateObj, dh, dm, ds, ms;
    //    var readout = '';
    //    var h = 1, m = 1, tm = 1, s = 0, ts = 0, ms = 0, init = 0;
    //    //функция для старта секундомера
    //    function StartTIME() {
    //        var cdateObj = new Date();
    //        var t = (cdateObj.getTime() - dateObj.getTime()) - (s * 1000);
    //        if (t > 999) { s++; }
    //        if (s >= (m * base)) {
    //            ts = 0;
    //            m++;
    //        } else {
    //            ts = parseInt((ms / 100) + s);
    //            if (ts >= base) { ts = ts - ((m - 1) * base); }
    //        }
    //        if (m > (h * base)) {
    //            tm = 1;
    //            h++;
    //        } else {
    //            tm = parseInt((ms / 100) + m);
    //            if (tm >= base) { tm = tm - ((h - 1) * base); }
    //        }
    //        ms = Math.round(t / 10);
    //        if (ms > 99) { ms = 0; }
    //        if (ms == 0) { ms = '00'; }
    //        if (ms > 0 && ms <= 9) { ms = '0' + ms; }
    //        if (ts > 0) { ds = ts; if (ts < 10) { ds = '0' + ts; } } else { ds = '00'; }
    //        dm = tm - 1;
    //        if (dm > 0) { if (dm < 10) { dm = '0' + dm; } } else { dm = '00'; }
    //        dh = h - 1;
    //        if (dh > 0) { if (dh < 10) { dh = '0' + dh; } } else { dh = '00'; }
    //        readout = dh + ':' + dm + ':' + ds + '.' + ms;
    //        document.MyForm.stopwatch.value = readout;
    //        clocktimer = setTimeout("StartTIME()", 1);
    //    }
    //    //Функция запуска и остановки
    //    function StartStop() {
    //        if (init == 0) {
    //            ClearСlock();
    //            dateObj = new Date();
    //            StartTIME();
    //            init = 1;
    //        } else {
    //            clearTimeout(clocktimer);
    //            init = 0;
    //        }
    //    } 

    //    return false;
    //})
    //$('.audio-wrap.recordering .pauseAudio').click(function () {
    //    $('.audio-wrap').removeClass('recordering').addClass('recordered');
    //    $('.track-countering').width(0)
    //    return false;
    //})
    //$('.audio-wrap.recordered .playAudio').click(function () {
    //    $('.audio-wrap').removeClass('recordered').addClass('repeatAudio');
    //    var maxWidth = $('.track-counter').width()
    //    for (activeWidth = 0; activeWidth < maxWidth; activeWidth++) {
    //        $('.track-countering').width(activeWidth)
    //    }
    //    return false;
    //})
    //$('.audio-wrap.repeatAudio .playAudio').click(function () {
    //    $('.audio-wrap').removeClass('repeatAudio').addClass('recordered');
    //    $('.track-countering').width(0)
    //    return false;
    //})
    //$('.audio-wrap.recordered .deleteAudio, .audio-wrap.repeatAudio .deleteAudio').click(function () {
    //    $('.audio-wrap').removeClass('recordered').addClass('unrecord');
    //    $('.track-countering').width(0)
    //    return false;
    //})
});