﻿wordsCards.$inject = ["api"];
function wordsCards(api) {
    return {
        restrict: 'E',
        scope: {
            card: '=card'
        },
        templateUrl: function () {
            return '/Areas/Admin/js/AngularInfrastructure/Directives/Cards/Templates/WordsTemplate.html?date=' +
                new Date();
        },
        link: function ($scope, elem, attrs) {
            $scope.words = [];
            $scope.model = {};
            var countOfWords = 0;
            $scope.card.checkData = function () {
                if ($scope.card.Id && $scope.card.Type === '5') {
                    $scope.Question = $scope.card.Question;
                    $scope.words = $scope.card.Answers;
                    $scope.sentence = JSON.parse($scope.card.AdditionalJSON).text;
                    countOfWords = $scope.card.Answers.length;
                }
            }
            $scope.addNewWord = function () {
                var res = $scope.model.typedWord;
                $scope.words.push({ Variant: res });
                console.log($scope.words);
                $scope.model.typedWord = "";
            }
            $scope.deleteWord = function (word) {
                var index = $scope.words.indexOf(word);
                $scope.words.splice(index, 1);
            }
            $scope.selectRightAnswer = function (word) {
                word.IsRight = true;
                var index = $scope.words.indexOf(word);
                $scope.words.filter(function (x, i) { return i != index }).forEach(function (x) { x.IsRight = false });
                console.log($scope.words);
            }
            $scope.getStyle = function (word) {
                if (word.IsRight)
                    return { backgroundColor: 'yellow', color: 'red' };
                return { backgroundColor: '#ccc', color: 'red' };
            }
            //$scope.$watch('sentence',
            //  function(y, w) {
            //    if (y && y !== w) {
            //      var st = (y.match(/__/g) || []).length;
            //      if (countOfWords === st) return;
            //      countOfWords = st;
            //      $scope.words = [];
            //      for (var i = 0; i < st; i++) {
            //        $scope.words.push({ Index: i, Variant: "" });
            //      }
            //    }
            //  });
            $scope.card.checkData();
            $scope.card.addCard = function () {
                if (!$scope.words.find(function (x) { return x.IsRight })) {
                    alert("You did not select right answer");
                    return null;
                }
                if (!$scope.words.length || $scope.words.find(function (x) { return !x.Variant })) {
                    alert("You have no words or one of the wor is empty!");
                    return null;
                }
                if (!$scope.Question) {
                    alert("You have no description");
                    return null;
                }
                $scope.card.Question = $scope.Question;
                $scope.card.Answers = $scope.words;
                $scope.card.AdditionalJSON = JSON.stringify({ text: $scope.sentence });
                debugger;
                return $scope.card;
            }
        }
    }
}