﻿memoryCard.$inject = ["api"];
function memoryCard(api) {
  return {
    restrict: 'E',
    scope: {
      card: '=card'
    },
    templateUrl: function () {
      return '/Areas/Admin/js/AngularInfrastructure/Directives/Cards/Templates/MemoryGameTemplate.html?date=' + new Date();
    },
    link: function ($scope, elem, attrs) {
      $scope.images = [
        { ImageSrc: null, Index: 0, Variant: null, IsRight: null },
        { ImageSrc: null, Index: 1, Variant: null, IsRight: null },
        { ImageSrc: null, Index: 2, Variant: null, IsRight: null },
        { ImageSrc: null, Index: 3, Variant: null, IsRight: null },
        ];
      $scope.selectedImageLayout = '2';
      $scope.card.addCard = function () {
        var selImgs = $scope.images.slice(0, $scope.selectedImageLayout);
        var nl = selImgs.find(function (x) { return !x.ImageSrc });
        if (nl)
          return { Error: "You did not upload images for each block" };
        //if (selImgs.length > 1 && !selImgs.find(function (x) { return x.IsRight }))
        //  return { Error: "You should check one Right answer" };
        $scope.card.Answers = selImgs;
        return $scope.card;
      }
      $scope.card.checkData = function () {
        if ($scope.card.Id && $scope.card.currentType === '8') {
          $scope.card.Answers.forEach(function (x) {
            if (x.Index === null) return;
            $scope.images[x.Index] = x;
          });
          $scope.selectedImageLayout = $scope.card.Answers.length.toString();
        }
      }
      $scope.uploadClick = function (i) {
        $("#upload_" + i).click();
      }
      //$('.count-pairs').select2();
      $scope.changeImage = function (files, th) {
        var ind = $(th).attr("id").split('_')[1];
        files[0].index = ind;
        readURL(files);
        console.log($scope.images);
      }
      $scope.getArroundImages = function () {
        var ds = [];
        for (var i = 0; i < $scope.selectedImageLayout; i++)
          ds.push(i);
        console.log(ds.length);
        return ds;
      }
      //$scope.setIsRight = function (index) {
      //  $scope.images.forEach(function (im) {
      //    if (im.Index === index)
      //      im.IsRight = true;
      //    else
      //      im.IsRight = false;
      //  });
      //}
      $scope.setImageLayout = function (state) {
        $scope.selectedImageLayout = state;
      }
      function readURL(input) {
        if (input && input.length) {
          for (var i = 0; i < input.length; i++) {
            var reader = new FileReader();
            reader.onload = function (e) {
              e.currentTarget.ImageSrc = e.target.result;
              var index = e.target.file.index;
              $scope.images[index].ImageSrc = e.target.result;
              $scope.$digest();
            }
            reader.file = input[i];
            reader.readAsDataURL(input[i]);
          }
        }
      }
        $scope.card.checkData();
    }
  }
}
