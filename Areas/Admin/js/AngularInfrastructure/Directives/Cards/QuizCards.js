﻿quizCards.$inject = ["api"];
function quizCards(api) {
  return {
    restrict: 'E',
    scope: {
      card: '=card'
    },
    templateUrl: '/Areas/Admin/js/AngularInfrastructure/Directives/Cards/Templates/QuizTemplate.html',
    link: function ($scope, elem, attrs) {
      $scope.forJsonStructure = [{ Question: "", Answers: [] }];
      $scope.card.checkData = function () {
        if ($scope.card.Id && $scope.card.currentType === '3')
          $scope.forJsonStructure = JSON.parse($scope.card.AdditionalJSON);
      }
        $scope.card.checkData();
      $scope.card.addCard = function () {
        var nullQuiz = $scope.forJsonStructure.find(function (x) { return !x.Question });
        if (nullQuiz)
          return { Error: "Some questions is empty" };
        var empyRightAnswers = $scope.forJsonStructure.find(function (x) { return !x.Answers.find(function (f) { return f.IsRight }); });
        if (empyRightAnswers)
          return { Error: "Some questions has no rigth answer" };
        var hasNoAnswers = $scope.forJsonStructure.find(function (x) { return !x.Answers.length });
        if (hasNoAnswers)
          return { Error: "Some questions has no answers" };
        $scope.card.AdditionalJSON = JSON.stringify($scope.forJsonStructure);
        return $scope.card;
      }
      $scope.addAnswer = function(item) {
        item.Answers.push({ Variant: "", IsRight: false });
      }
      $scope.addNewQuestion = function() {
        $scope.forJsonStructure.push({ Question: "", Answers: [] });
      }
      $scope.checkAnswer = function(item, index) {
        item.Answers.forEach(function(x,i) {
          if (i === index)
            x.IsRight = true;
          else
            x.IsRight = false;
        });
      }
    }
  }
}