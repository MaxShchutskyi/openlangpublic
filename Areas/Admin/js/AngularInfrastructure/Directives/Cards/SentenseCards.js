﻿sentenseCards.$inject = ["api"];
function sentenseCards(api) {
    return {
        restrict: 'E',
        scope: {
            card: '=card'
        },
        templateUrl: function () {
            return '/Areas/Admin/js/AngularInfrastructure/Directives/Cards/Templates/SentenseTemplate.html?date=' +
                new Date();
        },
        link: function ($scope, elem, attrs) {
            $scope.model = {};
            $scope.words = [];
            var indexer = 0;
            $scope.buildAnswer = function (word) {
                if (word.Index !== -1) {
                    word.Index = -1;
                    return;
                }
                word.Index = ++indexer;
            }
            $scope.card.checkData = function () {
                if ($scope.card.Id && $scope.card.currentType === '6') {
                    $scope.Question = $scope.card.Question;
                    $scope.words = $scope.card.Answers;
                    indexer = 0;
                    for (var i = 0; i < $scope.card.Answers.length; i++) {
                        if (indexer < $scope.card.Answers[i].Index)
                            indexer = $scope.card.Answers[i].Index;
                    }
                }
            }
            $scope.addNewWord = function () {
                if (!$scope.model.enteringword) return;
                var st = $scope.model.enteringword.slice(0, $scope.model.enteringword.length);
                $scope.words.push({ Index: -1, Variant: st });
                $scope.model.enteringword = "";
            }
            $scope.card.checkData();
            $scope.card.addCard = function () {
                //if ($scope.words.find(function (x) { return x.Index === null })) {
                //  alert("You did not use all words!");
                //  return null;
                //}
                if (!$scope.Question) {
                    alert("You did not enter Question");
                    return null;
                }
                var sorted = $scope.words.sort(function (x, y) {
                    if (x.Index > y.Index)
                        return 1;
                    if (x.Index < y.Index)
                        return -1;
                    return 0;
                });
                //debugger;
                //for (var i = 0; i < sorted.length; i++) {
                //    if (sorted[i].Index === -1)
                //        continue;
                //    var nt = $scope.words.find(function (t) { return t.Index === sorted[i].Index });
                //    nt.Index = i;
                //}
                indexer = $scope.words.length;
                $scope.card.Question = $scope.Question;
                $scope.card.Answers = $scope.words;
                console.log($scope.card);
                return $scope.card;
            }
        }
    }
}