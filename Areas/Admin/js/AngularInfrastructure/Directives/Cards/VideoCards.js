﻿videoCards.$inject = ["api"];
function videoCards(api) {
    return {
        restrict: 'E',
        scope: {
            card: '=card'
        },
        templateUrl: function (elem, attrs) {
            return '/Areas/Admin/js/AngularInfrastructure/Directives/Cards/Templates/VideoTemplate.html?date=' + new Date();
        },
        link: function ($scope, elem, attrs) {
            $scope.model = {};
            $scope.answer = { IsRight: true, Variant: null, Question: null };
            $scope.card.checkData = function () {
                if (!$scope.card.Id || $scope.card.Type !== '7') return;
                $scope.answer.Variant = $scope.card.Answers[0].Variant;
                $scope.answer.Question = $scope.card.Question;
                if ($scope.card.VideoSrc.indexOf('youtube') > -1)
                    $scope.model.VideoSrc = $scope.card.VideoSrc;
                else {
                    $("#videoControl source").attr('src', $scope.card.VideoSrc);
                    $("#videoControl").load();
                }
            }
            $scope.$watch("model.VideoSrc", function (newModel, oldModel) {
                if (!newModel || !oldModel)
                    return;
            })
            $scope.getVideoId = function () {
                if (!$scope.model.VideoSrc) return;
                var url = new URL($scope.model.VideoSrc);
                return url.searchParams.get("v");
            }
            $scope.card.checkData();
            $scope.card.addCard = function () {
                if (!$scope.answer.Question)
                    return { Error: "You did not enter Question" };
                if (!$scope.card.VideoSrc && !$scope.model.VideoSrc)
                    return { Error: "You did not not upload video source" };
                if (!$scope.card.VideoSrc)
                    $scope.card.VideoSrc = $scope.model.VideoSrc;
                $scope.card.Question = $scope.answer.Question;
                $scope.card.Answers = [$scope.answer];
                return $scope.card;
            }
            $scope.uploadVideo = function () {
                $("#upload_video").click();
            }
            $scope.changeVideo = function (files) {
                readURL(files);
            }
            $scope.allowDrop = function (ev) {
                ev.preventDefault();
                console.log('Yea1')
            }
            $scope.drop = function (ev) {
                ev.preventDefault();
                $("#videoControl").load();
                console.log('Yea')
            }
            function readURL(input) {
                if (input && input.length) {
                    for (var i = 0; i < input.length; i++) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $scope.card.VideoSrc = e.target.result;
                            $("#videoControl source").attr('src', e.target.result);
                            $("#videoControl").load();
                            $scope.$digest();
                        }
                        reader.file = input[i];
                        reader.readAsDataURL(input[i]);
                    }
                }
            }
        }
    }
}
