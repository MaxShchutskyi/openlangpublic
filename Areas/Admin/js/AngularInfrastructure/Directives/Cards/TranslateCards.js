﻿translateCards.$inject = ["api", "recorderService","$timeout"];
function translateCards(api, recorderService, $timeout) {
    return {
        restrict: 'E',
        scope: {
            card: '=card'
        },
        templateUrl: function () {
            return '/Areas/Admin/js/AngularInfrastructure/Directives/Cards/Templates/TranslateTemplate.html?date' + new Date();
        },
        link: function ($scope, elem, attrs) {
            console.log("Translate Cards");
            $scope.startReccord = function () {
                if (!$scope.isRecording) {
                    $scope.isRecording = true;
                    document.getElementById('reccord-logo').classList.add('fa-stop');
                    document.getElementById('reccord-logo').classList.remove('fa-microphone');
                    recorderService.start(null, callback);
                }
                else {
                    document.getElementById('reccord-logo').classList.remove('fa-stop');
                    document.getElementById('reccord-logo').classList.add('fa-microphone');
                    recorderService.stop();
                    $scope.isRecording = false;
                }
            }
            var callback = function (res) {
                $scope.audio = res;
                $scope.audio.isNewRecord = true;
                $timeout(function () {
                    document.getElementById('audio').load();
                });
                //$scope.shared.selectedWord.audioPath = res.path;
                //var el = getElem($scope.mywords, local)
                //el.audioPath = res.path;
                //notifications.show('success', "Loaded");
                //$timeout(function () {
                //    document.getElementById('word' + $scope.shared.selectedWord.id).load();
                //});
            };
            $scope.card.addCard = function () {
                if (!$scope.Question) {
                    alert("You have no description");
                    return null;
                }
                if (!$scope.Answer) {
                    alert("You have no Answer");
                    return null;
                }
                $scope.card.Question = $scope.Question;
                $scope.card.Answers = [{ Variant: $scope.Answer, IsRight: true }];
                if ($scope.audio.isNewRecord) {
                    $scope.card.VideoSrc = $scope.audio.blob;
                    $scope.card.isNewRecord = true;
                }
                return $scope.card;
            }
            $scope.card.checkData = function () {
                //debugger;
                if ($scope.card.Id && $scope.card.currentType === '12') {
                    $scope.Answer = $scope.card.Answers[0].Variant;
                    $scope.Question = $scope.card.Question;
                    $scope.audio = { url: $scope.card.VideoSrc };
                    $timeout(function () {
                        document.getElementById('audio').load();
                    });
                }
            }
            //$scope.card.checkData();

        }
    }
}