﻿budgetStatistic.$inject = ["api"];
function budgetStatistic(api) {
    return {
        restrict: 'A',
        scope: false,
        templateUrl: "/Dashboard/NewMyAccount/GetBudgetStatistic",
        link: function (scope, elem, attrs) {
            scope.updateStatistic = function () {
                api.ajax(attrs.url, 'get', null, function (res) {
                    console.log(scope.shared.globalDelta);
                    scope.budget = res;
                    scope.recalculate();
                    scope.$digest();
                });
            }
            scope.$watch('shared.globalDelta', function (news, old) {
                if (news === old || !news)
                    return;
                scope.recalculate();
            })
            scope.recalculate = function () {
                scope.statistic = {};
                scope.statistic.thisMonth = (scope.budget.thisMonth * scope.shared.globalDelta).toFixed(2);
                scope.statistic.lastMonth = (scope.budget.lastMonth * scope.shared.globalDelta).toFixed(2);
                scope.statistic.total = (scope.budget.total * scope.shared.globalDelta).toFixed(2);
            }
            scope.updateStatistic();
        }
    }
}