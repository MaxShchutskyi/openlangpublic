﻿using System.Web.Mvc;

namespace Xencore.Entities.IdentityEntities.Entities.IdentityEntities.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "Admin";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new {controller= "Main", action = "Index", id = UrlParameter.Optional },
                namespaces:new[] { "Xencore.Areas.Admin.Controllers", "Xencore.Areas.Admin.Controllers.ForAngular" }
            );
        }
    }
}