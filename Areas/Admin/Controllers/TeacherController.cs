﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Services;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.IdentityEntities;
using EmailSenderService;
using EmailSenderService.Models;
//using MvcApplicationPostFinanceLatest.Entity;
using MvcApplicationPostFinanceLatest.Helpers;
using MvcApplicationPostFinanceLatest.Models;
using MvcApplicationPostFinanceLatest.Repository;
using MvcApplicationPostFinanceLatest.ViewModel;

using Microsoft.AspNet.Identity.Owin;
using Xencore.Areas.ViewModels;

using Xencore.Controllers;

namespace Xencore.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TeacherController : TemplateOpenlangController
    {
        private CustomUserManager CustomUserManager => HttpContext.GetOwinContext()
            .Get<CustomUserManager>();
        private WizHttpRequest oRequest;
        private XmlGetResponse xmlGet;
        private CreateResponseModel createResponseModel;
        private List<Data> responseList;
        protected CustomUserManager GetUserManager => HttpContext.GetOwinContext().Get<CustomUserManager>();

        #region GET
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ViewResult AddTeacher()
        {
            return View();
        }
        [WebMethod]
        [HttpPost]
        public async Task<JsonResult> ChangePassword(Guid id, string password, string confirmPassword)
        {
            await CustomUserManager.RemovePasswordAsync(id);
            await CustomUserManager.AddPasswordAsync(id, password);
            return Json(new {success = true});
        }
        [HttpPost]
        public async Task<JsonResult> ChangeRate(TeacherRate rate)
        {
            using (var context = new MyDbContext())
            {
                context.TeacherRates.Attach(rate);
                context.Entry(rate).State = EntityState.Modified;
                await context.SaveChangesAsync();
            }
            return Json(new { success = true });
        }
        [HttpGet]
        public ViewResult EditTeacher()
        {
            var presenterRepository = new PresenterRepository();
            var model = new ClassViewModel
            {
                Classes = presenterRepository.GetPresenters()
            };
            return View(model);
        }
        [HttpGet]
        public async Task<ViewResult> GetAllTeachersDetails()
        {
            await NewAutorizingLogic();
            ViewBag.Admin = "teacher";
            var presenterRepository = new PresenterRepository();
            return View(presenterRepository.GetAllPresenters());
        }
        [HttpGet]
        public async Task<ViewResult> GetDetailsOfTeacher(Guid id)
        {
            var presenterRepository = new PresenterRepository();
            var videmodel = new ViewModelForDetailsOfTeacher() { CustomUser = await presenterRepository.GetPresentersDetails(id) };
            return View(videmodel);
        }
        public async Task<JsonResult> ChangeITeachn(Guid id, IEnumerable<string> langs )
        {
            if (langs == null)
                return Json(new {success = false});
            var str = string.Join("",langs.Select(x => $"cap_{x}|"));
            using (var context = new MyDbContext())
            {
                var info = await context.Users.FirstOrDefaultAsync(x => x.Id == id);
                info.Ilearn = str;
                await context.SaveChangesAsync();
            }
            return Json(new { success = true });
        }
        public async Task<JsonResult> ChangeISpeak(Guid id, IEnumerable<string> langs)
        {
            if (langs == null)
                return Json(new { success = false });
            var str = string.Join("", langs.Select(x => $"cap_{x}|"));
            using (var context = new MyDbContext())
            {
                var info = await context.Users.FirstOrDefaultAsync(x => x.Id == id);
                info.Ispeak = str;
                await context.SaveChangesAsync();
            }
            return Json(new { success = true });
        }

        public async Task<JsonResult> ChangeVideoLink(Guid id, string url)
        {
            using (var context = new MyDbContext())
            {
                var info = await context.TeacherInfos.FirstOrDefaultAsync(x => x.Id == id);
                info.Link = url;
                await context.SaveChangesAsync();
            }
            return Json(new { success = true });
        }
        [HttpPost]
        public async Task<JsonResult> SendConfirmationAboutRegistration(Guid id)
        {
            await GetUserManager.SendEmailAsync(id, "Your application has been accepted. Sign your contract", EmailGeneratorService.GetBody(new AfterConfirmationApplication()));
            var user = await GetUserManager.FindByIdAsync(id);
            user.IsApplicationChanckedByAdmin = true;
            await GetUserManager.UpdateAsync(user);
            //var presenterRepository = new PresenterRepository();
            //var res = await presenterRepository.ChangeActivete(id, GetUserManager);
            return null;
        }
        [WebMethod]
        [HttpPost]
        public async Task<JsonResult> ChangeTeacherActive(Guid id)
        {
            var presenterRepository = new PresenterRepository();
            var res = await presenterRepository.ChangeActivete(id, GetUserManager);
            return Json(new { Success = res });
        }

        [WebMethod]
        [HttpPost]
        public async Task<JsonResult> GetCountUnscheduledLessons()
        {
            var presenterRepository = new PresenterRepository();
            var res = await presenterRepository.GetCountUnscheduledLessons();
            return Json(new { count = 1 });
        }

        [NonAction]
        private async Task<IEnumerable<Middle>> GetStat(string chart)
        {
            using (var context = new MyDbContext())
            {
                var date = DateTime.UtcNow;
                if (chart == "monthly")
                {
                    return await context.Users.Where(x => x.CreationDateTime.Year == date.Year).Include(x=>x.TeacherRate).Where(x=>x.TeacherRate!=null).Select(x => new Middle() { Date = x.CreationDateTime, Id = x.Id }).OrderByDescending(x => x.Date).ToArrayAsync();
                }
                if (chart == "weekly" || chart == "daily")
                {
                    return await context.Users.Where(x =>x.CreationDateTime.Month == date.Month).Include(x => x.TeacherRate).Where(x => x.TeacherRate != null).Select(x => new Middle() { Date = x.CreationDateTime, Id = x.Id }).OrderByDescending(x => x.Date).ToArrayAsync();
                }
                if (chart == "yearly")
                {
                    return await context.Users.Include(x => x.TeacherRate).Where(x => x.TeacherRate != null).Select(x => new Middle() { Date = x.CreationDateTime, Id = x.Id }).OrderByDescending(x => x.Date).ToArrayAsync();
                }
                return null;
            }
        }
        [HttpGet]
        public async Task<JsonResult> GetStats(string chat)
        {
            var res = await GetStat(chat);
            var labels = new List<string>();
            var data = new List<decimal>();
            if (chat == "monthly")
            {
                var datas = res.GroupBy(x => x.Date.Month).Select(x => new { month = x.Key, amount = x.Count() }).ToArray();
                for (var i = 1; i < DateTime.UtcNow.Month + 1; i++)
                {
                    labels.Add(CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i));
                    data.Add(datas.FirstOrDefault(x => x.month == i)?.amount ?? 0);
                }
            }
            if (chat == "weekly")
            {
                var date = DateTime.UtcNow;
                var date1 = new DateTime(date.Year, date.Month, 1);
                var st = date1;
                var p = 1;
                while ((date1 = date1.AddDays(7)).Month == date.Month)
                {
                    labels.Add(p.ToString());
                    var t = res.Count(x => x.Date > st && x.Date < date1);
                    data.Add(t);
                    st = st.AddDays(7);
                    p++;
                }
            }
            if (chat == "daily")
            {
                var date = DateTime.UtcNow;
                var countDaysInMonth = DateTime.DaysInMonth(date.Year, date.Month);
                for (var i = 1; i <= countDaysInMonth; i++)
                {
                    labels.Add(i.ToString());
                    data.Add(
                        res.Count(x => new DateTime(x.Date.Year, x.Date.Month, x.Date.Day) == new DateTime(date.Year, date.Month, i)));
                }
            }
            if (chat == "yearly")
            {
                //var datas = res.GroupBy(x => x.CreationDate.Month).Select(x => new { month = x.Key, amount = x.Sum(f => f.Amount) }).ToArray();
                labels.AddRange(new[] { "2016", "2017", "2018" });
                labels.ForEach(x => { data.Add(res.Count(f => f.Date.Year == int.Parse(x))); });
            }
            return Json(new { labels, data }, JsonRequestBehavior.AllowGet);
        }
        class Middle
        {
            public DateTime Date { get; set; }
            public Guid Id { get; set; }
        }
        //[WebMethod]
        //[HttpPost]
        //public async Task<JsonResult> ClearUnscgeduledLessons()
        //{
        //    var presenterRepository = new PresenterRepository();
        //    var res = await presenterRepository.ClearOldUnscheduledLessons();
        //    return Json(new { result = res });
        //}
        #endregion GET

        #region POST
        [HttpPost]
        public async Task<ViewResult> AddTeacher(AddTeacherRequestModel addTeacherRequestModel)
        {
            //try
            //{
            //    modelAggregator = new ModelAggregator();
            //    var currentPresenter = modelAggregator.GetPresenterModel(addTeacherRequestModel);//123
            //    var user = await
            //        CustomUserManager
            //            .FindByEmailAsync(currentPresenter.PresenterEmail);
            //    if (user == null)
            //    {
            //        var attendee = new CustomUser()
            //        {
            //            Name = currentPresenter.Name,
            //            UserName = currentPresenter.PresenterEmail,
            //            Email = currentPresenter.PresenterEmail,
            //            PhoneNumber = currentPresenter.PhoneNumber,
            //            TimeZone = currentPresenter.TimeZone
            //        };
            //        await CustomUserManager.CreateAsync(attendee, currentPresenter.Password);
            //        await CustomUserManager.AddToRoleAsync(attendee.Id, "Presenter");
            //    }
            //    else
            //        await CustomUserManager.AddToRoleAsync(user.Id, "Presenter");
            //    using (var context = new MyDbContext())
            //    {
            //        //context.Presenters.Add(currentPresenter);
            //        await context.SaveChangesAsync();
            //    }
            //    //var provider = Membership.Provider as CustomMembershipProvider;
            //    //MembershipUser membershipUser = provider?.CreateUser(currentPresenter);

            //}
            //catch (Exception exception)
            //{
            //    Response.Write(exception.ToString());
            //}

            return View();
            //NameValueCollection requestParameters = new NameValueCollection();
            //requestParameters["access_key"] = addTeacherRequestModel.AccessKey;
            //requestParameters["timestamp"] = addTeacherRequestModel.TimeStamp;
            //requestParameters.Add("method", addTeacherRequestModel.Method);
            //requestParameters["signature"] = addTeacherRequestModel.Signature;
            //requestParameters.Add("name", addTeacherRequestModel.Name);
            //requestParameters.Add("email", addTeacherRequestModel.Email);
            //requestParameters.Add("password", addTeacherRequestModel.Password);
            //requestParameters.Add("phone_number", addTeacherRequestModel.PhoneNumber);
            //requestParameters.Add("mobile_number", addTeacherRequestModel.MobileNumber);
            //requestParameters.Add("time_zone", addTeacherRequestModel.TimeZone);
            //requestParameters.Add("about_the_teacher", addTeacherRequestModel.AboutTheTeacher);
            //requestParameters.Add("can_schedule_class", (addTeacherRequestModel.CanScheduleClass) ? "1" : "0");
            //requestParameters.Add("is_active", (addTeacherRequestModel.IsActive) ? "1" : "0");
            //string postFilePath = addTeacherRequestModel.PostFilePath;

            //fromListToModel = new FromListToModel();
            //oRequest = new WizHttpRequest();
            //xmlGet = new XmlGetResponse();
            //responseList = new List<Data>();
            //try
            //{
            //    responseList = xmlGet.Parse(oRequest.WiZiQWebRequest(addTeacherRequestModel.ServiceRootUrl + "method=" + addTeacherRequestModel.Method, requestParameters, postFilePath));
            //    //createResponseModel = new CreateResponseModel();
            //    int presenterId = fromListToModel.GetPresenterId(responseList);
            //    if (presenterId != 0)
            //    {
            //        modelAggregator = new ModelAggregator();
            //        presenterRepository = new PresenterRepository();
            //        var currentPresenter = modelAggregator.GetPresenterModel(addTeacherRequestModel, presenterId);//123
            //        //presenterRepository.SavePresenter(currentPresenter);
            //        MembershipUser membershipUser = ((CustomMembershipProvider)Membership.Provider).CreateUser(currentPresenter);//addTeacherRequestModel.Email, addTeacherRequestModel.Password
            //    }
            //        //if (membershipUser != null)
            //        //{
            //        //    FormsAuthentication.SetAuthCookie(addTeacherRequestModel.Email, false);
            //        //}
            //}
            //catch (Exception)
            //{
            //    return View("Response", responseList);
            //}
            //return View("Response", responseList);
        }

        #endregion POST
    }
}
