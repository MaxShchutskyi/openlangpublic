﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories.Interfaces;
using Xencore.Controllers;

namespace Xencore.Areas.Admin.Controllers
{
    public class LessonsController : TemplateOpenlangController
    {
        private ISchedulerRepository Repo { get; set; }

        public LessonsController(ISchedulerRepository repo)
        {
            Repo = repo;
        }
        public async Task<ActionResult> Index(string type, string query)
        {
            await NewAutorizingLogic();
            if (string.IsNullOrEmpty(type) && string.IsNullOrEmpty(query))
            {
                var res = await Repo.GetAllSchedules();
                return View(res);
            }
            DateTime? startDate;
            DateTime endDate = DateTime.UtcNow;
            switch (type)
            {
                case "All": startDate = null; ViewBag.type = "All"; break;
                case "LastMonth": startDate = endDate.AddMonths(-1);  ViewBag.type = "LastMonth"; break;
                case "Today": startDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 0,0,0); ViewBag.type = "Today"; break;
                case "LastWeek": startDate = endDate.AddDays(-7); ViewBag.type = "LastWeek"; break;
                case "LastYear": startDate = endDate.AddYears(-1); ViewBag.type = "LastYear"; break;
                default:startDate = null; ViewBag.type = ""; break;
            }
            ViewBag.query = query;
            var rest = await Repo.GetAllSchedulesByParams(startDate, endDate, query);
            return View(rest);

        }

        protected override void Dispose(bool disposing)
        {
            Repo.Dispose();
            base.Dispose(disposing);
        }
    }
}