﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories.Interfaces;

namespace Xencore.Areas.Admin.Controllers.ForAngular
{
    public class PaymentsController : Controller
    {
        private IPaymentTransactions PaymentRepository { get; }

        public PaymentsController(IPaymentTransactions paymentRepo)
        {
            PaymentRepository = paymentRepo;
        }

        protected override void Dispose(bool disposing)
        {
            PaymentRepository.Dispose();
            base.Dispose(disposing);
        }
        public async Task<ActionResult> Index()
        {
            var res = await PaymentRepository.GetAllTransactionsForAdmin();
            return View(res);
        }
    }
}