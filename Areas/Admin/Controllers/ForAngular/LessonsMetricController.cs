﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using Domain.Repositories.Interfaces;
using DTO.Enums;

namespace Xencore.Areas.Admin.Controllers.ForAngular
{
    public class LessonsMetricController : Controller
    {
        private ISchedulerRepository Repo { get; set; }

        public LessonsMetricController(ISchedulerRepository repo)
        {
            Repo = repo;
        }
        // GET: Admin/LessonsMetric
        public ActionResult Index()
        {

            return View();
        }

        public async Task<JsonResult> GetData(string type, string query)
        {
            if (string.IsNullOrEmpty(type) && string.IsNullOrEmpty(query))
            {
                var res = await Repo.GetAllSchedules();
                return Json(ConvertToView(res), JsonRequestBehavior.AllowGet);

            }
            DateTime? startDate;
            DateTime endDate = DateTime.UtcNow;
            switch (type)
            {
                case "All": startDate = null; ViewBag.type = "All"; break;
                case "LastMonth": startDate = endDate.AddMonths(-1); ViewBag.type = "LastMonth"; break;
                case "Today": startDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 0, 0, 0); ViewBag.type = "Today"; break;
                case "LastWeek": startDate = endDate.AddDays(-7); ViewBag.type = "LastWeek"; break;
                case "LastYear": startDate = endDate.AddYears(-1); ViewBag.type = "LastYear"; break;
                default: startDate = null; ViewBag.type = ""; break;
            }
            ViewBag.query = query;
            var rest = await Repo.GetAllSchedulesByParams(startDate, endDate, query);
            return Json(ConvertToView(rest), JsonRequestBehavior.AllowGet);

        }

        [NonAction]
        private object ConvertToView(IEnumerable<Schedule> schedules)
        {
            return new
            {
                CountLessons = schedules.Count(),
                CountLessonsTrial = schedules.Count(x => x.Type == TypeLessons.Trial),
                CountLessonsBasic = schedules.Count(x => x.Type == TypeLessons.Basic),
                CountLessonsPremium = schedules.Count(x => x.Type == TypeLessons.Premium),
                CountLessonsGroup = schedules.Count(x => x.Type == TypeLessons.Group),
                TotalPrice = schedules.SelectMany(x => x.ScheduleTransactions).Sum(x => x.Price),
                PriceTrialLessons = schedules.Where(x => x.Type == TypeLessons.Trial)
                    .SelectMany(x => x.ScheduleTransactions).Sum(x => x.Price),
                PriceBasicLessons = schedules.Where(x => x.Type == TypeLessons.Basic)
                    .SelectMany(x => x.ScheduleTransactions).Sum(x => x.Price),
                PricePremiumLessons = schedules.Where(x => x.Type == TypeLessons.Premium)
                    .SelectMany(x => x.ScheduleTransactions).Sum(x => x.Price),
                PriceGroupLessons = schedules.Where(x => x.Type == TypeLessons.Group)
                    .SelectMany(x => x.ScheduleTransactions).Sum(x => x.Price),
                List = schedules.OrderByDescending(x => x.StartDateTimeDateTime).Select(t => new
                {
                    TeacherName = t.CustomUser.Name,
                    StartDateTime = t.StartDateTimeDateTime.ToString("dd/MM/yyyy hh:mm"),
                    Type = t.Type.ToString(),
                    Language = Resources.Resource.ResourceManager.GetString(t.Language),
                    Price = t.ScheduleTransactions.Sum(x => x.Price),
                    Students = t.ScheduleTransactions.OrderByDescending(x => x.CreatedDate).Select(f=> new
                    {
                        f.User.Name,
                        f.User.Email,
                        f.User.Location
                    })
                })
            };
        }
        protected override void Dispose(bool disposing)
        {
            Repo.Dispose();
            base.Dispose(disposing);
        }
    }
}