﻿using Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Xencore.Areas.Admin.Controllers.ForAngular
{
    public class ChaptersController : Controller
    {
        IChapterRepo Repo { get; set; }
        public ChaptersController(IChapterRepo repo)
        {
            Repo = repo;
        }
        // GET: Admin/Chapters
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> UploadChapterIcon(int chapterId)
        {
            if (Request.Files.Count <= 0) return Json(new { path = "" });
            var files = Request.Files;
            var name = Guid.NewGuid() + ".jpg";
            if (!Directory.Exists(Server.MapPath($"~/Images/Icons/")))
                Directory.CreateDirectory(Server.MapPath($"~/Images/Icons/"));
            var serverpath = Server.MapPath($"~/Images/Icons/{name}");
            var directory = $"/Images/Icons/{name}";
            var pic = files[0];
            pic.SaveAs(serverpath);
            var oldPath = await Repo.UpdateIcon(chapterId, directory);
            if (oldPath != null)
                System.IO.File.Delete(Server.MapPath($"~/Images/Icons/") + oldPath.Split('/').Last());
            return Json(directory);
        }
        protected override void Dispose(bool disposing)
        {
            Repo?.Dispose();
            base.Dispose(disposing);
        }
    }
}