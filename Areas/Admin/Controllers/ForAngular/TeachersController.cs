﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.IdentityEntities;
using EmailSenderService;
using EmailSenderService.Models;
using Microsoft.AspNet.Identity.Owin;
using MvcApplicationPostFinanceLatest.Repository;
using Xencore.Areas.ViewModels;

namespace Xencore.Areas.Admin.Controllers.ForAngular
{
    public class TeachersController : Controller
    {
        private CustomUserManager CustomUserManager => HttpContext.GetOwinContext()
            .Get<CustomUserManager>();
        // GET: Admin/Teachers
        public ActionResult Index()
        {
            var presenterRepository = new PresenterRepository();
            return View("~/Areas/Admin/Views/Teachers/View.cshtml", presenterRepository.GetAllPresenters());
        }
        [HttpPost]
        public async Task<JsonResult> ChangePassword(Guid id, string password, string confirmPassword)
        {
            await CustomUserManager.RemovePasswordAsync(id);
            await CustomUserManager.AddPasswordAsync(id, password);
            return Json(new { success = true });
        }

        [HttpPost]
        public async Task<JsonResult> SendConfirmationAboutRegistration(Guid id)
        {
            await CustomUserManager.SendEmailAsync(id, "Your application has been accepted. Sign your contract",
                EmailGeneratorService.GetBody(new AfterConfirmationApplication()));
            var user = await CustomUserManager.FindByIdAsync(id);
            user.IsApplicationChanckedByAdmin = true;
            await CustomUserManager.UpdateAsync(user);
            //var presenterRepository = new PresenterRepository();
            //var res = await presenterRepository.ChangeActivete(id, GetUserManager);
            return Json(new {});
        }

        [HttpGet]
        public async Task<ActionResult> GetDetails(string id)
        {
            var presenterRepository = new PresenterRepository();
            var videmodel = new ViewModelForDetailsOfTeacher() { CustomUser = await presenterRepository.GetPresentersDetails(Guid.Parse(id)) };
            return View(videmodel);
        }
        [HttpPost]
        public async Task<JsonResult> ChangeITeachn(Guid id, IEnumerable<string> langs)
        {
            if (langs == null)
                return Json(new { success = false });
            var str = string.Join("", langs.Select(x => $"cap_{x}|"));
            using (var context = new MyDbContext())
            {
                var info = await context.Users.FirstOrDefaultAsync(x => x.Id == id);
                info.Ilearn = str;
                await context.SaveChangesAsync();
            }
            return Json(new { success = true });
        }
        [HttpPost]
        public async Task<JsonResult> ChangeVideoLink(Guid id, string url)
        {
            using (var context = new MyDbContext())
            {
                var info = await context.TeacherInfos.FirstOrDefaultAsync(x => x.Id == id);
                info.Link = url;
                await context.SaveChangesAsync();
            }
            return Json(new { success = true });
        }
        [HttpPost]
        public async Task<JsonResult> ChangeISpeak(Guid id, IEnumerable<string> langs)
        {
            if (langs == null)
                return Json(new { success = false });
            var str = string.Join("", langs.Select(x => $"cap_{x}|"));
            using (var context = new MyDbContext())
            {
                var info = await context.Users.FirstOrDefaultAsync(x => x.Id == id);
                info.Ispeak = str;
                await context.SaveChangesAsync();
            }
            return Json(new { success = true });
        }
        [HttpPost]
        public async Task<JsonResult> ChangeRate(TeacherRate rate)
        {
            using (var context = new MyDbContext())
            {
                context.TeacherRates.Attach(rate);
                context.Entry(rate).State = EntityState.Modified;
                await context.SaveChangesAsync();
            }
            return Json(new { success = true });
        }
        [HttpGet]
        public async Task<JsonResult> GetStats(string chat)
        {
            var res = await GetStat(chat);
            var labels = new List<string>();
            var data = new List<decimal>();
            if (chat == "monthly")
            {
                var datas = res.GroupBy(x => x.Date.Month).Select(x => new { month = x.Key, amount = x.Count() }).ToArray();
                for (var i = 1; i < DateTime.UtcNow.Month + 1; i++)
                {
                    labels.Add(CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i));
                    data.Add(datas.FirstOrDefault(x => x.month == i)?.amount ?? 0);
                }
            }
            if (chat == "weekly")
            {
                var date = DateTime.UtcNow;
                var date1 = new DateTime(date.Year, date.Month, 1);
                var st = date1;
                var p = 1;
                while ((date1 = date1.AddDays(7)).Month == date.Month)
                {
                    labels.Add(p.ToString());
                    var t = res.Count(x => x.Date > st && x.Date < date1);
                    data.Add(t);
                    st = st.AddDays(7);
                    p++;
                }
            }
            if (chat == "daily")
            {
                var date = DateTime.UtcNow;
                var countDaysInMonth = DateTime.DaysInMonth(date.Year, date.Month);
                for (var i = 1; i <= countDaysInMonth; i++)
                {
                    labels.Add(i.ToString());
                    data.Add(
                        res.Count(x => new DateTime(x.Date.Year, x.Date.Month, x.Date.Day) == new DateTime(date.Year, date.Month, i)));
                }
            }
            if (chat == "yearly")
            {
                //var datas = res.GroupBy(x => x.CreationDate.Month).Select(x => new { month = x.Key, amount = x.Sum(f => f.Amount) }).ToArray();
                labels.AddRange(new[] { "2016", "2017", "2018" });
                labels.ForEach(x => { data.Add(res.Count(f => f.Date.Year == int.Parse(x))); });
            }
            return Json(new { labels, data }, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        private async Task<IEnumerable<Middle>> GetStat(string chart)
        {
            using (var context = new MyDbContext())
            {
                var date = DateTime.UtcNow;
                if (chart == "monthly")
                {
                    return await context.Users.Where(x => x.CreationDateTime.Year == date.Year)
                        .Include(x => x.TeacherRate).Where(x => x.TeacherRate != null)
                        .Select(x => new Middle() {Date = x.CreationDateTime, Id = x.Id})
                        .OrderByDescending(x => x.Date).ToArrayAsync();
                }
                if (chart == "weekly" || chart == "daily")
                {
                    return await context.Users.Where(x => x.CreationDateTime.Month == date.Month)
                        .Include(x => x.TeacherRate).Where(x => x.TeacherRate != null)
                        .Select(x => new Middle() {Date = x.CreationDateTime, Id = x.Id})
                        .OrderByDescending(x => x.Date).ToArrayAsync();
                }
                if (chart == "yearly")
                {
                    return await context.Users.Include(x => x.TeacherRate).Where(x => x.TeacherRate != null)
                        .Select(x => new Middle() {Date = x.CreationDateTime, Id = x.Id})
                        .OrderByDescending(x => x.Date).ToArrayAsync();
                }
                return null;
            }
        }

    }
    class Middle
    {
        public DateTime Date { get; set; }
        public Guid Id { get; set; }
    }
}