﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xencore.Controllers;

namespace Xencore.Areas.Admin.Controllers.ForAngular
{
    [Authorize(Roles = "Admin")]
    public class CardTemplatesController : TemplateOpenlangController
    {
        [HttpGet]
        // GET: Admin/CardTemplates
        public ActionResult ImageCards()
        {
            return View();
        }
    }
}