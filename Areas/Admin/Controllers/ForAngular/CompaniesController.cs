﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;
using Domain.Repositories.Interfaces;
using Microsoft.AspNet.Identity.Owin;

namespace Xencore.Areas.Admin.Controllers.ForAngular
{
    public class CompaniesController : Controller
    {
        private ICompanyRepository Repo { get; set; }
        public CompaniesController(ICompanyRepository repo)
        {
            Repo = repo;
        }
        // GET: Admin/Companies
        public async Task<ActionResult> Index()
        {
            
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> GetAllCompanies()
        {
            var res = await Repo.GetAllCompanies();
            return Json(res.Select(x => new {x.Id, x.Name, x.Email, x.CreationDateTime, x.StudentsOfCompany.Count}).ToArray(),
                JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<JsonResult> AddCompany(PreSignInUser user)
        {
            if (!ModelState.IsValid)
                return null;
            var manager = HttpContext.GetOwinContext().Get<CustomUserManager>();
            var result = Mapper.Map<CustomUser>(user);
            result.Ilearn = "cap_" + user.ChosenLang + "|";
            await manager.CreateAsync(result, user.Password);
            await manager.AddToRoleAsync(result.Id, "Company");
            return Json(user);
        }

        [HttpGet]
        public async Task<ActionResult> CompanyDetails()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            Repo?.Dispose();
            base.Dispose(disposing);
        }
    }
}