﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Xencore.Models;
using Xencore.Repositories;

namespace Xencore.Areas.Admin.Controllers.ForAngular
{
    public class DiscountsController : Controller
    {
        // GET: Admin/Discounts
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<JsonResult> GetDiscounts()
        {
            var repo = await new DiscountRepository().GetAllDiscounts();
            return Json(repo.OrderByDescending(x=>x.PeriodAccessStart), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<JsonResult> AddDiscount(DiscountViewModel model)
        {
            if (!ModelState.IsValid) return null;
            var res = await new DiscountRepository().AddNewDiscount(model);
            return Json(res);
        }
    }
}