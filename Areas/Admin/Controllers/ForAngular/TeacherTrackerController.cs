﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Context;
using Domain.Entities.IdentityEntities;

namespace Xencore.Areas.Admin.Controllers.ForAngular
{
    public class TeacherTrackerController : Controller
    {
        // GET: Admin/TeacherScheduler
        public ActionResult Index()
        {
            using (var context = new MyDbContext())
            {
                var newDate = DateTime.UtcNow.Next(DayOfWeek.Sunday);
                var dateTimePlusSevenDays = newDate.AddDays(7);
                var tdAdd25Days = newDate.AddDays(28);
                var roles =
                    context.Roles.Where(x => x.Name == "Presenter")
                        .SelectMany(x => x.Users)
                        .Select(x => x.UserId)
                        .ToList();
                //var teachers =
                //    context.Users.Where(x => roles.Contains(x.Id) && x.Active)
                //        .ToList();
                var selectedNull = context.Users.Where(x => roles.Contains(x.Id) && x.Active).Where(
                    x =>
                        x.Schedules.All(
                            f =>
                                !(f.StartDateTimeDateTime > newDate &&
                                  f.StartDateTimeDateTime < tdAdd25Days))).ToArray();
                var ids = selectedNull.Select(x => x.Id).ToArray();
                var th = context.Users.Where(x => roles.Contains(x.Id) && x.Active).Where(x => ids.All(f => f != x.Id)).ToArray();
                var firstWeek = th.Where(
                        x =>
                            x.Schedules.Count(f => f.StartDateTimeDateTime > newDate &&
                                                   f.StartDateTimeDateTime < dateTimePlusSevenDays) < 1)
                    .ToList();
                var secondWeek = th.Where(
                        x =>
                            x.Schedules.Count(f => f.StartDateTimeDateTime > dateTimePlusSevenDays &&
                                                   f.StartDateTimeDateTime <
                                                   dateTimePlusSevenDays.AddDays(7)) < 1)
                    .ToList();
                var thirdWeek = th.Where(
                        x =>
                            x.Schedules.Count(f =>
                                f.StartDateTimeDateTime > dateTimePlusSevenDays.AddDays(7) &&
                                f.StartDateTimeDateTime < dateTimePlusSevenDays.AddDays(14)) <
                            1)
                    .ToList();
                var forthWeek = th.Where(
                        x =>
                            x.Schedules.Count(
                                f => f.StartDateTimeDateTime > dateTimePlusSevenDays.AddDays(14) &&
                                     f.StartDateTimeDateTime < dateTimePlusSevenDays.AddDays(21)) < 1)
                    .ToList();
                var dic = new Dictionary<string, List<CustomUser>>()
                            {
                                {$"from {newDate:dd.MM.yyyy} to {dateTimePlusSevenDays:dd.MM.yyyy}", firstWeek},
                                {
                                    $"from {dateTimePlusSevenDays:dd.MM.yyyy} to {dateTimePlusSevenDays.AddDays(7):dd.MM.yyyy}",
                                    secondWeek
                                },
                                {
                                    $"from {dateTimePlusSevenDays.AddDays(7):dd.MM.yyyy} to {dateTimePlusSevenDays.AddDays(14):dd.MM.yyyy}",
                                    thirdWeek
                                },
                                {
                                    $"from {dateTimePlusSevenDays.AddDays(14):dd.MM.yyyy} to {dateTimePlusSevenDays.AddDays(21):dd.MM.yyyy}",
                                    forthWeek
                                }
                            };
                //SendToEmailTeachers(selectedNull, dic);
                ViewBag.Users = selectedNull;
                ViewBag.Less = dic;
            }
            return View();
        }
    }
    public static class Extentions
    {
        public static DateTime Next(this DateTime from, DayOfWeek dayOfWeek)
        {
            int start = (int)from.DayOfWeek;
            int target = (int)dayOfWeek;
            if (target <= start)
                target += 7;
            return from.AddDays(target - start);
        }
    }
}