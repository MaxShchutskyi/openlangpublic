﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Helpers.Abstract;
using Domain.Repositories.Interfaces;

namespace Xencore.Areas.Admin.Controllers.ForAngular
{
    public class MonthlyEarnings2Controller : Controller
    {
        private ISchedulerRepository Repo { get; set; }
        private ITeacherManager TeacherManager { get; set; }

        public MonthlyEarnings2Controller(ISchedulerRepository repo, ITeacherManager manager)
        {
            Repo = repo;
            TeacherManager = manager;
        }
        // GET: Admin/MonthlyEarnings
        public async Task<ActionResult> Index()
        {
            var date = DateTime.UtcNow;
            var startDate = DateTime.UtcNow;
            if (date.Day < 25)
                startDate = startDate.AddMonths(-1);
            //var endDate = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month)).AddDays(1);
            var endDate = date;
            var res = await Repo.GetAllSchedulesForPeriod(startDate, endDate);
            var overal = TeacherManager.SetMonthlyEarnings(res);
            return View("~/Areas/Admin/Views/MonthlyEarnings/Index.cshtml", overal);
        }

        protected override void Dispose(bool disposing)
        {
            Repo?.Dispose();
            base.Dispose(disposing);
        }
    }
}