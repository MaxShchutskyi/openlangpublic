﻿using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Xencore.Areas.Admin.Controllers.ForAngular
{
    public class AttendeesController : Controller
    {
        public ActionResult Index()
        {
            var attendeeRepository = new AttendeeRepository();
            var attren = attendeeRepository.GetAttendees();
            return View("~/Areas/Admin/Views/Attendees/View.cshtml", attren);
        }

        public async Task<ActionResult> GetDetails(string id)
        {
            var attendeeRepository = new MvcApplicationPostFinanceLatest.Repository.AttendeeRepository();
            return View(await attendeeRepository.GetAttendeeAllInfo(Guid.Parse(id)));
        }
    }
}