﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ImageSaver.Converters;

namespace Xencore.Areas.Admin.Controllers.ForAngular
{
    public class CardsController : Controller
    {
        AudioConverter Converter { get; }

        public CardsController()
        {
            Converter = new AudioConverter();
        }

        // GET: Admin/Cards
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CardDetails()
        {
            return View();
        }
        [HttpPost]
        public string ConvertToMp3()
        {
            var files = Request.Files;
            if (files.Count == 0)
                return null;
            var res = Converter.Execute(files[0]?.InputStream);
            var arr = (res as MemoryStream)?.ToArray();
            return Convert.ToBase64String(arr);
        }

        protected override void Dispose(bool disposing)
        {
            Converter.Dispose();
            base.Dispose(disposing);
        }
    }
}