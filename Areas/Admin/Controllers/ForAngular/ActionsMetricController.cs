﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Helpers;
using Domain.Repositories.Interfaces;
using Action = Domain.Entities.Action;

namespace Xencore.Areas.Admin.Controllers.ForAngular
{
    public class ActionsMetricController : Controller
    {
        private IActionReoisitory Repo { get; set; }

        public ActionsMetricController(IActionReoisitory repo)
        {
            Repo = repo;
        }
        // GET: Admin/Action
        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> GetData(string type)
        {
            if (string.IsNullOrEmpty(type))
            {
                var res = await Repo.GetActions(null, null);
                return Json(ConvertData(res), JsonRequestBehavior.AllowGet);
            }
            DateTime? startDate;
            DateTime endDate = DateTime.UtcNow;
            switch (type)
            {
                case "All": startDate = null; ViewBag.type = "All"; break;
                case "LastMonth": startDate = endDate.AddMonths(-1); ViewBag.type = "LastMonth"; break;
                case "Today": startDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 0, 0, 0); ViewBag.type = "Today"; break;
                case "LastWeek": startDate = endDate.AddDays(-7); ViewBag.type = "LastWeek"; break;
                case "LastYear": startDate = endDate.AddYears(-1); ViewBag.type = "LastYear"; break;
                default: startDate = null; ViewBag.type = ""; break;
            }
            var rest = await Repo.GetActions(startDate, endDate);
            ViewBag.type = type;
            return Json(ConvertData(rest), JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public object ConvertData(IEnumerable<Action> actions)
        {
            return new
            {
                CountLessonsCreated = actions.Count(x => x.Status == MessageStatus.LessonCreated),
                CountUserRegistrated = actions.Count(x => x.Status == MessageStatus.UserRegistrated),
                CountLessonsCancel = actions.Count(x => x.Status == MessageStatus.LessonCancel),
                CountLessonsChanged = actions.Count(x => x.Status == MessageStatus.LessonChanged),
                CountLessonsRescheduled = actions.Count(x => x.Status == MessageStatus.LessosRescheduled),
                CountAddedStudentsToLesson = actions.Count(x => x.Status == MessageStatus.AddedStudentToLesson),
                CountLessonsCompleted = actions.Count(x => x.Status == MessageStatus.PaymentCompleted),
                Messages = actions.Select(x=> new
                {
                    Name = x.CusromUserId.Name,
                    Date = x.ActionDate.ToString("dd-MM-yyyy hh:mm"),
                    Message = x.Message,
                    ShortMessage = x.ShortMessage
                })
            };
        }
    }
}