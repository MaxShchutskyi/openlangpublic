﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Xencore.Areas.Dashboard.Controllers;
using Xencore.Filters;

namespace Xencore.Areas.Admin.Controllers.ForAngular
{
    [Authorize(Roles = "Admin")]
    [GlobalUserDetect]
    public class MainController : ParentController
    {
        // GET: Admin/Main
        public async Task<ActionResult> Index()
        {
            var user = await GetUserManager.FindByIdAsync(Guid.Parse(User.Identity.GetUserId()));
            ViewBag.User = JsonConvert.SerializeObject(new { profilePicture = user.ProfilePicture, name = user.Name, email = user.Email });
            return View();
        }
    }
}