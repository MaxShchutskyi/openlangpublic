﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Domain.Context;
using DTO;
using Microsoft.AspNet.Identity;
using Repositories.Abstract;

namespace Xencore.Areas.Admin.Controllers.ForAngular.api
{
    [System.Web.Mvc.Authorize]
    public class ChapterController : ApiController
    {
        private IChapterRepo Repo { get; set; }

        public ChapterController(IChapterRepo repo)
        {
            this.Repo = repo;
        }
        [HttpGet]
        public async Task<IEnumerable<object>> GetChaptersTable()
        {
            return await Repo.GetAllChaptersForTable();
        }
        [System.Web.Mvc.HttpPost]
        public async Task<bool> UpdateOrder(IEnumerable<AddUpdateChapterClass> cards)
        {
            var ids = cards.Select(x => x.Id).ToArray();
            using (var context = new MyDbContext())
            {
                var chapters = await context.Chapters.Where(x => ids.Contains(x.Id)).ToArrayAsync();
                foreach (var chap in chapters)
                {
                    var id = cards.First(x => x.Id == chap.Id);
                    chap.Index = id.Index;
                }
                await context.SaveChangesAsync();
            }
            return true;
        }
        [HttpPost]
        public async Task<AddUpdateChapterClass> AddOrUpdateChapter(AddUpdateChapterClass chapter)
        {
            chapter.Creator = Guid.Parse(User.Identity.GetUserId());
            return await Repo.AddOrUpdateChapter(chapter);
        }
        [HttpGet]
        public async Task<AddUpdateChapterClass> GetChapterById(int chapter)
        {
            return await Repo.GetChapterId(chapter);
            //chapter.Creator = Guid.Parse(User.Identity.GetUserId());
            //return await Repo.AddOrUpdateChapter(chapter);
        }

        protected override void Dispose(bool disposing)
        {
            Repo?.Dispose();
            base.Dispose(disposing);
        }
    }
}
