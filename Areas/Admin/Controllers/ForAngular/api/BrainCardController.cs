﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using BrainCardManager;
using Domain.Context;
using DTO;
using ImageSaver.Converters;
using Microsoft.AspNet.Identity;
using Repositories.Abstract;
using WebGrease.Css.Extensions;

namespace Xencore.Areas.Admin.Controllers.ForAngular.api
{
    [System.Web.Mvc.Authorize]
    public class BrainCardController : ApiController
    {
        private IBrainCardRepo Repo { get; set; }
        private IBrainCardManager Manager { get; set; }

        public BrainCardController(IBrainCardRepo repo, IBrainCardManager manager)
        {
            Repo = repo;
            Manager = manager;
        }
        [System.Web.Mvc.HttpGet]
        public async Task<IEnumerable<BrainCardDto>> GetAllCards()
        {
            var res  = await Repo.GetAllCards();
            var brainCardDtos = res as BrainCardDto[] ?? res.ToArray();
            brainCardDtos.ToList().ForEach(x => { x.DateTimeUtcString = x.UploadDate.ToString("dd.MM.yyyy");
                x.Language = Resources.Resource.ResourceManager.GetString(x.Language);
                x.TypeCardName = x.Type.ToString();
            });
            return brainCardDtos;
        }
        [System.Web.Mvc.HttpGet]
        public async Task<BrainCardDto> GetCardById(int id)
        {
            return await Repo.GatCardById(id);
        }
        [System.Web.Mvc.HttpGet]
        public async Task<IEnumerable<BrainCardDto>> GetCardsByLessonId(int id)
        {
            var res = await Repo.GetCardsByLessonId(id);
            res.ForEach(x => x.DateTimeUtcString = x.UploadDate.ToString("dd.MM.yyyy"));
            return res.ToArray();
        }
        [System.Web.Mvc.HttpPost]
        public async Task<BrainCardDto> AddNewCard(BrainCardDto card)
        {
            card.UploadedUserId = Guid.Parse(User.Identity.GetUserId());
            return await Repo.AddNewCard(card);
        }
        [System.Web.Mvc.HttpPost]
        public async Task<BrainCardDto> UpdateOrder(IEnumerable<BrainCardDto> cards)
        {
            return await Repo.UpdateOrder(cards);
        }
        [System.Web.Mvc.HttpPost]
        public async Task<BrainCardDto> UpdateCard(BrainCardDto card)
        {
            card.UploadedUserId = Guid.Parse(User.Identity.GetUserId());
            return await Manager.UpdateBrainCard(card);
        }
        [System.Web.Mvc.HttpDelete]
        public async Task<bool> DeleteCard(int id)
        {
            using (var context = new MyDbContext())
            {
                var slide = await context.BrainCards.FindAsync(id);
                if(slide  == null)
                    return false;
                context.BrainCards.Remove(slide);
                await context.SaveChangesAsync();
                return true;
            }
        }

        protected override void Dispose(bool disposing)
        {
            Repo?.Dispose();
            base.Dispose(disposing);
        }
    }
}