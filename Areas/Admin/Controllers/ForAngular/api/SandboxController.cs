﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DTO;
using Repositories.Abstract;

namespace Xencore.Areas.Admin.Controllers.ForAngular.api
{
    public class SandboxController : ApiController
    {
        private IBrainCardRepo Repo { get; set; }

        public SandboxController(IBrainCardRepo repo)
        {
            Repo = repo;
        }

        public async Task<BrainCardDto> GetRandomCard()
        {
            return await Repo.GetRandomCard();
        }
    }
}
