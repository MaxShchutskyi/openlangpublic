﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DTO;
using Microsoft.AspNet.Identity;
using Owin.Security.Providers.EVEOnline;
using Repositories.Abstract;

namespace Xencore.Areas.Admin.Controllers.ForAngular.api
{
    public class LessonsController : ApiController
    {
        private ILessonsRepo Repo { get; }

        public LessonsController(ILessonsRepo repo)
        {
            this.Repo = repo;
        }
        [HttpGet]
        public async Task<IEnumerable<object>> GetLessonsTable()
        {
            return await Repo.GetAllLessonsForTable();
        }
        [HttpGet]
        public async Task<AddOrUpdateLesson> GetLessonsById(int id)
        {
            return await Repo.GetLessonById(id);
        }
        [HttpGet]
        public async Task<object> GetLessonsByChapterId(int id)
        {
            return await Repo.GetAllLessonsByChapterId(id);
        }
        [HttpPost]
        public async Task<AddOrUpdateLesson> AddOrUpdateLesson(AddOrUpdateLesson lesson)
        {
            lesson.Creator = Guid.Parse(User.Identity.GetUserId());
            return await Repo.AddOrUpdateLessons(lesson);
        }

        [HttpGet]
        public IEnumerable<string> GetSlidesFromImages(int lessonId)
        {
            System.IO.DirectoryInfo dInfo = new System.IO.DirectoryInfo(System.Web.Hosting.HostingEnvironment.MapPath($"~/Images/PdfFiles/{lessonId}"));
            return dInfo.GetFiles().Select(x => $"/Images/PdfFiles/{lessonId}/" + x.Name);
        }

        protected override void Dispose(bool disposing)
        {
            Repo?.Dispose();
            base.Dispose(disposing);
        }
    }
}
