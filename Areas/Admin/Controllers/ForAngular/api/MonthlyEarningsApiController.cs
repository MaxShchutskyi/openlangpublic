﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Domain.Entities.HelpersEntity;
using Domain.Helpers.Abstract;
using Domain.Repositories.Interfaces;

namespace Xencore.Areas.Admin.Controllers.ForAngular.api
{
    public class MonthlyEarningsApiController : ApiController
    {
        private ISchedulerRepository Repo { get; set; }
        private ITeacherManager TeacherManager { get; set; }

        public MonthlyEarningsApiController(ISchedulerRepository repo, ITeacherManager manager)
        {
            Repo = repo;
            TeacherManager = manager;
        }
        // GET: Admin/MonthlyEarnings
        public async Task<IEnumerable<MonthlyEarningHelper>> GetMonthlyEarnings(DateTime date)
        {
            //var endDate = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month)).AddDays(1);
            var endDate = date.AddMonths(-1);
            var res = await Repo.GetAllSchedulesForPeriod(endDate, date);
            var overal = TeacherManager.SetMonthlyEarnings(res);
            return overal;
        }
        
        protected override void Dispose(bool disposing)
        {
            Repo?.Dispose();
            base.Dispose(disposing);
        }
    }
}
