﻿using Domain.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Xencore.Areas.Admin.Controllers.ForAngular.api
{
    public class LevelController : ApiController
    {
        [HttpGet]
        public async Task<IEnumerable<object>> Get()
        {
            using (var context = new MyDbContext())
            {
                return await context.Levels.Select(x => new { x.Id, x.Name }).ToArrayAsync();
            }
        }
    }
}
