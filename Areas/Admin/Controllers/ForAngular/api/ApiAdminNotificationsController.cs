﻿using Domain.Context;
using Domain.Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Xencore.Areas.Admin.Controllers.ForAngular.api
{
    public class ApiAdminNotificationsController : ApiController
    {
        [HttpGet]
        public async Task<IEnumerable<object>> GetAllNotes()
        {
            using (var context = new MyDbContext())
            {
                return await context.Notifications.Select(x => new { x.Id, x.Message, x.Title, x.CreationDate }).ToArrayAsync();
            }
        }
        [HttpPost]
        public async Task<object> CreateNotification(Notification note)
        {
            if (note == null) return null;
            var userId = Guid.Parse(User.Identity.GetUserId());
            note.UserId = userId;
            note.CreationDate = DateTime.UtcNow;
            using (var context = new MyDbContext())
            {
                var userIds = await context.Users.Select(x => x.Id).ToArrayAsync();
                context.Notifications.Add(note);
                await context.SaveChangesAsync();
                var  NotificationsUsers = userIds.Select(x => new NotificationsUsers() { IsRead = false, CustomUserId = x, NotificationId = note.Id }).ToArray();
                await context.BulkInsertAsync(NotificationsUsers);
                //await context.BulkSaveChangesAsync();
                return new { note.Title, note.Message, note.CreationDate, note.Id };
            }
        }
    }
}
