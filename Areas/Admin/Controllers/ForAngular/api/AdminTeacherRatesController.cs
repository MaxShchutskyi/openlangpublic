﻿using Domain.Context;
using Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Xencore.Areas.Admin.Controllers.ForAngular.api
{
    [Authorize(Roles ="Presenter")]
    public class AdminTeacherRatesController : ApiController
    {
        public async Task<IEnumerable<object>> GetTeachersRates()
        {
            using (var context = new MyDbContext())
            {
                var usersIds = context.Roles.Where(x => x.Name == "Presenter").SelectMany(x => x.Users).Select(x => x.UserId).Distinct();
                return await context.Users.Where(x=>usersIds.Contains(x.Id) && x.IndividualTeacherRates.Any()).Include(x => x.IndividualTeacherRates).Select(x => new
                {
                    x.Name,
                    Id = x.IndividualTeacherRates.FirstOrDefault() == null ? -1 : x.IndividualTeacherRates.FirstOrDefault().Id,
                    Trial = x.IndividualTeacherRates.FirstOrDefault() == null ? -1 : x.IndividualTeacherRates.FirstOrDefault().Trial,
                    OneLessonRate = x.IndividualTeacherRates.FirstOrDefault() == null ? -1 : x.IndividualTeacherRates.FirstOrDefault().OneLessonRate,
                    FiveLessonRate = x.IndividualTeacherRates.FirstOrDefault() == null ? -1 : x.IndividualTeacherRates.FirstOrDefault().FiveLessonRate,
                    TenLessonRate = x.IndividualTeacherRates.FirstOrDefault() == null ? -1 : x.IndividualTeacherRates.FirstOrDefault().TenLessonRate,
                    FifteenLessonRate = x.IndividualTeacherRates.FirstOrDefault() == null ? -1 : x.IndividualTeacherRates.FirstOrDefault().FifteenLessonRate,
                    TrialPercent = x.IndividualTeacherRates.FirstOrDefault() == null ? -1 : x.IndividualTeacherRates.FirstOrDefault().TrialPercent,
                    OneLessonRatePercent = x.IndividualTeacherRates.FirstOrDefault() == null ? -1 : x.IndividualTeacherRates.FirstOrDefault().OneLessonRatePercent,
                    FiveLessonRatePercent = x.IndividualTeacherRates.FirstOrDefault() == null ? -1 : x.IndividualTeacherRates.FirstOrDefault().FiveLessonRatePercent,
                    TenLessonRatePercent = x.IndividualTeacherRates.FirstOrDefault() == null ? -1 : x.IndividualTeacherRates.FirstOrDefault().TenLessonRatePercent,
                    FifteenLessonRatePercent = x.IndividualTeacherRates.FirstOrDefault() == null ? -1 : x.IndividualTeacherRates.FirstOrDefault().FifteenLessonRatePercent,
                }).ToArrayAsync();
            }
        }
        [HttpPost]
        public async Task<bool> UpdateDay(IndividualTeacherRate rate)
        {
            using (var context = new MyDbContext())
            {
                var oldRate = await context.IndividualTeacherRates.FirstOrDefaultAsync(x => x.Id == rate.Id);
                oldRate.TrialPercent = rate.TrialPercent;
                oldRate.OneLessonRatePercent = rate.OneLessonRatePercent;
                oldRate.FiveLessonRatePercent = rate.FiveLessonRatePercent;
                oldRate.TenLessonRatePercent = rate.TenLessonRatePercent;
                oldRate.FifteenLessonRatePercent = rate.FifteenLessonRatePercent;
                await context.SaveChangesAsync();
                return true;
            }
        }
        [HttpPost]
        public bool UpdateCommonRate(CommonRate rate)
        {
            var str = JsonConvert.SerializeObject(rate);
            var mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/img");
            var fullPath = Path.Combine(mapPath, "rate.json");
            if (!File.Exists(fullPath))
            {
                var stream = File.Create(fullPath);
                stream.Dispose();
            }
            File.WriteAllText(fullPath, str);
            return true;

        }
        [Authorize]
        [HttpGet]
        public CommonRate GetCommonRate()
        {
            var mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/img");
            var fullPath = Path.Combine(mapPath, "rate.json");
            if (!File.Exists(fullPath)) {
                var stream = File.Create(fullPath);
                stream.Dispose();
                var str = JsonConvert.SerializeObject(new CommonRate());
                File.WriteAllText(fullPath,str);
                return new CommonRate();
            }
            var text = File.ReadAllText(fullPath);
            return JsonConvert.DeserializeObject<CommonRate>(text);

                
        }
    }
    public class CommonRate {
        public decimal Rate { get; set; } = 15;
    }
}
