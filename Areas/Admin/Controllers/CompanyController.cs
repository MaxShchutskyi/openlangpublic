﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Domain.Context;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;
using Domain.Repositories.Interfaces;
using Xencore.Areas.Admin.Models;
using Xencore.Controllers;
using Microsoft.AspNet.Identity.Owin;

namespace Xencore.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CompanyController : TemplateOpenlangController
    {
        private ICompanyRepository Repo { get; set; }

        public CompanyController(ICompanyRepository repo)
        {
            this.Repo = repo;
        }
        // GET: Admin/Company
        public async Task<ActionResult> Index()
        {
            await NewAutorizingLogic();
            var res = await Repo.GetAllCompanies();
            return View(res);
        }

        public async Task<ActionResult> GetDatails(Guid id)
        {
            await NewAutorizingLogic();
            var res = await Repo.GetCompanyById(id);
            return View(res);
        }
        public async Task<ActionResult> AddCompany()
        {
            await NewAutorizingLogic();
            return View(new PreSignInUser());
        }
        [HttpPost]
        public async Task<ActionResult> AddCompany(PreSignInUser user)
        {
            await NewAutorizingLogic();
            if (!ModelState.IsValid)
                return View();
            var manager = HttpContext.GetOwinContext().Get<CustomUserManager>();
            var result = Mapper.Map<CustomUser>(user);
            result.Ilearn = "cap_" + user.ChosenLang + "|";
            await manager.CreateAsync(result, user.Password);
            await manager.AddToRoleAsync(result.Id, "Company");
            return RedirectToAction("Index");
        }
        [HttpPost]
        public async Task<ActionResult> BindTo(Guid teacherId, string studentEmail)
        {
            var res = await Repo.BindTo(teacherId, studentEmail);
            return Json(res);
        }

        [HttpPost]
        public async Task<ActionResult> UnbindTo(Guid teacherId, string studentEmail)
        {
            var res = await Repo.UnbindTo(teacherId, studentEmail);
            return Json(res);
        }

        protected override void Dispose(bool disposing)
        {
            Repo.Dispose();
            base.Dispose(disposing);
        }
    }
}