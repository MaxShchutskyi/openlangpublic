﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using PDFmanager;
using Repositories.Abstract;
using WebGrease.Css.Extensions;

namespace Xencore.Areas.Admin.Controllers
{
    public class UploadPDFController : Controller
    {
        private IBrainCardRepo Repo { get; set; }

        public UploadPDFController(IBrainCardRepo repo)
        {
            Repo = repo;
        }
        [HttpPost]
        // GET: Admin/UploadPDF
        public async Task<JsonResult> UploadPDFLesson(int lessonId)
        {
            if (Request.Files.Count <= 0) return Json(new { path = "" });
            var files = Request.Files;
            if (!Directory.Exists(Server.MapPath($"~/Images/PdfFiles/{lessonId}")))
                Directory.CreateDirectory(Server.MapPath($"~/Images/PdfFiles/{lessonId}"));
            var info = new DirectoryInfo(Server.MapPath($"~/Images/PdfFiles/{lessonId}")).GetFiles();
            info.ForEach(x => x.Delete());
            var serverpath = Server.MapPath($"~/Images/PdfFiles/{lessonId}");
            var directory = $"/Images/PdfFiles/{lessonId}/";
            var pic = files[0];
            var newImages = PDFEngine.Save(pic.InputStream, serverpath);
            newImages = newImages.Select(x => Path.Combine(directory, x)).ToArray();
            await Repo.ClearSlides(lessonId);
            foreach (var image in newImages) {
                await Repo.AddNewCard(new DTO.BrainCardDto() { Title = "Not_Action_Card", LessonId = lessonId, UploadDate = DateTime.Now, ImageSlideSrc = image, Type = DTO.Enums.TypeOfCard.NotAction });
            }
            return Json(newImages);
        }

        [HttpPost]
        // GET: Admin/UploadPDF
        public async Task<JsonResult> UploadLessonIcon(int lessonId)
        {
            if (Request.Files.Count <= 0) return Json(new { path = "" });
            var files = Request.Files;
            var name = Guid.NewGuid() + ".jpg";
            if (!Directory.Exists(Server.MapPath($"~/Images/Icons/")))
                Directory.CreateDirectory(Server.MapPath($"~/Images/Icons/"));
            var serverpath = Server.MapPath($"~/Images/Icons/{name}");
            var directory = $"/Images/Icons/{name}";
            var pic = files[0];
            pic.SaveAs(serverpath);
            var oldPath = await Repo.UpdateIcon(lessonId, directory);
            if (oldPath != null)
                System.IO.File.Delete(Server.MapPath($"~/Images/Icons/") + oldPath.Split('/').Last());
            return Json(directory);
        }
        protected override void Dispose(bool disposing)
        {
            Repo?.Dispose();
            base.Dispose(disposing);
        }
    }
}