﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Services;
using Domain.Context;
using Domain.Entities.IdentityEntities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
//using MvcApplicationPostFinanceLatest.Entity;
using MvcApplicationPostFinanceLatest.Helpers;
using MvcApplicationPostFinanceLatest.Models;
using MvcApplicationPostFinanceLatest.Repository;
using MvcApplicationPostFinanceLatest.ViewModel;

using Xencore.Controllers;
using Xencore.Entities.IdentityEntities;
using Xencore.Entities.IdentityEntities.Entities;


namespace Xencore.Areas.Admin.Controllers
{
    //[AllowAnonymous]
    [Authorize(Roles = "Presenter")]
    public class AttendeeController : TemplateOpenlangController
    {
        private AttendeeRepository attendeeRepository;
        private string result = "";
        public ActionResult Index()
        {
            return View();
        }

        public AttendeeController()
        {
            ViewBag.Admin = "attendee";
        }
        [HttpGet]
        public ViewResult Add()
        {
            //levelRepository = new LevelRepository();
            //languageRepository = new LanguageRepository();
            //var model = new AttendeeView()
            //{
            //    Levels = levelRepository.GetLevelSelectListItems(),
            //    Languages = languageRepository.GetLanguageSelectListItems()
            //};
            return View(new CustomUser());
        }
        [HttpPost]
        public async Task<ViewResult> Add(CustomUser attendeeView)
        {
            try
            {
                var pass = attendeeView.PasswordHash;
                attendeeView.PasswordHash = null;
                attendeeView.CreationDateTime = DateTime.Now;
                attendeeView.UserName = attendeeView.Email;
                var result2 = await HttpContext.GetOwinContext().Get<CustomUserManager>().CreateAsync(attendeeView, pass);
                if(!result2.Succeeded)
                    throw new Exception("Error to add new Attendee");
                //attendeeRepository.Add(attendee);
                result = "Success";
            }
            catch (Exception exception)
            {
                result = exception.ToString();
            }

            return View("StringResponse", null, result);
        }
        [HttpPost]
        public async Task<ViewResult> Delete(Guid attendeeId)
        {
            attendeeRepository = new AttendeeRepository();
            try
            {
                var manag = HttpContext.GetOwinContext().Get<CustomUserManager>();
                var user = manag.FindById(attendeeId);
                user.Active = false;
                await manag.UpdateAsync(user);
                result = "Success";
                //attendeeRepository.Delete(id);
            }
            catch (Exception)
            {
                result = "You can't delete a user because he has a relates with other tables";
                throw;
            }

            return View("StringResponse", null, result);
        }
        [HttpGet]
        public ViewResult Edit(Guid id)
        {
            //levelRepository = new LevelRepository();
            //languageRepository = new LanguageRepository();
            attendeeRepository = new AttendeeRepository();
            var attendee = attendeeRepository.GetAttendee(id);
            //var model = new AttendeeView()
            //{
            //    AttendeeId = id,
            //    FirstName = attendee.FirstName,
            //    LastName = attendee.LastName,
            //    Email = attendee.Email,
            //    //Password = attendee.Password,
            //    PhoneNumber = attendee.PhoneNumber,
            //    TimeZone = attendee.TimeZone,
            //    Skype = attendee.Skype,
            //    CreationDateTime = DateTime.Now,
            //    DateOfBirth = DateTime.Now,

            //};
            return View(attendee);
        }
        [HttpPost]
        public async Task<ActionResult>Edit(CustomUser attendeeView)
        {
            try
            {
                using (var ctx  = new MyDbContext())
                {
                    ctx.Users.AddOrUpdate(attendeeView);
                    await ctx.SaveChangesAsync();
                }
                //var mng = HttpContext.GetOwinContext().Get<CustomUserManager>();
                //await mng.UpdateAsync(attendeeView);
                //await HttpContext.GetOwinContext().Get<CustomUserManager>().(attendee, attendeeView.Password);
                //attendeeRepository.Add(attendee);
                result = "Success";
            }
            catch (Exception exception)
            {
                result = exception.ToString();
                throw;
            }
            return RedirectToAction("SeeDetails", new {id = attendeeView.Id});
        }

        [HttpGet]
        public ViewResult EditPassword(Guid id)
        {
            var manag = HttpContext.GetOwinContext().Get<CustomUserManager>();
            var user = manag.FindById(id);
            //attendeeRepository = new AttendeeRepository();
            //var attendee = attendeeRepository.GetAttendee(attendeeId);
            //attendee.Password = null;
            user.PasswordHash = "";
            return View(user);
        }

        [HttpPost]
        public async Task<ActionResult> EditPassword(CustomUser attendee)
        {
            try
            {
                attendeeRepository = new AttendeeRepository();
                await attendeeRepository.EditPassword(attendee, HttpContext.GetOwinContext().Get<CustomUserManager>());
                result = "Success";
            }

            catch (Exception exception)
            {
                result = exception.ToString();
            }
            return RedirectToAction("SeeDetails", new { id = attendee.Id });
        }

        [HttpGet]
        public async  Task<ViewResult> SeeAll()
        {
            await NewAutorizingLogic();
            attendeeRepository = new AttendeeRepository();
            return View(attendeeRepository.GetAttendees());
        }
        [NonAction]
        private async Task<IEnumerable<Middle>> GetStat(string chart)
        {
            using (var context = new MyDbContext())
            {
                var date = DateTime.UtcNow;
                if (chart == "monthly")
                {
                    return await context.Users.Where(x => !x.Roles.Any() && x.CreationDateTime.Year == date.Year).Select(x => new Middle() { Date = x.CreationDateTime, Id = x.Id }).OrderByDescending(x => x.Date).ToArrayAsync();
                }
                if (chart == "weekly" || chart == "daily")
                {
                    return await context.Users.Where(x => !x.Roles.Any() && x.CreationDateTime.Month == date.Month).Select(x => new Middle() { Date = x.CreationDateTime, Id = x.Id }).OrderByDescending(x => x.Date).ToArrayAsync();
                }
                if (chart == "yearly")
                {
                    return await context.Users.Where(x => !x.Roles.Any()).Select(x => new Middle() { Date = x.CreationDateTime, Id = x.Id }).OrderByDescending(x => x.Date).ToArrayAsync();
                }
                return null;
            }
        }
        [HttpGet]
        public async Task<JsonResult> GetStats(string chat)
        {
            var res = await GetStat(chat);
            var labels = new List<string>();
            var data = new List<decimal>();
            if (chat == "monthly")
            {
                var datas = res.GroupBy(x => x.Date.Month).Select(x => new { month = x.Key, amount = x.Count() }).ToArray();
                for (var i = 1; i < DateTime.UtcNow.Month + 1; i++)
                {
                    labels.Add(CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i));
                    data.Add(datas.FirstOrDefault(x => x.month == i)?.amount ?? 0);
                }
            }
            if (chat == "weekly")
            {
                var date = DateTime.UtcNow;
                var date1 = new DateTime(date.Year, date.Month, 1);
                var st = date1;
                var p = 1;
                while ((date1 = date1.AddDays(7)).Month == date.Month)
                {
                    labels.Add(p.ToString());
                    var t = res.Count(x => x.Date > st && x.Date < date1);
                    data.Add(t);
                    st = st.AddDays(7);
                    p++;
                }
            }
            if (chat == "daily")
            {
                var date = DateTime.UtcNow;
                var countDaysInMonth = DateTime.DaysInMonth(date.Year, date.Month);
                for (var i = 1; i <= countDaysInMonth; i++)
                {
                    labels.Add(i.ToString());
                    data.Add(
                        res.Count(x => new DateTime(x.Date.Year, x.Date.Month, x.Date.Day) == new DateTime(date.Year, date.Month, i)));
                }
            }
            if (chat == "yearly")
            {
                //var datas = res.GroupBy(x => x.CreationDate.Month).Select(x => new { month = x.Key, amount = x.Sum(f => f.Amount) }).ToArray();
                labels.AddRange(new[] { "2016", "2017", "2018" });
                labels.ForEach(x => { data.Add(res.Count(f => f.Date.Year == int.Parse(x))); });
            }
            return Json(new {labels, data}, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public async Task<ViewResult> SeeDetails(Guid id)
        {
            await NewAutorizingLogic();
            attendeeRepository = new AttendeeRepository();
            return View(await attendeeRepository.GetAttendeeAllInfo(id));
        }
        public ViewResult Search(string search, bool?delete)
        {
            attendeeRepository = new AttendeeRepository();
            var result = attendeeRepository.GetAttendees(search);
            if(delete == true)
                return View("DeleteMultyple", result);
            else
                return View("SeeAll",result);
        }
        [HttpGet]
        public ViewResult DeleteMultyple()
        {
            attendeeRepository = new AttendeeRepository();
            return View(attendeeRepository.GetAttendees());
        }
        [HttpPost]
        public RedirectToRouteResult DeleteMultyple(Guid[] ids)
        {
            attendeeRepository = new AttendeeRepository();
            attendeeRepository.DeleteMultiple(ids);
            return RedirectToAction("DeleteMultyple");

        }
        [HttpPost]
        [WebMethod]
        public async Task<JsonResult> GetCountsOf()
        {
            using (var context = new MyDbContext())
            {
                var attendees = await context.Users.Where(x=>x.Active).CountAsync();
                var teachers = await context.Roles.Where(x=>x.Name == "Presenter").Include(x => x.Users).SelectMany(x=>x.Users).CountAsync();
                var discounts = await context.Discounts.CountAsync();
                var companies = 0;
                return Json(new {attendees, teachers, discounts, companies});
            }
        }
    }
    class Middle
    {
        public DateTime Date { get; set; }
        public Guid Id { get; set; }
    }
}
