﻿using MvcApplicationPostFinanceLatest.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using MvcApplicationPostFinanceLatest.Helpers;

namespace Xencore.Areas.Admin.Controllers
{
    public class PFPaymentController : Controller
    {
        private WizHttpRequest oRequest;
        private XmlGetResponse xmlGet;
        private List<Data> responseList;

        private string AuthServer = @"https://e-payment.postfinance.ch/ncol/prod/orderdirect.asp?";
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ViewResult ProcessCard()
        {
            return View();
        }

        private string FormString(CardRequest card)
        {
            string seceret = "Mysecretsig1875!?";
            var str =  String.Format("ACCEPTURL={1}{0}AMOUNT={2}{0}BRAND={20}{0}CARDNO={3}{0}CN={21}{0}COM={22}{0}COMPLUS={4}{0}CURRENCY={5}{0}CVC={6}{0}DECLINEURL={7}{0}ED={8}{0}EMAIL={23}{0}EXCEPTIONURL={9}{0}FLAG3D={10}{0}HTTP_ACCEPT={11}{0}HTTP_USER_AGENT={12}{0}LANGUAGE={24}{0}OPERATION={13}{0}ORDERID={14}{0}PARAMPLUS={15}{0}PSPID={16}{0}PSWD={17}{0}USERID={18}{0}WIN3DS={19}{0}",
                seceret, card._ACCEPTURL, card._AMOUNT, card._CARDNO, card._COMPLUS, card._CURRENCY, card._CVC, card._DECLINEURL, card._ED, card._EXCEPTIONURL, card._FLAG3D, card._HTTP_ACCEPT, card._HTTP_USER_AGENT, card._OPERATION, card._ORDERID, card._PARAMPLUS, card._PSPID, card._PSWD, card._USERID, card._WIN3DS, card._BRAND, card._CN, card._COM, card._EMAIL, card._LANGUAGE);

            return str;
        }
        public byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA1.Create();  // SHA1.Create()
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public string Encrypt(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
        [HttpPost]
        public ViewResult ProcessCard(CardRequest card)
        {
            var requestParameters = new Dictionary<string, string>();
            requestParameters["PSPID"] = card._PSPID;
            requestParameters["ORDERID"] = card._ORDERID;
            requestParameters["USERID"] = card._USERID;
            requestParameters["PSWD"] = card._PSWD;
            requestParameters["AMOUNT"] = card._AMOUNT;
            requestParameters["CURRENCY"] = card._CURRENCY;
            requestParameters["CARDNO"] = card._CARDNO;
            requestParameters["ED"] = card._ED;
            requestParameters["CVC"] = card._CVC;
            requestParameters["OPERATION"] = card._OPERATION;
            requestParameters["BRAND"] = card._BRAND;
            requestParameters["LANGUAGE"] = card._LANGUAGE;
            requestParameters["CN"] = card._CN;
            requestParameters["EMAIL"] = card._EMAIL;
            requestParameters["COM"] = card._COM;
            requestParameters["FLAG3D"] = card._FLAG3D;
            requestParameters["HTTP_ACCEPT"] = card._HTTP_ACCEPT;
            requestParameters["HTTP_USER_AGENT"] = card._HTTP_USER_AGENT;
            requestParameters["WIN3DS"] = card._WIN3DS;
            requestParameters["ACCEPTURL"] = card._ACCEPTURL;
            requestParameters["DECLINEURL"] = card._DECLINEURL;
            requestParameters["EXCEPTIONURL"] = card._EXCEPTIONURL;
            requestParameters["PARAMPLUS"] = card._PARAMPLUS;
            requestParameters["COMPLUS"] = card._COMPLUS;
            requestParameters["SHASIGN"] = Encrypt(FormString(card));

            oRequest = new WizHttpRequest();
            xmlGet = new XmlGetResponse();
            responseList = xmlGet.Parse(oRequest.WiZiQWebRequest(AuthServer, requestParameters));
            return View("Response", responseList);
        }
    }
}
