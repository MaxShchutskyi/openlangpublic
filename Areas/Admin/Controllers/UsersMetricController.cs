﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Entities.HelpersEntity;
using Domain.Repositories.Interfaces;
using Xencore.Controllers;

namespace Xencore.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersMetricController : TemplateOpenlangController
    {
        private ICustomUserRepository Repo { get; set; }

        public UsersMetricController(ICustomUserRepository repo)
        {
            Repo = repo;
        }
        public async Task<ActionResult> Index()
        {
            await NewAutorizingLogic();
            var res = await Repo.GetAllUsersForMetric(DateTime.UtcNow.AddYears(-1), DateTime.UtcNow);
            var model = new UserMetricHelper();
            model.TotalCountUsers = res.Count();
            model.TotalCountTeachers = res.Count(x => x.TeacherInfo != null);
            model.TotalActiveTeachers = res.Count(x => x.TeacherInfo != null && x.Active);
            model.UsersUsedNewSystem = res.Count(x => x.LocalTime != null);
            model.TeachersUsedNewSystem = res.Count(x => x.TeacherInfo!=null && x.LocalTime != null);
            model.StudentWhichHasMoney = res.Count(x => x.Money > 0);
            model.SsersWhichHasOneTransaction = res.Count(x => x.PayPalPaymentTransactions.Any());
            model.StudentsForAllCompations = res.Count(x => x.CompanyId != null);
            model.StudentsFromEurope = res.Count(x => x.TimeZone!=null && x.TimeZone.Contains("Europe"));
            model.StudentsFromAsia = res.Count(x => x.TimeZone != null && x.TimeZone.Contains("Asia"));
            model.StudentsFromAmerica = res.Count(x => x.TimeZone != null && x.TimeZone.Contains("America"));
            model.StudentsUnknownTimezone =
                res.Count(
                    x =>
                        x.TimeZone == null ||
                        (!x.TimeZone.Contains("Europe") && !x.TimeZone.Contains("America") && !x.TimeZone.Contains("Asia")));
            model.Groups = res.GroupBy(x => x.Location).Select(x=> new UserMetricHelper.Item() {Key = x.Key, Count = x.Count()}).ToList();
            model.LocalTimeDiffirent = res.GroupBy(x => x.LocalTime?.ToString()).Select(x => new UserMetricHelper.Item() { Key = x.Key, Count = x.Count() }).ToList();
            ViewBag.Users = res;
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            Repo.Dispose();
            base.Dispose(disposing);
        }
    }
}