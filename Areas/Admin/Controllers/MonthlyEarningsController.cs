﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Helpers.Abstract;
using Domain.Repositories.Interfaces;
using Xencore.Controllers;

namespace Xencore.Areas.Admin.Controllers
{
    public class MonthlyEarningsController : TemplateOpenlangController
    {
        private ISchedulerRepository ScheduleRepo { get; }
        private ITeacherManager TeacherMaanger { get; }

        public MonthlyEarningsController(ISchedulerRepository repo, ITeacherManager manager)
        {
            ScheduleRepo = repo;
            TeacherMaanger = manager;
        }
        public async Task<ActionResult> GetMonthlyEarningsForAllTeachers(int month, int year)
        {
            await NewAutorizingLogic();
            var date = DateTime.UtcNow;
            var startDate = new DateTime(year, month, 25);
            if (date.Day < 25)
                startDate = startDate.AddMonths(-1);
            ViewBag.Date = new DateTime(year, month,1);
            //var endDate = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month)).AddDays(1);
            var endDate = date;
            var res = await ScheduleRepo.GetAllSchedulesForPeriod(startDate, endDate);
            var overal = TeacherMaanger.SetMonthlyEarnings(res);
            return View(overal);
        }

        protected override void Dispose(bool disposing)
        {
            ScheduleRepo.Dispose();
            base.Dispose(disposing);
        }
    }
}