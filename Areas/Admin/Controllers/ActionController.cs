﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories.Interfaces;
using Xencore.Controllers;

namespace Xencore.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ActionController : TemplateOpenlangController
    {
        private IActionReoisitory Repo { get; set; }

        public ActionController(IActionReoisitory repo)
        {
            Repo = repo;
        }
        // GET: Admin/Action
        public async Task<ActionResult> Index(string type)
        {
            await NewAutorizingLogic();
            if (string.IsNullOrEmpty(type))
            {
                var res = await Repo.GetActions(null, null);
                return View(res);
            }
            DateTime? startDate;
            DateTime endDate = DateTime.UtcNow;
            switch (type)
            {
                case "All": startDate = null; ViewBag.type = "All"; break;
                case "LastMonth": startDate = endDate.AddMonths(-1); ViewBag.type = "LastMonth"; break;
                case "Today": startDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 0, 0, 0); ViewBag.type = "Today"; break;
                case "LastWeek": startDate = endDate.AddDays(-7); ViewBag.type = "LastWeek"; break;
                case "LastYear": startDate = endDate.AddYears(-1); ViewBag.type = "LastYear"; break;
                default: startDate = null; ViewBag.type = ""; break;
            }
            var rest = await Repo.GetActions(startDate, endDate);
            ViewBag.type = type;
            return View(rest);
        }
    }
}