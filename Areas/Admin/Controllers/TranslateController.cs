﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Resources;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Services;
using WebGrease.Css.Extensions;
using Xencore.Areas.Admin.ViewModels;
using Xencore.Controllers;

namespace Xencore.Areas.Admin.Controllers
{
    [Authorize]
    public class TranslateController : TemplateOpenlangController
    {
        // GET: Admin/Translate
        public async Task<ActionResult> GetAllEnglishStrings(string prefix)
        {
            await NewAutorizingLogic();
            ViewBag.Prefix = prefix;
            var wrappwr = new Dictionary<string, string>();
            var wrpEng = new Dictionary<string, string>();
            using (
                var rsxr =
                    new ResXResourceReader(HttpContext.Server.MapPath($"~/App_GlobalResources/Resource.{prefix}.resx")))
            {
                foreach (DictionaryEntry res in rsxr)
                {
                    wrappwr.Add(res.Key.ToString(), res.Value.ToString());
                }
            }
            using (
                var rsxr = new ResXResourceReader(HttpContext.Server.MapPath($"~/App_GlobalResources/Resource.en.resx"))
            )
            {
                foreach (DictionaryEntry res in rsxr)
                {
                    wrpEng.Add(res.Key.ToString(), res.Value.ToString());
                }
            }
            var view = (from rs in wrappwr where wrpEng.ContainsKey(rs.Key) select new ResXWrapper() {EnglishValue = wrpEng[rs.Key], Key = rs.Key, Value = rs.Value}).ToList();
            // var view = wrappwr.Select(rs => new ResXWrapper() { EnglishValue = wrpEng[rs.Key], Key = rs.Key, Value = rs.Value }).ToList();
            return View(view);
            
        }
        [HttpPost]
        [WebMethod]
        public JsonResult UpdateString(IEnumerable<ResXWrapper> wrapper, string prefix)
        {
            if(wrapper==null || string.IsNullOrEmpty(prefix) || !wrapper.Any())
                return Json(new { success = true });
            var dic = new Dictionary<string, string>();
            using (var rsxr = new ResXResourceReader(HttpContext.Server.MapPath($"~/App_GlobalResources/Resource.{prefix}.resx")))
            {
                foreach (DictionaryEntry res in rsxr)
                {
                    dic.Add(res.Key.ToString(),res.Value.ToString());
                }
            }
            wrapper.ForEach(x => dic[x.Key] = x.Value);
            using (var rsxr = new ResXResourceWriter(HttpContext.Server.MapPath($"~/App_GlobalResources/Resource.{prefix}.resx")))
            {
                foreach (var vl in dic)
                {
                    rsxr.AddResource(vl.Key, vl.Value);
                }
                rsxr.Generate();
            }
            return Json(new {success = true});
        }
    }
}