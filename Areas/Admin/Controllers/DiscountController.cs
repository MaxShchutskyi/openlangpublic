﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Xencore.Models;
using Xencore.Repositories;

namespace Xencore.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DiscountController : Controller
    {
        // GET: Admin/Discount
        public async Task<ActionResult> GetDiscounts()
        {
            var repo = await new DiscountRepository().GetAllDiscounts();
            return View(new ModelsWithDiscountViewModels() {Discounts = repo});
        }
        public async Task<ActionResult> AddDiscount(DiscountViewModel model)
        {
            if (!ModelState.IsValid) return RedirectToAction("GetDiscounts");
            await new DiscountRepository().AddNewDiscount(model);
            return RedirectToAction("GetDiscounts");
        }
    }
}