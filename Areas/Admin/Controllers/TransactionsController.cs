﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Repositories.Interfaces;
using Xencore.Controllers;

namespace Xencore.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TransactionsController : TemplateOpenlangController
    {
        private IPaymentTransactions PaymentRepository { get; }

        public TransactionsController(IPaymentTransactions paymentRepo)
        {
            PaymentRepository = paymentRepo;
        }
        public async Task<ActionResult> GetAllTransactions()
        {
            await NewAutorizingLogic();
            var res = await PaymentRepository.GetAllTransactionsForAdmin();
            return View(res);
        }

        protected override void Dispose(bool disposing)
        {
            PaymentRepository.Dispose();
            base.Dispose(disposing);
        }
    }
}