﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xencore.Areas.Admin.ViewModels
{
    public class ResXWrapper
    {
        public string Key { get; set; }
        public string EnglishValue { get; set; }
        public string Value { get; set; }
    }
}