﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplicationPostFinanceLatest.Models
{
    public class DownloadRecordingRequestModel : WizRequestBase
    {
        public DownloadRecordingRequestModel()
            : base("download_recording")
        {
        }
        public int DownloadRecordingRequestModelID { get; set; }
        public string Method
        {
            get { return "download_recording"; }
        }
        [Required]
        public int ClassId { get; set; }

        public string RecordingFormat { get; set; }
    }
}