﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplicationPostFinanceLatest.Models
{
    public class AddAttendeesRequestModel : WizRequestBase
    {
        public AddAttendeesRequestModel()
            : base("add_attendees")
        {
        }
        public int AddAttendeesRequestModelID { get; set; }
        public string Method
        {
            get { return "add_attendees"; }
        }
        [Required]
        public int ClassId { get; set; }

        //public List<Attendee> AttendeeModels { get; set; }
    }
}