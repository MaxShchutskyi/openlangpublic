﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Domain.Entities;
using MvcApplicationPostFinanceLatest.Helpers;

namespace MvcApplicationPostFinanceLatest.Models
{
    public class TeacherBaseRequestModel
    {
        private readonly string _method;

        public TeacherBaseRequestModel(string _method)
        {
            this._method = _method;
        }
        public TeacherBaseRequestModel()
        {

        }
        private string timeStamp;
        private string accessKey;
        private Presenter presenters;
        public string ServiceRootUrl
        {
            get { return ConfigurationManager.AppSettings["ServiceRootUrl"]; }
        }
        public string SecretAccessKey
        {
            get
            {
                return ConfigurationManager.AppSettings["SeceretKey"]; ;
            }
        }
        public string AccessKey
        {
            get
            {
                accessKey = ConfigurationManager.AppSettings["AccessKey"];
                return accessKey;
            }
        }
        public string TimeStamp
        {
            get
            {
                timeStamp = AuthBase.GenerateTimeStamp();
                return timeStamp;
            }
        }
        public string Signature
        {
            get
            {
                AuthBase authBase = new AuthBase();
                var auth = new Dictionary<string, string>();
                auth["access_key"] = accessKey;
                auth["timestamp"] = timeStamp;
                auth["method"] = _method;
                return authBase.GenerateSignature(auth["access_key"], SecretAccessKey, auth["timestamp"], auth["method"]);
            }
        }
    }
}