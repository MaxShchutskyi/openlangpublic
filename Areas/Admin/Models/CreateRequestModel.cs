﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Web;
using System.Web.Mvc;
using MvcApplicationPostFinanceLatest.Attributes;

namespace MvcApplicationPostFinanceLatest.Models
{
    public class CreateRequestModel :  WizRequestBase
    {
        private readonly string _method;
        public CreateRequestModel()
            : base("create")
        { }

        public CreateRequestModel(string method)
            : base(method)
        {
            _method = method;
        }
        public int ClassId { get; set; }

        public int CreateRequestModelID { get; set; }
        public string Method
        {
            get { return "create"; }
        }
        [Required]
        public string StartTime { get; set; }
        [Required]
        public string Title { get; set; }
        public string TimeZone { get; set; }
        public string AttendeeLimit { get; set; }
        public string Duration { get; set; }
        public string PresenterDefaultControls { get; set; }
        public string AttendeeDefaultControls { get; set; }
        public bool CreateRecording { get; set; }
        public string ReturnUrl { get; set; }
        public string StatusPingUrl { get; set; }
        public string LanguageCultureName { get; set; }

        //
        public bool IsPermanent { get; set; }
        public bool IsRecurring { get; set; }
        public string Type { get; set; }    

        //
        public string SpecificTopic { get; set; }
        public string SubjectsStr { get; set; }

        public int? LevelId { get; set; }
        public IEnumerable<SelectListItem> Levels { get; set; }
        //public int? SubjectId { get; set; }
        public int? LanguageId { get; set; }
        public IEnumerable<SelectListItem> Languages { get; set; }
        public bool HasSeats { get; set; }
        public bool CanJoin { get; set; }


        //Details
        [AllowHtml]
        public string Description { get; set; }
        //[FileSize(1048576)]
        //[FileTypes("jpg,jpeg,png")]
        public HttpPostedFileBase Image { get; set; }
        //[FileSize(5242880)]
        //[FileTypes("pdf,doc,docx")]
        public HttpPostedFileBase Document { get; set; }
        public HttpPostedFileBase Audio { get; set; }
    }
}