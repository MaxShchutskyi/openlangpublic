﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplicationPostFinanceLatest.Models
{
    public class GetDataRequestModel : WizRequestBase
    {
        public GetDataRequestModel()
            : base("get_data")
        {
        }
        public int GetDataRequestModelID { get; set; }
        public string Method
        {
            get { return "get_data"; }
        }
        [Required]
        public int ClassId { get; set; }
        public List<int> MultipleClassId { get; set; }
    }
}