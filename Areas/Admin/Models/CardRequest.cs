﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplicationPostFinanceLatest.Models
{
    public class CardRequest
    {
        private string amount;

        public string _PSPID
        {
            get { return "newvisions"; }
        }
        public string _ORDERID
        {
            get { return Guid.NewGuid().ToString("N"); }//.Substring(0, 9)
        }
        public string _USERID
        {
            get { return "NPLUser"; }//APIUser
        }
        public string _PSWD
        {
            get { return "TL44d$77"; }
        }
        [Required]
        public string _AMOUNT { get; set; }
        //{
        //    get
        //    {
        //        amount = (Convert.ToInt32(_AMOUNT) * 100).ToString();//Math.Round(Convert.ToDecimal(_AMOUNT), 2).ToString().Replace(".", String.Empty);
        //        return amount;
        //    }
        //    set { amount = value; }
        //}
        [Required]
        public string _CURRENCY { get; set; }
        [Required]
        public string _CARDNO { get; set; }
        [Required]
        public string _ED { get; set; }
        [Required]
        public string _CVC { get; set; }

        public string _OPERATION 
        {
            get { return "SAL"; }
        }
        [Required]
        public string _BRAND { get; set; }
        public string _LANGUAGE
        {
            get { return "en_US"; }
        }
        public bool? IsSuccess { get; set; }
        public string Response { get; set; }

        //Additional
        public string _COM
        {
            get { return "Three telephone cards"; }
        }
        public string _CN { get; set; }
        public string _EMAIL { get; set; }

        // 3D secure

        public string _FLAG3D
        {
            get { return "Y"; }
        }
        public string _HTTP_ACCEPT
        {
            get { return "Accept: */*"; }
        }
        public string _HTTP_USER_AGENT
        {
            get { return "User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)"; }
        }
        public string _WIN3DS
        {
            get { return "POPUP"; }
        }
        public string _ACCEPTURL
        {
            get { return "www.google.com"; }
        }
        public string _DECLINEURL
        {
            get { return "www.yandex.ru"; }
        }
        public string _EXCEPTIONURL
        {
            get { return "www.mail.ru"; }
        }
        public string _PARAMPLUS
        {
            get { return "SessionID=1265483"; }
        }
        public string _COMPLUS
        {
            get { return "Three telephone cards"; }
        }
    }
}