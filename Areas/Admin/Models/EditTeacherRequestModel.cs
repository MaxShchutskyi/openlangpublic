﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplicationPostFinanceLatest.Models
{
    public class EditTeacherRequestModel : WizRequestBase
    {
        private string _method = "edit_teacher";
        public EditTeacherRequestModel()
            : base("edit_teacher")
        { }

        public EditTeacherRequestModel(string _method)
            : base(_method)
        {
            this._method = _method;
        }
        public int EditTeacherRequestModelID { get; set; }
        public string Method
        {
            get { return _method; }
            set { _method = value; }
        }

        public int PresenterId { get; set; }
        public string Name { get; set; }
        [Display(Name = "Email address")]
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [Required]
        [Display(Name = "Phone Number")]
        //[RegularExpression(@"((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}", ErrorMessage = "Invalid Phone Number!")]
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string TimeZone { get; set; }
        public string AboutTheTeacher { get; set; }
        public bool CanScheduleClass { get; set; }
        public bool IsActive { get; set; }
        public string PostFilePath { get; set; }
    }
}