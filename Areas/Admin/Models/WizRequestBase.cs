﻿using System.Collections.Generic;
using System.Configuration;
using MvcApplicationPostFinanceLatest.Helpers;

namespace MvcApplicationPostFinanceLatest.Models
{
    public class WizRequestBase
    {
        private readonly string _method;

        public WizRequestBase(string _method)
        {
            this._method = _method;
        }
        public WizRequestBase()
        {

        }
        private string timeStamp;
        private string accessKey;
        public string ServiceRootUrl
        {
            get { return ConfigurationManager.AppSettings["ServiceRootUrl"]; }
        }
        public string SecretAccessKey
        {
            get
            {
                return ConfigurationManager.AppSettings["SeceretKey"]; ;
            }
        }
        public string AccessKey
        {
            get
            {
                accessKey = ConfigurationManager.AppSettings["AccessKey"];
                return accessKey;
            }
        }
        public string TimeStamp
        {
            get
            {
                timeStamp = AuthBase.GenerateTimeStamp();
                return timeStamp;
            }
        }
        public string Signature
        {
            get
            {
                AuthBase authBase = new AuthBase();
                var auth = new Dictionary<string, string>();
                auth["access_key"] = accessKey;
                auth["timestamp"] = timeStamp;
                auth["method"] = _method;
                return authBase.GenerateSignature(SecretAccessKey, auth);
            }
        }
    }
}