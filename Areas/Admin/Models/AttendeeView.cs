﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RazorEngine;

namespace MvcApplicationPostFinanceLatest.Models
{
    public class AttendeeView
    {
        public Guid AttendeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public string TimeZone { get; set; }
        public string Skype { get; set; }
        public string FindUs { get; set; }
        public IEnumerable<SelectListItem> Languages { get; set; }
        public int? LanguageId { get; set; }
        public Language Language { get; set; }
        public IEnumerable<SelectListItem> Levels { get; set; }
        public int? LevelId { get; set; }
        public DateTime CreationDateTime { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}