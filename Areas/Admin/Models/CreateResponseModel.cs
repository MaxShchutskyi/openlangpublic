﻿using System.Collections.Generic;
using Domain.Entities;

namespace MvcApplicationPostFinanceLatest.Models
{
    public class CreateResponseModel
    {
        public int CreateResponseModelId { get; set; }
        public string Status { get; set; }
        public string CallId { get; set; }
        public string Method { get; set; }
        public string MethodStatus { get; set; }
        public string ClassId { get; set; }
        public string RecordingUrl { get; set; }
        public int? PresenterModelId { get; set; }
        public string PresenterUrl { get; set; }
        public string CoPresenterUrl { get; set; }
        public Presenter Presenters { get; set; }//public List<Presenter> Presenters { get; set; }
    }
}