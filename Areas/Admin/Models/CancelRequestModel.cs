﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplicationPostFinanceLatest.Models
{
    public class CancelRequestModel : WizRequestBase
    {
        public CancelRequestModel()
            : base("cancel")
        {
        }
        public int CancelRequestModelID { get; set; }
        public string Method
        {
            get { return "cancel"; }
        }
        [Required]
        public int ClassId { get; set; }
    }
}