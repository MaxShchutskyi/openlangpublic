﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xencore.Areas.Admin.Models
{
    public class BindToModel
    {
        public Guid TeacherId { get; set; }
        public Guid StudentId { get; set; }
    }
}