﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Xencore.Filters
{
    public class CultureAttribute : FilterAttribute, IActionFilter
    {
        public bool DisableAction { get; set; } = false;
        public bool IsCookieOnly { get; set; } = false;

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {

        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var langs = new[] {"en", "fr", "de", "es"};
            if(DisableAction) return;
            if (IsCookieOnly)
            {
                var cc = filterContext.HttpContext.Request.Cookies["lang"];
                var cn = cc != null ? cc.Value : "en";
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cn);
                Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(cn);
                return;

            }
            var clt = filterContext.RouteData.Values["culture"];
            if (clt == null || clt.ToString() == "ey")
            {
                if (filterContext.HttpContext.Request.UserLanguages == null)
                {
                    filterContext.Result = new RedirectResult("/en" + filterContext.HttpContext.Request.Path);
                    return;
                }
                var usereLang = filterContext.HttpContext.Request.UserLanguages[0].Split('-')[0];
                if (langs.All(x => x != usereLang.ToString()))
                    filterContext.Result = new RedirectResult("/en" + filterContext.HttpContext.Request.Path);
                filterContext.Result = new RedirectResult($"/{usereLang}" + filterContext.HttpContext.Request.Path);
                return;
            }
            if (langs.All(x => x != clt.ToString()))
            {
                filterContext.RouteData.Values["culture"] = "en";
                var url = new UrlHelper(filterContext.RequestContext);
                var newUrl = url.RouteUrl(filterContext.RouteData.Values);
                filterContext.Result = new RedirectResult(newUrl);
            }
            var cultureCookie = filterContext.HttpContext.Request.Cookies["lang"];
            if(cultureCookie == null || cultureCookie.Value != clt.ToString())
                filterContext.HttpContext.Response.Cookies.Add(new HttpCookie("lang", clt.ToString()) { Path = @"/", Expires = DateTime.UtcNow.AddDays(30)});
            //if (!IsCookieOnly)
            //{
            //    var culture = filterContext.RouteData.Values["culture"];
            //    if (culture == null)
            //        filterContext.Result = new RedirectResult("/" + cultureName + filterContext.HttpContext.Request.Path);
            //    else if (culture.ToString() != cultureName)
            //    {
            //        var arr = filterContext.HttpContext.Request.Path.Split('/');
            //        arr[1] = cultureName;
            //        filterContext.Result = new RedirectResult(string.Join("/", arr));
            //    }
            //}
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(clt.ToString());
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(clt.ToString());
        }
    }
}
