﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Domain.Entities.IdentityEntities;
using Microsoft.AspNet.Identity;
using Xencore.Entities.IdentityEntities;

namespace Xencore.Filters
{
    public class SetUserCookieAttribute: ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                filterContext.HttpContext.Request.Cookies.Remove("User");
                filterContext.HttpContext.Request.Cookies.Remove("IsChanged");
                return;
            }
            var userCookie = filterContext.HttpContext.Request.Cookies["User"];
            var changed = filterContext.HttpContext.Request.Cookies["IsChanged"];
            if (userCookie != null && changed==null) return;
            var user =
                HttpContext.Current.GetOwinContext()
                    .Get<CustomUserManager>()
                    .FindById(Guid.Parse(HttpContext.Current.User.Identity.GetUserId()));
        }
    }
}