﻿
using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CurrencyConverter;
using Domain.Entities.HelpersEntity;
using GeolocationManager.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Xencore.Extentions;
using Xencore.ViewModel;
using NLog;
using Prices.Prices;
using WebGrease.Css.Extensions;

namespace Xencore.Filters
{
    public class GlobalUserDetect : ActionFilterAttribute
    {
        public bool WithTablePrices { get; set; }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            var usr = filterContext.HttpContext.Request.Cookies["Usr"];
            if (usr == null)
            {
                var host = HttpContext.Current.Request.Url.Host.ToLower();
                var userIp = host == "localhost" ? "178.252.127.220" : filterContext.HttpContext.Request.UserHostAddress;
                //var userIp = "77.73.241.154";
                var user = GeolocationManager.GeolocationManager.Create(userIp).GetData();
                var str = JsonConvert.SerializeObject(user);
                filterContext.HttpContext.Response.Cookies.Add(new HttpCookie("Usr", str) { Path = "/", Expires = DateTime.UtcNow.AddDays(30)});
                filterContext.ActionParameters["Usr"] = user;
            }
            else
            {
                var user = JsonConvert.DeserializeObject<UserInfo>(usr.Value);
                filterContext.ActionParameters["Usr"] = user;
            }
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            var currentUser = new CurrentUser();
            var user = JsonConvert.DeserializeObject<UserInfo>(filterContext.HttpContext.Request.Cookies["Usr"].Value);
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                currentUser.Location = filterContext.HttpContext.User.Location();
                currentUser.CountryCode = filterContext.HttpContext.User.Location().Contains(":") ? filterContext.HttpContext.User.Location().Split(':')[1] : filterContext.HttpContext.User.Location();
                currentUser.Id = Guid.Parse(filterContext.HttpContext.User.Identity.GetUserId());
                currentUser.ProfilePicture = filterContext.HttpContext.User.ProfilePicture();
                currentUser.Timezone = filterContext.HttpContext.User.Timezone();
                currentUser.Name = filterContext.HttpContext.User.UserName();
                currentUser.Currency = filterContext.HttpContext.User.Currency();
            }
            else
            {
                currentUser.Location = user.Country + ":" + user.CountryCode;
                currentUser.Currency = user.CurrentCurency;
                currentUser.CountryCode = user.CountryCode;
                currentUser.Timezone = user.Timezone;
            }
            if (WithTablePrices)
            {
                var currency = filterContext.HttpContext.Request.QueryString["currency"]??currentUser.Currency.Split(':')[0];
                var prices =
                    PricesManager.CreatePrices(user.CountryCode, user.Timezone)
                        .GetAllPricesWithConvert(new CurrencyConverter<string>(), currency);
                filterContext.Controller.ViewBag.Prices = prices;
                //filterContext.Controller.ViewBag.Prices = JsonConvert.DeserializeObject(prices);
            }
                //currentUser.InitializePrices(user.Country);
            filterContext.Controller.ViewBag.CurrentUser = currentUser.ToString();
            filterContext.Controller.ViewBag.ObjectCurrentUser = currentUser;
            if (filterContext.HttpContext.Request.QueryString["currency"] != null)
                filterContext.Controller.ViewBag.ObjectCurrentUser.Currency =
                    filterContext.HttpContext.Request.QueryString["currency"] + ":CAP";
        }
    }
}