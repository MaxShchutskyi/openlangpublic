﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Entities.HelpersEntity;
using Newtonsoft.Json;

namespace Xencore.Filters
{
    public class TimezoneAttribute: ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            var timezone = filterContext.HttpContext.Request.Cookies["timezone"];
            if (timezone == null)
                filterContext.ActionParameters["timezone"] = short.Parse("0");
            else
                filterContext.ActionParameters["timezone"] = short.Parse(timezone.Value);
        }
    }
}