$('.circle-succes').circleProgress({
    value: 0.75,
    size: 120,
    lineCap: "round",
    startAngle: Math.PI + 1.5708,
    emptyFill: "#f6f5f5",
    fill: { color: "rgba(154, 211, 65, 0.85)" }
  });