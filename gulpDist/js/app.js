

var	app = angular.module('test-cards', []);

app.controller("TabCtrl", function($scope){
	$scope.tab = 1;
	$scope.check = 1;
	$scope.pass = 0;



	var chosen = $('.content-nav-main').children();
	
	$scope.isSet = function(checkTab){
		return $scope.tab === checkTab;
	};

	$scope.setTab = function() {
		$scope.tab = $scope.tab + 1;
		$scope.check = 1;
		setTimeout(function() { hideCheck() }, 200);
	};

	
	
	$scope.isCheck = function(checkCheck){

		return $scope.check === checkCheck;

	};

	$scope.setCheck = function(activeCheck){
		$scope.check = activeCheck;
		if ($scope.pass == chosen.length) {
			setTimeout(function() {
				$.magnificPopup.open({
					items: {
						src: '.white-popup'
					},
					type: 'inline',
					closeOnBgClick: false,
					closeBtnInside: false,
					showCloseBtn: false
				}, 0);
			}, 500);
			
		}
	};

	
	$( "body" ).keypress(function(e) {
		if (e.keyCode == 13) {
			event.preventDefault();
			if ($scope.check === 1) {
				$('.words-check').trigger('click');
			} else {
				$('.nextVideo').trigger('click');
			};

		}
	});

	$scope.testCheck = function(){
		if (!$('choice-dir').hasClass('ng-hide')) {
			$scope.choiceCheck();
		} else if(!$('video-dir').hasClass('ng-hide')) {
			$scope.videoCheck();
		} else if(!$('pictures-dir').hasClass('ng-hide')) {
			$scope.picturesCheck();
		} else if(!$('words-dir').hasClass('ng-hide')) {
			$scope.wordsCheck();
		} else if(!$('sentence-dir').hasClass('ng-hide')) {
			$scope.sentenceCheck();
		}

		
	};

	$scope.isSkip = function(){
		$scope.pass += 1;
		$scope.setCheck(3);
	};
});

var hideCheck = function(){

	$( document ).ready(function() {  
		if (!($('memory-dir').hasClass('ng-hide')) || !($('voice-dir').hasClass('ng-hide'))){
			console.log("perviy");
			$('#words-check').hide();
			$('.button_skip').show();

		} else {
			$('#words-check').show();
			$('.button_skip').hide();
			console.log("vtoroy");
		}

	});
} ;


///////////////////        CHOICE      ////////////////////////////////////////

app.directive("choiceDir", function(){
	return{
		restrict: "E",
        templateUrl: "/gulpDist/choice-model.html",
		
		controller: function($scope){

			$scope.choiceCheck = function(){

				$scope.rightAnswer = "Wrong";	
				var inp = document.getElementsByName('paragrath');
				for (var i = 0; i < inp.length; i++) {
					if (inp[i].type == "radio" && inp[i].checked) {

						if (inp[i].value === "a") {
							$('#correct').css('background-color', '#87d500');
							$('#correct').css('border-color', '#87d500');
							$scope.pass += 1;
							$scope.setCheck(2);

						} else {

							$('#correct').css('background-color', '#87d500');
							$('#correct').css('border-color', '#87d500');
							$('#correct').css('color', 'white');
							$('input.checked').css('color', '#87d500');
							$scope.pass += 1;
							$scope.setCheck(3);
							
						};
					};
				};

			};

		},
		controllerAs: "choice"
	};
});

///////////////////        VIDEO      ////////////////////////////////////////


app.directive("videoDir", function(){
	return{
		restrict: "E",
        templateUrl: "/gulpDist/video-model.html",
		controller: function($scope){
			$(document).ready(function () {
				$('textarea').focus(function(){
					$(this).data('placeholder',$(this).attr('placeholder'))
					$(this).attr('placeholder','');
				});
				$('input,textarea').blur(function(){
					$(this).attr('placeholder',$(this).data('placeholder'));
				});
			});
			$scope.videoCheck = function(){
				$scope.rightAnswer = "Max";
				var car = $("#carquestion").val();
				if (car.length > 0) {
					if (car === "max") {
						$scope.pass += 1;
						this.setCheck(2);
						
					} else {
						$scope.pass += 1;
						this.setCheck(3);
						
						
					}
				};
			};

		},
		controllerAs: "video"
	};
});

///////////////////       PICTURES     ////////////////////////////////////////

app.directive("picturesDir", function(){
	return{
		restrict: "E",
        templateUrl: "/gulpDist/pictures-model.html",
		controller: function($scope){
			
			$scope.picturesCheck = function(){
				$scope.rightAnswer = "Wrong";
				var pic = document.getElementsByName('pictures');
				for (var i = 0; i < pic.length; i++) {
					if (pic[i].type == "radio" && pic[i].checked) {

						if (pic[i].value === "2") {
							$scope.pass += 1;
							this.setCheck(2);
							$('#correctPic').css('background-color', '#87d500');
							$('#correctPic').css('border-color', '#87d500');
							$('#correctPic').css('color', 'white');
							$('#correctPic').parent().css('background-color', '#f0f7e8');
							
							
						} else {
							$scope.pass += 1;
							
							this.setCheck(3);
							$('#correctPic').css('background-color', '#87d500');
							$('#correctPic').css('border-color', '#87d500');
							$('#correctPic').css('color', 'white');
							$('#correctPic').parent().css('background-color', '#f0f7e8');
							
						}
					};
				};
			};
		},
		controllerAs: "pictures"
	};
});


///////////////////        WORDS      ////////////////////////////////////////


app.directive("wordsDir", function(){
	return{
		restrict: "E",
        templateUrl: "/gulpDist/words-model.html",
		controller: function($scope){
			this.hello = "hello world";
			$('.answer-nav li input').click(function(){
				$('.answer-nav li').css('background', 'white');
				$('.answer-nav li').css('color', '#009de4');
				$('.answer-nav li').css('border-color', '#009de4');
				$(this).parent().css('background', '#eef2f5');
				$(this).parent().css('color', '#eef2f5');
				$(this).parent().css('border-color', '#ddd');
				$('#task-word').css('background-color', '#009de4');
				$('#task-word').css('color', 'white');

				var wrd = $(this).val();
				$('#task-word').html(wrd);
			});
			$scope.wordsCheck = function(){
				$scope.rightAnswer = "Ich spreche kein Deutsch.";
				var wor = document.getElementsByName('words');

				for (var i = 0; i < wor.length; i++) {
					if (wor[i].type == "radio" && wor[i].checked) {

						if (wor[i].value === "Deutsch") {
							$scope.pass += 1;
							this.setCheck(2);
							
							
						} else {
							$scope.pass += 1;
							this.setCheck(3);
							
							
						}

					};
				};
			};
		},
		controllerAs: "words"
	};
});


///////////////////        SENTENCE      ////////////////////////////////////////


app.directive("sentenceDir", function(){
	return{
		restrict: "E",
        templateUrl: "/gulpDist/sentence-model.html",
		controller: function($scope){
			this.hello = "hello world";

			var sentence = [];
			var answer = '';
			$('.sentence-nav li').click(function(){
				var lett = $(this).html();
				sentence.push(lett);
				answer = sentence.join(' ');

				$('#sentence-word1').html(sentence[0]);
				$('#sentence-word2').html(sentence[1]);
				$('#sentence-word3').html(sentence[2]);
				$('#sentence-word4').html(sentence[3]);
			});	
			$scope.sentenceCheck = function(){
				
				
				if (sentence.length == 4) {
					$scope.rightAnswer = "Lass uns Schach spielen?";
					if (answer === "Lass uns Schach spielen?") {

						$('#sentence-word1').css('background', '#87d500');
						$('#sentence-word2').css('background', '#87d500');
						$('#sentence-word3').css('background', '#87d500');
						$('#sentence-word4').css('background', '#87d500');
						$scope.pass += 1;
						this.setCheck(2);
						
						
					}  else {
						$scope.pass += 1;
						this.setCheck(3);
						$('#sentence-word1').css('background', '#ee5a60');
						$('#sentence-word2').css('background', '#ee5a60');
						$('#sentence-word3').css('background', '#ee5a60');
						$('#sentence-word4').css('background', '#ee5a60');

						answer = '';
					};
				} else {
					sentence = [];
					$('#sentence-word1').html('');
					$('#sentence-word2').html('');
					$('#sentence-word3').html('');
					$('#sentence-word4').html('');
				}
			};
		},

		controllerAs: "sentence"
	};
});




///////////////////        MEMORY      ////////////////////////////////////////


app.directive("memoryDir", function(){
	return{
		restrict: "E",
        templateUrl: "/gulpDist/memory-games-model.html",
		controller: function($scope){
			this.hello = "hello world";
			
			

			
			function remotePic(){
				$(".opened").parent().parent().css('transform', 'rotateY(360deg)');
				$(".memory-descr").removeClass('opened');
			};

			$( document ).ready(function() {  

				var valuesOpened = [];
				var allright = [];
				if ($(this).hasClass('rightOpened')) {
				} else {
					$('.memory-nav li').on("click", function(){
						$(this).find(".flipper").css('transform', 'rotateY(180deg)');
						if ($(this).hasClass('rightOpened')) {
						} else {
							$(this).find(".memory-descr").addClass('opened');
						}

						valuesOpened = document.getElementsByClassName("opened");
						if (valuesOpened.length == 2) {
							var x1 = valuesOpened[0].innerHTML;
							var x2 = valuesOpened[1].innerHTML;

							if (x1 === x2) {
								$(".opened").parent().parent().parent().parent().addClass('rightOpened');
								$(".opened").parent().parent().parent().parent().addClass('first');
								$(".opened").parent().parent().parent().addClass('second');
								$(".opened").parent().parent().parent().css("cursor", "default");
								var opac = $(".opened").parent().parent().parent();
								$(".memory-descr").removeClass('opened');
								setTimeout(function(){opac.css("opacity", "0.7");

							}, 700); 


							} else {

								setTimeout(remotePic, 500);
							}

							valuesOpened = [];

						}

						$scope.memoryCheck = function(){

							allright = document.getElementsByClassName("rightOpened");
							if (allright.length === 8) {
								console.log(allright.length);
								$scope.pass += 1;
								this.setCheck(2);
								
								
								$('#words-check').trigger('click');
							}
						};
						$scope.memoryCheck();
					});

				}


			});
			
		},
		controllerAs: "memory"
	};
});

///////////////////        VOICE      ////////////////////////////////////////


app.directive("voiceDir", function(){
	return{
		restrict: "E",
        templateUrl: "/gulpDist/voice-model.html",
		controller: function($scope){
			this.hello = "hello world";


			function startRecognizer(){

				if ('webkitSpeechRecognition' in window) {
					var recognition = new webkitSpeechRecognition();
					recognition.lang = 'en';

					recognition.onresult = function (event) {
						console.log("hello");
						var result = event.results[event.resultIndex];
						console.clear();
						console.log(result[0].transcript);
						var rs = result[0].transcript;
						$('#voice-word').html(rs.split(' ')[0]);
						$('.circle').hide();
						$('.circle2').hide();
						if (rs.split(' ')[0] == 'Spanish') {
							$scope.voiceCheck = function(){
								$scope.pass += 1;
								
								this.setCheck(2);
								
								$('#words-check').trigger('click');
							};
							$scope.voiceCheck();
							$("#voice-word").css("background-color","#99d332");

						}
						else {

							$scope.voiceCheck = function(){
								$scope.rightAnswer = "I don't speak Spanish";
								$scope.pass += 1;
								
								this.setCheck(3);
								$('#words-check').trigger('click');
							};
							$scope.voiceCheck();

							$("#voice-word").css("background-color","#ee5a60");
							
						}

					};
					recognition.start();
				} else {alert('webkitSpeechRecognition is not supported in your browser :(');
				$('#nextVideo').show();
			};
		}
		$('#voice-record').click(function(){
			$('.circle').show();
			$('.circle2').show();
			startRecognizer();
		});

	},
	controllerAs: "voice"
};
});

/////////////////////////      MODAL    //////////////////////////////////

