$( document ).ready(function() {  
  $('.circle-level').circleProgress({
    value: 0.75,
    size: 115,
    lineCap: "round",
    startAngle: Math.PI,
    emptyFill: "#f6f5f5",
    fill: { color: "rgba(154, 211, 65, 0.85)" }
  });

  $('.circle-vocabulary').circleProgress({
    value: 0.75,
    size: 86,
    lineCap: "butt",
    startAngle: Math.PI ,
    emptyFill: "#f6f5f5",
    fill: { color: "rgba(154, 211, 65, 0.85)" }
  });
  $('.circle-grammar').circleProgress({
    value: 0.25,
    size: 86,
    lineCap: "butt",
    startAngle: Math.PI,
    emptyFill: "#f6f5f5",
    fill: { color: "rgba(154, 211, 65, 0.85)" }
  });
  $('.circle-reading').circleProgress({
    value: 0,
    size: 86,
    lineCap: "butt",
    startAngle: Math.PI,
    emptyFill: "#f6f5f5",
    fill: { color: "rgba(154, 211, 65, 0.85)" }
  });
  $('.circle-idioms').circleProgress({
    value: 0,
    size: 86,
    lineCap: "butt",
    startAngle: Math.PI,
    emptyFill: "#f6f5f5",
    fill: { color: "rgba(154, 211, 65, 0.85)" }
  });
  $('.circle-conversation').circleProgress({
    value: 0,
    size: 86,
    lineCap: "butt",
    startAngle: Math.PI,
    emptyFill: "#f6f5f5",
    fill: { color: "rgba(154, 211, 65, 0.85)" }
  });






  if ($('.circle-grammar').circleProgress('value') < 0.75) {
    $('.circle-grammar').circleProgress({ fill: { color: "rgba(0, 161, 234)" } });
  }

  });