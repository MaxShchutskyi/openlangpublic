﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Xencore.Entities;
using Xencore.Filters;
using Xencore.Models;
using Xencore.Repositories;

namespace Xencore.Controllers
{
    [Authorize]
    [Culture]
    public class TeacherDashboardController : TemplateOpenlangController
    {
        public async Task<ActionResult> Index()
        {
            await NewAutorizingLogic();
            var res = await new TeacherClassesRepository().GetAllClasses(Guid.Parse(HttpContext.User.Identity.GetUserId()));
            return View(res);
        }
        [Culture(DisableAction = true)]
        [HttpPost]
        public async Task<JsonResult> AddClass(TeacherClassesUi tc)
        {
            if (!ModelState.IsValid)
                return Json(new {success = false, errors = "Please fill all fields correct"});
            tc.CalculateEarnings();
            await new TeacherClassesRepository().AddNewClass(tc);
            return Json(new {success = true});
        }
    }
}