﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Xencore.Filters;

namespace Xencore.Controllers
{
    [Culture]
    [Authorize(Roles = "Presenter")]
    public class ReportsController : TemplateOpenlangController
    {
        public async Task<ActionResult> Index()
        {
            await NewAutorizingLogic();
            return View();
        }
    }
}