﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Xencore.Filters;

namespace Xencore.Controllers
{
    [Compress]
    [Culture]
    [GlobalUserDetect]
    public class PrivacyAndTermsController : TemplateOpenlangController
    {
        public async Task<ActionResult> Privacy()
        {
            await NewAutorizingLogic();
            return View();
        }
        public async Task<ActionResult> Terms()
        {
            await NewAutorizingLogic();
            return View();
        }
    }
}