﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using Domain.Context;
using Domain.Entities;
using Microsoft.AspNet.Identity;
using Xencore.Filters;

namespace Xencore.Controllers
{
    [Culture]
    [Authorize]
    //[RequireHttps]
    public class ChatController : TemplateOpenlangController
    {
        [HttpGet]
        public async Task<ActionResult> Chat()
        {
            await NewAutorizingLogic();
            ViewBag.TeacherId = TempData["TeacherId"];
            var id = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var res =
                    context.Dialogs.Where(x => x.UserOneId == id || x.UserTwoId == id).Select(x => new
                        {
                            CreateDate = x.CreateDate,
                            Replies = x.Replies.OrderByDescending(f => f.DateTime).Take(10).ToList(),
                            Id = x.Id,
                            UserOne = x.UserOne,
                            UserTwo = x.UserTwo,
                            UserOneId = x.UserOneId,
                            UserTwoId = x.UserTwoId,
                            Unread = x.Replies.Where(f => f.Unread && f.UserId != id).OrderBy(f => f.DateTime).ToList()
                        }).AsEnumerable().Select(x => new Dialog()
                        {
                            CreateDate = x.CreateDate,
                            Replies = x.Replies.OrderBy(f => f.DateTime).ToList(),
                            Id = x.Id,
                            UserOne = x.UserOne,
                            UserTwo = x.UserTwo,
                            UserOneId = x.UserOneId,
                            UserTwoId = x.UserTwoId,
                            Unread = x.Unread
                        }).ToList();
                return View(res);
            }
        }
        [HttpGet]
        public async Task<ActionResult> CreateChatIfNotExist(Guid id)
        {
            await NewAutorizingLogic();
            var usrId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var dialog =
                    context.Dialogs.Where(
                        x => (x.UserOneId == usrId && x.UserTwoId == id) || (x.UserOneId == id && x.UserTwoId == usrId));
                if (dialog.Any())
                {
                    Response.Cookies.Add(new HttpCookie("selectDialog", id.ToString()) { Path = "/" });
                    return new RedirectResult(Url.Action("LoadMain", "Main", new {area="Dashboard"}) + "#chat");
                }


                var diallog = new Dialog()
                {
                    CreateDate = DateTime.UtcNow,
                    UserOneId = id,
                    UserTwoId = Guid.Parse(User.Identity.GetUserId())
                };

                context.Dialogs.Add(diallog);
                await context.SaveChangesAsync();
                //return RedirectToAction("Index","NewMyAccount", new { teachid = id });
                Response.Cookies.Add(new HttpCookie("selectDialog", id.ToString()) {Path = "/"});
                return new RedirectResult(Url.Action("LoadMain", "Main", new { area = "Dashboard" }) + "#chat");
            }
        }

        [WebMethod]
        [HttpPost]
        public JsonResult UploadImages()
        {
            if (Request.Files.Count <= 0) return Json(new {path = ""});
            var userid = Guid.Parse(User.Identity.GetUserId());
            var files = Request.Files;
            if (!Directory.Exists(Server.MapPath($"~/Images/SendImages/{userid}")))
                Directory.CreateDirectory(Server.MapPath($"~/Images/SendImages/{userid}"));
            var serverpath = Server.MapPath($"~/Images/SendImages/{userid}");
            var directory = $"/Images/SendImages/{userid}/";
            var pic = files[0];
            var fl = Guid.NewGuid() + pic.FileName;
            pic.SaveAs(Path.Combine(serverpath, fl));
            return Json(new {path = Path.Combine(directory, fl), name = pic.FileName });
        }

        public async Task<ActionResult> OpenVideoChat()
        {
            await NewAutorizingLogic();
            return View("/Views/Chat/NewSolutionForVideoChat.cshtml");
        }
    }
}