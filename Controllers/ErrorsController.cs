﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Xencore.Filters;

namespace Xencore.Controllers
{
    [Culture]
    public class ErrorsController : TemplateOpenlangController
    {
        [GlobalUserDetect(WithTablePrices = true)]
        // GET: Errors
        public async Task<ActionResult> Error404()
        {
            //Response.StatusCode = 404;
            await NewAutorizingLogic();
            return View();
        }
        public async Task<ActionResult> FatalError()
        {
            //if (User.Identity.IsAuthenticated)
            //    HttpContext.GetOwinContext().Authentication.SignOut();
            await NewAutorizingLogic();
            return View();
        }
    }
}