﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Domain.Context;


namespace Xencore.Controllers.api
{
    public class TeacherStudentsController : ApiController
    {
        [HttpGet]
        public IEnumerable<object> Get(Guid id)
        {
            using (var context = new MyDbContext())
            {
                var res = context.Schedules.Where(x=>x.CustomUserId == id).SelectMany(x=>x.ScheduleTransactions.Select(f=>f.User))
                    .GroupBy(x => x, (guid, detailses) => new
                    {
                        UserId = guid.Id,
                        TotalCoutLessons = detailses.Count(),
                        SpentLessons = detailses.Count(),
                        guid.ProfilePicture,
                        guid.Location,
                        guid.Ilearn,
                        guid.Name
                    }).ToList();
                //var res = context.OrderDetailses.Where(x => x.CourseBoughtFromId == id)
                //    .Include(x => x.PaymentTransactionsId.CustomUserId)
                //    .GroupBy(x => x.PaymentTransactionsId.CustomUserId, (guid, detailses) => new
                //    {
                //        UserId = guid.Id,
                //        TotalCoutLessons = detailses.Sum(f=>f.CountLessons),
                //        SpentLessons = detailses.Sum(f=>f.CoursesSpent),
                //        guid.ProfilePicture,
                //        guid.Location,
                //        guid.Ilearn,
                //        guid.Name
                //    }).ToList();
                return res;
            }
        }
    }
}
