﻿using Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RefferalProgrammComponent;

namespace Xencore.Controllers.api.RefferalProgramm
{
    public class RefferalProgrammController : ApiController
    {
        private IReffreralProgrammRepository Repo { get; set; }
        public RefferalProgrammController(IReffreralProgrammRepository repo)
        {
            Repo = repo;
        }
        [AllowAnonymous]
        [HttpGet]
        public bool CheckReferCode(Guid refferCode)
        {
            return RefferalProgrammComponent.RefferalProgramm.CheckExistRefferalCode(Repo, refferCode);
        }
    }
}
