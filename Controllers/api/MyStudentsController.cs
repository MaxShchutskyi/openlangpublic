﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Domain.Context;
using Microsoft.AspNet.Identity;

using Xencore.Entities;

namespace Xencore.Controllers.api
{
    public class MyStudentsController : ApiController
    {
        [HttpGet]
        public IEnumerable<object> Get(Guid id, string filter, int?page)
        {
            var page2 = page ?? 0;
            using (var context = new MyDbContext())
            {
                var datetime = DateTime.UtcNow;
                var res =
                    context.Schedules.Where(x => x.CustomUserId == id)
                        .Include(x => x.ScheduleTransactions.Select(f => f.User))
                        .SelectMany(x => x.ScheduleTransactions)
                        .Select(r => new
                        {
                            r.Id,
                            UserId = r.Id,
                            r.User.Location,
                            r.User.ProfilePicture,
                            r.User.UserName,
                            r.User.Name,
                            r.Schedule.Level,
                            r.Price,
                            r.Schedule.StartDateTimeDateTime,
                            r.Language,
                            r.Schedule.Subject,
                            r.Schedule.IsConfirmed,
                            r.Schedule.Type
                        });
                //var res = context.OrderDetailses.Where(x => x.CourseBoughtFromId == id)
                //    .Include(x => x.PaymentTransactionsId.CustomUserId).Include(x => x.PaymentTransactionsId.Schedules).Select(r => new
                //    {
                //        r.PaymentTransactionsId.CustomUserId.Id,
                //        r.SelectedLanguage,
                //        r.CountLessons,
                //        CoursesSpent = r.PaymentTransactionsId.Schedules.Count(f => f.StartDateTimeDateTime < datetime),
                //        UserId = r.PaymentTransactionsId.CustomUserId.Id,
                //        r.PaymentTransactionsId.CustomUserId.Location,
                //        r.PaymentTransactionsId.CustomUserId.ProfilePicture,
                //        r.PaymentTransactionsId.CustomUserId.UserName,
                //        r.PaymentTransactionsId.CustomUserId.Name,
                //        r.CourseType,
                //        r.TransactionId
                //    });
                switch (filter)
                {
                    case "Active": res = res.Where(x => x.StartDateTimeDateTime > datetime); break;
                    case "Past": res = res.Where(x => x.StartDateTimeDateTime < datetime); break;
                    case "Pending":
                        res =
                            res.Where(x => x.StartDateTimeDateTime > datetime && !x.IsConfirmed);
                        break;
                }
                var result = res.OrderByDescending(x => x.StartDateTimeDateTime).AsEnumerable().Skip(page2 * 7).Take(7).Select(r => new
                    {
                        r.Id,r.StartDateTimeDateTime, r.Language, r.Level, r.Price, r.Type, r.Location,r.UserId,r.ProfilePicture,r.UserName,r.Name,isOnline = HubsOfSignalR.ChatHub.ConnectedIds.FirstOrDefault(x => x == r.Id.ToString())
                    }).Reverse().ToList();
                return result;
            }
        }

        [HttpGet]
        [Authorize]
        public async Task<IEnumerable<object>> GetMyStudents()
        {
            var userId = Guid.Parse(User.Identity.GetUserId());
            var systemservice = Guid.Parse("11111111-1111-1111-1111-111111111111");
            using (var context = new MyDbContext())
            {
                var detetime = DateTime.UtcNow;
                //var dt1 = await context.Schedules.Where(x => x.CustomUserId == userId).ToListAsync();
                //var dt = await context.Schedules.Where(x => x.CustomUserId == userId)
                //    .Include(x => x.ScheduleTransactions).SelectMany(x => x.ScheduleTransactions).ToListAsync();
                var students = await context.Schedules.Where(x => x.CustomUserId == userId).Include(x=>x.ScheduleTransactions).SelectMany(x => x.ScheduleTransactions)
                    .GroupBy(x =>x.User,(x,y)=>x).Select(x => new
                    {
                        x.Id,
                        x.ProfilePicture,
                        x.Name,
                        x.Money,
                        x.PricesForAllTypes,
                        credits = x.Credits.Where(f => f.LeftLessons != 0 && f.EndDate > detetime).Select(f => new { f.Id, f.LeftLessons, Type = f.Type.ToString(), f.StudentId, f.PricePerLesson })
                    }).ToListAsync();
                return students;
                //var students = await context.Dialogs.Where(x => (x.UserOneId == userId || x.UserTwoId == userId) && (x.UserOneId!= systemservice && x.UserTwoId!= systemservice))
                //    .Select(x => x.UserOneId != userId ? x.UserOne : x.UserTwo).Where(x=>x.Credits.Any(f => f.LeftLessons != 0 && f.EndDate > detetime))
                //    .Select(x => new
                //    {
                //        x.Id,x.ProfilePicture, x.Name, x.Money, x.PricesForAllTypes,
                //        credits = x.Credits.Where(f => f.LeftLessons != 0 && f.EndDate > detetime).Select(f=>new {f.Id, f.LeftLessons, Type =  f.Type.ToString(), f.StudentId, f.PricePerLesson})
                //    }).ToListAsync();
                //return students;
            }
        }
    }
}
