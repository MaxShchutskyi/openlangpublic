﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Domain.Context;
using Repositories.Abstract;

namespace Xencore.Controllers.api
{
    [Authorize]
    public class DiscountsController : ApiController
    {
        private Guid UserId =>  Guid.Parse(User.Identity.GetUserId());
        private IDiscountRepository Repo { get; }

        public DiscountsController(IDiscountRepository repo)
        {
            Repo = repo;
        }
        [HttpGet]
        public async Task<object> CheckExistsDiscount(string discount, string type)
        {
            using (var context = new MyDbContext())
            {
                var utc = DateTime.UtcNow;
                var res =
                    await
                        context.Discounts.Where(
                                x =>
                                    x.Number == discount && (x.Type == null || x.Type == "Referral" || x.Type == type) &&
                                    x.PeriodAccessEnd > utc && x.PeriodAccessStart < utc)
                            .Include(x => x.Payments)
                            .FirstOrDefaultAsync();
                if (res == null || res.Payments.Any(x=>x.AttendeeId == UserId))
                    return null;
                return new {res.Id, res.Value, res.IsPercent};
            }
        }
    }
}
