﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Domain.Context;


namespace Xencore.Controllers.api
{
    public class TeachersOfStudentsController : ApiController
    {
        [HttpGet]
        public IEnumerable<object> Get(Guid id, string filter)
        {
            var datetime = DateTime.UtcNow;
            using (var context = new MyDbContext())
            {
                var res =
                    context.ScheduleTransactions.Where(x => x.CustomUserId == id)
                        .Include(x => x.Schedule)
                        .Select(f => f.Schedule)
                        .Include(x => x.CustomUser)
                        .GroupBy(x => x.CustomUser, (user, enumerable) => new
                        {
                            user.Id,
                            user.Ilearn,
                            CountLessons = enumerable.Count(),
                            SpentLessons = enumerable.Count(),
                            user.Location,
                            user.ProfilePicture,
                            user.UserName,
                            user.Name
                        });
                if (filter != null)
                {
                    switch (filter)
                    {
                        case "Active": res = res.Where(x => x.CountLessons != x.SpentLessons); break;
                        case "Inactive":
                            res = res.Where(x => x.CountLessons == x.SpentLessons); break;
                    }
                }
                var rest = res.AsEnumerable().Select(r => new
                {
                    r.Id,
                    r.Ilearn,
                    r.CountLessons,
                    r.SpentLessons,
                    r.Location,
                    r.ProfilePicture,
                    r.UserName,
                    r.Name,
                    isOnline = HubsOfSignalR.ChatHub.ConnectedIds.FirstOrDefault(x => x == r.Id.ToString())
                }).ToList();
                return rest;
            }

            //var datetime = DateTime.UtcNow;
            //using (var context = new MyDbContext())
            //{
            //    var res =
            //        context.PaymentTransactions.Where(x => x.AttendeeId == id)
            //            .Include(x => x.OrderDetailsId.CourseBoughtFrom).Include(x=>x.Schedules)
            //            .Where(x => x.OrderDetailsId.CourseBoughtFromId != null)
            //            .GroupBy(x => x.OrderDetailsId.CourseBoughtFrom, (user, enumerable) => new
            //            {
            //                user.Id,
            //                user.Ilearn,
            //                CountLessons = enumerable.Sum(x => x.OrderDetailsId.CountLessons),
            //                SpentLessons = enumerable.SelectMany(z=>z.Schedules).Count(d=>d.StartDateTimeDateTime < datetime),
            //                user.Location,
            //                user.ProfilePicture,
            //                user.UserName,
            //                user.Name
            //            });
            //    if (filter != null)
            //    {
            //        switch (filter)
            //        {
            //            case "Active":res = res.Where(x=>x.CountLessons!=x.SpentLessons);break;
            //            case "Inactive":
            //                res = res.Where(x => x.CountLessons == x.SpentLessons);break;
            //        }
            //    }
            //    var rest = res.AsEnumerable().Select(r => new
            //    {
            //        r.Id,
            //        r.Ilearn,
            //        r.CountLessons,
            //        r.SpentLessons,
            //        r.Location,
            //        r.ProfilePicture,
            //        r.UserName,
            //        r.Name,
            //        isOnline = HubsOfSignalR.ChatHub.ConnectedIds.FirstOrDefault(x => x == r.Id.ToString())
            //    }).ToList();
            //    return rest;
            //}
        }
    }
}
