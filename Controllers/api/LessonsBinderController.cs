﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Domain.Context;
using Domain.Entities;
using Domain.Helpers;
using DTO.Enums;
using EmailSenderService;
using PushNotificationService;
using Xencore.EmailSender;
using Xencore.Helpers;
using Xencore.Managers.TablesOfPrices.Abstract;
using Xencore.Services;

namespace Xencore.Controllers.api
{
    public class LessonsBinderController : ApiController
    {
        [HttpGet]
        public async Task<object> BindLesson(short creditId, int scheduleId, string paymentId, decimal price, Guid teacherId, Guid studentId, TypeLessons type, string language, string level)
        {
            using (var context = new MyDbContext())
            {
                var teacher = context.Users.Find(teacherId);
                //var st = context.Users.Find(studentId);
                var st = context.Users.Where(x => x.Id == studentId).Include(x => x.Credits).FirstOrDefault();
                var credit = st.Credits.First(x => x.Id == creditId);
                var obj = new ScheduleTransaction()
                {
                    CreditId = creditId,
                    ScheduleId = scheduleId,
                    Price = price,
                    CustomUserId = studentId,
                    Language = "Obsolete"
                };
                context.ScheduleTransactions.Add(obj);
                credit.LeftLessons -= 1;
                context.Configuration.ValidateOnSaveEnabled = false;
                var scheduler = await context.Schedules.FindAsync(scheduleId);
                context.Schedules.Attach(scheduler);
                scheduler.Status = "view1 unconfirmed";
                scheduler.Type = type;
                scheduler.Language = language;
                scheduler.Level = level;
                scheduler.IsConfirmed = false;
                if (string.IsNullOrEmpty(st.Ilearn))
                    st.Ilearn = "cap_" + language + "|";
                else
                    st.Ilearn += "cap_" + language + "|";
                await context.SaveChangesAsync();
                var messToTeacher =
                    $"{st.Name} has just booked a {Resources.Resource.ResourceManager.GetString(scheduler.Language)} lesson with you on {teacher.GetLocalTime(scheduler.StartDateTimeDateTime)}. Please visit your profile to confirm the lesson.";
                var messToStudent = $"You have just booked a {Resources.Resource.ResourceManager.GetString(scheduler.Language)} lesson for {st.GetLocalTime(scheduler.StartDateTimeDateTime)} with {teacher.Name}";
                var messToAdmin =
                    $"{st.Name} has just booked a {Resources.Resource.ResourceManager.GetString(scheduler.Language)} lesson with {teacher.Name} on {scheduler.StartDateTimeDateTime} UTC time. Please visit your profile to confirm the lesson.";
                var notificationSystem = new NotificationChatService(st.Id, teacherId);
                await
                    PushNotificationService.PushNotificationService.SendNotification(
                        new NotificationModel()
                        {
                            Title = "Booked lesson",
                            Uri =
                                Request.RequestUri.Scheme + "://" +
                                Request.RequestUri.Authority + "/my-account#/mylessons",
                            Message = messToTeacher,
                            Icon =
                                Request.RequestUri.Scheme + "://" +
                                Request.RequestUri.Authority + "/img/icon.png"
                        }, teacherId);
                await notificationSystem.SendAsync(messToTeacher, messToStudent).ConfigureAwait(false);
                var client = new EmailSender.EmailSendClient();
                var htmlMessTeacher = EmailGeneratorService.GetBodyWithText(messToTeacher);
                var htmlMessStudent = EmailGeneratorService.GetBodyWithText(messToStudent);
                var htmlMessAdmin = EmailGeneratorService.GetBodyWithText(messToAdmin);
                client.SendAsync(teacher.Id,  "Booking lesson", htmlMessTeacher, EmailSender.MessageStatus.AddedStudentToLesson);
                client.SendAsync(st.Id, "Booking lesson", htmlMessStudent, EmailSender.MessageStatus.AddedStudentToLesson);
                client.SendForAdminAsync(htmlMessAdmin, "Student booked a lesson", EmailSender.MessageStatus.Empty);
                if (type == TypeLessons.Trial)
                {
                    return scheduler.StartDateTimeDateTime;
                }
                return 1;
            }
        }
        [HttpGet]
        public async Task<int> BindGroupLesson(int scheduleId, string paymentId, decimal price, Guid teacherId, Guid studentId, TypeLessons type)
        {
            return -1;
            //using (var context = new MyDbContext())
            //{
            //    var st = context.Users.Find(studentId);
            //    var teacher = context.Users.Find(teacherId);
            //    var exist = await context.ScheduleTransactions.FirstOrDefaultAsync(x => x.CustomUserId == studentId && x.ScheduleId == scheduleId);
            //    if (exist != null)
            //        return -1;
            //    var scHd = new ScheduleTransaction() { CreatedDate = DateTime.UtcNow, Price = price, CustomUserId = studentId, ScheduleId = scheduleId, Language = "Underfind"};
            //    st.Money -= price;
            //    context.ScheduleTransactions.Add(scHd);
            //    await context.SaveChangesAsync();
            //    var scheduler = await context.Schedules.FindAsync(scheduleId);
            //    var messToTeacher =
            //        $"{st.Name} has just booked a {Resources.Resource.ResourceManager.GetString(scheduler.Language)} lesson with you on {teacher.GetLocalTime(scheduler.StartDateTimeDateTime)}. Please visit your profile to confirm the lesson.";
            //    var messToStudent = $"You have just booked a {Resources.Resource.ResourceManager.GetString(scheduler.Language)} lesson for {st.GetLocalTime(scheduler.StartDateTimeDateTime)} with {teacher.Name}";
            //    var notificationSystem = new NotificationChatService(st.Id, teacherId);
            //    await notificationSystem.SendAsync(messToTeacher, messToStudent).ConfigureAwait(false);
            //    var client = new EmailSender.EmailSendClient();
            //    client.SendAsync(st.Id, "Booking lesson", messToStudent, EmailSender.MessageStatus.LessonCreated);
            //    client.SendAsync(teacherId, "Booking lesson", messToTeacher, EmailSender.MessageStatus.LessonCreated);
            //    return 1;
            //}
        }
    }
}
