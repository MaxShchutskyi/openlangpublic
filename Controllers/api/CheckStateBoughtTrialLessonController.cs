﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Domain.Context;
using DTO.Enums;
using Xencore.Managers.TablesOfPrices.Abstract;


namespace Xencore.Controllers.api
{
    public class CheckStateBoughtTrialLessonController : ApiController
    {
        [HttpGet]
        public async Task<IEnumerable<object>> IsBoughtTrialLesson(Guid id)
        {
            using (var context = new MyDbContext())
            {
                return await context.Schedules.Where(
                    x =>
                        x.Type == TypeLessons.Trial &&
                        x.ScheduleTransactions.Any(f => f.CustomUserId == id)).Select(x => new
                {
                    x.Language, x.CustomUserId, x.Type, x.StartDateTimeDateTime
                }).ToArrayAsync();
            }
        }
    }
}
