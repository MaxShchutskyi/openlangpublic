﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Domain.Context;
using Domain.Entities;
using Microsoft.AspNet.Identity;


namespace Xencore.Controllers.api
{
    [Authorize]
    public class ChatApiController : ApiController
    {
        public Guid UserId => Guid.Parse(User.Identity.GetUserId());
       [HttpGet]
        public object GetCountUnreadMessages()
        {
            using (var context = new MyDbContext())
            {
                var count =
                    context.Dialogs.Where(x => x.UserOneId == UserId || x.UserTwoId == UserId)
                        .Include(x => x.Replies)
                        .SelectMany(x => x.Replies.Where(f => f.Unread && f.UserId != UserId))
                        .Count();
                return new {count};
            }
        }

        public async Task<IEnumerable<object>> GetAllDialogs()
        {
            var id = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var res = await 
                    context.Dialogs.Where(x => x.UserOneId == id || x.UserTwoId == id).Select(x => new
                    {
                        x.CreateDate,
                        Replies = x.Replies.OrderByDescending(f => f.DateTime).Take(10).Select(f => new {f.Id, f.DateTime, f.UtcDateTime, f.DialogId, f.Message, f.Unread, f.UserId }),
                        Id = x.Id,
                        UserOne = new { x.UserOne.Id, x.UserOne.Email, x.UserOne.ProfilePicture, x.UserOne.Name, IsUser = x.UserOne.Id == id, x.UserOne.DateOffline },
                        UserTwo = new { x.UserTwo.Id, x.UserTwo.Email, x.UserTwo.ProfilePicture, x.UserTwo.Name, IsUser = x.UserTwo.Id == id, x.UserOne.DateOffline },
                        Dialog = x.UserOne.Id != id ? new { x.UserOne.Id, x.UserOne.Email, x.UserOne.ProfilePicture, x.UserOne.Name, x.UserOne.DateOffline } : new { x.UserTwo.Id, x.UserTwo.Email, x.UserTwo.ProfilePicture, x.UserTwo.Name, x.UserOne.DateOffline },
                        UserOneId = x.UserOneId,
                        UserTwoId = x.UserTwoId,
                        PageCounter = 0
                    }).ToListAsync();
                return res.OrderByDescending(x => x.Replies.FirstOrDefault()?.DateTime).ToArray();
            }
        }
    }
}