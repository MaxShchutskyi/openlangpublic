﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Xencore.Models;
using Xencore.Repositories;

namespace Xencore.Controllers.api
{
    public class TeacherClassesController : ApiController
    {
        [HttpGet]
        public async Task<TeacherClassesUi> GetClassById(int id)
        {
            var res = await new TeacherClassesRepository().GetClassById(id);
            return res;
        }
        [HttpDelete]
        public async Task<bool> DeleteClass([FromUri]string idss)
        {
            var ids = idss?.Split(',').Select(int.Parse);
            if (!ids.Any()) return false;
            await new TeacherClassesRepository().DeleteClassById(ids);
            return true;
        }
        [HttpGet]
        public async Task<int> GetCountClasses(Guid id)
        {
            return await new TeacherClassesRepository().GetCountAllClasses(id);
        }
    }
}
