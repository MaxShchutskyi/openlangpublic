﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Domain.Context;


namespace Xencore.Controllers.api
{
    public class TeacherLessonsController : ApiController
    {
        [HttpGet]
        public IEnumerable<object> Get(Guid id, int? page)
        {
            page = page ?? 5;
            using (var context = new MyDbContext())
            {
                var res = context.Schedules.Where(x => x.CustomUserId == id && x.PaymentTransactionsId != null).Include(x=>x.PaymentTransactions.OrderDetailsId)
                    .Include(x => x.PaymentTransactions.CustomUserId)
                    .Select(x => new
                    {
                        x.Id,
                        UserId = x.PaymentTransactions.CustomUserId.Id,
                        x.PaymentTransactions.CustomUserId.ProfilePicture,
                        x.StartDateTimeDateTime,
                        x.PaymentTransactions.CustomUserId.Location,
                        x.PaymentTransactions.OrderDetailsId.ChLang,
                        x.PaymentTransactions.OrderDetailsId.CoursesSpent,
                        x.PaymentTransactions.CustomUserId.Name,
                        x.PaymentTransactions.OrderDetailsId.CountLessons,
                    });
                return res.ToList();
            }
        }
    }
}
