﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;
using Domain.Helpers;
using DTO.Enums;
using EmailSenderService;
using Microsoft.AspNet.Identity;
using WebGrease.Css.Extensions;
using Xencore.Extentions;
using Xencore.Filters;
using Xencore.Managers.TablesOfPrices.Abstract;
using Xencore.Repository;
using Xencore.Services;
using Microsoft.AspNet.Identity.Owin;
using PushNotificationService;
using Xencore.EmailSender;
using Xencore.Helpers;

namespace Xencore.Controllers.api
{
    public class ScheduleController : ApiController
    {
        [HttpGet]
        public async Task<object> GetStudentScheduler(Guid teacherId, int? week, string localtime)
        {
            var year = DateTime.UtcNow.Year;
            using (var context = new MyDbContext())
            {
                var result = await
                    context.Schedules
                        .Where(
                            x =>
                                x.WeekNumber == week && x.StartDateTimeDateTime.Year == year &&
                                x.ScheduleTransactions.Any(f => f.CustomUserId == teacherId)).Select(x => new
                            {
                            title = x.CustomUser.Name,
                            Teacher = new {x.CustomUser.Name, x.CustomUser.Id, x.CustomUser.ProfilePicture },
                            students = x.ScheduleTransactions.Select(f => new { f.User.Name, f.User.Id, f.User.ProfilePicture }),
                            id = x.Id,
                            x.Level,
                            x.Type,
                            x.Title,
                            x.Language,
                            x.IsConfirmed,
                            start = x.StartDateTime,
                            end = x.EndDateTime,
                            className = x.Status
                        }).ToListAsync();
                var res2 = result.Select(x => new
                {
                    x.title,
                    x.id,
                    teacher = x.Teacher,
                    level = x.Level,
                    x.start,
                    x.students,
                    x.IsConfirmed,
                    topic = x.Title,
                    language = x.Language,
                    x.end,
                    x.Type,
                    x.className,
                    type = x.Type.ToString()
                });
                return Json(res2);
            }
        }
        [Authorize]
        public async Task<IEnumerable<object>> GetTeacherCalendar(Guid teacherId, int? week, string localtime)
        {
            var userId = Guid.Parse(User.Identity.GetUserId());
            week = GetCurrentWeek(week, localtime);
            var year = DateTime.UtcNow.Year;
            using (var context = new MyDbContext())
            {
                var res = await context.Schedules.Where(
                    x =>
                        x.CustomUserId == teacherId && x.WeekNumber == week && x.StartDateTimeDateTime.Year == year &&
                        (x.Type == TypeLessons.Group || !x.ScheduleTransactions.Any())).Select(x => new
                {
                    title = x.CustomUser.Name,
                    Teacher = new {x.CustomUser.Name, x.CustomUser.Id, x.CustomUser.ProfilePicture},
                    students = x.ScheduleTransactions.Select(f=> new {f.User.Name, f.User.Id, f.User.ProfilePicture}),
                    id = x.Id,
                    x.Level,
                    x.Type,
                    x.Language,
                    x.Title,
                    x.IsConfirmed,
                    x.StartDateTimeDateTime,
                    start = x.StartDateTime,
                    end = x.EndDateTime,
                    className = x.Status
                }).ToArrayAsync();
                var studExec = await 
                    context.ScheduleTransactions.Where(f => f.CustomUserId == userId)
                        .Select(f => f.Schedule)
                        .Where(f => f.WeekNumber == week && f.StartDateTimeDateTime.Year == year)
                        .Select(f => f.StartDateTimeDateTime)
                        .ToArrayAsync();
                return res.Where(x=>studExec.All(f=>f != x.StartDateTimeDateTime)).Select(x => new
                {
                    x.title,
                    x.id,
                    teacher = x.Teacher,
                    level = x.Level,
                    x.start,
                    x.students,
                    topic = x.Title,
                    x.IsConfirmed,
                    language = x.Language,
                    x.end,
                    x.Type,
                    x.className,
                    type = x.Type.ToString()
                });
            }
        }
        [HttpGet]
        [CultureWebApi]
        public async Task<IEnumerable<object>> GetShortListTeachers()
        {
            var userId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var res = 
                    await
                        context.ScheduleTransactions.Where(x => x.CustomUserId == userId)
                            .Select(x => new
                            {
                                x.Schedule.CustomUser.Name,
                                x.Schedule.CustomUser.ProfilePicture,
                                x.Schedule.CustomUser.Id,
                                x.Schedule.CustomUser.Ilearn
                            }).Distinct().ToArrayAsync();
                return res.Select(x=> new
                {
                    x.Name, x.Id, x.ProfilePicture, teach = x.Ilearn.ToJsonLearn('|')
                }).ToArray();
            }
        }
        [HttpPost]
        [Authorize]
        public async Task<object> BindToGroupLesson(BindToLessonsHelper helper)
        {
            var userId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var user = context.Users.Find(userId);
                var teacher = context.Users.Find(helper.TeacherInfo.Id);
                var price = user.CheckAccessMoney(helper.Type);
                if (price == -1)
                    return null;
                var scheduleTransaction = new ScheduleTransaction() {CreatedDate = DateTime.UtcNow, CustomUserId = userId, Price = price, ScheduleId = helper.Id, Language = "Undefind"};
                context.ScheduleTransactions.Add(scheduleTransaction);
                user.Money -= price;
                await context.SaveChangesAsync();
                var schedule = await context.Schedules.FindAsync(helper.Id);
                var notificationSystem = new NotificationChatService(userId, helper.TeacherInfo.Id);
                var messToTeacher =
                    $"{user.Name} has just booked a {Resources.Resource.ResourceManager.GetString(schedule.Language)} lesson with you on {teacher.GetLocalTime(schedule.StartDateTimeDateTime)}. Please visit your profile to confirm the lesson.";
                var messToStudent =
                    $"You have just booked a {Resources.Resource.ResourceManager.GetString(schedule.Language)} lesson for {user.GetLocalTime(schedule.StartDateTimeDateTime)} with {teacher.Name}";
                await notificationSystem.SendAsync(messToTeacher, messToStudent).ConfigureAwait(false);
                var client = new EmailSendClient();
                client.SendAsync(userId, "Booking lesson", messToStudent, EmailSender.MessageStatus.AddedStudentToLesson);
                client.SendAsync(teacher.Id, "Booking lesson", messToTeacher, EmailSender.MessageStatus.AddedStudentToLesson);
                return user.Money;
            }
        }
        [HttpPost]
        [Authorize]
        public async Task<object> BindToLesson(BindToLessonsHelper helper)
        {
            if (helper.Type == TypeLessons.Group && !ModelState.IsValid)
                return null;
            var userId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var user = context.Users.Where(x=>x.Id == userId).Include(x=>x.Credits).FirstOrDefault();
                var credit = user.Credits.First(x => x.Id == helper.CreditId);
                var teacher = context.Users.Find(helper.TeacherInfo.Id);
                var scheduleTransaction = new ScheduleTransaction() { CreatedDate = DateTime.UtcNow, CustomUserId = userId, Price = credit.PricePerLesson, ScheduleId = helper.Id, Language = "Undefind", CreditId = credit.Id};
                var schedule = await context.Schedules.FindAsync(helper.Id);
                schedule.Status += " " + "unconfirmed";
                schedule.Level = helper.Level;
                schedule.Type = helper.Type;
                schedule.Title = helper.Topic;
                schedule.Language = helper.Language;

                context.ScheduleTransactions.Add(scheduleTransaction);
                credit.LeftLessons -= 1;
                await context.SaveChangesAsync();
                var notificationSystem = new NotificationChatService(userId, helper.TeacherInfo.Id);
                var messToTeacher =
                    $"{user.Name} has just booked a {Resources.Resource.ResourceManager.GetString(schedule.Language)} lesson with you on {teacher.GetLocalTime(schedule.StartDateTimeDateTime)}. Please visit your profile to confirm the lesson.";
                var messToStudent =
                    $"You have just booked a {Resources.Resource.ResourceManager.GetString(schedule.Language)} lesson for {user.GetLocalTime(schedule.StartDateTimeDateTime)} with {teacher.Name}";
                await notificationSystem.SendAsync(messToTeacher, messToStudent);
                await
                    PushNotificationService.PushNotificationService.SendNotification(
                        new NotificationModel()
                        {
                            Title = "Booked lesson",
                            Uri = HttpContext.Current.Request.Url.Scheme + "://" +  HttpContext.Current.Request.Url.Authority + "/my-account#/mylessons",
                            Message = messToTeacher,
                            Icon = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/img/icon.png"
                        }, teacher.Id);
                var client = new EmailSendClient();
                
                var htmlMessTeacher = EmailGeneratorService.GetBodyWithText(messToTeacher);
                var htmlMessStudent = EmailGeneratorService.GetBodyWithText(messToStudent);
                client.SendAsync(teacher.Id, "Booking lesson", htmlMessTeacher, EmailSender.MessageStatus.AddedStudentToLesson);
                client.SendAsync(user.Id, "Booking lesson", htmlMessStudent, EmailSender.MessageStatus.AddedStudentToLesson);
                return user.Money;
            }
        }

        [System.Web.Http.NonAction]
        private int GetCurrentWeek(int? week, string localtime)
        {
            if (week != null) return (int)week;
            var utc = DateTime.UtcNow;
            week = CultureInfo.GetCultureInfo("en-GB").Calendar.GetWeekOfYear(utc,
                CultureInfo.CurrentCulture.DateTimeFormat.CalendarWeekRule,
                DayOfWeek.Sunday);
            if (localtime.Contains("."))
            {
                var hours = localtime.Split('.');
                var local = int.Parse(hours[0]) * -1;
                if (hours[1].Length == 1)
                    hours[1] = hours[1] + "0";
                var min = (int.Parse(hours[1]) * 0.6) * -1;
                if (utc.DayOfWeek == DayOfWeek.Saturday && utc.AddHours(local).DayOfWeek == DayOfWeek.Sunday)
                    week++;
                if (utc.DayOfWeek == DayOfWeek.Saturday && utc.AddHours(local).AddMinutes(min).DayOfWeek == DayOfWeek.Sunday)
                    week++;

            }
            else
            {
                var local = int.Parse(localtime) * -1;
                if (utc.DayOfWeek == DayOfWeek.Saturday && utc.AddHours(local).DayOfWeek == DayOfWeek.Sunday)
                    week++;
            }
            return (int)week;
        }
        //[HttpGet]
        //public async Task<object> GetScheduleById(int id)
        //{
        //    using (var context = new MyDbContext())
        //    {
        //        var res = await context.Schedules.FindAsync(id);
        //        return new
        //        {
        //            res.Id,
        //            StartDateTimeDateTime = res.StartDateTimeDateTime.ToString("dd-MM-yyyy HH:mm"),
        //            res.Img,
        //            res.Subject,
        //            res.Title,
        //            res.Description,
        //            res.Level,
        //            res.ScheduleTransactions.First().Language
        //        };
        //    }
        //}

        //[HttpGet]
        //public async Task<bool> UpdateScheduleById(int id,string  subject,string  title, string level, string desc, string img)
        //{
        //    using (var context = new MyDbContext())
        //    {
        //        var res = await context.Schedules.FindAsync(id);
        //        res.Subject = subject;
        //        res.Description = desc;
        //        res.Title = title;
        //        res.Level = level;
        //        if (!string.IsNullOrEmpty(img))
        //            res.Img = img;
        //        await context.SaveChangesAsync();
        //        return true;
        //    }
        //}
    }
}
