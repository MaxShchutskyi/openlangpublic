﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Domain.Context;
using Microsoft.AspNet.Identity;

namespace Xencore.Controllers.api
{
    [Authorize]
    public class StatisticController : ApiController
    {
        public Guid UserId => Guid.Parse(User.Identity.GetUserId());
        [HttpGet]
        public async Task<object> GetTeacherStatitic()
        {
            using (var context = new MyDbContext())
            {
                var date = DateTime.UtcNow;
                var schedules = await context.Schedules.Where(x => x.CustomUserId == UserId && !x.ClassIsNotHappen).Include(x => x.ScheduleTransactions).Select(x=> new
                {
                    x.StartDateTimeDateTime, users = x.ScheduleTransactions.Select(f=> new {f.CustomUserId})
                }).ToListAsync();
                var unschuduled = schedules.Count(x => x.StartDateTimeDateTime > date && !x.users.Any());
                var scheduled = schedules.Count(x => x.StartDateTimeDateTime > date && x.users.Any());
                var past = schedules.Count(x => x.StartDateTimeDateTime < date && x.users.Any());
                var students = schedules.SelectMany(x => x.users).Distinct().Count();
                return new {unschuduled, scheduled, past, students};

            }
        }
        public async Task<object> GetStudentStatitic()
        {
            using (var context = new MyDbContext())
            {
                var date = DateTime.UtcNow;
                var schedules = await context.ScheduleTransactions.Where(x => x.CustomUserId == UserId && !x.Schedule.ClassIsNotHappen).Select(x => new
                {
                    x.Schedule.StartDateTimeDateTime,
                    teacher = x.Schedule.CustomUserId
                }).ToListAsync();
                var scheduled = schedules.Count(x => x.StartDateTimeDateTime > date);
                var past = schedules.Count(x => x.StartDateTimeDateTime < date);
                var teachers = schedules.Select(x => x.teacher).Distinct().Count();
                return new { scheduled, past, teachers };
            }
        }
    }
}
