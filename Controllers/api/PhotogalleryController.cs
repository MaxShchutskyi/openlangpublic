﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.HelpersEntity;
using Microsoft.AspNet.Identity;
using WebGrease.Css.Extensions;

namespace Xencore.Controllers.api
{
    [Authorize(Roles = "Presenter")]
    public class PhotogalleryController : ApiController
    {
        public Guid UserId => Guid.Parse(User.Identity.GetUserId());
        public async Task<IEnumerable<PhotoGalleryHelper>> GetGallery()
        {
            using (var context = new MyDbContext())
            {
                var gal = await context.PhotoGalleries.Where(x => x.CustomUserId == UserId).ToListAsync();
                return gal.Select(x => new PhotoGalleryHelper()
                {
                    Id = x.Id,
                    CustomUserId = x.CustomUserId,
                    Description = x.Description,
                    Title = x.Title,
                    FileName = x.FileName,
                    Dimensision = x.Dimensision,
                    FileSize = x.FileSize,
                    FileType = x.FileType,
                    LinkBigImage = x.LinkBigImage,
                    LinkSmallImage = x.LinkSmallImage,
                    UploadDate = x.UploadDate
                }).ToList();
            }
        }
        [HttpPost]
        public async Task<object> RemoveImages(PhotoGalleryHelper img)
        {
            var mapPath = HttpContext.Current.Server.MapPath($"~/Images/teacher-gallery/{User.Identity.GetUserId()}");
            if (File.Exists(Path.Combine(mapPath, img.FileName)))
                System.IO.File.Delete(Path.Combine(mapPath, img.FileName));
            if (File.Exists(Path.Combine(mapPath, "small_" + img.FileName)))
                System.IO.File.Delete(Path.Combine(mapPath, "small_" + img.FileName));
            using (var context = new MyDbContext())
            {
                var gals = new PhotoGallery() { Id = img.Id };
                context.PhotoGalleries.Attach(gals);
                context.Entry(gals).State = EntityState.Deleted;
                await context.SaveChangesAsync();
            }
            return new { success = "Saved" };
        }
        [HttpPost]
        public async Task<object> RemoveActiveImages(IEnumerable<string> imgs)
        {
            var userid = Guid.Parse(User.Identity.GetUserId());
            var mapPath = HttpContext.Current.Server.MapPath($"~/Images/teacher-gallery/{User.Identity.GetUserId()}");
            foreach (var img in imgs)
            {
                if (File.Exists(Path.Combine(mapPath, img)))
                    System.IO.File.Delete(Path.Combine(mapPath, img));
                if (File.Exists(Path.Combine(mapPath, "small_" + img)))
                    System.IO.File.Delete(Path.Combine(mapPath, "small_" + img));
            }
            using (var context = new MyDbContext())
            {
                var images = context.PhotoGalleries.Where(x => x.CustomUserId == userid && imgs.Contains(x.FileName));
                context.PhotoGalleries.RemoveRange(images);
                //var gals = imgs.Select(x => new PhotoGallery() { Id = x.Id }).ToList();
                //gals.ForEach(x =>
                //{
                //    context.PhotoGalleries.Attach(x);
                //    context.Entry(x).State = EntityState.Deleted;
                //});
                await context.SaveChangesAsync();
            }
            return new { success = "Saved" };
        }
    }
    //[HttpPost]
    //public async Task<object> RemoveActiveImages(IEnumerable<PhotoGalleryHelper> imgs)
    //{
    //    var mapPath = HttpContext.Current.Server.MapPath($"~/Images/teacher-gallery/{User.Identity.GetUserId()}");
    //    foreach (var img in imgs)
    //    {
    //        if (File.Exists(Path.Combine(mapPath, img.FileName)))
    //            System.IO.File.Delete(Path.Combine(mapPath, img.FileName));
    //        if (File.Exists(Path.Combine(mapPath, "small_" + img.FileName)))
    //            System.IO.File.Delete(Path.Combine(mapPath, "small_" + img.FileName));
    //    }
    //    using (var context = new MyDbContext())
    //    {
    //        var gals = imgs.Select(x => new PhotoGallery() {Id = x.Id}).ToList();
    //        gals.ForEach(x =>
    //        {
    //            context.PhotoGalleries.Attach(x);
    //            context.Entry(x).State = EntityState.Deleted;
    //        });
    //        await context.SaveChangesAsync();
    //    }
    //    return new { success = "Saved" };
    //}
}
