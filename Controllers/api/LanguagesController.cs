﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Xencore.Filters;

namespace Xencore.Controllers.api
{
    [System.Web.Http.Authorize]
    [CultureWebApi]
    public class LanguagesController : ApiController
    {
        public CustomUserManager UserManager => HttpContext.Current.GetOwinContext().Get<CustomUserManager>();
        public HttpResponseMessage Put(NewLanguagesHepler lang)
        {
            try
            {
                var id = HttpContext.Current.User.Identity.GetUserId();
                var user = UserManager.FindById(Guid.Parse(id));
                user.Ilearn = lang.Ilearn;
                user.Ispeak = lang.Ispeak;
                UserManager.Update(user);
                return Request.CreateResponse(HttpStatusCode.Created);
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        [HttpGet]
        public IEnumerable<object> GetILearn()
        {
            var id = HttpContext.Current.User.Identity.GetUserId();
            var user = UserManager.FindById(Guid.Parse(id));
            var res = user.Ilearn?.Split('|').Reverse().Skip(1).Reverse().Select(x => new { lang =  Resources.Resource.ResourceManager.GetString(x.Split('_')[1]), key = x.Split('_')[1]}).ToList();
            return res;
        }
    }
}
