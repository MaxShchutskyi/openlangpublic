﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using createsend_dotnet;
using EmailSenderService;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json.Linq;
using NLog;
using PaymentService.Release;
using PayPal.Api;
using Repositories.Abstract;
using Stripe;

namespace Xencore.Controllers.api
{
    public class PaymentSucceededController : ApiController
    {
        private IPaymentRepository Repo;
        public PaymentSucceededController(IPaymentRepository repo) => Repo = repo;
        [HttpPost]
        public async Task<HttpResponseMessage> Stripe()
        {
            LogManager.GetCurrentClassLogger().Info("Stripe subscribtion started....");
            var json = new StreamReader(HttpContext.Current.Request.InputStream).ReadToEnd();

            var stripeEvent = StripeEventUtility.ParseEvent(json);
            //LogManager.GetCurrentClassLogger().Info(json);
            if (stripeEvent.Type != "invoice.payment_succeeded")
            {
                LogManager.GetCurrentClassLogger().Error("Is not invoice.payment_succeeded");
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            var subscription = stripeEvent.Data.Object.subscription;
            var amount = stripeEvent.Data.Object.amount_due.ToString();
            if (subscription == null || amount == "0")
            {
                LogManager.GetCurrentClassLogger().Error("Request has no subdcription or amount == 0!");
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            var manager = new PaymentManager.PaymentManager(Repo);
            var res = await manager.UpdateSubscribtion(subscription.ToString());
            var message = res
                ? $"Stripe subscribtion was updated. Subscribtion - {subscription.ToString()}"
                : $"Stripe Error updating subscribtion. Subscribtion - {subscription.ToString()}";
            var body = EmailGeneratorService.GetBodyWithText(message);
            var emailSender = new EmailSender.EmailSendClient();
            await emailSender.SendHtmlForAdminAsync(body, "Updated subscribtion", EmailSender.MessageStatus.PaymentCompleted);
            LogManager.GetCurrentClassLogger().Info($"Status of Subscribtion - {res}");
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
        [HttpPost]
        public async Task<HttpResponseMessage> PayPal()
        {
            //var provider = new PayPalPaymentProvider();
            //var apiContext = provider.GetApiContext;
            var requestBody = string.Empty;
            using (var reader = new System.IO.StreamReader(HttpContext.Current.Request.InputStream))
            {
                requestBody = reader.ReadToEnd();
            }

            dynamic jsonBody = JObject.Parse(requestBody);
            string webhookId = jsonBody.id;
           // var ev = WebhookEvent.Get(apiContext, webhookId);
            switch (jsonBody.event_type.ToString())
            {
                case "PAYMENT.SALE.COMPLETED":
                {
                   var subscription =  jsonBody.resource.billing_agreement_id?.ToString();
                    if (subscription == null)
                        return new HttpResponseMessage(HttpStatusCode.OK);
                        await CheckPayPalSubscription(subscription);
                }
                    break;
            }

            return new HttpResponseMessage(HttpStatusCode.OK);
        }
        [NonAction]
        private async Task<object> CheckPayPalSubscription(string subscribtion)
        {
            var manager = new PaymentManager.PaymentManager(Repo);
            var res = await manager.UpdateSubscribtion(subscribtion);
            var message = res
                ? $"PayPal subscribtion was updated. Subscribtion - {subscribtion}"
                : $"Paypal Error updating subscribtion. Subscribtion - {subscribtion}";
            var body = EmailGeneratorService.GetBodyWithText(message);
            var emailSender = new EmailSender.EmailSendClient();
            await emailSender.SendHtmlForAdminAsync(body, "Updated subscribtion", EmailSender.MessageStatus.PaymentCompleted);
            LogManager.GetCurrentClassLogger().Info($"Status of Subscribtion - {res}");
            return new {Success = true};
        }

        protected override void Dispose(bool disposing)
        {
            Repo?.Dispose();
            base.Dispose(disposing);
        }
    }
}
