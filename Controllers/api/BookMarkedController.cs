﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web.Http;

using System.Threading.Tasks;
using Domain.Context;
using Domain.Entities;

namespace Xencore.Controllers.api
{
    public class BookMarkedController : ApiController
    {
        [HttpGet]
        public async Task<IEnumerable<object>> Get(Guid id)
        {
            using (var context = new MyDbContext())
            {
                return await context.BookMarkedTeachers.Where(x => x.StudentId == id).Include(x => x.Teacher).Select(x=>new
                {
                    x.Teacher.Id,
                    x.Teacher.Name,
                    x.Teacher.Ilearn,
                    x.Teacher.Location,
                    x.Teacher.ProfilePicture
                }).ToListAsync();
            }
        }
        [HttpPost]
        public async Task<bool> AddBook([FromBody]BookMarkedTeachers booked )
        {
            using (var context = new MyDbContext())
            {
                context.BookMarkedTeachers.Add(booked);
                await context.SaveChangesAsync();
                return true;
            }
        }
        [HttpPost]
        public async Task<bool> RemveBook([FromBody]BookMarkedTeachers booked)
        {
            using (var context = new MyDbContext())
            {
                context.BookMarkedTeachers.Remove(context.BookMarkedTeachers.Single(x=>x.TeacherId == booked.TeacherId && x.StudentId == booked.StudentId));
                await context.SaveChangesAsync();
                return true;
            }
        }
    }
}
