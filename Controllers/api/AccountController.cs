﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using AutoMapper;
using CurrencyConverter;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using Prices.Prices;
using Xencore.Entities;
using Xencore.Entities.IdentityEntities;
using Xencore.Extentions;
using Xencore.Filters;
using Repositories.Abstract;
using Ninject;

namespace Xencore.Controllers.api
{ 
    [Authorize]
    public class AccountController : ApiController
    {
        private static Image ResizeImage(Image imgToResize, Size size)
        {
            return (Image)(new Bitmap(imgToResize, size));
        }
        public CustomSignIn SignIn => HttpContext.Current.GetOwinContext().Get<CustomSignIn>();
        [AllowAnonymous]
        [HttpGet]
        public async Task<bool> CheckExistUser(string email)
        {
            return (await SignIn.UserManager.FindByEmailAsync(email)) == null;
        }
        [System.Web.Http.HttpGet]
        public UserHelper Get()
        {
            var userId = HttpContext.Current.GetOwinContext().Authentication.User.Identity.GetUserId();
            var user =
                SignIn
                    .UserManager.FindById(
                        Guid.Parse(userId));
            var userhelper = Mapper.Map<UserHelper>(user);
            return userhelper;

        }
        [Authorize]
        [HttpGet]
        public object GetCurrentInfo()
        {
            var userId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var user = context.Users.Find(userId);
                return JsonConvert.DeserializeObject(user.PricesForAllTypes);
            }
        }
        [Authorize]
        [HttpGet]
        public object GetCurrentPrices(string country, string timezone)
        {
            var prices = PricesManager.CreatePrices(country, timezone);
            var str = prices.GetPricesFromFile();
            var js = JsonConvert.DeserializeObject(str);
            return js;
        }
        [Authorize]
        [HttpGet]
        public async Task<IEnumerable<object>> GetCurrentGetAllCreditsPrices()
        {
            var userId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var rt =
                    await
                        context.Credits.Where(
                                x => x.StudentId == userId && x.LeftLessons != 0 && x.EndDate > DateTime.UtcNow).Include(x=>x.Payment.StripeSubscription)
                            .ToListAsync();
                return
                    rt.Select(
                        x =>
                            new
                            {
                                id = x.Id,
                                leftLessons = x.LeftLessons,
                                countLessons = x.CountLessons,
                                expireDate = x.EndDate.ToString("dd-MM-yyyy"),
                                typeOfPrice = x.TypeOfPrice,
                                type = x.Type.ToString(),
                                timeOfLessons = x.TimeOfLessons,
                                subscription = x.Payment?.StripeSubscription!=null? new  { x.Payment.StripeSubscription.SubscribeId, x.Payment.StripeSubscription.PlanId, x.Payment.StripeSubscription.Active, x.Payment.StripeSubscription.TypeProvider, x.Payment.StripeSubscription.CustomerId } :null
                            }).ToList();
            }
        }

        [HttpGet]
        public async Task<object> GetBalance()
        {
            decimal delta = 1;
            var manager = HttpContext.Current.GetOwinContext().Get<CustomUserManager>();
            var userId = Guid.Parse(User.Identity.GetUserId());
            var user = await manager.FindByIdAsync(userId);
            var modeInNative = user.ConvertTo();
            var currency = user.Currency.Split(':')[0];
            if(currency != "EUR")
            {
                var dt = CurrencyConverterFactory.Create<string>();
                delta = dt.Convert(currency, "EUR").Result;
            }
            return new {user.Money, NativeMoney = modeInNative + " " + currency, Currency = currency, delta = delta};
        }

        public HttpResponseMessage Put(UserHelper userHelper)
        {
            try
            {
                var userId = Guid.Parse(HttpContext.Current.GetOwinContext().Authentication.User.Identity.GetUserId());
                using (var context = new MyDbContext())
                {
                    var user  =context.Users.Where(x => x.Id == userId).Include(x => x.AdditionalUserInformation).FirstOrDefault();
                    user = Mapper.Map(userHelper, user);
                    if(user.AdditionalUserInformation == null)
                        user.AdditionalUserInformation = new AdditionalUserInformation();
                    user.AdditionalUserInformation.ShotIntrodution = userHelper.Shortintro;
                    user.AdditionalUserInformation.WhatLookingFor = userHelper.Lookingfor;
                    user.PhoneNumber = userHelper.Phone;
                    context.SaveChanges();
                    User.DeleteClaim("BeforeActivate",null);
                    User.AddUpdateClaim("Currency", user.Currency);
                    User.AddUpdateClaim("UsName", user.Name);
                }
                return Request.CreateResponse(HttpStatusCode.Created);
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        public string UploadFile()
        {
            var userid = HttpContext.Current.User.Identity.GetUserId();
            var files = HttpContext.Current.Request.Files[0];
            var filename = userid + "_" + Guid.NewGuid() + "_" + files.FileName + ".png";
            try
            {
                if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Content/ProfileImages")))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Content/ProfileImages"));
                var directory = HttpContext.Current.Server.MapPath("~/Content/ProfileImages");
                var fileInfo = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/Content/ProfileImages"));
                var fl = fileInfo.GetFiles().FirstOrDefault(x => x.FullName.StartsWith(userid));
                fl?.Delete();
                var stringpath = Path.Combine(directory, filename);
                var img =  ResizeImage(new Bitmap(files.InputStream), new Size(200, 200));
                img.Save(stringpath);
                var userId = Guid.Parse(HttpContext.Current.User.Identity.GetUserId());
                var user = SignIn.UserManager.FindById(userId);
                user.ProfilePicture = "/Content/ProfileImages/" + filename;
                SignIn.UserManager.Update(user);
                HttpContext.Current.Response.StatusCode = 200;
                return user.ProfilePicture;
            }
            catch (Exception e)
            {
                HttpContext.Current.Response.StatusCode = 401;
                return null;
            }  
        }

    }

}