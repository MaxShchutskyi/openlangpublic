﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.HelpersEntity;
using Microsoft.AspNet.Identity;
using WebGrease.Css.Extensions;

namespace Xencore.Controllers.api
{
    [Authorize(Roles = "Presenter")]
    public class MyExperienceController : ApiController
    {
        public Guid UserId => Guid.Parse(User.Identity.GetUserId());
        public object GetMyExperiance()
        {
            using (var context = new MyDbContext())
            {
                var user = context.Users.Find(UserId);
                return new
                {
                    educations = user.Educations.Select(x=>new ExpirienceHelper() {Id = x.Id, Description = x.Description, Title = x.Title, PeriodTo = x.PeriodTo.ToString(), PeriodFrom = x.PeriodFrom.ToString()}),
                    expiriences = user.WorkExperiences.Select(x => new ExpirienceHelper() { Id = x.Id, Description = x.Description, Title = x.Title, PeriodTo = x.PeriodTo.ToString(), PeriodFrom = x.PeriodFrom.ToString()}),
                    certifications = user.Certifications.Select(x => new ExpirienceHelper() { Id = x.Id, Description = x.Description, Title = x.Title, PeriodTo = x.PeriodTo.ToString(), PeriodFrom = x.PeriodFrom.ToString() })
                };
            }
        }
        [HttpPost]
        public async Task<object> SaveEducation(IEnumerable<ExpirienceHelper> educations )
        {
            if (!ModelState.IsValid)
                return new {error = "All fields are required of fields so long"};
            using (var context = new MyDbContext())
            {
                var educs = context.Users.Find(UserId).Educations;
                var deleted = educs.Where(x => educations.Where(f => f.Deleted).Select(f=>f.Id).Contains(x.Id)).ToList();
                context.Educations.RemoveRange(deleted);
                var undeleted =
                    educations.Where(x => x.Deleted == false).Select(x => new Education()
                    {
                        Id = x.Id, Description = x.Description, PeriodFrom = short.Parse(x.PeriodFrom), PeriodTo = short.Parse(x.PeriodTo), Title = x.Title, CustomUserId = UserId
                    }).ToList();
                undeleted.ForEach(x=> context.Educations.AddOrUpdate(x));
                await context.SaveChangesAsync();
                return new { educations = undeleted.Select(x => new ExpirienceHelper() { Id = x.Id, Description = x.Description, Title = x.Title, PeriodTo = x.PeriodTo.ToString(), PeriodFrom = x.PeriodFrom.ToString() }) };
            }
        }
        [HttpPost]
        public async Task<object> SaveExpiriences(IEnumerable<ExpirienceHelper> educations)
        {
            if (!ModelState.IsValid)
                return new { error = "All fields are required of fields so long" };
            using (var context = new MyDbContext())
            {
                var educs = context.Users.Find(UserId).WorkExperiences;
                var deleted = educs.Where(x => educations.Where(f => f.Deleted).Select(f => f.Id).Contains(x.Id)).ToList();
                context.WorkExperiences.RemoveRange(deleted);
                var undeleted =
                    educations.Where(x => x.Deleted == false).Select(x => new WorkExperience()
                    {
                        Id = x.Id,
                        Description = x.Description,
                        PeriodFrom = short.Parse(x.PeriodFrom),
                        PeriodTo = short.Parse(x.PeriodTo),
                        Title = x.Title,
                        CustomUserId = UserId
                    }).ToList();
                undeleted.ForEach(x => context.WorkExperiences.AddOrUpdate(x));
                await context.SaveChangesAsync();
                return new { educations = undeleted.Select(x => new ExpirienceHelper() { Id = x.Id, Description = x.Description, Title = x.Title, PeriodTo = x.PeriodTo.ToString(), PeriodFrom = x.PeriodFrom.ToString() }) };
            }
        }

        [HttpPost]
        public async Task<object> SaveCertifications(IEnumerable<ExpirienceHelper> educations)
        {
            if (!ModelState.IsValid)
                return new { error = "All fields are required of fields so long" };
            using (var context = new MyDbContext())
            {
                var educs = context.Users.Find(UserId).Certifications;
                var deleted = educs.Where(x => educations.Where(f => f.Deleted).Select(f => f.Id).Contains(x.Id)).ToList();
                context.Certifications.RemoveRange(deleted);
                var undeleted =
                    educations.Where(x => x.Deleted == false).Select(x => new Certification()
                    {
                        Id = x.Id,
                        Description = x.Description,
                        PeriodFrom = short.Parse(x.PeriodFrom),
                        PeriodTo = short.Parse(x.PeriodTo),
                        Title = x.Title,
                        CustomUserId = UserId
                    }).ToList();
                undeleted.ForEach(x => context.Certifications.AddOrUpdate(x));
                await context.SaveChangesAsync();
                return new { educations = undeleted.Select(x => new ExpirienceHelper() { Id = x.Id, Description = x.Description, Title = x.Title, PeriodTo = x.PeriodTo.ToString(), PeriodFrom = x.PeriodFrom.ToString() }) };
            }
        }
    }
}
