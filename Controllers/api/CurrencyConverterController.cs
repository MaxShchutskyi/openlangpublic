﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using CurrencyConverter;
using Domain.Context;
using Domain.Entities.HelpersEntity;
using DTO.Enums;
using Newtonsoft.Json;
using Xencore.Managers.TablesOfPrices;
using Xencore.Managers.TablesOfPrices.Abstract;

namespace Xencore.Controllers.api
{
    public class CurrencyConverterController : ApiController
    {
        [HttpGet]
        public object GetCurrentPrice(TypeLessons type, string area, Guid userId, string currency)
        {
            using (var context = new MyDbContext())
            {
                var user = context.Users.Find(userId);
                var prices = JsonConvert.DeserializeObject<PricesPerOneLesson>(user.PricesForAllTypes);
                var price = decimal.Parse(prices.GetType()
                    .GetProperty(type.ToString(), BindingFlags.Public | BindingFlags.Instance)
                    .GetValue(prices).ToString(), CultureInfo.InvariantCulture);
                var money = user.Money;
                decimal curFactory;
                decimal convertedMoney;
                if (currency != null)
                {
                    var factory = CurrencyConverterFactory.Create<string>();
                    curFactory = Math.Round(factory.Convert(currency, "EUR", price).Result, 2);
                    convertedMoney = Math.Round(factory.Convert(currency, "EUR", money).Result, 2);
                }
                else
                {
                    curFactory = price;
                    convertedMoney = money;
                }
                return new { pricePerLesson = price, curFactory, paymentId = 0, type, money, convertedMoney, isDisable = money < price };
            }
        }

        [HttpGet]
        public object GetCurrentPrices(string area, Guid userId, string currency)
        {
            using (var context = new MyDbContext())
            {
                var prices = JsonConvert.DeserializeObject<PricesPerOneLesson>(context.Users.Find(userId).PricesForAllTypes);
                if (currency != null)
                    prices = prices.ConvertToPrice(currency);
                return new { priceBasic = prices.Basic, pricePremium = prices.Premium, @group = prices.Group };
            }
            //var ar = CountryAnalizer.Analize(area);
            //using (var context = new MyDbContext())
            //{
            //    var res = context.PaymentTransactions.Where(x => x.AttendeeId == userId)
            //        .OrderByDescending(x => x.CreateDate).Take(1)
            //        .Include(x => x.OrderDetailsId).Include(x => x.CustomUserId).FirstOrDefault();
            //    if(res == null || res.OrderDetailsId.CountLessons == 1)
            //        return new { priceBasic = TableMaanger.GetPricePerLesson(TypeLessons.Basic, ar, 5)
            //            , pricePremium = TableMaanger.GetPricePerLesson(TypeLessons.Premium, ar, 5), group = TableMaanger.GetPricePerLesson(TypeLessons.Group, ar, 5)
            //        };
            //    var priceBasic = TableMaanger.GetPricePerLesson(TypeLessons.Basic, ar, res.OrderDetailsId.CountLessons);
            //    var pricePremium = TableMaanger.GetPricePerLesson(TypeLessons.Premium, ar, res.OrderDetailsId.CountLessons);
            //    var group = TableMaanger.GetPricePerLesson(TypeLessons.Group, ar, res.OrderDetailsId.CountLessons);
            //}
        }
        [HttpGet]
        public string ConvertMoney(string currency, decimal money)
        {
            if (string.IsNullOrEmpty(currency))
                return money.ToString();
            return
                Math.Round(CurrencyConverterFactory.Create<string>()
                    .Convert(currency, "EUR", money)
                    .Result,2).ToString(CultureInfo.InvariantCulture);
        }
    }
}
