﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Services;
using dotless.Core.Abstractions;
using Domain.Context;
using Xencore.Extentions;

namespace Xencore.Controllers.api
{
    public class TeacherScheduleController : ApiController
    {
        [System.Web.Http.HttpGet]
        public async Task<object> GetTeacherScheduleWithoutPast(Guid teacherId, int? week, string localtime)
        {
            week = GetCurrentWeek(week, localtime);
            var dateTime = DateTime.UtcNow.AddHours(5);
            using (var context = new MyDbContext())
            {
                var result = await 
                    context.Schedules
                        .Where(
                            x =>
                                x.CustomUserId == teacherId && x.WeekNumber == week &&
                                x.StartDateTimeDateTime > dateTime).Select(x => new
                        {
                            id = x.Id,
                            start = x.StartDateTime,
                            end = x.EndDateTime,
                            className = x.Status
                        }).ToListAsync();
                return Json(result);
            }
        }
        [System.Web.Http.HttpGet]
        public async Task<object> GetTeacherScheduleFullDetails(Guid teacherId, int? week, string localtime)
        {
            week = GetCurrentWeek(week, localtime);
            var year = DateTime.UtcNow.Year;
            using (var context = new MyDbContext())
            {
                var result = await
                    context.Schedules
                        .Where(
                            x =>
                                x.CustomUserId == teacherId && x.WeekNumber == week &&
                                x.StartDateTimeDateTime.Year == year).Select(x => new
                        {
                            title = x.ScheduleTransactions.Select(f => f.User.Name),
                            students =
                            x.ScheduleTransactions.Select(f => new {f.User, f.Credit})
                                .Select(
                                    f =>
                                        new
                                        {
                                            f.User.Id,
                                            f.User.ProfilePicture,
                                            f.User.Name,
                                            active = true,
                                            credit = f.Credit != null?
                                            new
                                            {
                                                f.Credit.Id,
                                                f.Credit.LeftLessons,
                                                Type = f.Credit.Type.ToString(),
                                                f.Credit.StudentId,
                                                f.Credit.PricePerLesson
                                            }:null
                                        }),
                            id = x.Id,
                            level = x.Level,
                            x.Type,
                            x.IsConfirmed,
                            x.Title,
                            language = x.Language,
                            start = x.StartDateTime,
                            end = x.EndDateTime,
                            className = x.Status
                        }).ToListAsync();
                var res2 = result.Select(x => new
                {
                    title = string.Join(",",x.title).ToShort(),
                    x.students,
                    x.id,
                    x.start,
                    x.level,
                    x.IsConfirmed,
                    x.language,
                    topic = x.Title,
                    x.end,
                    x.className,
                    type = x.Type.ToString()
                });
                return Json(res2);
            }
        }

        [System.Web.Http.HttpPost]
        public async Task<string> ConfirmLesson([FromBody]int scheduleId)
        {
            using (var context = new MyDbContext())
            {
                var sc = await context.Schedules.FindAsync(scheduleId);
                sc.IsConfirmed = true;
                sc.Status = "view1 " + sc.Type.ToString();
                await context.SaveChangesAsync();
                return sc.Status;
            }
        }

        [System.Web.Http.NonAction]
        private int GetCurrentWeek(int? week, string localtime)
        {
            if (week != null) return (int) week;
            var utc = DateTime.UtcNow;
            week = CultureInfo.GetCultureInfo("en-GB").Calendar.GetWeekOfYear(utc,
                CultureInfo.CurrentCulture.DateTimeFormat.CalendarWeekRule,
                DayOfWeek.Sunday);
            if (localtime.Contains("."))
            {
                var hours = localtime.Split('.');
                var local = int.Parse(hours[0]) * -1;
                if (hours[1].Length == 1)
                    hours[1] = hours[1] + "0";
                var min = (int.Parse(hours[1]) * 0.6) * -1;
                if (utc.DayOfWeek == DayOfWeek.Saturday && utc.AddHours(local).DayOfWeek == DayOfWeek.Sunday)
                    week++;
                if (utc.DayOfWeek == DayOfWeek.Saturday && utc.AddHours(local).AddMinutes(min).DayOfWeek == DayOfWeek.Sunday)
                    week++;

            }
            else
            {
                var local = int.Parse(localtime) * -1;
                if (utc.DayOfWeek == DayOfWeek.Saturday && utc.AddHours(local).DayOfWeek == DayOfWeek.Sunday)
                    week++;
            }
            return (int)week;
        }
    }
}
