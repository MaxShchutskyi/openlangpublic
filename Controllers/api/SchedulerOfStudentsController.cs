﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.HelpersEntity;
using Domain.Helpers;
using Domain.Repositories.Interfaces;
using EmailSenderService;
using Microsoft.AspNet.Identity;
using PushNotificationService;
using Xencore.Helpers;
using Xencore.Services;
using MessageStatus = Xencore.EmailSender.MessageStatus;

namespace Xencore.Controllers.api
{
    public class SchedulerOfStudentsController : ApiController
    {
        private Guid Id => Guid.Parse(User.Identity.GetUserId());
        private ISchedulerRepository ScheduleRepo { get; set; }

        public SchedulerOfStudentsController(ISchedulerRepository schedule)
        {
            ScheduleRepo = schedule;
        }
        [HttpGet]
        public IEnumerable<object> Get(Guid id, int? week, string localtime)
        {
            if (week == null)
            {
                var utc = DateTime.UtcNow;
                week = CultureInfo.GetCultureInfo("en-GB").Calendar.GetWeekOfYear(utc,
                    CultureInfo.CurrentCulture.DateTimeFormat.CalendarWeekRule,
                    DayOfWeek.Sunday);
                if (localtime.Contains("."))
                {
                    var hours = localtime.Split('.');
                    var local = int.Parse(hours[0]) * -1;
                    if (hours[1].Length == 1)
                        hours[1] = hours[1] + "0";
                    var min = (int.Parse(hours[1]) * 0.6) * -1;
                    if (utc.DayOfWeek == DayOfWeek.Saturday && utc.AddHours(local).DayOfWeek == DayOfWeek.Sunday)
                        week++;
                    if (utc.DayOfWeek == DayOfWeek.Saturday &&
                        utc.AddHours(local).AddMinutes(min).DayOfWeek == DayOfWeek.Sunday)
                        week++;

                }
                else
                {
                    var local = int.Parse(localtime) * -1;
                    if (utc.DayOfWeek == DayOfWeek.Saturday && utc.AddHours(local).DayOfWeek == DayOfWeek.Sunday)
                        week++;
                }
            }
            using (var context = new MyDbContext())
            {
                var sc =
                    context.ScheduleTransactions.Where(x => x.CustomUserId == id)
                        .Include(x => x.Schedule)
                        .Select(x => x.Schedule)
                        .Where(x => x.WeekNumber == week)
                        .Include(x => x.CustomUser).Select(x => new
                        {
                            id = x.Id,
                            className = x.Status,
                            start = x.StartDateTime,
                            end = x.EndDateTime,
                            courseType = x.Type,
                            teacherName = x.CustomUser.Name,
                            image = x.CustomUser.ProfilePicture,
                        }).ToList();
                return sc;
            }

        }
        [HttpPost]
        [Authorize(Roles = "Presenter")]
        public async Task<object> CancelLesson(UpdateLessonHelper helper)
        {
            var studentsIds = helper.Students.Select(x => x.Id).ToArray();
            using (var context = new MyDbContext())
            {
                var teacher = context.Users.Find(Id);
                if (!studentsIds.Any())
                {
                    var sc = new Schedule() { Id = helper.Id };
                    context.Schedules.Attach(sc);
                    context.Entry(sc).State = EntityState.Deleted;
                    await context.SaveChangesAsync();
                    return new { newMoney = new int[0] };
                }
                var users = await context.Users.Where(x => studentsIds.Contains(x.Id)).Include(x=>x.Credits).ToListAsync();
                var schedule = await context.Schedules.FindAsync(helper.Id);
                var client = new EmailSender.EmailSendClient();
                foreach (var user in helper.Students)
                {
                    var tran = schedule.ScheduleTransactions.First(x => x.CustomUserId == user.Id);
                    var selUser = users.First(x => x.Id == user.Id);
                    var credit = users.First(x => x.Id == user.Id).Credits.First(x=>x.Id == user.Credit.Id);
                    credit.LeftLessons += 1;
                    context.ScheduleTransactions.Remove(tran);
                    var messToStudent = $"Lesson with {teacher.Name} on {selUser.GetLocalTime(schedule.StartDateTimeDateTime)} was canceled";
                    var notificationSystem = new NotificationChatService(selUser.Id);
                    await notificationSystem.SendAsync(messToStudent).ConfigureAwait(false);
                    var htmlMessStudent = EmailGeneratorService.GetBodyWithText(messToStudent);
                    client.SendAsync(selUser.Id, "Cancel lesson", htmlMessStudent, EmailSender.MessageStatus.LessonCancel);
                    //client.SendAsync(selUser.Id, "Cancel lesson", messToStudent, MessageStatus.LessonCancel);
                    //var urlForStundet = MailSenderUrlGenerator.BuildUniversalTemplate(Request.RequestUri.Host,
                    //    messToStudent);
                    await
                        PushNotificationService.PushNotificationService.SendNotification(
                            new NotificationModel()
                            {
                                Title = "Canceled lesson",
                                Uri =
                                    Request.RequestUri.Scheme + "://" +
                                    Request.RequestUri.Authority + "/my-account#/mylessons",
                                Message = messToStudent,
                                Icon =
                                    Request.RequestUri.Scheme + "://" +
                                    Request.RequestUri.Authority + "/img/icon.png"
                            }, selUser.Id);
                    //client.SendHtmlAsync(selUser.Id, urlForStundet.ToString(), "Cancel Lesson", MessageStatus.LessonCreated);
                }
                var messToTeacher =
                      $"Lesson on {teacher.GetLocalTime(schedule.StartDateTimeDateTime)} was canceled";
                var ns = new NotificationChatService(teacher.Id);
                await ns.SendAsync(messToTeacher).ConfigureAwait(false);
                var htmlMessTeacher = EmailGeneratorService.GetBodyWithText(messToTeacher);
                client.SendAsync(teacher.Id, "Cancel lesson", htmlMessTeacher, EmailSender.MessageStatus.LessonCancel);
                //client.SendAsync(teacher.Id, "Cancel lesson", messToTeacher, MessageStatus.LessonCancel);
                context.Schedules.Remove(schedule);
                await context.SaveChangesAsync();
                var newMoney = users.Select(x => new { x.Id, x.Money });
                return new { newMoney };
            }
        }

        [HttpPost]
        [Authorize(Roles = "Presenter")]
        public async Task<bool> RemoveFreeLesson([FromBody]int id)
        {
            using (var context = new MyDbContext())
            {
                var schedule = new Schedule() { Id = id };
                context.Schedules.Attach(schedule);
                context.Entry(schedule).State = EntityState.Deleted;
                await context.SaveChangesAsync();
            }
            return true;
        }
        [HttpPost]
        [Authorize(Roles = "Presenter")]
        public async Task<bool> ChangeDate([FromBody] ChangeDateTimeLesson helper)
        {
            using (var context = new MyDbContext())
            {
                var teacher = context.Users.Find(Guid.Parse(User.Identity.GetUserId()));
                var scheduler = await context.Schedules.Where(x => x.Id == helper.Id).Include(x => x.ScheduleTransactions.Select(f => f.User)).FirstOrDefaultAsync();
                if (scheduler.ScheduleTransactions.Any())
                {
                    foreach (var selUser in scheduler.ScheduleTransactions.Select(x => x.User))
                    {
                        var messToTeacher =
                            $"{selUser.Name} have just rescheduled a {Resources.Resource.ResourceManager.GetString(scheduler.Language)} lesson with you. The lesson on {teacher.GetLocalTime(scheduler.StartDateTimeDateTime)} has now been replaced by a lesson on {teacher.GetLocalTime(DateTime.Parse(helper.StartDateTime.Replace('T', ' ').Split('Z')[0]))}. Please visit your profile to confirm the  new lesson.";
                        var messToStudent =
                            $"You have just rescheduled a {Resources.Resource.ResourceManager.GetString(scheduler.Language)} lesson with {teacher.Name}. The lesson on {selUser.GetLocalTime(scheduler.StartDateTimeDateTime)} has now been replaced by a lesson on {teacher.GetLocalTime(DateTime.Parse(helper.StartDateTime.Replace('T', ' ').Split('Z')[0]))}";
                        var notificationSystem = new NotificationChatService(selUser.Id, teacher.Id);
                        await notificationSystem.SendAsync(messToTeacher, messToStudent).ConfigureAwait(false);
                        var client = new EmailSender.EmailSendClient();
                        //client.SendAsync(selUser.Id, "Rescheduled lesson", messToStudent, MessageStatus.LessonChanged);
                        //client.SendAsync(teacher.Id, "Rescheduled lesson", messToTeacher, MessageStatus.LessonChanged);
                        var urlForTeacher =
                            MailSenderUrlGenerator.BuildUniversalTemplate(Request.RequestUri.Host,
                                messToTeacher);
                        client.SendHtmlAsync(teacher.Id, urlForTeacher.ToString(), "Rescheduled lesson", EmailSender.MessageStatus.LessonCreated);
                        var urlForStudent =
                            MailSenderUrlGenerator.BuildUniversalTemplate(Request.RequestUri.Host,
                                messToStudent);
                        client.SendHtmlAsync(selUser.Id, urlForStudent.ToString(), "Rescheduled lesson", EmailSender.MessageStatus.LessonCreated);
                    }
                }
                scheduler.StartDateTimeDateTime = DateTime.Parse(helper.StartDateTime.Replace('T', ' ').Split('Z')[0]);
                scheduler.StartDateTime = helper.StartDateTime;
                scheduler.EndDateTime = helper.EndDateTime;
                await context.SaveChangesAsync();
            }
            return true;
        }
        [HttpPost]
        [Authorize(Roles = "Presenter")]
        public async Task<object> ClassNotHappenForTeacher(UpdateLessonHelper helper)
        {
            var currentUserId = Guid.Parse(User.Identity.GetUserId());
            var studentId = helper.Students.FirstOrDefault()?.Id;
            if(studentId == null)
                return new { Error = $"You can't do this, because class has no students" };
            var currentDate = DateTime.UtcNow;
            //var studentsIds = helper.Students.Select(x => x.Id).ToArray();
            using (var context = new MyDbContext())
            {
                var scheduler = context.Schedules.Find(helper.Id);
                if (scheduler.ClassIsNotHappen)
                    return new { Error = $"This class already not happned" };
                var teacher = context.Users.Find(currentUserId);
                var student = context.Users.Find(studentId);
                var credit = await context.Credits.Where(x => x.StudentId == studentId && x.Type == helper.Type && x.EndDate > currentDate)
                    .OrderByDescending(x => x.EndDate).FirstOrDefaultAsync();
                if (credit == null)
                    return new { Error = $"Student have no active credits with the {helper.Type.ToString()} type. Please write our admin" };
                credit.LeftLessons++;
                scheduler.ClassIsNotHappen = true;
                scheduler.Description = helper.Problem;
                scheduler.Status += " nothappen";
                await context.SaveChangesAsync();
                var messToStudent = EmailGeneratorService.GetBodyWithText($"Lesson with {teacher.Name} on {student.GetLocalTime(scheduler.StartDateTimeDateTime)} was not happen with problem - {helper.Problem}");
                var messToTeacher = EmailGeneratorService.GetBodyWithText($"Lesson with {student.Name} on {teacher.GetLocalTime(scheduler.StartDateTimeDateTime)} was not happen with problem - {helper.Problem}");
                var messToAdmin =
                    $"Teacher - {teacher.Name} canceled lessons. Lesson with student {student.Name} and teacher {teacher.Name} on {scheduler.StartDateTimeDateTime:F} UTC  reported that the class didn't happen with problem - {helper.Problem}";
                var client = new EmailSender.EmailSendClient();
                await client.SendAsync(student.Id, "Class not happen", messToStudent, EmailSender.MessageStatus.LessonCancel);
                await client.SendAsync(teacher.Id, "Class not happen", messToTeacher, EmailSender.MessageStatus.LessonCancel);
                await client.SendForAdminAsync(messToAdmin, "Class not happen", EmailSender.MessageStatus.LessonCancel);
                return new { Success = true };
            }
        }
        
        [HttpPost]
        [Authorize]
        public async Task<object> ClassNotHappen(UpdateLessonHelper helper)
        {
            var currentUserId = Guid.Parse(User.Identity.GetUserId());
            var currentDate = DateTime.UtcNow;
            //var studentsIds = helper.Students.Select(x => x.Id).ToArray();
            using (var context = new MyDbContext())
            {
                var scheduler = context.Schedules.Find(helper.Id);
                if(scheduler.ClassIsNotHappen)
                    return new { Error = $"This class already not happned" };
                var teacher = context.Users.Find(scheduler.CustomUserId);
                var student = context.Users.Find(currentUserId);
                var credit = await context.Credits.Where(x => x.StudentId == currentUserId && x.Type == helper.Type && x.EndDate> currentDate)
                    .OrderByDescending(x => x.EndDate).FirstOrDefaultAsync();
                if (credit == null)
                    return new {Error = $"You have no active credits with the {helper.Type.ToString()} type"};
                credit.LeftLessons++;
                scheduler.ClassIsNotHappen = true;
                scheduler.Description = helper.Problem;
                scheduler.Status += " nothappen";
                await context.SaveChangesAsync();
                var messToStudent = EmailGeneratorService.GetBodyWithText($"Lesson with {teacher.Name} on {student.GetLocalTime(scheduler.StartDateTimeDateTime)} was not happen with problem - {helper.Problem}");
                var messToTeacher = EmailGeneratorService.GetBodyWithText($"Lesson with {student.Name} on {teacher.GetLocalTime(scheduler.StartDateTimeDateTime)} was not happen with problem - {helper.Problem}");
                var messToAdmin =
                    $"Student - {student.Name} canceled lessons. Lesson with student {student.Name} and teacher {teacher.Name} on {scheduler.StartDateTimeDateTime:F} UTC  reported that the class didn't happen with problem - {helper.Problem}";
                var client = new EmailSender.EmailSendClient();
                await client.SendAsync(student.Id, "Class not happen", messToStudent, EmailSender.MessageStatus.LessonCancel);
                await client.SendAsync(teacher.Id, "Class not happen", messToTeacher, EmailSender.MessageStatus.LessonCancel);
                await client.SendForAdminAsync(messToAdmin, "Class not happen", EmailSender.MessageStatus.LessonCancel);
                return new {Success = true};
                //var teacher = context.Users.Find(Id);
                //if (!studentsIds.Any())
                //{
                //    var sc = new Schedule() { Id = helper.Id };
                //    context.Schedules.Attach(sc);
                //    context.Entry(sc).State = EntityState.Deleted;
                //    await context.SaveChangesAsync();
                //    return new { newMoney = new int[0] };
                //}
                //var users = await context.Users.Where(x => studentsIds.Contains(x.Id)).Include(x => x.Credits).ToListAsync();
                //var schedule = await context.Schedules.FindAsync(helper.Id);
                //var client = new EmailSender.EmailSendClient();
                //foreach (var user in helper.Students)
                //{
                //    var tran = schedule.ScheduleTransactions.First(x => x.CustomUserId == user.Id);
                //    var selUser = users.First(x => x.Id == user.Id);
                //    var credit = users.First(x => x.Id == user.Id).Credits.First(x => x.Id == user.Credit.Id);
                //    credit.LeftLessons += 1;
                //    context.ScheduleTransactions.Remove(tran);
                //    var messToStudent = $"Lesson with {teacher.Name} on {selUser.GetLocalTime(schedule.StartDateTimeDateTime)} was canceled";
                //    var notificationSystem = new NotificationChatService(selUser.Id);
                //    await notificationSystem.SendAsync(messToStudent).ConfigureAwait(false);
                //    var htmlMessStudent = EmailGeneratorService.GetBodyWithText(messToStudent);
                //    client.SendAsync(selUser.Id, "Cancel lesson", htmlMessStudent, EmailSender.MessageStatus.LessonCancel);
                //    //client.SendAsync(selUser.Id, "Cancel lesson", messToStudent, MessageStatus.LessonCancel);
                //    //var urlForStundet = MailSenderUrlGenerator.BuildUniversalTemplate(Request.RequestUri.Host,
                //    //    messToStudent);
                //    await
                //        PushNotificationService.PushNotificationService.SendNotification(
                //            new NotificationModel()
                //            {
                //                Title = "Canceled lesson",
                //                Uri =
                //                    Request.RequestUri.Scheme + "://" +
                //                    Request.RequestUri.Authority + "/my-account#/mylessons",
                //                Message = messToStudent,
                //                Icon =
                //                    Request.RequestUri.Scheme + "://" +
                //                    Request.RequestUri.Authority + "/img/icon.png"
                //            }, selUser.Id);
                //    //client.SendHtmlAsync(selUser.Id, urlForStundet.ToString(), "Cancel Lesson", MessageStatus.LessonCreated);
                //}
                //var messToTeacher =
                //      $"Lesson on {teacher.GetLocalTime(schedule.StartDateTimeDateTime)} was canceled";
                //var ns = new NotificationChatService(teacher.Id);
                //await ns.SendAsync(messToTeacher).ConfigureAwait(false);
                //var htmlMessTeacher = EmailGeneratorService.GetBodyWithText(messToTeacher);
                //client.SendAsync(teacher.Id, "Cancel lesson", htmlMessTeacher, EmailSender.MessageStatus.LessonCancel);
                ////client.SendAsync(teacher.Id, "Cancel lesson", messToTeacher, MessageStatus.LessonCancel);
                //context.Schedules.Remove(schedule);
                //await context.SaveChangesAsync();
                //var newMoney = users.Select(x => new { x.Id, x.Money });
                //return new { newMoney };
            }
        }
        
        [HttpPost]
        [Authorize(Roles = "Presenter")]
        public async Task<object> CopyCurrentWeek([FromBody]short curWeek)
        {
            //var timezone = short.Parse(Request.Headers.GetCookies()[0].Cookies.First(x => x.Name == "timezone").Value);
            var userId = Guid.Parse(User.Identity.GetUserId());
            //var date = DateTime.UtcNow.AddMinutes(timezone);
            //var week = CultureInfo.GetCultureInfo("en-GB").Calendar.GetWeekOfYear(date,
            //        CultureInfo.CurrentCulture.DateTimeFormat.CalendarWeekRule,
            //        DayOfWeek.Sunday);
            var week = curWeek;
            var schedules = (await ScheduleRepo.GetSchedulesForCopyWeekAsync(userId, (byte)curWeek)).ToList();
            var schhedulesCurrentWeek = schedules.Where(x => x.WeekNumber == week).Select(x => new Schedule()
            {
                WeekNumber = (byte)(week + 1),
                StartDateTimeDateTime = x.StartDateTimeDateTime.AddDays(7),
                TimeStart = x.TimeStart,
                Status = "view1",
                DayOfTheWeek = x.DayOfTheWeek,
                IsConfirmed = false,
                CustomUserId = userId,
                StartDateTime = x.StartDateTimeDateTime.AddDays(7).ToString("O") + "Z+00:00",
                EndDateTime = x.StartDateTimeDateTime.AddDays(7).AddHours(1).ToString("O") + "Z+00:00"
            }).Where(x => schedules.Where(f => f.WeekNumber == week + 1).All(f => f.StartDateTimeDateTime != x.StartDateTimeDateTime)).ToArray();
            ScheduleRepo.Add(schhedulesCurrentWeek);
            await ScheduleRepo.SaveChangesAsync();
            return new
            {
                schedules = schhedulesCurrentWeek.Select(x => new
                {
                    id = x.Id,
                    start = x.StartDateTime,
                    end = x.EndDateTime,
                    className = x.Status,
                    x.IsConfirmed,
                    language = x.Language,
                    type = x.Type,
                    topic = x.Title,
                    level = x.Level,
                    title = "",
                    students = new object[] { }
                })
            };
        }

        [HttpPost]
        [Authorize(Roles = "Presenter")]
        public async Task<object> UpdateLesson(UpdateLessonHelper helper)
        {
            var teacherId = Guid.Parse(User.Identity.GetUserId());
            var studentsIds = helper.Students.Select(x => x.Id).ToArray();
            using (var context = new MyDbContext())
            {
                var users = await context.Users.Where(x => studentsIds.Contains(x.Id)).Include(x=>x.Credits).ToListAsync();
                var teacher = context.Users.Find(teacherId);
                var schedule = await context.Schedules.FindAsync(helper.Id);
                foreach (var user in helper.Students)
                {
                    if (!user.Active)
                    {
                        var credit = users.First(x => x.Id == user.Id).Credits.First(x => x.Id == user.Credit.Id);
                        var tran = schedule.ScheduleTransactions.First(x => x.CustomUserId == user.Id);
                        var selUser = users.First(x => x.Id == user.Id);
                        credit.LeftLessons += 1;
                        context.ScheduleTransactions.Remove(tran);
                        var messToTeacher =
                            $"{selUser.Name} has just cancelled a {Resources.Resource.ResourceManager.GetString(helper.Language)} lesson with you on {teacher.GetLocalTime(schedule.StartDateTimeDateTime)}";
                        var messToStudent = $"You have just cancelled a {Resources.Resource.ResourceManager.GetString(helper.Language)} lesson with {teacher.Name} on {selUser.GetLocalTime(schedule.StartDateTimeDateTime)}";
                        var messToAdmin =
                            $"{selUser.Name} has just cancelled a {Resources.Resource.ResourceManager.GetString(helper.Language)} lesson with {teacher.Name} on {schedule.StartDateTimeDateTime} UTC time";
                        await
                            PushNotificationService.PushNotificationService.SendNotification(
                                new NotificationModel()
                                {
                                    Title = "Canceled lesson",
                                    Uri = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/my-account#/mylessons",
                                    Message = messToStudent,
                                    Icon = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/img/icon.png"
                                }, selUser.Id);
                        var notificationSystem = new NotificationChatService(selUser.Id, teacherId);
                        await notificationSystem.SendAsync(messToTeacher, messToStudent).ConfigureAwait(false);
                        var client = new EmailSender.EmailSendClient();
                        var bodyToTeacher = EmailGeneratorService.GetBodyWithText(messToTeacher);
                        var bodyToStudent = EmailGeneratorService.GetBodyWithText(messToStudent);
                        var bodyToAdmin = EmailGeneratorService.GetBodyWithText(messToAdmin);
                        client.SendAsync(teacher.Id, "Remove student", bodyToTeacher, EmailSender.MessageStatus.RemoveStudentFromLesson);
                        client.SendAsync(selUser.Id, "Remove student", bodyToStudent,
                            EmailSender.MessageStatus.RemoveStudentFromLesson);
                        client.SendForAdminAsync(bodyToAdmin, "Remove student",EmailSender.MessageStatus.RemoveStudentFromLesson);
                        continue;
                    }
                    if (user.Active && user.Added)
                    {
                        var selUser = users.First(x => x.Id == user.Id);
                        var credit = users.First(x => x.Id == user.Id).Credits.First(x=>x.Id == user.Credit.Id);
                        var pt = new ScheduleTransaction() {CreditId = credit.Id, CreatedDate = DateTime.UtcNow, CustomUserId = selUser.Id, ScheduleId = schedule.Id, Price = credit.PricePerLesson, Language = "Obsolete" };
                        credit.LeftLessons -= 1;
                        context.ScheduleTransactions.Add(pt);
                        var messToTeacher =
                            $"{selUser.Name} has just booked a {Resources.Resource.ResourceManager.GetString(helper.Language)} lesson with you on {teacher.GetLocalTime(schedule.StartDateTimeDateTime)}. Please visit your profile to confirm the lesson.";
                        var messToAdmin =
                            $"{selUser.Name} has just booked a {Resources.Resource.ResourceManager.GetString(helper.Language)} lesson with {teacher.Name} on {schedule.StartDateTimeDateTime} UTC time. Please visit your profile to confirm the lesson.";
                        var messToStudent = $"You have just booked a {Resources.Resource.ResourceManager.GetString(helper.Language)} lesson for {selUser.GetLocalTime(schedule.StartDateTimeDateTime)} with {teacher.Name}";
                        await
                            PushNotificationService.PushNotificationService.SendNotification(
                                new NotificationModel()
                                {
                                    Title = "Booked lesson",
                                    Uri = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/my-account#/mylessons",
                                    Message = messToStudent,
                                    Icon = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/img/icon.png"
                                }, selUser.Id);
                        var notificationSystem = new NotificationChatService(selUser.Id, teacherId);
                        await notificationSystem.SendAsync(messToTeacher, messToStudent).ConfigureAwait(false);
                        var client = new EmailSender.EmailSendClient();
                        var bodyToTeacher = EmailGeneratorService.GetBodyWithText(messToTeacher);
                        var bodyToStudent = EmailGeneratorService.GetBodyWithText(messToStudent);
                        var bodyToAdmin = EmailGeneratorService.GetBodyWithText(messToAdmin);
                        client.SendAsync(teacher.Id, "Booking lesson", bodyToTeacher, EmailSender.MessageStatus.LessonCreated);
                        client.SendAsync(selUser.Id, "Booking lesson", bodyToStudent,
                            EmailSender.MessageStatus.LessonCreated);
                        client.SendForAdminAsync(bodyToAdmin, "Booking lesson", MessageStatus.Empty);
                        continue;
                    }
                    if (user.Active && !user.Added && schedule.Type != helper.Type)
                    {
                        var credit = users.First(x => x.Id == user.Id).Credits.First(x => x.Id == user.Credit.Id);
                        var oldCredit =
                            users.First(x => x.Id == user.Id)
                                .Credits.OrderByDescending(x => x.EndDate)
                                .First(x => x.Type == schedule.Type);
                        var tran = schedule.ScheduleTransactions.First(x => x.CustomUserId == user.Id);
                        var selUser = users.First(x => x.Id == user.Id);
                        tran.Price = credit.PricePerLesson;
                        oldCredit.LeftLessons += 1;
                        credit.LeftLessons -= 1;
                        var messToStudent = $"Type of scheduler changed -  {schedule.StartDateTimeDateTime:f} UTC. Level - {Resources.Resource.ResourceManager.GetString(schedule.Level)}. Language - {Resources.Resource.ResourceManager.GetString(helper.Language)}";
                        var notificationSystem = new NotificationChatService(selUser.Id, teacherId);
                        await notificationSystem.SendAsync(null, messToStudent).ConfigureAwait(false);;
                        var bodyToStudent = EmailGeneratorService.GetBodyWithText(messToStudent);
                        var client = new EmailSender.EmailSendClient();
                        client.SendAsync(selUser.Id, "Type changed", bodyToStudent,
                            EmailSender.MessageStatus.LessonCreated);
                    }

                }
                schedule.Type = helper.Type;
                schedule.Language = helper.Language;
                schedule.Level = helper.Level;
                schedule.Title = helper.Topic;
                schedule.Status = "view1 " + schedule.Type;
                schedule.IsConfirmed = true;
                await context.SaveChangesAsync();
                var newMoney = users.Select(x => new { x.Id, x.Money });
                return new { newMoney, status = schedule.Status };
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            ScheduleRepo.Dispose();
        }
    }
}
