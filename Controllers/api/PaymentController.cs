﻿using System;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using CurrencyConverter;
using Domain.Context;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;
using Domain.Helpers;
using DTO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PaymentService;
using PaymentService.Models;
using Repositories.Abstract;
using Xencore.Helpers;
using EmailSenderService;
using EmailSenderService.Converters;
using EmailSenderService.Models;

namespace Xencore.Controllers.api
{
    [Authorize]
    public class PaymentController : ApiController
    {
        public Guid UserId => Guid.Parse(HttpContext.Current.User.Identity.GetUserId());
        private IPaymentRepository PaymentRepo { get; }

        public PaymentController(IPaymentRepository paymentRepo)
        {
            PaymentRepo = paymentRepo;
        }
        [HttpPost]
        public object PayPalPayment(ReplenishHelper helper)
        {
            var builder = new PaymentBuilder();
            var returnUrl = HttpContext.Current.Request.Url.Scheme + "://" +
                            HttpContext.Current.Request.Url.Authority +
                            $"/PayPalPayment/{helper.TypeOfPrice}PayPalNewResponse";
            var cancelUrl = HttpContext.Current.Request.Url.Scheme + "://" +
                         HttpContext.Current.Request.Url.Authority +
                         "/my-account#/makepayment";
            var retUrl = HttpContext.Current.Request.UrlReferrer?.ToString();
            var mn = helper.TypeOfPrice == "Membership" ? helper.Money : helper.Money - helper.Bonus;
            var result = builder.UseRemoteProvider().SetCallbackUrl(returnUrl, cancelUrl,
                new
                {
                    retUrl = retUrl,
                    payer = UserId,
                    eurPrice = helper.EurMoney,
                    eurbonus = helper.EurBonus,
                    discount = helper.Discount,
                    amount = helper.Money - helper.Bonus,
                    bonus = helper.Bonus,
                    duration = helper.Duration,
                    countLessons = helper.CountLessons,
                    timeOfLessons = helper.TimeOfLesson,
                    type = helper.TypeOfLessons,
                    typeOfPrice = helper.TypeOfPrice,
                    currency = helper.Currency
                }).UseSubscription(helper.TypeOfPrice == "Membership").FirstFeeWithDiscount(helper.Money - helper.Bonus).Execute(helper.Currency, mn);
            var cache = MemoryCache.Default;
            if (result.SubscriptionId != null)
                cache.Add(result.SubscriptionId, result.PlanId, DateTimeOffset.UtcNow.AddMinutes(20));
            return new { path = result.Link };
        }
        public async Task<object> CreditCardPayment(PaymentCreaterDto helper)
        {
            (var user, var resid) = await ExecutePayment(helper);
            if (resid == null || !string.IsNullOrEmpty(resid.Error))
                return new { error = true };
            using (var payManager = new PaymentManager.PaymentManager(PaymentRepo))
                await payManager.AddNewPayment(helper, user.Id, resid);
            user.InitializeCurrentCurrency(helper.Currency);
            await HttpContext.Current.GetOwinContext().Get<CustomUserManager>().UpdateAsync(user);
            var modeInNative = user.ConvertTo();
            var delta = CurrencyConverterFactory.Create<string>();
            var dt = delta.Convert(helper.Currency, "EUR").Result;
            var emailSender = new EmailSender.EmailSendClient();
            emailSender.SendHtmlAsync(user.Id,
                EmailGeneratorService.GetBody(new ConverterFromPaymantDtoToPamentCompleteModel().Convert(helper)),
                "Your Open Languages Lessons Schedule", EmailSender.MessageStatus.PaymentCompleted);
            emailSender.SendHtmlForAdminAsync(
                EmailGeneratorService.GetBody(new ConverterFromPaymentDtoToAdminPaymentComplete(user.Name, user.Email)
                    .Convert(helper)), "Student has been topped up balance", EmailSender.MessageStatus.Empty);
            return new { user.Money, NativeMoney = modeInNative + " " + helper.Currency, Currency = helper.Currency, delta = dt };
        }
        [NonAction]
        private async Task<(CustomUser user, TotalPaymentResult resid)> ExecutePayment(PaymentCreaterDto helper)
        {
            var manager = HttpContext.Current.GetOwinContext().Get<CustomUserManager>();
            var user = await manager.FindByIdAsync(UserId);
            var builder = new PaymentBuilder();
            var mn = helper.TypeOfPrice == "Membership" ? helper.Money : helper.Money - helper.Bonus;
            var resid = builder
                .UseCreditCard(new CreditCardInfo()
                {
                    Name = helper.Username,
                    CardNumber = helper.Card, //Card.CardNumber,
                    ExpMonth = helper.Month,
                    ExpYear = helper.Year, //int.Parse(year),
                    Cvc = helper.Cvc.ToString() //cvvnumber
                })
                .FirstFeeWithDiscount(helper.Money - helper.Bonus)
                .UseSubscription(helper.TypeOfPrice == "Membership", user.Email, "Openlanguages subscription")
                .Execute(helper.Currency, mn);
            return (user, resid);
        }

        protected override void Dispose(bool disposing)
        {
            PaymentRepo?.Dispose();
            base.Dispose(disposing);
        }

        [HttpPost]
        public bool PayPal_Unsubscribe(Subscr cubscr)
        {
            var builder = new PaymentBuilder();
            var state = builder.UseRemoteProvider().Unsubscribe(cubscr.PlanId);
            if (!state)
                return false;
            using (var context = new MyDbContext())
            {
                var plan = context.StripeSubscriptions.First(x => x.PlanId == cubscr.PlanId);
                plan.Active = false;
                context.SaveChanges();
            }
            return true;
        }
        [HttpPost]
        public bool CreditCard_Unsubscribe(Subscr cubscr)
        {
            var builder = new PaymentBuilder();
            var state = builder.UseCreditCard(null).Unsubscribe(cubscr.SubscribeId, cubscr.PlanId, cubscr.CustomerId);
            if (!state)
                return false;
            using (var context = new MyDbContext())
            {
                var plan = context.StripeSubscriptions.First(x => x.PlanId == cubscr.PlanId);
                plan.Active = false;
                context.SaveChanges();
            }
            return true;
        }
    }

    public class Subscr
    {
        public string SubscribeId { get; set; }
        public string PlanId { get; set; }
        public string CustomerId { get; set; }
    }
}