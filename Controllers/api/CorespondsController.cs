﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Domain.Entities;
using Domain.Entities.IdentityEntities;
using Domain.Helpers;
using Domain.Repositories.Interfaces;
using EmailSenderService;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Xencore.Helpers;
using SettingsAppHepler = Xencore.Helpers.SettingsAppHepler;

namespace Xencore.Controllers.api
{
    [Authorize(Roles = "Presenter")]
    public class CorespondsController : ApiController
    {
        private CustomUserManager Manager => HttpContext.Current.GetOwinContext().Get<CustomUserManager>();
        private ICorrespondRepository Repo { get; set; }

        public CorespondsController(ICorrespondRepository repo)
        {
            Repo = repo;
        }
        public async Task<object> GetAllCorrespons()
        {
            return await Repo.GetAllCorrespond(Guid.Parse(User.Identity.GetUserId()));
        }
        [HttpPost]
        public async Task<object> SendMessage(Correspond correspond)
        {
            correspond.TeacherId = Guid.Parse(User.Identity.GetUserId());
            var teacher = await Manager.FindByIdAsync(correspond.TeacherId);
            Repo.Add(new[] {correspond});
            await Repo.SaveChangesAsync();
            var emailservice = new NativeEmailService.NativeEmailService();
            //var client = new EmailSender.EmailSendClient();
            var urlForTeacher = MailSenderUrlGenerator.BuildAnswerForCorrespond(Request.RequestUri.Host, teacher.Name,
                correspond.Text);
            using (var client = new HttpClient())
            {
                var result = client.GetStringAsync(urlForTeacher.ToString());
                Task.WaitAll(result);
                await emailservice.SendAsync(new IdentityMessage()
                {
                    Body = result.Result,
                    Subject = correspond.Subject,
                    Destination = correspond.Email
                });
            }
            //client.SendHtmlAsync(teacher.Id, urlForTeacher.ToString(), correspond.Subject, MessageStatus.Empty);
            await
                emailservice.SendAsync(new IdentityMessage()
                {
                    Body =
                        $"Teacher {teacher.Name} ({teacher.Email}) send message to {correspond.Name} ({correspond.Email}) following message: <br><br><br> {correspond.Text}",
                    Destination = SettingsAppHepler.GetAdminEmail(),
                    Subject = "Teacher send email to"
                });
            return true;
        }

        protected override void Dispose(bool disposing)
        {
            Repo.Dispose();
            base.Dispose(disposing);
        }
    }
}
