﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.HelpersEntity;
using Microsoft.AspNet.Identity;

using Xencore.Entities;

namespace Xencore.Controllers.api
{
    [Authorize]
    public class MyWordsController : ApiController
    {
        public Guid UserId => Guid.Parse(User.Identity.GetUserId());
        [HttpGet]
        public async Task<IEnumerable<WordHelper>> GetMyWords(int page, string filter)
        {
            using (var context = new MyDbContext())
            {
                var listWords =
                    context.MyWords.Where(x => x.CustomUserId == UserId);
                if(!string.IsNullOrEmpty(filter) && filter!= "undefined")
                    listWords = listWords.Where(x=>x.Word.Contains(filter));
                var res = await listWords.OrderByDescending(x=>x.Id)
                            .Skip(page*5)
                            .Take(5)
                            .ToListAsync();
                return
                    res.Select(
                        x =>
                            new WordHelper()
                            {
                                Id = x.Id,
                                CustomUserId = x.CustomUserId,
                                Description = x.Description,
                                Language = x.Language,
                                Image = x.Image,
                                AudioPath = x.AudioPath,
                                Word = x.Word
                            });
            }
        }

        public async Task<WordHelper> SaveChanges(WordHelper helper)
        {
            var userId = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                if (helper.Id == -1)
                {
                    var newWord = new MyWords() {CustomUserId = userId, Description = helper.Description, Word = helper.Word, Language = helper.Language, Image = helper.Image};
                    context.MyWords.Add(newWord);
                    await context.SaveChangesAsync();
                    helper.Id = newWord.Id;
                    return helper;
                }
                var word = await context.MyWords.FindAsync(helper.Id);
                word.Description = helper.Description;
                word.Word = helper.Word;
                word.Language = helper.Language;
                await context.SaveChangesAsync();
                return helper;
            }
        }

        public async Task<WordHelper> RemoveMessage(WordHelper helper)
        {
            var directory = HttpContext.Current.Server.MapPath("~/");
            using (var context = new MyDbContext())
            {
                var word = await context.MyWords.FindAsync(helper.Id);
                if(File.Exists(Path.Combine(directory, word.AudioPath)))
                    File.Delete(Path.Combine(directory, word.AudioPath));
                word.AudioPath = helper.AudioPath = null;
                await context.SaveChangesAsync();
            }
            return helper;
        }
        //[HttpPost]
        //public async Task<IEnumerable<MyWords>> PostGetMyWords([FromBody]FilterWordsHelper helper)
        //{
        //    using (var context = new MyDbContext())
        //    {
        //        var listWords =
        //            context.MyWords.Where(
        //                x => x.CustomUserId == helper.Id);
        //        if (!string.IsNullOrEmpty(helper.Filter))
        //            listWords = listWords.Where(x => x.Description.Contains(helper.Filter) || x.Word.Contains(helper.Filter));
        //        if (!string.IsNullOrEmpty(helper.Lang))
        //            listWords = listWords.Where(x => x.Language.Contains(helper.Lang));
        //        listWords = (await listWords.OrderByDescending(x=>x.Id).Skip((int)helper.Page*5).Take(5).ToListAsync()).Select(x => new MyWords()
        //        {
        //            Id = x.Id,
        //            CustomUserId = x.CustomUserId,
        //            Language = x.Language,
        //            Word = x.Word,
        //            Description = x.Description,
        //            Image = x.Image,
        //            AudioPath = x.AudioPath
        //        }).AsQueryable();
        //        return listWords.ToList();
        //    }
        //}
        [HttpPut]
        public async Task<int> AddMyWord([FromBody]MyWords myword)
        {
            if (!ModelState.IsValid) return -1;
            using (var context = new MyDbContext())
            {
                //if (myword.Id != 0)
                //{
                //    context.MyWords.AddOrUpdate(x => x.Id, myword);
                //    var word = await context.MyWords.FirstOrDefaultAsync(x => x.Id == myword.Id);
                //    if (word.AudioPath != null && word.AudioPath == myword.AudioPath)
                //        File.Delete(System.Web.Hosting.HostingEnvironment.MapPath(word.AudioPath));
                //}
                context.MyWords.AddOrUpdate(x => x.Id, myword);
                await context.SaveChangesAsync();
                return myword.Id;
            }
        }
    }
}
