﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace Xencore.Controllers.api
{
    public class Obj
    {
        public byte[] Auth { get; set; }
        public byte[] Ph { get; set; }
        public string Endpoint { get; set; }
    }
    [Authorize]
    public class TestPushNotificationController : ApiController
    {
        [HttpPost]
        public async Task<object> Unsubscribe([FromBody]string endpoint)
        {
            await PushNotificationService.PushNotificationService.Unregister(endpoint);
            return "OK";
        }
        [HttpPost]
        public async Task<object> Subscribe(Obj obj)
        {
            var id = Guid.Parse(User.Identity.GetUserId());
            await PushNotificationService.PushNotificationService.Register(id, obj.Endpoint, obj.Auth, obj.Ph);
            return "OK";
            //if (obj == null)
            //    return "NO";
            //var auth = Convert.ToBase64String(obj.Auth);
            //var ph = Convert.ToBase64String(obj.Ph);
            //var push = new PushSubscription(obj.Endpoint, ph, auth);
            //var vapidDetails = new VapidDetails("mailto:pedagog-88@mail.ru",
            //    "BD0zXgf46NcgieZufIS5YyD2Si8fVSOoUbeWAF2LAx6qxFQ1iCqPQHuPruykaxUkPI1UJ0X5zbBY4B1dPptLFqw",
            //    "WwnAB7juZcn0oUooaLTYsGuDGwmZs6jESh9SzCk9r0U");
            //var webPushClient = new WebPushClient();
            //try
            //{
            //    webPushClient.SendNotification(push, JsonConvert.SerializeObject(
            //new { data = new {title = "Portugal vs. Denmark", text = "5 to 1"}}), vapidDetails);
            //    return "OK";
            //}
            //catch (Exception e)
            //{
            //    return "NO";
            //}
            ////    var lst = attr.Split('/').Last();
            //    var client = GetClient(attr);
            //    try
            //    {
            //        var res = await client.PostAsync(client.BaseAddress.ToString(),new StringContent(
            //            JsonConvert.SerializeObject(
            //                new {to = lst, data = new {title = "Portugal vs. Denmark", text = "5 to 1"}}), Encoding.UTF8,
            //            "application/json"));
            //        //var res = await client.PostAsync(new StringContent(
            //        //    JsonConvert.SerializeObject(
            //        //        new {to = lst, data = new {title = "Portugal vs. Denmark", text = "5 to 1"}}), Encoding.UTF8,
            //        //                    "application/json"));
            //        return res.Content;
            //    }
            //    catch (Exception e)
            //    {
            //        return e.Message;
            //    }
        }
        [NonAction]
        public HttpClient GetClient(string attr)
        {
            var client = new HttpClient();
            if (attr.Contains("google"))
            {
                client.DefaultRequestHeaders
                    .Accept
                    .Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("key",
                    "=AIzaSyDYWctJmgBYQHfDRSRsUUIimrnns0nVt5k");
                client.BaseAddress = new Uri("https://android.googleapis.com/gcm/send");
            }
            else
            {
                client.BaseAddress = new Uri(attr);
            }
            return client;
        }
    }
}

