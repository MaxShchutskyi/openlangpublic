﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.HelpersEntity;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace Xencore.Controllers.api
{
    [Authorize(Roles = "Presenter")]
    public class AboutMeController : ApiController
    {
        public Guid UserId => Guid.Parse(User.Identity.GetUserId());

        [HttpGet]
        public async Task<AboutMeHelper> GetAboutMeInfo()
        {

            using (var context = new MyDbContext())
            {
                var info = await context.TeacherInfos.FindAsync(UserId);
                var helper = new AboutMeHelper()
                {
                    Id = info.Id,
                    HasTrialLessonn = info.HasTrialLessonn,
                    Link = info.Link,
                    SpecialSkills = info.SpecialSkills,
                    VideoDescription = info.VideoDescription,
                    TeacherLevels = info.TeachLevels
                };
                return helper;
            }
        }
        [HttpPost]
        public async Task<object> UpdateAboutMeInfo(AboutMeHelper hepler)
        {
            if (!ModelState.IsValid)
                return new {error = "Fields too long"};
            using (var context = new MyDbContext())
            {
                var helper = new TeacherInfo()
                {
                    Id = hepler.Id,
                    HasTrialLessonn = hepler.HasTrialLessonn,
                    Link = hepler.Link,
                    SpecialSkills = hepler.SpecialSkills,
                    VideoDescription = hepler.VideoDescription,
                    TeachLevels = hepler.TeacherLevels
                };
                context.TeacherInfos.AddOrUpdate(helper);
                await context.SaveChangesAsync();
                return new {success = "Saved"};
            }
        }
    }
}
