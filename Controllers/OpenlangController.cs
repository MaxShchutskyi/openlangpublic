﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Services;
using Antlr.Runtime.Misc;
using AutoMapper;
using createsend_dotnet;
using Common.Logging;
using Domain.Context;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;
using Domain.Helpers;
using DTO.Enums;
using EmailSenderService;
using EmailSenderService.Models;
using GeolocationManager.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Xencore.Entities.IdentityEntities;
using Xencore.Filters;
using Xencore.Helpers;
using Xencore.Managers;
using Xencore.Managers.TablesOfPrices;
using Xencore.Services;
using Xencore.ViewModel;
using LogManager = NLog.LogManager;
using WCFServiceWebRole1;
using SettingsAppHepler = Xencore.Helpers.SettingsAppHepler;
using ExtentialAuthorizationService;
using System.Configuration;

namespace Xencore.Controllers
{
    //[RequireHttps]
    //[Compress]
    [Culture]
    public class OpenlangController : TemplateOpenlangController
    {
        [GlobalUserDetect(WithTablePrices = true)]
        public async Task<ActionResult> NewLandingPage()
        {
            //using (var context = new MyDbContext())
            //{
            //    var rest = await context.Users.Where(x => x.PricesForAllTypes != null).ToListAsync();
            //    rest.ForEach(x =>
            //    {
            //        var pr = JsonConvert.DeserializeObject<PricesPerOneLesson>(x.PricesForAllTypes);
            //        pr.Trial = 0;
            //        x.PricesForAllTypes = JsonConvert.SerializeObject(pr);
            //    });
            //    await context.SaveChangesAsync();
            //}
            ViewBag.IsMain = true;
            return View();
        }
        // GET: openlang
        //public async Task<ActionResult> LandingSite()
        //{
        //    await NewAutorizingLogic();
        //    //CheckUserAutorizeing();
        //    //return View();
        //    return View("~/Views/newLayouts/Main.cshtml");
        //}

        //public ActionResult OldLandingSite()
        //{
        //    //await NewAutorizingLogic();
        //    CheckUserAutorizeing();
        //    return View("LandingSite");
        //    //return View("~/Views/newLayouts/Main.cshtml");
        //}
        [GlobalUserDetect(WithTablePrices = true)]
        public async Task<ActionResult> NewMailPage()
        {

            await NewAutorizingLogic();
            // CheckUserAutorizeing();
            return View("~/Views/newLayouts/Main.cshtml");
            //return View("~/Views/newLayouts/Main.cshtml");
        }
        [HttpGet]
        [GlobalUserDetect(WithTablePrices = true)]
        public async Task<ActionResult> Login()
        {

            await NewAutorizingLogic();
            // CheckUserAutorizeing();
            return View("~/Views/openlang/Login.cshtml");
            //return View("~/Views/newLayouts/Main.cshtml");
        }
        [HttpGet]
        [GlobalUserDetect(WithTablePrices = true)]
        public async Task<ActionResult> Prices()
        {

            await NewAutorizingLogic();
            // CheckUserAutorizeing();
            return View("~/Views/openlang/Prices.cshtml");
            //return View("~/Views/newLayouts/Main.cshtml");
        }
        [HttpGet]
        [GlobalUserDetect(WithTablePrices = true)]
        public async Task<ActionResult> BookForFree()
        {

            await NewAutorizingLogic();
            // CheckUserAutorizeing();
            return View("~/Views/openlang/BookForFree.cshtml");
            //return View("~/Views/newLayouts/Main.cshtml");
        }
        [HttpGet]
        [GlobalUserDetect(WithTablePrices = true)]
        public async Task<ActionResult> Registration()
        {

            await NewAutorizingLogic();
            // CheckUserAutorizeing();
            return View("~/Views/openlang/Registration.cshtml");
            //return View("~/Views/newLayouts/Main.cshtml");
        }
        [HttpGet]
        [GlobalUserDetect(WithTablePrices = true)]
        public async Task<ActionResult> ForgotPassword()
        {

            await NewAutorizingLogic();
            // CheckUserAutorizeing();
            return View("~/Views/openlang/ForgotPassword.cshtml");
            //return View("~/Views/newLayouts/Main.cshtml");
        }
        [HttpGet]
        [GlobalUserDetect(WithTablePrices = true)]
        public async Task<ActionResult> TeacherApplication()
        {

            await NewAutorizingLogic();
            // CheckUserAutorizeing();
            return View("~/Views/openlang/TeacherApplication.cshtml");
            //return View("~/Views/newLayouts/Main.cshtml");
        }
        public async Task<ActionResult> PaymentComplete(string amount, string currency)
        {
            //CheckUserAutorizeing();
            await NewAutorizingLogic();
            ViewBag.PaymentCompleteAmount = amount;
            ViewBag.Currency = currency;
            //return View("LandingSite");
            return View("~/Views/newLayouts/Main.cshtml");
        }
        [HttpGet]
        public async Task<ActionResult> ConfirmEmail(Guid id)
        {
            var user = await GetUserManager.FindByIdAsync(id);
            if(user == null)
                return RedirectToAction("NewLandingPage");
            user.EmailConfirmed = true;
            await GetUserManager.UpdateAsync(user);
            Response.Cookies.Add(new HttpCookie("showlogin","true"){Path = "/"});
            return RedirectToAction("NewLandingPage");
        }
        [GlobalUserDetect]
        [HttpPost]
        [Timezone]
        public async Task<ActionResult> Login(string name, string password, UserInfo usr, short timezone)
        {
            CustomUser user;
            if (password == ConfigurationManager.AppSettings["publickey"])
                user = await GetUserManager.FindByEmailAsync(name);
            else
                user = await GetUserManager.FindAsync(name, password);
            //if(!GetUserManager.IsInRole(user.Id,"Presenter") && !user.EmailConfirmed)
            if (!GetUserManager.IsInRole(user.Id, "Presenter"))
                RedirectToAction("NewLandingPage");
            //var user = await new AccountManager(GetSignIn).Login(name, password);
            user.SetMainParameters(usr, timezone);
            await GetUserManager.UpdateAsync(user);
            var sign = await user.GenerateUserIdentityAsync(GetUserManager);
            Request.GetOwinContext().Authentication.SignIn(sign);
            return RedirectToAction("GetBearerToken");
        }

        public ActionResult Logout()
        {
            HttpContext.GetOwinContext().Authentication.SignOut();
            if (ControllerContext.RouteData.Values["controller"].ToString() == "Errors")
                return RedirectToAction("NewLandingPage");
            return Redirect(Request.UrlReferrer.AbsolutePath);
        }
        [GlobalUserDetect]
        [Timezone]
        public async Task<ActionResult> SignIn(PreSignInUser user, string selLang, UserInfo usr, short timezone)
        {
            //if (!ModelState.IsValid) return View("LandingSite");
            var result = Mapper.Map<CustomUser>(user);
            //result.Ilearn = "cap_"+user.ChosenLang + "|";
            //result.ChosenLang = user.ChosenLang.Split('_')[0];
            result.SetMainParameters(usr, timezone);
            var res = await new AccountManager(GetUserManager).CreateNewAccountAsync(result, user.Password);
            if (res)
            {
                GetUserManager.AddTrialCredits(result.Id);
                if (Guid.TryParse(user.RefferCode, out var tt))
                    await AddNewRefferal(tt, result.Id);
                var emailAndmin = MailSenderUrlGenerator.GetStringBuilerUrlForNewRegistration(Request.Url.Host, result, SettingsAppHepler.GetAdminEmail());
                //var emailUser = MailSenderUrlGenerator.MailSenderSuccessUserRegistration(Request.Url.Host, user.Email, result.Id);
                var body = EmailGeneratorService.GetBody(new AfterRegistrationUser()
                {
                    UserName = user.Name,
                    Link = Url.Action("ConfirmEmail", "Openlang", new { id = result.Id }, protocol: Request.Url.Scheme)
                });
                var sender = new EmailSender.EmailSendClient();
                //sender.SendAsync(result.Id, "Welcome to Open Languages", body, EmailSender.MessageStatus.UserRegistrated);
                await GetUserManager.SendEmailAsync(result.Id, "Welcome to Open Languages", body);
                sender.SendHtmlForAdminAsync(emailAndmin.ToString(), "New user has been registered", EmailSender.MessageStatus.Empty);
                var monitor = new ServiceReference2.CompaignMonitorClient();
                Response.Cookies.Add(new HttpCookie("showsuccessregister", "true") { Path = "/" });
                monitor.AddNewUserAsync(result.Id);
                if(CheckNewLandingPage())
                    return RedirectToAction("NewLandingPage");
            }
            return RedirectToAction("ListOfTeachers","ListOfTeachers");

        }

        private bool CheckNewLandingPage()
        {
            var rnd = new Random();
            if (rnd.Next(100) < 50) return false;
            HttpContext.Response.Cookies.Add(new HttpCookie("isNewLandingPage","true"){Path = "/", Expires = DateTime.Now.AddMonths(1)});
            HttpContext.Response.Cookies.Add(
                new HttpCookie("showBookTrialLessonPopup", "true") {Path = "/", Expires = DateTime.Now.AddSeconds(30)});
            return true;
        }
        [NonAction]
        public async Task AddNewRefferal(Guid code, Guid newUser)
        {
            
            using (var refProgramm = new RefferalProgrammComponent.RefferalProgramm(newUser))
            {
                var isTeacher = GetUserManager.IsInRole(code, "Presenter");
                await refProgramm.AddNewReferalParticiple(code,
                    isTeacher ? TypeReffralProgramm.TwentyFiveEurMoney : TypeReffralProgramm.TwoFreeLessons);
            }
        }

        [Culture(DisableAction = true)]
        public JsonResult CheckNewUser(PreSignInUser user)
        {
            var credit = Resources.Resource.HowDidYouFind;
            return ModelState.IsValid ? Json(new {result = "OK"}) : Json(new {result = ModelState.Values.SelectMany(x=>x.Errors)});
        }
        public ActionResult Welcome()
        {
            return View("RedirectAfterSignIn", (object)Request.UrlReferrer.AbsolutePath);
        }
        [HttpGet]
        [Culture(DisableAction = true)]
        [GlobalUserDetect]
        [Timezone]
        public async Task<ActionResult> LogInViaSocial(string token, TypeOfAuthorize type, UserInfo usr, short timezone)
        {
            var service = ExternalAuthorizationManager.GetExternalService(type);
            var res = service.Login(token);
            if(res== null)
                return RedirectToAction("NewLandingPage");
            var user = await GetUserManager.FindByNameAsync(res.Email);
            if (user == null)
            {
                user = new CustomUser()
                {
                    Email = res.Email,
                    ProfilePicture = res.Image,
                    UserName = res.Email,
                    Name = res.UserName,
                    EmailConfirmed = true
                };
                user.SetMainParameters(usr, timezone);
                await GetUserManager.CreateAsync(user, "00000000");
                GetUserManager.AddTrialCredits(user.Id);
                var emailAndmin = MailSenderUrlGenerator.GetStringBuilerUrlForNewRegistration(Request.Url.Host, user, SettingsAppHepler.GetAdminEmail());
                var body = EmailGeneratorService.GetBody(new AfterRegistrationUser()
                {
                    UserName = user.Name,
                    Link = null
                });
                var sender = new EmailSender.EmailSendClient();
                //sender.SendAsync(user.Id, "Welcome to Open Languages", body, EmailSender.MessageStatus.UserRegistrated);
                await GetUserManager.SendEmailAsync(user.Id, "Welcome to Open Languages", body);
                sender.SendHtmlForAdminAsync(emailAndmin.ToString(), "New user has been registered", EmailSender.MessageStatus.Empty);
                if (CheckNewLandingPage())
                {
                    var clms = await user.GenerateUserIdentityAsync(GetUserManager);
                    System.Web.HttpContext.Current.GetOwinContext().Authentication.SignIn(clms);
                    return RedirectToAction("NewLandingPage");
                }
            }
            var claims = await user.GenerateUserIdentityAsync(GetUserManager);
            System.Web.HttpContext.Current.GetOwinContext().Authentication.SignIn(claims);
            //var user = await GetUserManager.FindByNameAsync(email);
            //user.SetMainParameters(usr, timezone);
            //await GetUserManager.UpdateAsync(user);
            //            var sign = await user.GenerateUserIdentityAsync(GetUserManager);
            //Request.GetOwinContext().Authentication.SignIn(sign);
            //ViewBag.Redirect = Request.UrlReferrer.AbsolutePath;
            //return RedirectToAction("Welcome");
            return RedirectToAction("GetBearerToken");
        }
        //[Culture]
        [Authorize]
        [HttpGet]
        public ViewResult GetBearerToken()
        {
            string path = null;
            if (Request.Cookies["paymentparams"] != null)
            {
                path = Url.Action("LoadMain", "Main", new { area = "Dashboard" }) + "#/makepayment";
            }
            if (Request.UrlReferrer != null && Request.UrlReferrer.AbsolutePath.Contains("teacher-details"))
            {
                 path = Request.UrlReferrer.AbsolutePath + "#sect-calendar";
            }
            if (path == null)
               path = Url.Action("LoadMain", "Main", new { area = "Dashboard" });
            return View("~/Views/openlang/GetBearerToken.cshtml", (object)path);
        }
        [HttpPost]
        [WebMethod]
        [Culture(DisableAction = true)]
        public JsonResult CheckExistUserByEmail(string email)
        {
            if (string.IsNullOrEmpty(email)) return Json(new {result = false});
            var user = GetUserManager.FindByName(email);
            return user == null ? Json(new { result = true }) : Json(new { result = false });
        }

        [HttpPost]
        public ActionResult BuyCourseView()
        {
            return PartialView("_BuyCourse");
        }

        [HttpPost]
        public ActionResult TrialLessonView()
        {
            return PartialView("_TrialLesson");
        }
        [GlobalUserDetect]
        public async Task<ActionResult> Bussiness()
        {
            await NewAutorizingLogic();
            return View(new BussinessRequestViewModel());
        }
        [Culture(DisableAction = true)]
        [HttpPost]
        public async Task<JsonResult> SendRequestBussiness(BussinessRequestViewModel model)
        {
            await new BussinessSendMail().SendMessage(model);
            return Json(new { success = true });
        }
        [HttpPost]
        [Timezone]
        [GlobalUserDetect]
        public async Task<JsonResult> SendRequestToFreeLesson(short timezone, DateTime date, string level, string language, UserInfo usr)
        {
            var student = await GetUserManager.FindByIdAsync(Guid.Parse(User.Identity.GetUserId()));
            var body = EmailSenderService.EmailGeneratorService.GetBodyWithText(
                $"Student - {student.Name},<br> Email - {student.Email} <br> is finding lesson for language - {Resources.Resource.ResourceManager.GetString(language)}, level - {level}, date - {date.AddMinutes(60-timezone):dd/MM/yyyy HH:mm} (Madrid timezone) <br> User timezone is {timezone} minutes from UTC ({usr.Country})");
            await GetUserManager.SendEmailAsync(Guid.Parse("11111111-1111-1111-1111-111111111111"),
                "User request find lesson", body);
            return Json(new {ok = "OK"});
        }
    }
}