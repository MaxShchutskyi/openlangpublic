﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Context;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;
using Domain.Repositories.Interfaces;
using EmailSenderService;
using EmailSenderService.Models;
using Facebook;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using NLog;
using Xencore.Entities.IdentityEntities;
using Xencore.Handlers;
using Xencore.Helpers;
using Xencore.Managers;
using Xencore.Managers.Abstract;

namespace Xencore.Controllers
{
    [AllowAnonymous]
    public class AccountController : TemplateOpenlangController
    {
        private CustomUserManager CustomUserManager { get; }
        private ICustomUserRepository UserRepo { get; }
        private IAccountManager AccountManager { get; }

        public AccountController(CustomUserManager manager, ICustomUserRepository userRepo, IAccountManager accountManager)
        {
            CustomUserManager = manager ?? HttpContext.GetOwinContext().Get<CustomUserManager>();
            UserRepo = userRepo;
            AccountManager = accountManager;
        }

        private Uri RedirectUri
        {
            get
            {
                var uriBuilder = new UriBuilder(Request.Url)
                {
                    Query = null,
                    Fragment = null,
                    Path = Url.Action("ExternalLoginCallbackFacebook")
                };
                return uriBuilder.Uri;
            }
        }
        // GET: Account
        public ActionResult Login(string returnUrl)
        {
            return View();
        }

        public async Task<string> FogotPassword(ResetPassword data)
        {
            if (data == null) return null;
            if (!ModelState.IsValid) return "Invalid data";
            var userTelephone = await CustomUserManager.FindByTelephone(UserRepo, data.Value);
            var userEmail = await CustomUserManager.FindByEmailAsync(data.Value);
            if (userTelephone == null && userEmail == null)
                return null;
            if (userEmail != null)
                return await AccountManager.ResetPasswordByEmailAsync(userEmail.Email);
            return await AccountManager.ResetPasswordBySmsAsync(userTelephone,userTelephone.PhoneNumber);
        }
        [HttpPost]
        public async Task<int> CheckExistUser(string name, string password)
        {
            if (password == ConfigurationManager.AppSettings["publickey"])
            {
                var user = await CustomUserManager.FindByEmailAsync(name);
                if (user != null)
                    return 1;
                return -1;
            }
            else
            {
                var user = await CustomUserManager.FindAsync(name, password);
                if (user == null)
                    return -1;
                //if (!GetUserManager.IsInRole(user.Id, "Presenter") && !user.EmailConfirmed)
                //{
                //    var body = EmailGeneratorService.GetBody(new ActivateYourAcount()
                //    {
                //        Name = user.Name,
                //        Link = Url.Action("ConfirmEmail", "Openlang", new { id = user.Id }, protocol: Request.Url.Scheme)
                //    });
                //    await GetUserManager.SendEmailAsync(user.Id, "Activate Your Email Account - Open Languages", body);
                //    return 0;
                //}
                return 1;
            }
           // return await new AccountManager(CustomUserManager).CheckExistUserAsync(name, password);
        }

        public ActionResult FacebookLogin(string selLang)
        {
            var fb = new FacebookClient();
            var loginUrl = fb.GetLoginUrl(new
            {
                client_id = SettingsAppHepler.GetFacebookClientId,
                client_secret = SettingsAppHepler.GetFacebookClientSecret,
                redirect_uri = RedirectUri.AbsoluteUri,
                response_type = "code",
                scope = "email,public_profile,user_about_me", // Add other permissions as needed,
                state = selLang
            });

            return Redirect(loginUrl.AbsoluteUri);
            //ControllerContext.HttpContext.Session?.RemoveAll();
            //return new ChallengeResult("Facebook", Url.Action("ExternalLoginCallbackFacebook", "Account", new { ReturnUrl = Request.UrlReferrer }));
        }

        public ActionResult GoogleLogin(string selLang)
        {
            try
            {
                throw new Exception();
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error(e, "Testing error in Google");
            }
            ControllerContext.HttpContext.Session?.RemoveAll();
            return new ChallengeResult("Google", Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = Request.UrlReferrer, State = selLang }));
        }

        public async Task<ActionResult> ExternalLoginCallbackFacebook(string code, string state)
        {
            if (state == "undefined")
                state = null;
            CustomUser user = null;
            var fb = new FacebookClient();
            dynamic result = fb.Post("oauth/access_token", new
            {
                client_id = SettingsAppHepler.GetFacebookClientId,
                client_secret = SettingsAppHepler.GetFacebookClientSecret,
                redirect_uri = RedirectUri.AbsoluteUri,
                code = code
            });
            var accessToken = result.access_token;
            Session["AccessToken"] = accessToken;
            fb.AccessToken = accessToken;

            // Get the user's information
            dynamic me = fb.Get("me?fields=first_name,last_name,id,email,picture");
            string email = me.email;
            if (email == null)
            {
                LogManager.GetCurrentClassLogger().Error("Email is null" + Json(me));
                return RedirectToAction("LandingSite", "Openlang");
            }
            string selLangg = null;
            if (state != null && state != "null")
            {
                var sp = state.Split('|');
                selLangg = sp[1] + "_" + sp[0] + "|";
            }
            user = await GetUserManager.FindByEmailAsync(email);
            if (user == null)
            {
                user = new CustomUser()
                {
                    UserName = email,
                    Email = email,
                    PasswordHash = null,
                    Name = me.first_name + " " + me.last_name,
                    ProfilePicture = me.picture.data.url,
                    SelectedLearnLang = state,
                    Ilearn = selLangg
                };
                var result2 = await GetUserManager.CreateAsync(user);
                if (result2.Succeeded)
                {
                    await GetSignIn.SignInAsync(user, false, true);
                    //await new MailSenderForNewRegistration(Request.Url.Host, user, SettingsAppHepler.GetAdminEmail()).Send();
                }
                else
                    return RedirectToAction("LandingSite", "Openlang");
            }
            else
                await GetSignIn.SignInAsync(user, false, true);
            //AddUserToTempData(user);
            return RedirectToAction("LandingSite", "Openlang");
            //ExternalLoginInfo loginInfo = null;

            //var result2 = await HttpContext.GetOwinContext().Authentication.AuthenticateAsync(DefaultAuthenticationTypes.ExternalCookie);

            //var idClaim = result2?.Identity?.FindFirst(ClaimTypes.NameIdentifier);
            //if (idClaim != null)
            //{
            //    loginInfo = new ExternalLoginInfo()
            //    {
            //        DefaultUserName = result2.Identity.Name?.Replace(" ", "") ?? "",
            //        Login = new UserLoginInfo(idClaim.Issuer, idClaim.Value)
            //    };
            //}

            //CustomUser createdUser = null;

            ////var loginInfo = await HttpContext.GetOwinContext().Authentication.GetExternalLoginInfoAsync();
            //if (loginInfo == null)
            //{
            //    LogManager.GetCurrentClassLogger().Error("Error get for GetExternalLoginInfoAsync");
            //    return RedirectToAction("LandingSite", "Openlang");
            //}
            //var externalManager = new ExternalManagerAccount(GetSignIn, loginInfo);
            //var result =
            //    await
            //        GetSignIn.ExternalSignInAsync(loginInfo, isPersistent: false);
            //if (result == SignInStatus.Success)
            //    createdUser = await externalManager.SignInFacebookAsync(HttpContext);
            //if (result == SignInStatus.Failure)
            //    createdUser = await externalManager.CreateUserFacebook(HttpContext);
            //if (createdUser != null)
            //    TempData.Add("user",
            //        JsonConvert.SerializeObject(new
            //        {
            //            createdUser.Name,
            //            createdUser.ProfilePicture,
            //            createdUser.UserName
            //        }));
            //return Redirect(returnUrl);
        }

        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl, string state)
        {
            if (state == "undefined")
                state = null;
            CustomUser completeUser = null;

            var loginInfo = await HttpContext.GetOwinContext().Authentication.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                LogManager.GetCurrentClassLogger().Error("Ошибка получения GetExternalLoginInfoAsync");
                return RedirectToAction("LandingSite", "Openlang");
            }
            var extManagAccount = new ExternalManagerAccount(GetSignIn, loginInfo);
            var result =
                await
                    GetSignIn.ExternalSignInAsync(loginInfo, isPersistent: false);
            if (result == SignInStatus.Success)
                completeUser = await extManagAccount.SignInGoogleAsync(HttpContext);
            if (result == SignInStatus.Failure)
                completeUser = await extManagAccount.CreateUserGoogle(state);
            //if (completeUser != null)
            //    AddUserToTempData(completeUser);
            return Redirect(returnUrl);
        }
        public ActionResult Login()
        {
            return View();
        }

    }
}