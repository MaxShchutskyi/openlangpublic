﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using Domain.Context;
using Domain.Entities;
using Microsoft.AspNet.Identity;
using WebGrease.Css.Extensions;

using Xencore.Entities;
using Xencore.Extentions;

namespace Xencore.Controllers
{
    public class TeacherScheduleController : TemplateOpenlangController
    {
        [HttpGet]
        [WebMethod]
        public JsonResult GetTeacherSchedule(Guid teacherId, int? week, string localtime)
        {
            if (week == null)
            {
                var utc = DateTime.UtcNow;
                week = CultureInfo.GetCultureInfo("en-GB").Calendar.GetWeekOfYear(utc,
                    CultureInfo.CurrentCulture.DateTimeFormat.CalendarWeekRule,
                    DayOfWeek.Sunday);
                if (localtime.Contains("."))
                {
                    var hours = localtime.Split('.');
                    var local = int.Parse(hours[0])*-1;
                    if (hours[1].Length == 1)
                        hours[1] = hours[1] + "0";
                    var min = (int.Parse(hours[1])*0.6)*-1;
                    if (utc.DayOfWeek == DayOfWeek.Saturday && utc.AddHours(local).DayOfWeek == DayOfWeek.Sunday)
                        week++;
                    if (utc.DayOfWeek == DayOfWeek.Saturday &&
                        utc.AddHours(local).AddMinutes(min).DayOfWeek == DayOfWeek.Sunday)
                        week++;

                }
                else
                {
                    var local = int.Parse(localtime)*-1;
                    if (utc.DayOfWeek == DayOfWeek.Saturday && utc.AddHours(local).DayOfWeek == DayOfWeek.Sunday)
                        week++;
                }
            }
            using (var context = new MyDbContext())
            {
                var result =
                    context.Schedules
                        .Where(x => x.CustomUserId == teacherId && x.WeekNumber == week)
                        .Include(x => x.ScheduleTransactions.Select(f => f.User))
                        .Include(x => x.PaymentTransactions.OrderDetailsId)
                        .AsEnumerable()
                        .Select(x => new
                        {
                            id = x.Id,
                            start = x.StartDateTime,
                            end = x.EndDateTime,
                            className = x.Status,
                            studentName =
                            (x.ScheduleTransactions != null && x.ScheduleTransactions.Any())
                                ? x.ScheduleTransactions.First().User.Name
                                : null,
                            courseType =
                            (x.ScheduleTransactions != null && x.ScheduleTransactions.Any())
                                ? x.Type
                                : null,
                            image =
                            (x.ScheduleTransactions != null && x.ScheduleTransactions.Any())
                                ? x.ScheduleTransactions.First().User.ProfilePicture
                                : null
                        }).ToList();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [WebMethod]
        public JsonResult GetTeacherScheduleWithoutPast(Guid teacherId, int? week, string localtime)
        {
            if (week == null)
            {
                var utc = DateTime.UtcNow;
                week = CultureInfo.GetCultureInfo("en-GB").Calendar.GetWeekOfYear(utc,
                    CultureInfo.CurrentCulture.DateTimeFormat.CalendarWeekRule,
                    DayOfWeek.Sunday);
                if (localtime.Contains("."))
                {
                    var hours = localtime.Split('.');
                    var local = int.Parse(hours[0])*-1;
                    if (hours[1].Length == 1)
                        hours[1] = hours[1] + "0";
                    var min = (int.Parse(hours[1])*0.6)*-1;
                    if (utc.DayOfWeek == DayOfWeek.Saturday && utc.AddHours(local).DayOfWeek == DayOfWeek.Sunday)
                        week++;
                    if (utc.DayOfWeek == DayOfWeek.Saturday &&
                        utc.AddHours(local).AddMinutes(min).DayOfWeek == DayOfWeek.Sunday)
                        week++;

                }
                else
                {
                    var local = int.Parse(localtime)*-1;
                    if (utc.DayOfWeek == DayOfWeek.Saturday && utc.AddHours(local).DayOfWeek == DayOfWeek.Sunday)
                        week++;
                }
            }
            var dateTime = DateTime.UtcNow.AddHours(5);
            using (var context = new MyDbContext())
            {
                var result =
                    context.Schedules
                        .Where(
                            x =>
                                x.CustomUserId == teacherId && x.WeekNumber == week &&
                                x.StartDateTimeDateTime > dateTime).Select(x => new
                        {
                            id = x.Id,
                            start = x.StartDateTime,
                            end = x.EndDateTime,
                            className = x.Status
                        }).ToList();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [WebMethod]
        [Authorize(Roles = "Presenter")]
        public async Task<JsonResult> UpdateSchedule(IEnumerable<Schedule> schedule, IEnumerable<int> deleted)
        {
            if (schedule == null && deleted == null) return Json(new {success = true});
            using (var context = new MyDbContext())
            {
                if (deleted != null && deleted.Any())
                    context.Schedules.RemoveRange(context.Schedules.Where(x => deleted.Contains(x.Id)));
                if (schedule != null && schedule.Any())
                {
                    foreach (var sc in schedule)
                    {
                        sc.StartDateTimeDateTime = DateTime.Parse(sc.StartDateTime.Replace('T', ' ').Split('Z')[0]);
                        sc.DayOfTheWeek = (byte) (((byte) sc.StartDateTimeDateTime.DayOfWeek) + 1);
                    }
                    context.Schedules.AddRange(schedule);
                }
                    await context.SaveChangesAsync();
                return Json(schedule?.Select(x => new {
                    id = x.Id,
                    start = x.StartDateTime,
                    end = x.EndDateTime,
                    className = x.Status,
                    level = "",
                    language = "",
                    type="",
                    students= new object[] {}
                }));
            }
        }

        [WebMethod]
        [HttpGet]
        public JsonResult GetStatistic()
        {
            var id = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var td = DateTime.UtcNow;
                var schedulers = context.Schedules.Where(x => x.CustomUserId == id).Include(x=>x.ScheduleTransactions).ToList();
                var unscheduled = schedulers.Count(x => x.StartDateTimeDateTime > td && x.ScheduleTransactions.Count == 0);
                var scheduled = schedulers.Count(x => x.StartDateTimeDateTime > td && x.ScheduleTransactions.Count>0);
                var pastlessons = schedulers.Count(x => x.StartDateTimeDateTime < td && x.ScheduleTransactions.Count > 0);
                var count = schedulers.SelectMany(x=>x.ScheduleTransactions).GroupBy(f=>f.CustomUserId).Count();
                return Json(new { unscheduled, students = count, scheduled, pastlessons}, JsonRequestBehavior.AllowGet);
            }
        }

        [WebMethod]
        [HttpGet]
        public async Task<JsonResult> GetStatisticForStudent()
        {
            //var id = Guid.Parse(User.Identity.GetUserId());
            //using (var context = new MyDbContext())
            //{
            //    var td = DateTime.UtcNow;
            //    var schedulers = await 
            //        context.PaymentTransactions.Where(x => x.AttendeeId == id)
            //            .Include(x => x.ScheduleTransactions.Select(f=>f.Schedule)).ToListAsync();
            //    var sch = schedulers.SelectMany(x => x.ScheduleTransactions.Select(f=>f.Schedule))
            //            .ToList();
            //    //var unscheduled = schedulers.Count(x => x.StartDateTimeDateTime > td && x.PaymentTransactionsId == null);
            //    var scheduled = sch.Count(x => x.StartDateTimeDateTime > td);
            //    var pastlessons = sch.Count(x => x.StartDateTimeDateTime < td);
            //    //var tt = schedulers.Sum(x => x.OrderDetailsId.CountLessons);
            //    //var tt2 = schedulers.Sum(x => x.OrderDetailsId.CoursesSpent);
            //    //var unscheduled = tt - tt2; 
            //    var count = sch.GroupBy(x => x.CustomUserId).Count();
            //    return Json(new { unscheduled = 0, students = count, scheduled, pastlessons }, JsonRequestBehavior.AllowGet);
            //}
            return null;
        }

        [HttpPost]
        [Authorize(Roles = "Presenter")]
        public async Task<JsonResult> AssignStudent(string boughtCourseId, IEnumerable<int> schedules)
        {
            using (var context = new MyDbContext())
            {
                context.Schedules.Where(x => schedules.Contains(x.Id)).ForEach(x =>
                {
                    x.PaymentTransactionsId = boughtCourseId;
                    x.Status = "view3";
                    //x.IsPending = false;
                });
                var pay = await context.OrderDetailses.SingleOrDefaultAsync(x => x.TransactionId == boughtCourseId);
                pay.CoursesSpent += (byte)schedules.Count();
                if (pay.CoursesSpent > pay.CountLessons)
                    return Json(new {error = "Student spent all courses"});
                await context.SaveChangesAsync();
                return Json(new {result = true});
            }
        }

        [HttpPost]
        [Authorize(Roles = "Presenter")]
        public async Task<JsonResult> UnbindLessons(IEnumerable<int> schedules)
        {
            using (var context = new MyDbContext())
            {
                context.Schedules.Where(x => schedules.Contains(x.Id)).Include(x=>x.PaymentTransactions.OrderDetailsId).ForEach(x =>
                {
                    x.PaymentTransactions.OrderDetailsId.CoursesSpent -=
                        x.PaymentTransactions.OrderDetailsId.CoursesSpent;
                    x.PaymentTransactionsId = null;
                    x.Status = "view1";
                    //x.IsPending = false;
                });
                //var pay = await context.OrderDetailses.SingleOrDefaultAsync(x => x.TransactionId == boughtCourseId);
                //pay.CoursesSpent += (byte)schedules.Count();
                //if (pay.CoursesSpent > pay.CountLessons)
                //    return Json(new { error = "Student spent all courses" });
                await context.SaveChangesAsync();
                return Json(new { result = true });
            }
        }
    }
}