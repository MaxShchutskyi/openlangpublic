﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Services;
using CurrencyConverter;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;
using EmailSenderService;
using GeolocationManager.Models;
using Microsoft.AspNet.Identity;
using Xencore.Filters;
using Xencore.Helpers;

namespace Xencore.Controllers
{
    [Culture]
    public class ListOfTeachersController : TemplateOpenlangController
    {
        private MyDbContext context { get; set; }

        public MyDbContext Context
        {
            get
            {
                if (context == null)
                    return context = new MyDbContext();
                return context;
            }
        }
        public IQueryable<CustomUser> GetTeachers
        {
            get
            {
                return Context.Roles.Where(x => x.Name == "Presenter")
                    .SelectMany(x => x.Users)
                    .Join(context.Users, x => x.UserId, y => y.Id, (x, y) => y).Where(x => x.Active);
            }
        }

        [HttpPost]
        public async Task<JsonResult> SendMessageToTeacher(SendMessageHelper item)
        {
            var teacher = await GetUserManager.FindByIdAsync(item.TeacherId);
            var txt = $"Hi, My Name is {item.Name}.<br> Text: " + item.Text +
                      $". Please answer to this message from your Dashboard";
            var textForAdmin =
                $"User - {item.Name} ({item.Email}) sent to {teacher.Name} ({teacher.Email}) followig message <br>" +
                item.Text;
            await GetUserManager.SendEmailAsync(Guid.Parse("11111111-1111-1111-1111-111111111111"), item.Subject, EmailGeneratorService.GetBodyWithText(textForAdmin));
            var emailservice = new NativeEmailService.NativeEmailService();
            await emailservice.SendAsync(new IdentityMessage()
            {
                Body = EmailGeneratorService.GetBodyWithText(txt),
                Subject = item.Subject,
                Destination = teacher.Email
            });
            using (var contextt = new MyDbContext())
            {
                var correspond = new Correspond() { Email = item.Email, Input = true, Name = item.Name, Subject = item.Subject, TeacherId = item.TeacherId, Text = item.Text };
                contextt.Corresponds.Add(correspond);
                await contextt.SaveChangesAsync();
            }
            return Json(true);
        }

        [GlobalUserDetect]
        [HttpGet]
        [ActionName("ListOfTeachers")]
        public async Task<ActionResult> Index()
        {
            await NewAutorizingLogic();
            var users = GetTeachers.OrderBy(x => Guid.NewGuid()).Include(x => x.TeacherInfo).Take(5)
                .ToList();
            return View("~/Views/MyAccount/ListOfTeachers.cshtml", users);
        }

        //[HttpGet]
        //[WebMethod]
        //public ActionResult GetMoreWithHtml(int page)
        //{
        //    var users = GetTeachers
        //        .Include(x => x.TeacherInfo).OrderByDescending(x=>x.CreationDateTime).Skip(5 * page).Take(5)
        //        .ToList();
        //    return View("~/Views/MyAccount/_ListOfTeachers.cshtml", users);
        //}

        //[HttpGet]
        //[WebMethod]
        //public JsonResult GetMore(int page)
        //{
        //    var users = GetTeachers.OrderBy(x=>Guid.NewGuid())
        //        .Skip(5 * page)
        //        .Take(5)
        //        .Select(x => new
        //        {
        //            x.Id,
        //            x.Ilearn,
        //            x.Ispeak,
        //            x.PhoneNumber,
        //            x.ProfilePicture,
        //            x.Location,
        //            TeacherInfo = new
        //            {
        //                x.TeacherInfo.AboutMe,
        //                x.TeacherInfo.Additional,
        //                x.TeacherInfo.From,
        //                x.TeacherInfo.Link,
        //                x.TeacherInfo.Relevant,
        //                x.TeacherInfo.SpecialSkills,
        //                x.TeacherInfo.Teachers
        //            },
        //            x.UserName
        //        }).ToList();
        //    return Json(users, JsonRequestBehavior.AllowGet);
        //}

        [WebMethod]
        [HttpPost]
        public ActionResult Filter(FilterSearchHelper filter)
        {
            var users = DoFilter(filter);
            return View("~/Views/MyAccount/_ListOfTeachers.cshtml", users);
        }

        [HttpGet]
        [GlobalUserDetect(WithTablePrices = false)]
        public async Task<ActionResult> GetTeacherDetails(Guid id, UserInfo usr)
        {
            var currency = usr.CurrentCurency.Split(':')[0];
            if (User.Identity.IsAuthenticated)
            {
                await NewAutorizingLogic();
                var userId = Guid.Parse(User.Identity.GetUserId());
                var user = await Context.Users.FirstOrDefaultAsync(x => x.Id == userId);
                currency = user.Currency != null ? user.Currency.Split(':')[0] : currency;
            }
            ViewBag.Currency = currency;
            //if (!string.IsNullOrEmpty(amount) && !string.IsNullOrEmpty(currency))
            //{
            //    ViewBag.PaymentCompleteAmount = amount;
            //    ViewBag.Currency = currency;
            //}
            var teacher = await
                Context.Users.Where(x => x.Id == id)
                    .Include(x => x.TeacherInfo)
                    .Include(x => x.Educations)
                    .Include(x => x.WorkExperiences)
                    .Include(x => x.Certifications)
                    .Include(x => x.PhotoGalleries)
                    .Include(x => x.IndividualTeacherRates)
                    .Include(x => x.TeacherBookMarkedIds).FirstAsync();
            if (!teacher.IndividualTeacherRates.Any())
            {
                teacher.IndividualTeacherRates.Add(new IndividualTeacherRate());
                await Context.SaveChangesAsync();
            }
            if (currency.ToUpper() != "USD")
            {
                var converter = new CurrencyConverter<string>();
                var rate = teacher.IndividualTeacherRates.First();
                rate.OneLessonRate = converter.Convert(currency, "USD", rate.OneLessonRate).Result;
                rate.FiveLessonRate = converter.Convert(currency, "USD", rate.FiveLessonRate).Result;
                rate.TenLessonRate = converter.Convert(currency, "USD", rate.TenLessonRate).Result;
                rate.FifteenLessonRate = converter.Convert(currency, "USD", rate.FifteenLessonRate).Result;
                rate.Trial = converter.Convert(currency, "USD", rate.Trial).Result;
            }
            return View("~/Views/MyAccount/TeacherProfile.cshtml", teacher);
        }

        private IEnumerable<CustomUser> DoFilter(FilterSearchHelper filter)
        {
            var dt = DateTime.UtcNow;
            IEnumerable<Schedule> schedules = null;
            var users = GetTeachers.Include(x => x.TeacherInfo);
            if (filter.UsersAlreadyShown != null && filter.UsersAlreadyShown.Any())
                users = users.Where(x => filter.UsersAlreadyShown.All(f => f != x.Id));
            if (!string.IsNullOrEmpty(filter.Teach))
                users = users.Where(x => x.Ilearn != null && x.Ilearn.Contains(filter.Teach));
            if (!string.IsNullOrEmpty(filter.Speak))
                users = users.Where(x => x.Ispeak != null && x.Ispeak.Contains(filter.Speak));
            if (!string.IsNullOrEmpty(filter.Search))
                users =
                    users.Where(
                        x =>
                            x.Name.Contains(filter.Search) ||
                            x.TeacherInfo.AboutMe.Contains(filter.Search) ||
                            x.TeacherInfo.Additional.Contains(filter.Search) ||
                            x.TeacherInfo.SpecialSkills.Contains(filter.Search));
            if ((filter.Daysoftheweek != null && filter.Daysoftheweek.Any()) ||
                (filter.TimeRanges != null && filter.TimeRanges.Any()) || filter.TutoringAvailableNow)
            {
                schedules = Context.Schedules.Where(x => x.StartDateTimeDateTime > dt).ToList();
            }
            if (!filter.TutoringAvailableNow)
            {
                if (filter.Daysoftheweek != null && filter.Daysoftheweek.Any())
                {
                    schedules = schedules.Where(
                            f =>
                                filter.Daysoftheweek.Any(
                                    z =>
                                            z == (byte)(f.StartDateTimeDateTime.AddHours(-1 * filter.Zone).DayOfWeek + 1)))
                        .ToList();
                }
                if (filter.TimeRanges != null && filter.TimeRanges.Any())
                {
                    schedules = schedules.Where(f => filter.TimeRanges.Any(s =>
                                                     {
                                                         if (s.From > s.To)
                                                         {
                                                             if (s.From <= f.TimeStart)
                                                                 return true;
                                                             return s.From <= f.TimeStart + 24 &&
                                                                    s.To + 24 > f.TimeStart + 24;
                                                         }
                                                         return s.From <= f.TimeStart && s.To >= f.TimeStart;
                                                     }) && f.StartDateTimeDateTime > dt).ToList();
                }
            }
            else
            {
                var dtf = new DateTime(dt.Year, dt.Month, dt.Day + 1, 0, 0, 0);
                schedules = schedules.Where(f => f.StartDateTimeDateTime < dtf);
            }
            if (schedules != null)
            {
                var ids = schedules.Select(x => x.CustomUserId).Distinct();
                users = users.Where(x => ids.Any(f => f == x.Id));
            }
            return users.OrderByDescending(x => Guid.NewGuid()).Take(5).Select(x => new
            {
                Id = x.Id,
                Ilearn = x.Ilearn,
                Ispeak = x.Ispeak,
                TeacherInfo = new { x.TeacherInfo.VideoDescription, x.TeacherInfo.HasTrialLessonn, x.TeacherInfo.SpecialSkills, x.TeacherInfo.Link },
                Name = x.Name,
                UserName = x.UserName,
                ProfilePicture = x.ProfilePicture,
                Location = x.Location,
                x.Skype,
                x.QQAccount,
                x.FaceTimeAccount
            }).AsEnumerable().Select(x => new CustomUser()
            {
                Id = x.Id,
                Ilearn = x.Ilearn,
                Ispeak = x.Ispeak,
                TeacherInfo = new TeacherInfo() { VideoDescription = x.TeacherInfo.VideoDescription, HasTrialLessonn = x.TeacherInfo.HasTrialLessonn, SpecialSkills = x.TeacherInfo.SpecialSkills, Link = x.TeacherInfo.Link },
                Name = x.Name,
                UserName = x.UserName,
                ProfilePicture = x.ProfilePicture,
                Location = x.Location,
                Skype = x.Skype,
                QQAccount = x.QQAccount,
                FaceTimeAccount = x.FaceTimeAccount
            }).ToList();
        }
    }
}