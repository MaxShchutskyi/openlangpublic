﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Services;
using Domain.Context;
using Microsoft.AspNet.Identity;

using Xencore.Filters;

namespace Xencore.Controllers
{
    [Authorize]
    [Culture]
    public class UserPortalController : TemplateOpenlangController
    {
        private MyDbContext context { get; set; }

        public MyDbContext Context
        {
            get
            {
                if (context == null)
                    return context = new MyDbContext();
                return context;
            }
        }

        public ActionResult Index()
        {
            return User.IsInRole("Presenter")
                ? RedirectToAction("TeacherPortal")
                : RedirectToAction("StudentPortal");
        }

        [Authorize(Roles = "Presenter")]
        public async Task<ActionResult> TeacherPortal()
        {
            var id = Guid.Parse(User.Identity.GetUserId());
            await NewAutorizingLogic();
            var teacher = await
                Context.Users.Where(x => x.Id == id)
                    .Include(x => x.TeacherInfo)
                    .Include(x => x.Educations)
                    .Include(x => x.WorkExperiences)
                    .Include(x => x.Certifications)
                    .Include(x => x.PhotoGalleries).FirstAsync();
            return View("~/Views/MyAccount/TeacherPortal.cshtml", teacher);
        }
        [Authorize(Roles = "Presenter")]
        public async Task<ActionResult> ManagerDashboard()
        {
            var id = Guid.Parse(User.Identity.GetUserId());
            await NewAutorizingLogic();
            var teacher = await
                Context.Users.Where(x => x.Id == id)
                    .Include(x => x.TeacherInfo)
                    .Include(x => x.Educations)
                    .Include(x => x.WorkExperiences)
                    .Include(x => x.Certifications)
                    .Include(x => x.PhotoGalleries).FirstAsync();
            return View("~/Views/MyAccount/ManagerDashboard.cshtml", teacher);
        }
        [Authorize]
        public async Task<ActionResult> StudentPortal()
        {
            var id = Guid.Parse(User.Identity.GetUserId());
            await NewAutorizingLogic();
            var teacher = await
                Context.Users.Where(x => x.Id == id).FirstAsync();
            return View("~/Views/MyAccount/StudentPortal.cshtml", teacher);
        }

        [HttpPost]
        [Authorize(Roles = "Presenter")]
        public JsonResult GetMyStudents(string filterStatus)
        {
            var id = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var res = context.OrderDetailses.Where(x => x.CourseBoughtFromId == id)
                    .Include(x => x.PaymentTransactionsId.CustomUserId).Select(r => new
                    {
                        r.PaymentTransactionsId.CustomUserId.Id,
                        r.SelectedLanguage,
                        r.CountLessons,
                        r.CoursesSpent,
                        UserId = r.PaymentTransactionsId.CustomUserId.Id,
                        r.PaymentTransactionsId.CustomUserId.Location,
                        r.PaymentTransactionsId.CustomUserId.ProfilePicture,
                        r.PaymentTransactionsId.CustomUserId.UserName,
                        r.PaymentTransactionsId.CustomUserId.Name
                    }).AsEnumerable().Select(r=>new
                    {
                        r.Id,
                        r.SelectedLanguage,
                        r.CountLessons,
                        r.CoursesSpent,
                        r.UserId,
                        r.Location,
                        r.ProfilePicture,
                        r.UserName,
                        r.Name,
                        isOnline = HubsOfSignalR.ChatHub.ConnectedIds.FirstOrDefault(x=>x == r.Id.ToString())
                    }).ToList();
                return Json(res);
            }
        }
        [WebMethod]
        [System.Web.Http.HttpPost]
        public void Delete(int id)
        {
            using (var context = new MyDbContext())
            {
                var word = context.MyWords.First(x => x.Id == id);
                if (word.AudioPath != null)
                {
                    var path = Server.MapPath(word.AudioPath);
                    System.IO.File.Delete(path);
                }
                context.MyWords.Remove(word);
                context.SaveChanges();
            }
        }

        [WebMethod]
        [System.Web.Http.HttpPost]
        public JsonResult LoadImage()
        {
            var userid = Guid.Parse(User.Identity.GetUserId());
            var files = Request.Files[0];
            if (!Directory.Exists(Server.MapPath($"~/Images/my-words/{userid}")))
                Directory.CreateDirectory(Server.MapPath($"~/Images/my-words/{userid}"));
            var serverpath = Server.MapPath($"~/Images/my-words/{userid}");
            var directory = $"/Images/my-words/{userid}/";
            var smallimg = ScaleImage(new Bitmap(files.InputStream), 60, 60);
            smallimg.Save(Path.Combine(serverpath, files.FileName));
            return Json(new {directory = directory + $"{files.FileName}"});
        }
        [WebMethod]
        [System.Web.Http.HttpPost]
        [Culture(DisableAction = true)]
        public JsonResult LoadAudio()
        {
            var userid = Guid.Parse(User.Identity.GetUserId());
            var files = Request.Files[0];
            if (!Directory.Exists(Server.MapPath($"~/Audios/{userid}")))
                Directory.CreateDirectory(Server.MapPath($"~/Audios/{userid}"));
            var serverpath = Server.MapPath($"~/Audios/{userid}");
            var directory = $"/Audios/{userid}/";
            var filename = Guid.NewGuid() + "__" + files.FileName + ".wav";
            files.SaveAs(Path.Combine(serverpath, filename));
            return Json(new { directory = directory + $"{filename}" });
        }
        //public JsonResult GetMyLessons()
        //{
        //    var id = Guid.Parse(User.Identity.GetUserId());
        //    using (var context = new MyDbContext())
        //    {
        //       // context.Schedules.Where(x => x.CustomUserId == id).Include(x => x.ScheduleStudents.);
        //    }
        //}
    }
}