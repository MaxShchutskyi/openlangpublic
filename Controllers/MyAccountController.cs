﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;
using Microsoft.AspNet.Identity;
using Xencore.Filters;
using Xencore.Repositories;

namespace Xencore.Controllers
{
    [Culture]
    [System.Web.Mvc.Authorize]
    public class MyAccountController : TemplateOpenlangController
    {
        private List<PaymentTransactions> Transactions { get; set; }
        // GET: MyAccount
        public async Task<ActionResult> Index(bool? isAssign, Guid?studentId)
        {
            ViewBag.isAssgn = isAssign;
            ViewBag.studentId = studentId;
            CustomUser user;
            if (HttpContext.User.IsInRole("Presenter"))
            {
                using (var context = new MyDbContext())
                {
                    var id = Guid.Parse(HttpContext.User.Identity.GetUserId());
                    user =
                        await context.Users.Where(x => x.Id == id)
                            .Include(x => x.TeacherInfo)
                            .Include(x => x.Educations)
                            .Include(x => x.WorkExperiences)
                            .Include(x => x.Certifications)
                            .Include(x => x.PhotoGalleries)
                            .Include(x=>x.AdditionalUserInformation)
                            .Include(x => x.PayPalPaymentTransactions.Select(f => f.OrderDetailsId)).FirstAsync();
                }
                ViewBag.User = user;
                return View("GeneralPage", user);
            }
            using (var context = new MyDbContext())
            {
                var id = Guid.Parse(HttpContext.User.Identity.GetUserId());
                user =
                    await context.Users.Where(x => x.Id == id)
                        .Include(x => x.PayPalPaymentTransactions.Select(f => f.OrderDetailsId)).Include(x=>x.AdditionalUserInformation)
                        .FirstAsync();
            }
            ViewBag.User = user;
            return View("GeneralPage", user);
            //return View();
        }
        public ActionResult Get_General(CustomUser user)
        {
            List<Countries> listCountries;
            using (var context = new MyDbContext())
            {
                listCountries = context.Countries.ToList();
            }

            return View("_Main", new UserWithCountries() { Countries = listCountries, User = user });
        }

        public ActionResult Get_ChangePassword()
        {
            return View("_NewChangePassword");
        }
        [System.Web.Mvc.HttpGet]
        [Culture(IsCookieOnly = true)]
        public ActionResult GetPageForPdf(string paymentId)
        {
            paymentId = paymentId.Trim();
            return View("PrintToPdfPayment", new PaymentTransactionRepository().GetAllTransactionsByIdPayment(paymentId));
        }

        public ActionResult Get_CourseBooking()
        {
            Transactions = new PaymentTransactionRepository().GetAllTransactionsById(HttpContext.User.Identity.GetUserId());
            TempData["Payments"] = Transactions;
            return View("_NewCourseBooking", Transactions);
        }
        public async Task<ActionResult> Bussiness()
        {
            await NewAutorizingLogic();
            return View();
        }
    }
}