﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Xencore.Controllers.templates
{
    public class TenplateDesignController : Controller
    {
        // GET: TenplateDesign
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Video()
        {
            return View();
        }
        public ActionResult Pictures()
        {
            return View();
        }
        public ActionResult Words()
        {
            return View();
        }
        public ActionResult Sentence()
        {
            return View();
        }
        public ActionResult Voice()
        {
            return View();
        }
    }
}