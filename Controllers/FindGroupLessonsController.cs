﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Context;
using DTO.Enums;
using Xencore.Filters;
using Xencore.Managers.TablesOfPrices.Abstract;
using Xencore.ViewModel;

namespace Xencore.Controllers
{
    [Culture]
    [Authorize]
    public class FindGroupLessonsController : TemplateOpenlangController
    {
        public async Task<ActionResult> GeiListOfLessons()
        {
            await NewAutorizingLogic();
            using (var context = new MyDbContext())
            {
                var datetimenow = DateTime.UtcNow;
                var res = await context.Schedules.Where(x=>x.Type == TypeLessons.Group)
                    .Where(x => x.StartDateTimeDateTime > datetimenow).Include(x => x.CustomUser).Select(x=> new
                    {
                        x.StartDateTimeDateTime,
                        xId = x.Id,
                        Id = x.CustomUserId,
                        x.Level,
                        x.Language,
                        x.CustomUser.Name,
                        x.CustomUser.ProfilePicture,
                        Sbj = x.Subject,
                        x.Description,
                        x.Title,
                        x.Img,
                        x.ScheduleTransactions.Count
                    }).OrderByDescending(x=>x.StartDateTimeDateTime).ToListAsync();
                var result = res.Select(x => new GroupScheduleViewModel()
                {
                    TeacherId = x.Id,
                    ScheduleId = x.xId,
                    Level = x.Level,
                    Language = x.Language,
                    Count = (short) x.Count,
                    Name = x.Name,
                    ProfilePicture = x.ProfilePicture,
                    StartDateTime = x.StartDateTimeDateTime,
                    Sbj = x.Sbj,
                    Description = x.Description,
                    Title = x.Title,
                    Img = x.Img
                });
                return View(result);
            }
        }
    }
}