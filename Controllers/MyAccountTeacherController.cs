﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using AutoMapper;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.HelpersEntity;
using Microsoft.AspNet.Identity;
using RazorEngine;
using WebGrease.Css.Extensions;

using Xencore.Entities;
using Xencore.Helpers;

namespace Xencore.Controllers
{
    [Authorize]
    public class MyAccountTeacherController : TemplateOpenlangController
    {
        private async Task<bool> Update<T>(IEnumerable<T> educations) where T : CommonInfoOfTeacher
        {
            if (educations == null || !educations.Any())
                return false;
            var userId = Guid.Parse(User.Identity.GetUserId());
            educations.ForEach(x => x.CustomUserId = userId);
            var ids = educations.Select(x => x.Id);
            using (var context = new MyDbContext())
            {
                context.Set<T>().RemoveRange(
                    context.Set<T>().Where(x => x.CustomUserId == userId && ids.All(f => f != x.Id)));
                context.Set<T>().AddOrUpdate(x => x.Id, educations.ToArray());
                await context.SaveChangesAsync();
                return true;
            }
        }

        [ValidateInput(false)]
        [WebMethod]
        [HttpPost]
        public async Task<JsonResult> UpdateTeacherExpiriens(IEnumerable<Education> items)
        {
            var res = await Update(items);
            return Json(new {success = res, errors = ""});
        }

        [ValidateInput(false)]
        [WebMethod]
        [HttpPost]
        public async Task<JsonResult> UpdateTeacherWorkExperience(IEnumerable<WorkExperience> items)
        {
            var res = await Update(items);
            return Json(new {success = res, errors = ""});
        }

        [ValidateInput(false)]
        [WebMethod]
        [HttpPost]
        public async Task<JsonResult> UpdateTeacherCertificates(IEnumerable<Certification> items)
        {
            var res = await Update(items);
            return Json(new {success = res, errors = ""});
        }

        [WebMethod]
        [HttpPost]
        public async Task<JsonResult> UploadImages()
        {
            var userid = Guid.Parse(User.Identity.GetUserId());
            var files = Request.Files;
            if (!Directory.Exists(Server.MapPath($"~/Images/teacher-gallery/{userid}")))
                Directory.CreateDirectory(Server.MapPath($"~/Images/teacher-gallery/{userid}"));
            var serverpath = Server.MapPath($"~/Images/teacher-gallery/{userid}");
            var directory = $"/Images/teacher-gallery/{userid}/";

            var lst = new List<PhotoGallery>();
            for (var s = 0; s < files.Count; s++)
            {
                var pic = files[s];
                var i = new PhotoGallery();
                var stringpath = directory + pic.FileName;
                var stringpathsmall = directory + "small_" + pic.FileName;
                i.FileName = pic.FileName;
                i.FileSize = pic.ContentLength;
                i.CustomUserId = userid;
                i.FileType = pic.ContentType;
                i.LinkBigImage = stringpath;
                i.LinkSmallImage = stringpathsmall;
                i.UploadDate = DateTime.Now;
                var smallimg = ScaleImage(new Bitmap(pic.InputStream), 500, 500);
                var biglimg = ScaleImage(new Bitmap(pic.InputStream), 1200, 1200);
                biglimg.Save(Path.Combine(serverpath, pic.FileName));
                smallimg.Save(Path.Combine(serverpath, "small_" + pic.FileName));
                lst.Add(i);
            }
            using (var context = new MyDbContext())
            {
                    context.PhotoGalleries.AddRange(lst);
                    await context.SaveChangesAsync();
            }

            return
                Json(
                    new
                    {
                        items =
                        lst.Select(
                            x =>
                                new PhotoGalleryHelper()
                                {
                                    Id = x.Id,
                                    CustomUserId = x.CustomUserId,
                                    Description = x.Description,
                                    FileName = x.FileName,
                                    FileSize = x.FileSize,
                                    FileType = x.FileType,
                                    LinkBigImage = x.LinkBigImage,
                                    LinkSmallImage = x.LinkSmallImage,
                                    Title = x.Title,
                                    UploadDate = x.UploadDate,
                                    Dimensision = x.Dimensision
                                }),
                        success = true,
                        errors = ""
                    });
        }
        [WebMethod]
        [HttpPost]
        public async Task<JsonResult> DeleteImage(IEnumerable<PhotoGallery> imgs)
        {
            if (imgs == null || !imgs.Any())
                return Json(new {success = false, errors = new Collection<string>()});
            var ids = imgs.Select(x => x.Id);
            var srv = Server.MapPath($"~/Images/teacher-gallery/{User.Identity.GetUserId()}");
            using (var context = new MyDbContext())
            {
                context.PhotoGalleries.RemoveRange(context.PhotoGalleries.Where(x => ids.Contains(x.Id)));
                var tsk = context.SaveChangesAsync();
                foreach (var img in imgs)
                {
                    System.IO.File.Delete(Path.Combine(srv, img.FileName));
                    System.IO.File.Delete(Path.Combine(srv, "small_" + img.FileName));
                }
                await Task.WhenAll(tsk);
                return Json(new {success = true, errors = new Collection<string>()});
            }
        }

        [WebMethod]
        [HttpPost]
        public async Task<JsonResult> UpdateAboutMeInfo(AboutMeHelper aboutMe)
        {
            if (!ModelState.IsValid) return Json(new {success = false, error = "Model is not valid"});
            var id = Guid.Parse(User.Identity.GetUserId());
            using (var context = new MyDbContext())
            {
                var info = context.TeacherInfos.First(x => x.Id == id);
                Mapper.Map(aboutMe, info);
                if (!string.IsNullOrEmpty(aboutMe.Link) && aboutMe.Link != "https://player.vimeo.com/video/")
                    info.Link = aboutMe.Link;
                await context.SaveChangesAsync();
            }
            return Json(new {success = true, error = ""});
        }
        [WebMethod]
        [HttpGet]
        public JsonResult RemoveUploadedFile(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                Response.StatusCode = 400;
                return Json(new { Error = "Invalid path" }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                System.IO.File.Delete(Server.MapPath(path));
                return Json(new {Success = true}, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                Response.StatusCode = 400;
                return Json(new {Error = "Invalid delete file"}, JsonRequestBehavior.AllowGet);
            }
        }
        [Authorize(Roles = "Presenter")]
        [WebMethod]
        [HttpPost]
        public JsonResult UploadAssingmentFile()
        {
            var userid = Guid.Parse(User.Identity.GetUserId());
            if(Request.Files.Count==0) return Json(new { error = "Can't upload file" });
            var file = Request.Files[0];
            if (!Directory.Exists(Server.MapPath($"~/UploadFiles/Assingments/{userid}")))
                Directory.CreateDirectory(Server.MapPath($"~/UploadFiles/Assingments//{userid}"));
            var serverpath = Server.MapPath($"~/UploadFiles/Assingments/{userid}");
            var directory = $"/UploadFiles/Assingments/{userid}/";
            var filename = file.FileName +"__"+ Guid.NewGuid();
            file.SaveAs(Path.Combine(serverpath, filename));
            return Json(new { directory = directory + filename });
        }
    }
}