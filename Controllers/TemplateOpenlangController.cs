﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.IdentityEntities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using RazorEngine;
using Xencore.AdditionalsActionResults;
using Xencore.Entities;
using Xencore.Entities.IdentityEntities;

namespace Xencore.Controllers
{
    public class TemplateOpenlangController : Controller
    {

        protected CustomSignIn GetSignIn => HttpContext.GetOwinContext().Get<CustomSignIn>();
        protected CustomUserManager GetUserManager => HttpContext.GetOwinContext().Get<CustomUserManager>();

        protected async Task NewAutorizingLogic()
        {
            if (User.Identity.IsAuthenticated)
                ViewBag.User = await GetUserManager.FindByIdAsync(Guid.Parse(User.Identity.GetUserId()));
        }
        protected Image ScaleImage(Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);

            using (var graphics = Graphics.FromImage(newImage))
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);

            return newImage;
        }
    }
}