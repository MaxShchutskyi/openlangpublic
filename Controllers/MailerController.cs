﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.IdentityEntities;
using Microsoft.AspNet.Identity;
using NLog;
using Xencore.Entities;
using Xencore.Entities.IdentityEntities;
using Xencore.Entities.IdentityEntities.Entities;

namespace Xencore.Controllers
{
    public class MailerController : TemplateOpenlangController
    {
        // GET: Mailer
        public ActionResult GetForTrialLesson(string username, string language, string level, string startDate, string time, string timezone, string utcDateTime, string teacherId)
        {
            var teach = GetTeacherById(teacherId);
            if (teach != null)
            {
                ViewBag.teacherName = teach.Name;
                ViewBag.teacherEmail = teach.Email;
            }
            ViewBag.username = username;
            ViewBag.language = language;
            ViewBag.level = level;
            ViewBag.date = startDate;
            ViewBag.time = time;
            ViewBag.timezone = timezone;
            ViewBag.utcDateTime = utcDateTime;
            return View("~/Views/MailerTemlates/MailMessageForTrialLesson.cshtml");
        }
        public async Task<ActionResult> GetForNewRegistration(string hui)
        {
            LogManager.GetCurrentClassLogger().Error("This is new user - " + hui);
            using (var context = new MyDbContext())
            {
                var user = await context.Users.FirstOrDefaultAsync(x => x.UserName == hui);
                return View("~/Views/MailerTemlates/NewRegistration.cshtml", user);
            }
        }
        public ActionResult GetForNewOrder(PaymentTransactions transact)
        {
            using (var context = new MyDbContext())
            {
                var transacts = context.PaymentTransactions.Where(x => x.PaymentId == transact.PaymentId).Include(x => x.Credit).FirstOrDefault();
                return View("~/Views/MailerTemlates/MailOrder.cshtml", transacts);
            }
        }
        public ActionResult GetOrderForAdmin(PaymentTransactions transact, string studentName, string studentEmail)
        {
            using (var context = new MyDbContext())
            {
                var transacts = context.PaymentTransactions.Where(x => x.PaymentId == transact.PaymentId).Include(x => x.Credit).Include(x => x.CustomUserId).FirstOrDefault();
                return View("~/Views/MailerTemlates/MailOrderForAdmin.cshtml", transact);
            }
        }

        public ActionResult GetForFaqRequest(string emailText, string subject)
        {
            ViewBag.emailText = emailText;
            ViewBag.subject = subject;
            return View("~/Views/MailerTemlates/FaqRequestForUser.cshtml");
        }
        public ActionResult GetForNewsLetter()
        {
            return View("~/Views/MailerTemlates/Newsletter.cshtml");
        }
        public ActionResult GetMailForTeacher(TeacherApplication application)
        {
            return View("~/Views/MailerTemlates/ForTeacherEmail.cshtml", application);
        }

        public ActionResult SuccessUserRegistration(string userName, Guid? id)
        {
            ViewBag.UserName = userName;
            ViewBag.Id = id;
            return View("~/Views/MailerTemlates/SuccessUserRegistration.cshtml");
        }

        private CustomUser GetTeacherById(string id)
        {
            return !string.IsNullOrEmpty(id) ? GetUserManager.FindById(Guid.Parse(id)) : null;
        }

        public ActionResult GetBookingLessonForTeacherTemplate(Guid teacherId, Guid studentId, DateTime date, string language)
        {
            using (var context = new MyDbContext())
            {
                var teacher = context.Users.Find(teacherId);
                var student = context.Users.Find(studentId);
                ViewBag.TeacherName = teacher.Name;
                ViewBag.StudentName = student.Name;
                ViewBag.Date = date;
                ViewBag.Language = language;
                return View("~/Views/MailerTemlates/GetBookingLessonForTeacherTemplate.cshtml");
            }
        }
        public ActionResult GetResheduledLessonForTeacherTemplate(Guid teacherId, Guid studentId, DateTime date, string language)
        {
            using (var context = new MyDbContext())
            {
                var teacher = context.Users.Find(teacherId);
                var student = context.Users.Find(studentId);
                ViewBag.TeacherName = teacher.Name;
                ViewBag.StudentName = student.Name;
                ViewBag.Date = date;
                ViewBag.Language = language;
                return View("~/Views/MailerTemlates/GetResheduledLessonForTeacherTemplate.cshtml");
            }
        }
        public ActionResult GetUniversalTemplate(string text)
        {
            ViewBag.Text = text;
            return View("~/Views/MailerTemlates/GetUniversalTemplate.cshtml");
        }
        public ActionResult AnswerForUserFromTeacher(string teacherName, string text)
        {
            ViewBag.TeacherName = teacherName;
            ViewBag.Text = text;
            return View("~/Views/MailerTemlates/AnswerForUserFromTeacher.cshtml");
        }
        
    }
}