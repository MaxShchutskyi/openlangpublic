﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Xencore.Filters;

namespace Xencore.Controllers
{
    [Culture]
    public class OfficesController : TemplateOpenlangController
    {
        [GlobalUserDetect]
        public async Task<ActionResult> Offices()
        {
            await NewAutorizingLogic();
            return View();
        }
    }
}