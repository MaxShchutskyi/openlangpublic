﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using Microsoft.AspNet.Identity;
using Xencore.Entities.IdentityEntities;
using Xencore.Managers;
using Xencore.Repositories;

namespace Xencore.Controllers
{
    [Authorize]
    public class DiscountController : Controller
    {
        [WebMethod]
        [HttpPost]
        public async Task<JsonResult> GetDiscount(string number, string ammount, string currency)
        {
            var discountRes = await new DiscountRepository().GetDiscountByNumber(number, Guid.Parse(User.Identity.GetUserId()));
            if (discountRes == null) return Json(new { newPrice = "" });
            var discountManager = new DiscountManager(discountRes, ammount, currency);
            var newPrice = await discountManager.GetDiscount();
            return Json(new {newPrice, discountRes.Id});
        }
    }
}