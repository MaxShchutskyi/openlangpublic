﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Services;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.HelpersEntity;
using DTO.Enums;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Xencore.Entities;
using Xencore.Helpers;
using Xencore.Managers;
using Xencore.Managers.TablesOfPrices;
using Xencore.Managers.TablesOfPrices.Abstract;
using Xencore.Repositories;
using Xencore.Services;

namespace Xencore.Controllers
{
    public class CreditCardPaymentController : TemplateOpenlangController
    {
        [Authorize]
        [HttpPost]
        [WebMethod]
        public async Task<JsonResult> MakePayment(CreditCardHelper card, string advance, int  dateId = -1, decimal eurPrice = 0.99m, int? discountId = null)
        {
            //Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("us");
            var pl = advance.Split(' ');
            Response.TrySkipIisCustomErrors = true;
            if (ModelState.IsValid)
            {
                var service = new StripeService(card,pl);
                var result = service.Charge();
                if (result == "Error")
                {
                    Response.StatusCode = 400;
                    return Json(new { error = "Stripe Service API return error" });
                }
                PaymentTransactions transact;
                if ((transact = await new CreditCardRepository().Add(card, result,
                    Guid.Parse(HttpContext.User.Identity.GetUserId()),"Credit Card",advance, dateId, discountId, eurPrice))!=null)
                {
                    if (transact.OrderDetailsId.CourseType != "Trial")
                    {
                        var discount = transact.Discounts?.FirstOrDefault();
                        if (discount != null)
                        {
                            var discountManager = new DiscountManager(discount, transact.EurPrice.ToString(CultureInfo.InvariantCulture));
                            var newPrice = await discountManager.GetDiscount();
                            var user = await GetUserManager.FindByIdAsync(Guid.Parse(User.Identity.GetUserId()));
                            var prices = PricesPerOneLesson.SetPricesPerOneLesson(
                                CountryAnalizer.Analize(user.Location.Split(':')[1]), card.PaymentDetails.CountLessons);
                            var str = JsonConvert.SerializeObject(prices);
                            user.Money += newPrice;
                            user.PricesForAllTypes = str;
                            await GetUserManager.UpdateAsync(user);
                        }
                        else
                        {
                            var user = await GetUserManager.FindByIdAsync(Guid.Parse(User.Identity.GetUserId()));
                            var prices = PricesPerOneLesson.SetPricesPerOneLesson(
                                 CountryAnalizer.Analize(user.Location.Split(':')[1]), card.PaymentDetails.CountLessons);
                            var str = JsonConvert.SerializeObject(prices);
                            user.PricesForAllTypes = str;
                            user.Money += transact.EurPrice;
                            await GetUserManager.UpdateAsync(user);
                        }
                    }
                    if (transact.OrderDetailsId.CourseType == "Trial")
                    {
                        var paymentTrans = new ScheduleTransaction()
                        {
                            ScheduleId = dateId,
                            Price = eurPrice,
                            Language = transact.OrderDetailsId.ChLang,
                        };
                        using (var context = new MyDbContext())
                        {
                            var schid = context.Schedules.Find(dateId);
                            schid.Status = "view4";
                            schid.Type = (TypeLessons)Enum.Parse(typeof(TypeLessons), transact.OrderDetailsId.CourseType, true);
                            schid.Level = transact.OrderDetailsId.SelectedLevel;
                            context.ScheduleTransactions.Add(paymentTrans);
                            context.SaveChanges();
                            var teacher = context.Users.Find(schid.CustomUserId);
                            //await SendMessagesAndCheckerNewDialogs.SendMeessageIntoChatAfterBoutghtACourse(transact.PaymentId, teacher, TypeLessons.Trial, context);
                        }
                        await new MailSenderForTrial("Your Open Languages Trial Lesson Schedule", transact.CustomUserId.Name, transact.CustomUserId.Email, transact.OrderDetailsId,
                            Request.Url.Host).Send();
                        await new MailSenderOrderForAdmin(Request.Url.Host, SettingsAppHepler.GetAdminEmail() , transact).Send();
                    }

                    else
                    {
                        await new MailSenderNewOrder("Your Open Languages Lessons Schedule", Request.Url.Host, transact.CustomUserId.Email, transact).Send();
                        await new MailSenderOrderForAdmin(Request.Url.Host, SettingsAppHepler.GetAdminEmail(), transact).Send();
                    }
                    //if(card.PaymentDetails.CourseBoughtFromId!=null)
                    //    await SendMeessageIntoChatAfterBoutghtACourse(transact, card.PaymentDetails.CourseBoughtFromId?? Guid.NewGuid());
                    Response.StatusCode = 200;
                    return
                        Json(
                            new
                            {
                                response = "Success operation",
                                redirectTo =
                                System.Web.HttpContext.Current.Request.UrlReferrer + "?amount="+ advance.Split(' ')[1] + "&currency="+ advance.Split(' ')[0]
                            }
                        );
                }
                Response.StatusCode = 400;
                return Json(new { error = "Uncorrect data" });

            }
            Response.StatusCode = 400;
            return Json(new { error = ModelState.Values.SelectMany(v => v.Errors) });
        }

        public ActionResult RedirectToThanks(string url)
        {
            TempData["redirect"] = url;
            return RedirectToAction("ThankYou");
        }
        [System.Web.Mvc.Authorize]
        public ActionResult ThankYou()
        {
            if (!TempData.ContainsKey("redirect"))
                return RedirectToAction("NewMailPage", "Openlang");
            ViewBag.Redirect = TempData["redirect"];
            TempData.Remove("redirect");
            return View("~/Views/openlang/ThankYouCreditCard.cshtml");
        }
    }
}