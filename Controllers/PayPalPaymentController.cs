﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Runtime.Caching;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using AutoMapper.Internal;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;
using Domain.Helpers;
using DTO.Enums;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using PaymentService;
using PaymentService.Models;
using PayPal.Api;
using RefferalProgramm.Realization;
using Xencore.Entities.IdentityEntities;
using Xencore.Filters;
using Xencore.Helpers;
using Xencore.Managers;
using Xencore.Managers.TablesOfPrices;
using Xencore.Managers.TablesOfPrices.Abstract;
using Xencore.Repositories;
using Xencore.Services;
using SettingsAppHepler = Xencore.Helpers.SettingsAppHepler;

namespace Xencore.Controllers
{
    public class PayPalPaymentController : TemplateOpenlangController
    {
        public CustomSignIn GetSignIn => HttpContext.GetOwinContext().Get<CustomSignIn>();
        public APIContext GetApiContext
        {
            get
            {
                var config = ConfigManager.Instance.GetProperties();
                var accessToken = new OAuthTokenCredential(config).GetAccessToken();
                return new APIContext(accessToken);
            }
        }
        [HttpPost]
        [WebMethod]
        [Authorize]
        // GET: PayPalPayment
        public async Task<JsonResult> Get_PayPalLink(PaymentDetailsHelper paymentDetails, string advance, int dateId = -1, decimal eurPrice = 0.99m, string teacherId = null, int? discountId = null)
        {
            //Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
            var slice = advance.Split(' ');
            Response.TrySkipIisCustomErrors = true;
            paymentDetails.StartTime = paymentDetails.StartTime?.Replace(" ", "");
            if (ModelState.IsValid)
            {
                try
                {
                    var payPalService = new PayPalService(GetApiContext);
                    var payment = payPalService.GetPayment(paymentDetails.CountLessons, decimal.Parse(slice[1]), slice[0], dateId, teacherId);
                    var userId = HttpContext.User.Identity.GetUserId();
                    var repo = new PaymentTransactionRepository();
                    repo.Add(userId, paymentDetails, payment.id, "PayPal", advance, dateId, discountId, eurPrice);
                    var manager = HttpContext.GetOwinContext().Get<CustomUserManager>();
                    var usr = await manager.FindByIdAsync(Guid.Parse(userId));
                    await ChangeILearn.ChangeAsync(usr, manager, paymentDetails.ChLang.Split('_')[1]);

                    Response.StatusCode = 200;
                    return Json(new { success = payment.GetApprovalUrl() });
                }
                catch (Exception e)
                {

                }
            }
            Response.StatusCode = 400;
            return Json(new { error = ModelState.Values.SelectMany(v => v.Errors) });
        }
        [HttpGet]

        public async Task<ActionResult> PackagePayPalNewResponse(string paymentId, string PayerID, Guid payer, string retUrl, decimal eurPrice, string currency, decimal eurbonus, short? discount, decimal bonus, decimal amount, int duration, int timeOfLessons, TypeLessons type, string typeOfPrice,int countLessons)
        {
            var builder = new PaymentBuilder();
            var result = builder.UseRemoteProvider().CheckPayment(PayerID, paymentId);
            if(result == null)
                return RedirectToAction("NewMailPage", "Openlang");
            //var paymentExecution = new PaymentExecution() { payer_id = PayerID };
            //var payment = new Payment() { id = paymentId };
            //var executedPayment = payment.Execute(GetApiContext, paymentExecution);
            //if (executedPayment == null)
            //    return RedirectToAction("NewMailPage", "Openlang");
            var newpayment = new PaymentTransactions()
            {
                Amount = result.Amount,
                Approved = true,
                AttendeeId = payer,
                CurrenceAmount = result.CurrencyAmount,
                CreateDate = DateTime.UtcNow,
                PayerId = PayerID,
                PaymentMethod = "PayPal",
                PaymentId = paymentId,
                DiscountId = discount,
                EurPrice = eurPrice - eurbonus,
                Bonus = eurbonus,
                OrderDetailsId =
                    new OrderDetails()
                    {
                        ChLang = "English_us",
                        CountLessons = -1,
                        SelectedLevel = "Underfind",
                        StartDate = DateTime.Now,
                        StartTime = "00:00",
                        CoursesSpent = 0,
                        UtcDateTime = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                        SelectedLanguage = "Underfind"
                    },
                Credit = new Domain.Entities.Credit()
                {
                    StudentId = payer, CountLessons = (short)countLessons, Duration = (short)duration, StartDate = DateTime.UtcNow, EndDate = DateTime.UtcNow.AddMonths(duration), LeftLessons = (short)countLessons, TypeOfPrice = typeOfPrice, Type = type, TimeOfLessons = (short)timeOfLessons, PricePerLesson = eurPrice / countLessons
                }
            };
            using (var context = new MyDbContext())
            {
                using (var referal  = new RefferalProgrammComponent.RefferalProgramm(payer))
                    await referal.InitRefferalsIfExst(new SendDiscountCodeOnItem(), new AddPackagesToRefferedItem());
                context.PaymentTransactions.Add(newpayment);
                var user = context.Users.Find(payer);
                user.Money += eurPrice + eurbonus;
                user.InitializeCurrentCurrency(result.Currency);
                await context.SaveChangesAsync();
                var emailSender = new EmailSender.EmailSendClient();
                emailSender.SendHtmlAsync(user.Id, MailSenderUrlGenerator.MailSenderToppedUpBalanceForStudent("", Request.Url.Host, newpayment).ToString(), "Your Open Languages Lessons Schedule", EmailSender.MessageStatus.PaymentCompleted);
                emailSender.SendHtmlForAdminAsync(MailSenderUrlGenerator.MailSenderToppedUpBalanceForAdmin(Request.Url.Host, user, newpayment).ToString(), "Student has been topped up balance", EmailSender.MessageStatus.Empty);
            }
            Response.Cookies.Add(new HttpCookie("showstep3") {Path = "/", Value = JsonConvert.SerializeObject(new {amount, bonus})});
            return Redirect(retUrl);
        }

        public async Task<ActionResult> MembershipPayPalNewResponse(string token, Guid payer, string planId,
            string retUrl, decimal eurPrice, string currency, decimal eurbonus, short? discount, decimal bonus,
            decimal amount, int duration, int timeOfLessons, TypeLessons type, string typeOfPrice, int countLessons)
        {
            var cache = MemoryCache.Default;
            var builder = new PaymentBuilder();
            dynamic obj = builder.UseRemoteProvider().GetSubscriptionDetails(token);
            var plId = cache.Get(token);
            if (plId == null)
            {
                Response.StatusCode = 400;
                return Redirect(retUrl);
            }
            var PayerID = obj.payer.payer_info.payer_id;
            var result = new CheckPaymentResponse() { Amount = amount, Currency = currency };
            var newpayment = new PaymentTransactions()
            {
                Amount = result.Amount,
                Approved = true,
                AttendeeId = payer,
                CurrenceAmount = result.CurrencyAmount,
                CreateDate = DateTime.UtcNow,
                PayerId = PayerID,
                PaymentMethod = "PayPal",
                PaymentId = "Pay-" + Guid.NewGuid(),
                DiscountId = discount,
                EurPrice = eurPrice - eurbonus,
                Bonus = eurbonus,
                OrderDetailsId =
                    new OrderDetails()
                    {
                        ChLang = "English_us",
                        CountLessons = -1,
                        SelectedLevel = "Underfind",
                        StartDate = DateTime.Now,
                        StartTime = "00:00",
                        CoursesSpent = 0,
                        UtcDateTime = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                        SelectedLanguage = "Underfind"
                    },
                StripeSubscription = new StripeSubscription()
                {
                    Active = true, Amount = result.Amount, CountLessons = (short)countLessons, Currency = currency, CustomerId = PayerID, EurAmount = eurPrice, TypeProvider = "PayPal", SubscribeId = obj.id, PlanId = plId.ToString()
                },
                Credit = new Domain.Entities.Credit()
                {
                    StudentId = payer,
                    CountLessons = (short)countLessons,
                    Duration = (short)duration,
                    StartDate = DateTime.UtcNow,
                    EndDate = DateTime.UtcNow.AddMonths(duration),
                    LeftLessons = (short)countLessons,
                    TypeOfPrice = typeOfPrice,
                    Type = type,
                    TimeOfLessons = (short)timeOfLessons,
                    PricePerLesson = eurPrice / countLessons
                }
            };
            using (var context = new MyDbContext())
            {
                using (var referal = new RefferalProgrammComponent.RefferalProgramm(payer))
                    await referal.InitRefferalsIfExst(new SendDiscountCodeOnItem(), new AddPackagesToRefferedItem());
                context.PaymentTransactions.Add(newpayment);
                var user = context.Users.Find(payer);
                user.InitializeCurrentCurrency(result.Currency);
                await context.SaveChangesAsync();
                var emailSender = new EmailSender.EmailSendClient();
                emailSender.SendHtmlAsync(user.Id, MailSenderUrlGenerator.MailSenderToppedUpBalanceForStudent("", Request.Url.Host, newpayment).ToString(), "Your Open Languages Lessons Schedule", EmailSender.MessageStatus.PaymentCompleted);
                emailSender.SendHtmlForAdminAsync(MailSenderUrlGenerator.MailSenderToppedUpBalanceForAdmin(Request.Url.Host, user, newpayment).ToString(), "Student has been topped up balance", EmailSender.MessageStatus.Empty);
            }
            Response.Cookies.Add(new HttpCookie("showstep3") { Path = "/", Value = JsonConvert.SerializeObject(new { amount, bonus }) });
            return Redirect(retUrl);
        }

        [System.Web.Mvc.Authorize]
        public ActionResult ThankYou()
        {
            if (!TempData.ContainsKey("redirect"))
                return View("~/Views/openlang/ThankYouPayPal.cshtml");
            //return RedirectToAction("NewMailPage", "Openlang");
            ViewBag.Redirect = TempData["redirect"];
            TempData.Remove("redirect");
            return View("~/Views/openlang/ThankYouPayPal.cshtml");
        }
    }
}