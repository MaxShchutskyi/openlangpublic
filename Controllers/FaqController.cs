﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using Domain;
using Microsoft.AspNet.Identity;
using Xencore.Entities;
using Xencore.Filters;
using Xencore.Helpers;
using Xencore.Repositories;
using Xencore.Repository;
using Xencore.Services;

namespace Xencore.Controllers
{
    [Culture]
    public class FaqController : TemplateOpenlangController
    {
        [GlobalUserDetect]
        public async Task<ActionResult> FAQ(string request)
        {
            if (request != null)
                ViewBag.Request = request;
            await NewAutorizingLogic();
            //CheckUserAutorizeing();
            return View(new FAQRepository().GetQuestions());
        }
        [HttpPost]
        [WebMethod]
        [Culture(DisableAction = true)]
        public JsonResult ThumbUpOrDown(int id, bool isHelp)
        {
            if (new FAQRepository().Thumb(id, isHelp))
            {
                Response.StatusCode = 200;
                return Json(new { response = "Success thumb" });
            }
            Response.StatusCode = 400;
            return Json(new { response = "Error in adding thumb" });
        }
        [HttpPost]
        public async Task<ActionResult> FAQ(AnyQuestion quest)
        {
            string path;
            if (ModelState.IsValid)
            {
                var result = new AnyQuestionRepository().Add(quest);
                if (!result) return RedirectToAction("FAQ", new { request = "Error to save request" });
                var file = Request.Files["File"];
                path = Server.MapPath("~/Content/ClassImages/") + quest.Id + "_" + file?.FileName;
                file?.SaveAs(path);
            }
            else
                return RedirectToAction("FAQ", new { request = "Unvalid inputted data" });
            var emailToUser = new MailSenterForFaqRequest("Your request for Openlanguages.com", Request.Url.Host, quest).Send();
            var emailToAdmin = new NativeEmailService.NativeEmailService().SendAsync(new IdentityMessage()
            {
                Body = new StringBuilder().Append($"<div><h3>From:</h3> {quest.Email}</div>")
                    .Append($"<div><h3>Description:</h3> {quest.Description}</div>")
                    .Append($"<div><h3>Subject:</h3> {quest.Subject}</div>")
                    .Append($"<div><h3>File:</h3> {path}</div>").ToString(),
                Destination = SettingsAppHepler.GetAdminEmail(),
                Subject = "Created new question"
            });
            await Task.WhenAll(emailToUser, emailToAdmin);
            return RedirectToAction("FAQ", new { request = "Your request send successful" });
        }
    }
}