﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Services;
using Microsoft.AspNet.Identity;
using Xencore.Filters;
using Xencore.Helpers;

namespace Xencore.Controllers
{
    [Culture]
    public class ContactUsController : TemplateOpenlangController
    {
        [GlobalUserDetect]
        public async Task<ActionResult> ContactUs()
        {
            await NewAutorizingLogic();
            return View();
            //return View("~/Views/newLayouts/contact.cshtml");
        }
        [HttpPost]
        [WebMethod]
        [Culture(DisableAction = true)]
        public async Task<JsonResult> SendMessage(string name, string email, string subject, string text)
        {
            var service = new NativeEmailService.NativeEmailService();
            var builder = new StringBuilder();
            builder.Append("Name:")
                .Append(name)
                .Append("<br>")
                .Append("From:")
                .Append(email)
                .Append("<br>")
                .Append("Subject:")
                .Append(subject)
                .Append("<br>")
                .Append(text);
            try
            {
                var message = new IdentityMessage()
                {
                    Body = builder.ToString(),
                    Destination = SettingsAppHepler.GetAdminEmail(),
                    Subject = subject
                };
                await service.SendAsync(message);
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
            Response.StatusCode = 200;
            return Json(new { success = true, responseText = "Success" }, JsonRequestBehavior.AllowGet);
        }
    }
}