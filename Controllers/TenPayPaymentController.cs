﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Services;
using System.Web.UI;
using Domain.Context;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NLog;
using Xencore.Entities.IdentityEntities;
using Xencore.Helpers;
using Xencore.Repositories;
using Xencore.Services;

namespace Xencore.Controllers
{
    public class TenPayPaymentController : TemplateOpenlangController
    {
        [Authorize]
        [WebMethod]
        [HttpPost]
        public async Task<string> MakePayment(PaymentDetailsHelper paymentDetails, string advance, int dateId, int? discountId)
        {
            //string partner = "1219657301";
            //string key = "e9b77af2ec9e772a8c960c23f74fdf07";

            //string out_trade_no = "" + DateTime.Now.ToString("HHmmss") + TenpayUtil.BuildRandomStr(4);
            //var feetype = "USD";
            //// https://gw.tenpay.com/intl/gateway/pay.htm?attach=123&bank_type=DEFAULT&body=%E6%B5%8B%E8%AF%95&fee_type=USD&input_charset=GBK&notify_url=http%3a%2f%2fwww.openlanguages.com%2fen%2fhome%2f1048&out_trade_no=1935283857&partner=1219657301&product_fee=099&return_url=http%3a%2f%2fwww.openlanguages.com%2fen%2fhome%2f%3fcpid%3d1048&service_version=1.0&sign=2ac8b054d51ae238d536696a783197c7&sign_key_index=1&sign_type=MD5&spbill_create_ip=127.0.0.1&time_expire=&time_start=20160117193528&total_fee=099&transport_fee=0
            //var totalfee = paymentDetails.Amount.ToString("F").Replace(",", String.Empty).Replace(".", String.Empty);
            ////var totalfee = 0.99.ToString("F").Replace(",", String.Empty).Replace(".", String.Empty);
            //var reqHandler = new RequestHandler(System.Web.HttpContext.Current);
            //var slice = advance.Split(' ');
            //Response.TrySkipIisCustomErrors = true;
            //paymentDetails.StartTime = paymentDetails.StartTime?.Replace(" ", "");
            //var id = "TenPay_" + Guid.NewGuid();
            //if (ModelState.IsValid)
            //{
            //    var userId = HttpContext.User.Identity.GetUserId();
            //    var repo = new PaymentTransactionRepository();
            //    repo.Add(userId, paymentDetails,id, "TenPay", advance, dateId, discountId);
            //    var manager = HttpContext.GetOwinContext().Get<CustomUserManager>();
            //    var usr = await manager.FindByIdAsync(Guid.Parse(userId));
            //    await ChangeILearn.ChangeAsync(usr, manager, paymentDetails.ChLang.Split('_')[1]);
            //}
            //reqHandler.init();
            //reqHandler.setKey(key);
            //reqHandler.setGateUrl("https://gw.tenpay.com/intl/gateway/pay.htm");
            //reqHandler.setParameter("total_fee", totalfee);
            //reqHandler.setParameter("fee_type", feetype);

            //reqHandler.setParameter("spbill_create_ip", "127.0.0.1");
            //reqHandler.setParameter("return_url", $"{Request.Url.Scheme}://" + Request.Url.Authority + "/TenPayPayment/SuccessResult?cid="+id + "&retUrl="+Request.UrlReferrer);
            //reqHandler.setParameter("partner", partner);
            //reqHandler.setParameter("out_trade_no", out_trade_no);
            //reqHandler.setParameter("notify_url", $"{Request.Url.Scheme}://" + Request.Url.Authority  + "/TenPayPayment/SuccessResult?cid=" + id + "&retUrl=" + Request.UrlReferrer);
            //reqHandler.setParameter("attach", "123");
            //reqHandler.setParameter("body", "测试");
            //reqHandler.setParameter("bank_type", "DEFAULT");

            //reqHandler.setParameter("sign_type", "MD5");

            //reqHandler.setParameter("product_fee", totalfee);
            //return reqHandler.getRequestURL();
            return null;
        }
        public async Task<JsonResult> SuccessResult(string cid, string retUrl)
        {
            //LogManager.GetCurrentClassLogger().Info("In Success mathod");
            //LogManager.GetCurrentClassLogger().Info(cid);
            //Response.TrySkipIisCustomErrors = true;
            //string partner = "1219657301";
            //string key = "e9b77af2ec9e772a8c960c23f74fdf07";
            ////string partner = "1900000109";
            ////string key = "8934e7d15453e97507ef794cf7b0519d";
            //ResponseHandler resHandler = new ResponseHandler(System.Web.HttpContext.Current);
            //resHandler.setKey(key);
            //if (resHandler.isTenpaySign())
            //{
            //    string notify_id = resHandler.getParameter("notify_id");
            //    RequestHandler queryReq = new RequestHandler(System.Web.HttpContext.Current);
            //    queryReq.init();
            //    queryReq.setKey(key);
            //    queryReq.setGateUrl("https://gw.tenpay.com/gateway/verifynotifyid.xml");
            //    queryReq.setParameter("partner", partner);
            //    queryReq.setParameter("notify_id", notify_id);

            //    TenpayHttpClient httpClient = new TenpayHttpClient();
            //    httpClient.setTimeOut(5);
            //    httpClient.setReqContent(queryReq.getRequestURL());
            //    if (httpClient.call())
            //    {
            //        LogManager.GetCurrentClassLogger().Info("After call");
            //        ClientResponseHandler queryRes = new ClientResponseHandler();
            //        queryRes.setContent(httpClient.getResContent());
            //        queryRes.setKey(key);
            //        if (queryRes.isTenpaySign() && queryRes.getParameter("retcode") == "0" && queryRes.getParameter("trade_state") == "0" && queryRes.getParameter("trade_mode") == "1")
            //        {
            //            LogManager.GetCurrentClassLogger().Info("Finish");
            //            string out_trade_no = queryRes.getParameter("out_trade_no");
            //            string transaction_id = queryRes.getParameter("transaction_id");
            //            string total_fee = queryRes.getParameter("total_fee");
            //            string discount = queryRes.getParameter("discount");
            //            using (var context = new MyDbContext())
            //            {
            //                var pay = context.PaymentTransactions.Where(x => x.PaymentId == cid).Include(x=>x.OrderDetailsId).Include(x=>x.CustomUserId).FirstOrDefault();
            //                if (pay != null)
            //                    pay.Approved = true;
            //                context.PaymentTransactions.AddOrUpdate(pay);
            //                context.SaveChanges();
            //                if (pay.OrderDetailsId.CourseBoughtFromId != null)
            //                    await SendMeessageIntoChatAfterBoutghtACourse(pay, pay.OrderDetailsId.CourseBoughtFromId ?? Guid.NewGuid());
            //                LogManager.GetCurrentClassLogger().Info("WIN!!");
            //            }
            //            Response.StatusCode = 200;
            //            return Json(new {Success = "Success pay"});

            //        }
            //        else
            //        {
            //            Response.StatusCode = 400;
            //            return Json(new { error = "Error pay" });
            //        }


            //    }
            //    else
            //    {
            //        Response.StatusCode = 400;
            //        return Json(new { error = httpClient.getErrInfo() });

            //    }

            //}

            //Response.Redirect($"{Request.Url.Scheme}://" + Request.Url.Authority + "/openlang/LandingSite");
            return null;
        }
    }
}