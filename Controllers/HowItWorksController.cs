﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Xencore.Filters;

namespace Xencore.Controllers
{
    [Culture]
    public class HowItWorksController : TemplateOpenlangController
    {
        [GlobalUserDetect]
        // GET: HowItWorks
        public async Task<ActionResult> Index()
        {
            await NewAutorizingLogic();
            return View();
        }
    }
}