﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Entities.IdentityEntities;
using Microsoft.AspNet.Identity.Owin;
using Xencore.Entities.IdentityEntities;
using Xencore.Filters;

namespace Xencore.Controllers
{
    [Culture(DisableAction = true)]
    [Authorize(Roles = "Admin")]
    public class LoginAsController : Controller
    {
        private CustomUserManager UserManager => HttpContext.GetOwinContext().Get<CustomUserManager>();
        private CustomSignIn SignInManager => HttpContext.GetOwinContext().Get<CustomSignIn>();
        private string Key { get; } = "2eaf2d71-7d56-4b90-b573-83e9e374efba";
        // GET: LoginAs
        public async Task<ActionResult> Index(string email,string key)
        {
            if(key == null || email == null) return RedirectToAction("NewMailPage", "Openlang");
            if (key != Key) return RedirectToAction("NewMailPage", "Openlang");
            var user = await UserManager.FindByEmailAsync(email);
            if(user == null) return RedirectToAction("NewMailPage", "Openlang");
            var sign = await user.GenerateUserIdentityAsync(UserManager);
            Request.GetOwinContext().Authentication.SignIn(sign);
            return RedirectToAction("NewMailPage", "Openlang");
        }
    }
}