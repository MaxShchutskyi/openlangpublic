﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Context;
using Domain.Entities;
using Domain.Helpers;
using Domain.Helpers.Additionals;
using Domain.Repositories;
using ImageSaver;
using ImageSaver.Converters;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.Provider;
using NAudio.Lame;
using NAudio.Wave;
using Xencore.AdditionalsActionResults;
using Xencore.Extentions;

namespace Xencore.Controllers
{
    public class UploadImagesController : TemplateOpenlangController
    {
        private Guid UserId=> Guid.Parse(User.Identity.GetUserId());
        [Authorize]
        public async Task<JsonResult> UploadAvatar()
        {
            var files = Request.Files;
            if (files.Count == 0)
                return Json(new {error = "You have not uploeded image"});
            var file = files[0];
            var serverpath = Server.MapPath($"~/Images/teacher-avatar/{UserId}");
            var filename = Guid.NewGuid() + "_" + file.FileName;
            var directory = $"/Images/teacher-avatar/{UserId}/";
            var saver = new FileSaver(serverpath, null, filename);
            saver.Save(file.InputStream, new ScaleConterter(300,300));
            var repo = new AttendeeRepository();
            await repo.ChangeProfilePhotoAsync(UserId, Path.Combine(directory, filename));
            User.AddUpdateClaim("ProfilePicture", Path.Combine(directory, filename));
            return Json(new {path = Path.Combine(directory, filename)});
        }
        [HttpPost]
        public async Task<JsonResult> UploadAssingment(int id)
        {
            var userid = Guid.Parse(User.Identity.GetUserId());
            var files = Request.Files;
            if (files.Count == 0)
                return Json(new {path = ""});
            //if (!Directory.Exists(Server.MapPath($"~/Images/Assingments/{userid}")))
            //    Directory.CreateDirectory(Server.MapPath($"~/Images/Assingments/{userid}"));
            var serverpath = Server.MapPath($"~/Images/Assingments/{userid}");
            var directory = $"/Images/Assingments/{userid}/";
            var file = files[0];
            var filename = Guid.NewGuid() + "_" + file.FileName;
            var fileSaver = new FileSaver(serverpath,null, filename);
            fileSaver.Save(file.InputStream, new ScaleConterter(300, 300));
            using (var context = new MyDbContext())
            {
                var ass = context.MyAssingnmentses.Find(id);
                if(System.IO.File.Exists(Server.MapPath($"~") + ass.Image) && !ass.Image.Contains("assig-img2"))
                    System.IO.File.Delete(Server.MapPath($"~") + ass.Image);
                ass.Image = Path.Combine(directory, filename);
                await context.SaveChangesAsync();
            }
            return Json(new { path = Path.Combine(directory, filename) });
        }

        [HttpPost]
        public async Task<JsonResult> UploadAssingmentFile(int id)
        {
            var userid = Guid.Parse(User.Identity.GetUserId());
            var files = Request.Files;
            if (files.Count == 0)
                return Json(new { path = "" });
            var serverpath = Server.MapPath($"~/Images/Assingments/{userid}/Files");
            var directory = $"/Images/Assingments/{userid}/Files/";
            var file = files[0];
            var filename = Guid.NewGuid() + "_" + file.FileName;
            var fileSaver = new FileSaver(serverpath, null, filename);
            fileSaver.Save(file.InputStream, new ScaleConterter(300, 300));
            using (var context = new MyDbContext())
            {
                var ass = context.MyAssingnmentses.Find(id);
                if (System.IO.File.Exists(Server.MapPath($"~") + ass.UploadFilePath))
                    System.IO.File.Delete(Server.MapPath($"~") + ass.UploadFilePath);
                ass.UploadFilePath = Path.Combine(directory, filename);
                await context.SaveChangesAsync();
            }
            return Json(new { path = Path.Combine(directory, filename) });
        }
        [HttpPost]
        public async Task<JsonResult> UploadWordImage(int id)
        {
            var userid = Guid.Parse(User.Identity.GetUserId());
            var files = Request.Files;
            if (files.Count == 0)
                return Json(new { path = "" });
            var serverpath = Server.MapPath($"~/Images/Words/{userid}");
            var directory = $"/Images/Words/{userid}/";
            var file = files[0];
            var filename = Guid.NewGuid() + "_" + file.FileName;
            var fileSaver = new FileSaver(serverpath, null, filename);
            fileSaver.Save(file.InputStream, new ScaleConterter(300, 300));
            using (var context = new MyDbContext())
            {
                var ass = context.MyWords.Find(id);
                if ((System.IO.File.Exists(Server.MapPath($"~") + ass.Image) && !ass.Image.Contains("assig-img2.png")))
                    System.IO.File.Delete(Server.MapPath($"~") + ass.Image);
                ass.Image = Path.Combine(directory, filename);
                await context.SaveChangesAsync();
            }
            return Json(new { path = Path.Combine(directory, filename) });
        }
        [HttpPost]
        public async Task<JsonResult> UploadAudioMessageFile(int id)
        {
            var userid = Guid.Parse(User.Identity.GetUserId());
            var files = Request.Files;
            if (files.Count == 0)
                return Json(new { path = "" });
            var serverpath = Server.MapPath($"~/Images/Assingments/{userid}/Files/Audio");
            var directory = $"/Images/Assingments/{userid}/Files/Audio/";
            var filename = Guid.NewGuid() + "_AudioMessage.mp3";
            var fileSaver = new FileSaver(serverpath, null, filename);
            fileSaver.Save(files[0].InputStream, new AudioConverter());
            using (var context = new MyDbContext())
            {
                var ass = context.MyAssingnmentses.Find(id);
                if ((System.IO.File.Exists(Server.MapPath($"~") + ass.UploadMessagePath)))
                    System.IO.File.Delete(Server.MapPath($"~") + ass.UploadMessagePath);
                ass.UploadMessagePath = Path.Combine(directory, filename);
                await context.SaveChangesAsync();
            }
            return Json(new { path = Path.Combine(directory, filename) });
        }

        [HttpPost]
        public JsonResult UploadAudioPath()
        {
            var userid = Guid.Parse(User.Identity.GetUserId());
            var files = Request.Files;
            if (files.Count == 0)
                return Json(new { path = "" });
            var serverpath = Server.MapPath($"~/Images/Files/Audio");
            if (!Directory.Exists(serverpath))
                Directory.CreateDirectory(serverpath);
            var directory = $"/Images/Files/Audio/";
            var filename = Guid.NewGuid() + ".mp3";
            var fileSaver = new FileSaver(serverpath, null, filename);
            fileSaver.Save(files[0].InputStream, new AudioConverter());
            return Json(new { path = Path.Combine(directory, filename) });
        }
        [HttpPost]
        public async Task<JsonResult> UploadWordAudioMessageFile(int id)
        {
            var userid = Guid.Parse(User.Identity.GetUserId());
            var files = Request.Files;
            if (files.Count == 0)
                return Json(new { path = "" });
            var serverpath = Server.MapPath($"~/Images/Words/{userid}/Files/Audio");
            var directory = $"/Images/Words/{userid}/Files/Audio/";
            var filename = Guid.NewGuid() + "_AudioMessage.mp3";
            var fileSaver = new FileSaver(serverpath, null, filename);
            fileSaver.Save(files[0].InputStream, new AudioConverter());
            using (var context = new MyDbContext())
            {
                var ass = context.MyWords.Find(id);
                if ((System.IO.File.Exists(Server.MapPath($"~") + ass.AudioPath)))
                    System.IO.File.Delete(Server.MapPath($"~") + ass.AudioPath);
                ass.AudioPath = Path.Combine(directory, filename);
                await context.SaveChangesAsync();
            }
            return Json(new { path = Path.Combine(directory, filename) });
        }
    }
}