﻿
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using AutoMapper;
using Domain.Entities;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;
using Domain.Helpers;
using DTO.Enums;
using GeolocationManager.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Xencore.EmailSender;
using Xencore.Entities;
using Xencore.Entities.IdentityEntities;
using Xencore.Filters;
using Xencore.Helpers;
using Xencore.Managers.TablesOfPrices;
using Xencore.Repositories;
using Xencore.Services;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Xencore.Controllers
{
    [Culture]
    public class ForteachersController : TemplateOpenlangController
    {
        [GlobalUserDetect]
        public async Task<ActionResult> Teachers(string response)
        {
            if (response != null)
                ViewBag.Response = Resources.Resource.YouAppWasSentSuccess;//Resources.Resource.YouAppWasSentSuccess;
            await NewAutorizingLogic();
            return View();
        }
        [HttpPost]
        [GlobalUserDetect]
        [Timezone]
        public async Task<ActionResult> PostTeacherAuthorize(TeacherApplicationHepler app, UserInfo usr, short timezone)
        {
            if (!ModelState.IsValid) return RedirectToAction("Teachers");
            var user = Mapper.Map<CustomUser>(app);
            user.Ilearn = "cap_" + app.Language + "|";
            user.Active = false;
            user.SetMainParameters(usr, timezone);
            user.TimeZone = usr.Timezone;
            var result = await GetUserManager.CreateAsync(user, app.Password);
            //await GetUserManager.AddClaimAsync(user.Id, new System.Security.Claims.Claim("BeforeActivate", "first"));
            if (result.Succeeded)
                await GetUserManager.AddToRoleAsync(user.Id, "Presenter");
            var claims = await user.GenerateUserIdentityAsync(GetUserManager);
            System.Web.HttpContext.Current.GetOwinContext().Authentication.SignIn(claims);
            var emailsender = new EmailSender.EmailSendClient();
            emailsender.SendForAdminAsync(MailSenderUrlGenerator.MailSenderSuccessTeacherSignIn(app),
                "New Teacher Registrated", EmailSender.MessageStatus.Empty);
            emailsender.SendHtmlAsync(user.Id, MailSenderUrlGenerator.MailSenderForTeachApplay(Request.Url.Host, app).ToString(), "Your Teacher Application created successful", EmailSender.MessageStatus.UserRegistrated);
            //await ApplyTeacher(app);
            //HttpContext.Response.Cookies.Add(new HttpCookie("teacherSuccess", "Success") { Path = @"/" });
            return RedirectToAction("GetBearerToken", "Openlang");
            //return RedirectToAction("Teachers", "Forteachers", new {response = "Success"});
            //менеджер по ипотеке Лилия Алексашина

        }
        //[HttpPost]
        //[Authorize]
        //public async Task<ActionResult> PostTeacherAuthorizeAuthor(TeacherApplicationHepler app)
        //{
        //    if (!ModelState.IsValid) return RedirectToAction("Teachers");
        //    var user = await GetUserManager.FindByIdAsync(Guid.Parse(User.Identity.GetUserId()));
        //    await GetUserManager.AddToRoleAsync(user.Id, "Presenter");
        //    user.TeacherInfo = Mapper.Map<TeacherInfo>(app);
        //    user.Active = false;
        //    await GetUserManager.UpdateAsync(user);
        //    await ApplyTeacher(app);
        //    HttpContext.GetOwinContext().Authentication.SignOut();
        //    await GetSignIn.SignInAsync(user, false, false);
        //    HttpContext.Response.Cookies.Add(new HttpCookie("teacherSuccess","Success") { Path = @"/" });
        //    return Redirect(Request.UrlReferrer.ToString());
        //    //return RedirectToAction("Teachers", "Forteachers", new { response = "Success" });

        //}
        //[NonAction]
        //private async Task ApplyTeacher(TeacherApplicationHepler app)
        //{
        //    var application = Mapper.Map<TeacherApplication>(app);
        //    var str = new StringBuilder().Append("<h3>New Teacher Application</h3>")
        //            .Append($"From : {app.Email} <br>")
        //            .Append($"Name : {application.Name} <br>")
        //            .Append($"Language : {application.Language} <br>")
        //            .Append($"Telephone : {application.Telephone} <br>")
        //            .Append($"Record teacher simple video : {app.Link} <br>")
        //            .Append($"Additional : {application.Additional} <br>")
        //            .Append($"Relevant : {application.Relevant} <br>");
        //        var sendForAdmin = new EmailService().SendAsync(new IdentityMessage()
        //        {
        //            Body = str.ToString(),
        //            Destination = SettingsAppHepler.GetAdminEmail(),
        //            Subject = "New Teacher Application"
        //        });
        //        var sendForTeacher = new MailSenderForTeachApplay("Your Teacher Application created successful",
        //            Request.Url.Host,app.Email, application).Send();
        //        await Task.WhenAll(sendForAdmin, sendForTeacher);
        //    }


    }
}