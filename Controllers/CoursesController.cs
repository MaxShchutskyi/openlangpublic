﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Xencore.Filters;

namespace Xencore.Controllers
{
    [Culture]
    public class CoursesController : TemplateOpenlangController
    {
        [GlobalUserDetect]
        public async Task<ActionResult> Courses()
        {
            //CheckUserAutorizeing();
            await NewAutorizingLogic();
            return View();
            //return View("~/Views/newLayouts/Courses.cshtml");
        }
    }
}