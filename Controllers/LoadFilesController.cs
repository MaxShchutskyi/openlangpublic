﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Services;
using Domain.Context;
using Domain.Entities;
using Microsoft.AspNet.Identity;
using Xencore.Filters;

namespace Xencore.Controllers
{
    [Authorize(Roles = "Presenter")]
    public class LoadFilesController : TemplateOpenlangController
    {
        [WebMethod]
        [HttpPost]
        public async Task<JsonResult> LoadImageForLesson()
        {
            var userid = Guid.Parse(User.Identity.GetUserId());
            var files = Request.Files;
            if (!Directory.Exists(Server.MapPath($"~/Images/teacher-lessons/{userid}")))
                Directory.CreateDirectory(Server.MapPath($"~/Images/teacher-lessons/{userid}"));
            var serverpath = Server.MapPath($"~/Images/teacher-lessons/{userid}");
            var directory = $"/Images/teacher-lessons/{userid}/";

            var pic = files[0];
            var fullName = Guid.NewGuid() + "_" + pic.FileName;
            var smallimg = ScaleImage(new Bitmap(pic.InputStream), 200, 200);
            var alldirectory = Path.Combine(serverpath, fullName);
            smallimg.Save(alldirectory);
            return Json(directory + fullName);
        }
        [WebMethod]
        [HttpPost]
        public async Task<JsonResult> LoadVideoForTheTeacher()
        {
            var userid = Guid.Parse(User.Identity.GetUserId());
            var files = Request.Files;
            if (!Directory.Exists(Server.MapPath($"~/Images/video/{userid}")))
                Directory.CreateDirectory(Server.MapPath($"~/Images/video/{userid}"));
            var serverpath = Server.MapPath($"~/Images/video/{userid}");
            var directory = $"/Images/video/{userid}/";
            foreach (var fl in new DirectoryInfo(serverpath).GetFiles())
            {
                fl.Delete();
            }
            var pic = files[0];
            if(pic.ContentType != "video/mp4")
                return Json(new {link = ""});
            var fullName = Guid.NewGuid() + "_" + pic.FileName;
            var alldirectory = Path.Combine(serverpath, fullName);
            pic.SaveAs(alldirectory);
            using (var context = new MyDbContext())
            {
                var info = await context.TeacherInfos.FindAsync(userid);
                info.Link = directory + fullName;
                await context.SaveChangesAsync();
            }
            return Json(new {link = directory + fullName});
        }
    }
}