﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using Facebook;
using Microsoft.AspNet.Identity;

namespace Xencore.Controllers
{
    public class MyAssignController : TemplateOpenlangController
    {
        private HttpPostedFileBase UploadFile { get; set; }
        public HttpPostedFileBase UploadImage { get; set; }
        public HttpPostedFileBase UploadMessage { get; set; }
        private Guid UserId { get; set; }
        private List<object> RetirnedResult { get; } = new List<object>();

        [WebMethod]
        [System.Web.Http.HttpPost]
        public JsonResult LoadFiles()
        {
            UserId = Guid.Parse(User.Identity.GetUserId());
            ParseFiles();
            if(UploadImage!=null)
                RetirnedResult.Add(new {ImagePath = SaveImage() });
            if (UploadFile != null)
                RetirnedResult.Add(new { UploadPath = SaveUpload() });
            if(UploadMessage!=null)
                RetirnedResult.Add(new { MessagePath = SaveMessage() });
            return Json(RetirnedResult);


        }
        [NonAction]
        private void ParseFiles()
        {
            foreach (string name in Request.Files)
            {
                var file = Request.Files[name];
                if (name.Contains("__IMAGE"))
                    UploadImage = file;
                if (name.Contains("__UPLOAD"))
                    UploadFile = file;
                if (name.Contains("__MESSAGE"))
                    UploadMessage = file;
            }
        }
        [NonAction]
        private string SaveImage()
        {
            var filename = UploadImage.FileName;
            if (!Directory.Exists(Server.MapPath($"~/Images/my-assingments/{UserId}")))
                Directory.CreateDirectory(Server.MapPath($"~/Images/assingments/{UserId}"));
            var serverpath = Server.MapPath($"~/Images/assingments/{UserId}");
            var directory = $"/Images/assingments/{UserId}/";
            var smallimg = ScaleImage(new Bitmap(UploadImage.InputStream), 60, 60);
            smallimg.Save(Path.Combine(serverpath, filename));
            return directory + filename;
            //return Json(new { directory = directory + $"{UploadImage.FileName}" });
        }
        [NonAction]
        private string SaveMessage()
        {
            var filename = Guid.NewGuid() + ".wav";
            if (!Directory.Exists(Server.MapPath($"~/UploadFiles/Assingments/{UserId}/Messages/")))
                Directory.CreateDirectory(Server.MapPath($"~/UploadFiles/Assingments/{UserId}/Messages/"));
            var serverpath = Server.MapPath($"~/UploadFiles/Assingments/{UserId}/Messages");
            var directory = $"/UploadFiles/Assingments/{UserId}/Messages/";
            UploadMessage.SaveAs(Path.Combine(serverpath, filename));
            return directory + filename;
            //return Json(new { directory = directory + $"{UploadImage.FileName}" });
        }

        private string SaveUpload()
        {
            var filename = Guid.NewGuid() + "__" +UploadFile.FileName;
            if (!Directory.Exists(Server.MapPath($"~/UploadFiles/Assingments/{UserId}")))
                Directory.CreateDirectory(Server.MapPath($"~/UploadFiles/Assingments//{UserId}"));
            var serverpath = Server.MapPath($"~/UploadFiles/Assingments/{UserId}");
            var directory = $"/UploadFiles/Assingments/{UserId}/";
            UploadFile.SaveAs(Path.Combine(serverpath, filename));
            return directory + filename;
        }
    }
}