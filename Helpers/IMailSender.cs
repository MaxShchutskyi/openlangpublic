﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xencore.Helpers
{
    public interface IMailSender
    {
        Task<bool> Send();
    }
}
