﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xencore.Helpers
{
    public static class SettingsAppHepler
    {
        public static string GetAdminEmail()
        {
            return System.Configuration.ConfigurationManager.AppSettings["adminEmail"];
        }

        public static string GetUsingPort()
        {
            return System.Configuration.ConfigurationManager.AppSettings["usingPort"];
        }

        public static T GetValue<T>(string key)
        {
            var val = System.Configuration.ConfigurationManager.AppSettings[key];
            try
            {
                var changeType = Convert.ChangeType(val, typeof(T));
                return (T) changeType;
            }
            catch (Exception)
            {
                return default(T);
            }
        }
        public static string GetNexmoKey=>System.Configuration.ConfigurationManager.AppSettings["NexmoKey"];
        public static string GetNexmoSecret => System.Configuration.ConfigurationManager.AppSettings["NexmoSecret"];
        public static string GetFacebookClientId=> System.Configuration.ConfigurationManager.AppSettings["FaceBookClientId"];
        public static string GetFacebookClientSecret => System.Configuration.ConfigurationManager.AppSettings["FaceBookClientISecret"];
    }
}
