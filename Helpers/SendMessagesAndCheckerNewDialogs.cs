﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.IdentityEntities;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Xencore.Managers.TablesOfPrices.Abstract;

namespace Xencore.Helpers
{
    public class SendMessagesAndCheckerNewDialogs
    {
        private static Guid AdminId { get; } = Guid.Parse("11111111-1111-1111-1111-111111111111");
        public async Task SendMeessageIntoChatAfterBoutghtACourse(CustomUser studentId, CustomUser teacher, MyDbContext context, Schedule schedule)
        {
            var checkUser = await CreateDialogIfNotExist(studentId.Id, AdminId, context);
            var checkTeacher = await CreateDialogIfNotExist(teacher.Id, AdminId, context);
            await CreateDialogIfNotExist(studentId.Id, teacher.Id);
            var messToStudent =
                $"You have just binded to lesson on {schedule.StartDateTimeDateTime:f} UTC. Level - {Resources.Resource.ResourceManager.GetString(schedule.Level)}. Language - {Resources.Resource.ResourceManager.GetString(schedule.Language)}. Teacher - {teacher.Name}";
            var messToTeacher =
                $"{studentId.Name} have just binded to lesson on {schedule.StartDateTimeDateTime:f} UTC. Level - {Resources.Resource.ResourceManager.GetString(schedule.Level)}. Language - {Resources.Resource.ResourceManager.GetString(schedule.Language)}.";
            var messId = await SendMessage(AdminId, checkUser, () => messToStudent, context);
            var messId2 = await SendMessage(AdminId, checkTeacher, () => messToTeacher, context);
            SendMessageToHub(teacher.Id.ToString(), messToTeacher, messId2, checkTeacher);
            SendMessageToHub(studentId.ToString(), messToStudent, messId, checkUser);
        }
        static async Task<int> CreateDialogIfNotExist(Guid userOne, Guid userTwo, MyDbContext context = null)
        {
            if (context == null)
                context = new MyDbContext();
            var dialog = context.Dialogs.FirstOrDefault(
                x =>
                    (x.UserOneId == userOne && x.UserTwoId == userTwo) ||
                    (x.UserOneId == userTwo && x.UserTwoId == userOne));
            if (dialog != null) return dialog.Id;
            dialog = new Dialog() { UserOneId = userOne, UserTwoId = userTwo };
            context.Dialogs.Add(dialog);
            await context.SaveChangesAsync();
            return dialog.Id;
        }
        static async Task<int> SendMessage(Guid from, int dialogId, Func<string> messageText, MyDbContext context = null)
        {
            if (context == null)
                context = new MyDbContext();
            var dialogreply = new DialogReply() { UserId = from, DialogId = dialogId, Message = messageText() };
            context.DialogReplies.Add(dialogreply);
            await context.SaveChangesAsync();
            return dialogreply.Id;
        }
        static void SendMessageToHub(string to, string message, int dialogreplyid, int dialogid)
        {
            var from =
                new
                {
                    id = AdminId,
                    imageProfile = "/images/default-img.png",
                    name = "Service"
                };
            var hub = GlobalHost.ConnectionManager.GetHubContext<HubsOfSignalR.ChatHub>();
            hub.Clients.User(to).send(message, JsonConvert.SerializeObject(from), dialogreplyid, dialogid);
        }
    }
}