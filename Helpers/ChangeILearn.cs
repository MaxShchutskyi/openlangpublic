﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Context;
using Domain.Entities.IdentityEntities;

using Xencore.Entities.IdentityEntities;

namespace Xencore.Helpers
{
    public static class ChangeILearn
    {
        public static async Task ChangeAsync(CustomUser user, CustomUserManager manager, string responsePrefix)
        {
            if(CheckAndUpdateUser(user, responsePrefix))
                await manager.UpdateAsync(user);
        }

        public static async Task ChangeAsync(CustomUser user, MyDbContext context, string responsePrefix)
        {
            if(CheckAndUpdateUser(user,responsePrefix))
                context.Users.AddOrUpdate(user);
                await context.SaveChangesAsync();
        }

        private static bool CheckAndUpdateUser(CustomUser user, string responsePrefix)
        {
            if (user.Ilearn == null)
            {
                user.Ilearn = LanguagesHepler.Languages[responsePrefix].Ilearn + "|";
                return true;
            }
            var ilearnArr = user.Ilearn.Split('|').Where(x=>!string.IsNullOrEmpty(x)).ToList();
            if (ilearnArr.Select(learn => learn.Split('_')[1]).Any(prefix => prefix == responsePrefix)) return false;
            user.Ilearn += LanguagesHepler.Languages[responsePrefix].Ilearn + "|";
            return true;
        }
    }
}
