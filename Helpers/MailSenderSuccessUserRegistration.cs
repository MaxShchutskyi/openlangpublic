﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Xencore.Helpers
{
    public class MailSenderSuccessUserRegistration: AbstractMailSender
    {
        protected override UriBuilder Uri { get; set; }
        protected override string Destination { get; set; }

        public MailSenderSuccessUserRegistration(string subject,string host, string destination, string userName)
        {
            Subject = subject;
            Uri = new UriBuilder()
            {
                Host = host,
                Scheme = "http",
                Port = int.Parse(SettingsAppHepler.GetUsingPort()),
                Path = "/Mailer/SuccessUserRegistration",
                Query = new StringBuilder().
                    Append($"userName={userName}").ToString()
            };
            Destination = destination;
        }
    }
}