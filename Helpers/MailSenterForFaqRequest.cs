﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Xencore.Entities;
using Xencore.Entities.IdentityEntities.Entities;

namespace Xencore.Helpers
{
    public class MailSenterForFaqRequest:AbstractMailSender
    {
        protected override sealed UriBuilder Uri { get; set; }
        protected override sealed string Destination { get; set; }
        public MailSenterForFaqRequest(string subject, string host, AnyQuestion emailText)
        {
            Subject = subject;
            Uri = new UriBuilder()
            {
                Host = host,
                Scheme = "http",
                Port = int.Parse(SettingsAppHepler.GetUsingPort()),
                Path = "/Mailer/GetForFaqRequest",
                Query = new StringBuilder().
                    Append($"EmailText={emailText.Description}").
                Append($"&Subject={emailText.Subject}").ToString()

            };
            Destination = emailText.Email;
        }
    }
}
