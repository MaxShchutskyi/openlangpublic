﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Entities.IdentityEntities;
using Domain.Entities;
using Domain.Entities.HelpersEntity;

namespace Xencore.Helpers
{
    public static class MailSenderUrlGenerator
    {
        public static UriBuilder GetStringBuilerUrlForNewRegistration(string host, CustomUser user, string destination)
        {
            var Uri = new UriBuilder()
            {
                Host = host,
                Scheme = "http",
                Port = int.Parse(SettingsAppHepler.GetUsingPort()),
                Path = "/Mailer/GetForNewRegistration",
                Query = new StringBuilder().
                    Append($"hui={user.UserName}").ToString()
            };
            return Uri;
        }
        public static UriBuilder MailSenderSuccessUserRegistration(string host, string userName, Guid? id = null)
        {
            var url = new UriBuilder()
            {
                Host = host,
                Scheme = "http",
                Port = int.Parse(SettingsAppHepler.GetUsingPort()),
                Path = "/Mailer/SuccessUserRegistration",
                Query = new StringBuilder().
                    Append($"userName={userName}").
                    Append($"id={id}").ToString()
            };
            return url;
        }

        public static UriBuilder MailSenderForTeachApplay(string host, TeacherApplicationHepler application)
        {
            return new UriBuilder()
            {
                Host = host,
                Scheme = "http",
                Port = int.Parse(SettingsAppHepler.GetUsingPort()),
                Path = "/Mailer/GetMailForTeacher",
                Query = new StringBuilder($"&Name={application.Name}")
                    .Append($"&Telephone={application.Telephone}")
                    .Append($"&Link={application.Link}")
                    .Append($"&Language={application.Language}").ToString()
            };
        }

        public static string MailSenderSuccessTeacherSignIn(TeacherApplicationHepler userName)
        {
            var str = new StringBuilder().Append("<h3>New Teacher Application</h3>")
                .Append($"From : {userName.Email} <br>")
                .Append($"Name : {userName.Name} <br>")
                .Append($"Language : {userName.Language} <br>")
                .Append($"Telephone : {userName.Telephone} <br>")
                .Append($"Record teacher simple video : {userName.Link} <br>");
            return str.ToString();
        }

        public static UriBuilder MailSenderToppedUpBalanceForStudent(string subject, string host, PaymentTransactions transact)
        {

            return new UriBuilder()
            {
                Host = host,
                Scheme = "http",
                Port = int.Parse(SettingsAppHepler.GetUsingPort()),
                Path = "/Mailer/GetForNewOrder",
                Query = new StringBuilder().
                Append($"&CurrenceAmount={transact.CurrenceAmount}")
                .Append($"&PaymentId={transact.PaymentId}")
                .Append($"&PaymentMethod={transact.PaymentMethod}")
                .Append($"&Type={transact.Credit.Type.ToString()}")
                .Append($"&CountLessons={transact.Credit.CountLessons}")
                .Append($"&TypeOfPrice={transact.Credit.TypeOfPrice}")
                .Append($"&EurPrice={transact.EurPrice}")
                .Append($"&Bonus={transact.Bonus}")
                .Append($"&CreateDate={transact.CreateDate.ToString("dd/MM/yyyy")}")
                .ToString()

            };
        }

        public static UriBuilder MailSenderToppedUpBalanceForAdmin(string host, CustomUser student, PaymentTransactions transact)
        {
            return new UriBuilder()
            {
                Host = host,
                Scheme = "http",
                Port = int.Parse(SettingsAppHepler.GetUsingPort()),
                Path = "/Mailer/GetOrderForAdmin",
                Query = new StringBuilder().
                Append($"&CurrenceAmount={transact.CurrenceAmount}")
                .Append($"&PaymentId={transact.PaymentId}")
                .Append($"&PaymentMethod={transact.PaymentMethod}")
                .Append($"&PaymentMethod={transact.PaymentMethod}")
                .Append($"&EurPrice={transact.EurPrice}")
                .Append($"&Bonus={transact.Bonus}")
                .Append($"&StudentName={student.Name}")
                .Append($"&StudentEmail={student.Email}")
                .Append($"&CreateDate={transact.CreateDate.ToString("dd/MM/yyyy")}").ToString()

            };
        }

        public static UriBuilder BookedLessonForteacher(string host, Guid teacherId, Guid studentId, DateTime date, string lang)
        {
            return new UriBuilder()
            {
                Host = host,
                Scheme = "http",
                Port = int.Parse(SettingsAppHepler.GetUsingPort()),
                Path = "/Mailer/GetBookingLessonForTeacherTemplate",
                Query = new StringBuilder().
                Append($"&teacherId={teacherId}")
                .Append($"&studentId={studentId}")
                .Append($"&date={date}")
                .Append($"&language={lang}").ToString()
            };
        }

        public static UriBuilder BuildUniversalTemplate(string host, string text)
        {
            return new UriBuilder()
            {
                Host = host,
                Scheme = "http",
                Port = int.Parse(SettingsAppHepler.GetUsingPort()),
                Path = "/Mailer/GetUniversalTemplate",
                Query = new StringBuilder().
                Append($"&text={text}").ToString()
            };
        }
        public static UriBuilder BuildAnswerForCorrespond(string host, string userName, string text)
        {
            return new UriBuilder()
            {
                Host = host,
                Scheme = "http",
                Port = int.Parse(SettingsAppHepler.GetUsingPort()),
                Path = "/Mailer/AnswerForUserFromTeacher",
                Query = new StringBuilder().
                Append($"&text={text}")
                .Append($"&teacherName={userName}").ToString()
            };
        }

    }
}
