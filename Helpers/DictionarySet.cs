﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using Domain.Entities;
using MvcApplicationPostFinanceLatest.Models;

namespace MvcApplicationPostFinanceLatest.Helpers
{
    public static class DictionarySet
    {
        public static void CreateDictionaryFromModel<T>(ref Dictionary<string, string> dictionary, T model)
        {
            foreach (var prop in model.GetType().GetProperties())
            {
                if (prop.PropertyType.Name == "Presenter")
                {
                    var inner = prop.GetValue(model, null) as Presenter;
                    if (inner != null)
                    {
                        var prModel = new Presenter();
                        foreach (var innerProp in inner.GetType().GetProperties())
                        {
                            if (innerProp.Name.Contains("ID")||innerProp.Name.Contains("PresenterUrl")||innerProp.Name.Contains("CoPresenterUrl"))
                                continue;
                            dictionary[SplitProperty(innerProp.Name)] = Convert.ToString(innerProp.GetValue(prModel, null));
                        }
                    }
                }
                else
                {
                    if ((prop.Name.Contains("ID")) || (prop.Name == "SecretAccessKey") || (prop.Name == "ServiceRootUrl"))
                        continue;
                    dictionary[SplitProperty(prop.Name)] = Convert.ToString(prop.GetValue(model, null));
                }
            } 
        }

        private static string SplitProperty(string text)
        {
            if (text=="TimeStamp")
            {
                return text.ToLower();
            }
            if (string.IsNullOrWhiteSpace(text))
                return "";
            StringBuilder newText = new StringBuilder(text.Length * 2);
            newText.Append(text[0]);
            for (int i = 1; i < text.Length; i++)
            {
                if (char.IsUpper(text[i]) && text[i - 1] != ' ')
                    newText.Append('_');
                newText.Append(text[i]);
            }
            return newText.ToString().ToLower();
        }
    }
}