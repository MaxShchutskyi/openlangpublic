﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;
using Xencore.Entities.IdentityEntities;

namespace Xencore.Helpers
{
    public class IncludeScheduleFilter : AbstractFilter
    {
        public IncludeScheduleFilter(IQueryable<CustomUser> users, FilterSearchHelper filters) : base(users, filters)
        {
        }

        public override IQueryable<CustomUser> Filter()
        {
            if ((Filters.Daysoftheweek != null && Filters.Daysoftheweek.Any()) ||
                (Filters.TimeRanges != null && Filters.TimeRanges.Any()) || Filters.TutoringAvailableNow)
                return Users = Users.Include(x => x.Schedules);
            return Users;
        }
    }
}