﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;
namespace Xencore.Helpers
{
    public class SerachFilter:AbstractFilter
    {
        public SerachFilter(IQueryable<CustomUser> users, FilterSearchHelper filters) : base(users, filters)
        {
        }

        public override IQueryable<CustomUser> Filter()
        {
            if (!string.IsNullOrEmpty(Filters.Search))
                return Users =
                    Users.Where(
                        x => (x.Name != null && x.Name.Contains(Filters.Search)) || x.Email.Contains(Filters.Search));
            return Users;
        }
    }
}