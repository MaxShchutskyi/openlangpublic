﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;

namespace Xencore.Helpers
{
    public class TutoringAvalableFilter:AbstractFilter
    {
        public TutoringAvalableFilter(IQueryable<CustomUser> users, FilterSearchHelper filters) : base(users, filters)
        {
        }

        public override IQueryable<CustomUser> Filter()
        {
            var dt = DateTime.UtcNow;
            var dtf = dt.AddDays(1).AddMinutes(-dt.Minute).AddHours(-dt.Hour);
            return Users =
                Users.Where(x => x.Schedules.Any(f => f.StartDateTimeDateTime > dt && f.StartDateTimeDateTime < dtf));
        }
    }
}