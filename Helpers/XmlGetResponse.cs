﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;

namespace MvcApplicationPostFinanceLatest.Helpers
{
    public class XmlGetResponse
    {
        public List<Data> Parse(string str)
        {
            StringBuilder output = new StringBuilder();
            string tmp = "";
            List<Data> dictionary = new List<Data>();
            using (XmlReader reader = XmlReader.Create(new StringReader(str)))
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (reader.HasAttributes)
                            {
                                while (reader.MoveToNextAttribute())
                                {
                                    dictionary.Add(new Data { Key = reader.Name, Value = reader.Value });
                                }
                            }
                            else
                            {
                                dictionary.Add(new Data { Key = reader.Name, Value = reader.Value });
                                tmp = reader.Name;
                            }
                            break;
                        case XmlNodeType.Text:
                            dictionary.Add(new Data { Key = tmp, Value = reader.Value });
                            break;
                        default:
                            if (reader.NodeType != XmlNodeType.Whitespace)
                                dictionary.Add(new Data { Key = tmp, Value = reader.Value });
                            break;
                    }
                }
            }
            dictionary.RemoveAll(x => x.Value == "");// || x.Key == ""
            
            return dictionary;
        }
    }
}