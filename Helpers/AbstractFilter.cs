﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;

namespace Xencore.Helpers
{
    public abstract class AbstractFilter
    {
        public IQueryable<CustomUser> Users { get; set; }
        public FilterSearchHelper Filters { get; set; }

        protected AbstractFilter(IQueryable<CustomUser> users, FilterSearchHelper filters)
        {
            Filters = filters;
            Users = users;
        }
        public abstract IQueryable<CustomUser> Filter();
    }
}