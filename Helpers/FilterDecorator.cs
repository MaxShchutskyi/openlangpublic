﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;
using Xencore.Entities.IdentityEntities;

namespace Xencore.Helpers
{
    public class SimpleFilterDecorator:AbstractFilter
    {
        public AbstractFilter Filterr { get; set; }


        public override IQueryable<CustomUser> Filter()
        {
            return Filterr != null ? Filterr.Filter() : Users;
        }

        public SimpleFilterDecorator(AbstractFilter filter):base(filter.Users,filter.Filters)
        {
            Filterr = filter;
        }

        public SimpleFilterDecorator(IQueryable<CustomUser> users, FilterSearchHelper filters) : base(users, filters)
        {
        }
    }
}