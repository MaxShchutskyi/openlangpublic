﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;

namespace Xencore.Helpers
{
    public class SpeakFilter:AbstractFilter
    {
        public SpeakFilter(IQueryable<CustomUser> users, FilterSearchHelper filters) : base(users, filters)
        {
        }

        public override IQueryable<CustomUser> Filter()
        {
            if (!string.IsNullOrEmpty(Filters.Speak))
                return Users = Users.Where(x => x.Ispeak != null && x.Ispeak.Contains(Filters.Speak));
            return Users;
        }
    }
}