﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xencore.Helpers
{
    public class Lang
    {
        public string Name { get; set; }
        public string Ilearn { get; set; }
    }

    public static class LanguagesHepler
    {
        public static Dictionary<string, Lang> Languages=>new Dictionary<string, Lang>()
        {
            {"us", new Lang() {Name = "English", Ilearn = "English_en"} },
            {"en", new Lang() {Name = "English", Ilearn = "English_en"} },
            {"fr", new Lang() {Name = "French", Ilearn = "French_fr"} },
            {"es", new Lang() {Name = "Spanish", Ilearn = "Spanish_es"} },
            {"se", new Lang() {Name = "Swedish", Ilearn = "Swedish_se"} },
            {"br", new Lang() {Name = "Portuguese", Ilearn = "Portuguese_br"} },
            {"gr", new Lang() {Name = "Greek", Ilearn = "Greek_gr"} },
            {"de", new Lang() {Name = "German", Ilearn = "German_de"} },
            {"it", new Lang() {Name = "Italian", Ilearn = "Italian_it"} },
            {"cn", new Lang() {Name = "Chinese", Ilearn = "Chinese_cn"} },
            {"ru", new Lang() {Name = "Russian", Ilearn = "Russian_ru"} },
            {"ae", new Lang() {Name = "Arabic", Ilearn = "Arabic_ae"} }
        };
    }
}
