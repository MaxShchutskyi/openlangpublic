﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;

namespace Xencore.Helpers
{
    public class SearchFilter:AbstractFilter
    {

        public SearchFilter(IQueryable<CustomUser> users, FilterSearchHelper filters) : base(users, filters)
        {
        }
        public override IQueryable<CustomUser> Filter()
        {
            if (!string.IsNullOrEmpty(Filters.Teach))
                return Users = Users.Where(x => x.Ilearn != null && x.Ilearn.Contains(Filters.Teach));
            return Users;
        }
    }
}