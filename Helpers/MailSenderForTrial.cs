﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Domain.Entities;
using NLog;
using Xencore.Entities;
using Xencore.Entities.IdentityEntities.Entities;

namespace Xencore.Helpers
{
    public class MailSenderForTrial :AbstractMailSender
    {

        protected override sealed UriBuilder Uri { get; set; }
        protected override sealed string Destination { get; set; }


        public MailSenderForTrial(string subject, string username,string destination, OrderDetails paymenthelper, string host)
        {
            Subject = subject;
            Uri = new UriBuilder()
            {
                Host = host,
                Scheme = "http",
                Port = int.Parse(SettingsAppHepler.GetUsingPort()),
                Path = "/Mailer/GetForTrialLesson",
                Query = new StringBuilder().
                    Append($"username={username}").
                    Append($"&language={paymenthelper.SelectedLanguage}")
                    .Append($"&level={paymenthelper.SelectedLevel}")
                    .Append($"&startDate='{paymenthelper.StartDate.ToString("dd/MM/yyyy")}'")
                    .Append($"&time={paymenthelper.StartTime}")
                    .Append($"&timezone={paymenthelper.Timezone}")
                    .Append($"&utcDateTime={paymenthelper.UtcDateTime}")
                    .Append($"&teacherId={paymenthelper.CourseBoughtFromId}").ToString()

            };
            Destination = destination;
        }
    }
}
