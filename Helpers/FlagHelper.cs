﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Context;
using Domain.Entities;
using Owin.Security.Providers.ArcGISOnline.Provider;
using Resources;
using Xencore.Entities;

namespace Xencore.Helpers
{
    public static class FlagHelper
    {
        public static MvcHtmlString GetTeachingLanguages(this HtmlHelper helper)
        {
            using (var context = new MyDbContext())
            {
                var res = context.Users.Where(x => x.Active)
                    .Include(x => x.TeacherInfo)
                    .Where(x => x.TeacherInfo != null && x.Ilearn != null)
                    .Select(x => x.Ilearn).ToList();
                var res2 =
                    res.SelectMany(x => x.Split('|').Reverse().Skip(1).Select(f => f.Split('_')[1]))
                        .Distinct()
                        .ToList();
                var dictionary = res2.ToDictionary(item => item, item => Resource.ResourceManager.GetString(item)).OrderBy(x=>x.Value);
                var options = "";
                foreach (var re in dictionary)
                {
                    var tagBuilder = new TagBuilder("option");
                    tagBuilder.Attributes.Add("value", re.Key);
                    tagBuilder.SetInnerText(re.Value);
                    options += tagBuilder.ToString();
                }
                return new MvcHtmlString(options);
            }
        }
        public static string DrawLinkForFlag(this HtmlHelper helper, string translate, string flag,string dataLang, bool autorize)
        {
            if (autorize)
                return $"<a class='for-test' href='#section2' data-lang='{dataLang}'>" +
                       $"<img src = '/images/flags/{flag}' alt = 'learn {LanguagesHepler.Languages[dataLang].Name}' />" +
                       $"<p> {translate} </p ></a>";
                return $"<a class='for-test' data-toggle = 'modal' data-lang = '{dataLang}' data-target='#signup'>" +
                       $"<img src = '/images/flags/{flag}' alt = 'learn {LanguagesHepler.Languages[dataLang].Name}' />" +
                       $"<p> {translate} </p ></a>";
        }

        public static string DrawILearnAndISpeak(this HtmlHelper th, string item)
        {
            if (item == null) return "";
            var strapp = new StringBuilder("");
            var strings = item.Split('|');
            foreach (var lng in strings)
            {
                var code = lng.Split('_').Skip(1).FirstOrDefault();
                if (code == null) continue;
                var pr = LanguagesHepler.Languages.FirstOrDefault(x => x.Key == code).Value;
                strapp.Append(pr != null
                    ? $"<span> {pr.Name} ({code}) </span ><br>"
                    : $"<span> {lng.Split('_').Skip(0).FirstOrDefault()} ({code}) </span ><br>");
            }
            return strapp.ToString();
        }

        public static MvcHtmlString GetCountries(this HtmlHelper hepler)
        {
            IEnumerable<Countries> countries;
            using (var context = new MyDbContext())
            {
                countries = context.Countries.ToList();
            }
            var str = "<option></option>";
            foreach (var country in countries)
            {
                var tag = new TagBuilder("option");
                tag.Attributes.Add("value", country.Alpha2_code);
                tag.Attributes.Add("data-2code", country.Alpha2_code);
                tag.Attributes.Add("data-3code", country.Alpha3_code);
                tag.SetInnerText(country.Name);
                str += tag.ToString();
            }
            return MvcHtmlString.Create(str);
        }

        public static IEnumerable<TK> ConvertTo<T,TK>(this IEnumerable<T> data) where TK:class
        {
            return data.OfType<TK>().Select(d => (d as TK));
        }
        public static MvcHtmlString GetAllLanguagesOfTeacher(this HtmlHelper helper, Guid id)
        {
            using (var context = new MyDbContext())
            {
                var res = context.Users.Where(x => x.Id == id).Select(x => x.Ilearn).FirstOrDefault();
                if (string.IsNullOrEmpty(res)) return new MvcHtmlString("");
                var options = "";
                foreach (var re in res.Split('|'))
                {
                    if(string.IsNullOrEmpty(re)) continue;
                    var option = new TagBuilder("option");
                    option.Attributes.Add("value", re);
                    option.SetInnerText(Resources.Resource.ResourceManager.GetString(re.Split('_')[1]));
                    options += option.ToString();
                }
                return new MvcHtmlString(options);
            }
        }
    }
}