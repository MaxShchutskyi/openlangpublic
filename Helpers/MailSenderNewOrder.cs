﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using Xencore.Entities;
using Xencore.Entities.IdentityEntities.Entities;

namespace Xencore.Helpers
{
    public class MailSenderNewOrder : AbstractMailSender
    {
        protected override sealed UriBuilder Uri { get; set; }
        protected override sealed string Destination { get; set; }

        public MailSenderNewOrder(string subject,string host, string destination, PaymentTransactions transact)
        {
            Subject = subject;
            Uri = new UriBuilder()
            {
                Host = host,
                Scheme = "http",
                Port = int.Parse(SettingsAppHepler.GetUsingPort()),
                Path = "/Mailer/GetForNewOrder",
                Query = new StringBuilder().
                    Append($"CountLessons={transact.OrderDetailsId.CountLessons}").
                    Append($"&Amount={transact.Amount}")
                    .Append($"&PaymentId={transact.PaymentId}")
                    .Append($"&CreateDate={transact.CreateDate.ToString("dd/MM/yyyy")}")
                    .Append($"&CourseType={transact.OrderDetailsId.CourseType}")
                    .Append($"&SelectedLanguage={transact.OrderDetailsId.SelectedLanguage}")
                    .Append($"&SelectedLevel={transact.OrderDetailsId.SelectedLevel}")
                    .Append($"&StartDate={transact.OrderDetailsId.StartDate.ToString("dd/MM/yyyy")}")
                    .Append($"&utcDateTime={transact.OrderDetailsId.UtcDateTime}")
                    .Append($"&CurrenceAmount={transact.CurrenceAmount}")
                    .Append($"&teacherId={transact.OrderDetailsId.CourseBoughtFromId}").ToString()

            };
            Destination = destination;
        }
    }
}
