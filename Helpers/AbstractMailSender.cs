﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using NLog;
using Xencore.Entities.IdentityEntities.Entities;
using Xencore.Services;

namespace Xencore.Helpers
{
    public abstract class AbstractMailSender:IMailSender
    {
        protected abstract UriBuilder Uri { get; set; }
        protected string Subject { get; set; }
        protected abstract string Destination { get; set; }

        public async Task<bool> Send()
        {
            var result = await GetMessage();
            if (result == null)
                return false;
            var emailService = new NativeEmailService.NativeEmailService();
            await emailService.SendAsync(new IdentityMessage()
            {
                Subject = Subject,
                Destination = Destination,
                Body = result
            });
            return true;

        }
        private async Task<string> GetMessage()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var result = await client.GetStringAsync(Uri.ToString());
                    return result;
                }
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error(e, "Error in getting httpresult");
                return null;
            }
        }
    }
}
