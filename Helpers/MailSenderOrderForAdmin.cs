﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using Xencore.Entities;
using Xencore.Entities.IdentityEntities;
using Xencore.Entities.IdentityEntities.Entities;

namespace Xencore.Helpers
{
    class MailSenderOrderForAdmin:AbstractMailSender
    {
        protected override sealed UriBuilder Uri { get; set; }
        protected override sealed string Destination { get; set; }

        public MailSenderOrderForAdmin(string host, string destination, PaymentTransactions transact)
        {
            Uri = new UriBuilder()
            {
                Host = host,
                Scheme = "http",
                Port = int.Parse(SettingsAppHepler.GetUsingPort()),
                Path = "/Mailer/GetOrderForAdmin",
                Query = new StringBuilder().
                    Append($"CountLessons={transact.OrderDetailsId.CountLessons}").
                    Append($"&Amount={transact.Amount}")
                    .Append($"&PaymentId={transact.PaymentId}")
                    .Append($"&CreateDate={transact.CreateDate}")
                    .Append($"&CourseType={transact.OrderDetailsId.CourseType}")
                    .Append($"&SelectedLanguage={transact.OrderDetailsId.SelectedLanguage}")
                    .Append($"&SelectedLevel={transact.OrderDetailsId.SelectedLevel}")
                    .Append($"&userName={transact.CustomUserId.Name}")
                    .Append($"&userEmail={transact.CustomUserId.Email}")
                    .Append($"&utcDateTime={transact.OrderDetailsId.UtcDateTime}")
                    .Append($"&CurrenceAmount={transact.CurrenceAmount}")
                    .Append($"&TeacherId={transact.OrderDetailsId?.CourseBoughtFromId}").ToString()

            };
            Destination = destination;
        }
    }
}
