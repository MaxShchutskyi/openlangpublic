﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Xencore.Services;
using Xencore.ViewModel;

namespace Xencore.Helpers
{
    public class BussinessSendMail
    {
        public async Task<bool> SendMessage(BussinessRequestViewModel model)
        {
            var subject = "Business enquiry";
            var emailService = new NativeEmailService.NativeEmailService();
            var body = new StringBuilder();
            body.Append($"<div><h3>Name:</h3><span>{model.Name}</span></div>");
            body.Append($"<div><h3>Company:</h3><span>{model.Company}</span></div>");
            body.Append($"<div><h3>Email:</h3><span>{model.Email}</span></div>");
            body.Append($"<div><h3>Phone:</h3><span>{model.Phone}</span></div>");
            body.Append($"<div><h3>Country:</h3><span>{model.Country}</span></div>");
            body.Append($"<div><h3>Timezone:</h3><span>{model.TimeZone}</span></div>");
            body.Append($"<div><h3>Target Language:</h3><span>{model.TargetLanguage}</span></div>");
            body.Append($"<div><h3>Level:</h3><span>{model.Level}</span></div>");
            body.Append($"<div><h3>EMPLOYEES WITH TRAINING REQUIREMENTS:</h3><span>{model.EmploeeWithTranungs}</span></div>");
            //body.Append("<div><h3>Interested:</h3><ul>");
            //foreach (var lang in model.Languages)
            //{
            //    body.Append($"<li>{lang}</li>");
            //}
            //body.Append("</ul></div>");
            body.Append($"<div><h3>Text:</h3><span>{model.Text}</span></div>");
            await emailService.SendAsync(new IdentityMessage()
            {
                Body = body.ToString(),
                Subject = subject,
                Destination = SettingsAppHepler.GetAdminEmail(),
            });
            return true;
        }
    }
}