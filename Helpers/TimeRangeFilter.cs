﻿using System;
using System.Linq;
using Domain.Entities.HelpersEntity;
using Domain.Entities.IdentityEntities;

namespace Xencore.Helpers
{
    public class TimeRangeFilter:AbstractFilter
    {
        public TimeRangeFilter(IQueryable<CustomUser> users, FilterSearchHelper filters) : base(users, filters)
        {
        }

        public override IQueryable<CustomUser> Filter()
        {
            var dt = DateTime.UtcNow;
            if (Filters.TimeRanges != null && Filters.TimeRanges.Any())
               return Users = Users.AsEnumerable().Where(
                    x => x.Schedules.Any(f => Filters.TimeRanges.Any(s =>
                    {
                        if (s.From > s.To)
                            return s.From <= f.TimeStart + 24 && s.To + 24 > f.TimeStart + 24;
                        return s.From <= f.TimeStart && s.To > f.TimeStart;
                    }) && f.StartDateTimeDateTime > dt)).AsQueryable();
            return Users;
        }
    }
}