﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using Xencore.Entities;
using Xencore.Entities.IdentityEntities.Entities;

namespace Xencore.Helpers
{
    public class MailSenderForTeachApplay:AbstractMailSender
    {
        protected override sealed UriBuilder Uri { get; set; }
        protected override sealed string Destination { get; set; }

        public MailSenderForTeachApplay(string subject, string host, string destination, TeacherApplication application )
        {
            Subject = subject;
            Uri = new UriBuilder()
            {
                Host = host,
                Scheme = "http",
                Port = int.Parse(SettingsAppHepler.GetUsingPort()),
                Path = "/Mailer/GetMailForTeacher",
                Query = new StringBuilder($"&Name={application.Name}")
                .Append($"&Telephone={application.Telephone}")
                .Append($"&Link={application.Link}")
                .Append($"&Language={application.Language}")
                .Append($"&Relevant={application.Relevant}")
                .Append($"&Additional={application.Additional}").ToString()
            };
            Destination = destination;
        }
    }
}
