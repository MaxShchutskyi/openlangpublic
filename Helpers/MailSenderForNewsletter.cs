﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xencore.Helpers
{
    public class MailSenderForNewsletter:AbstractMailSender
    {
        protected override sealed UriBuilder Uri { get; set; }
        protected override sealed string Destination { get; set; }

        public MailSenderForNewsletter(string subject, string host, string destination)
        {
            Subject = subject;
            Uri = new UriBuilder()
            {
                Host = host,
                Scheme = "http",
                Port = int.Parse(SettingsAppHepler.GetUsingPort()),
                Path = "/Mailer/GetForNewsLetter",
            };
            Destination = destination;
        }
    }
}
