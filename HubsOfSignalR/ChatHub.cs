﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Domain.Context;
using Domain.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Owin.Security.Providers.ArcGISOnline.Provider;
using PushNotificationService;
using Xencore.Entities;
using Xencore.Services;

namespace Xencore.HubsOfSignalR
{
    public class ChatHub : Hub
    {
        public static List<string> ConnectedIds { get; set; } = new List<string>();
        private MyDbContext MyContext { get; } = new MyDbContext();
        public override Task OnConnected()
        {
            var partners = Context.QueryString["partners"];
            if (partners == null || !partners.Any()) return base.OnConnected();
            var spl = partners.Split(',');
            var users = Clients.Users(spl);
            var id = Guid.Parse(Context.User.Identity.GetUserId());
            users.SombodyConnected(id);
            Clients.Caller.GetAllUsersInChat(
                JsonConvert.SerializeObject(ConnectedIds.Where(x => spl.Any(f => f.Contains(x)))));
            //var id = Guid.Parse(Context.User.Identity.GetUserId());
            ConnectedIds.Add(id.ToString());
            return base.OnConnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {
            var partners = Context.QueryString["partners"];
            var id = Guid.Parse(Context.User.Identity.GetUserId());
            var user = MyContext.Users.First(x => x.Id == id);
            user.DateOffline = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss+00:00");
            MyContext.SaveChanges();
            if (partners != null && partners.Any())
            {
                Clients.Users(partners.Split(',')).SombodyDisconnected(id);
            }
            ConnectedIds.Remove(Context.User.Identity.GetUserId());
            return base.OnDisconnected(stopCalled);
        }

        public void MessageRead(string messId)
        {
            var messages = messId.Split(',');
            var usr = MyContext.DialogReplies.Where(x => messages.Any(f => f == x.Id.ToString())).ToList();
            foreach (var u in usr)
            {
                u.Unread = false;
            }
            MyContext.SaveChanges();
        }

        public void SendTo(string dialogId, string from, string message, string to)
        {
            var userId = Guid.Parse(Context.User.Identity.GetUserId());
            var dialogreply = new DialogReply()
            {
                Message = message,
                UserId = userId,
                DialogId = int.Parse(dialogId)
            };
            MyContext.DialogReplies.Add(dialogreply);
            var frId = Guid.Parse(to);
            var usr = MyContext.Users.FirstOrDefaultAsync(x => x.Id == userId);
            Task.WaitAll(usr);
            MyContext.SaveChanges();
            Clients.User(to).receiveMessage(message, from, dialogreply.Id, dialogId);
            Clients.Caller.MessageWasSent(dialogreply.Id);

            PushNotificationService.PushNotificationService.SendNotificationNoAsync(
                new NotificationModel()
                {
                    Title = $"Message from {usr.Result.Name}",
                    Uri =
                        HttpContext.Current.Request.Url.Scheme + "://" +
                        HttpContext.Current.Request.Url.Authority + "/my-account#/chat",
                    Message = message,
                    Icon =
                        HttpContext.Current.Request.Url.Scheme + "://" +
                        HttpContext.Current.Request.Url.Authority + "/img/icon.png"
                }, frId);
        }

        public void GetMoreMessages(int dialogId, int page)
        {
            var mess = MyContext.DialogReplies.Where(x=>x.DialogId == dialogId).OrderByDescending(x=>x.DateTime).Include(x=>x.CustomUser).Select(x=>new
            {
                x.DateTime,
                x.UtcDateTime,
                x.Id,
                x.Message,
                UserId = x.CustomUser.Id,
                x.CustomUser.ProfilePicture
            }).OrderByDescending(x=>x.DateTime).AsEnumerable().Skip(10*page).Take(10).ToList();
            Clients.Caller.replyMoreMessages(mess);
        }

        public void InviteToVideoChat(string to)
        {
            Clients.User(to).InviteToVideoChatToVideo(Context.User.Identity.GetUserId());
        }
        public void AnswerForCall(string to, bool success)
        {
            Clients.User(to).confirmCall(Context.User.Identity.GetUserId(),success);
        }
    }
}