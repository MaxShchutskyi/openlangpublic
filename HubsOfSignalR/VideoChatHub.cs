﻿using System;
using System.Collections.Generic;

using Microsoft.AspNet.SignalR;

namespace Xencore.HubsOfSignalR
{
    [Authorize]
    public class VideoChatHub:Hub
    {
        public static List<string> ConnectedIds { get; set; } = new List<string>();

        public void CreateOffer(string id, string to, string offer)
        {
            Clients.User(to).reciveOffer(id, offer);
        }
        public void Answer(string id,string answer)
        {
            Clients.User(id).reciveAnswer(answer);
        }

        public void SendIceCandidate(string id, string candidate)
        {
            Clients.User(id).newCandidate(candidate);
        }


        public void SendMessage(string message, string from, string to)
        {
            if(to == null) return;
            Clients.User(to).messages(from,message);
        }

        public void EndVideoChat(string clientId)
        {
            Clients.User(clientId).endCall();
        }
    }
}