﻿function shared(logout, ismainPage) {
  function updateQueryString(key, value, url) {
    if (!url) url = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
      hash;

    if (re.test(url)) {
      if (typeof value !== 'undefined' && value !== null)
        return url.replace(re, '$1' + key + "=" + value + '$2$3');
      else {
        hash = url.split('#');
        url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
        if (typeof hash[1] !== 'undefined' && hash[1] !== null)
          url += '#' + hash[1];
        return url;
      }
    }
    else {
      if (typeof value !== 'undefined' && value !== null) {
        var separator = url.indexOf('?') !== -1 ? '&' : '?';
        hash = url.split('#');
        url = hash[0] + separator + key + '=' + value;
        if (typeof hash[1] !== 'undefined' && hash[1] !== null)
          url += '#' + hash[1];
        return url;
      }
      else
        return url;
    }
  }
    logouts = logout;
    if (!$.cookie("timezone")) {
        $.cookie("timezone", new Date().getTimezoneOffset() * -1, { expires: 10, path: '/' });
    }
    $(document).ready(function () {
      //$('#currency2').select2();
      $('#currency2').change(function () {
        if (getParameterByName("offset")) {
          var updated = updateQueryString("currency", $('#currency2').val(), location.href);
          var ht = updateQueryString("offset", window.pageYOffset, updated);
            location.href = updated;
          return;
        }
        location.href = location.href + "?currency=" + $('#currency2').val() + "&offset=" + window.pageYOffset;
      });
      if (getParameterByName("offset")) {
        window.scrollTo(0, parseFloat(getParameterByName("offset")));
      }
        function GetCountUnreadMessages() {
            if (!sessionStorage.getItem('token')) return;
            $.ajax({
                url: "/api/ChatApi/GetCountUnreadMessages",
                beforeSend: function (xhr) {
                    var token = sessionStorage.getItem('token');
                    xhr.setRequestHeader("Authorization", "Bearer " + token);
                },
                async: true,
                headers: { "cache-control": "no-cache", "X-MicrosoftAjax": "Delta=true" },
                dataType: "json",
                contentType: "application/json",
                method: "GET",
            }).then(function (res) {
                $(".mess-count").css("display", "flex").text(res.count).addClass('messages-' + res.count)
                console.log(res);
            }, function (err) {
                console.log(err);
            })
        }
        $(".messages").click(function () {
            var href = $(this).data('href');
            location.href = href;
        })
        setInterval(GetCountUnreadMessages,5000);
        GetCountUnreadMessages();
        $('.js-togglePopups').click(function () {
            //закрыть текущий попап
            var close = '#' + $(this).parents('.modal').attr('id')
            jQuery("body").addClass("modal-open");
            var open = $(this).data('target')
            changePopups(close, open);
        });
        $('#toggle').click(function () {
            $(this).toggleClass('active');
            if ($(this).hasClass('active'))
                $('#overlay').addClass('open').siblings().removeClass("open");
            else
                $('#overlay').removeClass('open').siblings().removeClass("open");
        });
        $("#loginbtn, #applytoteach1").click(function () {
            $('#overlay').toggleClass('open');
            $('#toggle').toggleClass('active')
        });
        $(".redirecttobuy").click(function () {
            var redirect = $(this).data("href");
            var params = { type: $(this).data('type'), member: $(this).data('member'), key:$(this).data('key') };
            $.cookie("paymentparams", JSON.stringify(params), { path: '/' });
            location.href = redirect;
        });
        $(".js-lang").click(function () {
            var selLang = $(this).data("lang");
            var query = location.pathname.split('/');
            query[1] = selLang;
            location.href = query.join("/");
            //$.cookie("lang", selLang, { path: "/" });
            //location.reload();

        });
        if ($.cookie("lang") != null) {
            var cook = $.cookie("lang");
            console.log(cook);
            var chech = $(".js-lang").filter("[data-lang = '" + cook + "']");
            console.log(chech);
            $(".dropdown-languages .ccc").text(chech.text());
        }
    });
    $("#google_login,#google_signup").click(function () {
        //location.href = '/Account/GoogleLogin' + "?selLang=" + (selLangg ? selLang : "");
        //handleAuthClick();
        new SocialResponseHundler().buttonClickEvent(new GoogleAuthourizeTs());
        $(this).parents(".modal-content").find("button.close").click();
        //location.href = '/Account/GoogleLogin';
    });
    $("#facebook_login,#facebook_signup").click(function () {
        //location.href = '/Account/FacebookLogin' + "?selLang=" + (selLangg ? selLang : "");
        //location.href = '/Account/FacebookLogin';
        //facebookLogin();
        new SocialResponseHundler().buttonClickEvent(new FacebookAuthorizeTs());
        $(this).parents(".modal-content").find("button.close").click();
    });
}
function logOut() {
    sessionStorage.removeItem('token');
    if ($.connection)
        $.connection.hub.stop();
    location.href = logouts;
    //location.href = "http://localhost:21514/MyAccount/Index";
};
function changePopups(close, open) {
    $(close).modal('hide');
    setTimeout(function () { 
        $(open).modal('show');
    },500);
}
function goToByScroll(id) {
    // Remove "link" from the ID
    id = id.replace("link", "");
    // Scroll
    $('html,body').animate({
        scrollTop: $("#" + id).offset().top
    },
        'slow');
}
$(".add-to-cookies").click(function () {
    var params = { type: $(this).data('type'), member: $(this).data('member'), key: $(this).data('key') };
    $.cookie("paymentparams", JSON.stringify(params), { path: '/' });
});
$("button.close").click(function () {
    clearCookie('paymentparams');
});
function clearCookie(cookie) {
    $.cookie(cookie, null, { path: '/' });
    $.removeCookie(cookie, { path: '/' });
    document.cookie = cookie + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
$(".info-tooltip").tooltip();

function func() {
    $(".page-preloader-wrap").hide();
}
setTimeout(func, 1500);
function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function checkExistReferralCode(){
  var code = getParameterByName("refcode");
  var expDate = new Date();
  expDate.setTime(expDate.getTime() + (43200 * 60 * 1000));
  if (!code) return;
  $.cookie('refcode', code, { path: '/', expires: expDate });
}
function checkExistCompanyId() {
  var code = getParameterByName("compy");
  var expDate = new Date();
  expDate.setTime(expDate.getTime() + (43200 * 60 * 1000));
  if (!code) return;
  $.cookie('compy', code, { path: '/', expires: expDate });
}
function checkExistShowRegister() {
  var param = getParameterByName("showregister");
  if (!param) return;
  $("#SignIn").modal("show");
}
function checkShowLogin() {
  if ($.cookie("showlogin")) {
    $("#Login").modal('show');
    clearCookie("showlogin");
  }
  
}
function checkShowAlertRegister() {
  if ($.cookie("showsuccessregister")) {
    alert("Please confirm your email!");
    clearCookie("showsuccessregister");
  }
}
function changeLang(lang) {
    var query = location.pathname.split('/');
    query[1] = lang;
    location.href = query.join("/");
}
checkShowAlertRegister();
checkExistReferralCode();
checkExistCompanyId();
checkExistShowRegister();
checkShowLogin();
