﻿'use strict';
var appServerKey = "BD0zXgf46NcgieZufIS5YyD2Si8fVSOoUbeWAF2LAx6qxFQ1iCqPQHuPruykaxUkPI1UJ0X5zbBY4B1dPptLFqw";
function subscribe() {
    if ('serviceWorker' in navigator) {
        console.log('Service Worker is supported');
        navigator.serviceWorker.register('/sw.js').then(function (reg) {
            reg.update();
            return navigator.serviceWorker.ready;
        }).then(function (reg) {
            var options = { userVisibleOnly: true };
            console.log(navigator.appName);
            if (get_browser_info().name.toLowerCase() == 'chrome' || get_browser_info().name.toLowerCase() == 'opera') {
                options['applicationServerKey'] = urlB64ToUint8Array(appServerKey);
            }
            console.log('Service Worker is ready :^)', reg);
            reg.pushManager.subscribe(options).then(function (sub) {
                var ph = Array.from(new Uint8Array(sub.getKey('p256dh')));
                var auth = Array.from(new Uint8Array(sub.getKey('auth')));
                var obj = JSON.stringify({'endpoint': sub.endpoint, 'auth':auth, 'ph':ph })
                console.log(obj);
                $.ajax({
                    type: 'post',
                    beforeSend: function (xhr) {
                        var token = sessionStorage.getItem('token');
                        xhr.setRequestHeader("Authorization", "Bearer " + token);
                    },
                    url:"/api/TestPushNotification/Subscribe",
                    data: obj,
                    dataType: "json",
                    contentType: "application/json"
                }).then(function (res) {

                });
            });
        }).catch(function (error) {
            console.log('Service Worker error :^(', error);
        });
    }
}
function unsubscribe() {
    if ('serviceWorker' in navigator) {
        console.log('Service Worker is supported');
        navigator.serviceWorker.register('/sw.js').then(function (reg) {
            reg.update();
            return navigator.serviceWorker.ready;
        }).then(function (reg) {
            var options = { userVisibleOnly: true };
            console.log(navigator.appName);
            if (get_browser_info().name.toLowerCase() == 'chrome' || get_browser_info().name.toLowerCase() == 'opera') {
                options['applicationServerKey'] = urlB64ToUint8Array(appServerKey);
            }
            console.log('Service Worker is ready :^)', reg);
            reg.pushManager.subscribe(options).then(function (sub) {
                var obj = JSON.stringify({ 'endpoint': sub.endpoint})
                console.log(obj);
                $.ajax({
                    type: 'post',
                        beforeSend: function (xhr) {
                            var token = sessionStorage.getItem('token');
                            xhr.setRequestHeader("Authorization", "Bearer " + token);
                        },
                    url: "/api/TestPushNotification/Unsubscribe",
                    data: JSON.stringify(sub.endpoint),
                    dataType: "json",
                    contentType: "application/json"
                }).then(function (res) {

                });
            });
        }).catch(function (error) {
            console.log('Service Worker error :^(', error);
        });
    }
}
function urlB64ToUint8Array(base64String) {
    var padding = '='.repeat((4 - base64String.length % 4) % 4);
    var base64 = (base64String + padding)
      .replace(/\-/g, '+')
      .replace(/_/g, '/');

    var rawData = window.atob(base64);
    var outputArray = new Uint8Array(rawData.length);

    for (var i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}
function get_browser_info() {
    var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return { name: 'IE ', version: (tem[1] || '') };
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/)
        if (tem != null) { return { name: 'Opera', version: tem[1] }; }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) { M.splice(1, 1, tem[1]); }
    return {
        name: M[0],
        version: M[1]
    };
}