$(function () {
    jQuery(".sect-pricing .info-tooltip").click(function () {
        if (jQuery(this).hasClass("active")) {
            jQuery(this).removeClass("active");
            jQuery(this).parent().children(".tooltip ").hide();
        } else {
            jQuery(this).addClass("active");
            jQuery(this).parent().children(".tooltip ").show();
        }
    })
    $("#loginform .custom-input").keydown(function (e) {
        if (e.keyCode == 13) {
            $('.modal-custom-button.bt-login').click();
        }
    });
    $("#formLogin .custom-input").keydown(function (e) {
        if (e.keyCode == 13) {
            $('#formLogin .modal-custom-button').click();
        }
    });
    $("#resPass .custom-input").keydown(function (e) {
        if (e.keyCode == 13) {
            $('#resPass .modal-custom-button').click();
        }
    });
    $("#applytoteach .custom-input").keydown(function (e) {
        if (e.keyCode == 13) {
            $('#applytoteach .modal-custom-button').click();
        }
    });
    //$(".HowItWorks .step-subtitle").equalHeights();
    //$(".HowItWorks .step").equalHeights();
    $(".buttons-wrap ul li a").click(function () {
        var data = $(this).attr("data-type");
        $(".buttons-wrap ul li a").removeClass('active');
        $(this).addClass("active");
        $("#" + data).removeClass("hidden").siblings("[id]").addClass('hidden');
        return false;
        //if($(this).hasClass('active') !== 'true') {
        //	$(".buttons-wrap ul li a").removeClass('active');
        //	$(this).addClass('active');
        //	if($(this).hasClass('basic')) {
        //		$("#basic").removeClass('hidden');
        //		$("#premium").addClass('hidden')
        //	} else {
        //		$("#premium").removeClass('hidden');
        //		$("#basic").addClass('hidden');
        //	}
        //
        //} 
        //	return false;
    });
    if (!jQuery(".block-wrap").children('.popular-wrap').show()) {
        jQuery(this).css('margin-top', '40px');
    }
    //popup
    //$(".button-send").magnificPopup();

    //all block equal Height
    //$(".popular-wrap").equalHeights();
    //$("#sect-price.sect-pricing .blocks-wrap .container .col-md-3 ul").equalHeights();
    $("#sect-price.sect-pricing .blocks-wrap .container .col-md-3 .per_month").equalHeights();
    //animated css Docs: https://daneden.github.io/animate.css/
    //$(".blocks-wrap .block-wrap:first-of-type , .blocks-wrap .block-wrap:nth-of-type(2)").animated("fadeInLeft");

    //owl-carousel 2  Docs: https://owlcarousel2.github.io/OwlCarousel2/

    /*
    var owl = $('.slider');
    owl.owlCarousel({
        items:1,
        loop:true,
    });
    */

    /* Mobile menu Docs:
     $(document).ready(function() {
          $("#menu").mmenu({
            "extensions": [
                "effect-menu-slide",
                "effect-listitems-drop"
             ]
          });
    });
    */

    //Documentation & Example: https://github.com/agragregra/uniMail
    $("#callback-form").submit(function () { //Change
        var th = $(this);
        $.ajax({
            type: "POST",
            url: "mail.php", //Change
            data: th.serialize()
        }).done(function () {
            alert("Спасибо за заявку!");
            setTimeout(function () {
                // Done Functions
                th.trigger("reset");
            }, 1000);
        });
        return false;
    });

    //Chrome Smooth Scroll
    try {
        $.browserSelector();
        if ($("html").hasClass("chrome")) {
            $.smoothScroll();
        }
    } catch (err) {

    };

    $("img, a").on("dragstart", function (event) { event.preventDefault(); });
    //$(".preloader-container").fadeOut();
});
