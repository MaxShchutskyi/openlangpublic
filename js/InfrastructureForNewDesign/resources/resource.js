﻿var resource = (function () {
    var resourceObject;
    function match(find) {
        return resourceObject[find];
    }
    var lang = $.cookie("lang");
    //console.log(lang)
    $.getJSON("/js/InfrastructureForNewDesign/resources/resource." + lang + ".json").then(function (res) {
        console.log(res);
        resourceObject = res;
    }, function (err) { console.log(err) });
    return {
        match: match
    }
})()