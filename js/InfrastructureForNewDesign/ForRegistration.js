﻿$.validator.setDefaults({
    ignore: []
});
function fn(code, actionUrl) {
  FacebookAuthorizeTs.loadSDK();
  $(".bt-login").click(function () {
    if (!$("#loginform").valid())
      return;
    var loginFom = $("#loginform");
    $.ajax({
      url: "/Account/CheckExistUser",
      type: "post",
      data: loginFom.serialize(),
      async: true,
      success: function (result) {
        debugger;
        if (result === "1") {
          loginFom.submit();
        } else if (result === "-1") {
          alert(resource.match('incorrect_email'));
        }
        else if (result === "0")
          alert("Your email is not activated");

      }
    });
  });
  $("#startlearning").click(function () {
    //if (!$("#tellusmore").valid())
    //    return;
    var signin = $("#formLogin input").clone();
    var elems = $("#tellusmore input").clone();
    var form = $("<form method='post' action = '" + actionUrl + "'></form>");
    var selectors = $("#tellusmore select").map(function (index, elem) {
      var sel = $(elem).find("option:selected").first();
      return $("<input name=" + elem.name + " value = " + sel.val() + "></input>")
    }).toArray();
    console.log(selectors);
    debugger;
    form.append(signin).append(elems).append(selectors);
    form.css("display", 'none');
    form.appendTo('body');
    form[0].PhoneNumber.value = $("#phone").intlTelInput("getNumber");
    console.log(form);
    form.submit();
  });
  $("#loginform,#formLogin,#resPass").each(function (index, elem) {
    $(this).validate({
      errorPlacement: function () { }
    });
  });
  $(".phone").each(function () {
    $(this).intlTelInput({
      utilsScript: "/Scripts/openlang/utils.js",
    });
  });
  $(".phone").each(function () {
    $(this).intlTelInput("selectCountry", code)
  });
  $("#tellusmore,#applytoteach").each(function () {
    $(this).validate({
      errorPlacement: function () { },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass("error").siblings("button").addClass("error");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass("error").siblings("button").removeClass("error");
      }
    });
  });
    $("#goToTeach").click(function () {
        debugger;
    var appteach = $("#applytoteach");
        if (!appteach.valid() || !$("#Language").valid()) return;
    //appteach.find("#phone2").val($("#phone2").intlTelInput("getNumber"))
    appteach.submit();
  });
}
//$("#applytoteach").validate({
//    rules: {
//        Language: {
//            required: true,
//        }
//    }
//});
var checkLoginForm = function () {
  if (!$("#formLogin").valid())
    return;
  if ($.cookie("compy"))
    $("#companyId").val($.cookie("compy"));
  if (!$("#refferCode").val() && $.cookie('refcode')) {
    var code = $.cookie('refcode');
    debugger;
    $.get("/api/RefferalProgramm/CheckReferCode?refferCode=" + code).then(function (res) {
      if (!res) return;
      $("#refferCode").val($.cookie('refcode'));

      clearCookie("refcode");
      console.log("THEN");
    }).always(function () {
      console.log("ALLWAIS")
      $("#startlearning").click();
    });
  } else {
    console.log("NOT WORK");
    $("#startlearning").click();
  }
  //changePopups("#SignIn", "#TellUsMore1")
}
function resetPassword(redirect) {
  console.log("reset");
  if (!$("#resPass").valid())
    return;
  $.ajax({
    url: "/Account/FogotPassword",
    type: "post",
    data: $("#resPass").serialize(),
    success: function (res) {
        if (!res)
            alert("Error Query")
        else
            location.href = redirect;
    },
    error: function (err) {
      console.log(err.responseText);
    }
  });
}