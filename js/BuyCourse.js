﻿function buy_course() {
    var seldate = $("#buy_datetimepicker").val();
    var selLang = $("#buy_select_lang").select2('data')[0];
    var chLang = $(selLang.element).attr("data-translate") + "_"+ selLang.id;
    var level = $("#buy_select_level").val();
    var details = {
        CountLessons: countLessons,
        Amount: parseFloat(selectedprice),
        SelectedLanguage: selLang.text.trim(),
        StartDate: seldate,
        SelectedLevel: level,
        CourseType: selectedType,
        UtcDateTime: new Date().toString(),
        Advance: CurrenciesCountry.selectedCurrency.currency + " " + selectedprice,
        dateId:dateId,
        chLang: chLang,
        CourseBoughtFromId: modelId,
        discountId: discountId,
        eurPrice: selectedpriceeur
    };
    if ($("#buy_paypal").prop("checked")) {
        $.ajax({
            method: "post",
            url: '/PayPalPayment/Get_PayPalLink',
            data: details,
            beforeSend: function () {
                $('#buy-full-step-2 button.pre-btn').attr('disabled', true).html('  <img src="/Images/1.gif" />  ');
                $('body').append('<div class="cover-block"></div>')
            },
            success: function (e) {
                $('#buy-full-step-2 button.pre-btn').html($('.pre-btn').attr('data-text'));
                $('#buy-full-step-2 button').removeAttr('disabled');
                $('div.cover-block').remove();
                location.href = e.success;
            },
            error: function (err) {
                alert("Error");
                var str = "";
                err.responseJSON.error.forEach(function (elem) {
                    str += elem.ErrorMessage + "\n";
                });

            }
        });
    }
    if ($("#buy_card").prop("checked")) {
        $.ajax({
            method: "post",
            url: '/CreditCardPayment/MakePayment',
            data: {
                Name: $("#buy_name-on-card").val(),
                CardNumber: $("#buy_card-number").val(),
                ExpMonth: parseInt($("#buy_card_month").val()),
                ExpYear: parseInt($("#buy_card_year").val()),
                Cvc: $("#buy_cvv-card").val(),
                Advance: CurrenciesCountry.selectedCurrency.currency + " " + selectedprice,
                PaymentDetails: details,
                courseBoughtFromId: modelId,
                dateId: dateId,
                discountId: discountId,
                eurPrice: selectedpriceeur
            },
            beforeSend: function () {
                $('#buy-full-step-2 button.pre-btn').attr('disabled', true).html('  <img src="/Images/1.gif" />  ');
                $('body').append('<div class="cover-block"></div>')
            },
            success: function (e) {
                console.log(e);
                location.href = "/CreditCardPayment/RedirectToThanks?url=" + e.redirectTo;
                return;
                $('#buy-full-step-2 button.pre-btn').html($('.pre-btn').attr('data-text'));
                $('#buy-full-step-2 button').removeAttr('disabled');
                $('div.cover-block').remove();
                $('#buy-full-step-2').modal('hide')
                setTimeout(function () {
                    $('#buy-full-step-3').modal('show')
                }, 500);
                $('#buyFullStep2').trigger('reset');
                $('#buyFullStep1').trigger('reset');
            },
            error: function (err) {
                alert("Error");
                var error = err.responseJSON.error;
                if (Array.isArray(error)) {
                    var str = "";
                    err.responseJSON.error.forEach(function (elem) {
                        str += elem.ErrorMessage + "\n";
                    });
                    alert(str);
                    return;
                }

            }
        });
    }
    if ($("#buy_tenpay").prop("checked")) {
        $.ajax({
            method: "post",
            url: '/TenPayPayment/MakePayment',
            data: details,
            beforeSend: function () {
                $('#buy-full-step-2 button.pre-btn').attr('disabled', true).html('  <img src="/Images/1.gif" />  ');
                $('body').append('<div class="cover-block"></div>')
            },
            success: function (e) {
                $('#buy-full-step-2 button.pre-btn').html($('.pre-btn').attr('data-text'));
                $('#buy-full-step-2 button').removeAttr('disabled');
                $('div.cover-block').remove();
                location.href = e;
            },
            error: function (err) {
                console.log(err);
                console.log(err.responseJSON.error);
            }
        });
    }
};

function trial_pay() {
    var lernData = $("#trial_select_lang").select2('data')[0];
    var learn = lernData.text.trim();
    var level = $("#trial_select_level").select2('data')[0].text.trim();
    var learnLia = $("#trial_select_learn_via").select2('data')[0].text.trim();
    var tel = $("#trial_select_phone").intlTelInput("getNumber");
    var howFind = $("#trial_select_find_us").select2('data')[0].text.trim();
    var timezone = $("#trial_select_timezone").select2('data')[0].text.trim();
    var startDate = $("#trial_datetimepicker").val();
    var startTime = $("#start-hours").val();
    var chLang = lernData.text.trim() + "_" + lernData.id;
    var details = {
        CountLessons: 1,
        Amount: "0.99",
        SelectedLanguage: learn,
        StartDate: startDate,
        SelectedLevel: level,
        StartTime: startTime,
        Timezone: timezone,
        Skype: learnLia,
        HowDidFind: howFind,
        Telephone: tel,
        CourseType: "Trial",
        UtcDateTime: new Date().toString(),
        dateId:dateId,
        Advance: "EUR 0.99",
        ChLang: chLang,
        CourseBoughtFromId: modelId
    };
    if (typeof selectedTeacherIdForTrial !== 'undefined')
        details['teacherId'] = window.location.protocol + "//"+ window.location.host + "/TeacherDetails/" + selectedTeacherIdForTrial;
    if ($("#trial_paypal").prop("checked")) {
        $.ajax({
            method: "post",
            url: '/PayPalPayment/Get_PayPalLink',
            data:  details,
            beforeSend: function () {
                $('#buy-step-2 button.pre-btn').attr('disabled', true).html('  <img src="/Images/1.gif" />  ');
                $('body').append('<div class="cover-block"></div>')
            },
            success: function (e) {
                $('#buy-step-2 button.pre-btn').html($('.pre-btn').attr('data-text'));
                $('#buy-step-2 button').removeAttr('disabled');
                $('div.cover-block').remove();
                location.href = e.success;

            },
            error: function (err) {
                var str = "";
                err.responseJSON.error.forEach(function (elem) {
                    str += elem.ErrorMessage + "\n";
                });
                alert(str);
            }
        });
    }
    if ($("#trial_card").prop("checked")) {

        $.ajax({
            method: "post",
            url: "/CreditCardPayment/MakePayment",
            data: {
                Name: $("#name-on-card").val(),
                CardNumber: $("#card-number").val(),
                ExpMonth: parseInt($("#trial_card_month").val()),
                ExpYear: parseInt($("#trial_card_year").val()),
                Cvc: $("#cvv-card").val(),
                Advance: "EUR 0.99",
                dateId:dateId,
                PaymentDetails: details
            },
            beforeSend: function () {
                $('#buy-step-2 button.pre-btn').attr('disabled', true).html('  <img src="/Images/1.gif" />  ');
                $('body').append('<div class="cover-block"></div>')
            },
            success: function (e) {
                console.log(e);
                location.href = "/CreditCardPayment/RedirectToThanks?url=" + e.redirectTo;
                return;
                $('#buy-step-2 button.pre-btn').html($('.pre-btn').attr('data-text'));
                $('#buy-step-2 button').removeAttr('disabled');
                $('div.cover-block').remove();
                $('#buy-step-2').modal('hide');
                setTimeout(function () {
                    $('#buy-step-3').modal('show')
                }, 500);
                $('#byTrialStep2').trigger('reset');
                $('#byTrialStep1').trigger('reset');

            },
            error: function (err) {
                var error = err.responseJSON.error;
                if (Array.isArray(error)) {
                    var str = "";
                    err.responseJSON.error.forEach(function (elem) {
                        str += elem.ErrorMessage + "\n";
                    });
                    alert(str);
                    return;
                }
                console.log(err);

            }
        });
    }
    if ($("#trial_tenpay").prop("checked")) {
        $.ajax({
            method: "post",
            url: "/TenPayPayment/MakePayment",
            data: details,
            beforeSend: function () {
                $('#buy-step-2 button.pre-btn').attr('disabled', true).html('  <img src="/Images/1.gif" />  ');
                $('body').append('<div class="cover-block"></div>')
            },
            success: function (e) {
                $('#buy-step-2 button.pre-btn').html($('.pre-btn').attr('data-text'));
                $('#buy-step-2 button').removeAttr('disabled');
                $('div.cover-block').remove();
                location.href = e;
            },
            error: function (err) {
                console.log(err);
                console.log(err.responseJSON.error);
            }
        });
    }
};
$("#go_to_trialStep2").click(function () {
    $("#selLng").text($("#trial_select_lang").select2('data')[0].text.trim());
    $("#selLevel").text($("#trial_select_level").select2('data')[0].text.trim());
    $("#selDate").text($("#trial_datetimepicker").val());
});