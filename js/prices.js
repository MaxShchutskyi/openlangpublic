var CurrenciesCountry = function () {
    function n(n, t) {
        this.countryCode = n;
        this.currency = t
    }

    return n.initCurrencies = function () {
        this.listCurrencyCodes = [];
        this.listCurrencyCodes.push(new n("US", "USD"));
        this.listCurrencyCodes.push(new n("Other", "EUR"));
        this.listCurrencyCodes.push(new n("CH", "CHF"));
        this.listCurrencyCodes.push(new n("BR", "BRL"));
        this.listCurrencyCodes.push(new n("SE", "SEK"));
        this.listCurrencyCodes.push(new n("NO", "NOK"))
    }, n.convert = function (n, t, i) {
        var r = t ? t : this.getCurrencyCountry(PlansPrices.Location.code).currency;
        if (!r || t === "EUR" || r === "EUR") {
            i(n);
            return
        }
        CurrencyConverter.convert(r, n, i)
    }, n.getCurrencyCountry = function (t) {
        this.listCurrencyCodes || this.initCurrencies();
        for (var i = 0; i < this.listCurrencyCodes.length; i++)if (this.listCurrencyCodes[i].countryCode === t)return this.selectedCurrency = this.listCurrencyCodes[i];
        return this.selectedCurrency = new n("Other", "EUR")
    }, n
}(), CurrencyConverter = function () {
    function n() {
    }

    return n.convert = function (n, t, i) {
        var u = this, r = this.checkExistsСoefficient(n);
        if (!isNaN(r)) {
            i(r * t);
            return
        }
        this.getСoefficient(n, function (r) {
            u.setСoefficientToStorage(n, r);
            i(r * t)
        }, function () {
            i(null)
        })
    }, n.getСoefficient = function (n, t, i) {
        var r = "http://query.yahooapis.com/v1/public/yql?q=select%20%2a%20from%20yahoo.finance.xchange%20where%20pair%20in%20%28%22EUR" + n + "%22)&format=json&env=store://datatables.org/alltableswithkeys";
        $.ajax({
            type: "get", dataType: "json", url: r, success: function (n) {
                var i = n.query.results.rate.Rate, r = parseFloat(i);
                t(r)
            }, error: function () {
                i()
            }
        })
    }, n.checkExistsСoefficient = function (n) {
        var i = localStorage.getItem(n), r, t;
        return i ? (r = CryptoJS.AES.decrypt(i, "5899404"), t = JSON.parse(r.toString(CryptoJS.enc.Utf8)), this.compareDates(t.date)) ? t.coefficient : NaN : NaN
    }, n.compareDates = function (n) {
        var t = new Date((new Date).toLocaleString("en")), r = t.getDate(), u = t.getMonth() + 1, f = t.getFullYear(), i = new Date(n);
        return i.getDate() !== r || i.getMonth() + 1 !== u || i.getFullYear() !== f ? !1 : !0
    }, n.setСoefficientToStorage = function (n, t) {
        var i = {
            date: (new Date).toLocaleString("en"),
            coefficient: t
        }, r = CryptoJS.AES.encrypt(JSON.stringify(i), "5899404");
        localStorage.setItem(n, r)
    }, n
}(), Europe = function () {
    function n() {
    }

    return n.prototype.getPricePremium = function (n) {
        switch (n) {
            case 5:
                return new ProprtiesPrices(125, (125 / n).toFixed(2).toString());
            case 10:
                return new ProprtiesPrices(243, (243 / n).toFixed(2).toString());
            case 20:
                return new ProprtiesPrices(458, (458 / n).toFixed(2).toString());
            case 15:
                return new ProprtiesPrices(354, (354 / n).toFixed(2).toString());
            case 25:
                return new ProprtiesPrices(555, (555 / n).toFixed(2).toString());
            case 50:
                return new ProprtiesPrices(1040, (1040 / n).toFixed(2).toString());
            case 100:
                return new ProprtiesPrices(1940, (1940 / n).toFixed(2).toString());
            default:
                return null
        }
    }, n.prototype.getPriceBasic = function (n) {
        switch (n) {
            case 5:
                return new ProprtiesPrices(105, (105 / n).toFixed(2).toString());
            case 10:
                return new ProprtiesPrices(205, (205 / n).toFixed(2).toString());
            case 20:
                return new ProprtiesPrices(390, (390 / n).toFixed(2).toString());
            case 15:
                return new ProprtiesPrices(300, (300 / n).toFixed(2).toString());
            case 25:
                return new ProprtiesPrices(475, (475 / n).toFixed(2).toString());
            case 50:
                return new ProprtiesPrices(875, (875 / n).toFixed(2).toString());
            case 100:
                return new ProprtiesPrices(1600, (1600 / n).toFixed(2).toString());
            default:
                return null
        }
    }, n
}(), PlansPrices = function () {
    function n() {
    }

    return n.Init = function (n) {
        var t = this;
        this.SelectObject = $(n);
        this.PricesBtn = this.SelectObject.find(".individ_price .tutorial_btns>div");
        this.CurrentPriceBasic = this.SelectObject.find("#price_basic");
        this.CurrentPricePrimium = this.SelectObject.find("#price_primium");
        this.Currency && this.SelectObject.find(".currency_for_price").html(this.Currency === "EUR" ? "&#8364;" : this.Currency);
        this.CurrentCountLesson = this.PricesBtn.filter(".active");
        CurrenciesCountry.initCurrencies();
        this.PricesBtn.on("click", function (n) {
            var r, i;
            t.CurrentCountLesson.removeAttr("class");
            r = $(n.currentTarget);
            r.attr("class", "active");
            t.CurrentCountLesson = r;
            i = parseInt(t.CurrentCountLesson.text());
            t.SelectObject.find(".count_lessons").text(i);
            t.ChangeBasic(i);
            t.ChangePremium(i)
        });
        this.initLocation();
        $("#show_group").click(this.ShowGroup);
        $("#show_idivid").click(this.ShowIndivid)
    }, n.initLocation = function () {
        GeolocationManager.init(function (t) {
            n.Location = t;
            n.initCountryPrices();
            CurrenciesCountry.initCurrencies();
            n.ChangeBasic(parseInt(n.CurrentCountLesson.text()));
            n.ChangePremium(parseInt(n.CurrentCountLesson.text()))
        })
    }, n.initCountryPrices = function () {
        this.CuntryPrices = this.Location.country === "Sweden" || this.Location.country === "Norway" || this.Location.country === "Finland" || this.Location.country === "Denmark" || this.Location.country === "Iceland" ? new Scandinavia : this.Location.country === "Switzerland" ? new Switzerland : new Europe
    }, n.ShowGroup = function () {
        $(this).parents(".individ").find(".price").css("display", "none");
        $(this).removeClass("unselect").siblings().addClass("unselect");
        $(".individ_price").css("opacity", 0);
        $(".group").removeAttr("style")
    }, n.ShowIndivid = function () {
        $(this).parents(".individ").find(".price").show();
        $(this).removeClass("unselect").siblings().addClass("unselect");
        $(".individ_price").css("opacity", 1);
        $(".group").attr("style", "display:none")
    }, n.ChangePremium = function (n) {
        var t = this, i;
        this.CuntryPrices && (i = this.CuntryPrices.getPricePremium(n), this.PricePremium = i.price.toString(), CurrenciesCountry.convert(i.price, this.Currency, function (i) {
            var r, u;
            if (!i) {
                r = t.CuntryPrices.getPricePremium(n);
                t.CurrentPricePrimium.text(r.price.toFixed(2));
                $("#premium_cur_one_price").text(r.priceForOneLesson);
                return
            }
            u = parseFloat((i / n).toFixed(2));
            t.CurrentPricePrimium.text((u * n).toFixed(2));
            $("#premium_cur_one_price").text(u)
        }))
    }, n.ChangeBasic = function (n) {
        var t = this, i;
        this.CuntryPrices && (i = this.CuntryPrices.getPriceBasic(n), this.PriceBasic = i.price.toString(), CurrenciesCountry.convert(i.price, this.Currency, function (i) {
            var r, u;
            if (!i) {
                r = t.CuntryPrices.getPriceBasic(n);
                t.CurrentPriceBasic.text(r.price.toFixed(2));
                $("#basic_cur_one_price").text(r.priceForOneLesson);
                t.SelectObject.find(".currency_for_price").html("&#8364;");
                return
            }
            t.Currency || t.SelectObject.find(".currency_for_price").html(CurrenciesCountry.selectedCurrency.currency === "EUR" ? "&#8364;" : CurrenciesCountry.selectedCurrency.currency);
            u = parseFloat((i / n).toFixed(2));
            t.CurrentPriceBasic.text((u * n).toFixed(2));
            $("#basic_cur_one_price").text(u)
        }))
    }, n
}(), ProprtiesPrices = function () {
    function n(n, t) {
        this.price = n;
        this.priceForOneLesson = t
    }

    return n
}(), Scandinavia = function () {
    function n() {
    }

    return n.prototype.getPricePremium = function (n) {
        switch (n) {
            case 5:
                return new ProprtiesPrices(195, (195 / n).toFixed(2).toString());
            case 10:
                return new ProprtiesPrices(370.5, (370.5 / n).toFixed(2).toString());
            case 20:
                return new ProprtiesPrices(663, (663 / n).toFixed(2).toString());
            case 15:
                return new ProprtiesPrices(526.5, (526.5 / n).toFixed(2).toString());
            case 25:
                return new ProprtiesPrices(780, (780 / n).toFixed(2).toString());
            case 50:
                return new ProprtiesPrices(1462.5, (1462.5 / n).toFixed(2).toString());
            case 100:
                return new ProprtiesPrices(2847, (105 / n).toFixed(2).toString());
            default:
                return null
        }
    }, n.prototype.getPriceBasic = function (n) {
        switch (n) {
            case 5:
                return new ProprtiesPrices(175, (175 / n).toFixed(2).toString());
            case 10:
                return new ProprtiesPrices(334, (334 / n).toFixed(2).toString());
            case 20:
                return new ProprtiesPrices(604, (604 / n).toFixed(2).toString());
            case 15:
                return new ProprtiesPrices(477, (477 / n).toFixed(2).toString());
            case 25:
                return new ProprtiesPrices(715, (715 / n).toFixed(2).toString());
            case 50:
                return new ProprtiesPrices(1350, (1350 / n).toFixed(2).toString());
            case 100:
                return new ProprtiesPrices(2540, (2540 / n).toFixed(2).toString());
            default:
                return null
        }
    }, n
}(), Switzerland = function () {
    function n() {
    }

    return n.prototype.getPricePremium = function (n) {
        switch (n) {
            case 5:
                return new ProprtiesPrices(185, (185 / n).toFixed(2).toString());
            case 10:
                return new ProprtiesPrices(351.5, (351.5 / n).toFixed(2).toString());
            case 20:
                return new ProprtiesPrices(629, (629 / n).toFixed(2).toString());
            case 15:
                return new ProprtiesPrices(499.5, (499.5 / n).toFixed(2).toString());
            case 25:
                return new ProprtiesPrices(740, (740 / n).toFixed(2).toString());
            case 50:
                return new ProprtiesPrices(1387.5, (1387.5 / n).toFixed(2).toString());
            case 100:
                return new ProprtiesPrices(2701, (2701 / n).toFixed(2).toString());
            default:
                return null
        }
    }, n.prototype.getPriceBasic = function (n) {
        switch (n) {
            case 5:
                return new ProprtiesPrices(160, (160 / n).toFixed(2).toString());
            case 10:
                return new ProprtiesPrices(304, (304 / n).toFixed(2).toString());
            case 20:
                return new ProprtiesPrices(544, (544 / n).toFixed(2).toString());
            case 15:
                return new ProprtiesPrices(432, (432 / n).toFixed(2).toString());
            case 25:
                return new ProprtiesPrices(640, (640 / n).toFixed(2).toString());
            case 50:
                return new ProprtiesPrices(1200, (1200 / n).toFixed(2).toString());
            case 100:
                return new ProprtiesPrices(2240, (2240 / n).toFixed(2).toString());
            default:
                return null
        }
    }, n
}()