﻿$(document).ready(function () {
 
    $('.flags-group a').click(function () {
        $('.js-select2-country').val($(this).attr('data-lang')).trigger("change");
    })

    $("#google_login,#google_signup").click(function() {
        //location.href = '/Account/GoogleLogin' + "?selLang=" + (selLangg ? selLang : "");
        //handleAuthClick();
        new SocialResponseHundler().buttonClickEvent(new GoogleAuthourizeTs());
        $(this).parents(".modal-content").find("button.close").click();
        //location.href = '/Account/GoogleLogin';
    });
        $("#facebook_login,#facebook_signup").click(function() {
            //location.href = '/Account/FacebookLogin' + "?selLang=" + (selLangg ? selLang : "");
            //location.href = '/Account/FacebookLogin';
            //facebookLogin();
            new SocialResponseHundler().buttonClickEvent(new FacebookAuthorizeTs());
            $(this).parents(".modal-content").find("button.close").click();
        });
        $("#stayTouchClick").click(function() {
            var email = $("#send_email").val();
            if (!email || !email.match(Patterns.email)) {
                $(this).siblings("input").css("border-color", "red");
                $("#notEnterFieldTestimonials").fadeIn("slow");
                return;
            }
            $(this).siblings("input").css("border-color", "#ccc");
            $("#notEnterFieldTestimonials span").text("Your message has been sent ").parent().fadeIn("slow", function() {
                setTimeout(function() {
                    $("#notEnterFieldTestimonials").fadeOut("slow", function() {
                        $("#notEnterFieldTestimonials span").text("@Resources.Resource.EnterCorrectEmail");
                    });
                }, 5000);
            });
            $.ajax({
                url: "/openlang/StayInTouchSubscription",
                type: "POST",
                data: { email: email },
                async: true,
                success: function(result) {
                    $("#stayTouchClick").siblings("input").val("");
                },
                error: function(er) {
                    alert(er.message);
                }
            });
        });

        setEqualHeight($(".slider-col > div p"));
        setEqualHeight($(".slider-col > div ul"));
        setEqualHeight($(".customers .custom-media"));


        $('.jquery-background-video').bgVideo({
            fadeIn: 2000,
            showPausePlay: false,
            pauseAfter: 0
        });
        var owl = $('.owl-carousel');
        if ($(window).width() < 992) {

            owl.owlCarousel({
                items: 1,
                dots: true
            });

        } else {
            owl.trigger('destroy.owl.carousel');
        }

        //$('.js-closeModals').click(function() {
        //    $target = $(this).attr('data-target')
        //    $(this).parents('.modal').modal('hide');
        //    setTimeout(function() {
        //        $($target).modal('show');
        //    }, 150)
        //})
        $('.modal-open').click(function() {
        })

        //$(".js-lang").click(function() {
        //    var selLang = $(this).data("lang");
        //    var chLang = $(this).data("check");
        //    $.cookie("lang", selLang, { path: "/" });
        //    $.cookie("checkLang", chLang, { path: "/" });
        //    location.reload();
        //});


        //if ($.cookie("checkLang") != null) {
        //    var cook = $.cookie("checkLang");
        //    var chech = $(".js-lang").filter("[data-check = '" + cook + "']");
        //    $(".js-selectedLocalLang").text(chech.text());
        //    chech.css('display', 'none');
        //    //document.write($.cookie("checkLang"));
        //    //ckCook = $.cookie("checkLang");
        //} else {
        //    $(".js-selectedLocalLang").text("English");
        //    ckCook = "English";
        //}

        //mainResize();
    });
    //$(window).resize(function() {
    //    mainResize();
    //});
    function setEqualHeight(columns) {
        var tallestcolumn = 0;
        columns.each(
        function () {
            currentHeight = $(this).height();
            if (currentHeight > tallestcolumn) {
                tallestcolumn = currentHeight;
            }
        }
        );
        columns.height(tallestcolumn);
    }
    function go_login(errorMessage) {
        var name = $("#login-name").val();
        var pass = $("#login-password").val();
        var locate = window.location.href;
        $.ajax({
            url: "/Account/CheckExistUser",
            type: "post",
            data: { 'name': name, 'password': pass },
            async: true,
            success: function (result) {
                if (result === "True") {
                    form = $('#loginform')
                    $.ajax({
                        type: "POST",
                        url: form.attr("action"),
                        data: form.serialize(), // serializes the form's elements.
                        success: function () {
                            window.location = locate
                        }
                    });
                    //document.forms["loginform"].submit();
                $('#loginform .js-service').fadeOut(300);
            } else {
                    $('#loginform .js-service span').text(errorMessage);
                    $('#loginform .js-service').fadeIn(300);
                    //alert(errorMessage);
                }
            },
            error: function () {
                //alert('Error occured');
            }
        });
    }


    function go_tellUsMore(error) {
        var pop = $("#formLogin");
        var email = pop.find("#signup-email").val();
        $.ajax({
            url: "/Openlang/CheckExistUserByEmail",
            type: "post",
            data: { email: email },
            success: function (result) {
                if (!result.result)
                    alert(error);
            },
            error: function() {
            }
        });
}

    //function mainResize() {
    //    //if (window.location.href == 'http://localhost:21514/' || window.location.href == 'http://xencoredevelop.azurewebsites.net/' || window.location.href == 'http://xencoredevelop.azurewebsites.net/') {
       
    //    var windowHeight = $(window).height();
    //    var windowWidth = $(window).width();
    //    if (windowWidth < 540) {
    //        var widthItem = $('.for-xs-slider .slider-col');
    //        $('.for-xs-slider').width(widthItem[0].offsetWidth * 2 + 'px')
    //    }
    //    if (windowWidth < 530) {
    //        var allWidth = $('.for-xs-slider').width();
    //        var block = $('.slider-col');
    //        var windowWidth = $(window).width();
    //        $('.for-xs-slider')[0].addEventListener('swr', function() {
    //            $('.for-xs-slider').animate({
    //                'right': 0 + 'px'
    //            }, 100)
    //            block.removeClass('opacity');
    //            $(block[1]).addClass('opacity');
    //        }, false);
    //        $('.for-xs-slider')[0].addEventListener('swl', function() {
    //            $('.for-xs-slider').animate({
    //                'right': allWidth - windowWidth + 'px'
    //            }, 100)
    //            block.removeClass('opacity');
    //            $(block[0]).addClass('opacity');
    //        }, false);
    //    }
    //    };
    ////}

    (function($) {
        $(window).load(function() {
            $('.custom-scroll').height(Math.ceil($(window).height() * 0.6))
            $(".custom-scroll").mCustomScrollbar();
        });
    })(jQuery);

    jQuery(function($) {
        $.mask.definitions['~'] = '[+-]';
        //$(".card-number").mask("9999 9999 9999 9999 999", { placeholder: "0000 0000 0000 0000 000" });
    });

    $(document).ready(function () {
        var $page = $('html, body');
        $('a[href*="#"].for-test').click(function () {
            $page.animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 600);
            return false;
        });

    var contentSections = $('.cd-section'),
                navigationItems = $('#cd-vertical-nav a');

    updateNavigation();
    $(window).on('scroll', function () {
        updateNavigation();
    });

    //smooth scroll to the section
    navigationItems.on('click', function (event) {
        event.preventDefault();
        smoothScroll($(this.hash));
    });
    //smooth scroll to second section
    $('.cd-scroll-down').on('click', function (event) {
        event.preventDefault();
        smoothScroll($(this.hash));
    });

    //open-close navigation on touch devices
    $('.touch .cd-nav-trigger').on('click', function () {
        $('.touch #cd-vertical-nav').toggleClass('open'); 

    });
    //close navigation on touch devices when selectin an elemnt from the list
    $('.touch #cd-vertical-nav a').on('click', function () {
        $('.touch #cd-vertical-nav').removeClass('open');
    });

    function updateNavigation() {
        contentSections.each(function () {
            $this = $(this);
            var activeSection = $('#cd-vertical-nav a[href="#' + $this.attr('id') + '"]').data('number') - 1;
            if (($this.offset().top - $(window).height() / 2 < $(window).scrollTop()) && ($this.offset().top + $this.height() - $(window).height() / 2 > $(window).scrollTop())) {
                navigationItems.eq(activeSection).addClass('is-selected');
            } else {
                navigationItems.eq(activeSection).removeClass('is-selected');
            }
            if (navigationItems.eq(activeSection).hasClass('is-selected')) {
                if (activeSection == 2 || activeSection == 4) {
                    $('.cd-label').css('color', 'white')
                } else {
                    $('.cd-label').css('color', '#a8c75f')
                }
            }

        });
    }

    function smoothScroll(target) {
        $('body,html').animate(
                { 'scrollTop': target.offset().top },
                600
        );
    }
    $('.js-toogle-payment').click(function() {
        $.each($('.js-toogle-payment'), function(e, c) {
            c.checked = false;
            $('.' + $(c).attr('data-type')).addClass('hidden')
        })
        $(this)[0].checked = true;
        $('.' + $(this).attr('data-type')).removeClass('hidden')
    });

    $('.confirm').change(function() {
        if ($(this)[0].checked == true) {
            $('.pay').attr('disabled', false);
        } else {
            $('.pay').attr('disabled', 'disabled');
        }
    });

    //$('.pay').click(function(e) {
    //    e.preventDefault();
    //    if ($('.confirm')[0].checked == true) {
    //        nextModal($(this));
    //    } else {
    //        alert('accept our terms')
    //    }

    //});
    $('.times').on('keypress', function (e) {
        if (e.charCode <= 48 && e.charCode >= 57) {
            e.preventDefault
        }
    });
    $('.times').on('keyup', function(e) {
        if (e.keyCode != 8) {
                var string = $(this).val();
                var check = string.split(':');
                if (string.length == 1 && string > 2) {
                    $(this).val('0' + $(this).val() + ':')
                }
                if (string.length == 2 && check.length == 1) {
                    $(this).val($(this).val() + ':')
                }
                if (check.length > 1 && check[1] > 59) {
                    $(this).val(check[0] + ':' + 59)
                }

                if (string.length >= 3 && check.length == 1) {
                        $(this).val(23 + ':' + 59)
                }
                if (string.length >= 5) {
                    if (!validateTime($(this).val())) {
                        $(this).val(23 + ':' + 59)
                    }
                    
                }
                console.log(string);
           
        }
    });
    $('.js-closeModals').click(function() {
        nextModal($(this))
    });

    //if ($.cookie("checkLang") != null) {
    //    var cook = $.cookie("checkLang");
    //    var chech = $(".js-lang").filter("[data-check = '" + cook + "']");
    //    $(".js-selectedLocalLang").text(chech.text());
    //    chech.css('display', 'none');
    //    //document.write($.cookie("checkLang"));
    //    //ckCook = $.cookie("checkLang");
    //} else {
    //    $(".js-selectedLocalLang").text("English");
    //    ckCook = "English";
    //}
    $('.time').timepicker({
        minuteStep: 1,
        template: 'modal',
        appendWidgetTo: 'body',
        showSeconds: false,
        showMeridian: false,
        defaultTime: false
    });


    //$(".phone").intlTelInput({
    //    //customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
    //    //    return "e.g. + " + selectedCountryData.dialCode + ' ' + selectedCountryPlaceholder;
    //    //},

    //    utilsScript: "/js/utils.js"

    //});
});

    function validateTime(time) {
        var re = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
        return re.test(time);
    }


function resetPassword() {
    var email = $("#reset_select_email").val();
    var telephone = $("#reset_select_phone").intlTelInput("getNumber");
    if ((email && telephone) || (!email && !telephone)) {
        $("#reset_select_email").parent().addClass('has-error');
        $("#reset_select_phone").parent().addClass('has-error');
        $('#reset-pass .js-service span').append('You need select one way')
        $('#reset-pass .js-service').fadeIn('slow')
        return;
    }
    $.ajax({
        context: this,
        url: "/Account/FogotPassword",
        type: "post",
        data: { 'email': email, 'telephone': telephone },
        beforeSend: function(){
            $('#resetpassword button.pre-btn').attr('disabled', true).html('  <img src="/Images/1.gif" />  ');
            $('body').append('<div class="cover-block"></div>')
        },
        success: function (result) {
            $('#resetpassword button.pre-btn').html($('.pre-btn').attr('data-text'));
            $('#resetpassword button').removeAttr('disabled');
            $('div.cover-block').remove();
            if (!email) {
                //$('#resetpassword2 .modal-body p').text("Please enter valid email address or mobile number");
                $('#resetpassword2 .modal-body span').text(asteriskify(telephone, 7));
            }
            if (!telephone) {
                var str = email.split('@')
                //$('#resetpassword2 .modal-body p').text("Please enter valid email address or mobile number");
                $('#resetpassword2 .modal-body span').text(asteriskify(str[0],3)+'@'+str[1]);
            }
            $('#resetpassword').modal('hide')
            setTimeout(function () {
                $('#resetpassword2').modal('show')
            }, 500);
        },
        error: function (e) {
            //alert('Error occured');
        }
       
    });
}

function nextModal(that) {
        $target = that.attr('data-target')
        that.parents('.modal').modal('hide');
        setTimeout(function() {
            $($target).modal('show');
        }, 700)
}

$('#buy-full-step-2')
    .on('hide.bs.modal',
        function() {
            discountId = '';
        });
discountId = '';
function nextModalBuyFull(that) {
    var disc = $("#discount").val();
    var tt = $("#buy-full-step-1 .white");
    var currency = tt.find(".currency-for-price").text();
    var amount = tt.children().last().text();
    if (disc) {
        $.ajax({
            context: that,
            url: "/Discount/GetDiscount",
            type: "post",
            data: { 'number': disc, 'ammount': amount, 'currency': currency },
            dataType: "json",
            success: function (res) {
                var tt = $("#buy-full-step-1 .white");
                var currency = tt.find(".currency-for-price").text();
                var amount = tt.children().last().text();
                if (res.newPrice) {
                    var step2 = $("#buy-full-step-2");
                    //selectedprice = res.newPrice.toFixed(1);
                    discountId = res.Id;
                    step2.find(".total-discount").text(currency + " " + (parseFloat(amount) - res.newPrice).toFixed(1) + "0");
                    step2.find(".total-amount").text(currency + " " + res.newPrice.toFixed(1) + "0");
                } else {
                    var step2 = $("#buy-full-step-2");
                    step2.find(".total-discount").text(currency + " 0");
                    step2.find(".total-amount").text(currency + " " + amount);
                }
                var $target = $(this).attr('data-target')
                $(this).parents('.modal').modal('hide');
                setTimeout(function() {
                        $($target).modal('show');
                    },
                    700);
            }

        });
    } else {
        var step2 = $("#buy-full-step-2");
        step2.find(".total-discount").text(currency + " 0");
        step2.find(".total-amount").text(currency + " " + amount);
        $target = that.attr('data-target')
        that.parents('.modal').modal('hide');
        setTimeout(function () {
            $($target).modal('show');
        },
            700);
    }

}


function signUp() {
        var pop = $("#formLogin");
        var frm = document.forms.formToSend;
        var name = frm.elements.Name.value = pop.find("#signup-name").val();
        var email = frm.elements.Email.value = pop.find("#signup-email").val();
        var pass = frm.elements.Password.value = pop.find("#signup-password").val();
        var confPass = frm.elements.ConfirmPassword.value = pop.find("#signup-passwordConfirm").val();
        var selectLang = frm.elements.ChosenLang.value = $("#signup_select_lang").select2('data')[0].text.trim() + "_" + $("#signup_select_lang").select2('data')[0].id;
        var howlevel = frm.elements.ChosenLevel.value = $("#signup_select_level").select2("data")[0].text.trim();
    //var likeLearn = frm.elements.Skype.value = $("#signup_select_learn_via").select2("data")[0].text.trim();
        var likeLearn = frm.elements.Skype.value = $("#SkypeId").val();
        var tel = frm.elements.PhoneNumber.value = $("#signup_select_phone").intlTelInput("getNumber");
        var howfind = frm.elements.HowDidFind.value = $("#signup_select_find_us").select2("data")[0].text.trim();
        var timezone = frm.elements.TimeZone.value = $("#signup_select_timezone").select2("data")[0].text.trim();
        var ispeak = frm.elements.ISpeak.value = $("#ISpeak").select2("data")[0].text.trim() + "_" + $(".js-select2-native-language").val() + "|";
        console.log($(".js-select2-native-language").select2("data")[0]);
        var data = {
            'name': name,
            'password': pass,
            'confirmPassword': confPass,
            'email': email,
            'ChosenLang': selectLang,
            'ChosenLevel': howlevel,
            'Skype': likeLearn,
            'PhoneNumber': tel,
            'HowDidFind': howfind,
            'TimeZone': timezone,
            'ISpeak': ispeak
        };
        console.log(data);
        $.ajax({
            context: frm,
            url: "/openlang/CheckNewUser",
            type: "post",
            data: data,
            async: true,
            beforeSend: function () {
                $('#go_signup').attr('disabled', true).html('  <img src="/Images/1.gif" />  ');
                $('body').append('<div class="cover-block"></div>')
            },
            success: function (result) {
               
                if (result.result === "OK")
                    $(this).submit();
                else {
                    $('#go_signup').html($('#go_signup').attr('data-text'));
                    $('#go_signup').removeAttr('disabled');
                    $('div.cover-block').remove()
                    var str = "";
                    result.result.forEach(function (elem) {
                        str += elem.ErrorMessage + "\n";
                    });
                    alert(str);
                }
            },
            error: function () {
                $('#go_signup').html($('#go_signup').attr('data-text'));
                $('#go_signup').removeAttr('disabled');
                $('div.cover-block').remove()
                alert(resource.match('error_occured'));
            }
        });
}

    $(function() {
    var date = new Date();
    $(".datetimepicker:not('.hasMinDate')").datetimepicker({
        minDate: date,
        format: 'DD/MM/YYYY',
        allowInputToggle: true,
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        }
    });

});
function asteriskify(word, letters, type) {
    type = type || '*';

    var asteriskesNumber = word.length - Math.abs(letters);
    var asteriskes = (asteriskesNumber > 0 ? (new Array(asteriskesNumber + 1)).join(type) : '');

    if (letters > 0) {
        return word.substring(0, letters) + asteriskes;
    } else {
        return asteriskes + word.substring(word.length + letters, word.length)
    }
}


    function formatState(state) {
        if (!state.id) {
            return state.text;
        }
        var $state = $(
            '<span><img src="/images/flag/' + state.element.value.toLowerCase() + '.png" class="img-flag" /> ' + state.text + '</span>'
        );
        return $state;
    }

    function formatSocials(state) {
        if (!state.id) {
            return state.text;
        }
        var $state = $(
            '<span><img src="/images/socials/' + state.element.value.toLowerCase() + '.png" class="img-socials" /> ' + state.text + '</span>'
        );
        return $state;
    }

    (function($) {
        $(window).load(function() {
            $('.custom-scroll').height(Math.ceil($(window).height() * 0.6))
            $(".custom-scroll").mCustomScrollbar();
        });
    })(jQuery);

