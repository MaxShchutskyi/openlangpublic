var Patterns = (function () {
    function Patterns() {
    }
    Patterns.time = /^\s*([01]?\d|2[0-3]):?([0-5]\d)\s*$/;
    Patterns.replaceSpaces = / /g;
    Patterns.replaceTabsAndNewLines = /[_\s]/g;
    Patterns.replaceQuot = /&quot;/g;
    Patterns.email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    Patterns.numbers = /^[0-9]$/;
    Patterns.date = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    Patterns.password = /^[a-zA-Z0-9!#$%^&*]{6,16}$/;
    Patterns.loadFile = /(\jpg|\jpeg|\png|\pdf|\xls)$/i;
    Patterns.replaceSpecSymbols = /[&\\#,=_+()$~%'"*?<>{}]/g;
    Patterns.loadImage = /(\jpg|\jpeg|\BMP|\bmp|\image\/bmp|\png|\image\/x-icon)$/i;
    return Patterns;
}());
//# sourceMappingURL=Patterns.js.map