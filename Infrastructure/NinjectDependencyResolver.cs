﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Domain.MyAccount.Abstract;
using Domain.MyAccount.Concrete;
using Ninject;
using Ninject.Activation;
using Uow.MyAccount.Abstract;
using Uow.MyAccount.Api;
using IRequest = Uow.MyAccount.Abstract.IRequest;

namespace MyAccount.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            // Domain
            kernel.Bind<IAttendeeRepository>().To<EFAttendeeRepository>();
            kernel.Bind<IClassRepository>().To<EFClassRepository>();
            kernel.Bind<ILevelRepository>().To<EFLevelRepository>();
            kernel.Bind<IPresenterRepository>().To<EFPresenterRepository>();
            kernel.Bind<ISubjectRepository>().To<EFSubjectRepository>();

            // Uow
            kernel.Bind<IAuthBase>().To<AuthBase>();
            kernel.Bind<IFromListToModel>().To<FromListToModel>();
            kernel.Bind<IRequest>().To<Uow.MyAccount.Api.Request>();
            kernel.Bind<IWizHttpRequest>().To<WizHttpRequest>();
            kernel.Bind<IXmlGetResponse>().To<XmlGetResponse>();
        }
    }
}