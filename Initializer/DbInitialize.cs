﻿using System;
using System.Data.Entity;
using System.Net.Http;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.IdentityEntities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

using Xencore.Entities;
using Xencore.Entities.IdentityEntities;
namespace Xencore.Initializer
{
    public class DbInitialize:DropCreateDatabaseIfModelChanges<MyDbContext>
    {
        protected async override void Seed(MyDbContext context)
        {
            base.Seed(context);
            using (var context2 = new MyDbContext())
            {
                var roleManager = new CustomRoleManager(new CustomRoleStore(context2));
                await roleManager.CreateAsync(new CustomRole() { Name = "Admin" });
                await roleManager.CreateAsync(new CustomRole() { Name = "Presenter" });
                var userManager = new CustomUserManager(new CustomUserStore(context2));
                var admin = new CustomUser()
                {
                    Name = "Admin",
                    UserName = "pedagog-99@mail.ru",
                    Email = "pedagog-99@mail.ru",
                    Currency = "RUB - Ruble",
                    Location = "Russia"
                };
                await userManager.CreateAsync(admin, "5899404");
                userManager.AddToRole(admin.Id, "Admin");
            }
            using (var context3  = new MyDbContext())
            {

                using (var client = new HttpClient())
                {
                    var result = await client.GetAsync(new Uri("http://services.groupkt.com/country/get/all"));
                    var json = JsonConvert.DeserializeObject<dynamic>(await result.Content.ReadAsStringAsync());
                    var countries = json.RestResponse.result;
                    foreach (var country in countries)
                    {
                        context3.Countries.Add(new Countries()
                        {
                            Alpha2_code = country.alpha2_code,
                            Alpha3_code = country.alpha3_code,
                            Name = country.name
                        });
                    }
                }
                context3.SaveChanges();
            }

        }

    }
}
