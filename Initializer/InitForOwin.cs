﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppFunc = System.Func<System.Collections.Generic.IDictionary<string, object>, System.Threading.Tasks.Task>;

namespace Xencore.Initializer
{
    public class InitForOwin
    {
        private readonly AppFunc _next;

        public InitForOwin(AppFunc next)
        {
            if (next == null)
                throw new ArgumentNullException(nameof(next));

            this._next = next;
        }

        public Task Invoke(IDictionary<string, object> env)
        {
            Database.SetInitializer(new DbInitialize());
            return this._next(env);
        }
    }
}
