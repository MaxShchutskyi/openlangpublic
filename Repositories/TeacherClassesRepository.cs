﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.IdentityEntities;
using WebGrease.Css.Extensions;
using Xencore.Entities;
using Xencore.Entities.IdentityEntities;
using Xencore.Models;
using Xencore.ViewModel;

namespace Xencore.Repositories
{
    public class TeacherClassesRepository
    {
        public async Task<int> GetCountAllClasses(Guid id)
        {
            using (var context = new MyDbContext())
            {
                return await context.TeacherClasseses.CountAsync(x => x.TeacherCreatedId == id);
            }
        }
        public async Task<bool> DeleteClassById(IEnumerable<int> ids)
        {
            var classes = ids.Select(id => new TeacherClasses() {Id = id}).ToList();
                using (var context = new MyDbContext())
                {
                    foreach (var clas in classes)
                    {
                        context.TeacherClasseses.Attach(clas);
                        context.TeacherClasseses.Remove(clas);
                    }
                    await context.SaveChangesAsync();
                }
            return true;
        }
        public async Task<TeacherClassesUi> GetClassById(int id)
        {
            using (var context = new MyDbContext())
            {
                var res = await context.TeacherClasseses.Where(x => x.Id == id).Include(x => x.Students).FirstOrDefaultAsync();
                return new TeacherClassesUi()
                {
                    Id = res.Id, Students = res.Students.Select(x=>x.Id), Duration = res.Duration, SubjectOfTheLesson = res.SubjectOfTheLesson, StartDate = res.StartDate, Language = res.Language, MoneyEarned = res.MoneyEarned, Time = res.Time, Level = res.Level, MaterialsUsed = res.MaterialsUsed, TeacherId = res.TeacherCreatedId, Comments = res.Comments
                };
            }
        }

        public async Task<bool> AddNewClass(TeacherClassesUi model)
        {
            var teach = new TeacherClasses()
            {
                Comments = model.Comments,
                Duration = model.Duration,
                IsPrivate = model.IsPrivate,
                Language = model.Language,
                Level = model.Level,
                TeacherCreatedId = model.TeacherId,
                MoneyEarned = model.MoneyEarned,
                Time = model.Time,
                MaterialsUsed = model.MaterialsUsed,
                StartDate = model.StartDate,
                SubjectOfTheLesson = model.SubjectOfTheLesson,
                Students = model.Students.Select(x => new CustomUser() {Id = x}).ToList()
            };
                using (var context = new MyDbContext())
                {
                    teach.Students.ForEach(x=> context.Users.Attach(x));
                    if (model.Id != 0)
                    {
                        var clas = new TeacherClasses() { Id = model.Id };
                        context.TeacherClasseses.Attach(clas);
                        context.TeacherClasseses.Remove(clas);
                    }
                    context.TeacherClasseses.AddOrUpdate(teach);
                    await context.SaveChangesAsync();
                }
            return true;
        }

        public async Task<TeacherClassesWithStudents> GetAllClasses(Guid teacherId)
        {
            var date = DateTime.UtcNow;
            var startDate = new DateTime(date.Year, date.Month, 1);
            var endDate = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
            using (var context = new MyDbContext())
            {
                var res = (await
                    context.TeacherClasseses.Where(x => x.TeacherCreatedId == teacherId && x.StartDate >= startDate && x.StartDate<= endDate)
                        .Include(x => x.Students)
                        .Select(x => new
                        {
                            x.Id,
                            x.Language,
                            x.Level,
                            x.StartDate,
                            x.MoneyEarned,
                            x.IsPrivate,
                            x.Duration,
                            x.Time,
                            x.SubjectOfTheLesson,
                            x.Comments,
                            x.MaterialsUsed,
                            Students = x.Students.Select(f => f.Name),
                        }).ToListAsync()).Select(x => new TeacherClassesViewModel()
                {
                    Id = x.Id,
                    Duration = x.Duration,
                    IsPrivate = x.IsPrivate,
                    Language = x.Language,
                    Time = x.Time,
                    Level = x.Level,
                    SubjectOfTheLesson = x.SubjectOfTheLesson,
                    MaterialUsed = x.MaterialsUsed,
                    Comments = x.MaterialsUsed,
                    StartDate = x.StartDate,
                    MoneyEarned = x.MoneyEarned,
                    StudentsNames = x.Students
                }).ToList();
                var stds =
                    (await context.OrderDetailses.Where(x => x.CourseBoughtFromId == teacherId && x.CountLessons!= x.CoursesSpent)
                        .Include(x => x.PaymentTransactionsId.CustomUserId)
                        .Select(x => new
                        {
                            x.PaymentTransactionsId.CustomUserId.Name,
                            x.PaymentTransactionsId.CustomUserId.Id
                        }).Distinct().ToListAsync()).Select(x=>new CustomUser() {Id = x.Id, Name = x.Name}).ToList();
                return new TeacherClassesWithStudents() {StudentsOfTheTeacher = stds, TeacherClassesViewModels = res};
            }
        }
    }
}