﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Domain.Context;
using Domain.Entities;
using Xencore.Entities;
using Xencore.Models;

namespace Xencore.Repositories
{
    public class DiscountRepository
    {
        public async Task<Discount> GetDiscountByNumber(string number, Guid id)
        {
            using (var context = new MyDbContext())
            {
                var dt = DateTime.UtcNow;
                var res =
                    await
                        context.Discounts.Where(
                                x => x.Number == number && dt > x.PeriodAccessStart && dt < x.PeriodAccessEnd && x.IsActive)
                            .Include(x => x.Payments)
                            .FirstOrDefaultAsync();
                if (res == null) return null;
                return res.Payments.Any(x => x.AttendeeId == id && x.Approved) ? null : res;
            }
        }

        public async Task<IEnumerable<Discount>> GetAllDiscounts()
        {
            using (var context = new MyDbContext())
            {
                var res =  await context.Discounts.ToArrayAsync();
                return res.Select(x => new Discount()
                {
                    Id = x.Id,
                    IsActive = x.IsActive,
                    IsPercent = x.IsPercent,
                    Number = x.Number,
                    PeriodAccessStart = x.PeriodAccessStart,
                    PeriodAccessEnd = x.PeriodAccessEnd,
                    Type = x.Type,
                    Value = x.Value
                }).ToArray();
            }
        }
        public async Task<Discount> AddNewDiscount(DiscountViewModel model)
        {
            using (var context = new MyDbContext())
            {
                var discount = new Discount()
                {
                    IsActive = true,
                    IsPercent = model.IsPercent,
                    Number = model.Number,
                    Value = model.Value,
                    Type = model.Type,
                    PeriodAccessStart = model.DateStart,
                    PeriodAccessEnd = model.DateEnd
                };
                context.Discounts.Add(discount);
                await context.SaveChangesAsync();
                return discount;
            }
        }
    }
}