﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.HelpersEntity;
using NLog;
using Xencore.Helpers;

namespace Xencore.Repositories
{
    public class CreditCardRepository
    {
        public async Task<PaymentTransactions> Add(CreditCardHelper card,string idPayment, Guid userId,string payMethod,string advance, int dateId, int? discountId, decimal eurPrice)
        {
            try
            {
                using (var context = new MyDbContext())
                {
                    var details = Mapper.Map<OrderDetails>(card.PaymentDetails);
                    var pay = new PaymentTransactions()
                    {
                        PaymentMethod = payMethod,
                        AttendeeId = userId,
                        PaymentId = idPayment,
                        PayerId = "Stripe Card",
                        Amount = card.PaymentDetails.Amount,
                        Approved = true,
                        OrderDetailsId = details,
                        CurrenceAmount = advance,
                        EurPrice = eurPrice
                    };
                    if (discountId != null)
                    {
                        var discount = await context.Discounts.FindAsync(discountId);
                        pay.Discounts = new List<Discount>() { discount };
                    }
                    context.PaymentTransactions.Add(pay);
                    context.SaveChanges();
                    var pmnt = 
                        context.PaymentTransactions.Where(x => x.AttendeeId == userId && x.PaymentId == idPayment)
                            .Include(x => x.OrderDetailsId)
                            .Include(x => x.CustomUserId)
                            .First();
                    var nechLang = card.PaymentDetails.ChLang.Split('_')[1];
                    await ChangeILearn.ChangeAsync(pmnt.CustomUserId, context, nechLang);
                    //var schedule = await context.Schedules.FirstOrDefaultAsync(x => x.Id == dateId);
                    //schedule.PaymentTransactionsId = pay.PaymentId;
                    //schedule.Status = "view4";
                    //schedule.IsPending = true;
                    await context.SaveChangesAsync();
                    return pmnt;
                }
            }
            catch (DbEntityValidationException e)
            {
                LogManager.GetCurrentClassLogger().Error(e,"Error to adding payment in database");
                return null;
            }
        }
    }
}
