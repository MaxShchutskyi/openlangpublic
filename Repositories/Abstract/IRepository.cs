﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Xencore.Repositories.Abstract
{
    public interface IRepository<T>: IDisposable where T : class
    {
        IEnumerable<T> Get();
        Task<IEnumerable<T>> GetAsync();
        IEnumerable<T> GetByQuery(Expression<Func<T, bool>> call);
        Task<IEnumerable<T>> GetByQueryAync(Expression<Func<T, bool>> call);
        void Add(T elem);
        void Delete(T elem);
        void DeleteByParam(Expression<Func<T, bool>> call);
        void Update(T elem);
        void Save();
        T GetById<TKey>(Expression<Func<T, TKey>> func);
        void DeleteById<TKey>(Expression<Func<T, TKey>> func);
        void SaveAsync();
    }
}
