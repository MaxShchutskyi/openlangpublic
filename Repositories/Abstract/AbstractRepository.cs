﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using Domain.Context;
using Xencore.Entities;

namespace Xencore.Repositories.Abstract
{
    public abstract class AbstractRepository<T> : IRepository<T> where T:class
    {
        public MyDbContext Context { get; set; }

        AbstractRepository()
        {
            Context = new MyDbContext();
        }

        public void DeleteById<TKey>(Expression<Func<T, TKey>> func)
        {
            var res = Context.Set<T>().Find(func);
            Context.Set<T>().Remove(res);
        }
        public T GetById<TKey>(Expression<Func<T, TKey>> func)
        {
            return Context.Set<T>().Find(func);
        }
        public async Task<IEnumerable<T>> GetByQueryAync(Expression<Func<T, bool>> call)
        {
            return await Context.Set<T>().Where(call).ToListAsync();
        }

        public void Add(T elem)
        {
            Context.Set<T>().Add(elem);
        }

        public void Delete(T elem)
        {
            Context.Set<T>().Remove(elem);
        }

        public void DeleteByParam(Expression<Func<T, bool>> call)
        {
            Context.Set<T>().RemoveRange(Context.Set<T>().Where(call));
        }

        public void Dispose()
        {
            Context.Dispose();
        }

        public IEnumerable<T> Get()
        {
            return Context.Set<T>().ToList();
        }

        public async Task<IEnumerable<T>> GetAsync()
        {
            return await Context.Set<T>().ToListAsync();
        }

        public IEnumerable<T> GetByQuery(Expression<Func<T, bool>> call)
        {
            return Context.Set<T>().Where(call).ToList();
        }

        public void Save()
        {
            Context.SaveChanges();
        }

        public async void SaveAsync()
        {
            await Context.SaveChangesAsync();
        }

        public abstract K Para<K>(K peram);

        public void Update(T elem)
        {
            Context.Set<T>().Attach(elem);
        }
    }
}