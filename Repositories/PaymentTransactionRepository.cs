﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Domain.Context;
using Domain.Entities;
using Domain.Entities.HelpersEntity;

namespace Xencore.Repositories
{
    public class PaymentTransactionRepository
    {
        public List<PaymentTransactions> GetAllTransactionsById(string userId)
        {
            var gUserId = Guid.Parse(userId);
            using (var context = new MyDbContext())
            {
                return context.PaymentTransactions.Include(x=>x.OrderDetailsId).Where(x => x.AttendeeId == gUserId && x.Approved).OrderByDescending(x=>x.CreateDate).ToList();
            }
        }
        public PaymentTransactions GetAllTransactionsByIdPayment(string paymentId)
        {
            using (var context = new MyDbContext())
            {
                return context.PaymentTransactions.Include(x => x.OrderDetailsId).Include(x=>x.CustomUserId).First(x => x.PaymentId == paymentId);
            }
        }

        public bool Add(string userId, PaymentDetailsHelper details, string paymentId, string payMethod, string curAmount, int dateId, int?discountId, decimal eurPrice)
        {
            try
            {
                using (var context = new MyDbContext())
                {
                    var tranDetails = Mapper.Map<OrderDetails>(details);
                    var pay = new PaymentTransactions()
                    {
                        AttendeeId = Guid.Parse(userId),
                        Amount = details.Amount,
                        PaymentId = paymentId,
                        OrderDetailsId = tranDetails,
                        PaymentMethod = payMethod,
                        CurrenceAmount = curAmount,
                        EurPrice = eurPrice
                    };
                    if (discountId != null)
                    {
                        var discount = new Discount() {Id = (short) discountId};
                        context.Discounts.Attach(discount);
                        pay.Discounts = new List<Discount>() { discount};
                    }
                    context.PaymentTransactions.Add(pay);
                    context.SaveChanges();
                    //var sc = context.Schedules.FirstOrDefault(x => x.Id == dateId);
                    //if (sc != null)
                    //{
                    //    sc.IsPending = true;
                    //    sc.PaymentTransactionsId = pay.PaymentId;
                    //    sc.Status = "view4";
                    //}
                    context.SaveChanges();

                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
