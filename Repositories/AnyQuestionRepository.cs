﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Domain.Context;
using NLog;
using Xencore.Entities;
using Xencore.Entities.IdentityEntities.Entities;

namespace Xencore.Repositories
{
    public class AnyQuestionRepository
    {
        public bool Add(AnyQuestion quest)
        {
            using (var context = new MyDbContext())
            {
                try
                {
                    context.AnyQuestions.Add(quest);
                    context.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    LogManager.GetCurrentClassLogger().Error(e,"Error in add AnyQuestion method");
                    return false;
                }

            }
        }
    }
}
