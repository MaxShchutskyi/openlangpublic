﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Domain.Entities.HelpersEntity;
using NLog;
using Stripe;
namespace Xencore.Services
{
    public class StripeService
    {
        private string[] Pl { get; set; }
        private CreditCardHelper Card { get; set; }
        private string ApiKey=> System.Configuration.ConfigurationManager.AppSettings["stripeAPIKey"];
        private StripeClient Client { get; set; }

        public StripeService(CreditCardHelper card, string[] pl)
        {
            Pl = pl;
            Card = card;
            Client = new StripeClient(ApiKey);
        }

        public string Charge()
        {
            try
            {
                var card = new CreditCard
                {
                    Name = Card.Name,
                    Number = Card.CardNumber,//Card.CardNumber,
                    ExpMonth = Card.ExpMonth,
                    ExpYear = Card.ExpYear,//int.Parse(year),
                    Cvc = Card.Cvc.ToString()//cvvnumber
                };

                dynamic response = Client.CreateCharge(
                    amount: decimal.Parse(Pl[1]),
                    currency: Pl[0],
                    card: card);
                if (!response.IsError && response.Paid)
                    return response.id;
                return "Error";
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error(e,"Ошибка транзакии");
                return "Error";
            }
        }
    }
}
