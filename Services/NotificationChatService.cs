﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Domain.Context;
using Domain.Entities;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Xencore.Repository;
using Xencore.Services.Abstract;

namespace Xencore.Services
{
    public class NotificationChatService:INotificationChatService
    {
        private static Guid AdminId { get; } = Guid.Parse("11111111-1111-1111-1111-111111111111");
        public Guid StudentId { get; set; }
        public Guid TeacherId { get; set; }
        private DialogRepository DialogRepo { get; set; }
        private DialogReplyrepository DialogReply { get; set; }
        private MyDbContext Context { get; set; } = new MyDbContext();

        public NotificationChatService(Guid studentId, Guid teacherId)
        {

            this.StudentId = studentId;
            this.TeacherId = teacherId;
            this.DialogRepo = new DialogRepository(Context);
            this.DialogReply = new DialogReplyrepository(Context);
        }
        public NotificationChatService(Guid userId)
        {

            this.StudentId = userId;
            this.DialogRepo = new DialogRepository(Context);
            this.DialogReply = new DialogReplyrepository(Context);
        }

        public async Task SendAsync(string messageToUser)
        {
            var checkStudent = await DialogRepo.CreateDialogIfNotExistAsync(AdminId, StudentId);
            await DialogRepo.CreateDialogIfNotExistAsync(TeacherId, StudentId);
            var studId = await DialogReply.AddMessageAsync(checkStudent, AdminId, messageToUser);
            if (!string.IsNullOrEmpty(messageToUser))
                SendMessageToHub(StudentId.ToString(), messageToUser, studId, checkStudent);
            Context.Dispose();
        }

        public async Task SendAsync(string messageToTeacher, string messageToUser)
        {
            var checkTeacher = await DialogRepo.CreateDialogIfNotExistAsync(AdminId, TeacherId);
            var checkStudent = await DialogRepo.CreateDialogIfNotExistAsync(AdminId, StudentId);
            await DialogRepo.CreateDialogIfNotExistAsync(TeacherId, StudentId);
            var teachId = await DialogReply.AddMessageAsync(checkTeacher, AdminId, messageToTeacher);
            var studId = await DialogReply.AddMessageAsync(checkStudent, AdminId, messageToUser);
            if(!string.IsNullOrEmpty(messageToTeacher))
                SendMessageToHub(TeacherId.ToString(), messageToTeacher, teachId, checkTeacher);
            if (!string.IsNullOrEmpty(messageToUser))
                SendMessageToHub(StudentId.ToString(), messageToUser, studId, checkStudent);
            Context.Dispose();
        }
        static void SendMessageToHub(string to, string message, int dialogreplyid, int dialogid)
        {
            var from =
                new
                {
                    id = AdminId,
                    imageProfile = "/images/default-img.png",
                    name = "Service"
                };
            var hub = GlobalHost.ConnectionManager.GetHubContext<HubsOfSignalR.ChatHub>();
            hub.Clients.User(to).receiveMessage(message, JsonConvert.SerializeObject(from), dialogreplyid, dialogid);
        }
    }
}