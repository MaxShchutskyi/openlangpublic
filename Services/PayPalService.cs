﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using DTO.Enums;
using Ninject.Activation;
using PayPal.Api;
using Xencore.Managers.TablesOfPrices.Abstract;

namespace Xencore.Services
{
    public class PayPalService
    {
        private APIContext Context { get; set; }

        public PayPalService(APIContext context)
        {
            Context = context;
        }

        public Payment GetPayment(int countLessons, decimal price, string currency, Guid userId, decimal eurPrice,
            decimal eurbonus, short? discount, decimal bonus, int duration, int timeOfLesson, TypeLessons type,
            string typeOfPrice, string url = null)
        {
            var payer = new Payer() {payment_method = "paypal"};
            var transaction = new Transaction()
            {
                description = "Buy a course",
                invoice_number = Guid.NewGuid().ToString(),
                amount = new Amount()
                {
                    currency = currency,
                    total = price.ToString("F")
                },
                item_list = new ItemList
                {
                    items = new List<Item>
                    {
                        new Item
                        {
                            name = "OpenLanguage Courses",
                            currency = currency,
                            price = (price/1).ToString("F"),
                            quantity = "1",
                            sku = "sku"
                        }
                    }
                }
            };
            var redirectUrls = new RedirectUrls
            {
                return_url = HttpContext.Current.Request.Url.Scheme + "://" +
                             HttpContext.Current.Request.Url.Authority +
                             "/PayPalPayment/PayPalNewResponse?retUrl=" + url + "&payer=" + userId + "&eurPrice=" +
                             eurPrice + "&eurbonus=" + eurbonus + "&discount=" + discount + "&amount=" + price +
                             "&bonus=" + bonus + "&duration=" + duration + "&countLessons=" + countLessons +
                             "&timeOfLesson=" + timeOfLesson + "&type=" + type + "&typeOfPrice=" + typeOfPrice,
                cancel_url = HttpContext.Current.Request.Url.Scheme + "://" +
                             HttpContext.Current.Request.Url.Authority +
                             "/Openlang/LandingSite"
            };
            var pay = new Payment
            {
                intent = "sale",
                payer = payer,
                transactions = new List<Transaction>
                    {transaction},
                redirect_urls = redirectUrls
            };
                return pay.Create(Context);
        }
        public Payment GetPayment(int countLessons, decimal price, string currency, int dateId, string url = null)
        {
            var payer = new Payer() { payment_method = "paypal" };
            var transaction = new Transaction()
            {
                description = "Buy a course",
                invoice_number = Guid.NewGuid().ToString(),
                amount = new Amount()
                {
                    currency = currency,
                    total = price.ToString("F")
                },
                item_list = new ItemList
                {
                    items = new List<Item>
                    {
                        new Item
                        {
                            name = "OpenLanguage Courses",
                            currency = currency,
                            price = (price/countLessons).ToString("F"),
                            quantity = countLessons.ToString(),
                            sku = "sku"
                        }
                    }
                }
            };
            var redirectUrls = new RedirectUrls
            {
                return_url = HttpContext.Current.Request.Url.Scheme + "://"+
                    HttpContext.Current.Request.Url.Authority  +
                    "/PayPalPayment/PayPalResponse?retUrl="+ (url??HttpContext.Current.Request.UrlReferrer.ToString()) + "&dateid="+dateId,
                cancel_url = HttpContext.Current.Request.Url.Scheme + "://" +
                    HttpContext.Current.Request.Url.Authority +
                    "/Openlang/LandingSite"
            };
            var pay = new Payment
            {
                intent = "sale",
                payer = payer,
                transactions = new List<Transaction>
                {transaction},
                redirect_urls = redirectUrls
            };
            return pay.Create(Context);
        }
    }
}
