﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Xencore.Services.Abstract
{
    public interface INotificationChatService
    {
        Task SendAsync(string messageToTeacher, string messageToUser);
    }
}