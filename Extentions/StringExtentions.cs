﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Xencore.Extentions
{
    public static class StringExtentions
    {
        public static string ToUpperFirstCharacter(this string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("ARGH!");
            return input.First().ToString().ToUpper() + input.Substring(1).ToLower();
        }

        public static string ToShort(this string input, short countchar = 15)
        {
            if (input == null)
                throw new ArgumentException("ARGH!");
            if (input.Length < countchar)
                return input;
            return input.Substring(0, countchar) + "...";
        }
        public static string ToShortWithReady(this string input, string url, short countchar = 15)
        {
            if (input == null)
                throw new ArgumentException("ARGH!");
            if (input.Length < countchar)
                return input;
            return input.Substring(0, countchar) + $"<a class='readmore' href={url}>{Resources.Resource.ResourceManager.GetString("ReadMore")}</a>";
        }
        public static string ISomethingToShort(this string input, char separator)
        {
            if (string.IsNullOrEmpty(input))
                return input;
            var strs = input.Split(separator);
            var strBld = new StringBuilder();
            var newArr = strs.Reverse().Skip(1).Reverse().ToArray();
            foreach (var str in newArr)
            {
                var str1 = str.Split('_');
                var lang = str1[1];
                if(lang.Trim() == "en")
                    lang = "us";
                strBld.Append(Resources.Resource.ResourceManager.GetString(lang));
                if (!Equals(str, newArr.Last()))
                    strBld.Append(", ");
            }
            return strBld.ToString();
        }
        public static IEnumerable<object> ToJsonLearn(this string input, char separator)
        {
            var listObj = new List<object>();
            if (string.IsNullOrEmpty(input))
                return null;
            var strs = input.Split(separator);
            var newArr = strs.Reverse().Skip(1).Reverse().ToArray();
            foreach (var str in newArr)
            {
                var str1 = str.Split('_');
                listObj.Add(new {key = str1[1], lang = Resources.Resource.ResourceManager.GetString(str1[1]) });
            }
            return listObj;
        }

        public static string ToNullIfEmpty(this string input)
        {
            return string.IsNullOrEmpty(input) ? null : input;
        }

        public static string GetTechersLevel(this string input)
        {
            if (input == null)
                return "";
            JObject obj = JObject.Parse(input);
            var res =  obj.Children<JProperty>().Where(x => x.Value.Value<bool>()).Select(x => x.Name)
                .Aggregate("",
                    (current, propertyInfo) =>
                            current + Resources.Resource.ResourceManager.GetString(propertyInfo) + ", ");
            return !string.IsNullOrEmpty(res) ? res.Remove(res.Length - 2,2) : "";
        }
        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source.IndexOf(toCheck, comp) >= 0;
        }
    }
}