﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xencore.Extentions
{
    public static class ObjectExtention
    {
        public static decimal ConvertPrice(this object price, string currency)
        {
            if (!(price is decimal)) return -1;
            var pr = decimal.Parse(price.ToString());
            return new CurrencyConverter.CurrencyConverter<string>().Convert(currency, "EUR", pr).Result;

        }
    }
}