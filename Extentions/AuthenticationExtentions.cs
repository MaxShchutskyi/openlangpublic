﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using Microsoft.Owin.Security;
using WebGrease.Css.Extensions;

namespace Xencore.Extentions
{
    public static class AuthenticationExtentions
    {
        private static string GetProperty(IPrincipal principal, string prop)
        {
            var claimsIdentity = principal.Identity as ClaimsIdentity;
            return claimsIdentity?.Claims.First(x => x.Type == prop).Value;
        }
        public static string Location(this IPrincipal principal)
        {
            return GetProperty(principal, "Location");
        }
        public static string ProfilePicture(this IPrincipal principal)
        {
            return GetProperty(principal, "ProfilePicture");
        }
        public static string UserName(this IPrincipal principal)
        {
            return GetProperty(principal, "UsName");
        }
        public static string Currency(this IPrincipal principal)
        {
            return GetProperty(principal, "Currency");
        }
        public static string Timezone(this IPrincipal principal)
        {
            return GetProperty(principal, "Timezone");
        }
        public static void AddUpdateClaim(this IPrincipal currentPrincipal, string key, string value)
        {
            var identity = currentPrincipal.Identity as ClaimsIdentity;
            if (identity == null)
                return;
            var existingClaim = identity.FindFirst(key);
            if (existingClaim != null)
                identity.RemoveClaim(existingClaim);
            identity.AddClaim(new Claim(key, value));
            HttpContext.Current.GetOwinContext().Authentication.SignIn(new AuthenticationProperties() { IsPersistent = true }, identity);
        }
        public static void DeleteClaim(this IPrincipal currentPrincipal, string key, string value)
        {
            var identity = currentPrincipal.Identity as ClaimsIdentity;
            identity?.FindAll(key).ForEach(x=> identity.RemoveClaim(x));
            HttpContext.Current.GetOwinContext().Authentication.SignIn(new AuthenticationProperties() { IsPersistent = true }, identity);
        }
    }
}